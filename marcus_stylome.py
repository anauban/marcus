# -*- coding: UTF-8 -*-
from rank_distances import RankDistances
from collections import Counter, OrderedDict
from nltk import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.stem import SnowballStemmer

from scipy import stats
from scipy.cluster.hierarchy import dendrogram
from scipy.cluster.hierarchy import linkage

from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from pylab import *
import pandas as pd

import numpy as np
import os
import fnmatch
import re
from pprint import pprint

import csv_unicode
import json

stemmer_ro = SnowballStemmer("romanian")

def text_distances(tokenized_text1, tokenized_text2, stopwords, idfs, vocab_size=100, content_words=False):
    '''
    :param text1: list of words representing entire first text (tokenized)
    :param text2: list of words representing entire second text (tokenized)
    :stopwords: stopwords list
    :all words: use content words as features
    :return: distance between texts, by rank distance on their stopwords list
    '''

    frequencies1 = Counter(tokenized_text1)
    # tfidf
    if idfs:
        frequencies1_filtered = Counter({el: frequencies1[el]/float(idfs[el]) for el in frequencies1 if el in stopwords})
    else:
        frequencies1_filtered = Counter({el: frequencies1[el] for el in frequencies1 if el in stopwords})
    if content_words:
        # TODO: in this case im not using idf
        frequencies1_filtered = Counter({el:frequencies1[el] for el in frequencies1 if el not in stopwords})
    frequencies2 = Counter(tokenized_text2)
    if idfs:
        frequencies2_filtered = Counter({el: frequencies2[el]/float(idfs[el]) for el in frequencies2 if el in stopwords})
    else:
        frequencies2_filtered = Counter({el: frequencies2[el] for el in frequencies2 if el in stopwords})
    if content_words:
        # TODO: in this case im not using idf
        frequencies2_filtered = Counter({el:frequencies2[el] for el in frequencies2 if el not in stopwords})

    ranking1 = [key[0] for key in frequencies1_filtered.most_common(vocab_size)]
    ranking2 = [key[0] for key in frequencies2_filtered.most_common(vocab_size)]
    # print frequencies1_filtered.most_common(vocab_size), frequencies2_filtered.most_common(vocab_size)

    # print len(ranking1), len(ranking2)
    # TODO: do they have to be the same len?
    return 1-RankDistances.rank_distance(ranking1, ranking2)
    # return RankDistances.cosine(frequencies1_filtered, frequencies2_filtered)

def compute_idfs(words, texts):
    '''return dictionary of inverse document frequency for words
    N / nr_of_docs_with_word
    '''
    N = len(texts)

    idfs = Counter(words)
    for text in texts:
        tokenized_text = tokenize(text)
        for word in idfs:
            if word in tokenized_text:
                idfs[word] += 1
    return idfs
    
def texts_2Drepresentations(texts, vocabulary, idfs, vocabulary_length=100):
    '''
    :param text: list of texts
    :all_words: use all words in text as vocabulary, otherwise use only stopwords
    :return: distance between texts, by rank distance on their vocabulary list
    '''

    np.set_printoptions(threshold=1000000000000)
    vocabulary_frequencies = np.zeros((len(texts), len(vocabulary)))
    t = -1
    s = -1
    for text in texts:
        t += 1
        tokenized = tokenize(text)
        freqs = Counter(tokenized)
        s = -1
        for word in vocabulary:
            s += 1
            if idfs:
                vocabulary_frequencies[t][s] = freqs[word] / float(idfs[word]) / float(len(tokenized))
            else:
                vocabulary_frequencies[t][s] = freqs[word] / float(len(tokenized))

    pca = PCA(n_components=2)
    vocabulary_frequencies_2D = pca.fit_transform(vocabulary_frequencies)

    # cluster1 = []
    # for idx, pt in enumerate(vocabulary_frequencies_2D):
    #     if pt[0] > -0.00005:
    #         print "left", idx+1, texts[idx][:10]
    #     else:
    #         print "right", idx+1, texts[idx][:10]

    # print components
    comps = pca.components_
    dim1 = comps[0]
    dim2 = comps[1]
    for i, word in enumerate(vocabulary):
        print word.encode("utf-8"), dim1[i], dim2[i]

    return vocabulary_frequencies_2D
    # seems to be clustered into these groups:
    # return np.array(filter(lambda t: t[0]<-0.01, vocabulary_frequencies_2D))

def plot_2Drepresentations(data, labels, years = None, publications = None, lengths = None):
    '''
    :param years: list of texts publication years
    :param lengths: list of texts lengths
    '''
    label_colors = []
    if years:
        for label in labels:
            if label in years:
                year = int(years[label])
                if year:
                    color = (2016-year)/75.
                    label_colors.append((color, color, color))
                else:
                    color = 0
            else:
                label_colors.append(0)
    elif publications:
        publications_set = list(set(publications.values()))
        for label in labels:
            if label in publications:
                color = publications_set.index(publications[label])/150.
                label_colors.append((color, color, color))
    else:
        for label in labels:
            if "RD1" in label:
                label_colors.append('r')
            if "RD2" in label:
                label_colors.append('g')
            if "RD3" in label:
                label_colors.append('b')
            if "RD4" in label:
                label_colors.append('y')

    if years or publications:
        plt.scatter(data[:,0], data[:,1], c = label_colors, s = [c[0]*50 for c in label_colors], label="year (bigger point = older text)")
    elif lengths:
        plt.scatter(data[:,0], data[:,1], c = label_colors, s = [c/50. for c in lengths], label="text length")

    else:
        plt.scatter(data[:,0], data[:,1], c = label_colors, s=100)
    
    # plt.xlim([-0.00002, 0.00002])
    # plt.ylim([-0.00002, 0.00002])


    # legend
    if years or publications:
        plt.legend(numpoints = 1)
    else:
        red = mpatches.Patch(color='red', label='Rani deschise 1')
        green = mpatches.Patch(color='green', label='Rani deschise 2')
        blue = mpatches.Patch(color='blue', label='Rani deschise 3')
        yellow = mpatches.Patch(color='yellow', label='Rani deschise 4')
        plt.legend(handles=[red, green, blue, yellow])
    plt.savefig("marcus_PCA.png")

    plt.show()

def clean(text):
    # clean
    # footnotes
    text = text.decode("utf-8")
    # remove footers
    text = re.sub(".*", "", text)
    text = re.sub(ur"Rãni deschise.*", "", text, flags = re.UNICODE)
    # page numbers
    # TODO: does it work?
    text = re.sub("^[0-9]+$", "", text)


    text = text.lower()
    # normalize spaces. otherwise re.sub will only work on one line
    text = " ".join(text.split())
    # normalize ș - comma no cedilla
    text = re.sub(u"\u015f", u"\u0219", text, flags = re.UNICODE)
    # normalize ţ to ț
    text = re.sub(u"\u0163", u"\u021B", text, flags = re.UNICODE) 
    # normalize ă: replace ã and ā
    text = re.sub(u"\u0101", u"\u0103", text, flags = re.UNICODE)
    # normalize : replace ű with ü
    text = re.sub(u"\u0171", u"\u00FC", text, flags = re.UNICODE)
    # normalize quotes and apostrophes:
    # ˮ
    text = re.sub(u"\u02EE", u"\u0022", text, flags = re.UNICODE)
    # „
    # ”
    # “
    text = re.sub(u"\u201C", u"\u0022", text, flags = re.UNICODE)
    text = re.sub(u"\u201D", u"\u0022", text, flags = re.UNICODE)
    text = re.sub(u"\u201E", u"\u0022", text, flags = re.UNICODE)
    # ’
    text = re.sub(u"\u2018", u"\u0027", text, flags = re.UNICODE)
    text = re.sub(u"\u2019", u"\u0027", text, flags = re.UNICODE)
    # `
    text = re.sub(u"\u0060", u"\u0027", text, flags = re.UNICODE)
    text = re.sub(u"`", u"'", text, flags = re.UNICODE)

    # …
    # or not?
    text = re.sub(u"\u2026", "...", text, flags = re.UNICODE)
    # normalize hyphens
    text = re.sub(u"\u2013", u"\u0027", text, flags = re.UNICODE)
    text = re.sub(u"\u2014", u"\u0027", text, flags = re.UNICODE)

    return text

def tokenize(text, stem=True):
    text = clean(text)
    if stem:
        tokenized = map(lambda w: stemmer_ro.stem(w), word_tokenize(text))
    else:
        tokenized = word_tokenize(text)
    return tokenized

def load_stopwords(path="ro_stopwords_frommarcus.txt"):
    with open(path) as f:
        stopwords = map(lambda w: stemmer_ro.stem(clean(w)), f.read().split())
    return stopwords

def find_stopwords(text, all_stopwords_path="stopwords_all.txt", nr=None):
    '''build list of most frequent stopwords in all texts'''
    freqs = Counter(tokenize(text))
    all_stopwords = load_stopwords(all_stopwords_path)
    freqs_filtered = Counter({el: freqs[el] for el in freqs if el in all_stopwords})
    return [el[0] for el in freqs_filtered.most_common(nr)]

def load_text(path="books/RD1.txt"):
    with open(path) as f:
        return f.read()

def draw_dendrogram(matrix, labels, large=False):
    linkage_mat = linkage(matrix, method='complete')
    # linkage_mat = matrix
    dendrogram(Z=linkage_mat, labels=labels, 
        orientation='left', 
        # truncate_mode='level',
        # color_threshold=0.3*max(linkage_mat[:,2]),
        )

    # Assignment of colors to labels: 'a' is red, 'b' is green, etc.
    label_colors = {}
    for label in labels:
        if "RD1" in label:
            label_colors[label] = 'r'
        if "RD2" in label:
            label_colors[label] = 'g'
        if "RD3" in label:
            label_colors[label] = 'b'
        if "RD4" in label:
            label_colors[label] = 'y'



    fig = plt.gcf()
    if large:
        fig.set_size_inches(30.5, 140.5)
    
    # ax = plt.gca()
    # xlbls = ax.get_xmajorticklabels()
    # for lbl in xlbls:
    #     lbl.set_color(label_colors[lbl.get_text()])
    
    fig.savefig('marcus_stylome.png')

def load_excluded_paths(path="books/interviews_texts_names.txt"):
    excluded = []
    with open(path) as f:
        for line in f:
            excluded.append(line.strip())
    return excluded

def load_texts_paths(excluded_files):
    paths = []
    labels = []
    for book in ['RD1', 'RD2', 'RD3', 'RD4']:
        for file in os.listdir('books/' + book):
            if fnmatch.fnmatch(file, 'RD*.txt'):
                path = "books/" + book + "/" + file
                if file not in excluded_files and "cuprins" not in file:
                    # print file
                    paths.append(path)
                    labels.append(file)
                else:
                    pass
                    # print "excluded:", file
    return paths, labels

def draw_PCA(texts, labels, years = None, publications = None):
    '''
    :param years: list of texts publication years
    :param lengths: list of texts lengths
    '''

    all_words = True
    vocabulary_length = 1000
    stopwords = load_stopwords()
    if not all_words:
        vocabulary = stopwords[:vocabulary_length]
    else:
        words = tokenize(" ".join(texts))
        # no stopwords
        # words = filter(lambda word: word not in stopwords and not re.match(r'[,\.\(\):;–\?\'`"]+', word), words)
        vocabulary = [el[0] for el in Counter(words).most_common(vocabulary_length)]
    print vocabulary
    idfs = compute_idfs(vocabulary, texts)
    res = texts_2Drepresentations(texts, vocabulary=vocabulary, idfs=None, vocabulary_length=vocabulary_length)
    lengths = map(lambda text: len(tokenize(text)), texts)
    plot_2Drepresentations(res, labels, years = years, publications = publications, lengths = None)

def load_texts(paths):
    print "Loading text..."
    texts = []
    for path in paths:
        texts.append(load_text(path))
    return texts

def load_texts_grouped(paths):
    print "Loading text..."
    rds = [[],[],[],[]]
    for path in paths:
        if "RD1" in path:
            rds[0].append(path)
        if "RD2" in path:
            rds[1].append(path)
        if "RD3" in path:
            rds[2].append(path)
        if "RD4" in path:
            rds[3].append(path)
    texts = []
    labels = []
    for rd in range(4):
        for i in range(0, len(rds[rd]), 10):
            # chunk of 10 texts
            texts_chunk = map(lambda p: load_text(p), rds[rd][i:min(i+10, len(rds[rd]))])
            texts.append(" ".join(texts_chunk))
            labels.append("RD" + str(rd+1) + "_" + str(i) + "-" + str(min(i+10, len(rds[rd]))))
    return texts, labels

def compute_distances(texts):
    print "Tokenizing text..."
    tokenized_texts = map(lambda t: tokenize(t), texts)
    print "Computing distances..."
    n = len(texts)
    all_words = False
    distances = np.zeros((n, n))
    vocab_size = 300
    stopwords = load_stopwords()[:vocab_size]
    # print stopwords
    # TODO: optimize
    if not all_words:
        vocab = stopwords
    else:
        words = tokenize(" ".join(texts))
        # no stopwords
        words = filter(lambda word: word not in stopwords and not re.match(r'[,\.\(\):;–\?\'`"]+', word), words)
        vocab = [el[0] for el in Counter(words).most_common(vocab_size)]

    print vocab
    idfs = compute_idfs(vocab, texts)
    for i in range(n):
        for j in range(n):
            # print i, j
            distance = text_distances(tokenized_texts[i], tokenized_texts[j], stopwords = stopwords, content_words=all_words, idfs = None, vocab_size=vocab_size)
            distances[i][j] = distance
    return distances

def text_representation(text):
    pass

def print_text_rankings(texts, labels):
    stopwords = load_stopwords()
    vocab_size = 5
    for i, text in enumerate(texts):
        tokenized_text = tokenize(text)
        frequencies = Counter(tokenized_text)
        frequencies_filtered = Counter({el: frequencies[el] for el in frequencies if el in stopwords})
        ranking = [key[0] for key in frequencies_filtered.most_common(vocab_size)]
        print labels[i], ", ".join(ranking)

def label_years():
    '''return dictionary associating file names with publication years
    '''
    cuprins_paths = ["books/RD1/RD1_cuprins.csv", "books/RD2/RD2_cuprins.csv", "books/RD3/RD3_cuprins.csv", "books/RD4/RD4_cuprins.csv"]
    years = {}
    for cuprins_path in cuprins_paths:
        reader = csv_unicode.UnicodeReader(open(cuprins_path), delimiter=',', quotechar='"')
        for row in reader:
            name, path, _, _ = row
            r = re.compile("[^0-9]?((20|19)[0-9]{2})[^0-9]?")
            res = re.findall(r, name)
            year = None
            if res:
                candidates = [int(t[0]) for t in res]
                year = str(max(candidates))
            if year:
                years[path] = year
            else:
                years[path] = '0'
    return years

def label_publications():
    '''return dictionary associating file names with publication magazine etc
    '''
    cuprins_paths = ["books/RD1/RD1_cuprins.csv", "books/RD2/RD2_cuprins.csv", "books/RD3/RD3_cuprins.csv", "books/RD4/RD4_cuprins.csv"]
    publications = {}
    for cuprins_path in cuprins_paths:
        reader = csv_unicode.UnicodeReader(open(cuprins_path), delimiter=',', quotechar='"')
        for row in reader:
            name, path, _, _ = row
            try:
                publication = name.split("–".decode("utf-8"))[1].split()[0].strip()
                publications[path] = publication
            except:
                publications[path] = ''
    return publications

def load_clusters():
    '''return 2 groups with labels of the 2 clusters that form in the PCA plot,
    (read them from a file on disk where i stored them)
    '''
    with open("left_cluster") as l:
        left_labels = l.read().split()
    with open("right_cluster") as r:
        right_labels = r.read().split()
    return left_labels, right_labels

def load_texts_paths_bycluster(left_cluster=False):
    '''if left_cluster: return left cluster paths and labels, if not, return right cluster'''
    left_labels, right_labels = load_clusters()
    paths = []
    if left_cluster:
        labels = left_labels
    else:
        labels = right_labels
    for label in labels:
        book = re.findall("RD[1-4]", label)[0]
        paths.append("books/%s/%s" % (book, label))
    return paths, labels

def plot_words_frequencies_barchart(texts, nr_words=25, left_cluster=True, right_cluster=False, words=None):
    '''words: words to plot on barchart, in order
    '''
    text = " ".join(texts)
    if not words:
        words = tokenize(text)
        freqs = dict(Counter(words).most_common(nr_words))
    else:
        words_all = tokenize(text)
        words_filtered = filter(lambda w: w in words, words_all)
        freqs = dict(Counter(words_filtered))
    # freqs = {w.encode("utf-8"): freqs[w] for w in freqs}
    if left_cluster:
        name = 'words_freqs_left.png'
    elif right_cluster:
        name = 'words_freqs_right.png'
    else:
        name = 'words_freqs.png'
    bar_graph(freqs, '', name)

def plot_words_frequencies_barcharts(left_cluster, right_cluster, nr_words=25):
    '''left_cluster: list of texts in left cluster
    right_cluster: list of texts in right cluster
    plot 3 barcharts: left cluster, right cluster, all
    '''
    print "Computing words frequencies..."
    words = tokenize(" ".join(left_cluster+right_cluster))
    print "Counting..."
    # TODO: or get the most common words in left cluster, along with most common words in right cluster?
    freqs = dict(Counter(words).most_common(nr_words))
    top_words = freqs.keys()
    print " ".join(top_words).encode("utf-8")

    print "Plotting words barchart..."
    plot_words_frequencies_barchart(left_cluster, left_cluster=True, right_cluster=False, words=top_words)
    plot_words_frequencies_barchart(right_cluster, left_cluster=False, right_cluster=True, words=top_words)
    plot_words_frequencies_barchart(left_cluster + right_cluster, left_cluster=False, right_cluster=False, words=top_words)

def bar_graph(name_value_dict, graph_title='', output_name='bargraph.png'):
    figure(figsize=(8, 4)) # image dimensions   
    title(graph_title, size='x-small')
    
    # add bars
    # sort
    name_value_dict = OrderedDict(sorted(name_value_dict.items(), key = lambda x: x[0]))
    for i, key in zip(range(len(name_value_dict)), name_value_dict.keys()):
        bar(i + 0.25 , name_value_dict[key], color='red')
    
    # axis setup
    xticks(arange(0.65, len(name_value_dict)), 
        [('%s' % (name)) for name, value in 
        zip(name_value_dict.keys(), name_value_dict.values())], 
        size='xx-small')
    max_value = max(name_value_dict.values())
    tick_range = arange(0, max_value, max_value/10)
    yticks(tick_range, size='xx-small')
    formatter = FixedFormatter([str(x) for x in tick_range])
    gca().yaxis.set_major_formatter(formatter)
    gca().yaxis.grid(which='major') 
    
    savefig(output_name)

def lexical_richness_for_text(path):
    text = load_text(path)
    stopwords = load_stopwords()
    tokenized_text = [w for w in tokenize(text) if w not in stopwords]
    tokens = len(tokenized_text)
    types = len(set(tokenized_text))
    return float(types)/tokens

def lexical_density_for_text(path):
    text = load_text(path)
    stopwords = load_stopwords()
    tokenized_text = [w for w in tokenize(text, stem=False)]# if w not in stopwords]
    tokens = len(tokenized_text)
    types = len(set(tokenized_text))
    return float(types)/tokens

def average_word_length_for_text(path):
    text = load_text(path)
    stopwords = load_stopwords()
    tokenized_text = [w for w in tokenize(text, stem=False) if w not in stopwords]
    average_word_length = float(sum([len(w) for w in tokenized_text])) / len(tokenized_text)
    return average_word_length

def stopword_density_for_text(path):
    text = load_text(path)
    stopwords = load_stopwords()
    tokenized_text = [w for w in tokenize(text, stem=False)]# if w not in stopwords]
    text_stopwords = [w for w in tokenized_text if w in stopwords]
    stopword_density = float(len(text_stopwords)) / len(tokenized_text)
    return stopword_density

def average_sentence_length_for_text(path):
    text = clean(load_text(path))
    words = word_tokenize(text)
    sentences = sent_tokenize(text)
    return float(len(words)) / len(sentences)

def automated_readability_index_for_text(path):
    text = clean(load_text(path))
    c = len(text)
    w = len(word_tokenize(text))
    s = len(sent_tokenize(text))
    return 4.71 * (float(c)/w) + 0.5 * (float(w) / s) - 21.43

def plot_text_property_evolution(fn=lexical_richness_for_text):
    '''Plot evolution of a property of the texts across years.
    fn is a function computing this property given a text path.
    e.g. lexical richness
    Measures takes from here: http://www.aclweb.org/anthology/W11-4112
    '''
    excluded_files = load_excluded_paths(path="books/interviews_texts_names.txt") + \
                    load_excluded_paths(path="books/nonro_texts_names.txt") + \
                    load_excluded_paths(path="books/otherexcept_texts_names.txt")

    print "Loading paths..."
    paths, labels = load_texts_paths(excluded_files)
    years = label_years()
    property = {}
    for path in paths:
        clean_path = path.split("/")[-1]
        property[clean_path] = fn(path)
    common_paths = [p for p in property.keys() if p in years.keys() and int(years[p])>0]
    y_data = [property[p] for p in sorted(common_paths, key=lambda p: int(years[p]))]
    x_data = sorted([int(years[p]) for p in common_paths])
    sorted_paths = sorted(common_paths, key=lambda p: int(years[p]))
    label_colors = []
    for label in sorted_paths:
        if "RD1" in label:
            label_colors.append('r')
        if "RD2" in label:
            label_colors.append('g')
        if "RD3" in label:
            label_colors.append('b')
        if "RD4" in label:
            label_colors.append('y')
    print years
    print property
    print len(x_data), len(y_data)
    print x_data, y_data
    plt.scatter(x_data, y_data, c=label_colors, s=100)
    red = mpatches.Patch(color='red', label='Rani deschise 1')
    green = mpatches.Patch(color='green', label='Rani deschise 2')
    blue = mpatches.Patch(color='blue', label='Rani deschise 3')
    yellow = mpatches.Patch(color='yellow', label='Rani deschise 4')
    plt.legend(handles=[red, green, blue, yellow])
    plt.xlabel("year essay was written")
    # plt.ylabel("average word length in essay")
    plt.show()

def _count_tokens(text, stem=False):
    tokens = tokenize(text, stem=stem)
    return len(tokens)

def _count_types(text, stem=False):
    tokens = tokenize(text, stem=stem)
    return len(set(tokens))

def _count_sentences(text):
    return len(split_sentences(text))

def split_sentences(text):
    '''Split text into sentences
    '''
    #TODO: improve
    return re.split("[\.\?!]", text)

def compute_lexical_density(text):
    nrtypes = _count_types(text)
    nrtokens = _count_tokens(text)
    return float(nrtypes)/nrtokens

def compute_lexical_richness(text):
    nrtypes = _count_types(text, stem=True)
    nrtokens = _count_tokens(text)
    return float(nrtypes)/nrtokens

def compute_avg_wlen(text):
    words = tokenize(text, stem=False)
    avg_wlen = float(sum([len(w) for w in words])) / len(words)
    return avg_wlen

def compute_avg_slen(text):
    sentences = split_sentences(text)
    tokens = tokenize(text, stem=False)
    avg_slen = float(len(tokens)) / len(sentences)
    return avg_slen

def compute_readability(text):
    s = float(len(split_sentences(text)))
    w = float(len(tokenize(text, stem=False)))
    # TODO: should we exclude non-word characters here?
    c = float(len(text))
    LR = 4.71 * (c/w) + 0.5 * (w/s) - 21.43
    return LR

def compute_metrics_evolution():
    texts = []
    paths = []
    labels = []
    excluded_files = load_excluded_paths(path="books/interviews_texts_names.txt") + \
                    load_excluded_paths(path="books/nonro_texts_names.txt") + \
                    load_excluded_paths(path="books/otherexcept_texts_names.txt")

    print "Loading paths..."
    paths, labels = load_texts_paths(excluded_files)
    years = label_years()

    texts = load_texts(paths)

    metrics = {}

    for i, text in enumerate(texts):
        label = labels[i]
        metrics[label] = {'readability': 0, 'lexical_richness': 0, 'lexical_density': 0, 'avg_senlen': 0, 'avg_wlen': 0}
        metrics[label]['readability'] = compute_readability(text)
        metrics[label]['lexical_richness'] = compute_lexical_richness(text)
        metrics[label]['lexical_density'] = compute_lexical_density(text)
        metrics[label]['avg_senlen'] = compute_avg_slen(text)
        metrics[label]['avg_wlen'] = compute_avg_wlen(text)
        metrics[label]['year'] = years[label]

    plot_metrics_evolution(metrics, years, ['readability'])
    plot_metrics_evolution(metrics, years, ['avg_senlen'])
    plot_metrics_evolution(metrics, years, ['lexical_richness'])
    plot_metrics_evolution(metrics, years, ['avg_wlen'])

    # compute_statistical_significance(metrics, years)


    return metrics

def compute_statistical_significance(metrics, years, metrics_list=['readability', 'avg_senlen', 'lexical_density', 'lexical_richness', 'avg_wlen'], split_year=1990):
    years = {l: int(years[l]) for l in years.keys() if l in metrics.keys() and years[l]!='0'}
    significance = {}
    statistic = {}
    for m in metrics_list:
        communism_metric = [metrics[l][m] for l in years.keys() 
            if years[l] < split_year]
        democracy_metric = [metrics[l][m] for l in years.keys()
            if years[l] >= split_year]
        # standardized_metric = stats.mstats.zscore(communism_metric + democracy_metric)
        standardized_metric = communism_metric + democracy_metric
        communism_metric = standardized_metric[:len(communism_metric)]
        democracy_metric = standardized_metric[len(communism_metric):]
        print "communism articles: %d, democracy articles: %d" % (len(communism_metric), len(democracy_metric))
        print "communism average for %s: %f; democracy average for %s: %f" % (m, np.mean(communism_metric), m, np.mean(democracy_metric))
        print "communism variance for %s: %f; democracy variance for %s: %f" % (m, np.var(communism_metric), m, np.var(democracy_metric))
        print "Statistical significance for %s in %d: t-statistic, pvalue = " % (m, split_year), stats.ttest_ind(communism_metric, democracy_metric)
        significance[m] = stats.ttest_ind(communism_metric, democracy_metric)[1]
        statistic[m] = stats.ttest_ind(communism_metric, democracy_metric)[0]

        # print "Random split:"
        # for i in range(1, len(standardized_metric), 10):
        #     communism_metric = standardized_metric[:i]
        #     democracy_metric = standardized_metric[i:]
        #     print "first period articles: %d, democracy articles: %d" % (len(communism_metric), len(democracy_metric))
        #     # print "first period average for %s: %f; democracy average for %s: %f" % (m, np.mean(communism_metric), m, np.mean(democracy_metric))
        #     # print "first period variance for %s: %f; democracy variance for %s: %f" % (m, np.var(communism_metric), m, np.var(democracy_metric))
        #     print "Statistical significance for %s: t-statistic, pvalue = " % m, stats.ttest_ind(communism_metric, democracy_metric)
    return significance, statistic

def plot_metrics_evolution(metrics, years, metrics_list = ['avg_senlen']):
    years = {l: int(years[l]) for l in years.keys() if l in metrics.keys() and years[l]!='0'}
    chronological_years_list = sorted(set(years.values()))
    # texts corresponding to years
    inverted_years = {}
    for label in years.keys():
        year = years[label]
        if year not in inverted_years:
            inverted_years[year] = []
        inverted_years[year].append(label)
    def average(l):
        return float(sum(l))/len(l) 
    plt.figure(figsize=(4,3))
    significances = {}
    statistics = {}
    for y in chronological_years_list:
        significances[y], statistics[y] = compute_statistical_significance(metrics, years, split_year=y)

    # subplot_nrs = [211, 212, 221, 222]
    # fig, ax = plt.subplots(2, 2)
    fig, ax1 = plt.subplots()
    for i, m in enumerate(metrics_list):
        # ax.subplot(subplot_nrs[i])
        ax2 = ax1.twinx()
        metric = [average([metrics[l][m] for l in inverted_years[y]]) for y in chronological_years_list]
        significance = [significances[y][m] for y in chronological_years_list]
        statistic = [statistics[y][m] for y in chronological_years_list]
        metric = pd.rolling_mean(pd.Series(metric), 10)

        # ax1.plot(chronological_years_list, metric, label=m)
        ax1.plot(chronological_years_list, statistic, 'g+')
        ax1.set_xlabel('year')
        # Make the y-axis label, ticks and tick labels match the line color.
        ax1.set_ylabel(m, color='b')

        ax2.set_yscale("log")
        for i, y in enumerate(chronological_years_list):
            if y==1990:
                only1990s = significance[i]
        ax2.plot(chronological_years_list, significance, 'r.')
        # ax2.plot([1990], only1990s, 'r.')
        ax2.set_ylabel('p-value significance of split', color='r')

        plt.axvline(x=1990, linestyle=':', color='k')
        plt.savefig(m + '_evolution.png', bbox_inches='tight')
    # plt.savefig('metrics_evolution.png', bbox_inches='tight')
    # ax1.legend(loc=0)

    # ax2.plot(chronological_years_list, [len(inverted_years[y]) for y in chronological_years_list], 'r.')
    # ax2.set_ylabel('nr_articles', color='r')
    # fig.tight_layout()
    # plt.show()

def main():
    texts = []
    paths = []
    labels = []
    excluded_files = load_excluded_paths(path="books/interviews_texts_names.txt") + \
                    load_excluded_paths(path="books/nonro_texts_names.txt") + \
                    load_excluded_paths(path="books/otherexcept_texts_names.txt")

    print "Loading paths..."
    paths, labels = load_texts_paths(excluded_files)
    # paths1, labels1 = load_texts_paths_bycluster(left_cluster=False)
    # paths2, labels2 = load_texts_paths_bycluster(left_cluster=True)
    # paths = ["books/RD1.txt", "books/RD2.txt", "books/RD3.txt", "books/RD4.txt"]
    # labels = paths
    years = label_years()

    # texts = load_texts(paths)
    texts, labels = load_texts_grouped(paths)
    # paths1 = paths[:2]
    # paths2 = paths[2:]
    # texts1 = load_texts(paths1)
    # texts2 = load_texts(paths2)
    # # print "\n".join(find_stopwords(" ".join(texts), nr=200))
    
    distances = compute_distances(texts)
    # draw_dendrogram(distances, labels)#, large=True)

    # plot_words_frequencies_barcharts(texts2, texts1)
    draw_PCA(texts, labels)#, years)#, publications=publications)
    # print "\n".join(labels)
    # print_text_rankings(texts, labels)
    # 

if __name__ == '__main__':
    # main()
    # plot_text_property_evolution(fn=average_word_length_for_text)
    compute_metrics_evolution()