% Created 2017-10-04 Wed 00:45
\documentclass[11pt]{article}
                  \usepackage{lrec}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage[margin=25mm,bottom=25mm,left=25mm,right=25mm]{geometry}
\usepackage[romanian, english]{babel}
\usepackage{combelow}
\usepackage{newunicodechar}
\newunicodechar{Ș}{\cb{S}}
\newunicodechar{ș}{\cb{s}}
\newunicodechar{Ț}{\cb{T}}
\newunicodechar{ț}{\cb{t}}
\name{Liviu P. Dinu, Ana Sabina Uban}

\address{Faculty of Mathematics and Computer Science, University of Bucharest, \\
         {\tt liviu.p.dinu@gmail.com}, {\tt ana.uban@gmail.com}}
\date{\today}
\title{Analyzing Stylistic Variation across Different Political Regimes}
% \begin{document}

% \maketitle
\abstract{
In this article we propose a stylistic analysis of texts written across two different periods, which differ not only temporally, but politically and culturally: democracy and communism in Romania.
We aim to analyze the stylistic variation between texts of these two periods, and determine at what levels
the variation is more apparent (if any): at the stylistic level, at the topic level etc.
We take a look at the stylistic profile of these texts comparatively, by performing clustering and classification experiments on the texts,
using traditional authorship attribution methods and features (bag-of-stopwords). We also perform a simple
analysis of the variation in topic between the two epochs, to compare with the varation at style level.
These analyses show that texts from the two periods can indeed be distinguished, even using simple
bag-of-words features. While the stylistic variation is apparent, the analysis also hints at a variation at the topic level,
though more analysis could help confirm and elaborate on this. Furthermore, the promising results suggest that
a closer look at the details of the stylistic and topic variation, using more sophisticated methods,
would be interesting in future experiments.
\textbf{Keywords:} authorship attribution, stylome classification, stylistic variation, Romanian
}

\begin{document}
\maketitleabstract
\section{Previous Work and Motivation}
\label{sec-1}


Automated authorship attribution has a long history (starting from the early 20th century \cite{mendenhall1901mechanical}) and has since then been
extensively studied and elaborated upon. The problem of authorship identification is based on the assumption 
that there are stylistic features that can help distinguish the real author of a text from any other theoretical author.
This set of stylistic features was recently defined as a linguistic fingerprint
(or stylome), which can be measured, is largely unconscious, and is constant \cite{van2005new}. 
One of the oldest studies to propose an approach to this problem is on the issue of the \emph{Federalist Papers},
in which \cite{mosteller1963inference} try to determine the real author of a few of these papers which have
disputed paternity. This work remains iconic in the field, both for introducing a standard dataset and for
proposing an effective method for distinguishing between the author's styles, that is still relevant to this day,
based on function words frequencies. Many other types of features have been proposed and successfully used 
in subsequent studies to determine the author of a text. These types of features generally contrast with the 
content words commonly used in text categorization by topic, and are said to be used unconsciously and harder
to control by the author. Such features are, for example, grammatical structures \cite{baayen1996outside},
part-of-speech n-grams \cite{koppel2003exploiting}, lexical richness \cite{tweedie1998variable}, or even the more general
feature of character n-grams \cite{kevselj2003n,dinu2008authorship}. Having applications that go beyond
finding the real authors of controversial texts, ranging from plagiarism detection to forensics to security,
stylometry has widened its scope into other related subtopics such as author verification (verifying whether a text
was written by a certain author) \cite{koppel2004authorship}, author profiling (e.g. gender or age prediction),
author diarization, or author masking (given a document, paraphrase it so that the original style does not match that of its original author anymore).

In this work we attempt to explore a slightly different topic, strongly related with the stylistic fingerprint of an 
author, namely if an author preserves his stylome across different time periods, and across political and cultural environments.
To be more precise, we want to see if we can discriminate between texts written under a communist regime,
as compared to democracy. More than that, we are interested whether this effect can be seen within the works of a single author or, at a larger scale, at the level
of many authors living and writing in the same environment.

We aim to include in our full study various Romanian authors; but we focus in this preliminary analysis on Solomon Marcus (1925-2016), one of the most prominent scientists and men of culture of modern Romania.
As a scientist, he was active in an impressive range of different fields, such as mathematics, computer science, mathematical and computational linguistics,
semiotics etc, and published over 50 books translated in more than 10 languages,
and about 400 research articles in specialized journals in almost all European countries. 
He is one of the initiators of mathematical linguistics \cite{marcus1971introduzione} and of mathematical poetics \cite{marcus1973mathematische},
and has been a member of the editorial board of tens of international scientific journals covering all
his domains of interest. As a man of culture,
he has written an equally impressive amount of texts, having as preferred topics mathematical education, culture, science, children, etc.
His wide interests and complex personality have left deep imprints in the Romanian scientific and cultural world.


We chose these works and this period in Romanian history as a particularly interesting study case for stylistic variation.
Marcus' essays \cite{marcus2012rani} were written over a period spanning almost half a century:
22 years of communist regime and 27 years of democracy (after the fall of communism in Romania in 1989).
The significant changes in Romania's cultural life with the fall of communism motivate such an analysis beyond simple temporal
analysis of texts: in the pre-democracy period, the communist norms demanded an elaborated writing style,
whereas the list of officially approved topics was much more limited.
On the other hand, with the fall of communism, we expect that the author would
would be able to radically change his style to express his ideas freely.



\section{The Corpus}
\label{sec-2}

For the purposes of the analysis presented here, we used a collection of Solomon Marcus' non-scientific texts.
The whole collection of Marcus’ non-scientific publications is available in print,
in six volumes entitled "Răni deschise" (\emph{Open wounds}), of which we analyze the first four: two containing texts written
before the fall of communism, and the other two with texts written under democracy.
The first volume comprises 1256 pages of texts, conferences and interviews, from 2002 to 2011 (and a few published before 1990).
The second volume "Cultură sub dictatură" (\emph{Culture under dictatorship}) of 1088 pages and the third "Depun Mărturie" (\emph{I testify}), of 668 pages,
is a collection of texts published between 1967 and 1989. The fourth volume "Dezmeticindu-ne" (\emph{Awaking}), of 1030 pages, contains texts from
 the period immediately following the Romanian Revolution (December 1989) to the year 2011.

Before the texts could be used for automatic analysis, some pre-processing was involved, including extracting the texts from PDF files into
text form, and splitting the corpus into individual essays.
We also parsed the table of contents in order to label each text with its year of publication. Additionally, we excluded some texts
that would interfere with the experiments, such as texts in languages different than Romanian (most of the texts are in Romanian),
and interviews (which also contain lines of the interviewer, that should not be considered when analysing text authored by Solomon Marcus).

\section{Methodology}
\label{sec-3}

In order to analyze the style of Marcus' work and its variation, we start by taking a look the involuntary fingerprint of the two distinct periods.
Putting aside the content of the texts or the imposed style of the period, it is interesting to see whether Marcus' style was affected in depth,
at a subcoscious level.
To answer this question, we resort to one of the most commonly accepted indicators of personal style of an author,
namely the distribution of functional words, such as pronouns, determiners, prepositions, etc, which were successfully used in many
previous studies to solve authorship attribution problems.
Here we use a curated list of 120 functional Romanian words \cite{dinu2012pastiche}. 



\subsection{Clustering Experiments}
\label{sec-3-1}

Processing the text involves extracting all function words from each essay, and representing each text as a vector of frequencies of function words.
On these vectors we then apply similarity metrics in order to cluster texts based on their stylistic profile.
The distance we have chosen is rank distance \cite{dinu2003classification}, \cite{popescu2008rank},
an ordinal distance which was successfully previously used in other problems of authorship \cite{dinu2008authorship}, \cite{dinu2012pastiche}.

For our purposes, we applied a hierarchical clustering algorithm, and plotted the resulted dendrogram, as shown in Figure 1.
Texts from each volume are marked with the prefix \emph{RD<volume\_number>}.
The intention is to see whether texts from the same period are clustered together: in this case, we would expect to see texts
from volumes 1 and 4 (communism) appear close together, and texts from volumes 2 and 3 (post-communism) appear in a separate group.

An additional processing step we performed before applying the clustering algorithm was to group the essays into longer texts, while
keeping them separated into volumes (texts from different volumes/periods were not grouped together). For each volume,
batches of 10 essays were grouped toghether in 1 text.
This results in a fewer number of texts, which can be more easily plotted on a dendrogram, and additionally should contain less
noise than the very short original texts.

\begin{figure}[htb]
\centering
\includegraphics[width=1.0\linewidth]{../plots/marcus_stylome.png}
\caption{Style dendrogram: Hierarchical clustering of function word vectors}
\end{figure}

\subsection{Dimensionality Reduction and Visualization}
\label{sec-3-2}

In addition to the clustering experiments, we perform an experiment where we plot the essays in the 4 volumes in 2-D space,
by initially applying dimensionality reduction (principal component analysis) on the texts, represented as function word frequency
vectors, as described above. The resulted plot is shown in Figure 2.


\begin{figure}[htb]
\centering
\includegraphics[width=1.0\linewidth]{../marcus_stopwords_PCA_new.png}
\caption{Style space: 2-D visualization of function word vectors}
\end{figure}



In a separate experiment, we take a brief look at the content profile of the texts (as opposed to the stylistic one), by performing the same
analysis on content words instead of function words. We extract all content words from all the (grouped) texts, and represent the texts as 
vectors of content words frequencies. We then apply PCA on the resulted vector space, and plot the 2-dimensional representations of
the texts, as shown in Figure 3. Although this is still a very crude analysis, the result should provide an insight into the content similarities and dissimilarities
between the texts in the 4 volumes.

We also perform a version of the clustering experiments on the same vectors of content words, with the same methodology that was applied in the
stylistic analysis: the rank distance similarity is applied on the word vectors, and the distances are used in a hierarchical clustering algorithm,
which results in the "content" dendrogram in Figure 4.


\begin{figure}[htb]
\centering
\includegraphics[width=1.0\linewidth]{../marcus_topic_grouped2.png}
\caption{Topic dendrogram: Hierarchical clustering on content word vectors}
\end{figure}


\begin{figure}[htb]
\centering
\includegraphics[width=1.0\linewidth]{../marcus_contentwords_PCA_new.png}
\caption{Topic space: 2-D visualization of content word vectors}
\end{figure}

\subsection{Classification Experiments}
\label{sec-3-3}

In order to have a more conclusive result regarding the differences between the texts written in the two epochs and the levels at which
they occur, we also performed some classification experiments where we try to predict for each essay if it was written before or
after the fall of communism, as well as to which specific volume it pertains.

For classifying the texts we used an SVM classifier with a linear kernel, and tested its performance in a series of leave-one-out experiments. As a featureset,
we used a simple bag-of-words model, and experimented both with features specific for modelling style and topic: functional words and content words, respectively.
Results are shown in Table 1 and Table 2: in Table 1 we report the results of trying to classify the texts into the two distinct periods, while Table 2 shows the
results of classifying each text into the specific volume to which it pertains.

\section{Results and Analysis}
\label{sec-4}

The clustering experiments, as well as the 2-dimensional visualizations, show a clear distinction between the works of the two periods.
The style dendrogram in Figure 1 shows that texts tend to be grouped according to the volume they belong to, but moreover,
the volumes belonging to each period tend to be grouped together.
It is noticeable that texts belonging to the first and fourth volumes, written during the post-communist epoch (marked "RD1" and "RD4"),
are grouped together on the lower branch of the dendrogram, and the same happens with texts in volumes 2 and 3 ("RD2" and "RD3"), written
during communism, which are grouped on the upper branch.

The same tendency is visible in the 2-dimensional vector space plot of the texts. It is interesting that the separation
between the pre-communist texts (marked with green and blue points) and communist texts (marked with red and yellow points)
seems even more prominent than the separation between each of the four volumes - indicating that the main variable contributing
to style differences is indeed the period when the text was written.

We can conclude from these results that, independently on possible variation at other levels (topic, etc),
there is inarguable variation between the texts at a stylistic level.

A similar but less obvious effect is noticeable in the content analysis. While the vector space plot of the texts again shows
a reasonably clear distinction between the texts in the two periods, the cluster analysis is less conclusive. 

\begin{table}[htb]
\centering
\begin{tabular}{ll}
Feature set & Precision\\
\hline
function words & 75.80\%\\
content words & 74.46\%\\
\end{tabular}\caption{\label{results1}Results of classifying texts into periods}

\end{table}


\begin{table}[htb]
\centering
\begin{tabular}{ll}
Feature set & Precision\\
\hline
function words & 61.29\%\\
content words & 84.13\%\\
\end{tabular}\caption{\label{results2}Results of classifying texts into volumes}

\end{table}

The results of the classification experiments very clearly show that essays can be easily separated into the two classes,
both when using function words and content words as a feature set. We can conclude, given all of these results, that during the two periods, corresponding
to two different political regimes, Marcus produced works which are clearly distinct both stylistically and from the
point of view of the topics discussed. It is noteworthy that the classifier manages not only to classify the texts into
the two periods in which they were written, but is also successful at predicting the exact volume to which each essay pertains.
This suggests there may be other (volume-specific) factors contributing to the distinctiveness of the essays in each class, which
it may be interesting to further explore. The stylistic features, in comparison to the content words, seem to be more successful
at predicting the period rather than the specific volume, indicating that style may indeed be the aspect of the texts that varies
the most between the two periods and political regimes.


\section{Conclusions and Future Directions}
\label{sec-5}

We have proposed in this paper an analysis of the stylistic variation of the works of Solomon Marcus
between two distinct historical periods, and have presented some initial results that show the separation
between the two periods is indeed clear at the level of the author's style.
The clustering approach based on the preference of the author regarding the functional words
shows that the functional words were unconsciously used by author in a different way in communism than in democracy period,
and we can use them to discriminate between the communist and post-communist texts of the given author.
We further confirm in several classification experiments, that the texts can be automatically separated into
the two periods, and are distinguishable both at the level of style and topic.

In future work, it would be very interesting to examine more closely how this variation is manifested.
From the stylistic point of view: what are the exact changes that occur with the passing of time and
the change of historical and cultural context? 
From the point of view of topic modelling, what are the specific changes in the topic of the text
corresponding to the two periods, if any?
Do these changes match our intuitions about writing in the communist as compared to post-communist era?

Another important question is to distinguish between the factors responsible for the stylistic variation.
Though the analysis hints at a separation between the two epochs, it is not clear how much of this effect
is due to intrinsic variation in the author's style with the passing of time, and how much is related to
the political and cultural context. Correlating the stylistic variation with the exact year of publication
(or a finer-grained indicator of period, not only a binary pre/post-democracy split) could be helpful in this direction.
Furthermore, it would be interesting to see what kind of changes in style are caused by which kind of factors.

Finally, continuing this analysis on other Romanian authors with works in both communism and post-communism
would be interesting to confirm the phenomenon is not unique to this author. Possibly extending this to
other authors in different cultural contexts could lead to an interesting analysis on the external factors
that influence style.


\bibliographystyle{lrec}
\bibliography{article}{}
% Emacs 24.5.1 (Org mode 8.2.10)
\end{document}
