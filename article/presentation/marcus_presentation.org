#+TITLE:     Analyzing Stylistic Differences \\ across Political Regimes
#+AUTHOR:    Liviu P. Dinu, Ana Sabina Uban
#+LaTeX_HEADER: \institute[HLT Bucharest]{Faculty of Mathematics and Computer Science, University of Bucharest \\ Human Language Technologies Research Center}
#+EMAIL:     ana.uban@gmail.com
#+DATE:      March 2018, CICLing
#+DESCRIPTION: 
#+KEYWORDS: 
#+LANGUAGE:  en
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+BEAMER_FRAME_LEVEL: 1
#+OPTIONS: H:2 toc:t
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME:
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:notintoc

#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+LATEX_HEADER: \usecolortheme[RGB={16,78,139}]{structure} 
#+LATEX_HEADER: \definecolor{lightblue}{RGB}{212,215,255}
#+LATEX_HEADER: \setbeamercolor{structure}{bg=lightblue}
#+LATEX_HEADER: \setbeamertemplate{blocks}[rounded]
#+LATEX_HEADER: \useoutertheme{infolines}
#+LATEX_HEADER: \useinnertheme{circles}
# diacritics
#+LATEX_HEADER: \usepackage[romanian]{babel}
#+LATEX_HEADER: \usepackage{combelow}
#+LATEX_HEADER: 
#+LATEX_HEADER: \usepackage{newunicodechar}
#+LATEX_HEADER: 
#+LATEX_HEADER: \newunicodechar{Ș}{\cb{S}}
#+LATEX_HEADER: \newunicodechar{ș}{\cb{s}}
#+LATEX_HEADER: \newunicodechar{Ț}{\cb{T}}
#+LATEX_HEADER: \newunicodechar{ț}{\cb{t}}
#+latex_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Topic}\tableofcontents[currentsection]\end{frame}}
* Does the political regime influence style?
** Stylome  
- **Stylome**: stylistic fingerprint, distinguishes between authors (authorship attribution)
\\
- External factors: socio-political environment

* Data: Communist Romania & Solomon Marcus
  
** Romanian Communism
- Romania: communist from 1947-1989 and democratic after 1990
- Specific norms around writing:
  - Some topics were taboo
  - Writing style: elaborated, technical, complex sentences, complex words
- Did the change in political regime affect the writing style of authors living and writing in these periods? 


** Solomon Marcus 
- Important Romanian scientist and intellectual, extensive body of work
- Not politically engaged. Favorite topics: mathematics, education, science, culture
- Was his style affected subconsciously?
- Non-scientific texts in /Răni deschise/ (/Open Wounds/): essays spanning 22 years of communism (RD2, RD3) and 27 years of democracy (RD1, RD4)

* Classification
** Classification 
Distinguishing automatically between the 2 periods
- style level
- topic level (in parallel, for a better understanding)

Model:
- linear SVM classifier
- 2 sets of features (BoW, complex features) 

** Bag-of-Words Models
Feature set:
*** Style level
- functional words - unconsciously used
*** Semantic level
- content words


** Complex Features
*** Stylistic metrics
- Automated Readability Index
$$ ARI = 4.71\frac{c}{w} + 0.5\frac{w}{s} - 21.43 $$

- average word length
- average sentence length
- lexical richness
$$ LR = \frac{number\ of\ unique\ lemmas}{total\ number\ of\ tokens} $$

*** Topic modelling
- Latent Dirichlet Allocation for topic extraction
- 3 latent topics per text

** Results
*** 
#+CAPTION: Results of classifying texts into periods
#+NAME: results1
| Feature set                         | Precision |
|-------------------------------------+-----------|
| function words (style)              |    75.80% |
| stylistic metrics (style)           |    73.12% |
| content words (semantic content)    |    74.46% |
| topic modelling (semantic content) |    71.24% |

** 2D Style Space

#+CAPTION: Style space: 2-D visualization of function word vectors
#+ATTR_LATEX: :width 0.7\linewidth
     [[../../marcus_stopwords_PCA_new.png]]
     
** 2D Semantic Space
#+CAPTION: Topic space: 2-D visualization of content word vectors
#+ATTR_LATEX: :width 0.7\linewidth
     [[../../marcus_contentwords_PCA_new.png]]

* Effect of time vs change of political regime
** Variation of stylistic metrics over time
*** 

- How much of the change is the effect of time and how much the effect of change in regime?
- Difference in the distribution of the metrics before and after the Revolution is statistically significant?
  


** Variation of stylistic metrics over time

\begin{figure}[h]
 
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/avg_senlen_evolution_simple.png} 
\label{fig:subim1}
\end{subfigure}
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/avg_wlen_evolution_simple.png}
\label{fig:subim2}
\end{subfigure}

\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/readability_evolution_simple.png} 
\label{fig:subim1}
\end{subfigure}
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/lexical_richness_evolution_simple.png}
\label{fig:subim2}
\end{subfigure}
\caption{Evolution of stylistic metrics over time}
\label{fig:image2}
\end{figure}
  

** Variation of stylistic metrics over time

\begin{figure}[h]
 
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/avg_senlen_evolution2.png} 
\label{fig:subim1}
\end{subfigure}
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/avg_wlen_evolution2.png}
\label{fig:subim2}
\end{subfigure}

\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/readability_evolution2.png} 
\label{fig:subim1}
\end{subfigure}
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/lexical_richness_evolution2.png}
\label{fig:subim2}
\end{subfigure}
\caption{Evolution of stylistic metrics over time}
\label{fig:image2}
\end{figure}
  
** Variation of stylistic metrics over time

\begin{figure}[h]
 
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/avg_senlen_evolution.png} 
\label{fig:subim1}
\end{subfigure}
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/avg_wlen_evolution.png}
\label{fig:subim2}
\end{subfigure}

\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/readability_evolution.png} 
\label{fig:subim1}
\end{subfigure}
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/lexical_richness_evolution.png}
\label{fig:subim2}
\end{subfigure}
\caption{Evolution of stylistic metrics over time}
\label{fig:image2}
\end{figure}
  
** Variation of stylistic metrics over time
#+CAPTION: Evolution of sentence length
#+ATTR_LATEX: :width 0.8\linewidth
     [[../../metrics_evolution/avg_senlen_evolution.png]]
** Variation of stylistic metrics over time
#+CAPTION: Evolution of word length
#+ATTR_LATEX: :width 0.8\linewidth
     [[../../metrics_evolution/avg_wlen_evolution.png]]
     
** Variation of stylistic metrics over time
#+CAPTION: Evolution of readability
#+ATTR_LATEX: :width 0.8\linewidth
     [[../../metrics_evolution/readability_evolution.png]]
    
** Variation of stylistic metrics over time
#+CAPTION: Evolution of lexical richness
#+ATTR_LATEX: :width 0.8\linewidth
     [[../../metrics_evolution/lexical_richness_evolution.png]]

* Conclusions
** Conclusions 
***  
- The 2 periods can be distinguished, topic level and style level (especially)
  - Author's style was even subconsciously affected
- Statistically significant difference between the 2 periods
- Evolution of metrics over time, combined effect of change in regime vs natural evolution of style

** Future 
*** 
- Closer look at the discriminating features, like change in topics
- Other authors, other cultures
  - How universal the phenomenon is
  - How external factors influence style


** 
  \centering \Large
  \emph{Thank you!}

** 


\section*{Additional Slides}
\subsection*{Variation of stylistic metrics over time}

\begin{figure}[h]
 
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/avg_senlen_evolution_tstat.png} 
\label{fig:subim1}
\end{subfigure}
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/avg_wlen_evolution_tstat.png}
\label{fig:subim2}
\end{subfigure}

\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/readability_evolution_tstat.png} 
\label{fig:subim1}
\end{subfigure}
\begin{subfigure}
\includegraphics[width=0.4\linewidth]{../../metrics_evolution/lexical_richness_evolution_tstat.png}
\label{fig:subim2}
\end{subfigure}
\caption{Evolution of stylistic metrics over time - t-statistic}
\label{fig:image2}
\end{figure}
