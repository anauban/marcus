import re
import sys
import csv_unicode
def clean(cuprins):
    # clean
    # footnotes
    cuprins = re.sub(".*", "", cuprins)
    # page numbers
    # TODO: does it work?
    cuprins = re.sub("^[0-9]+$", "", cuprins, re.MULTILINE)
    return cuprins

def write_cuprins_csv(path, path_csv):
    with open(path) as f:
        cuprins = f.read()
    # first 2 lines are title
    cuprins = "\n".join(filter(None, map(lambda l: l.strip(), cuprins.split("\n")))[2:])
    cuprins = clean(cuprins)

    entries = re.findall("(.*?)\.{3,} ?([0-9]+)", cuprins, re.DOTALL)
    # print "\n------------------------------\n".join(entries)

    res = open(path_csv, "a+")
    writer = csv_unicode.UnicodeWriter(res, delimiter=',', quotechar='"')
    file_index = 1
    file_path = lambda i: "RD" + nr + "_" + str(i) + ".txt"
    for idx,entry in enumerate(entries):
        if idx<len(entries)-1:
            next_page = str(int(entries[idx+1][1]) - 1)
        else:
            next_page = ''
        file_index += 1
        writer.writerow(["\t".join(entry[0].decode("utf-8").split("\n")).strip(), file_path(file_index), entry[1], next_page])
    res.close()

if __name__ == '__main__':
    nr = sys.argv[1]
    path = "books/RD%s/RD%s_cuprins.txt" % (nr, nr)
    path_csv = "books/RD%s/RD%s_cuprins.csv" % (nr, nr)

    write_cuprins_csv(path, path_csv)