„Matematica este, prin excelenţă,
o ştiinţă a infinitului”
Scânteia Tineretului – Supliment literar-artistic,
anul VIII, nr. 10 (337), sâmbătă, 12 martie 1988, p. 3
Nicolae Ţone: Cum aţi imagina un interviu ideal?
Solomon Marcus: Interviul devine ideal atunci când
îşi transgresează propria sa condiţie, de asimetrie între
parteneri; atunci când, din dialog între cineva care întreabă
şi altcineva care răspunde, devine o discuţie simetrică între
persoane cu drepturi egale de a întreba, de a răspunde, de a
se îndoi, de a argumenta şi de a contraargumenta, de
a-şi manifesta adeziunea sau reticenţa, entuziasmul sau
dezamăgirea.
Dintre interviurile „pe viu” la care am participat, cel
mai mult m-au pasionat cele acordate de marele nostru
matematician Grigore C. Moisil la televiziune, în urmă cu
aproape 20 de ani. De cele mai multe ori, era vorba de o
discuţie polemică în jurul unor probleme controversate, cu
parteneri oameni de ştiinţă, artişti, scriitori, gazetari etc. O
altă discuţie pe care am urmărit-o cu deosebit interes a fost
aceea dintre Paul Ricoeur şi A.J. Greimas, în 1984, la Cerisy
– la Salle. A fost o confruntare acerbă a hermeneuticii cu
semiotica, partenerii nemenajându-se deloc.
1069

Solomon Marcus

În ultimă instanţă, criteriul unui autentic dialog este
inevitabilitatea lui.
N.Ţ.: N-am găsit, în interviurile pe care le-aţi acordat
de-a lungul timpului, aproape nimic despre locurile dvs.
natale…
S.M.: Mi-am trăit anii şcolarităţii în oraşul Bacău, în
deceniul al patrulea. Au fost ani grei, în care condiţia materială
a părinţilor mei se deteriora, pe fondul crizei care se configura
în acea perioadă şi care avea să culmineze cu ascensiunea
fascismului şi cu războiul. Şcoala mi-a dat şi satisfacţii, şi
frustrări, dar primele pasiuni s-au configurat prin lecturi
particulare din mari poeţi ca Eminescu, Arghezi, Blaga,
Bacovia, Baudelaire, Rilke, Esenin etc. Descoperirea poeziei
a însemnat pentru mine posibilitatea de a da un sens major
vieţii; poezia, nu atât ca literatură, ci ca stare de spirit, ca
univers intelectual şi emoţional. Îmi caligrafiam cu evlavie,
în caiete speciale, poeziile care mă impresionau. Probabil că
aşa se explică faptul că memoria mea, atât de defectuoasă în
alte privinţe, este în stare să restituie şi acum sute, poate mii
de versuri pe care nu le-am mai recitit de atunci.
N.Ţ.: În 1944 luaţi bacalaureatul şi vă înscriaţi la
Facultatea de Matematică.
S.M.: Mi-am susţinut bacalaureatul în septembrie 1944,
la mai puţin de o lună după 23 August. Am plecat imediat la
Bucureşti. Mă grăbeam, fără să ştiu pentru ce. Nu ştiam încă
ce vreau să fac. În anii 1943-1944 descoperisem, este
1070

Rãni deschise

adevărat, frumuseţea geometriei, rezolvasem toate exerciţiile
din manualul şcolar al vremii. Nu-mi dădeam însă seama ce
anume se află dincolo de matematica şcolară. În octombrie
1944, profitând de întârzierea deschiderii anului universitar,
umblam prin localurile tuturor facultăţilor, urmăream toate
anunţurile şi afişele, pentru a mă edifica în ce constă alegerea
pe care o am de făcut. Cele citite în avizierul Facultăţii de
Ştiinţe – Secţia Matematică au avut asupra mea un efect
puternic. Denumirile cursurilor mă fascinau, deşi nu le prea
înţelegeam. M-am decis să merg la Matematică, rezervândumi posibilitatea de a-mi schimba ulterior decizia. Însă chiar
din primele zile de cursuri, profesorul Miron Nicolescu, în
lecţia sa de deschidere, m-a convins că matematica urma sămi rezerve mari bucurii. Alegerea mea devenea ireversibilă.
N.Ţ.: Alături de Miron Nicolescu, l-aţi avut profesor
şi pe Dan Barbilian. Ce amintiri le păstraţi?
S.M.: Profesorul Miron Nicolescu mi-a dezvăluit
frumuseţea infinitului matematic, prin lecţii care au rămas
pentru mine un termen de referinţă. De la el am înţeles, pentru
prima oară, rolul fundamental al factorului estetic în cercetarea,
descoperirea şi pedagogia matematică. I-am devenit, ulterior,
asistent, discipol şi colaborator apropiat în elaborarea tratatului
său de analiză matematică şi a manualului de analiză
matematică al catedrei pe care o conducea.
Pe Dan Barbilian l-am avut pentru prima oară profesor
în anul al doilea, începând cu toamna anului 1945. Chiar de
1071

Solomon Marcus

la prima lecţie, de deschidere, consacrată lui Evariste Galois
şi ideii de grup, am asistat la un spectacol uluitor: cunoşteam
pentru prima oară o persoană care trăieşte aproape
permanent într-o stare poetică şi numai rareori practică
limbajul uzual, preponderent denotativ. Am fost studentul
său trei ani, i-am urmărit toate cutele personalităţii şi mi-am
dat seama cât de naivă este încercarea (încurajată chiar de
Barbilian) de a-i periodiza activitatea în sensul unei alternări
între matematică şi poezie. Dar probabil că vor mai trebui
să treacă una sau două generaţii pentru ca să se poată ajunge
la o înţelegere întreagă a acestei personalităţi.
N.Ţ.: Când aţi avut revelaţia drumului, a destinului
dvs.? În aceşti ani, mai târziu?
S.M.: Dacă opţiunea pentru Analiza matematică exista
încă din primul an de facultate, aşteptarea momentului în
care itinerarul ştiinţific va putea să interfereze cu cel umanist
a fost mereu vie. În a doua jumătate a deceniului al şaselea
se conturează mai precis, atât în Est, cât şi în Vest, orientarea
spre studiul matematic al limbajelor, ca rezultat al unor
dezvoltări mai vechi în lingvistică, logică, matematică şi
inginerie. A fost momentul în care, cu toate forţele, „am
intrat în arenă”. Am avut norocul să fiu sprijinit de doi mari
savanţi, matematicianul Gr. C. Moisil şi lingvistul Al.
Rosetti, care au creat facilităţi deosebite, atât la Academie,
cât şi la Universitate, pentru promovarea lingvisticii
matematice, care tocmai se năştea. Colaboram intens, în
1072

Rãni deschise

acea perioadă, cu colegul meu lingvist, profesorul Emanuel
Vasiliu. Ţineam amândoi cursuri şi conferinţe prin care
căutam să injectăm gândirea formală în lingvistică.
N.Ţ.: Ce alţi mari matematicieni români aţi cunoscut?
S.M.: Pe Dimitrie Pompeiu l-am ascultat rostind
câteva fraze, cu prilejul vizitei la Bucureşti a unui
matematician francez. Imprima rostirii o deosebită
muzicalitate, pe care am regăsit-o în articolele sale de
matematică. Alături de clasicul Gh. Ţiţeica, barocul lui
Pompeiu oferea un efect de contrast stilistic repetat şi de
generaţia următoare de matematicieni (de exemplu, Moisil
faţă de Stoilow sau Barbilian faţă de Vrănceanu). Dar
matematicieni români de anvergură există, din fericire, şi în
rândul generaţiei mele. Chiar în aceste zile am sărbătorit-o
pe profesoara Cabiria Andreian Cazacu, iar în urmă cu
câteva luni, pe profesorul Martin Jurchescu, amândoi
discipoli de frunte ai profesorului Stoilow şi autori ai unor
rezultate ştiinţifice prestigioase.
N.Ţ.: Cu ce ochi priviţi azi spre prima dvs. carte:
Lingvistica matematica (1963)?
S.M.: Cartea de debut editorial, în urmă cu 25 de ani,
îmi prilejuieşte o frumoasă amintire, deşi a fost publicată cu
dificultate şi într-un tiraj aproape confidenţial. Există o
ezitare în faţa unei discipline incerte. La scurt timp după
apariţia ei, am aflat de la profesorul Gr.C. Moisil că la
editură sosise o scrisoare din partea unei edituri pariziene,
1073

Solomon Marcus

prin care se exprima interesul pentru o versiune franceză a
lucrării mele. Această versiune a trebuit să aştepte 3-4 ani,
deoarece ea a devenit, de fapt, o carte nouă. Introduction
mathématique à la linguistique structurale (Dunod, Paris,
1967). Concomitent, cartea fusese solicitată şi în SUA, unde
a devenit o altă carte nouă. Algebraic linquistics (Academic
Press, New York, 1967).
N.Ţ.: Poetica matematică, lucrarea dvs. de referinţă,
publicată în 1970, a întâmpinat în timpul redactării şi
publicării ei anume greutăţi? Cine a fost aproape de dvs. în
această perioadă de timp?
S.M.: Am fost cu deosebire stimulat, în preocupările
mele de poetică matematică, de profesorii Al. Rosetti, O.
Onicescu, Miron Nicolescu şi Gr.C. Moisil, de discuţiile cu
Roman Jakobson (cu prilejul Congresului Internaţional de
Lingvistică din 1967, de la Bucureşti), de corespondenţa cu
R. Jakobson (cu prilejul colaborării la volumul pe care
acesta l-a coordonat sub auspiciile UNESCO) şi de reacţia
lui Roland Barthes la articolul meu Poétique mathématique
nonprobabiliste, scris la invitaţia sa, pentru numărul special
al revistei Langages (nr. 4, Paris, 1968), pe care l-a
coordonat. Stimulatoare au fost şi discuţiile cu membrii
grupului M de la Liège (autorii Retoricii generale), cu
prilejul participării lor la Congresul Internaţional de Estetică
de la Bucureşti, cu V.V. Ivanov, Umberto Eco, Cesare Segre,
S. Levin şi cu mulţi alţii.
1074

Rãni deschise

N.Ţ.: În Capitolul 1, „Preliminarii”, aţi ales ca motto
afirmaţia lui Marston Morse: „Matematica este sora şi
auxiliara necesară a artelor şi este atinsă de nebunie şi
geniu”. Bănuiesc că aţi urmărit prin această alegere şi
prezentarea unui punct de vedere acut polemic în ceea ce
priveşte starea matematicii în epocă, condiţia ei.
S.M.: Punctul de vedere al lui Marston Morse este
destul de cuminte faţă de alte atitudini, mai radicale.
Matematica nu este numai sora artei, ci este ea însăşi artă.
Matematica este prin excelenţă o ştiinţă a infinitului şi a
antiintuitivului, chiar dacă finitul şi intuiţia au un rol
fundamental în această ştiinţă. Jaques Hadamard observa că
logica nu face decât să confirme cuceririle intuiţiei. Tocmai
aceste cuceriri sunt esenţiale şi în matematică. Poetul
german Novalis spunea că viaţa zeilor este matematica.
Tocmai prin aceste trăsături, matematica transgresează zona
raţionalului şi rutinei, hrănindu-se din intuiţii, imaginar,
sensibilitate şi reprezentări estetice, deci din nebunie şi
geniu. Chiar dacă matematicii i-ar rămâne numai studiul
numerelor naturale, ea tot infinită, inepuizabilă ar rămâne.
Cred că în special revistele literare adresate cu precădere
tinerilor trebuie să acorde o atenţie deosebită acestor
orientări, simptome ale viitorului, deoarece viitorul nu este
al celor certaţi cu matematica şi, în general, cu ştiinţa, nici
al celor indiferenţi la artă, ci al viziunilor interdisciplinare
şi integratoare, în cadrul cărora multe situaţii, considerate
1075

Solomon Marcus

până mai ieri ireconciliabile, interacţionează, iar graniţa
dintre ele se estompează.
N.Ţ.: Nu sunteţi, oare, stimate Solomon Marcus, un
matematician pe care l-a învins poezia?
S.M.: S-ar putea ca impresia dvs. să fie determinată de
modul în care aţi parcurs lucrările mele. Dar, independent
de aceasta, aşa cum rezultă şi din răspunsul meu la
întrebarea dvs. anterioară, nu văd între matematică şi poezie
o relaţie de concurenţă, de întrecere de tipul „care pe care”.
Un mare matematician din secolul trecut, K. Weirstrass,
observa că pentru a fi capabil de creaţie în matematică îţi
trebuie un suflet de poet. „Războiul” la care vă referiţi este,
deci, nu numai o formă de colaborare, ci şi o căsătorie din
dragoste. Tocmai de aceea vreau să-mi exprim stupoarea cu
care am citit, în revista Dialog de la Iaşi, că eu aş fi afirmat
că „dacă Eminescu nu ar fi fost poet, ar fi fost om de ştiinţă”.
De fapt, eu spusesem că Noica glumea, desigur, atunci când
scria: „Câteodată te întrebi – văzând cum creşte interesul lui
Eminescu pentru matematică, cu trecerea anilor – dacă o
mai bună pregătire ştiinţifică în anii tineri nu l-ar fi răpit
poeziei”.
N.Ţ.: Nichita Stănescu v-a dedicat, de altminteri, o
poezie care se intitulează, sugestiv, Matematica poetică…
S.M.: Scrisă în replică la Poetica matematică şi
inclusă ulterior în volumul său Măreţia frigului, această
poezie, ca şi altele ale lui Nichita Stănescu, reîncorporează
1076

Rãni deschise

numerele în concreteţea din care ele s-au desprins, deci
generalul în particularul cu ajutorul căruia s-a format.
Programul poetic al lui Nichita Stănescu este izomorf celui
urmărit de Constantin Noica în Scrisori despre logica lui
Hermes. Există, la fiecare dintre ei, o formidabilă sete de
individual, de „holomeri” care recuperează întregul, dau
seama despre el. Dar, aceeaşi tendinţă se manifestă şi în
matematică (prin ideea de analicitate), în fizică (prin studiul
structurilor holografice), biologie şi psihologie (de
exemplu, se vorbeşte despre structura holografică a
creierului), ştiinţele sociale (holonomia la care ne-am referit
în Timpul, Ed. Albatros, 1985).
N.Ţ.: Gorki spunea că literatura este „ştiinţa despre
om”. Privită din acest punct de vedere, al raportării la uman,
ce definiţie aţi putea propune azi matematicii?
S.M.: Cred că matematica va fi tot mai mult o ştiinţă a
modelării fenomenelor de fineţe cu ajutorul gândirii
geometrice; deci nu geometrie şi fineţe, ca la Pascal, ci fineţe
prin geometrie.
N.Ţ.: De asemenea, în Artă şi ştiinţă, în capitolul
dedicat lui Eminescu, afirmaţi: „Este de dorit să se creeze
cât mai curând posibilitatea investigării pe calculator a
creaţiei eminesciene…Un progres substanţial în cercetarea
statistică informaţională a poeziei lui Eminescu nu poate
veni până nu vom dispune de dicţionarul de frecvenţe
(subl. mea, N.Ţ.) al operei poetice a lui Eminescu ”.
1077

Solomon Marcus

S.M.: Dicţionarele de concordanţe sunt, în toate ţările
avansate, un instrument inevitabil în exegeza marilor
scriitori. Cercetările de istorie literară abundă în probleme
de paternitate, de filiaţie a unor manuscrise, de datare şi de
localizare, în care calculatorul a devenit la fel de necesar
ca automobilul, autobuzul sau tramvaiul în deplasările
cotidiene. În aceasta şi în alte activităţi similare constă
ingineria literară, căreia îi sunt dedicate numeroase
publicaţii şi congrese internaţionale.
Eminescu preconiza, în manuscrisele sale, „punerea
în ecuaţiune” a întregii cunoaşteri, de la fizică, chimie şi
astronomie, până la filologie, psihologie şi istorie,
neexcluzând din acest proces de matematizare nici poezia
(„poezia în ecuaţiune”, ms. 2292, f. 23). La peste o sută
de ani de la aceste intuiţii ale poetului naţional, procesul
de matematizare a întregii cunoaşteri umane a devenit o
realitate; nu este legitim ca poezia eminesciană să
beneficieze cu precădere de acest progres al
instrumentelor de investigaţie? Într-un moment în care
opera unor mari scriitori ca Shakespeare, Dante,
Cervantes, Goethe, Puşkin şi Victor Hugo face de multă
vreme obiectul unor analize automate exhaustive privind
datele textuale, nu a devenit oare un imperativ crearea
acelui Institut Eminescu la care se referea de curând
profesorul Zoe Dumitrescu Buşulenga? Un institut în
cadrul căruia să fie asigurată şi baza materială pentru
1078

Rãni deschise

exegeza exhaustivă şi testarea diferitelor ipoteze şi
controverse privind opera eminesciană?
N.Ţ.: „Diferenţierea extremă a disciplinelor s-a produs
abia în secolul trecut, ea nu a fost starea naturală a culturii
umane – afirmaţi într-un interviu recent. Pe cine mira
altădată un Leonardo da Vinci sau un Pascal? Sunt convins
că o nouă renaştere ne aşteaptă”. Care sunt argumentele
dvs., stimate Solomon Marcus, în acest sens?
S.M.: Transformările complexe prin care trec ştiinţa şi
societatea, în condiţiile revoluţiei informaţionale care abia
a început, vor conduce, cred, la o schimbare importantă a
conceptului de cultură, în sensul reactualizării pe un plan
superior a dezideratului renascentist şi culturii totale. Dacă
civilizaţia umană va supravieţui, depăşind momentul
dramatic actual, cultura se va desprinde de reprezentările
fragmentare care au rezultat din prima revoluţie industrială.
În ceea ce priveşte cultura românească, la argumentele
aduse voi adăuga aici şi unul de ordin istoric. O bună parte
din dezvoltarea acesteia, începând cu Dimitrie Cantemir,
trecând prin corifeii Şcolii Ardelene, apoi prin spiritele
iluministe ale secolului al XIX-lea (Gh. Lazăr, Gh. Asachi,
Ion Heliade Rădulescu, Ion Ghica), prin Mihai Eminescu,
Hasdeu, Iorga, până la Blaga, Eliade, Ion Barbu, Octav
Onicescu şi Gr.C. Moisil, s-a înscris într-o viziune totală
asupra lumii; această tradiţie trebuie dusă mai departe.

1079

