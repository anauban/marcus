SUMAR

Matematica, „industria grea” a ştiinţei
– Ştiinţă şi tehnică, anul XIX,
seria a II-a, nr. 12, 1967, p. 8, 31 ...................................35
Autobiografie ştiinţifică
– Familia, anul 3(103), nr. 5(21), mai 1967, p. 4 ..........41
Matematica, faţă cu viitorul ei – Viaţa studenţească,
anul XII, nr. 43, 20 decembrie 1967, pp. 6-7 ................52
Limbaj comun între matematician şi inginer
– Ştiinţă şi tehnică, anul XX, februarie 1968, pp. 2-4.......56
At the Interface of Mathematics and Linguistics
– Romanian Today, nr. 2(158), februarie 1968, pp. 44-45 ......65
Procesul de matematizare a ştiinţei
– Familia, anul 5(106), nr. 6(46), iunie 1969, p. 6 ........76
7

Solomon Marcus

Formarea de diplomaţi universitari
cu profil mixt: matematico-umanistic
– Gazeta Învăţământului, 19 februarie 1971 .................85
Omagiu limbii române
– Dialog cu Gabriela Melinescu, Luceafărul, anul XV,
nr. 16 (520), 15 aprilie 1972, pp. 1-6 .............................91
Pornind de la opiniile lui Zacharias Lichter
– România literară, 4 mai 1972...................................109
Ştiinţa în revistele de cultură
– România liberă, 15 mai 1973 ...................................114
Metafora este o modalitate esenţială a limbajului
ştiinţific – Dialog cu Catinca Muscan, Tribuna
României, revistă editată de Asociaţia „România”,
nr. 15, 15 iunie 1973, p. 7 ............................................119
Timpul liber şi cultura – Contemporanul,
6 iulie 1973 ..................................................................124
Ştiinţa în afara culturii? – Era socialistă,
1 noiembrie 1973 .........................................................130
Introducere în matematica modernă
– Radio TV, 12 noiembrie 1973 ...................................142
Pedagogia matematicii – Viaţa studenţească,
11 ianuarie 1977...........................................................145
8

Răni deschise

Colaborare între trei arte – Viaţa studenţească,
8 februarie 1977 ...........................................................151
Croitorul şi clienţii săi – Viaţa studenţească,
8 martie 1977 ...............................................................155
Matematica şi candoarea – Viaţa studenţească,
29 martie 1977 .............................................................159
Telefonul infinit – Viaţa studenţească,
5 aprilie 1977 ...............................................................163
Olimpiadele – Scânteia Tineretului, nr. 8,
24 noiembrie 1977 .......................................................168
Originalitatea gândirii ştiinţifice româneşti I
– Amfiteatru, decembrie 1977......................................172
Originalitatea gândirii ştiinţifice româneşti II
– Amfiteatru, nr. 12, decembrie 1977, p. 11.................185
Abordarea sistemică în ştiinţele sociale
– Era socialistă, ianuarie 1978, nr. 1,
pp. 29-30 şi 35-37 ........................................................190
Despre natura emotivă a japonezilor
– Viaţa studenţească, nr. 4 (690),
31 ianuarie 1978, p. 7 ..................................................210
Matematica, între profesie şi cultură
– Viaţa studenţească, 22 martie 1978 ..........................214
9

Solomon Marcus

Cercetarea ştiinţifică studenţească
– Viaţa studenţească, 12 aprilie 1978 ..........................218
Cunoaşterea, între ştiinţă şi filosofie
– Dezbatere realizată de revista Contemporanul,
17 noiembrie 1978 .......................................................222
Matematica de revelion – Viaţa studenţească,
10 ianuarie 1979 ..........................................................226
Matematica şi societatea – Contemporanul,
12 ianuarie 1979 ..........................................................230
Atenţie la sursele şi implicaţiile matematicii
– Contemporanul, 9 februarie 1979.............................235
Cărţile cu cel mai mare tiraj – Viaţa studenţească,
nr. 6, 7, 14, februarie 1979, p. 2...................................239
Chimia, ca metaforă – Viaţa studenţească,
23 mai 1979 .................................................................250
Trusa lui Dienes – Viaţa studenţească,
30 mai 1979 .................................................................254
Informatica în umanistică – Viaţa studenţească,
13 iunie 1979 ...............................................................258
Creaţie şi comunicare – Flacăra, 14 iunie 1979 .......262
Literatura de informare ştiinţifică
– Dezbatere realizată de revista Contemporanul,
nr. 24 (1701), 15 iunie 1979.........................................266
10

Răni deschise

Cum ne imaginăm viitorul – Contemporanul,
26 iunie 1979 ...............................................................270
Rosturile umane ale cunoaşterii
– România liberă, 4 iulie 1979 ....................................280
Ştiinţa şi creaţia artistică – Contemporanul,
nr. 39 (1716), 28 septembrie 1979, p. 2 .......................284
Prezent şi viitor în dialogul literaturii şi artei
româneşti – Anchetă, Amfiteatru, revistă literară
şi artistică editată de Uniunea Asociaţiilor
Studenţilor Comunişti din România,
octombrie 1979, nr. 10 (166), p. 4 ...............................288
Ceva în care mai încape totdeauna ceva
– Flacăra, 18 octombrie 1979 .....................................294
Pentru o istorie culturală a ştiinţei româneşti
– Familia, 1 noiembrie 1979 .......................................297
Dezvoltare prin învăţare – Flacăra,
29 noiembrie 1979 .......................................................305
The Originality of Romanian Scientific Thought
– Romanian Review, 1979 ...........................................309
Matematica trebuie învăţată ca o a doua
limbă naturală – Convingeri comuniste,
organ al Consiliului Uniunii Asociaţiilor
11

Solomon Marcus

Studenţilor Comunişti din Centrul
Universitar Bucureşti, nr. 1, 1979, p. 12 ......................332
Matematica, matematicienii şi lumea modernă
– Simpozion internaţional, Revista Astra,
nr. 1 (114), martie 1979, pp. 4-5 ..................................341
Învăţământul – dileme şi speranţe
– Contemporanul, 23 mai 1980 ...................................345
Un loz câştigător pentru fiecare
– Scânteia tineretului, 3 iulie 1980 ..............................349
Să facem totul mai bine – Flacăra, 17 iulie 1980 ....352
Potenţialul interdisciplinar al matematicii
– În volumul Interdisciplinaritatea în ştiinţa contemporană,
Editura Ştiinţifică şi Enciclopedică, 1980.....................355
Comanda socială supremă – Viaţa studenţească,
nr. 34 (806), anul XXIV, 22 octombrie 1980, p. 2 .......376
International Communication Crises
– Romanian Review, 1980, no. 12, pp. 62-72 ..............380
Viitorul, între a vrea şi a şti – Contemporanul,
nr. 2 (1731), 11 ianuarie 1980, p. 4..............................404
Forscher Wissenschaft Sozialismus
Gespräch mit Professor Solomon Marcus
– Forum 16, 1980, p. 2 ................................................412
12

Răni deschise

Dialog cu Ion Biberi: Lumea de azi – Colecţia
Reporter XX, Editura Junimea, Iaşi, 1980, 270 p. .........419
Responsabilitatea culturii faţă de viitor – Anchetă,
în volumul Viitorul omenirii în conştiinţa contemporană
(realizatorul ediţiei – Victor Isac), Editura Politică,
Bucureşti, 1980, 472 p., p. 337, 355, 364 ...................432
Artă şi codificare – Arta, revistă a Uniunii
Artiştilor Plastici din Republica Socialistă România,
nr. 11, 1980, pp. 11-13 .................................................443
O îndatorire de înaltă răspundere patriotică:
A afirma valorile ştiinţei româneşti
– Anchetă de Gheorghe Săsărman, Contemporanul,
nr. 7 (1788), 13 februarie 1981, pp. 6-7.......................457
Învaţă-te să cauţi – Revista de pedagogie,
aprilie 1981, pp. 34-35.................................................464
Tăcerile criticii – Contemporanul, nr. 25 (1806),
19 iunie 1981, p. 3 .......................................................469
O privire asupra poeziei
– Anchetă de Ioan Iacob, Scânteia Tineretului,
nr. 9963, 8 iunie 1981, p. 4 ..........................................472
Deschiderea matematicii – Contemporanul,
nr. 28 (1809), 11 iulie 1981, p. 19 (titlul publicat:
„Forţa «expansionistă» a matematicii”).......................476
13

Solomon Marcus

Procesualitatea artistică şi contextul ei cognitiv
– Arta, nr. 10-11, 1982 .................................................484
Istoria ştiinţei ca istorie a paradoxurilor
– Revista de filosofie, 1982, nr. 1, pp. 61-65 ...............490
Linguistics for Development (I) – Scientists and Peace,
1982, Revue Roumaine de Linguistique ......................504
Mathematics for Development I – Revue
Roumaine de Mathématiques Pures et Appliquées,
XXVII, 10, 1982, pp. 1101-1102 .................................509
Limbaj poetic – limbaj matematic
– Orizont, nr. 12 (734), 26 martie 1982, pp. 2-3..........514
Un mare proiect internaţional
în etapa de finalizare – Amfiteatru,
nr. 8 (200), august 1982, p. 12 .....................................519
Matematica şi problemele societăţii
– Era socialistă, nr. 13, 5 iulie 1982, pp. 22-24 ..........527
Pentru un dialog generos între toate
compartimentele culturii – Flacăra, nr. 28,
16 iulie 1982, p. 9, 14 ..................................................544
Personajul literar – un erou al timpurilor noastre
– Scânteia tineretului, nr. 10224,
12 aprilie 1982, p. 2 .....................................................551
14

Răni deschise

„Avem, cred, o mare poezie, care nu pierde
prea mult prin traducere” – Flacăra, vineri,
21 mai 1982, p. 9, 10, 11 ............................................556
Condiţia cercetătorului – Anchetă, Contemporanul,
nr. 41 (1874), 8 octombrie 1982, pp. 8-9 .....................571
Matematica – ştiinţă aristocratică? – Din cartea
lui George Arion: Interviuri, Editura Albatros, Bucureşti,
1982, 304 p. (acest al doilea volum de interviuri continuă
seria convorbirilor publicate de către autor în revista
Flacăra, în perioada 1975-1981) .................................577
„Iată eroarea: arta e una, ştiinţele...”
– Interviu realizat de Mariana Brăescu
pentru Viaţa studenţească, 1982 ..................................586
Istoria recentă a unui domeniu interdisciplinar
– Viaţa Românească, ianuarie 1983, nr. 1, pp. 17-18.....589
Gândirea binară şi funcţiile limbajului
– Contemporanul, nr. 15 (1900),
8 aprilie 1983, p. 4 .......................................................594
„O pasiune, oricât de târziu ar veni, îşi are rădăcinile
în copilărie şi adolescenţă” – Interviu realizat
de prof. Petrache Dima şi prof. Corneliu Savu,
Revista noastră, publicaţie a elevilor liceului
„Unirea” din Focşani, nr. 94-95-96,
aprilie-iunie 1983, pp. 1637-1639 şi 1654...................599
15

Solomon Marcus

O creaţie a ultimelor decenii: „artele de calculator”
– Interviu realizat de Leonard Gavriliu pentru Informaţia
Bucureştiului, nr. 9173, 7 aprilie 1983, p. 2.................608
Dimineaţa naturii – Contemporanul, nr. 20 (1905),
13 mai 1983, p. 2 .........................................................612
Mathematics for Development II – Revue Roumaine
de Mathématiques Pures et Appliquées, XXVIII,
no. 5, 1983, pp. 461-462 .............................................615
Puncte de vedere – Buletinul român de informatică.
Supliment. Probleme actuale ale pregătirii şi
perfecţionării cadrelor din informatică, Institutul
central pentru conducere şi informatică, Bucureşti,
1983, pp. 113-114. .......................................................619
Adolescentul îl prefigura pe savant
(Titlul publicat: „Savantul, om al societăţii”)
– Magazin, nr. 26 (1342), 25 iunie 1983, p. 3..............622
Rigoare şi competenţă – Contemporanul,
nr. 31 (1916), 29 iulie 1983, p. 6 .................................627
Copiii şi informatica – Contemporanul,
9 septembrie 1983, nr. 37 (1922), p. 7 .........................633
Alo! Bună ziua! Sunteţi de acord să mărturisiţi
public modelul vieţii dumneavoastră? – Flacăra,
nr. 39 (1476), 30 septembrie 1983, p. 14 .....................637
16

Răni deschise

Paradoxul şi antinomia – Contemporanul,
nr. 43 (1928), 21 octombrie 1983, p. 5 ........................640
Mathematics for Development III
– Revue Roumaine de Mathématiques Pures
et Appliquées, no. 8, 1983, pp. 785-786 ......................646
Semiotica şi matematica – Viaţa studenţească,
7 decembrie 1983, nr. 49, p. 9......................................649
Spectacolul matematicii – Ramuri, nr. 12 (234),
15 decembrie 1983, p. 10.............................................655
Caragiale, din unghi matematic – Amfiteatru,
anul XVIII, nr. 1 (217), ianuarie 1984, p. 10. ..............660
Comunicarea umană: un fapt comun şi o enigmă
– Contemporanul, nr. 6 (1943),
3 februarie 1984, pp. 6-7..............................................669
Matematica şi publicul ei – Contemporanul, nr. 8
(1945), 17 februarie 1984, p. 7 ....................................676
Ştiinţa şi profesiile viitorului – Discuţie cu
Constantin Vişan, Magazin, nr. 19 (1387),
12 mai 1984, p. 1, 8 .....................................................682
Raza de acţiune a matematicii – Convorbire cu
Constantin Vişan, Magazin, nr. 20 (1388),
19 mai 1984, p. 4 .........................................................687
17

Solomon Marcus

Exemplul tinerilor matematicieni – Răspunsuri
la întrebările lui Dan Badea, Convingeri comuniste,
revista CUASC din Centrul Universitar Bucureşti,
1984, nr. 4 (titlul publicat: „Cercetarea ştiinţifică:
o preocupare constantă a matematicienilor”) ..............693
Explorări în enigmatic – Magazin, nr. 22 (1390),
2 iunie 1984, p. 9 .........................................................698
Ştiinţă şi artă – România literară, nr. 8,
21 februarie 1985, pp. 12-13........................................701
Semiotica, azi (I) – Ramuri, nr. 2 (248),
15 februarie 1985, pp. 1-4............................................706
Semiotica, azi (II) – Ramuri, nr. 3 (249),
15 martie 1985, p. 11 ...................................................714
Primer plano, Solomon Marcus:
Ciencia y poesia – Rumania de Hoy,
no. 4 (365), 1985, pp. 37-38 ........................................722
Cultura românească în Japonia – România literară,
anul XIII, nr. 28, joi, 11 iulie 1985 ..............................735
Specificul anticipaţiei româneşti – Masă rotundă
– Almanahul Anticipaţia, august 1985, pp. 60-70 .......738
Lingvistica matematică, azi
– Ramuri, nr. 8 (254), 15 august 1985, p. 9 .................742
18

Răni deschise

Dialoguri culturale la Costineşti
– Viaţa studenţească, nr. 37 (1061),
11 septembrie 1985, p. 4...................................................752
Pentru stimularea creativităţii
– Viaţa studenţească, nr. 40 (1064),
2 octombrie 1985, p. 3 .................................................761
Învăţăm de la alţii, îi învăţăm pe alţii
– Flacăra, anul XXXIV, nr. 45 (1586),
8 noiembrie 1985, p. 9, 14 ...........................................766
De ce ştiinţa? – Luceafărul, anul XXVIII,
nr. 47 (1229), 23 noiembrie 1985, p. 1 ........................772
„Vom recupera caracterul sincretic al cunoaşterii
umane!” – Astra, anul XX, nr. 12 (171),
decembrie 1985, p. 6....................................................775
Prestigiul şi tradiţia unei şcoli: Facultatea
de Matematică – Universitatea Comunistă,
revistă a Consiliului Uniunii Asociaţiilor
Studenţilor Comunişti din Universitatea
Bucureşti, nr. 4 (60), 1985, p. 3 ...................................788
Matematica şi alegerea profesiunii – Columne,
revista Liceului Agroindustrial Slobozia, 1985, p. 37........794
Tipuri de înţelegere în cunoaşterea contemporană
– Cartea Interferenţelor, Editura Ştiinţifică
şi Enciclopedică, Bucureşti, 1985, pp. 151-159 ..........796
19

Solomon Marcus

La Colocviile revistei Studii şi cercetări
de documentare, vol. XXVIII, 1986, nr. 1-2
– Ministerul Educaţiei şi Învăţământului.
Oficiul de informare documentară pentru învăţământ .....809
Ce este holografia? – Astra, nr. 4, aprilie 1986, p. 13......831
Întrecerea celor mai buni
– Viaţa Studenţească, anul XXX, nr. 15 (1091),
miercuri, 16 aprilie 1986, p. 3.................................................841
Zile de pace – Flacăra, anul XXXV,
nr. 15 (1608), 11 aprilie 1986, p. 4 ..............................847
Primul colocviu naţional de limbaje,
logica şi lingvistica matematică
– Viaţa Studenţească, anul XXX, nr. 24 (1100),
miercuri, 18 iunie 1986, p. 2........................................851
Lectură şi repetiţie – Ramuri, nr. 8 (266),
15 august, 1986, p. 11 ..................................................854
Cărţi destin – Opinia studenţească, organ
al Consiliului U.A.S.C. din Centrul Universitar, Iaşi
nr. 3-4 (92-93), anul XIII, 1986, p. 18 .........................862
Dialog cu Solomon Marcus – Vatra, Târgu Mureş,
serie nouă (1971), anul XVI, nr. 183, p. 6,
nr. 6, iunie 1986 ...........................................................864
20

Răni deschise

„M-am considerat un supravieţuitor norocos”
– Opinia Studenţească, organ al Consiliului U.A.S.C.,
din Centrul Universitar Iaşi, nr. 1,
anul XIII, 1986, p. 5.....................................................874
Noile exigenţe ale predării matematicii
– Secera şi Ciocanul, organ al Comitetului judeţean
Argeş al P.C.R., anul XXXVI, nr. 6813,
vineri, 30 mai 1986, p. 6 ..............................................882
„Matematica este o invitaţie la efort”
– Argeş, nr. 7, iulie 1986, p. 5 ......................................885
Universitatea de vară din Sibiu
– Viaţa Studenţească, anul XXX, nr. 33 (1189),
miercuri, 20 august 1986, p. 8 .....................................889
Unde începe ştiinţa? – Viaţa Studenţească, anul XXX,
nr. 38 (1114), miercuri, 24 septembrie 1986, p. 2........895
Lectură, inducţie şi circularitate – Ramuri,
nr. 9 (267), 15 septembrie 1986, p. 12 .........................901
Cercetarea ştiinţifică în şcoală
– Ştiinţă şi Tehnică, nr. 9, 1986, p. 4............................909
La Viaţa studenţească
– Viaţa studenţească, anul XXXI, nr. 10 (1138),
miercuri, 11 martie 1987, p. 8......................................913
21

Solomon Marcus

Ştiinţa şi arta în interacţiune
– Viaţa Studenţească, anul XXXI, nr. 41 (1169),
miercuri, 14 octombrie 1987, p. 2................................918
Din nou despre teorie şi practică
– Viaţa Studenţească, anul XXXI, nr. 42 (1170),
miercuri, 21 octombrie 1987, p. 2................................923
Semiotica şi sistemul indicatorilor sociali,
câteva puncte de reper – Studii şi Cercetări
de Calcul Economic şi Cibernetică Economică,
anul XXII, 1987 ...........................................................928
Comunitatea profesională a informaticienilor
– Viaţa Studenţească, anul XXXI, nr. 43 (1171),
miercuri, 28 octombrie 1987, p. 2................................937
Invenţie şi descoperire – Revista Noastră,
publicaţie a liceului „Unirea”, Focşani, anul XIV,
nr. 131-133, octombrie-decembrie 1987......................942
Provocare la dialog (I)
– Viaţa Studenţească, anul XXXI, nr. 48 (1176),
miercuri, 2 decembrie 1987, p. 2 .................................949
Provocare la dialog (II) – Viaţa Studenţească,
anul XXXI, nr. 51 (1179), 23 decembrie 1987, p. 2 ....954
A găsi sau numai a verifica?
– Ghidul candidatului la admiterea în învăţământul
superior, nr. 30 (6), 1987, pp. 28-30............................958
22

Răni deschise

O teorie cuantică a conştiinţei
– Ateneu, nr. 12 (216), decembrie 1987, p. 5...............975
Prezenţă românească la al VII-lea
Congres internaţional de cibernetică şi sisteme
– Ştiinţă şi tehnică, nr. 12, 1987, pp. 11-12 .................980
Când suntem mai aproape sau mai departe de noi
– Mugur Alb, revista Şcolii cu clasele I-VIII nr. 19
Bacău, anul VI, nr. 3-4-5, 1985-1987, p. 17 ................989
Istoria matematicii în România
– Almanahul Cutezătorii, 1987, pp. 26-32 ..................991
Matematica, matematica... Sursă de îngrijorare
sau de mari şi sublime satisfacţii?
– Almanahul Scânteia, 1987, pp. 230-235.................1006
Niciun inventator fără calculator – Marea şansă
a omului contemporan în confruntarea cu viitorul:
Accelerarea creativităţii.
– Anchetă de Mircea Bunea şi Valeria Ichim,
Almanahul Scânteia, 1988, pp. 115-130....................1028
Teatrul şi educarea gustului public
– Anchetă de Aurelia Boriga, Teatrul,
revistă a Consiliului Culturii şi Educaţiei Socialiste,
nr. 2 (februarie), anul XXXII, 1988, pp. 16-28 .........1035
23

Solomon Marcus

Matematica, sub acţiunea informaticii
– Viaţa Studenţească, anul XXXII, nr. 2 (1182),
miercuri, 13 ianuarie 1988, p. 2 .................................1038
Experimentul în matematică
– Viaţa Studenţească, anul XXXII, nr. 3 (1183),
miercuri, 20 ianuarie 1988, p. 2 .................................1043
La joncţiunea informaticii cu matematica
– Viaţa Studenţească, anul XXXII, nr. 5 (1185),
miercuri, 3 februarie 1988, p. 2 .................................1048
De la ştiinţe speciale la cultură generală
– Contemporanul, nr. 6 (2151),
5 februarie 1988, p. 12 ...............................................1053
Informatica, de la îndemânare la ştiinţă
– Viaţa Studenţească, anul XXXII, nr. 8 (1188),
miercuri, 24 februarie 1988, p. 2 ...............................1057
Rigoare şi creativitate – Evrika, revistă editată
de Consiliul Uniunii Asociaţiilor Studenţilor
Comunişti din Centrul Universitar Bucureşti,
februarie 1988, p. 15 ..................................................1062
De la ştiinţa gata făcută la secretele ei
de fabricaţie – Viaţa Studenţească, anul XXXII,
nr. 9 (1189), miercuri, 2 martie 1988, p. 2 .................1064
24

Răni deschise

„Matematica este, prin excelenţă,
o ştiinţă a infinitului” – Scânteia Tineretului –
Supliment literar-artistic, anul VIII, nr. 10 (337),
sâmbătă, 12 martie 1988, p. 3 ....................................1069
Bricolajul ştiinţificului şi al poeticului
– Viaţa Studenţească, anul XXXII, nr 11 (1191),
miercuri, 16 martie 1988, p. 2....................................1080

25

