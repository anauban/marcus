Responsabilitatea culturii faţă de viitor
Anchetă, în volumul Viitorul omenirii în conştiinţa
contemporană (realizatorul ediţiei – Victor Isac), Editura
Politică, Bucureşti, 1980, 472 p., p. 337, 355, 364
1. Responsabilitate şi gratuitate
Victor Isac: Gratuitatea sau „nobila inutilitate”,
atribuită uneori filosofiei, revine de fapt oricărui factor de
cultură atunci când manifestă dezinteres faţă de destinul
omului şi problemele majore ale societăţii. Într-o astfel de
situaţie, acţiunile culturale pot degenera în anticultură, prin
realizări reprezentând gratuitatea, lipsa de semnificaţie şi
inautenticul în creaţie, elemente favorabile formelor
negative ale culturii.
Responsabilitatea constituie polul opus acestor
manifestări, implicând consacrare, angajare şi mesaj în
sprijinul celor mai grave probleme şi mai imperioase
obiective privind omul şi – în mod deosebit – viitorul
umanităţii. Responsabilitatea în cultură a fost şi rămâne un
apanaj al umanismului.
Un exemplu model de afirmare a responsabilităţii în
cultură, specific umanismului socialist, îl găsim în
consideraţiile: „Timp de milenii, popoarele s-au prădat,
432

Rãni deschise

s-au alungat de pe pământ şi s-au masacrat unele pe
altele... Oamenii s-au sfâşiat între ei, cu o sălbăticie ce lear fi îngrozit şi pe animale”. Drept remediu: „Cultura
trebuie să zguduie conştiinţele pentru a le trezi” (Dumitru
Popescu), ceea ce se impune mai ales în faţa problemelor
de viitor ale omenirii. Sub imperiul îngrijorărilor, temerilor
şi pericolelor ce se datoresc în mare măsură lipsei de
responsabilitate în activitatea culturală de toate genurile,
vă propun următoarele două întrebări:
Care sunt manifestările de gratuitate cele mai grave
ale domeniului căruia în mod deosebit v-aţi consacrat, cu
consecinţe negative pentru întreaga finalitate a culturii şi,
inevitabil, pentru aspiraţiile şi imperativele sociale ale
zilelor noastre?
Care sunt acţiunile de responsabilitate ale aceluiaşi
domeniu şi ce elemente de optimizare propuneţi, astfel
încât creaţiile şi activitatea domeniului dvs. să devină de
maximă eficienţă pentru viaţa socială, inclusiv pentru
pregătirea şi făurirea viitorului dorit al omenirii?
S.M.: Responsabilitatea se opune iresponsabilităţii,
în timp ce gratuitatea se opune existenţei unei finalităţi.
Atras de viaţa internă a disciplinei sale, omul de ştiinţă ar
putea, eventual, să neglijeze corelaţiile acestei discipline
cu celelalte activităţi umane şi sociale. Obiectul
preocupărilor sale ar putea atunci să se detaşeze treptat de
condiţiile care au determinat apariţia sa şi de implicaţiile
433

Solomon Marcus

sale – directe sau indirecte – devenind o lume în sine, cu
acelaşi statut pe care-l au jocurile inteligenţei, şahul, de
pildă.
Absenţa simţului de responsabilitate a omului de
ştiinţă poate să apară în condiţiile în care cercetătorul
pierde din vedere funcţia socială a ştiinţei. Fenomenele de
gratuitate despre care am vorbit mai sus pot alimenta
uneori o asemenea dezertare de la misiunea nobilă a
ştiinţei. Din păcate, mai există încă oameni de ştiinţă care
declară că nu fac politică şi care consideră timp pierdut
orice altă activitate decât aceea de cercetare. Dar în
societatea noastră socialistă, atât de profund implicată în
problemele grave ale acestui sfârşit de secol, acest tip
uman devine din ce în ce mai rar. Ca oameni de ştiinţă,
suntem esenţial interesaţi în problemele de politică a
ştiinţei, vrem să îmbunătăţim relaţiile noastre cu
beneficiarii ştiinţei, să fim cât mai receptivi la comanda
socială. Nu uităm, de asemenea, că o mare parte a
locuitorilor pământului privesc plini de speranţă spre
oamenii de ştiinţă, de la care aşteaptă soluţii la atâtea
probleme care-i frământă. Savanţii sunt pentru mulţi
oameni modele de comportare umană şi socială. Ar fi, de
aceea, de dorit ca presa, radioul, televiziunea şi celelalte
mijloace ale comunicării de masă să-şi lărgească cercul
colaboratorilor din rândul oamenilor de ştiinţă, dându-le
acestora posibilitatea de a se afla într-un dialog mai
434

Rãni deschise

eficient şi mai variat cu publicul. Editurile ar trebui să
acorde un loc mai mare cărţilor în care savanţii îşi exprimă
crezul. Cartea Paşi peste graniţă a lui Werner Heisenberg
ar putea să fie urmată de multe altele de acest fel.
2. Eticism cultural
V.I.: În mod categoric, prospectologul român M.
Maliţa, afirmând că „fără cultură, conflictele nu se pot
aplana, convieţuirea şi dialogul paşnic fiind excluse”,
atestă că rolul decisiv al culturii pentru viitorul omenirii
constă în condiţiile promovării unui climat moral.
Considerând ca adecvat epocii acest eticism cultural,
echivalent umanismului contemporan, vă întreb:
Care sunt mijloacele, metodele şi actele de creaţie şi
culturalizare, din domeniul dvs. de activitate, care ar putea
contribui în cea mai mare măsură la dezvoltarea
eticismului cultural? Care dintre valorile morale le
consideraţi cele mai pertinente epocii şi ce noi valori
morale propuneţi în sarcina dezvoltării viitoare a culturii?
Care dintre valorile culturale ale trecutului, ale culturilor
specifice unor zone geografice şi epoci istorice, merită a fi
reactualizate în sprijinul eticismului cultural şi al soluţionării
problemelor pe care societatea contemporană le ridică?
S.M.: Faptul că numeroşi locuitori ai planetei noastre
suferă încă de lipsă de hrană şi adăpost, nu au posibilitatea
435

Solomon Marcus

să-şi îngrijească sănătatea şi să-şi crească copiii, să
beneficieze de câştigurile civilizaţiei şi culturii
contemporane, creează o situaţie dramatică, pe care nu o
putem ignora în nicio discuţie despre morală. A reduce,
mai întâi, şi a lichida, apoi, inegalităţile profunde de
dezvoltare existente încă între diferitele ţări este un
imperativ pe care trebuie să-l avem în vedere în orice
strategie socială. Puţini dintre noi pot acţiona direct pentru
înlăturarea inegalităţilor despre care am vorbit mai sus.
Dar fiecare dintre noi poate dezvolta, în acest sens, o
acţiune indirectă, dar eficientă, militând pentru înlăturarea
nedreptăţilor şi pentru promovarea valorilor autentice în
sfera sa de activitate. Marile probleme ale omenirii nu se
rezolvă atât prin deciziile câtorva oameni politici, cât prin
acumularea unui mare număr de activităţi în aparenţă
derizorii, dar în fapt subsumate unui scop nobil comun şi
capabile să se închege într-o acţiune majoră a maselor de
pretutindeni. Tocmai în acest sens suntem stimulaţi de
către conducerea ţării noastre. Dincolo de ceea ce poate să
pară ţinând de o anumită conjunctură, avem datoria să
scoatem în evidenţă şi să promovăm permanenţele
spirituale ale poporului nostru, dragostea şi lupta sa pentru
independenţă şi demnitate. În particular, merită să fie cu
mai multă forţă subliniate inteligenţa şi eroismul cu care
au acţionat, în fiecare etapă de dezvoltare, oamenii noştri
de ştiinţă, pentru a crea o ştiinţă românească în stare nu
436

Rãni deschise

numai să se sincronizeze cu cercetările dezvoltate în ţările
cele mai înaintate, dar să şi iniţieze numeroase direcţii noi
de creaţie. În echilibrul dintre ceea ce am primit şi ceea ce
am dat putem găsi motivaţia unor relaţii cât mai profunde
şi necesare cu celelalte popoare ale lumii, relaţii de
colaborare de la egal la egal, spre binele întregii omeniri.
3. Comunitatea de cultură şi societatea viitorului
V.I.: Pornind de la consideraţia lui Engels că „acolo
unde nu există o comunitate de interese nu poate exista o
unitate de scopuri, ca să nu mai vorbim de o unitate de
acţiune”, putem spune că societatea contemporană
întruneşte condiţiile unei „unităţi de acţiune”, cât timp
„comunitatea de interese” este o realitate vie, iar „unitatea
de scopuri” a devenit o adevărată luptă pentru pace,
progres şi cooperare a întregului glob. Saltul calitativ de
la comunitatea de interese economice la comunitatea
socială a cooperării şi înţelegerii poate găsi în cultură cel
mai bun aliat şi cel mai fidel factor al împlinirii sale.
Oamenii de cultură au dat primele îndemnuri concrete.
Încă Socrate spunea: „eu sunt un cetăţean nu al Atenei sau
Greciei ci al lumii.” Voltaire şi Goethe se considerau cetăţeni
ai umanităţii. Mărturisiri sincere şi teze în sprijinul ideii
comunităţii sociale mondiale sunt nenumărate astăzi, din
partea oamenilor de cultură, mai ales a celor ce şi-au
437

Solomon Marcus

consacrat activitatea viitorului omenirii. Astfel, Johan
Galtung susţine ideea unui „soi de convergenţă care se
impune lumii”, John McHale consideră că „evoluăm spre o
societate planetară a viitorului”, iar Karl Jaspers concepe ca
posibilă şi mai ales necesară „comunitatea reală în care
oamenii să poată cunoaşte dragostea, prietenia, simţindu-se
cetăţeni ai lumii”.
„Declaraţia comună a grupului de experţi reuniţi de
UNESCO”, a fost semnată de zece personalităţi culturale
de pe toate meridianele pământului, întrunind toate
culturile: N. K. Sidautha, C. C. Berg, S. Buarque de
Hollanda, M. Castro Leal, L. Febvre, M. Griaude, R. P.
McKeon, Yi Chi Mei, Mostafa Amer Bey şi J. M. Romein.
Din finalul declaraţiei desprindem: „Problema înţelegerii
internaţionale este o problemă a relaţiilor dintre culturi.
Dintre aceste relaţii trebuie să apară o nouă comunitate
mondială de înţelegere şi respect mutual. Această
comunitate trebuie să ia forma unui nou umanism, în care
universalitatea va fi realizată prin recunoaşterea valorilor
comune, sub semnul diversităţii culturilor”.
Un aport concret şi util viitoarei comunităţi
mondiale a cooperării şi păcii îl realizează antropologia
culturală contemporană, care susţine ideea unităţii
posibile a culturii umane. Saltul calitativ năzuit pe plan
social este cel puţin în parte realizat în planul culturii.
Drept dovadă, de la definiţii localizate gen O. Spengler
438

Rãni deschise

– „Cultura este realizarea posibilităţilor sufleteşti de
simţire şi gândire ale unei grupări oarecare de oameni,
pe un teren oarecare” –, antropologul Richard
Carrington, urmărind evoluţia omului pe parcursul a un
milion de ani, susţine: „În mod deosebit, noi am încercat
să arătăm cum, prin anvergura civilizaţiei occidentale,
specia noastră a devenit parte a unui grup cultural
unitar”. Chiar dacă acest grup nu este încă realizat, ideea
rămâne: Specia umană poate deveni o comunitate
unitară, de ordin cultural într-o primă fază, după care
poate trece la o comunitate de ordin social.
Dacă „viitorul aparţine tuturor oamenilor” şi
„evoluăm spre o societate planetară a viitorului” (John
McHale), atunci, în interesul acestei societăţi a viitorului,
se impune să relevăm tot ceea ce cultura îi poate oferi mai
valoros. În acest scop şi ţinând seama de complexitatea
problemei, propun trei întrebări. Iată prima dintre ele:
Care dintre capodoperele scrise, sau orice alt gen de
valori ale culturii – legi ştiinţifice, opere de artă, acte de
înţelepciune, aforisme, maxime morale, principii şi reguli
sociale etc. – consideraţi că au contribuit în mod deosebit
şi încă mai sunt capabile să stimuleze principiile de
cooperare, colaborare, înţelegere, în final progresul şi
pacea între popoare?
S.M.: Într-o epocă de atât de mare instabilitate a
instituţiilor, relaţiilor şi organizaţiilor sociale, de evoluţie
439

Solomon Marcus

atât de rapidă a ideilor şi metodelor, este foarte important
să prezentăm tineretului valorile perene ale civilizaţiei şi
culturii. Tineretul va înţelege astfel că dincolo de evoluţiile
rapide, de suprafaţă, există o stabilitate a năzuinţelor
umane şi că acest sentiment de stabilitate şi durabilitate
nu-l putem obţine decât cu ajutorul istoriei. Progresul
ştiinţific, de exemplu, nu poate fi înţeles decât prin
examinarea modului în care legile, rezultatele, teoriile
cunoscute se inserează drept cazuri particulare în noile
descoperiri. Numai în acest fel fapte ca teorema lui
Pitagora, teoria evoluţionistă a lui Darwin, geometria lui
Euclid, logica lui Aristotel, principiile termodinamicii, îşi
capătă semnificaţia lor profundă. De exemplu, o lectură
contemporană a teoremei lui Pitagora ne obligă să luăm în
consideraţie versiunea ei în analiza funcţională modernă,
după cum principiul al doilea al termodinamicii apare
într-o lumină nouă prin analogia sa cu legătura dintre
entropia şi energia informaţională. Logica lui Aristotel
apare acum ca un moment al logicilor polivalente şi al
celor imprecise, în timp geometria euclidiană a făcut
posibile geometriile care stau la baza fizicii moderne. Dacă
reuşim să-i deprindem pe tineri să înţeleagă momentele
ştiinţei ca opere mişcătoare în semnificaţie, dar eterne prin
valoarea lor de cunoaştere, vom putea dezvolta acel
sentiment de încredere în progresul uman, pe care, din
nefericire, atâţia încearcă să-l tăgăduiască. Ne îndreptăm
440

Rãni deschise

acum către un nou umanism, o nouă Renaştere, o nouă
integrare a culturii omeneşti, prin dezvoltarea colaborărilor
interdisciplinare, prin estomparea graniţelor dintre
domenii. Avem astfel condiţii favorabile de a realiza o
solidaritate culturală, ştiinţifică şi artistică a popoarelor.
Pentru a mă referi la propriul meu domeniu, voi observa
că matematica, prin raza ei de acţiune practic nelimitată,
mi-a permis să intru în dialog cu economişti şi arhitecţi,
cu chimişti şi medici, cu ingineri şi artişti. Ce poate fi mai
stimulator pentru principiile de cooperare şi înţelegere
între popoare, pe care le proclamă cu atâta fermitate
conducerea societăţii noastre socialiste?
V.I.: Întrucât practica socială constituie un tezaur de
modele şi mijlocul suprem de verificare a valabilităţii
oricărui gen de comunitate, propun, în continuare,
întrebarea: Care dintre formaţiunile culturale constituie în
mod organizat, începând cu şcolile filosofice ale
Antichităţii şi continuând cu diversele genuri de asociaţii,
organizaţii, curente, grupări, mişcări etc. ale trecutului şi
prezentului istoric, consideraţi că merită reliefate ca
modele prezumtive de comunităţi culturale? În plus, care
anume dintre principiile doctrinare, normele de
organizare, regulile şi actele de conduită apreciaţi că
merită a fi reactualizate şi reluate în interesul consolidării
comunităţii culturale actuale şi al pregătirii comunităţii
sociale a lumii de mâine?
441

Solomon Marcus

S.M.: Am o deosebită simpatie pentru două mişcări
intelectuale care înnobilează istoria culturii europene.
Renaşterea şi Iluminismul. Timpul nostru le reface pe un
plan superior. Renaşterea rămâne ca un model exemplar
de raţionalism şi echilibru, de respect pentru valorile
culturale ale trecutului, în timp ce Iluminismul ne dă o
lecţie de militantism ideologic, deosebit de actual.
V.I.: Ce valori ale folclorului, ale artei şi ale culturilor
istorice, şi de asemenea, ce modele practice de acţiune şi
atitudine ale societăţilor respectivelor culturi le consideraţi
încă actuale, ca fiind necesare sau eficiente pentru
progresul comunităţii culturale şi al celei sociale în viitor?
S.M.: Marile mituri şi teme folclorice, baladele şi
basmele populare, proverbele şi ghicitorile condensează o
înţelepciune pe care nu o poate ignora nicio activitate
umană. Mutaţiile sociale rezultate din procesul de
urbanizare sunt, fără îndoială, mari, dar ele nu pot fi şterse,
ci, dimpotrivă, vor amplifica semnificaţiile unor culturi
care, chiar dacă sunt istoric încheiate, îşi pun sigiliul pe
întreaga dezvoltare viitoare a societăţii omeneşti.

442

