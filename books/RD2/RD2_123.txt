Noile exigenţe ale predării matematicii
Secera şi Ciocanul, organ al Comitetului judeţean
Argeş al P.C.R., anul XXXVI, nr. 6813,
vineri, 30 mai 1986, p. 6
Introducere din partea revistei:
Din expunerea prilejuită de „Zilele şcolii Argeşului”,
pe care prof. univ. dr. doc. Solomon Marcus o făcea în faţa
unor cadre didactice din Piteşti, despre amprenta pe care
şi-o pune asupra vieţii contemporane noua revoluţie
ştiinţifică şi tehnică, printre altele, reieşea că în viitorul
apropiat trebuie să ne aşteptăm la o dezvoltare
exponenţială a calculatoarelor, ca răspuns la necesitatea tot
mai stringentă de automatizare a unor operaţii intelectuale,
de rezolvare tot mai rapidă a unor probleme pe care le
ridică viaţa. În faţa acestei perspective şi, în general, în faţa
ansamblului de înnoiri pe care le aduce noua revoluţie
tehnico-ştiinţifică, învăţământul, evitând orice tendinţă de
inerţie, trebuie să se adapteze prompt şi profund noilor
cerinţe. El urmează să confere noi puteri inteligenţei,
gândirii umane creatoare…
După deosebit de interesanta expunere pe această temă,
din care rezultau şi unele cerinţe noi ale predării matematicii,
într-o convorbire ne-am adresat distinsului oaspete.
882

Rãni deschise

I. Popescu: Stimate tovarăşe Solomon Marcus, ca
profesor la Facultatea de Matematică de la Universitatea
Bucureşti, ce ne puteţi spune referitor la pregătirea la
matematică a studenţilor proveniţi din liceele argeşene?
S.M.: Ştiu că Piteştiul dă mulţi absolvenţi foarte buni.
Eu am să mă refer la studenţii din anul I. Odată intraţi în
facultate, aproape toţi sunt marcaţi de un şoc; chiar şi cei
intraţi cu mediile cele mai bune au mari dificultăţi.
Matematica din facultate este pentru ei parcă alta decât cea
din liceu. În ce constă diferenţa, am sesizat-o şi noi, ne-au
mărturisit-o şi ei, cu prilejul unor anchete. Obişnuiţi din
liceu cu rezolvarea intensivă de exerciţii şi probleme, în
facultate sunt surprinşi de volumul mare de teorie al acestei
ştiinţe. De fapt, în liceu nu se realizează acest echilibru
indispensabil dintre teorie şi exerciţii practice; chiar
elementele teoretice prevăzute în programă, existente în
manuale – e drept, uneori nu destul de explicit – nu sunt
abordate corespunzător.Fără lumina teoriei, nici rezolvarea
de exerciţii şi probleme nu se valorifică.
I.P.: Dar ca însuşire a unui stil de muncă…?
S.M.: Desigur, şocul de care vorbeam se datorează şi
stilului nou de muncă cerut în facultate. În liceu, obligaţiile
de pregătire ale elevului au o scadenţă apropiată, ceea
ce-i impune o anumită continuitate, în pregătirea
individuală. În facultate, scadenţa este relativ îndepărtată,
ceea ce-i face pe unii studenţi să se înşele, amânând
883

Solomon Marcus

nejustificat efortul individual de însuşire temeinică a
materiei predate la cursuri.
I.P.: …Interlocutorul nostru ne-a comunicat, de
asemenea, câteva observaţii, tot aşa de interesante,
rezultate din numeroasele sale asistenţe la lecţiile de
matematică din licee. Uneori, profesorii se grăbesc şi nu
sunt suficient de expliciţi în predarea unor noţiuni; nu
problematizează şi nu incită gândirea elevilor şi nu creează
acel climat propice în care aceştia să formuleze, ei înşişi,
întrebări, opinii proprii… Nu se urmăreşte, deci, suficient
conştientizarea învăţării, formarea unei gândiri active…
Nu se realizează totdeauna un învăţământ anticipativ…

884

