Universitatea de vară din Sibiu
Viaţa Studenţească, anul XXX, nr. 33 (1189),
miercuri, 20 august 1986, p. 8
Între 4 şi 14 august 1986 s-au desfăşurat la Sibiu
cursurile de vară ale Universităţii Cultural – Ştiinţifice
locale. Clădirea nouă, elegantă şi somptuoasă a Casei de
Cultură a Sindicatelor, situată în centrul Sibiului, a oferit
un cadru adecvat acestei manifestări de prestigiu, iniţiate
de Comitetul de Cultură şi Educaţie Socialistă al judeţului
Sibiu, Universitatea Cultural – Ştiinţifică Sibiu, Comitetul
Judeţean U.T.C. şi Casa de Cultură a Sindicatelor din
Sibiu. Prin provenienţa şi valoarea cursanţilor şi corpului
profesoral, prin importanţa şi varietatea temelor abordate,
prin nivelul ridicat al expunerilor şi dezbaterilor,
Universitatea de vară care şi-a închis recent porţile la Sibiu
rămâne ca un eveniment cultural de importanţă naţională,
care reia glorioasa tradiţie a Universităţii de vară de la
Vălenii de Munte, iniţiate de Nicolae Iorga. Tocmai de
aceea o prezentăm în cele ce urmează şi încercăm să-i
desprindem semnificaţiile.
Cursanţii, în număr de aproape 200, au provenit din
numeroase judeţe ale ţării, în special însă din Transilvania. În
schimbul unei taxe modeste, ei au putut participa la toate
889

Solomon Marcus

manifestările prevăzute în program. Foarte puţini elevi şi
pensionari (aceştia din urmă, personalităţi ale vieţii culturale
sibiene, ca profesorul de istorie Ion Albescu, autor al cărţii
Cântecul miresei, studiu şi culegere de folclor din judeţul
Sibiu; avocatul Cioran, fratele scriitorului cu acelaşi nume;
profesoara de istorie Micu, fostă directoare a unui liceu din
Sibiu), mulţi studenţi şi profesionişti cu pregătire universitară,
de la ingineri, medici şi arhitecţi la profesori, economişti şi
oameni de artă, precum şi alte profesiuni. Nu au lipsit, dintre
cursanţi, nici unele cadre didactice universitare, ca lector dr.
Lucia Fetcu, de la Institutul Politehnic Bucureşti. Aceasta, în
condiţiile unei publicităţi cu totul insuficiente în revistele
centrale. L-am întrebat pe unul dintre cursanţi, profesorul de
matematică Ştefan Fătulescu din Bucureşti, cum a aflat despre
cursurile de la Sibiu; mi-a răspuns că a citit un anunţ foarte
laconic în revista Ştiinţă şi tehnică. Dar puţine reviste au
inserat un astfel de anunţ.
Desfăşurate sub emblema Cultură şi civilizaţie
românească, cursurile s-au ţinut între orele 17 şi 22, pentru
a fi accesibile şi celor ocupaţi în producţie. Dimineţile erau
ocupate cu dialoguri între profesori şi cursanţi, vizitarea
unor obiective istorice şi culturale sibiene, o excursie la
Păltiniş, vizitarea casei memoriale Octavian Goga din
Răşinari, vernisajul unei expoziţii de artă plastică etc.
O contribuţie importantă au avut-o intelectualii
sibieni, în special cei de la Centrul de Ştiinţe Sociale
890

Rãni deschise

(Mihai Chioreanu, Livia Dumitru, Vasiliu Florica),
Institutul de învăţământ superior Sibiu (Nicolae Todericiu),
filiala Sibiu a Uniunii Artiştilor Plastici şi Complexul
muzeal Sibiu (Ioan Chişu, Mihai Racoviţan, Olimpia
Tudoran-Ciungan, Corneliu Bucur, Valer Deleanu) şi alţi
intelectuali sibieni, ca Mircea Avram, Pamfil Matei şi
Mihai Sofronie. Aceştia din urmă au susţinut, alături de
Livia Dumitru, un simpozion prilejuit de cea de-a 125-a
aniversare a ASTREI, cea mai importantă societate
culturală a românilor din Transilvania, printre ai cărei
membri s-au numărat Andrei Bârseanu, Vasile Goldiş,
Nicolae Popoviciu, Ion Raţiu, Vasile Lucaciu, Gh. Pop de
Băseşti, G. Bogdan Duică, Octavian Goga, Victor Babeş,
Aron Cotruş, Valeriu Bologa, Iuliu Haţieganu. Exemplul
ASTREI, ai cărei fruntaşi au apărat pe memorandişti în
procesul de la Cluj şi ai cărei bursieri au devenit
intelectuali de frunte ai ţării (unul dintre aceştia, istoricul
Andrei Oţetea, a donat ASTREI biblioteca sa de7.000 de
volume; biblioteca ASTREI, de 600 000 de volume, este
azi una dintre cele mai prestigioase instituţii culturale ale
Sibiului), constituie un stimulent pentru toţi cei care
activează în domeniul culturii.
Dintre invitaţii nesibieni au răspuns: Radu Bercea
(Sensibilitate românească şi artă simbolică orientală), Virgil
Cândea (Cultura românească pe meridianele lumii), Andrei
Cornea (Premise ale culturii româneşti medievale; civilizaţia
891

Solomon Marcus

bizantină), Paula Diaconescu (Limba română literară: istorie
şi cultură), Mihai Drăgănescu (Obiecte fizice şi
informaţionale), Ion Lotreanu (Umanismul şi ştiinţele.
Promovarea umanismului revoluţionar, caracteristică
esenţială a politicii partidului nostru), Mircea Maliţa
(Inteligenţa artificială; potenţial şi limite), Solomon Marcus
(Ştiinţa şi arta – forme fundamentale ale creativităţii umane),
Vasile Netea (Din tradiţiile universităţilor populare
româneşti; Românii din Transilvania şi lupta lor pentru
unitate naţională), Alexandru Surdu (Momente semnificative
din istoria logicii româneşti; Stadiul actual al editării operei
lui Eminescu), Sorin Vieru (Logica, mintea umană şi
maşinile; Opinia şi cunoaşterea – aspecte logice) şi
Alexandru Zub (Dimensiunea istoriografică a culturii
române). Este regretabil faptul că programul tipărit (care a
cunoscut două variante) s-a depărtat în mai multe puncte de
cel real; să sperăm însă că acest decalaj, datorat unei
insuficiente experienţe organizatorice, nu se va mai repeta la
următoarea ediţie a cursurilor. Poate că o explicaţie similară
o are şi faptul că literatura a fost absentă de la cursurile de la
Sibiu (dacă exceptăm un recital Eminescu susţinut de actorul
Ion Buleandră şi o seară literară de filme de anticipaţie,
în organizarea cenaclului casei de cultură a sindicatelor
din Sibiu).
Însă faptul cel mai semnificativ mi se pare a fi fost
interesul cu care cursanţii au urmărit expunerile, dialogul
892

Rãni deschise

pe care ei l-au provocat cu cei de la catedră, discuţiile
personale în care s-au angajat în afara orelor de program.
Într-un amfiteatru imens, sute de persoane urmăreau cu
atenţie concentrată pe vorbitor, luau note şi se
dezlănţuiau cu întrebări şi observaţii critice de îndată ce
expunerea se încheia. Dacă se intra în criză de timp, se
programa pentru o dimineaţă următoare o întâlnire
specială, care putea să dureze şi câteva ore, iar, dacă nici
acest timp nu se dovedea suficient, discuţia era
continuată sub forme de bileţele (extinse uneori la câteva
pagini mari) care ne urmăreau până la recepţie, la hotel.
Eram acostaţi cu întrebări pe stradă, în librării, la
restaurant. Probleme ca cele privind etnogeneza
poporului român, evoluţia limbii române, relaţiile om –
maşină, ştiinţă – artă, obiect fizic – obiect informaţional,
artă românească – artă orientală aprindeau imaginaţia
participanţilor, generând întrebări şi ipoteze dintre cele
mai îndrăzneţe. Trebuie să recunoaştem că în amfiteatrele
universitare nu întâlnim întotdeauna o atmosferă atât de
favorabilă interacţiunii dintre catedră şi sală. Lecţia
Universităţii de vară de la Sibiu este una a calităţii şi
pasiunii, a dialogului şi dragostei de cultură.
Un aspect interesant l-a constituit faptul că fiecare
dintre cei invitaţi a vizitat câte o întreprindere sau instituţie
sibiană. Aceste noi dialoguri, de astă dată, cu oameni ai
muncii şi cu teme mai apropiate de viaţa practică, au
893

Solomon Marcus

constituit o prelungire firească a Universităţii de vară, o
formidabilă propagandă pentru cultură.
Desigur, unele stângăcii, imperfecţiuni, defecţiuni
organizatorice nu au lipsit, la acest capitol putând menţiona
şi faptul aproape incredibil că, exceptând prima zi a
cursurilor, ziarul local Tribuna Sibiului nu şi-a informat
cititorii asupra desfăşurării lucrărilor Universităţii de vară.
Toate acestea nu pot umbri meritele organizatorilor, dintre
care vom menţiona aici pe Gheorghe Vasile (Comitetul
Judeţean de Cultură şi Educaţie Socialistă), Emil Stratulat
(directorul Casei de Cultură a Sindicatelor), Dorina Marian
(Comitetul Judeţean Sibiu al U.T.C), arh. Dorin Boilă (din
conducerea clubului UNIVERS 20). Tuturor le suntem
recunoscători pentru ospitalitatea manifestată, pentru
faptul că au prilejuit o manifestare de înaltă ţinută
patriotică şi intelectuală, pe care o aşteptăm, în 1987, la a
doua ei ediţie.

894

