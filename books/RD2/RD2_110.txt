„Vom recupera caracterul sincretic
al cunoaşterii umane!”
Astra, anul XX, nr. 12 (171), decembrie 1985, p. 6
Marius Vaida: Stimate tovarăşe profesor Solomon
Marcus, aş dori să cantonăm dialogul nostru pe tema
condiţiei omului secolului XX, în special a omului de
ştiinţă. Voi începe cu o afirmaţie: trăim o vreme cu o
puternică agresiune informaţională. Sunteţi de acord?
Solomon Marcus: Evident.
M.V.: Ce fel de consecinţe decurg din aceasta?
S.M.: Prima consecinţă este faptul că intrăm în ceea
ce se numeşte azi, tot mai mult, o „societate
informaţională”. Ce înseamnă acest lucru?! Iată, am să vă
dau un exemplu: încă în urmă cu trei-patru ani, statisticile
arătau că, în America de Nord, mai mult de jumătate din
forţa de muncă lucra în diverse sectoare ale prelucrării
informaţiei. Cu alte cuvinte, există tendinţa ca informaţia
să ocupe primul loc în preocuparea societăţii, înaintea
materiei şi energiei. Dacă, înainte, informaţia era aproape
eludată sau se estompa, iar atenţia era îndreptată spre
binomul materie-energie, cu toate consecinţele din secolul
al XIX-lea – marea dezvoltare a ştiinţelor naturii şi a
ingineriilor substanţialist-energetice, care a adus la crearea
775

Solomon Marcus

învăţământului politehnic clasic – acum trăim această
mişcare, în care trecem la triada materie – energie –
informaţie, cu accent tot mai mare asupra vârfului
informaţional. Societatea informaţională înseamnă deci că
cea mai mare parte a forţei de muncă este ocupată cu
primirea, prelucrarea, păstrarea, transmiterea şi studiul
informaţiei. Sigur că aceasta revoluţionează învăţământul.
O consecinţă importantă, în ceea ce priveşte matematica,
este importanţa tot mai mare a matematicii finite şi a
matematicii discrete. În învăţământul ingineresc, sub
influenţa primei revoluţii industriale, predomină
matematica continuului: studiul ecuaţiilor diferenţiale, al
ecuaţiilor cu derivate parţiale, toate comandate de ştiinţele
clasice ale naturii – fizica, chimia, biologia. Acum, studiul
informaţiei este în mare măsură legat de un proces de
cuantificare şi discretizare. Noţiunea de inginer capătă un
conţinut nou şi aş spune chiar că, odată cu această mişcare,
se produce şi un alt proces: se atenuează acea veche
discrepanţă dintre ştiinţă şi inginerie, care a condus, de
altfel, şi la organizarea diferenţiată a învăţământului pe
universităţi şi politehnici.
M.V.: Ce este nou în condiţia inginerului de azi?
S.M.: Sub aspect profesional, ingineria s-a extins
considerabil prin adăugarea acestei perspective
informaţionale la cea substanţialist-energetică. Contextul
social al inginerului este, astăzi, mult mai complex,
776

Rãni deschise

aspectele tehnologice aflându-se într-o conexiune din ce
în ce mai strânsă cu cele ştiinţifice, pe de o parte, cu cele
economice şi ecologice, pe de altă parte. Inginerului de azi
i se cere mai multă inventivitate decât celui de ieri, sub
presiunea unei comenzi sociale mai urgente. El trebuie să
manifeste tot mai multă fermitate pentru ca ideile sale să
fie efectiv folosite.
M.V.: Apropo de explozia informaţională, nu cumva,
devenind viaţa mai intensă, se lărgeşte astfel timpul
subiectiv, cu implicaţii imediate în modificarea duratei
active a vieţii?
S.M.: Problema prelungirii duratei vieţii are mai
multe aspecte. Unul este cel legat de dezvoltare; progresul
social general, progresul cunoaşterii medicale al condiţiilor
de îngrijire. Aşa cum durata medie de viaţă astăzi este mai
avansată decât cea de acum 50 de ani, aşa şi aceasta era
mai avansată decât cea din secolul trecut. Se preconizează,
astfel, o durată medie de viaţă mai mare decât cea actuală.
Dar aceasta se referă la durata cronologică. În materie de
durată psihică – durată subiectivă – se vor produce mutaţii
importante. Avem tot mai mult posibilitatea de a aduce în
universul nostru casnic, al familiei, ceea ce se întâmplă în
lume; prim mijloacele rapide de informare aducem
Universul în casă. Deci, lumea devine mult mai mică,
aproape un sat, cum au spus unii. Imediat aflăm,
participăm. Distanţele sunt practic desfiinţate şi asta
777

Solomon Marcus

măreşte extraordinar bogăţia de evenimente la care
participăm şi, evident, în felul acesta, timpul nostru
subiectiv este considerabil influenţat. Aceasta schimbă şi
natura artei. Poezia capătă o altă natură, romanul capătă o
altă natură, modul de a organiza timpul liber, relaţiile
umane, dragostea capătă şi ele o altă natură. Se spune că
există sentimente fundamentale, care sunt aceleaşi de
veacuri şi milenii – dar şi ele se schimbă sub influenţa
acestor mutaţii. Sigur că schimbările acestea sunt lente, nu
pot fi percepute de la o zi la alta.
M.V.: În lucrările dvs. dedicate timpului aderaţi la
ideea că timpul subiectiv variază după o lege logaritmică,
ori de tip „rădăcină pătrată”, faţă de cel cronologic. Oare
marile iubiri, marile revelaţii, momentele importante ale
vieţii unui individ nu sar din această lege cam monotonă,
matematic vorbind?
S.M.: Ar fi nişte momente în evenimentele deosebite
ale vieţii, de mare emoţie, de mare intensitate. De pildă,
momentul în care aflăm de moartea unei fiinţe apropiate
sau momentul în care trăim pericolul unei boli grave şi
viaţa noastră stă sub semnul întrebării sau… În sfârşit, sunt
o serie de momente care concentrează, într-o durată
cronologică relativ mică, o bogăţie de evenimente
extraordinare. Acestea ar fi momente de discontinuitate, în
sensul că se creează o disproporţie între durata cronologică
şi bogăţia de evenimente conţinute în acea durată, sau,
778

Rãni deschise

dacă vreţi, ar fi „puncte catastrofice”, în sensul teoriei lui
René Thom a catastrofelor. Cam aşa aş vedea momentele
în care se produc anumite salturi. În astfel de momente,
aceste distribuţii, ca cea logaritmică sau rădăcină pătrată,
sunt toate anulate şi se creează anumite dependenţe cu totul
deosebite, dar care sunt valabile numai pentru intervale
relativ scurte.
M.V.: Care credeţi că va fi principala achiziţie a
omului anului 2000?
S.M.: E interesant că o anchetă de tipul acesta a fost
propusă de revista Contemporanul, chiar de redactorul ei
şef, D.R. Popescu. Era formulată puţin diferit, dar cred că
tot la asta revine. Sună cam aşa: „Care este moştenirea
secolului nostru pentru secolul următor?”. Întrebarea
imediată este: din ce punct de vedere? Discutam în urmă
cu un an cu filosoful Constantin Noica şi el era de părere
că, din punctul de vedere al cunoaşterii ştiinţifice, secolul
XX nu ar fi dat ce a dat secolul XIX, că secolul nostru ar
fi net inferior. I-am amintit de teoria relativităţii, mecanica
cuantică şi descifrarea codului genetic. Mi-a replicat că
misterul eredităţii nu a dispărut. Am înţeles imediat că
aşteptăm lucruri diferite de la ştiinţă. Mi se pare că, sub
aspectul cunoaşterii ştiinţifice, secolul nostru, cel puţin în
anumite discipline care-mi sunt mie mai apropiate, a dat
mai mult decât toate celelalte la un loc. În matematică, de
exemplu, este o revoluţie extraordinară. În fizică, la fel.
779

Solomon Marcus

Toate acestea au adus o schimbare radicală în modul nostru
de a înţelege lumea, infinitul mic şi infinitul mare al
materiei şi al matematicii. Ceea ce s-a publicat în ştiinţă
în secolul nostru depăşeşte net tot ce s-a publicat anterior.
În niciun caz nu putem considera că nivelul mediu de
inteligenţă în secolul nostru e inferior secolelor anterioare.
Acum vine imediata replică pe care o dau foarte mulţi
oameni: dar sub aspect moral ce a dat secolul nostru? Sigur
că secolul XX a înregistrat câteva mari eşecuri, mă refer
în primul rând la eclipsa adusă de fascism. Mi se pare,
totuşi, că şi sub aspect moral am progresat mult faţă de
secolele anterioare. Este adevărat că prin exemple izolate
putem demonstra orice, putem găsi argumente pentru orice
teză. E adevărat că încă nu ştim cum se formează
trăsăturile morale la copil. Nu ştim nici măcar cum trebuie
predată matematica pentru ca să o iubească copilul; cu atât
mai mult, nu avem nicio metodă sigură de a educa copiii,
în aşa fel încât, ajunşi la adolescenţă, la tinereţe, să
confirme aşteptările. Suntem obsedaţi de anumite eşecuri
morale, de valul de violenţă despre care aflăm aproape în
fiecare zi. Se spune că din cauza ştiinţei am ajuns la
mijloacele de ucidere în masă şi că tot din cauza ei
civilizaţia umană a ajuns în situaţia de a se putea sinucide.
Dar progresul tehnico-ştiinţific este oricum necesar pentru
a depăşi situaţia actuală, în care nevoile umane cresc în
progresie geometrică, dar satisfacerea lor nu creşte decât
780

Rãni deschise

în progresie aritmetică; fără reducerea acestui decalaj, tot
la o sinucidere planetară – e drept, mai lentă – ajungem.
M.V.: Este ştiinţa actuală capabilă să amelioreze
calitatea vieţii tuturor locuitorilor planetei noastre?
S.M.: Răspunsul este, cred, afirmativ, cu condiţia ca
acţiunea ştiinţei să nu fie neutralizată de absenţa spiritului
de înţelegere între oameni şi între state. Aparentele eşecuri
ale ştiinţei în domeniul nuclear, îngrijorările pe care ea lea provocat în legătură cu ingineria genetică sau cu
informatica sunt, de fapt, nu eşecuri ale ştiinţei, ci ale
relaţiilor ei cu puterea. În ciuda caracterului limitat al unor
resurse ale planetei noastre, posibilităţile pe care le oferă
ştiinţa asigură pentru o lungă perioadă o adevărată stare de
prosperitate… cu condiţia ca la progresul ştiinţific să se
adauge şi progresul moral.
M.V.: Ce rol acordaţi marilor întâlniri cu
personalităţi, în devenirea unui om?
S.M.: E vorba, înţeleg, de rolul pe care îl are
contactul direct cu o personalitate. Mulţi studenţi întreabă
azi de ce mai e nevoie de cursuri şi de seminarii vorbite
dacă avem atâtea manuale, cursuri multiplicate,
litografiate, dactilografiate. Este nevoie, pentru că un
contact direct cu o personalitate umană aduce în faţa
noastră o experienţă unică ce nu poate fi suplinită prin
nicio carte. Este posibilitatea de a vedea în acţiune o
personalitate, de a o vedea cum gândeşte, cum se apropie
781

Solomon Marcus

de un subiect. Iată, l-am avut profesor pe Dan Barbilian,
care a publicat multe cursuri, tratate. Ei bine, nimic din ce
a publicat nu putea suplini prezenţa lui la catedră, unde
efectiv putea fi văzut cum elabora. Te introducea în
laboratorul său de gândire. Un text tipărit are tendinţa de
a lineariza materia, în sensul că mai toate lucrurile apar la
fel de importante sau de neimportante. Prezenţa umană
degajează esenţialul. Vezi care sunt ezitările, vezi cum
reconstituie un matematician, de pildă, anumite lucruri pe
care le-a uitat – şi le aminteşte procedând metodic de la
problema pe care vrea s-o rezolve –, cum e actualizată
memoria, toată această euristică a descoperirii matematicii
care ţi se relevă prin prezenţa directă.
M.V.: Care este motivaţia unui ins care se angajează
pe pretenţiosul drum al investigaţiei ştiinţifice?
Automulţumirea, autosatisfacţia, dorinţa de a mări suma
cunoştinţelor omenirii, ori ce anume?
S.M.: Impresia mea este că apropierea de cercetarea
ştiinţifică nu are decât a posteriori aceste motivaţii despre
care aţi vorbit. Ea este, în general, rezultatul unui imbold
lăuntric, cel care dă curiozitatea pe care o vedem la copilul
de patru ani, care tot timpul întreabă: ce înseamnă asta?,
dar asta de ce este aşa?, de ce nu este aşa? Această
curiozitate la copilul de patru ani, întreţinută cu grijă, se
metamorfozează în mod natural în creaţie ştiinţifică,
artistică, tehnologică etc. Faptul de a face cercetare este,
782

Rãni deschise

pentru o persoană de 20-25-30 ani, exact ceea ce pentru
un copil e faptul de a-şi manifesta mereu curiozitatea
pentru lucrurile din jur. Este ceva care ţine de esenţa fiinţei
umane.
M.V.: Care este atitudinea dvs. faţă de indivizii
superdotaţi?
S.M.: Din fericire, descoperim mereu astfel de
indivizi superdotaţi şi ni se pare că datoria noastră
principală faţă de aceste persoane este de a le oferi o
alegere cât mai mare. Cu alte cuvinte, de a căuta să le
semnalăm, să le aducem în atenţie tot ceea ce există
interesant în lumea noastră, în domeniul în care ei au o
anumită înclinare. Să nu trăiască peste zece ani, de pildă,
regretul că nu au ştiut la timp că există cutare domeniu,
cutare problemă.
M.V.: E o chestiune de şansă, deci?
S.M.: Evident că pentru un astfel de tânăr foarte dotat
este un lucru de multe ori decisiv faptul de a trăi într-un
context uman favorabil. Iată, vă dau un exemplu, marele
nostru geometru Gh. Vrănceanu era copilul unui ţăran
sărac dintr-un cătun din judeţul Vaslui. Norocul lui a fost
că învăţătorul i-a observat aplicaţia deosebită pentru
matematică şi s-a ocupat să-l trimită la Liceul Internat din
Iaşi, dacă-mi aduc bine aminte, şi apoi la facultate. Câţi
copii dotaţi nu s-or fi pierdut, fără să mai fi ştiut cineva de
ei!... Un noroc asemănător l-a avut şi colegul meu,
783

Solomon Marcus

profesorul Gheorghe Dincă, de la Facultatea de
Matematică din Bucureşti.
M.V.: Care credeţi că e cel mai important element din
mediul de lucru al savantului?
S.M.: În mediul de lucru al savantului intră multe
aspecte. O condiţie esenţială este o relativă linişte, o stare
relaxată, ştiinţa nu se poate face decât cu o stare de spirit
relaxată, nu împins de la spate; adică să trăieşti acel răgaz
necesar reflexiei. De aceea, pregătirea examenelor nu oferă
o stare de spirit favorabilă însuşirii ştiinţei. Ea îţi dă o stare
de precipitare, eşti terorizat de timpul pe care îl ai la
dispoziţie pentru a pregăti o anumită materie, de fapt de a nu
uita anumite lucruri. Vă mărturisesc că atunci când am
terminat facultatea mi-am zis: în sfârşit, acum mă voi putea
apuca de matematică. Mai sunt multe alte condiţii. Trebuie
să ai acces la o informaţie la zi – reviste, cărţi etc. – şi să
dispui de timpul necesar. Adevărul este că în multe cazuri
aceste condiţii ideale n-au fost îndeplinite, dar cei care ard
de dorinţa de a face cercetare, care sunt terorizaţi de anumite
probleme înving aceste situaţii. Evident, plătesc un tribut
pentru asta. În cele mai multe cazuri, se constată că oamenii
care sunt într-adevăr pasionaţi de ştiinţă, în orice condiţii fac
faţă. Un absolvent pe care de curând l-am ascultat ţinând o
comunicare la sesiunea ştiinţifică a facultăţii noastre, Vasile
Ene, a terminat facultatea în urmă cu trei ani şi acum este
profesor într-o comună din judeţul Constanţa. Vă daţi seama
784

Rãni deschise

ce bibliotecă are el la şcoala aceea, ca să se poată ţine la
curent cu cercetarea. Ei bine, în aceste condiţii, el, totuşi, cum
are un moment de răgaz, se repede la Bucureşti, vine la
bibliotecă, îşi xeroxează diferite noutăţi, ţine legătura cu
mine, îi furnizez reviste, extrase. Şi în aceste condiţii, iată,
el a reuşit deja să elaboreze mai multe articole care au fost
publicate în reviste internaţionale prestigioase. E un exemplu
care arată ce înseamnă pasiunea pentru cercetare. Vă daţi
seama câte probleme întâlneşte absolventul în primii ani de
intrare în producţie. Este problema adaptării la locul de
muncă, este, de obicei, momentul în care se căsătoreşte,
momentul în care îşi caută o locuinţă. Toate aceste probleme
vin deodată. Totuşi, când există pasiune, niciuna dintre aceste
probleme nu este o oprelişte definitivă pentru cercetare.
M.V.: Ce rol ocupă publicistica în activitatea dvs.?
Nu de alta, dar la Viaţa Studenţească serialul dvs. este un
punct forte.
S.M.: Iniţiativa acestui serial din V. S. a venit din
partea revistei şi iniţial am ezitat să accept. Nu eram sigur
că mă voi putea adapta la acest ritm săptămânal. După un
timp, mi-am dat seama că e un lucru foarte bun, căci, întradevăr, activitatea universitară, experienţa mea cu studenţii
au acumulat o serie de observaţii şi de probleme pe care
ţineam să le aduc în atenţia tinerilor cititori. Iată, sunt în
al nouălea an de colaborare cu V. S. şi cred că, în ceea ce
mă priveşte, va continua multă vreme.
785

Solomon Marcus

M.V.: În felul acesta se încheagă şi unele cărţi, nu?
S.M.: Evident, este un mod de a-ţi ritma reflecţia
pentru a putea comenta tot ce ai acumulat în câteva decenii
de viaţă ştiinţifică, universitară. Toate aceste acumulări se
convertesc într-o experienţă pe care simţi nevoia s-o
comunici. Şi de asta cred că este necesară această
publicistică.
M.V.: Nu am putea încheia interviul fără să vă întreb
dacă există un miraj, să zic aşa, al poeziei pentru
matematicieni. Uitându-ne la Dan Barbilian, la Grigore
Moisil, la Solomon Marcus…
S.M.: Există, vedeţi, aici şi anumite prejudecăţi. Unii
spun: ei, da, dar acestea sunt câteva excepţii, au fost cu
totul în istorie câţiva mari matematicieni care au fost şi
creatori în artă. Mi se pare că este vorba de o afinitate de
un caracter mai general. Între matematică şi poezie, între
ştiinţă şi artă în general, nu cred că există deosebirea
considerată în mod obişnuit, că una se ocupă de raţional şi
cealaltă de afectiv. Mi se pare că asta este o reprezentare
perimată. Şi una, şi alta au şi elemente raţionale, şi
elemente afective. Nu aici e deosebirea. Aş zice că
deosebirea este mai mult de limbaj, că e una care ţine de o
anumită dezvoltare istorică. Moisil spunea că o teoremă
este un sentiment, şi nu greşea. Acea haină raţională, pe
care o are ştiinţa când iese în lume, nu e de fapt substanţa
principală a ştiinţei, nici a matematicii deci. Toţi marii
786

Rãni deschise

matematicieni sunt de acord că matematica e făcută foarte
mult din intuiţii, din euristică, din emoţii. Modul în care
ea se sistematizează este de natură deductivă: definiţii,
axiome, teoreme, demonstraţii. Dar procesul de
descoperire matematică nu e acesta. El e dominat de multe
acte intuitive, de găsiri, de bănuieli, de conjecturi, de
tatonări. Asta este. Exact ca şi arta. Cred că numai această
evoluţie din ultimele secole ale culturii către o diferenţiere
cât mai mare a disciplinelor a dat această impresie. Dar nu
uitaţi că în Antichitate cunoaşterea umană era unică şi mie
se pare că vom reveni la o situaţie similară, evident pe un
alt nivel al spiralei. Vom recupera acest caracter sincretic
al cunoaşterii umane.
M.V.: Sunteţi un om celebru. Vă convine acest lucru?
S.M.: Cred că nu trebuie să confundăm celebritatea
cu notorietatea. Prin funcţia pe care o am, prin activitatea
pe care o desfăşor, în mod natural mi-am creat o anumită
notorietate. Sigur că noi folosim de multe ori o
terminologie şocantă. Cineva m-a pus într-o carte având
pe copertă „Români celebri”, însă trebuie să fim mult mai
prudenţi în această privinţă şi să avem răbdare, să aşteptăm
să se aşeze lucrurile şi să se decanteze valorile într-un timp
rezonabil.
M.V.: În primăvara acestui an aţi împlinit 60 de ani.
Cred că este chiar rezonabil să vă dorim La mulţi ani!
S.M.: Vă mulţumesc.
787

