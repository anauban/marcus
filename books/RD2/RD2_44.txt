Dezvoltare prin învăţare
Flacăra, 29 noiembrie 1979
O carte de numai 160 de pagini aduce între copertele
sale o analiză pătrunzătoare, de o mare densitate de
informaţie, de experienţă şi sistematizare, de previziune
lucidă şi imaginaţie tulburătoare, de spirit de răspundere
cu privire la posibilităţile pe care oamenii le au de a depăşi
dificultăţile cu care este confruntată planeta noastră. Nu
este deloc întâmplător faptul că o lucrare de o asemenea
gravă semnificaţie a fost elaborată împreună de trei echipe
provenind din trei ţări cu orânduiri sociale diferite: Statele
Unite, Maroc şi România. Cei trei autori (în ordine
alfabetică): James W. Botkin, de la Centrul Internaţional
de Studii Integrative din New York; Mahdi Elmandjra,
profesor la Universitatea din Rabat, preşedinte al
Federaţiei mondiale de studiere a viitorului, şi Mircea
Maliţa, profesor la Universitatea din Bucureşti, şi-au unit
eforturile, dând un exemplu strălucit de colaborare în
elaborarea unui adevărat manifest pentru o abordare
constructivă şi optimistă a problemelor globale actuale.
Istoria rapoartelor către Clubul de la Roma este relativ
recentă. Primul raport, Limitele creşterii, sub direcţia lui D.L.
Meadows, a apărut în 1972, având semnificaţia unui
305

Solomon Marcus

avertisment privind posibilitatea ca, în următorii o sută de
ani, dezvoltarea omenirii să fie pur şi simplu blocată, dacă
actualele tendinţe de creştere a populaţiei, industrializării şi
poluării, de diminuare a resurselor, vor continua
neschimbate. Al doilea raport, datorat lui Mesarovič şi Pestel,
Omenirea la răspântie, a apărut în 1974 şi a propus scenarii
alternative privind soluţiile social-politice ale problemelor
lumii contemporane, preconizând o restructurare calitativă a
actualelor procese de creştere cantitativă, inevitabil limitată.
S-a cristalizat astfel ideea necesităţii unei noi ordini
economice mondiale, idee care avea să stea la baza celui
de-al treilea raport către Clubul de la Roma, apărut în 1976
sub direcţia lui Jan Tinbergen şi intitulat Restructurarea
ordinii internaţionale. Acest raport a întreprins o analiză
minuţioasă a sistemului monetar internaţional, a redistribuirii
veniturilor în vederea finanţării dezvoltării, a industrializării,
comerţului şi diviziunii internaţionale a muncii, a producţiei
alimentare şi distribuţiei ei, a energiei şi resurselor minerale,
a programelor ecologice şi administrării oceanului, a
internaţionalizării întreprinderilor multinaţionale, a cercetării
ştiinţifice şi tehnologice, a reducerii înarmărilor şi a celorlalte
componente ale unei noi ordini economice internaţionale. În
diferite analize, mai ample decât aceea pe care o putem face
aici, s-a arătat meritul acestor rapoarte, dar şi limitele lor. Ne
vom referi acum la un singur aspect: imperativul atât de
actual al depăşirii stării de subdezvoltare. Toate acţiunile care
306

Rãni deschise

se desfăşoară în vederea realizării de obiective de dezvoltare
economică şi socială relevă faptul că, în condiţiile întrunirii
factorilor materiali legaţi de investiţii, resurse şi tehnologie,
cuvântul decisiv îl are în ultimă instanţă capacitatea de
învăţare a populaţiei, capacitatea ei de iniţiativă şi creaţie.
Noul raport către Clubul de la Roma atrage atenţia asupra
faptului că activitatea de învăţare de tip tradiţional este
insuficientă în raport cu nevoile actuale ale dezvoltării
sociale, dar în acelaşi timp el demonstrează că există rezerve
imense de ameliorare a proceselor de învăţare. Învăţarea de
tip tradiţional era una de adaptare, de menţinere. Masele îşi
însuşeau procedeele elaborate de o mică aristocraţie a
spiritului. Evident, totdeauna a existat un decalaj între ceea
ce trebuia învăţat şi ceea ce se învăţa de fapt. Însă acest
decalaj era numai o problemă de calitate a vieţii, nu una de
existenţă, de supravieţuire a speciei umane, aşa cum se
prezintă astăzi dilema dezvoltării. În aceste condiţii, trecerea
de la o învăţare prin repetiţie şi adaptare la una creativă, deci
anticipativă şi participativă, de elaborare a unor noi variante
pentru opţiunile noastre, se impune. Raportul analizează
modul în care învăţarea poate să facă faţă complexităţii
crescânde a problemelor, punând un accent deosebit pe
învăţarea în context social (deci, în condiţii de interacţiune
în cadrul unor grupuri sociale) şi pe modul în care diferite
grupuri sociale pot învăţa să-şi amelioreze comportamentul
prin creşterea gradului de participare a membrilor grupului
307

Solomon Marcus

la activităţile întregului grup. Învăţarea, în aceste condiţii, se
referă în mare măsură la ameliorarea relaţiilor umane, la
deprinderea şi dezvoltarea valorilor etice, estetice şi de altă
natură. După ce în primul capitol raportul rezumă decalajul
actual dintre finalitate şi realitate iar în al doilea capitol
dezvoltă un cadru conceptual pentru procesele de învăţare
inovativă, capitolul al treilea se ocupă de obstacolele care
stau în calea învăţării inovative, datorită în bună măsură
sărăciei existente încă în o mare parte a lumii, cursei
înarmărilor, discriminărilor sociale, rasiale, de sex, iar
capitolul al IV-lea discută noile perspective ale învăţării, prin
lichidarea analfabetismului, prin apropierea şcolii de viaţă,
prin deprinderea de a aborda probleme globale.
Noul raport către Clubul de la Roma preconizează o
revoluţie în învăţare şi ne convinge că omenirea are
posibilitatea de a înfăptui această revoluţie, are obligaţia de
a o înfăptui, pentru că aceasta este singura ei alternativă.
Este o satisfacţie să constatăm că ideile sale de bază sunt în
convergenţă cu politica ţării noastre de promovare a unei
atitudini participative la marile probleme ale omenirii, din
partea tuturor popoarelor, a tuturor statelor. Este în acelaşi
timp un succes al politicii noastre de colaborare constructivă
cu toate mişcările care urmăresc progresul social.

308

