Tăcerile criticii
Contemporanul, nr. 25 (1806), 19 iunie 1981, p. 3
Nu e un moment oarecare cel pe care literatura îl
trăieşte astăzi. Atât scriitorii, cât şi cititorii sunt intens
preocupaţi de rosturile literaturii. Trebuie literatura să-i dea
cititorului ce aşteaptă sau să-l surprindă? Să ofere modele
sau mai degrabă să pună întrebări? Să oglindească sau să
se constituie într-un univers autonom? Să prezinte fapte
reale sau să construiască situaţii verosimile? Luaţi
anchetele sociale privind lectura (din păcate, încă prea
puţine şi prea defectuoase) şi veţi observa această derută.
Stăruie impresia că cititorul aşteaptă azi din partea
literaturii nu numai ceea ce a aşteptat întotdeauna de la ea,
ci şi multe altele, care ar fi trebuit să-i vină de la alte
domenii de activitate intelectuală (de exemplu de la istorie)
sau pur şi simplu de la viaţă. Nici fenomenul
complementar nu poate fi eludat. Nu sunt puţini cei care
întorc spatele literaturii sau care pur şi simplu nu au
descoperit-o încă. Scriitorul simte aceste primejdii, el este
conştient că trebuie să lupte pentru a câştiga şi menţine
interesul şi dragostea cititorului. Nu-i este uşor. Exigenţele
au devenit foarte mari. Distincţia dintre autor şi operă se
estompează. Autorul, ca om, ca cetăţean, trebuie să fie şi
469

Solomon Marcus

el o operă, să se constituie drept cel mai important personaj
al operei sale, dacă vrea ca aceasta să fie credibilă.
Literatura a căpătat o gravitate pe care nu a mai avut-o
niciodată. Desigur, ar fi greu să înţelegem aceasta dacă am
rămâne numai în perimetrul literaturii. O dilemă gravă
stăruie asupra lumii în care trăim, iar literatura a devenit
pentru mulţi dintre noi, o formă de iubire şi speranţă, o
replică la valul de ură şi de violenţă care se ridică tot mai
ameninţător pe planeta noastră.
În aceste condiţii, scriitorul merită să fie înconjurat
de afecţiunea şi stima noastră. Prestigiul său înseamnă
implicit buna funcţionare a metabolismului social al
literaturii. Un rol important îl are aici critica literară. Ea
este, pentru literatură, ceea ce literatura este pentru
societate: o formă a conştiinţei sale de sine. În această
privinţă, am impresia că mulţi scriitori au un sentiment de
frustrare. Este el legitim? Greu de răspuns. Este oare critica
literară doar suma aritmetică a forţelor individuale care o
compun sau este ea o instituţie datoare să înregistreze
fenomenul literar în integralitatea sa? Nu ne asociem celor
care blamează critica literară. Credem, dimpotrivă, că
avem un mare număr de critici literari valoroşi. Dar tocmai
valoarea lor ridicată generează sentimentul de frustrare de
care vorbeam. Dacă am fi avut critici slabi, opinia lor n-ar
fi interesat prea mult. Având însă mulţi buni, scriitorii şi
cititorii aşteaptă cu înfrigurare reacţia lor. Numai că
470

Rãni deschise

această reacţie de foarte multe ori nu se produce. Putem
să-i acuzăm pe critici? Pe fiecare în parte nu (este greu
să-i ceri unui critic să comenteze mai mult decât câteva
zeci de cărţi într-un an), dar critica literară, ca instituţie,
manifestă o carenţă. S-a spus de multe ori că şi tăcerea este
un gest critic, poate cel mai negativ verdict al criticii
literare. Lucrurile nu stau chiar aşa. Tăcerea este o
atitudine foarte ambiguă. Motivaţiile ei pot fi dintre cele
mai variate. Iată o problemă importantă, care nu a fost încă
discutată. Viaţa literară ar fi mai destinsă dacă s-ar primi
un răspuns public, de la cât mai mulţi critici, la întrebări
ca: În ce măsură citiţi cărţile despre care nu scrieţi? Care
este tipologia acestor cărţi?
Cum s-ar putea totuşi împăca dreptul fiecărui critic
de a-şi alege cărţile despre care scrie, cu datoria criticii
literare de a se confrunta cu integralitatea fenomenului
literar contemporan?
Un răspuns ar trebui dat, cred, chiar în paginile
Contemporanului, revistă care are în urmă o tradiţie de 100
de ani şi ai cărei critici, începând cu Gherea şi continuând
cu Călinescu, au slujit cu pertinenţă şi înaltă pasiune cultul
adevărului şi al valorii.

471

