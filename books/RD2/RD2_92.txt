Caragiale, din unghi matematic
Amfiteatru, anul XVIII, nr. 1 (217),
ianuarie 1984, p. 10.
Cunoşteam excelenta carte dedicată de Florin
Manolescu SF-ului, îi citisem unele articole interesante prin
reviste. Aşa se face că, în momentul în care am aflat despre
noua sa carte (dedicată lui Caragiale), curiozitatea mea era
deosebit de vie. Mi-am procurat-o imediat, nerăbdător să
văd despre ce este vorba. Titlul cărţii, Caragiale şi
Caragiale, mă avertiza că autorul este decis să se despartă
de o reprezentare acreditată, pentru a ne propune un
Caragiale nou. Nou în ce sens? La această întrebare părea
să răspundă subtitlul cărţii: Jocuri cu mai multe strategii.
Înţelegem că este vorba de o aplicare a teoriei jocurilor,
ramură modernă a matematicii, creată de către John Von
Neumann în anii celui de Al Doilea Război Mondial,
stimulat de problemele de strategie pe care le comportă
orice conflict. Eram totuşi derutat: Ce să cred despre un
subtitlu atât de pleonastic? Este ca şi cum ai spune „Cărţi
cu mai mulţi cititori”. Existenţa mai multor strategii este
prevăzută chiar în definiţia jocului. Un joc cu o singură
strategie posibilă ar fi o banalitate, un joc degenerat,
jucătorilor nerămânându-le nicio posibilitate de alegere.
660

Rãni deschise

Am deschis repede cartea să văd cu ce definiţii
lucrează autorul. Spre uimirea mea, am constatat că nu sunt
amintite nici măcar definiţiile de bază din teoria jocurilor,
nu este amintită nici măcar definiţia noţiunii de joc. La
pagina 8 sunt date unele explicaţii care mizează pe
accepţiunea pe care unii termeni din teoria jocurilor o au în
limba comună. În acest stil, se continuă de-a lungul întregii
cărţi. Alteori se alunecă în afirmaţii ilariante prin alăturarea
unor lucruri fără nicio legătură între ele. Astfel, la p. 11 se
afirmă: „ În realitate, fragmentele textului numit
începuturile literare ale lui Caragiale se organizează în
jurul a două modele de referinţă, ca două mulţimi aproape
complementare, în care se repetă, pentru fiecare în parte,
un număr constant de valori”. (Pentru abordarea textelor,
ca problemă de teoria mulţimilor, V. Max Bense,
Einführung in die Informationstheoretische Asthetik,
Rowohlt, Reinbek bei Hamburg, 1969 cap. II 2 pp. 76-83).
La p. 12 ni se propune, pentru analiza debutului, un
arbore… care nu este arbore, deoarece posibilitatea
înapoierii la debut creează un ciclu incompatibil cu
structura de arbore. La p. 13, în legătură cu ofertele literare
făcute de Caragiale, ni se propune o funcţie de plăţi în care
coloana a doua nu aduce nicio informaţie nouă faţă de
prima coloană, ca să nu mai vorbim de faptul că nu se dă
nicio explicaţie privind utilizarea diferitelor semne şi
aranjamente. De altfel, paginile 12 şi 13 sunt singurele, în
661

Solomon Marcus

toată cartea, în care se încearcă să se recurgă la o utilizare
efectivă a unor noţiuni de teoria jocurilor. Vocabularul
teoriei jocurilor nu este lipsit de o anumită virtualitate
metaforică, însă sesizarea şi folosirea ei corectă sunt
condiţionate de o înţelegere denotativă corespunzătoare,
căreia autorul nu-i acordă atenţie. Aşa se face că „funcţia
de plăţi a unei epoci” despre care se vorbeşte la p. 23 sună
pe cât de frumos pe atât de obscur. La p. 262 autorul
vorbeşte despre „colaborarea dintre teoria (matematică) a
jocului şi ştiinţele sociale”, expresia „teoria jocului”
revenind mereu (p. 262, p. 263, p. 264, p. 360 etc.). Trecând
peste nejustificata punere între paranteze a matematicii
(teoria jocurilor este o teorie matematică în toată puterea
cuvântului), folosirea singularului în loc de pluralul
jocurilor îl trădează pe autor de a nu fi avut curiozitatea să
arunce măcar o privire în bogata literatură românească
dedicată acestei teorii. Expresii ca „Spieltheorie” sau
„Game Theory” nu trebuie să ne înşele: structura germanei
şi englezei este diferită de a românei şi francezei, unde se
foloseşte exclusiv pluralul, tot aşa cum se spune „Teoria
probabilităţilor” şi „Teoria sistemelor”, deşi în engleză
avem „Probability Theory” şi „System Theory”. Dar chiar
în engleză se spune „Theory of Games”.
Este adevărat că teoria jocurilor nu a fost încă aplicată
la Caragiale (deşi probleme de strategie în „O scrisoare
pierdută” au fost studiate, dar cu alte metode matematice).
662

Rãni deschise

Poate că chiar Florin Manolescu va fi cel care o va face.
Unele intuiţii care apar în capitolul 15 al cărţii s-ar putea
să se dovedească inspirate, dar deocamdată ele rămân în
stare crudă, autorul nesupunându-şi niciuna dintre ele unui
test de confruntare cu teoria jocurilor ca atare. Referinţe la
autori ca J. Levy şi I. Lotman sunt insuficient de
semnificative pentru aplicarea teoriei jocurilor în literatură.
Desigur, o viziune strategică a literaturii poate fi dezvoltată
şi independent de ceea ce se numeşte azi teoria jocurilor;
este ceea ce se întâmplă de fapt în multe dintre
consideraţiile autorului. Dar teoria jocurilor a fost folosită
în studiul literaturii (şi, cu deosebire, al teatrului).
Cercetători români ca Pia Teodorescu Brînzeu, Mihai Dinu,
Mariana Steriadi, I. Lalu şi alţii (a se vedea, de exemplu,
numărul dublu din 1977 pe care revista internaţională
Poetics de la Amsterdam l-a dedicat metodelor matematice
în studiul teatrului) au făcut-o cu strălucire. Tot în 1977,
Elizabeth Brussa a publicat importantul studiu The Game
of literature and some literary games (în revista New
Literary History, vol. 9, p. 153-172) continuat, recent, de
Linda A. Davey. Din păcate, informaţia lui Florin
Manolescu în această privinţă a rămas în urmă cu vreo 14
ani iar, în ceea ce priveşte bibliografia pe care o foloseşte,
ea este mai degrabă întâmplătoare, deoarece, dacă este să
judecăm după relevanţa pe care folosirea teoriei jocurilor
în ştiinţele sociale o poate avea pentru literatură, mult mai
663

Solomon Marcus

importante decât lucrările menţionate de Fl. M. sunt, de
exemplu, cărţile lui Anatol Rapoport (cum ar fi Fights,
games and debates, Univ. of Michigan Press, Ann Arbor,
1960) şi, mai recent, cele ale lui Erving Goffman.
Din fericire, aceste inadvertenţe nu afectează
substanţa cărţii lui Florin Manolescu, substanţă însă care
este alta decât aceea sugerată de nefericitul subtitlu al
cărţii, subtitlu care revine în carte, ca titlu al capitolului
15. Noutatea acestui Caragiale al lui Florin Manolescu stă
într-o viziune strategică intuitivă, a cărei joncţiune cu
teoria matematică a jocurilor rămâne încă palidă. Deficitul,
din punct de vedere literar, constă în faptul că tipologia
strategică pe care o propune autorul rămâne o simplă
ipoteză, situaţiile de joc fiind numai schiţate, nu şi urmărite
în desfăşurarea lor efectivă. Rămâne de văzut câte şi care
dintre ele confirmă intuiţiile autorului. Căutările sugerate
sunt în orice caz interesante.
Tot în legătură cu folosirea matematicii în studiul lui
Caragiale, dar într-o cu totul altă ordine de idei, ne vom
referi în continuare la articolul „Leibniz la tarabă”, din
cartea lui Alexandru George Simple întâmplări în gând şi
spaţii (carte publicată în 1982, la Editura Cartea
Românească). Este vorba de una dintre cele mai tipice şi
mai frecvente reacţii faţă de folosirea, în genere, a unui
aparat matematic. Remarcabilul eseist reproşează
cercetărilor matematice relative la Caragiale faptul că, la
664

Rãni deschise

capătul unor calcule laborioase, nu fac decât să regăsească
observaţii la care orice spectator al teatrului lui Caragiale
ajunge folosind exclusiv bunul său simţ. Alexandru George
concretizează, pretinzând că un autor de astfel de cercetări
„calculând şi iar calculând, a ajuns la concluzia esenţială
cum că personajele principale din Scrisoarea pierdută de
Caragiale sunt… Fănică Tipătescu şi Coana Joiţica – o
descoperire pe care orice inteligenţă normală o poate face
prin simpla vizionare a piesei, eventual chiar la vârsta când
nu a învăţat încă să rezolve o ecuaţie de gradul întâi”. Să
admitem, pentru moment, că demonstraţia matematică
persiflată de Al. George ar conduce într-adevăr la
constatarea că Tipătescu şi Zoe sunt personajele principale
ale piesei lui Caragiale. Ar fi aceasta o banalitate? Un
adevăr intuitiv, la îndemâna oricui? N-ar fi, intuitiv,
Trahanache, Caţavencu (şi poate şi alţii) tot atât de
îndreptăţiţi ca şi Tipătescu şi Zoe de a fi consideraţi
personajele principale? Mă-ndoiesc că „inteligenţa
normală” i-ar da dreptate lui Al. George. Am fi mai
degrabă tentaţi să credem că această inteligenţă normală,
dacă i s-ar cere să ierarhizeze personajele Scrisorii
pierdute, ar simţi nevoia să i se precizeze din ce punct de
vedere anume trebuie s-o facă.
Dar Al. George este departe de a reproduce faptele în
mod corect, deşi, fără îndoială, incorectitudinea sa este
involuntară. Pentru că nu putem reconstitui emisiunea de
665

Solomon Marcus

televiziune din urmă cu vreo opt ani, la care se pretinde că
s-au făcut afirmaţiile incriminate, să deschidem Poetica
matematică. Chiar de la începutul capitolului Metode
matematice în studiul teatrului, în paragraful intitulat Un
spectator original se explică problema studiată; aceea a
modului în care structura scenică a piesei, bazată exclusiv pe
configuraţiile de personaje asociate diferitelor scene, este
capabilă să furnizeze informaţii sau măcar sugestii despre
conflictele şi strategiile dramatice pe care de obicei le
sesizăm cu ajutorul dialogului dintre personaje, a decorului,
costumaţiei şi celorlalte elemente de spectacol. Este deci clar
că, prin spectacolul său „normal”, Al. G. se situează într-o
cu totul altă ipoteză decât a noastră. Concluziile la care
ajungem arată că o bună parte din informaţiile pe care le
desprindem din întreaga complexitate a spectacolului teatral
pot fi obţinute pe baza exclusivă a structurii scenice. Dar nu
este vorba numai de atât. Sunt definite numeroase tipuri de
relaţii între personaje, asociate cu diferite puncte de vedere
care pot fi adoptate în considerarea acestora. Fiecare punct
de vedere conduce la o anumită ierarhie a personajelor.
Astfel, din punctul de vedere al gradului de confruntare pe
care un personaj îl manifestă cu celelalte personaje, pe primul
loc se află Trahanache (pp. 271 şi 275). Cuplul
Zoe-Tipătescu se detaşează prin valoarea cea mai ridicată a
parametrului numit „legătura dintre două personaje” (p. 279).
Dacă, urmându-l pe Mihai Dinu, la structura scenică se
666

Rãni deschise

adaugă distincţia dintre cele două planuri ale conflictului (cel
al înfruntării politice şi cel al relativei indiferenţe faţă de
această înfruntare), atunci, considerând un personaj cu atât
mai important cu cât participă mai intens la ambele planuri
ale conflictului, cel mai important personaj se dovedeşte a fi
Cetăţeanul turmentat (pp. 281-283).
Am reprodus aceste câteva rezultate, pentru ca
cititorul să poată compara nuanţele operate într-o analiză
matematică şi simplismul afirmaţiei pe care Al. G. o
atribuie acestei analize. Desigur, cine are prejudecata
incompatibilităţii dintre spiritul literar şi gândirea ştiinţifică
poate decreta toate aceste lucruri irelevante. Putem decreta
inutil şi termometrul, deoarece şi fără el operăm distincţia
dintre fierbinte, cald şi rece. Problemă de nuanţe. Cine
urmăreşte orientarea internaţională a cercetării literare, ştie
cât de puternic este interesul pentru nuanţe de tipul celor
relevate mai sus; iar ecoul internaţional al rezultatelor
obţinute de şcoala românească de teatrologie matematică
nu pare nici el să-i dea dreptate lui Al. G.
Ar fi însă nedrept să omitem, în încheiere, un lucru
esenţial. Întreaga carte a lui Alexandru George (ca, de
altfel, şi cărţile sale anterioare) se citeşte cu o adevărată
delectare intelectuală.
Tot relativ la Caragiale din unghi matematic să mai
facem o observaţie. La Editura Univers a apărut de curând
cartea (nu lipsită de unele merite) lui Radu Bagdasar
667

Solomon Marcus

Critică şi cibernetică, unde, pe parcursul a 20 de pagini
(pp. 120-139), este reprodus un model matematic propus
de Mihai Dinu în 1963 pentru strategia personajelor în O
scrisoare pierdută. Acelaşi model, cu mai puţine detalii,
fusese însă reprodus şi în lucrarea noastră din 1970 Poetica
matematică (pp. 277-287), numeroase alte referiri la
acelaşi model fiind făcute şi ulterior. Între timp, s-au
acumulat numeroase metode şi rezultate noi (unele
datorate tot lui Mihai Dinu), care le aprofundează pe cele
din anii ’70, privind strategia matematică a personajelor
dramatice şi structura conflictului dramatic, după cum
s-au exprimat şi unele contestaţii. Era oare natural ca,
într-o carte publicată la sfârşitul lui 1983, toate acestea să
fie ignorate, reproducându-se în schimb o analiză
bine-cunoscută celor ce se interesează de aceste chestiuni?

668

