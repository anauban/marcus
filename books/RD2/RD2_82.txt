Mathematics for Development II
Revue Roumaine de Mathématiques Pures
et Appliquées, XXVIII, no. 5, 1983, pp. 461-462
The second part of the monograph [4] is mainly
devoted to social indicators, a basic problem of the
contemporary development.
Dan Romalo’s Ethical base of a desirable social
dynamics aims at identifying a most general social
indicator, capable to evaluate the human value of any
social process, and also to determine the set of universal
principles capable to ensure, on a normative level, human
individual equity in social life. The author tries to
demonstrate that, on the bases of only two principles, the
ethic-rational desirable evolution of a human society can
be determined at the level of an abstract model. The study
is presented in an informal style, but it also includes a
formalized component. The last part of the investigation
is concerned with a conceptual analysis aimed to
determine the theoretical background by means of which
a normative control of human development, following an
optimal social evolution, can be realised. The model is
inspired by ideas coming from theoretical physics and the
variational calculus.
615

Solomon Marcus

Gheorghe Păun’s research, Restrictions concerning
the aggregation of social indicators, is dealing with a
crucial problem related to the possibility of aggregation of
some individual indicators into a social one. Such
operations of aggregation seem to be unavoidable, in view
of proliferation of more and more individual indicators. In
order to be able to cope with them, it is necessary to reduce
their number either by selection or by aggregation. Since,
in many cases no convenient selection can be made,
aggregation remains the only solution. The requirements
of such operations of aggregation are of the following
type: some properties of the individual indicators should
be transferred to the indicator obtained by aggregation.
The author choses three conditions required to be fulfiled
by operation of aggregation. These conditions could be
formulated in a rigorous way, but we prefer here to be
intuitive rather than rigorous. The first condition requires
that aggregation should be sensitive; this means that to
modifications of an individual positive indicator
correspond modifications of the same sign of the
aggregated indicator, whereas if the individual indicator is
negative, then the modifications of the aggregated
indicator are of opposite sign. The second requirement is
to have an anticatastrophic aggregation; this means that for
small modifications of the individual indicators we get
small modifications of the aggregated indicator. So, no
616

Rãni deschise

catastrophe is possible. The third requirement is to have a
noncompensatory aggregation. In other words, a low value
of one of the indicators which are aggregated cannot be
compensated by a high value of another indicator (this is
a natural requirement; one could not accept, for instance,
to compensate lack of freedom by a larger quantity of
food). The key result obtained by Gh. Păun is a theorem
which shows that there is no aggregated indicator
simultaneously
sensitive,
anticatastrophic
and
noncompensatory. In other words, each aggregated
indicator must violate at least one of these three natural
conditions. This theorem recalls another one, of a similar
type, the famous result obtained by K. J. Arrow [1],
asserting the impossibility to aggregate individual
decisions in a social one, if we want to fulfil some intuitive
requirements. In fact, this result was, to some extent,
anticipated by the Condorcet paradox, exploiting the non
transitivity of the relation „the majority prefers a to b”.
In the second part of the study by Gh. Păun, a
contradiction principle is formulated and exemplified. This
principle says that in each reasonably complete system of
indicators there is at least a pair of indicators which
evolves contradictorily. The illustrations are from the
critical group-size theory, originated by Friedman [2] and
from the hierarchically structured groups theory.

617

Solomon Marcus

References
1. K. J. Arrow, Social Choice and Individual Value.
John Wiley, New York, 1963.
2. Y. Friedman, About critical group-size. Goals,
Processes and Indicators of Development Project, United
Nations University, Tokyo, 1978.
3. S. Marcus, Mathematics for Development (I).
Rev. Roum. Math. Pures et Appl. XXVII, 10, (1982),
1101-1102.
4. S. Marcus (coordinator), Mathematical Methods
in the Development Problematique (in Romanian), Editura
Academiei, Bucure;ti, 1982.

618

