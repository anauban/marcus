Ştiinţă şi artă
România literară, nr. 8, 21 februarie 1985, pp. 12-13
Redacţia: Sub auspiciile Uniunii Artiştilor Plastici,
Uniunii Arhitecţilor, Societăţii Profesorilor de Muzică şi
Desen, Institutului de Arhitectură „Ion Mincu”, în sălile
institutului s-a deschis expoziţia Ştiinţă şi Artă – teme,
studii, cercetări, în concepţia pictorului şi profesorului
Ştefan Sevastre. După cum se precizează în prefaţa
catalogului, „Expoziţia îşi propune să pună în lumină
contribuţia artelor plastice, alături de disciplinele umaniste
şi de cele ştiinţifico-tehnice, la realizarea educaţiei
complete, pentru dezvoltarea plenară a capacităţilor
umane.
S-au ales, din activitatea unora dintre foştii elevi ai
Liceului de Arte Plastice „Nicolae Tonitza” din Bucureşti,
care au devenit ingineri, medici, arhitecţi, critici de artă,
pictori, sculptori, designeri, mărturii ale extinderii
posibilităţilor lor de creaţie, de exprimare şi de
comunicare, marcate de educaţia artistică primită în şcoală
şi utilizate în exercitarea diferitelor lor profesiuni.
Concepţia expoziţiei vizează omul realizat în
totalitatea fiinţei lui, „Omul integral”. Fiecare profesiune
poate deveni o şansă întru această realizare, dacă în cadrul
701

Solomon Marcus

ei te implici total şi dacă în acest sens ai fost şi eşti pregătit
încă din perioada formării personalităţii tale, când, alături
de educarea mentalului, ai parcurs în egală măsură şi
etapele practice, în educarea senzorialităţii şi afectivităţii,
la nivelul unor trăiri spirituale.”
În cadrul acestei valoroase manifestări, a avut loc
simpozionul pe tema: „Vizualitatea, factor formativ al
creativităţii”, la care au participat: Răzvan Theodorescu,
Al. Paleologu, Aurel Stroe, Octav Grigorescu, Solomon
Marcus, Dan Grigorescu, Ştefan Berceanu, Vasile Drăguţ,
Paul Caravia, Octavian Barbosa şi Adina Nanu.
S-au ridicat în cadrul simpozionului probleme de
prim ordin în orizontul culturii contemporane şi al
îndatoririlor educative ale societăţii noastre. Publicăm o
sinteză a punctelor de vedere exprimate cu acest prilej.
Solomon Marcus – Provocarea vizualului
Voi pleca de la exemplul evocat aici de maestrul Octav
Grigorescu, privind pe acea studentă care nu putea picta
pădurea pentru că nu o auzea. Această natură sinestezică a
artei mi se pare esenţială. Artele vizuale nu pot fi opuse celor
temporale. A trece în vizual timpul ciclic (profan sau mitic),
clipa, fugitivul, timpul istoric, amintirea şi aşteptarea, iată
una dintre marile provocări ale vizualului, pe care m-a
bucurat s-o întâlnesc într-o meditaţie a Wandei Mihuleac, în
702

Rãni deschise

expoziţia de faţă. Din punctul de vedere al capacităţii de a
intra în relaţii sinestezice, vizualul ocupă locul întâi într-o
ierarhie în care pe ultimul loc se află tactilul. Toate tipurile
de senzaţii şi, în mod corespunzător, toate artele au nevoie
de reprezentări vizuale (iar acestea, la rândul lor, recurg la
limbaj, vorbit sau scris). Gândirea abstractă, speculativă, a
matematicianului sau a filosofului, de exemplu, recurge de
multe ori la puncte şi linii de tot felul, care pentru ceilalţi
pot să pară haotice. Manuscrisele lui Eminescu, caietele lui
Paul Valéry abundă în astfel de peregrinări vizuale.
Ideea expoziţiei, de a urmări modul în care formaţia
artistică în cadrul Liceului de Arte Plastice „N. Tonitza” a
marcat evoluţia absolvenţilor săi, prezintă un mare interes
şi nu ştiu s-o mai fi întâlnit. Tema Ştiinţă şi Artă, pe care
expoziţia o propune, se explică probabil prin faptul că o
bună parte dintre absolvenţi s-au îndreptat spre profesiuni
ştiinţifice (medic, inginer, matematician etc.). Cred totuşi
că distincţia ştiinţă-artă nu este atât de categorică pe cât
este ea prezentată de obicei. Ea ţine mai degrabă de
consideraţiile de metodă decât de natura lucrurilor. În orice
act de cunoaştere şi creaţie există şi artă şi ştiinţă. Octavian
Barbosa are dreptate să sublinieze faptul că un om de
ştiinţă trebuie să gândească şi artistic. În matematică, de
altfel, în numeroase domenii, există perioade în care
criteriul principal de evaluare a unei teorii, de alegere a
problemelor şi de apreciere a evoluţiei generale este cel
703

Solomon Marcus

estetic. Despre rolul ştiinţei în artă, de exemplu, despre ce
a însemnat pentru artele vizuale descoperirea legilor
matematice ale perspectivei, nu cred că mai este nevoie să
vorbim. Aş sublinia totuşi că, dacă în mai multe opere de
artă a fost identificată o anumită structură matematică,
atunci este interesant ca acea structură să fie degajată şi
folosită deliberat în elaborarea altor opere de artă. Acest
tip de experiment este necesar.
Destinul ştiinţei şi al artei este atât de unitar, încât
orice deteriorare a educaţiei artistice este un simptom al
unei deteriorări corespunzătoare a educaţiei ştiinţifice şi
reciproc. Cultura contemporană a devenit atât de organic
legată, încât de oriunde porneşti, de la orice fapt
fundamental de cultură, poţi ajunge oriunde. O discuţie
despre logaritm, de exemplu, evoluează firesc spre o
discuţie despre fizică şi chimie, despre biologie şi
medicină, despre calculatoare şi psihologie, despre
literatură şi artă, pentru a se transforma într-o discuţie
despre problemele globale ale omenirii.
În a sa Istorie a artei, E.H. Gombrich afirmă că arta vrea
nu numai să ţină pasul cu ştiinţa şi tehnologia, ci, de
asemenea, să găsească o cale de a scăpa de aceşti monştri.
Această atitudine o întâlnim la mulţi artişti care îşi exprimă
îngrijorarea faţă de procesul de mecanizare, automatizare şi
standardizare promovat de noua revoluţie tehnologică, proces
care ar fi incompatibil cu starea de spirit pe care o presupune
704

Rãni deschise

arta. Dacă însă acest proces este o consecinţă firească a
dezvoltării nevoilor sociale (şi, într-o mare măsură, credem
că el este), nevoi care cresc în progresie geometrică şi sunt
satisfăcute deocamdată numai în progresie aritmetică, atunci
o artă care nu se poate reoerienta faţă de noua realitate nu mai
este o artă a lui aici şi acum, adică a lumii în care trăim, ci o
artă a unei lumi ipotetice, de laborator. Calculatorul nu este o
ameninţare la adresa artei, ci o provocare pe care, acceptândo, arta îşi face într-adevăr viaţa mai grea, dar chiar prin aceasta
mai interesantă şi mai imprevizibilă.

705

