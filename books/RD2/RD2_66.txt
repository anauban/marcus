Linguistics for Development (I)
Scientists and Peace, 1982, Revue Roumaine de
Linguistique
Five years ago, the United Nations University
(Tokyo) initiated an international project called GOALS,
PROCESSES AND INDICATORS OF DEVELOPMENT
(GPID). Teams from about 30 countries were invited to
collaborate at this project, whose aim was to investigate,
within an interdisciplinary framework, the global problems
of the world today. Paying the main attention to the
problems of the third world countries, the project tried to
bring a new perspective in the field of human development
and to offer not only to scientists, but also to decisionmakers and to common people a better understanding of
our world and some alternative scenarios in solving the
future problématique.
The tradition is to see social development mainly
as economic growth, by neglecting or underestimating
the other dimensions of the social life. The originality
of the present project was from the beginning to focus
attention on a human-centered social development. On
the one hand, the human person is considered the basic
entity of social development, its basic goal; on the other
504

Rãni deschise

hand, global ploblems related to food, energy, ecology,
war and new international order are approached in a
unitary way, by taking into consideration all
intermediary levels, from the local communities to
national states and groups of states. Human development
is placed at the intersection of three spaces: the natural,
the habital, and the social space.
In a world threatened by a nuclear war, a
coordination of efforts toward an international
collaboration among scientists of very different
backgrounds and nationalities needs to be developed more
and more. The project GPID was organized in several
subprojects: Human Needs; Concepts and Theories of
Development; Alternative Ways of Life; Visions of
Desirable Societies; Visions of Desirable Worlds;
Indicators of Territorial Systems; Indicators of
Nonterritorial Systems; Processes of Militarization;
Expansion and Exploitation; Liberation and Autonomy;
Energy; Ecology; United Nations System; Human Rights,
etc. Some subprojects involve specially various problems
of communication and language: Networks; Dialogue;
Semiotics and Mathematics; Forms of Presentation.
As one especially involved in all the subprojects
related to communications and language (I was the
coordinator of the subprojects „Dialogue” and „Semiotics
and Mathematics” and of the Romanian team participating
505

Solomon Marcus

in the GPID project), I would like to make some remarks
about the way in which Linguistics was involved in this
project, which finishes at the end of the year 1982.
Dialogue was placed from the beginning at the heart
of the project. This assertion has to be understood at least
in three ways: (1) Dialogue as a way of collaboration
between various participants in the project (working
groups, network meetings etc.); (2) Dialogue with very
heterogeneous people (peasants, workers, decision-makers
etc.) about development problématique, in order to know
better and from different stand-points the development
problems; (3) A posteriori dialogues, in order to make
people aware of the results of investigation and to
implement some of these results.
Good dialogues are conceived symmetrically. They
are neither of a Socratic type, nor of a teacher-student
type. The Romanian team has organized a lot of such
dialogues, in order to get insiders’ and outsiders’ images
on GPID problématique and to check how do the layman
perceive some key topics of the GPID project. Thirty-five
basic terms related to development were proposed to 110
subjects; these subjects were asked to define the proposed
terms and to indicate their opposites. The list of terms is:
aggression, alienation, altruism, aspiration, autonomy,
belonginess, coevolution, coexistence, contradiction,
conflict, conviviality, cooperation, debate, democratic
506

Rãni deschise

relationship, development, dialogue, freedom needs,
fragmentalism, human development, identity needs,
maturity (social and individual) needs (basic needs,
material needs, non-material needs), needs humanisation,
participation, personal growth, quality of life, repression,
responsibility, satisfaction (of needs), self-realization,
solidarity, survival needs, welfare needs, violence,
wisdom, ways of life.
This experiment showed how people conceive
linguistic oppositeness. It also was an opportunity to check
various standpoints in linguistics about antonymy.
Let us recall that John Lyons (1968) has identified
three different kinds of oppositeness: complementarity,
antonymy, and converseness. Complementarity covers
terms like single and married or male and female, where
the assertion of one implies the denial of the other and also
the denial of one implies the assertion of the other.
Complementarity holds primarily between two adjectives
or two adverbs, but in the above list of 35 terms only
dialogue (opposed to monologue) is of this type.
Converseness (or reciprocity) covers situations like
buy – sell, husband – wife, offer – accept. It does not
involve negation al all, but a permutation of associated
individuals. No such situation appeared in the above list.
What about antonymy? Following Lyons, it covers
those pairs of terms where the assertion of one implies the
507

Solomon Marcus

denial of the other, but the denial of one does not imply the
assertion of the other (black – white). Unfortunately, under
this form antonymy is of no utility in investigating
oppositions of terms in the above list, because oppositeness
is meant here in the sense of negation. Then, why doesn’t
this correspond to the phenomenon of complementarity?
Because complementarity (see the examples given above)
concerns only terms where a sharp (abrupt) distinction can
be made between a property and its negation, whereas
properties like participation, solidarity, wisdom do not fulfil
this condition. They are fuzzy properties in the sense of
Zadeh (1965). This situation obliged us to use a more
general concept of antonymy (which could cover Lyons’
concept as a particular case). It is a combination of
contrariness, in the sense of Dubois et al. (1973) and
fuzziness in the sense of Zadeh (1965).
References
Jean Dubois, Mathée Giacomo, Louis Guespin,
Christian Marcellesi, Jean-Baptiste Marcellesi, Jean-Pierre
Mével, (1974), Dictionnaire de linguistique, Librairie
Larousse, Paris, p. 122.
John Lyons (1968), Introduction to Theoretical
Linguistics. Cambridge, University Press.
L. A. Zadeh (1965), Fuzzy Sets, Information and
Control, vol. 8, 1965, pp. 338-353.
508

