Ştiinţa în revistele de cultură
România liberă, 15 mai 1973
Apar în momentul de faţă foarte multe reviste de
cultură. Ele sunt expresia unei dezvoltări fireşti a culturii
noastre, care promovează toate formele de activitate
spirituală. Comparaţia cu trecutul este, în această privinţă,
revelatoare şi, hotărât, în favoarea prezentului. Însă acest
proces de creştere nu e lipsit de unele anomalii. Pe una
dintre ele am dori s-o discutăm în cele ce urmează.
Cultura, după cum se ştie, include şi ştiinţa, dar aceasta
din urmă ocupă, în revistele noastre, un spaţiu cu totul
redus în raport cu cel consacrat altor sfere ale culturii şi
în raport cu ponderea pe care ea o are în viaţa socială.
Dintr-un total de 10 pagini, Contemporanul acordă o
singură pagină ştiinţei; în ultima vreme, în multe numere
rubrica ştiinţifică trăind printr-un singur articol. Am în
faţă numerele pe aprilie ale revistelor Argeş şi Astra.
Amândouă sunt interesante, dar primul e dedicat aproape
în întregime literaturii, iar cel de al doilea consacră ştiinţei
o singură pagină dintr-un total de 20. (Să observăm, în
treacăt că România literară, care, cum s-ar spune, n-are
nicio obligaţie în această privinţă, dedică totuşi uneori o
pagină ştiinţei.) Majoritatea revistelor noastre manifestă
114

Rãni deschise

acelaşi dezechilibru. Rubricile ştiinţifice permanente
constituie o raritate.
Este semnificativ exemplul revistei Săptămâna
culturală a Capitalei. Menirea ei era de a consemna şi
discuta evenimentele culturale demne de atenţie care se
desfăşoară în Bucureşti. Dacă, în ceea ce priveşte
evenimentele artistice, revista îşi îndeplineşte cu succes
această obligaţie, evenimentele ştiinţifice sunt aproape
ignorate. Totuşi, ştiinţa oferă şi ea spectacole, ca şi teatrul
sau filmul. În fiecare săptămână apar cărţi şi articole
ştiinţifice, se ţin comunicări ştiinţifice, conferinţe,
dezbateri aprinse relative la probleme ştiinţifice
controversate, au loc discuţii relative la organizarea
învăţământului, vin în vizită savanţi străini, savanţi
români, participă la manifestări ştiinţifice internaţionale,
se susţin teze de doctorat, dintre care unele dau naştere la
controverse interesante, se stabilesc noi colaborări
interdisciplinare, noi legături între cercetarea fundamentală
şi cea aplicativă, se obţin noi succese ale dialogului dintre
ştiinţă şi artă şi se desfăşoară noi încercări de a ţine pe
acestea departe una de alta. Unde se consemnează, unde
se discută aceste evenimente şi aceste probleme? Avem
mulţi savanţi tineri de seamă, dar lucrul acesta este
cunoscut mai degrabă în străinătate decât la noi. Dar nu e
vorba numai de aspectul cantitativ. Chiar puţinele articole
pe teme ştiinţifice care se publică au de multe ori un
115

Solomon Marcus

caracter întâmplător, sunt compilaţii vizibile, de la a doua
sau a treia mână, uneori mergând până la plagiat, alteori
conţin erori grosolane. La fel de întâmplător sunt
semnalate prezenţele ştiinţei româneşti peste hotare; un
articol apărut într-o revistă străină, o recenzie la un articol,
o simplă referire a unui autor străin la un autor român sunt
date ca evenimente ale zilei, ca şi cum acestea n-ar fi
devenit, în viaţa ştiinţifică actuală, fapte curente, zilnice,
care se ridică, de exemplu, numai în domeniul
matematicienilor la ordinul miilor. În schimb, atâtea fapte,
într-adevăr revelatorii, care atestă locul fruntaş pe care-l
ocupăm în unele domenii, sunt ignorate.
Desigur, manifestările ştiinţifice au un anumit grad
de inaccesibilitate. Totuşi, personalităţi proeminente ale
ştiinţei noastre au demonstrat o deosebită măiestrie în
prezentarea accesibilă a unor aspecte importante ale ştiinţei
contemporane. Aceasta nu este însă de ajuns. Se pare că
revistele de cultură îşi recrutează redactorii responsabili
aproape exclusiv dintre scriitori. Unii dintre aceştia
manifestă o reală deschidere de spirit, dar în ansamblu
fenomenul nu ni se pare normal. Este foarte bine că
Uniunea Asociaţiilor Studenţilor Comunişti din România
editează o revistă literară şi artistică, dar de ce nu şi o
revistă ştiinţifică? Viaţa studenţească, organul acestei
Asociaţii, este departe de a acorda ştiinţei locul pe care-l
merită. Pentru a da un singur exemplu, activitatea
116

Rãni deschise

cercurilor ştiinţifice studenţeşti, de o însemnătate
covârşitoare, ca element de avangardă al tineretului nostru
studios, este reflectată cu totul întâmplător în paginile
acestei reviste. Nu putem trece uşor nici peste tratamentul
uneori bizar pe care-l au articolele ştiinţifice trimise unor
redacţii. Mi s-a întâmplat ca trimiţând un articol, în urmă
cu câţiva ani, unei reviste să primesc răspuns abia peste un
an. Alteori, se operează modificări care denaturează
conţinutul şi introduc tot felul de greşeli de exprimare.
Ce-ar fi dacă un număr de absolvenţi ai facultăţilor de
matematică-mecanică, fizică, chimie, biologie, ştiinţe
umaniste şi sociale, absolvenţi care simt o anumită atracţie
pentru aspectele culturale ale disciplinei în care s-au
specializat, ar fi repartizaţi să lucreze în redacţiile
revistelor de cultură? Ce-ar fi dacă aceste reviste ar
organiza o reţea de corespondenţi în institutele de cercetare
ştiinţifică şi în cele de învăţământ, la editurile de cărţi
ştiinţifice şi la comisiile recent create, pentru răspândirea
cunoştinţelor ştiinţifice? Ce-ar fi dacă revistele de cultură
n-ar mai apela mereu la aceiaşi şi aceiaşi oameni de ştiinţă
şi ar face efortul de a-şi include printre posibilii lor
colaboratori atâţia savanţi remarcabili pe care ştiinţa
noastră i-a format? Aş putea să fac pe loc o listă de zeci de
colegi care au dobândit mari succese în cariera lor
ştiinţifică şi care, în acelaşi timp, şi-au dezvoltat o
personalitate intelectuală multilaterală, pe care însă
117

Solomon Marcus

revistele noastre nu-i solicită. Ar fi suficient ca fiecare
dintre ei să scrie o dată pe an un articol pentru un public
mai larg, pentru ca revistele noastre să-şi schimbe
înfăţişarea. Este, cred, necesară o reconsiderare generală
a modului în care viaţa ştiinţifică este reflectată în revistele
noastre de cultură.

118

