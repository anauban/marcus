O creaţie a ultimelor decenii:
„artele de calculator”
Interviu realizat de Leonard Gavriliu pentru Informaţia
Bucureştiului, nr. 9173, 7 aprilie 1983, p. 2
L.G.: Informatica invadează astăzi nu numai lumea
ştiinţei şi tehnicii, a economiei, ci şi pe aceea a creatorilor
de artă. Asistăm la o ofensivă tot mai puternică, în întreaga
lume, a aşa-numitelor „arte de calculator” sau
„computaţionale” şi se pare că nu este departe timpul în
care niciun creier omenesc nu va mai putea să dea
maximum de randament fără a se alia cu un calculator
programat în raport cu profesia, talentul şi aspiraţiile
fiecăruia. Dat fiind că mă adresez unuia dintre protagoniştii
recunoscuţi ai acestei mişcări, autor al mult apreciatei
Poetici matematice şi coordonator al recentului volum
Semiotica matematică a artelor vizuale (Editura Ştiinţifică
şi Enciclopedică, 1982), vă întreb: sunteţi într-adevăr
convins că aceste noi tehnici sau mijloace de creaţie
artistică vor sta la baza capodoperelor de mâine? Personal,
drept să vă spun, nu am găsit în „poemele de calculator”,
citate de specialişti, versuri care să depăşească din punct de
vedere estetic fie şi cele mai proaste versuri compuse de
dadaişti. La ce bun, atunci, acest „dadaism computerizat”?
608

Rãni deschise

S.M.: Cred că în domeniul poeziei, al literaturii în
general, este încă prea devreme să pretindem realizări de
valoare, deşi sunt semne de certă reuşită în ceea ce
priveşte parodierea unor autori, adică simularea pe
calculator a manierei lor de a scrie poezie. Pot fi date,
astfel, la iveală, procedeele utilizate de un poet sau altul,
algoritmii care stau la baza generării textului liric. În
afară de aceasta, calculatorul se dovedeşte un preţios
auxiliar pentru poet, nu numai pentru găsirea rapidă a
rimelor (Eminescu elaborase în acest scop un dicţionar
de rime), de pildă, ci şi în direcţia creativităţii metaforice,
având în vedere faptul că în poezie există o mai mare
libertate a combinării cuvintelor decât în limbajul uzual.
Cu totul altfel stau lucrurile în domeniul muzicii şi al
artelor vizuale (pictură, grafică, sculptură, arhitectură,
design), unde de pe acum pot fi puse în evidenţă izbutiri
mai mult decât promiţătoare. Iată un exemplu
semnificativ: pornind de la analiza statistică a
elementelor componente ale tabloului Chei şi ocean, de
Mondrian, Michael Noll a putut obţine, cu ajutorul
calculatorului, o nouă lucrare, având aceleaşi
caracteristici. Expus împreună cu originalul, noul tablou
a întrunit preferinţele a 55 la sută dintre privitori, faţă de
Mondrian-ul autentic.
L.G.: Înseamnă că informatica, în speţă calculatorul,
poate suplini talentul?
609

Solomon Marcus

S.M.: Din nou o întrebare dură, nemiloasă. Nu cred
că poate fi vorba de vreo suplinire a talentului, ci de faptul
că acum există posibilitatea ca acesta să fie realmente
asistat de calculator. Indiferent de domeniu, talentul are
numai de câştigat din conlucrarea sa cu ştiinţa şi cu
invenţiile tehnice. În introducerea la Semiotica matematică
a artelor vizuale, am încercat să arăt că artistul este azi tot
mai mult dublat de un cercetător ştiinţific şi că evoluţia
culturii contemporane reface, evident la un nivel superior,
situaţia deosebit de rodnică pe planul creaţiei existente în
Antichitate şi în Renaştere: colaborarea artei cu ştiinţa.
Informatica ne permite identificarea mecanismelor
generative ale unei opere, a aşa-ziselor ei „secrete de
fabricaţie”. Odată cunoscute în structura şi funcţionalitatea
lor, acestea pot fi puse la lucru cu o mai mare
productivitate şi chiar cu rezultate calitativ superioare,
dincolo de orice aşteptare – adesea sterilă – a momentelor
de „inspiraţie”.
L.G.: Puteţi aduce, în sprijinul afirmaţiilor
dumneavoastră, exemple edificatoare?
S.M.: Ele se găsesc din abundenţă în cartea citată.
Este de relevat între altele, marea varietate, ca şi acurateţea
graficii de calculator, a cărei expresie sintetică aminteşte
de decorurile de factură folclorică românească, dar de o
fineţe imposibil de realizat manual. Bineînţeles că, pe
lângă talent, graficianul care apelează la serviciile
610

Rãni deschise

calculatorului este obligat să aibă şi cunoştinţe de
informatică. Fapt este că, în aceste condiţii, posibilităţile
sale de inovaţie şi creaţie devin, practic, nelimitate.
Extinderea principiilor graficii de calculator are
perspective largi în tapiserie, industria ceramică,
imprimerie textilă, scenografie, caricatură, publicitate şi
design industrial (bunăoară, în proiectarea caroseriilor de
automobile, a profilurilor de aeronave etc.). Este de reţinut
faptul că informatizarea acestor arte face cu putinţă
transformarea în cvasiunicate a unor lucrări de serie,
introducând diversitatea estetică în sfera ambientului şi a
bunurilor de larg consum, în consens cu gustul şi
exigenţele actuale ale personalităţii umane.

611

