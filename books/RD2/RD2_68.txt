Limbaj poetic – limbaj matematic
Orizont, nr. 12 (734), 26 martie 1982, pp. 2-3
Problema convergenţelor dintre limbajul poetic şi cel
matematic, ca şi problema opoziţiilor dintre cele două
limbaje m-au preocupat intens şi după apariţia Poeticii
matematice. Am publicat în acest sens câteva studii dintre
care, în româneşte, cel mai cuprinzător este capitolul
dedicat lui Pius Servien în cartea mea Din gândirea
matematică românească. Servien a constituit punctul meu
de plecare în această problemă. Întreaga mea abordare este
o dezvoltare şi o continuare a punctului său de vedere.
Concomitent însă, mi-am luat distanţa faţă de Servien.
Astfel, în loc să consider că limbajul ştiinţific (LS) şi cel
poetic (LP) sunt disjuncte, am plecat de la ipoteza că orice
enunţ manifestă diferite disponibilităţi, de fiecare dată
actualizându-se una sau alta dintre ele. Desigur, ceea ce
spun se referă la o stare ideală, o stare limită a limbajului,
confirmată însă de poezia ultimelor decenii. Pentru a da
numai două exemple, voi aminti că enunţul deux et deux
quatre este un vers din Jacques Prévert iar ecuaţia e=mc2
din teoria relativităţii a devenit un vers de Nichita Stănescu.
Adeziunea la opoziţiile pe care le-am propus, între
LS şi LP, a fost puternică, dar nici contestaţia nu a fost
514

Rãni deschise

neglijabilă. Cercetările în acest domeniu, la noi în ţară şi
peste hotare, au fost atât de bogate, încât ar furniza
materia unei cărţi de câteva sute de pagini. Nu poate fi
vorba de o „meditaţie ultimă” pe această temă. În primul
rând, chiar eu am polemizat cu unele dintre propriile mele
idei din 1970. Am crezut, de exemplu, în Poetica
matematică, în caracterul pur denotativ al limbajului
matematic, pentru ca să mă îndoiesc ulterior, după cum
nu am mai putut adera la identificarea LS cu ipostaza
raţională şi a LP cu ipostaza emoţională a limbajului. Un
fapt derutant în structura opoziţiilor dintre LS şi LP îl
constituie situarea lor la niveluri de abstracţie diferite.
Astfel, absenţa sinonimiei în LP se referă la un nivel
superior de abstracţie al acestui limbaj, nivel la care
semantica LP nu mai este discretă, ci continuă, în timp ce
proprietatea de proiectivitate sintactică a celor mai multe
propoziţii din LS are în vedere o analiză la nivelul
expresiei direct perceptibile. Am considerat că cititorul va
situa, de la sine, fiecare opoziţie la nivelul corespunzător
de abstracţie. Nu s-a întâmplat însă aşa. Astfel, I. Funeriu
(Versificaţia românească, p. 161) contesta ideea absenţei
sinonimiei în LP, argumentând că sinonimia „nu
presupune identitatea (echivalenţa absolută) a două
cuvinte, din punct de vedere semantic”. Nu s-a sesizat că
1) noi avem în vedere o tendinţă asimptotică a limbajului
poetic, faţă de care diferitele texte sunt simple aproximări
515

Solomon Marcus

şi 2) sinonimia la care ne referim este tocmai echivalenţa
absolută a două enunţuri, deoarece priveşte nu situaţia din
limbajul uzual, ci aceea din limbajul matematic, unde
orice enunţ poate fi parafrazat, reformulat într-o infinitate
de feluri semantic identice. Această idee a definirii LS
prin existenţa, pentru orice enunţ, a o infinitate de
enunţuri echivalente se află de altfel la baza întregii
estetici a lui Servien, estetică şi ea contestată, negată în
însăşi existenţa ei de către un autor valoros ca regretatul
Ion Maxim, care-i reproşa că nu defineşte categoria
frumosului. O obiecţie surprinzătoare a venit din partea
lui Marian Popa, care interpreta alegerea limbajului
matematic drept termen de referinţă în analiza LP în
sensul că preconizăm o poezie al cărei punct de plecare
se află în matematică; aspectul structural al LP este astfel
confundat cu aspectul său genetic. Abordarea noastră se
referă la o viziune a posteriori, structurală, a limbajului
poetic şi nicidecum la procesul de creaţie poetică. Poetul
poate ignora limbajul matematic, după cum poate fi
preocupat de diferite alegeri între expresii cvasisinonime,
dar rezultatul căutărilor sale va putea fi raportat cu folos
la limbajul matematic şi va fi apreciat, între altele, şi după
rezistenţa la substituţie.
Lista dicotomiilor dintre LS şi LP este deschisă.
Într-un articol din România literară din 1981, prozatorul
Mircea Ciobanu atrăgea atenţia asupra unei distincţii
516

Rãni deschise

subtile între ştiinţă şi artă. Prima este condiţionată de
„disponibilitatea punerii la îndoială”, în timp ce a doua stă
sub semnul „nevoii fiinţei cititoare de a se lăsa înşelată”.
Cât de mult ar fi de glosat pe marginea acestei idei!
Din păcate, versiunea din 1975 a listei mele de
dicotomii a trecut neobservată de către lingvişti şi literaţi,
probabil din cauza faptului că a fost publicată într-o carte
al cărei titlu trimitea la matematică. Între timp însă s-au
acumulat nu numai noi clarificări, ci şi noi nedumeriri.
Într-un moment în care literatura recuperează multe dintre
încercările eşuate ale altor discipline, este interesant de
observat modul în care ideea însăşi de literaritate este
mereu repusă în discuţie. Şerban Cioculescu observa, nu
de mult, că pe Nicolae Iorga, scriitorul, trebuie să-l căutăm
nu atât în scrierile sale propriu-zis literare, cât în opera sa
istorică. În două cărţi remarcabile, Eugen Negrici şi Livius
Ciocârlie au atras atenţia asupra unor modalităţi prin care
unele texte neliterare în intenţia lor pot căpăta o identitate
literară. Nu cumva este vorba aici de un fenomen mai
general şi mai profund? Ipoteza noastră asupra poeticităţii
potenţiale a oricărui enunţ (mai general: capacitatea
oricărui enunţ de a fi actualizat, după împrejurări, în LS,
în LP sau în alte limbaje posibile) vine în întâmpinarea
unui răspuns afirmativ la această întrebare. Într-o atare
perspectivă ne putem, în continuare, întreba dacă nu
cumva avem încă mari datorii faţă de una dintre cele mai
517

Solomon Marcus

singulare personalităţi ale culturii româneşti. Într-adevăr,
s-au acumulat monografii critice şi ediţii valoroase privind
opera lui Ion Barbu (Rosetti-Călin, Pillat, Vulpescu,
Mincu, Gibescu, Teodorescu, Foarţă, Scarlat, Ciorănescu),
ediţii ale operei lui Dan Barbilian (Vrănceanu, Mihăileanu,
Mihoc-Vodă), avem chiar două monografii care-l caută pe
cel de al doilea în opera celui dintâi (Nicolescu, Mandics),
dar nu dispunem încă de nicio cercetare privind ipostaza
poetică a textelor de geometrie, algebră şi teoria numerelor
aparţinând lui Dan Barbilian. Tatonând această problemă,
putem anticipa un rezultat pe care, suntem convinşi,
cercetări ulterioare îl vor confirma: există un poet Dan
Barbilian, altul decât cel din eseurile dedicate lui. Gauss,
Ţiţeica, Hilbert şi Galois, un poet poate la fel de mare ca
Ion Barbu. Esenţială nu este aici „expresivitatea
involuntară”, ci poeticitatea inevitabilă a oricărui text
purtând marca Barbu-Barbilian. Această apartenenţă
simultană a aceloraşi texte la LS şi la LP constituie o
sfidare a multor principii considerate de veacuri
sacrosante, în legătură cu incompatibilitatea dintre LS şi
LP; însă ne va trebui încă multă răbdare şi meditaţie pentru
a înţelege şi explica mecanismul de constituire a acestei
duble funcţionalităţi a textelor lui Barbilian.

518

