Rosturile umane ale cunoaşterii
România liberă, 4 iulie 1979
Cartea doctorului în filosofie Vasile Sporici,
Determinism şi structură în genetica modernă, Editura
Junimea, Iaşi, 1978, tratează o problemă deosebit de
delicată, care interesează întreaga evoluţie a cunoaşterii
umane şi care, în lumina rezultatelor recente ale ştiinţei,
reclamă o reconsiderare totală: aceea a raportului dintre
determinare şi structură. Deşi titlul lucrării se referă numai
la biologie, analiza întreprinsă de autor e atât de
cuprinzătoare, încât semnificaţiile ei pot fi uşor valorificate
în orice domeniu al creaţiei umane. De fapt, autorul
explică, prin intermediul biologiei şi – în special – al
geneticii, dinamica întregii cunaoşteri umane. Argumentele
autorului pentru acest rol privilegiat al biologiei sunt
convingătoare şi originale. Se ştie că viziunile unificatoare
obişnuite fac apel la modelele logico-matematice,
sistemice sau semiotice. Deşi nu neagă semnificaţia
acestor modele, viziunea unificatoare a autorului este cu
totul alta, deoarece nu se referă atât la metodologie, cât la
problematica şi complexitatea domeniilor actuale de
creaţie şi vizează, în ultimă instanţă, „căutarea rosturilor
umane ale cunoaşterii”.
280

Rãni deschise

Considerentele care-l conduc pe autor să preconizeze
un rol unificator al biologiei în raport cu întreaga cultură
umană sunt judicios prezentate, operându-se distincţii
nuanţate între biologie şi diferite curente care tind să
integreze zone întinse ale culturii. Biologia, este, într-adevăr,
o disciplină situată la răscrucea mai tuturor drumurilor mari
pe care le-a străbătut cunoaşterea umană în secolul nostru.
Biologia valorifică toate achiziţiile fizicii şi chimiei, atât în
ceea ce priveşte rezultatele, cât şi în ceea ce priveşte
metodele. Dar, în acelaşi timp, prin complexitatea superioară
a fenomenelor pe care le studiază, modelele biologice sunt
mai aproape de procesele sociale. Nu întâmplător, teoria
sistemelor – mai bine zis, abordarea sistemică – s-a născut
la sugestiile biologiei. Să ne amintim apoi faptul că o lungă
tradiţie, care-şi are rădăcinile în Antichitate, dar care s-a
îmbogăţit mereu cu alte şi alte rezultate, culminând, recent,
în aşa-numita „teorie a catastrofelor”, a invederat puternica
solidaritate de structură a proceselor de creştere, de evoluţie
a formelor, în lumea organică, pe de o parte, în artă, pe de
altă parte. Iar în momentul în care ştiinţele sociale au început
a manifesta exigenţe sporite în ceea ce priveşte studiul
aspectelor procesuale dinamice, de sistem, biologia le-a
venit în ajutor, cu un întreg arsenal de modele de o mare
complexitate.
Să observăm însă că, din marea varietate a disciplinelor
biologice, autorul reţine cu precădere genetica. Această
281

Solomon Marcus

alegere poate fi justificată în mai multe feluri. În primul rând,
genetica detectează unităţile biologice fundamentale, a căror
combinatorică explică întreaga varietate infinită a lumii vii,
dar în acelaşi timp şi unitatea acestei lumi (deoarece
altfabetul genetic este universal). În al doilea rând, genetica
este disciplina care a cunoscut, în ultimele decenii, cele mai
multe descoperiri de anvergură, care ne-au pus în faţa
„dicţionarului vieţii” (a se vedea marele număr de premii
Nobel atribuite pentru descoperiri în domeniul geneticii). În
al treilea rând, este greu de găsit un domeniu care ilustrează
într-un mod mai semnificativ decât genetica raportul dintre
necesitate şi întâmplare, dintre unitate şi varietate, dintre finit
şi infinit. În al patrulea rând, aspectele procesuale ale realităţii
înconjurătoare îşi găsesc un prototip în procesul de formare
a proteinei, deci, genetica rezumă, am putea spune, întreaga
dialectică a realităţii înconjurătoare.
De o deosebită fineţe, sunt, în analiza autorului,
dialectica dintre geneză şi structură, dintre ereditate şi
experienţă, distincţia dintre determinism şi cauzalitate,
dintre întâmplare şi indeterminare. De o mare valoare ni se
par următoarele trei idei ale autorului şi modul ingenios în
care ele sunt argumentate: 1. Înlocuirea reprezentării
rudimentare date de relaţia cauză-efect prin raportul
interactiv determinare-structură; 2. Interpretarea preadaptării
ca expresie a compatibilităţii logico-istorice dintre
structurile vii şi determinismul lor abiotic; 3. Interpretarea
282

Rãni deschise

fenomenelor statistice ca expresie a unui determinism global
şi modul ingenios de explicare a mecanismului prin care
acumularea unui mare număr de nedeterminări instantanee
sau locale poate conduce la un determinism global foarte
riguros.
Desigur, o carte trebuie judecată în primul rând în
raport cu ceea ce conţine. Nu vom ascunde însă regretul
nostru (explicabil, fireşte, şi prin obsesiile noastre
lingvistice) de a nu fi găsit în această carte – şi aşa foarte
bogată – atitudinea autorului faţă de o altă viziune
unificatoare, căreia nu i-a scăpat nici genetica, aceea care
o aşază sub semnul teoriei limbajelor; cu atât mai mult cu
cât, în lumina noilor teorii, conform cărora lingvistica e o
ramură a psihologiei cognitive, limbajul este rezultatul
dialecticii dintre competenţă şi performanţă, această
dialectică fiind sediul contradicţiilor care explică evoluţia
unui limbaj.
Prin profunzimea ideilor, prin originalitatea
argumentelor şi prin fineţea distincţiilor pe care le propune,
lucrarea de faţă se constituie într-un dialog interesant
pentru orice intelectual preocupat de semnificaţiile marilor
descoperiri ştiinţifice din ultimele decenii. Cartea răspunde
curiozităţii legitime a tineretului nostru pentru marile
aventuri spirituale ale omenirii.

283

