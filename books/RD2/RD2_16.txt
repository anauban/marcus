Pedagogia matematicii
Viaţa studenţească, 11 ianuarie 1977
I
Primele pagini ale unei cărţi de matematică dau
adesea impresia unor consideraţii banale. Dar, fără
răbdarea de a le parcurge cu atenţie, s-ar putea ca paginile
imediat următoare să devină ininteligibile.
*
Noţiunile matematice impresionează prin polivalenţa
lor. Cuvinte ca sinus şi cosinus, trimit la lucruri atât de
diferite ca evaluarea elementelor unui triunghi şi studiul
fenomenelor periodice.
*
Definiţiile matematice sunt aşezate de obicei la
începutul expunerii, deşi, în procesul de descoperire, ele
aparţin, de cele mai multe ori, etapei finale. Atenuarea
acestei situaţii paradoxale constituie una din marile
probleme ale pedagogiei matematicii.
*
Pedagogia matematicii este confruntată la fiecare pas
cu două tendinţe complementare ale gândirii matematice:
tendinţa spre sistematizare şi tendinţa spre descoperire.
145

Solomon Marcus

Cine nu a trăit el însuşi starea de spirit a cercetătorului nu
va putea decât să simuleze tendinţa de înnoire a
matematicii. După cum, cel care nu este preocupat să
comunice şi altora faptele matematice care-l preocupă va
găsi cu greu prezentarea cea mai adecvată auditoriului pe
care-l are în faţă.
*
Este o iluzie să credem că, învăţând o definiţie, am
asimilat şi noţiunea care face obiectul ei. Noţiunile
matematice au oarecum soarta cuvintelor. Aşa cum
definiţia unui cuvânt în dicţionar nu poate epuiza bogăţia
sa de sensuri, o definiţie matematică nu poate cuprinde
totalitatea comportărilor contextuale ale obiectului
matematic la care ea se referă. De aici, necesitatea de a
face un număr cât mai mare de exerciţii; ele adaugă
definiţiilor un număr mare de nuanţe contextuale.
*
Cuvintele nu pot căpăta o putere magică numai pentru
că sunt tipărite. Cărţile de matematică, chiar cele mai bune,
pot conţine motivaţii insuficiente, lacune în demonstraţii,
lungimi inutile sau pur şi simplu greşeli de tipar. Învaţă-te să
le descoperi singur. Este cel mai bun test al asimilării materiei.
*
E adevărat că definiţiile nu se demonstrează. Dar
aceasta nu înseamnă ce ele sunt arbitrare; posibilitatea de
a le motiva este simptomatică pentru înţelegerea
146

Rãni deschise

matematicii nu numai ca tehnică de lucru, ci şi ca fapt de
cultură.
*
Nu te lăsa intimidat, dar nici păcălit, de expresii ca
„se vede uşor că” sau „este evident că”. Sub astfel de
asigurări care se vor liniştitoare se pot ascunde oricare
dintre următoarele categorii de aserţiuni: 1) propoziţii
într-adevăr imediate; 2) propoziţii uşoare, dar nu chiar
evidente; 3) propoziţii evidente pentru un matematician
versat, dar cerând o argumentare destul de laborioasă din
partea unui începător; 4) teoreme grele chiar pentru un
matematician format; 5) propoziţii false. Este cunoscută
întâmplarea cu celebrul matematician englez Hardy, care,
în faţa studenţilor, după ce a avansat un rezultat ca evident,
a avut nevoie de un timp îndelungat pentru a constata că
rezultatul în cauză e într-adevăr... evident.
II
La întrebarea „Cât fac 5 împărţit la zero?” unii elevi
mai răspund, din păcate, 5. Au pregătit şi o justificare:
„Dacă ai cinci mere şi nu le împarţi la nimeni, rămâi cu
toate cinci”. Nu cumva de vină sunt acele manuale care
evită definirea lui zero ca element nul în operaţia de
adunare, preferând definirea sa „conţinutistică”, de tipul
„zero înseamnă nimic”?
147

Solomon Marcus

*
O situaţie asemănătoare o prezintă mulţimea vidă,
care acum figurează chiar în manuale de la clase mici.
Nefericită definirea acestei mulţimi drept muţimea care nu
conţine niciun element (definiţie care provoacă replica
firească a unora dintre elevi: „Dacă n-are niciun element,
atunci a dispărut mulţimea!”). Introducerea acestui concept
pe cale operaţională, prin indicarea comportării mulţimii
vide în anumite operaţii cu mulţimi, este mult mai
conformă cu natura lucrurilor.
*
Învaţă-te să citeşti în zigzag. Dacă nu ai înţeles un
lucru chiar după mai multe încercări, treci totuşi mai
departe. S-ar putea ca unele puncte de rezistenţă de la
pagina 50 să capituleze abia după ce le-ai asediat,
avansând până la pagina 100. Chiar dacă ţi se pare că ai
înţeles totul, reia din când în când lectura textului parcurs.
Probabil că-ţi va apărea într-o lumină nouă.
*
După ce ai rămas cu impresia că ai înţeles o teoremă
relativă la spaţii topologice, controlează-te dacă o poţi
particulariza la bietul spaţiu euclidian.
*
Eşti în stare să recunoşti o teoremă sau o definiţie
atunci când se schimbă notaţia uzuală, sau când formularea
ca atare este modificată într-o variantă totuşi echivalentă?
148

Rãni deschise

Dacă răspunsul e negativ, înseamnă că nu cunoşti teorema,
ci numai veşmântul ei, n-ai înţeles spiritul definiţiei, ci
numai litera ei.
*
Nu e totuna dacă studiezi în fiecare zi câte trei ore sau
de două ori pe săptămână câte zece ore. Aşa cum nu e
totuna dacă mănânci de trei ori pe zi sau o singură dată cât
de trei ori. Creierul (ca şi stomacul) pretinde o anumită
periodicitate, un anume ritm. O activitate intelectuală
zilnică generează un mediu intelectual corespunzător, care
ne permite să ne familiarizăm cu obiectul supus studiului,
să dezvoltăm nu numai înţelegerea lui raţională, dar şi
cuprinderea lui intuitivă.
*
Calculele pe care profesorul le scrie la tablă se află
probabil şi în manual. Nu e foarte important să fie
transcrise cu toate detaliile în caiet. Fii însă cel puţin la fel
de atent la comentariile pe care profesorul le face fără a le
scrie pe tablă. S-ar putea ca ele să nu se afle în nicio carte!
*
Într-un eseu despre Bach şi muzica pură, Georges
Duhamel spune că înclinaţia pentru matematică este
aproape neomenească, pentru că este comună omului şi
animalului. Duhamel are aici în vedere faptul că o albină
construieşte cu perfecţiune alveola hexagonală, „minune
a geometriei”. Dar se poate pune semnul egalităţii între
149

Solomon Marcus

structurile matematice ale naturii (fizice, chimice sau
biologice) şi cele pe care mintea omenească le creează sau
le regăseşte în mod deliberat, pentru a diminua entropia
universului în sensul dorit de ea? Matematica, produs al
culturii, este una, în timp ce structura matematică a naturii
este cu totul altceva. Dacă prima dintre acestea este
neomenească pentru că-şi ia ca model pe cea de a doua,
atunci şi arta e neomenească, pentru că fără frumuseţea
naturală a Monei Lisa nici pânza lui Leonardo n-ar fi
existat. Sau, pentru a ne referi chiar la domeniul pe care
Duhamel îl are în vedere şi pe care-l contrapune
matematicii: este oare muzica neomenească pentru că e
„concurată” de trilurile privighetorii?

150

