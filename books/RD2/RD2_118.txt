Primul colocviu naţional de limbaje,
logica şi lingvistica matematică
Viaţa Studenţească, anul XXX, nr. 24 (1100),
miercuri, 18 iunie 1986, p. 2
În zilele de 5-7 iunie 1986, Universitatea din Braşov a
găzduit o manifestare ştiinţifică prestigioasă. La iniţiativa
Catedrei de Matematică (conf. dr. Gabriel Orman), a fost
organizată o întâlnire interdisciplinară a cercetătorilor care,
venind din direcţii dintre cele mai diferite, se preocupă de
structura limbajelor umane, informaticieni, lingvişti,
matematicieni, logicieni, filozofi, fizicieni, literaţi s-au putut
astfel asculta unii pe alţii, iar tentativele de dialog, chiar dacă
au întâmpinat dificultăţi serioase, au fost de multe ori
fructuoase. Peste 40 de comunicări au abordat probleme
dintre cele mai variate, ca structura logico-matematicoalgoritmică a limbilor naturale (Valentina Agrigoroaiei şi
Alexandru Cărăuşu; Neculai Curteanu, Oana Popărda, Ioan
Oprea); studiul limbajelor de programare la calculator
(Dragoş Vaida; Tudor Bălănescu şi Marian Gheorghe;
Florian Boian); teoria limbajelor formale (Gheorghe Păun;
Gabriel V. Orman; Irina Bucurescu, Anca Pascu; Adrian
Atanasiu); studiul logico-matematic al limbilor naturale
(Emanuel Vasiliu; Mihai Dinu; Lucreţia Vasilescu);
851

Solomon Marcus

probleme ale limbajelor logice, la interferenţa logicii
matematice cu informatica (Mihail Cherciu, Alexandru
Mateescu, Doina Tătar, Viorel Mihăilă); demonstrarea
automată a teoremelor (Mircea Maliţa, Mihaela Maliţa);
probleme de logică matematică (Cristian Calude, Dumitru
Dumitrescu, George Ceauşu, Tiberiu Gheţie), aplicaţii ale
teoriei limbajelor în biologie (Manuela Mateescu, Mihail
D. Nicu); modele analitice ale limbilor naturale (Emil
Popescu); limbaje picturale, recunoaşterea formelor (Florian
Petrescu, Daniela Marinescu, Răzvan Andonie); analiza
tipurilor de discurs (Ernestin Pataky, Pompiliu Peculea;
Daniela Frumuşani); semiotica obiectelor (Rodica Marcu);
limbaje în grafuri (Moise Cocan, Aurel Luca); analiza
textului literar (Dan Mârza; Zorin Diaconescu); teatru
(Costică Mustaţă); automate programabile (Sanda Sassu;
Cetin Denislam, Emilian Gh. Oprea); timpul verbal
(Dimitrie Cazacu); infinitudinea în logica modală a fizicii
(Liviu Sofonea); o masă rotundă privind statutul limbajelor
artificiale a fost organizată de Solomon Marcus.
În afara autorilor de comunicări, au luat parte la
discuţii şi alţi specialişti, ca profesorul Florea Dudiţă, de
la Universitatea din Braşov, şeful catedrei de teoria
mecanismelor, şi prof. Gh. Ardelean, matematician de la
Baia Mare.
Limbajul constituie, fără îndoială, una dintre
problemele globale ale omenirii. În multe părţi ale lumii,
852

Rãni deschise

limbile naturale trec printr-o perioadă de criză, ca urmare
a faptului că noile generaţii stăpânesc tot mai defectuos
limba maternă. Civilizaţia vizuală, noile mijloace de
comunicare îşi au, fără îndoială, rolul lor în această
privinţă. Dezvoltarea impetuoasă a unor noi limbaje
artificiale pune probleme delicate în ceea ce priveşte
asimilarea lor. În numeroase medii intelectuale, tensiunea
dintre limbile artificiale şi cele naturale se accentuează.
Noile generaţii se află în faţa unor probleme tot mai
complexe de stăpânire a limbajelor celei de a doua
revoluţii industriale. Toate aceste aspecte au apărut şi în
desfăşurarea colocviului de la Braşov. S-a putut vedea că
pe cât de mare este dorinţa de înţelegere reciprocă, pe atât
de dificilă este încă realizarea ei. Unii cercetători sunt încă
insuficient antrenaţi în a se face înţeleşi de către colegii
lor, care folosesc un alt ,,jargon” ştiinţific. În pofida acestei
situaţii, mulţi dintre participanţi şi-au exprimat satisfacţia
pentru contactul realizat la Braşov, pentru numeroasele
sugestii de care au beneficiat ascultându-i pe ceilalţi.
Am regretat faptul că numărul studenţilor participanţi
la colocviu a fost destul de redus. Pentru următoare ediţie
a acestei manifestări, va trebui să existe o preocupare
specială de a introduce în program comunicări ale
studenţilor valoroşi din facultăţile de matematică, din cele
de filologie, din cele de filozofie, din politehnici şi institute
economice.
853

