Teatrul şi educarea gustului public
Anchetă de Aurelia Boriga, Teatrul, revistă a Consiliului
Culturii şi Educaţiei Socialiste, nr. 2 (februarie),
anul XXXII, 1988, pp. 16-28
1. Pot spectacolele existente la ora actuală să
formeze o cultură teatrală?
2. Cum contribuie teatrul la formarea unei limbi
frumoase şi expresive?
3. Cum poate fi definit, detectat şi combătut prostul
gust?
Un rol important în cultivarea limbii îl au actorii
1. Prin cultură teatrală înţelegem două lucruri: a) o
cultură care se constituie într-o imagine fidelă a stadiului
actual atins de literatura dramatică şi de spectacolul teatral
în lume; şi b) o cultură care dă seama despre modul în care
teatrul înregistrează marile probleme care stau azi în faţa
omenirii. Aceste obiective de o mare complexitate se
realizează, în primul rând, printr-un repertoriu adecvat. În
ceea ce priveşte obiectivul a, probabil că ne aflăm într-o
situaţie relativ satisfăcătoare (prudenţa mea este datorată
faptului că nu cunosc repertoriul teatrelor nebucureştene
1035

Solomon Marcus

şi nu am o imagine mai precisă asupra evoluţiei
repertoriului de-a lungul ultimelor decenii). Chiar şi pentru
un observator superficial, varietatea repertoriului actual
este evidentă. Spectacole ca Hamlet, Doamna cu camelii,
Menajeria de sticlă, Ploşniţa, Zbor deasupra unui cuib de
cuci, Marea, din repertoriul internaţional, O scrisoare
pierdută, D’ale carnavalului, Mitică Popescu, Aceşti
îngeri trişti, Io, Mircea Voievod, Preşul, Noţiunea de
fericire, Dimineaţa pierdută, din repertoriul naţional,
furnizează o imagine elocventă a varietăţii şi valorii.
Alta este situaţia în privinţa obiectivului b. Aşteptăm
de la teatru, mai mult decât de la oricare altă artă,
înregistrarea şi aprofundarea noilor tipuri de conflicte pe
care le generează, în relaţiile social-umane, cea de-a doua
revoluţie industrială, problemele globale ale omenirii,
ritmul rapid de transformare a profesiilor, modul în care
satisfacerea nevoilor umane fundamentale de
supravieţuire, de libertate, de identitate şi de sens trece
astăzi printr-o fază fără precedent în istoria omenirii.
O cultură teatrală nu poate eluda niciuna dintre
problemele majore ale omenirii.
2. Rolul teatrului în cultivarea unui limbaj îngrijit se
realizează atât prin calităţile textului şi prin interpretarea
actorilor, cât şi prin „complicitatea” spectatorilor. Teatrul
poate fi o lecţie de dicţiune şi de retorică, iar aceste calităţi,
dincolo de valoarea lor culturală generală, sunt un exemplu
1036

Rãni deschise

binefăcător pentru atâţia spectatori care practică profesii
în care „punerea în scenă” are o anumită importanţă (cadre
didactice, jurişti etc.). Nu vom ascunde însă că, uneori,
autorii dramatici şi actorii tratează superficial această
chestiune; chiar zilele trecute am putut auzi o actriţă, de
altfel foarte talentată, exprimându-se în modul următor:
„poezia care am spus-o”.
Teatrul, prin varietatea personajelor pe care le aduce
în scenă, furnizează o vastă tipologie a abaterilor de la o
vorbire exemplară. Eroii lui Caragialeau creat, în această
privinţă, un adevărat termen de referinţă. Este necesar deci
ca actorul să-şi ia o distanţă necesară faţă de personajul pe
care-l interpretează, iar spectatorul trebuie să fie suficient
de cult pentru a sesiza această distanţă.
3. Prostul gust este un aspect al inculturii. Ar trebui
să se acorde o atenţie mai mare culturii teatrale la vârsta
şcolară, când se formează gusturile. Spectacolele însoţite
de dezbateri ar fi foarte eficiente. Eventual, se poate face
aceasta numai cu un act dintr-o piesă. O manifestare a
lipsei de gust (de fapt, lipsă de înţelegere) am putut-o
vedea la o parte din spectatorii la Dimineaţa pierdută:
Rumoarea stârnită de unele momente dintre cele mai
nepotrivite este o adevărată provocare la adresa actorilor,
care pot fi tentaţi să orienteze spectacolul într-o direcţie
greşită. Cei mai mulţi rezistă, dar există şi momente de
cedare.
1037

