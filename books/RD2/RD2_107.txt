Pentru stimularea creativităţii
Viaţa studenţească, nr. 40 (1064),
2 octombrie 1985, p. 3
,,Viaţa ştiinţifică şi culturală a ultimilor ani ne aduce
din ce în ce mai frecvent în atenţie o serie de manifestări
care sfidează tradiţionala împărţire a cunoaşterii pe felii şi
parcele. Fără îndoială, reuniunile ştiinţifice pe teme de
matematică, fizică, chimie, biologie etc. nu-şi pierd cu nimic
din importanţă, ele continuă să fie organizate, exprimând o
necesitate vitală (chiar în ultima vreme au avut loc, la noi în
ţară, câteva întâlniri de acest fel), dar concomitent cu ele se
produc şi altfel de întâlniri, care aduc faţă în faţă, în dialog,
pe matematicieni şi ingineri, pe fizicieni şi economişti, pe
biologi şi sociologi, pe chimişti şi lingvişti, pe medici şi
filosofi, pe jurişti şi istorici, toţi simţindu-se implicaţi întro aceeaşi problematică, în care încearcă să descopere un
limbaj comun. Iată, de exemplu, pentru a ne referi numai la
manifestări care au loc în luna octombrie a acestui an, sub
egida Academiei R.S.R., vom menţiona o sesiune la nivel
naţional de cibernetică şi sisteme, apoi sesiunea anuală pe
teme de istorie şi filosofia ştiinţei (între altele, pe tema:
„Simetrie şi asimetrie”), iar la sfârşitul lunii, o întâlnire pe
probleme de inteligenţă artificială.
761

Solomon Marcus

Dar seria manifestărilor de acest fel a fost deschisă,
în acest sezon, de întâlnirea la nivel naţional de la Buşteni,
sub egida Ministerului Educaţiei şi Învăţământului şi
Uniunii Sindicatelor din învăţământ, ştiinţă şi cultură: Cel
de al ΙΙ-lea Simpozion Naţional de Creativitate şi Creaţie,
Documentele multiplicate (un program de 20 de pagini şi
două volume totalizând 438 de pagini dactilografiate la un
rând) au reunit comunicările unui mare număr de cadre
didactice şi specialişti; am simţit realul câştig al acestei
ediţii şi cred că pot rezuma în două puncte caracteristicile
acestei manifestări: accentuarea dialogului interdisciplinar
şi intensificarea referinţelor la realităţile româneşti actuale,
atât din învăţământ şi cercetare, cât şi din producţie. Tonul
a fost dat chiar de către vorbitorii din şedinţa plenară de
deschidere. Profesorul G. Rulea nu s-a mulţumit să
semnalizeze succesele Institutului Politehnic Bucureşti în
domeniul invenţiilor şi inovaţiilor, ci a identificat cu
fermitate carenţele care încă mai persistă: un insuficient
dialog între inginer şi economist (,,tehnologul se apropie
mai uşor de tehnologii din alte întreprinderi decât de
economiştii din propria sa întreprindere”), o anumită
timiditate în ceea ce priveşte transformarea ideilor în
invenţii, brevete, recunoaşterea oficială, în valorificarea
lor efectivă în producţie. Bogată în observaţii subtile, rod
al unei experienţe îndelungate, expunerea profesorului
Rulea ar merita o discuţie specială.
762

Rãni deschise

De o mare densitate de idei a fost expunerea care a
încheiat şedinţa plenară. Cu arta sa bine-cunoscută de
vorbitor în acelaşi timp profund şi agreabil, acad. prof.
Radu Voinea a reuşit să aducă în atenţia participanţilor o
extraordinară bogăţie de probleme, cu accentul nu pe
soluţii simpliste, ci pe marile dileme cu care este
confruntată activitatea creatoare, pe distincţiile de nuanţă
atât de necesare pentru evitarea erorii. Evocând o poezie a
lui Blaga, vorbitorul a subliniat superioritatea, în faţa
frumosului natural, a frumuseţii valorilor create prin
munca omenească. Opiniei curente, după care omul de
ştiinţă se luptă cu necunoscutul, profesorul Voinea i-a
adăugat, în replică, ideea luptei cu cunoscutul, luptă uneori
mai grea decât cea dintâi, deoarece are de înfruntat barajul
prejudecăţilor de tot felul, argumentul atât de molipsitor
al „bunului simţ”, care de atâtea ori este contrazis de
argumentul ştiinţific. Alte idei interesante: Înţelegerea unei
legi este relativ simplă, mai dificilă şi mai importantă este
sesizarea limitelor ei de aplicare, despre care nu se spune
mai nimic în memoratoarele curente (oare nu dintr-o atare
sesizare s-a născut teoria relativităţii?). Important nu e să
faci o mare descoperire, ci să-ţi dai seama că ai făcut-o. În
funcţionarea oricărui aparat de măsură se află măcar un
pic de teorie. A fost evocat Planck, după care o teorie nu
se afirmă atât prin argumente ştiinţifice, cât datorită
faptului că mor susţinătorii teoriei vechi. A fost discutată
763

Solomon Marcus

delicata problemă a valorii culturale a descoperirilor
ştiinţifice, în condiţiile în care teoriile şi ideile ştiinţifice
se schimbă mereu. Cultura nu este o sumă de bunuri, ci de
valori: din acest punct de vedere, au fost criticate
manualele şcolare, încă încărcate cu elemente descriptive.
M-am temut că marele număr de comunicări va
transforma simpozionul într-o suită de monologuri. Nu a
fost aşa. Cei mai mulţi participanţi au reuşit să fie scurţi,
dându-se astfel posibilitatea unor controverse fecunde
privind: dialectica descoperirii şi invenţiei în ştiinţă,
tehnică şi artă; strategiile dezvoltării creativităţii la copii
şi adolescenţi; rolul cercurilor ştiinţifice şi oportunitatea şi
natura sesiunilor de comunicări ştiinţifice, în diferite etape
ale învăţământului; procedee de stimulare a inventivităţii
în domeniul tehnologic; metodologii noi ale cercetării
colective interdisciplinare; contextul psiho-social al
creativităţii. Au fost aduse în atenţie experienţe deosebit
de interesante în stimularea creativităţii studenţilor (prof.
Marina Mureşanu-Ionescu, de la Universitatea din Iaşi), a
fost accentuată noutatea sub aspect filosofic pe care o aduc
noile teorii fizice (prof. Ionel I. Purica). Deosebit de
semnificativă mi se pare prezenţa unui mare număr de
comunicări elaborate de colective pluridisciplinare de la
Cluj-Napoca, Timişoara, Craiova, Bucureşti etc. N-au
lipsit nici unele colaborări studenţeşti, dar am fi vrut ca
numărul lor să fie mai mare. Prof. Ion Cuculescu (de la
764

Rãni deschise

Facultatea de Matematică, Bucureşti) a adus în atenţie
experienţa sa impresionantă în stimularea creativităţii celor
mai buni tineri matematicieni, în cadrul olimpiadelor
internaţionale de matematică. Pedagogii, psihologii,
filosofii şi politologii au venit cu vasta lor experienţă de
cercetare, care s-a articulat fericit cu experienţa personală
a celorlalţi cercetători.
Ar fi bine ca cel de al ΙΙΙ-lea Simpozion, care va avea
loc peste doi ani, să preia întreaga experienţă a primelor
două simpozioane, să o ducă mai departe.

765

