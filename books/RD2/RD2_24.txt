Abordarea sistemică în ştiinţele sociale
Era socialistă, ianuarie 1978, nr. 1, pp. 29-30 şi 35-37
O recentă manifestare internaţională, care a avut loc
la Bucureşti, a readus în atenţie unul dintre demersurile
cele mai importante ale cercetării ştiinţifice actuale: teoria
sistemelor. Avem în vedere întâlnirea internaţională de
experţi desemnaţi de UNESCO, întâlnire având ca obiect
realizarea unui schimb de experienţă în domeniul teoriei
sistemelor şi al informaticii, mai cu seamă sub aspectul
aplicaţiilor în domeniile sociale şi umaniste*. Alegerea
temei se justifică prin rolul crescând pe care abordarea
sistemică îl are în cercetarea contemporană şi prin aceea
că studiul sistemelor care apar efectiv în ştiinţele naturii,
în inginerie, în ştiinţele sociale, în organizare şi planificare
comportă, de multe ori, prelucrarea unui mare număr de
date, care nu se poate face fără ajutorul calculatorului
electronic.
Problemele generale ale abordării sistemice nu pot fi
discutate cu folos de profesioniştii unei singure discipline;
ele au nevoie de schimbul de opinii între specialişti
provenind din domenii foarte variate. Dar o atare condiţie
este greu de îndeplinit la reuniunile ştiinţifice uzuale, care,
chiar dacă uneori îşi propun o abordare interdisciplinară,
190

Rãni deschise

nu reuşesc să strângă laolaltă slujitori ai unor ramuri atât
de variate ca matematica şi economia, informatica şi
studiul relaţiilor internaţionale, biologia şi ingineria,
filozofia şi prospectiva.
Iniţiativa UNESCO de a organiza această întâlnire la
Bucureşti a venit în întâmpinarea unei situaţii deosebit de
favorabile în ceea ce priveşte stadiul cercetărilor de teoria
sistemelor la noi în ţară. Aşa se şi explică alegerea
României ca gazdă a acestei întâlniri. O discuţie a ideilor
enunţate la această întâlnire este un bun prilej de a schiţa
stadiul actual al cercetării sistemice, impactul ei social. În
această perspectivă, atenţia principală o vom îndrepta
asupra unor contribuţii româneşti, pentru a desprinde
originalitatea şi ponderea lor socială şi culturală.
Precizăm, de la început, că nu avem pretenţia unei
priviri totale asupra preocupărilor româneşti de teoria
sistemelor. Varietatea acestor preocupări face dificilă
prezentarea lor de către o singură persoană, chiar din
punctul de vedere al informaţiei foarte ample de care ar fi
nevoie în acest scop. Dar experienţa în cadrul
preocupărilor noastre privind rolul sistemelor lingvistice
şi semiotice în ştiinţă şi în artă, ca şi în activitatea de la
secţia de studiul sistemelor a Universităţii din Bucureşti,
ne-a pus în contact cu o gamă foarte variată de preocupări
teoretice şi aplicative din acest domeniu, cărora vom
încerca să le desprindem semnificaţiile. După unele
191

Solomon Marcus

chestiuni cu caracter general, vom discuta două clase
speciale de sisteme studiate la noi în ţară: sistemele
economice şi sistemele de învăţare, trecând apoi în revistă
şi alte tipuri de sisteme (inginereşti, informaţionale,
educaţionale, lingvistice, ş.a.).
Noţiunea de sistem este definită în multe feluri. Dar
toţi cercetătorii sunt de acord că un sistem constituie o
entitate care nu poate fi redusă la simpla alăturare a
componentelor ei; sistemul constă, în primul rând, în
interdependenţa acestor componente. Încă în 1969,
S. Watanabe introdusese o măsură entropică a conexiunii
sau interdependenţei dintre subsistemele unui sistem. Dar
dinamica unui sistem include evoluţii entropice, ca şi
evoluţii antientropice. O evoluţie entropică constă într-o
creştere a incertitudinii, în timp ce una antientropică este
marcată de o descreştere a incertitudinii. Un sistem fizic
închis este guvernat de o evoluţie entropică (principiul al
doilea al termodinamicii), în timp ce sistemele din lumea
vie şi din societate au o evoluţie antientropică.
Dar nu e suficient să se descrie evoluţia întâmplătoare
a subsistemelor unui sistem; este necesar să se ia în
considerare, în măsurarea incertitudinii şi a interdependenţei componentelor unui sistem, atât probabilităţile
evenimentelor posibile, cât şi utilităţile ce li se atribuie
acestor evenimente. Silviu Guiaşu şi Mircea Maliţa au pus
şi au rezolvat această problemă cu ajutorul unei entropii
192

Rãni deschise

ponderate şi al unei măsuri ponderate a conexiunii
componentelor, ilustrând soluţia cu exemple elocvente din
studiul relaţiilor internaţionale (sistemul fiind aici un grup
de ţări legate prin anumite înţelegeri şi obligaţii reciproce).
Teoria matematică a informaţiei devine astfel un
instrument esenţial în teoria sistemelor. În aceeaşi
perspectivă, a conceperii teoriei sistemelor ca studiul
sistemelor de obiecte aflate în interacţiune, se plasează şi
cercetările lui P. Whittle de la Universitatea din
Cambridge, care se preocupă îndeosebi de sistemele cu
autoorganizare, de sistemele aflate în evoluţie şi de cele
guvernate de un control ierarhic. Recunoaştem aici tocmai
tipurile de sisteme care apar în lumea biologică şi în
organizarea socială; dar studiul lor se află abia la început.
O concepţie cuprinzătoare asupra teoriei sistemelor a
fost dezvoltată în ultimii ani de acad. Nicolae Teodorescu,
care vede în noţiunea de sistem una dintre cele patru
noţiuni de bază ale cunoaşterii contemporane (celelalte trei
fiind noţiunile de model, informaţie şi limbaj). Matematic,
sistemul este reprezentat aici ca o mulţime înzestrată cu o
structură de conexiune. Se definesc stările, intrările şi
ieşirile unui sistem, iar sub forma unor cazuri particulare
se regăsesc variate tipuri de sisteme de care se ocupă
diferite ştiinţe: sisteme dinamice, sisteme cibernetice,
sisteme automate, sisteme informaţionale, sisteme
informatice ş.a.m.d.
193

Solomon Marcus

Dar, oricare ar fi baza teoretică a investigaţiei, studiul
sistemelor sociale prezintă dificultăţi care nu trebuie să fie
escamotate. Karl G. Jöreskog, de la Universitatea din
Uppsala (Suedia), se referă, în această ordine de idei, la
numărul redus al unor experimente riguros controlabile; la
aceasta se adaugă dificultatea de a realiza în domeniul
social repetarea unei aceleiaşi experienţe (de exemplu,
pentru a decide dacă un model poate fi validat),
dependenţa de un număr foarte mare de variabile a celor
mai multe fenomene sociale, precum şi faptul că, în sfera
socială, măsurătorile au, în general, un caracter indirect şi
foarte aproximativ. Uneori se pot depăşi asemenea
dificultăţi, utilizându-se metode bazate pe aşa-numite
ecuaţii de structură. Pentru astfel de fenomene, fiecare
ecuaţie care intră în componenţa modelului exprimă o
legătură cauzală. Parametrii structurali reprezintă, într-un
atare model, trăsături relativ invariante şi independente ale
mecanismului care generează variabilele observabile.
Pentru a se putea depăşi simplele asociaţii empirice,
modelele de acest tip utilizează instrumente statistice
deosebit de puternice.
Variabilele observabile pot fi cantitative (deci
evaluabile numeric) sau calitative, deci apreciate cu
ajutorul unor proprietăţi care, în cazuri fericite, pot
conduce la o ordine ierarhică. Apar astfel mai multe tipuri
de modele:
194

Rãni deschise

Modele bazate pe variabile cantitative observabile
(deosebit de utile în econometrie);
Modele bazate pe variabile latente cu indicatori
cantitativi. Aici se inserează variabile ca „statut social sau
socioeconomic”, „motivaţie”, „atitudine” (necesare în
sociologie), „inteligenţă”, „abilitate verbală” ş.a. (necesare
în psihologie), „nivelul preţurilor”, „venitul permanent”,
elaborări sau „constructe ipotetice” (care intervin în
economie); deşi aceste variabile latente sau „constructe
ipotetice” nu sunt direct măsurabile, diferite aspecte ale lor
pot fi reflectate cu ajutorul altor variabile, acestea din urmă
putând fi măsurate nemijlocit.
Modelele în care unele sau toate variabilele sunt
calitative (fie ele lente sau direct observabile) pot foarte
puţin profita de metodologia statistică, aceasta fiind încă
insuficient dezvoltată pentru astfel de cazuri. Dar modelele
de acest tip sunt inevitabile dacă ţinem seama de faptul că,
în ştiinţele sociale, o bună parte a informaţiei este de natură
calitativă; într-adevăr, ea este obţinută deseori prin
chestionare, discuţii sau extrasă din descrieri conţinând
aprecieri mai degrabă calitative decât numerice.
Desigur, succesul modelelor care se elaborează în
studiul sistemelor sociale depinde în bună măsură de
caracterul mai mult sau mai puţin perfecţionat al tehnicilor
utilizate, de gradul lor de rigoare şi de posibilitatea de a le
da o formă algoritmică, pe baza căreia ele să poată fi
195

Solomon Marcus

programate la calculator. Gradul de adecvare a unui model
la realitatea socială pe care caută s-o reflecte este, în
acelaşi timp, o chestiune de opţiune filozofică şi
ideologică, în funcţie de care punctul de pornire al
modelului, componentele sale de bază, relaţiile care sunt
aduse în centrul atenţiei, interpretarea datelor iniţiale şi a
formei elaborate a modelului pot să se modifice în mod
substanţial. De exemplu, modelele macroeconomice
depind esenţial de faptul dacă se are în vedere un sistem
economic socialist sau unul capitalist. În alte cazuri,
modelele pot fi validate în orice tip de organizare socială,
cu condiţia schimbării unor detalii ale construcţiei şi a unor
aspecte de interpretare. Vom ilustra această situaţie cu o
clasă întreagă de microsisteme economice, cunoscute în
literatura de specialitate sub denumirea de „problema
voiajorului comercial”, probleme de triaje, de fire de
aşteptare sau probleme de ordonanţare.
Procesele menţionate sunt, toate, concretizate printr-o
evoluţie discretă şi au un număr finit de stări. Dar, evident,
„voiajorul comercial”, atât de caracteristic pentru
economia şi comerţul capitalist, îşi schimbă aşa de mult
funcţiile în sistemul socialist încât însăşi denumirea de
„voiajor” dispare. Înseamnă oare aceasta că modelul
matematic corespunzător îşi pierde valabilitatea?
Răspunsul este negativ. Modelele matematice au, între
altele, calitatea de a nu fi prea mult dependente de
196

Rãni deschise

conţinutul fenomenelor la care se referă; iar în măsura în
care această dependenţă există, ea se exprimă în modificări
care nu alterează esenţial arhitectura modelului. De
exemplu, în comerţul socialist se răspândeşte procedeul
caselor de comenzi la care cumpărătorii pot comanda prin
telefon diferite mărfuri ce urmează să le fie aduse la
domiciliu. Datele prin care diferă acest mod de servire a
publicului de un mod oarecum corespunzător în comerţul
capitalist nu ating structura, ci numai detaliile modelului
matematic asociat.
Dar avantajul modelării matematice a microsistemelor social-economice sau socioculturale nu se opreşte
aici. Modelarea matematică permite să se aducă într-o
albie comună diferite activităţi sociale, economice,
culturale care la prima vedere par foarte diferite.
Să considerăm, de exemplu, următoarele activităţi:
a) strângerea, cu ajutorul unui camion poştal, a scrisorilor
introduse în cutiile poştale din anumite puncte bine
determinate; b) deplasarea unei biblioteci rulante în câteva
cartiere sau localităţi mai mici; c) colectarea, cu ajutorul
unor camioane, a deşeurilor din anumite cartiere;
d) activitatea unei caravane cinematografice care urmează
să se oprească în anumite puncte de aglomerare umană;
e) activitatea unui turist care-şi propune să viziteze
anumite puncte turistice; f) activitatea unui inspector care
are de controlat anumite unităţi economice. Toate aceste
197

Solomon Marcus

activităţi pot fi reduse la o schemă comună, aceeaşi ca în
cazul unei case de comenzi: este vorba despre un punct
mobil care are de vizitat anumite puncte fixe. Organizarea
orarului acestor vizite urmează să ţină seama de anumite
restricţii: timpul care se scurge între două vizite în acelaşi
punct trebuie să nu fie nici prea scurt, nici prea lung, iar în
unele cazuri trebuie să se încadreze între anumite ore
convenabile pentru punctul mobil, ca şi pentru punctele
fixe. O situaţie mai deosebită o prezintă cazul f, în care
natura activităţii impune crearea unor condiţii de surpriză
în ceea ce priveşte data vizitării unui anumit obiectiv.
În toate exemplele menţionate, problema care se pune
este aceea a organizării optime a itinerarului: cu un minim
de efort (uman, tehnic, financiar) şi într-un timp cât mai
scurt să fie realizată vizitarea unui număr cât mai mare de
puncte fixe. Acestea sunt numai o parte din clasa foarte largă
a aşa-numitelor „probleme de optimizare”. Ele se complică
de îndată ce creşte numărul de parametri ce urmează să fie
luaţi în considerare. De exemplu, în cazurile de mai sus,
referitoare la organizarea unor itinerare, pot fi luate în
considerare durata deplasării de la un punct fix la altul şi
durata staţionării în fiecare punct fix; dar problema se poate
complica şi mai mult dacă se ţine seama şi de importanţa
fiecărui punct fix sau de consumul de carburant cerut de
deplasarea între diferitele puncte, consum care ar putea,
eventual, să depindă de starea drumurilor, de relief ş.a.m.d.
198

Rãni deschise

Am insistat asupra acestor exemple deoarece
cercetătorii români au obţinut soluţii interesante pentru
multe dintre problemele de organizare a itinerarelor.
O bună parte dintre aceste soluţii (bazate în special pe
metodele teoriei grafurilor) au o formă algoritmică, putând
fi deci uşor programate la calculator; de altfel, pentru unele
dintre ele au şi fost realizate asemenea programe. S-a
stabilit, în acest domeniu, o frumoasă colaborare între
profesori şi studenţi, între cercetătorii formaţi şi proaspeţii
absolvenţi. Chiar de curând s-a susţinut la Universitatea
din Bucureşti o teză de doctorat privind simularea unor
procese economice cu ajutorul teoriei automatelor. Totuşi,
doresc să menţionez că, deşi există în general numeroşi
beneficiari potenţiali ai acestor metode şi rezultate, ei ezită
încă să se transforme în beneficiari efectivi.
Contribuţiile cercetătorilor români nu se opresc aici.
Alături de problemele de optimizare au fost abordate
altele, privind evaluarea gradului de complexitate al unor
activităţi social-economice. Vom da două exemple. S-a
stabilit că activitatea de planificare a itinerarelor unui
inspector, bunăoară, este de un grad superior de
complexitate în raport cu stabilirea itinerarelor posibile ale
unui turist sau ale unui camion care colectează deşeuri.
Gradul de complexitate al unui proces de producţie
depinde de natura stocului de semifabricate, gradul fiind
mai coborât atunci când acest stoc nu depăşeşte o anumită
199

Solomon Marcus

limită. Activitatea unei staţii de triaj poate fi evaluată, de
asemenea, din punctul de vedere al complexităţii ei.
În toate aceste cazuri, se porneşte de la ipoteza
potrivit căreia la baza oricărei activităţi umane – fie ea
individuală sau de grup – se află anumite mecanisme
generative, a căror funcţionare are ca rezultat procesele
care decurg din activităţile considerate. Care este motivaţia
acestei ipoteze de o mare capacitate explicativă?
Răspunsul se află, probabil, în acea ramură a teoriei
sistemelor care în urmă cu vreo trei decenii a revoluţionat
cunoaşterea umană: cibernetica. Într-adevăr, în studiul unui
sistem cibernetic atât de complex cum este creierul uman,
cibernetica a adoptat faimoasa strategie a „cutiei negre”,
strategie conform căreia activitatea unui sistem poate fi
studiată fără a se cunoaşte anatomia sa, cu condiţia de a se
observa cu atenţie modul său de funcţionare, aşa cum
rezultă el din compararea intrărilor şi ieşirilor sistemului.
Dar simularea funcţionării sistemului revine tocmai
la imaginarea unei „maşini” ale cărei activităţi au un
rezultat asemănător celui produs de sistemul în discuţie.
Aceste „maşini imaginare” prezintă o mare însemnătate
practică, deoarece activitatea lor are un caracter algoritmic,
putând fi uşor programată la calculator. Însă problema
studiată are şi o deosebită semnificaţie teoretică, deoarece
permite să se distingă, în orice activitate umană,
individuală ori socială, aspectul competenţei de cel al
200

Rãni deschise

performanţei. Competenţa are un caracter ideal, potenţial,
în timp ce performanţa cuprinde numai realizările efective.
Din punctul de vedere al competenţei, un camion poate
colecta deşeuri la infinit, repetând mereu acelaşi ciclu de
operaţii; dar din punctul de vedere al performanţei el
trebuie să se oprească după un timp finit, deoarece şoferul
oboseşte, camionul trebuie alimentat cu carburant ş.a.m.d.
Distincţia dintre competenţă şi performanţă este
fundamentală în psihologia modernă şi în modul în care se
încearcă să se explice funcţionarea sistemelor de învăţare
individuală sau socială. Multă vreme, învăţarea a fost
văzută numai ca un proces de adaptare, neglijându-se
aspectul ei creator şi prospectiv. Ca urmare a acestui fapt,
dintre cele două laturi, în realitate solidare, care definesc
complexitatea procesului de învăţare – competenţa şi
performanţa – a fost reţinută în special cea de-a doua
latură. Acum, sub impresia noilor tehnologii
psiholingvistice, unii cercetători tind către o exagerare în
sens invers, reducând învăţarea la o dezvoltare a
competenţei umane. Dar orice teorie cuprinzătoare a
învăţării trebuie să ia în considerare atât competenţa, cât
şi performanţa, atât creaţia, cât şi rutina.
În momentul de faţă, în întreaga lume se depune un
efort impresionant pentru descifrarea mecanismelor
sistemelor de învăţare. La iniţiativa profesorului Mircea
Maliţa de la Universitatea din Bucureşti, Clubul de la
201

Solomon Marcus

Roma procedează acum la elaborarea, cu participare
internaţională, a unei ample lucrări, care aşază învăţarea
printre cele câteva paradigme ale dezvoltării
contemporane. La această lucrare, contribuţia românească
va fi esenţială.
Ţara noastră are frumoase tradiţii în cercetarea
problemelor învăţării. Încă în 1935, profesorii Octav
Onicescu şi Gheorghe Mihoc au elaborat teoria lanţurilor
cu legături complete, care avea să devină ulterior baza
teoriei matematice a învăţării; această teorie a fost ulterior
dezvoltată esenţial şi de către noile generaţii de probabilişti
români (George Ciucu, Silviu Guiaşu, dar mai ales Marius
Iosifescu).
Problemele generale ale abordării sistemice nu pot fi
discutate cu folos de profesioniştii unei singure discipline;
ele au nevoie de schimbul de opinii între specialişti
provenind din domenii foarte variate.
Secţia de studiul sistemelor a Universităţii din
Bucureşti are un contract de cercetare cu Consiliul
Naţional pentru Ştiinţă şi Tehnologie, privind tocmai
studierea sistemelor de învăţare şi a aplicaţiilor lor. La
aceste eforturi interdisciplinare colaborează matematicieni
şi psihopedagogi, economişti şi ingineri, viitorologi,
lingvişti şi informaticieni.
Studiul sistemelor de învăţare prezintă interes şi din
alt punct de vedere, acela al raportului dintre concepţia de
202

Rãni deschise

bază şi tehnicile utilizate, raport despre care am vorbit ceva
mai sus. Teoria matematică a învăţării s-a dezvoltat iniţial
ca o teorie probabilistă, în cadrul căreia învăţarea era
reprezentată ca un proces stohastic, sub forma unor probe
succesive la care este supus un subiect aflat sub
supravegherea unui profesor sau experimentator, cu sau
fără intervenţia acestora din urmă. De-a lungul şirului de
probe, subiectul este supus unei configuraţii de stimuli,
alese dintr-o mulţime de astfel de configuraţii, definite prin
caracteristici controlate de experimentator. Subiectul dă
apoi un răspuns, care constituie unul dintre
comportamentele sale posibile.
Datorită caracterului foarte perfecţionat al instrumentului matematic folosit, teoria probabilistă a învăţării a
obţinut frumoase succese în studiul sistemelor de învăţare
în care predomină învăţarea din experienţă. Dar un
asemenea mod de abordare escamota cealaltă latură a
proceselor de învăţare, privind elementele moştenite. Orice
proces de învăţare este rezultatul interferenţei a doi factori,
cel înnăscut (deci aparţinând speciei, eredităţii) şi cel
dobândit, constând în experienţa acumulată în cursul
existenţei. Aceste două componente ale oricărui sistem de
învăţare real se completează şi se condiţionează reciproc,
după o dialectică încă destul de puţin cunoscută în detalii,
potrivit căreia experienţa nu poate deveni învăţare în absenţa
mecanismelor creative ale creierului, iar creativitatea
203

Solomon Marcus

rămâne numai virtuală, deci nu se poate actualiza, fără
contactul cu experienţa.
Un asemenea mod de reprezentare a sistemelor de
învăţare, deşi anticipat de multă vreme, a căpătat un statut
mai convingător şi mai explicit abia în ultimii douăzeci de
ani, sub influenţa noilor teorii privind învăţarea limbii. Acest
lucru nu trebuie să mire; învăţarea limbii materne este
procesul fundamental de învăţare, ea fiind prototipul tuturor
celorlalte procese de învăţare, individuală sau socială.
Tocmai studiindu-se cadrul acestui proces de
actualizare a mecanismelor lingvistice ale creierului s-a
putut contura mai bine distincţia dintre competenţă şi
performanţă, extinzându-se, apoi, aceasta la toate
procesele de învăţare. Orice învăţare revine, în esenţă, la
deprinderea unui limbaj, chiar dacă acest fenomen este de
multe ori ascuns. Aşa se şi explică importanţa
considerabilă a limbajelor artificiale, care ne permit azi să
dialogăm cu calculatoarele electronice, iar mâine poate ne
vor înlesni să intrăm în legătură cu eventuale civilizaţii de
pe alte corpuri cereşti.
Dar teoria probabilistă a învăţării neglija componenta
înnăscută, îndreptându-şi atenţia exclusiv asupra învăţării
experimentale. Abia în 1973, o altă teorie matematică a
învăţării, elaborată de cercetători japonezi şi bazată în
special pe topologie şi pe teoria automatelor, propunea un
model nou al sistemelor de învăţare, în cadrul căruia se
204

Rãni deschise

restabilea echilibrul dintre factorii înnăscuţi şi cei
dobândiţi. Cuprinzătoare în punctul de pornire, această
nouă teorie este încă puţin dezvoltată în detalii. Joncţiunea
ei cu teoria probabilistă a învăţării va fi, cred,
binefăcătoare pentru înţelegerea mai profundă a sistemelor
de învăţare.
În strânsă legătură cu sistemele de învăţare, au fost
studiate cele educaţionale (Dragoş Vaida, Cornel
Zidăroiu). Dar numeroase alte clase de sisteme au făcut,
de asemenea, obiectul cercetărilor recente: sistemele
biologice, sistemele inginereşti, sistemele lingvistice şi
semiotice, sistemele de relaţii internaţionale, sistemele
prospective, ş.a. De cele mai multe ori, apare o interferenţă
între tipuri diferite de sisteme. Astfel, în studiul sistemelor
inginereşti (menţionăm cu precădere cercetările dr. Adrian
Gheorghe, de la Institutul politehnic din Bucureşti) –
folosindu-se metode dintre cele mai variate, care merg de
la procesele de decizie la sistemele parţial observabile şi
de la programarea matematică la analiza booleană – s-au
putut aprofunda chestiuni foarte diferite ca natură, cum ar
fi ingineria nucleară şi diagnoza medicală. O adevărată
premieră metodologică au constituit-o cercetările
româneşti privind sistemele sociologice (având ca
precursor pe Spiru Haret şi evoluând azi spre studiul
sistemelor de strategie diplomatică sau militară, de
tratative şi relaţii internaţionale) şi cele prospective (este
205

Solomon Marcus

unanim recunoscută valoarea cercetătorilor români în acest
domeniu).
Sistemele bazate pe mulţimi imprecise au fost
studiate de dr. C. V. Negoiţă. Sistemele informaţionale au
beneficiat de conceptele noi introduse de acad. Octav
Onicescu – cum sunt „energia informaţională” şi „corelaţia
informaţională” – care au permis să se aprofundeze
analogia dintre sistemele informaţionale şi cele
termodinamice; aplicaţiile acestor noi parametri
informaţionali s-au dovedit deosebit de semnificative în
economie şi în folclor, în etnografie şi în literatură, în
urbanistică şi în demografie. Sistemele cibernetice au fost
studiate de profesorul Edmond Nicolau.
O discuţie specială ar merita-o sistemele economice;
raporturile om-mediu au beneficiat la noi în ţară de
cercetări dintre cele mai variate, mergând de la cele ale
medicilor (acad. Ştefan Milcu, dr. Victor Săhleanu) până
la cele ale inginerilor (Mihai Dinu). Teoria sistemelor
dinamice şi teoria controlului optimal (atât de importantă
pentru buna funcţionare a celor mai variate sisteme) au fost
adâncite de prof. A. Halanay şi colaboratorii săi, la
Bucureşti, şi prof. C. Corduneanu şi şcoala sa, la Iaşi.
Sistemele informatice s-au bucurat de o atenţie
deosebită la Centrul de calcul al Universităţii din
Bucureşti, unde, sub îndrumarea acad. Nicolae Teodorescu
şi a conf. dr. Ion Văduva, s-au elaborat un limbaj de
206

Rãni deschise

simulare algoritmică şi diferite modele de simulare
algoritmică privind optimizarea transportului subteran
într-o mină, sistemele de irigare, estimarea rezervelor
geologice, precum şi numeroase probleme de inventariere
sau de aşteptare, de clasificare, de diagnoză medicală.
Cercetări interesante privind elaborarea sistemelor
informatice se desfăşoară şi la Institutul Central pentru
Conducere şi Informatică, la Centrul de calcul
al Academiei de Ştiinţe Economice şi în alte centre de
calcul din ţară, precum şi la catedra de informatică
de la Academia „Ştefan Gheorghiu” (prof. Paul
Constantinescu). În momentul de faţă, secţia de
matematică de la INCREST se află şi ea angajată în unele
cercetări aplicative importante, iar la Întreprinderea de
prospecţiuni geologice au loc cercetări interdisciplinare,
cu o bază matematică deosebit de modernă. Importanţa
socială a acestor cercetări este imensă.
Deosebit de îmbucurător este faptul că la toate aceste
cercetări un rol de seamă, adesea decisiv, îl au cercetătorii
tineri, uneori chiar studenţi sau absolvenţi aflaţi încă în
perioada de stagiu. Practica productivă, cercurile ştiinţifice
studenţeşti, lucrările de diplomă şi de specializare,
pregătirea doctoranzilor şi a tezelor de doctorat şi, ca o
încununare a tuturor acestor modalităţi, politica generală
de colaborare organică a învăţământului cu cercetarea şi
cu producţia şi-au manifestat efectul lor binefăcător. Astfel,
207

Solomon Marcus

pentru a da numai câteva exemple, vom menţiona că
studiul sistemelor juridice, din punctul de vedere al
posibilităţilor de automatizare, a fost dezvoltat la noi în
ţară (din iniţiativa acad. Gr. C. Moisil) de către doctorandul
Cristian Calude; prima aplicare a teoriei automatelor la
sistemele economice a fost efectuată de doctorandul
Gheorghe Păun; dr. Luminiţa State şi dr. Monica Bad au
primit distincţii la Balcaniada de la Belgrad, destinată
tinerilor cercetători, pentru rezultatele obţinute în studiul
sistemelor aleatoare; Gabriela Ghioca a inţiat aplicarea
teoriei automatelor în studiul sistemelor arhitecturale, iar
Monica Scorţaru a aplicat aceeaşi teorie în praxiologie
(activitatea de conducere a automobilului); Vasile Drăgan
s-a distins în studiul problemelor de control optimal, iar
Alexandru Dincă, Octavian Bîscă şi Eva Zsido au adus
contribuţii interesante în studiul sistemelor de
documentare automată şi al sistemelor lingvistice.
Sistemul etnografic al caselor ţărăneşti de lemn a fost
studiat cu metodele teoriei matematice a clasificării de
Anca Manolescu, iar tinerii cercetători Marian Drăghici,
Ana Maria Sandi, Cristina Patraulescu, Mihaela Maliţa şi
Rodica Ceterchi (matematicieni), Ileana Ionescu Siseşti
(economist) şi Adrian Vasilescu (inginer) se află angajaţi
la Secţia de teoria sistemelor, în contracte importante
privind sistemele de învăţare şi sistemele prospective. Să
mai menţionăm studiile privind sistemul comunicării de
208

Rãni deschise

masă la care, alături de cercetători bine cunoscuţi ca prof.
Al. Tănase, I. Pascadi, H. Culea, P. Cîmpeanu, lucrează
tineri cercetători ca Gina Stoiciu.
Atenţia deosebită pe care partidul o acordă
problemelor de conducere şi organizare la nivelul
întreprinderilor şi instituţiilor, ca şi la nivelurile superioare,
importanţa unei circulaţii cât mai operative a informaţiei
atât orizontal, între unităţi de acelaşi rang, cât şi vertical,
între unităţi aflate în raporturi ierarhice, îndrumarea şi
controlul permanent al activităţii găsesc, în abordarea
sistemică, o importantă fundamentare ştiinţifică.
Cercetarea sistemică nu este o activitate de cabinet. Ea este
chemată să promoveze o mentalitate nouă, care să
stăpânească pe toţi cei care reprezintă o verigă – fie ea cât
de modestă – în mecanismul atât de complex al unei
societăţi planificate, ai cărei membri îşi asumă
răspunderile ce decurg din condiţionările şi interdependenţele la care participă.

209

