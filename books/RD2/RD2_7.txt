Procesul de matematizare a ştiinţei
Familia, anul 5 (106), nr. 6 (46), iunie 1969, p. 6
Procesul de matematizare a ştiinţei a fost discutat
într-un mare număr de articole şi cărţi. Raportându-ne
numai la perioada ultimilor douăzeci de ani, în ţara noastră
s-au publicat peste 300 de articole pe această temă. Din
păcate, o foarte mare parte a acestor articole nu face decât
să regăsească, eventual în nuanţe noi, consideraţii deja
făcute în unele articole precedente. Există mai multe cauze
ale acestei situaţii. Mai întâi, articolele de această natură
apar în publicaţii dintre cele mai variate, ceea ce face foarte
dificilă urmărirea lor. Autorii unor astfel de articole nu
cunosc decât o foarte mică parte din articolele pe teme
similare, publicate anterior. Nicio revistă de referate nu
centralizează semnalarea şi recenzarea acestor articole. De
exemplu, revistele de referate din domeniul lingvisticii
semnalează numai articolele care privesc aplicarea
matematicii în lingvistică şi aceasta numai în măsura în
care aceste articole apar în publicaţii care intră, de obicei,
în raza de observaţie a revistelor respective. Deci, chiar
unei reviste de referate din domeniul lingvisticii, îi scapă
o parte considerabilă din literatura consacrată procesului
de matematizare a lingvisticii.
76

Rãni deschise

La aceste dificultăţi de natură obiectivă se adaugă şi
faptul că majoritatea autorilor care abordează problemele
generale ale procesului de matematizare ţin nu atât să
întreprindă o cercetare a problemei, cât să împărtăşească
o experienţă proprie. Chiar pe plan mondial, nu dispunem
de un bilanţ – fie el cât de aproximativ – al literaturii
privind procesul de matematizare. La acest domeniu nu
există încă o cercetare propriu-zisă, există doar observaţii
şi mărturii disparate – unele deosebit de pătrunzătoare.
Există trei categorii mai importante de persoane care
scriu articole pe teme privind procesul de matematizare:
matematicienii, filosofii şi specialiştii în domeniile care
solicită metode matematice (am putea adăuga şi a patra
categorie, aceea a ziariştilor, de care însă nu ne vom ocupa
aici). Fiecare din aceste categorii prezintă un profil bine
conturat al modului de abordare a problemei de care ne
ocupăm. Matematicianul are tendinţa de a justifica
adoptarea, în procesul de modelare a celor mai variate
fenomene, a acelor tipuri de structuri care există deja în
matematică şi în legătură cu care s-au obţinut numeroase
rezultate. Această tendinţă e firească şi în bună măsură
pozitivă, deoarece, cu cât este mai dezvoltată teoria în
termenii căreia se elaborează modelul, cu atât vor fi mai
bogate consecinţele logice ale modelului adoptat, deci
informaţia pe care el o furnizează va fi mai amplă. Astfel,
când pentru un fenomen economic sau sociologic adoptăm
77

Solomon Marcus

un model elaborat în termenii teoriei grafurilor, acest
model va profita de întreaga bogăţie de noţiuni şi rezultate
existente în teoria grafurilor.
Totuşi, tendinţa amintită nu este lipsită de riscuri,
deoarece poate duce – şi efectiv aceasta se întâmplă uneori
– la încercări de a potrivi fenomenul studiat într-un „pat al
lui Procust”, de a-i ignora trăsăturile, în măsura în care ele
nu pot fi descrise cu ajutorul structurilor matematice
cunoscute. Matematicianul urmăreşte dialectica internă a
procesului de modelare matematică, menţinându-se
necontenit în imediata apropiere a acestui proces,
urmărindu-i cutele cele mai fine. Filosoful este, în schimb,
tentat să se desprindă cât mai repede de aspectele tehnice
ale procesului de matematizare, pentru ca, degajând câteva
trăsături generale ale acestui proces, să caute a le raporta
la unele categorii generale ale filosofiei, la unele curente,
concepţii şi metodologii cu un statut recunoscut în
domeniul gândirii filosofice. În ceea ce-i priveşte pe
specialiştii nematematicieni, ei manifestă tendinţa unei
modelări cât mai cuprinzătoare şi mai complexe, care
aspiră la exhaustivitate, aspiraţie care, evident, nu poate fi
realizată, deoarece modelul, prin însăşi natura sa,
constituie doar o aproximaţie a obiectului. Matematica
este, într-adevăr, domeniul privilegiat al exactităţii, dar ne
permitem să adăugăm că substantivul pe lângă care
adjectivul exactă este aşezat în modul cel mai firesc, în
78

Rãni deschise

ceea ce priveşte modelarea matematică, este acela de
aproximaţie. Modelarea matematică este domeniul
aproximaţiilor exacte; aproximaţii, pentru că modelele
reţin doar unele aspecte ale fenomenelor modelate; exacte,
pentru că aceste modele sunt construcţii matematice. În
acelaşi timp, nematematicianul este tentat să identifice, în
fenomenul modelat, structuri cât mai originale, pe care cu
greu acceptă să le recunoască izomorfe cu structuri
matematice deja apărute în studiul altor fenomene, aidoma
acelor clienţi exigenţi, care preferă un costum de comandă
unuia gata confecţionat. Specialistul nematematician are
tendinţa de a accentua caracterul singular, de unicat, al
fenomenului pe care-l studiază, în timp ce matematicianul
are tendinţa de a insera fenomenele în clase de fenomene
izomorfe; meseria matematicianului este, după cum spune
Banach, accea de a stabili analogii.
Fără îndoială că absolutizarea oricăreia dintre
tendinţele de mai sus este dăunătoare. Marea varietate de
structuri existente în matematica modernă răspunde, în
bună măsură, la necesităţile activităţii de modelare a lumii
reale, dar există numeroase fenomene a căror modelare
reclamă introducerea unor structuri matematice noi sau,
cel puţin, a unor noţiuni şi problematici inedite, în cadrul
unor teorii matematice deja existente. Economia
matematică a îmbogăţit problematica teoriei grafurilor, iar
lingvistica matematică şi teoria automatelor au determinat
79

Solomon Marcus

o problematică nouă în algebră, în special în teoria
semigrupurilor libere.
Filosofii şi, în general, specialiştii nematematicieni
sunt foarte preocupaţi de problema limitelor procesului de
modelare matematică. Venind parcă în replică la
îngrijorarea acestora, matematicienii obişnuiesc să
amintească, uneori, cu oarecare înfumurare, pe acei
oameni mari care au afirmat că o disciplină este ştiinţifică
în măsura în care ea se matematizează. Aici, poziţiile sunt
încă foarte depărtate. Orice ştiinţă este permanent
alimentată cu fapte ale lumii intuitive, care formează
materia ei primă, izvorul ei de prospeţime şi viaţă; tocmai
acest flux permanent dinspre lumea reală spre lumea
constructelor ştiinţifice explică imposibilitatea formalizării
integrale a unei ştiinţe. Însă unii gânditori denaturează
semnificaţia acestei situaţii, căutând să deducă de aici
existenţa unor limite imanente, apriorice, ale procesului de
matematizare, existenţa unor aspecte care nu vor putea fi
niciodată matematizate, uitând că matematica însăşi este
în continuă evoluţie, absolutizând deci unele limite care
ţin de stadiul de dezvoltare, la un anumit moment al
ştiinţei. Caracterul inepuizabil al modelării matematice
constă tocmai în faptul că orice limită atinsă în modelare
este susceptibilă de a fi depăşită; despre niciun aspect al
unui fenomen nu se poate afirma că el nu va intra niciodată
în raza modelării matematice; aceasta este viziunea
80

Rãni deschise

dinamică şi dialectică a evoluţiei ştiinţei, inclusiv a
matematicii. În principiu, putem admite că fiecărui aspect
îi corespunde un moment la care întreaga ştiinţă să fie
formalizată.
Matematizarea este un indiciu al măsurii în care o
ştiinţă şi-a degajat aspectele structurale, formale,
funcţionale, dar eficacitatea unei ştiinţe se apreciază nu
numai după aceasta, ci şi după gradul ei de adecvare la
fenomenele studiate, adecvare pe care matematica o
realizează de obicei prin intermediul altor ştiinţe. Nu se
poate vorbi deci de o ierarhie de importanţă a ştiinţelor,
care ar avea în vârf matematica; ştiinţele sunt
necomparabile ca importanţă, fiecare este faţă de celelalte
şi deasupra şi dedesubt.
Ceea ce complică însă considerabil – dar nu
iremediabil – depăşirea dificultăţilor este absenţa
dialogului (înlocuit fie prin monologuri paralele, fie prin
discuţii la un grad de generalitate destul de ridicat, pentru
ca chestiunile controversate să fie cât mai bine escamotate)
şi evoluţia deosebit de rapidă a fenomenelor
interdisciplinare. Volumul Matematica şi cunoaşterea
ştiinţifică, apărut în 1967 la Editura ştiinţifică şi
înmănunchind articolele elaborate de matematicieni în
cadrul „Seminarului de probleme filosofice ale
matematicii”, de sub conducerea prof. M. Neculcea, nu
numai că a rămas fără nicio replică din partea
81

Solomon Marcus

nematematicienilor, dar nu s-a bucurat nici măcar de o
recenzie din partea acestora.
Acum douăzeci şi cinci de ani existau aplicaţii ale
matematicii în lingvistică, dar nu exista încă lingvistica
matematică; aceasta din urmă s-a născut abia în urmă cu
vreo zece ani, ea însemnând o stare de spirit nouă, care nu
se obţine deci prin simpla alăturare a viziunii matematice
şi a viziunii lingvistice. Astăzi însă nu mai este vorba de
simple domenii interdisciplinare bilaterale, ci de
interferenţe de complexitate multiplă, în cadrul cărora
viziunea matematică funcţionează adeseori ca un
catalizator. Nu este locul aici să discutăm în ce fel, de
exemplu, domenii ca psihologia, genetica, poetica, studiul
afaziei, teoria automatelor, teoria informaţiei, teoria
gramaticilor, termodinamica, logica matematică şi
pedagogia au interferat, dând naştere unui domeniu sui
generis, căruia nu i s-a găsit încă nici măcar un nume
provizoriu – şi a cărui armătură este matematica. Pentru
că, de exemplu, izomorfismul atât de interesant astăzi între
genetică şi lingvistică nu a putut fi identificat decât prin
intermediul matematicii. Este aici o privelişte măreaţă, dar
şi înfricoşătoare; poate că o perioadă de semidiletantism
necesar, inevitabil, ni se întinde în faţă, deoarece pe harta
cunoaşterii apar ţări noi, neexplorate, pe care trebuie totuşi
să încercăm să le cucerim, iar învăţământul
interdisciplinar, de natură să formeze pe locuitorii
82

Rãni deschise

îndreptăţiţi ai acestor ţinuturi, se dezvoltă cu încetineală
de melc. Ce teren prielnic pentru apariţia adevăraţilor
diletanţi şi chiar a diletanţilor-escroci!
Comoditatea cu care unii matematicieni refuză să
înţeleagă că nu vor putea fi cu adevărat folositori altor
specialişti decât dacă vor învăţa ceva – cât de puţin – din
meseria acestora, dacă vor încerca să înţeleagă măcar o
parte din felul lor de a vedea; ignoranţa multora dintre ei
în ceea ce priveşte problematica generală a
structuralismului, care reprezintă, pentru un număr mare
de discipline, anticamera matematizării lor, dar care s-a
dezvoltat, de multe ori, din surse foarte depărtate de o
adevărată viziune matematică (a se vedea, de exemplu,
antropologia structurală a lui Claude Lévi-Strauss);
ignorarea faptului că matematica tinde şi ea să solicite alte
discipline şi nu numai să fie solicitată de acestea (şi poate
că nu e departe ziua în care un cercetător al limbajelor de
programare va avea nevoie de cunoştinţe complexe de
lingvistică structurală); snobismul uneori, candoarea
altădată, cu care atâţi nematematicieni se prevalează de
eticheta unor teorii matematice moderne, în timp ce ei le
ignoră fondul şi nu folosesc decât câteva biete cuvinte sau
notaţii întâlnite în primele zece pagini ale unei cărţi de
matematică – şi aceea de popularizare – şi uneori nici
măcar atât; persistenţa atâtor nematematicieni – inclusiv
filosofi – într-un structuralism de tip eseistic, în care
83

Solomon Marcus

aluziile la matematizare sunt cât mai puţine şi flancate de
cât mai multe limite; această ambiţie de a salva aparenţele,
această goană fără nicio convingere personală după ceea
ce pare a fi la modă, toate aceste fenomene supărătoare,
dar amuzante, nu vor putea, evident, împiedica dezvoltarea
adevăratului proces de matematizare, care este în realitate
mai lent şi mai puţin spectaculos decât ar putea să pară la
prima vedere.

84

