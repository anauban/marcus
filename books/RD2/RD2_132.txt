Semiotica şi sistemul indicatorilor sociali,
câteva puncte de reper
Studii şi Cercetări de Calcul Economic şi Cibernetică
Economică, anul XXII, 1987
Studiile dedicate indicatorilor sociali se remarcă, pe
de o parte, prin numărul lor mare, pe de altă parte,
printr-o oarecare lipsă de varietate. Sunt abordate mai ales
aspectele economice, monetare, demografice şi mai puţin
cele ale bunăstării. Or, în ultimii ani, un interes din ce în
ce mai mare este arătat unor indicatori paneconomici cum
sunt: nivelul de viaţă, calitatea vieţii, dezvoltarea socială
şi umană, schimbarea socială. Din păcate însă, această
lărgire a ariei de interes nu a fost însoţită sau măcar urmată
de o amplificare corespunzătoare a cercetărilor teoretice şi
metodologice cu privire la indicatori. Cele mai multe
dintre date sunt disponibile numai sub formă statistică şi,
deci, se referă doar la aspecte cantitative. Dar statisticile,
aşa cum se ştie prea bine în matematică, sunt deseori
înşelătoare, derutante, pot deveni un instrument de
manipulare. Cei mai mulţi indicatori sunt produşi în
exclusivitate de către factori de decizie, de persoane sau
organizaţii internaţionale. Aceasta face ca mulţi indicatori
să fie prezentaţi într-un jargon specific, într-o formă
928

Rãni deschise

tehnică, uneori neinteligibilă pentru cei mai mulţi
nespecialişti. Astfel, indicatorii sociali ridică importante
probleme de comunicare, care însă nu au fost niciodată
abordate într-un mod sistematic, explicit. Mai mult chiar,
în majoritatea cercetărilor din domeniul indicatorilor
sociali se poate lesne observa o oarecare lipsă de
generalitate, de perspectivă teoretică, de cadru
metodologic. Eterogenitatea procedeelor, caracterul ad-hoc
al multor investigaţii fac dificilă legarea diferitelor fapte
individuale de unele ipoteze şi concepte fundamentale, fac
dificilă obţinerea de evaluări comparative, precum şi
organizarea indicatorilor în diverse tipuri de sisteme.
Un program posibil pentru o cercetare ca cea schiţată
mai sus ar cuprinde, între altele, următoarele puncte:
1. De ce ne trebuie indicatori? Indicatorii sunt
reclamaţi de însăşi
complexitatea vieţii sociale, de caracterul din ce în ce
mai ascuns al multora dintre aspectele acesteia, de gradul
scăzut de efectivitate al unora dintre ele, de necesitatea de
a face realitatea socială mai inteligibilă, mai operaţională.
2. Indicatori despre ce? Indicatorii se pot referi la
obiective, procese, stări de lucruri, tendinţe etc. Ei se pot
referi fie la situaţii deja existente, fie la fenomene
plauzibile, probabile în viitor.
3. Indicatori pentru cine? Există o mare varietate de
destinaţii pentru indicatori. Ei se pot adresa publicului,
929

Solomon Marcus

copiilor, factorilor de decizie, politicienilor, experţilor,
oamenilor de ştiinţă, intelectualilor, instituţiilor. Fiecare
dintre aceste categorii are un limbaj specific în care ar
trebui exprimaţi indicatorii. Aşa cum arată majoritatea
lucrărilor despre indicatori, nu există din păcate până acum
decât o slabă preocupare de a aduce indicatorii sociali la o
formă inteligibilă pentru marele public. Copiii, mai ales,
sunt oarecum neglijaţi. Manualele şi sistemul de
învăţământ ar trebui să se preocupe mai mult de acest
lucru.
4. Indicatorii sunt semne în sensul semiotic al
cuvântului: ceva ce înlocuieşte altceva pentru cineva,
într-o anumită privinţă, într-o anumită măsură. Prin
urmare, ei au o intensiune (înţelesul lor) şi o extensiune
(realitatea pe care o acoperă). De exemplu, pentru un
indicator cum ar fi procentul persoanelor alfabetizate,
extensiunea (obiectul) este realitatea materială a
persoanelor alfabetizate, în timp ce intensiunea
(interpretantul) este înţelesul creat de acest indicator şi de
obiectul său, de exemplu: cât de relevantă este această cifră
pentru nivelul educaţiei. Dacă pentru satisfacerea nevoilor
fundamentale (ca hrană, îmbrăcăminte, adăpost, sănătate,
educaţie), pentru creşterea economică sau pentru creşterea
socială (urbanizare, transport, comunicaţii), distincţia
dintre extensiune şi intensiune, dintre obiect şi interpretant,
se face relativ uşor, trebuie arătat că pentru alte nevoi
930

Rãni deschise

umane ca dragoste, creaţie, sens, sinergie (pentru a utiliza
clasificarea propusă de Mallmann, Nudler, Max-Neef),
intensiunea îşi caută încă extensiunea. Pe de altă parte, în
alte situaţii, ca de pildă în educaţia copiilor, pornim
totdeauna de la extensiunea indicatorului şi încercăm să
descoperim intensiunea corespunzătoare.
5. Indicatorii sunt elemente de bază în orice proces
de comunicare legat de dezvoltare. Acest lucru rezultă din
punctul 4 şi este comparabil cu rolul cuvintelor ca
elemente de bază ale unei limbi naturale. K. Valaskakis şi
I. Martin („Economic indicators and the GPID: An attempt
to bring economics back into the church without losing the
faith”, Working paper HSDR GPID-23/ UNUP-134, 1980)
consideră că indicatorii sunt pentru ştiinţă ceea ce limbajul
este pentru gândire. Indicatorii sunt partea vizibilă a
icebergului dezvoltării sociale.
6. Natura semiotică a relaţiei dintre indicator şi
obiectul său nu poate fi eludată. Un indicator poate avea o
legătură motivată sau o legătură nemotivată (simbolică) cu
obiectul său. Legăturile nemotivate sunt în general de
natură convenţională (de exemplu, indicatorii exprimaţi în
bani). Legăturile motivate sunt de două feluri: analogice,
în cazul indicatorilor iconici (diagrame, desene, imagini
ale unor aspecte ale realităţii având o relaţie de analogie
cu obiectul lor), şi de contiguitate, în cazul indicatorilor
indiciali, având o relaţie de contiguitate cu obiectul lor (de
931

Solomon Marcus

pildă, zâmbetul poate fi considerat un indicator indicial al
unei bune calităţi a vieţii).
Indicatorii iconici pot fi ştiinţifici (modele ale unor
procese) sau poetici (metafore). Indicatorii indiciali pot fi
o parte a procesului pe care îl exprimă, o aproximare, o
consecinţă sau o cauză a acestuia. Indicatorii cantitativi
sau statistici (de exemplu produsul naţional brut) sunt mai
degrabă indiciali, deoarece sunt deduşi din realitatea pe
care o exprimă.
Mulţi dintre indicatori au atât aspecte nemotivate
(simbolice), cât şi aspecte motivate (indiciale sau iconice),
dar unele dintre acestea sunt predominante.
7. Gradul de mediere. Indicatorii pot fi legaţi direct
de unele procese reale sau pot fi indicatori ai altor
indicatori. Indicatorii cu un grad mai ridicat de
complexitate au nevoie de indicatori cu un grad mai scăzut
de complexitate. Avem deci de a face cu lanţuri de
indicatori. O cale sistematică de a genera indicatori cu un
grad mai mare de complexitate este aplicarea unor operaţii
de un anumit tip unui set iniţial de indicatori. De exemplu,
există un număr mare de indicatori de ordinul al doilea.
Un alt aspect important este modul de obţinere a
indicatorilor globali din cei locali.
8. Tipuri de cuprindere. Indicatorii pot fi mai mult sau
mai puţin teoretici (de exemplu, indicatorii sofisticaţi), mai
mult sau mai puţin empirici (de exemplu, imaginea unei
932

Rãni deschise

străzi, a unei pieţe), mai mult sau mai puţini analitici, mai
mult sau mai puţin holistici. Există un curent puternic în
favoarea unor indicatori teoretico-analitici inteligibili şi
pentru oameni simpli. Fără astfel de indicatori, se deschid
largi posibilităţi de manipulare a semnificaţiei
indicatorilor.
9. Destinaţia în raport cu distincţia dintre emisfera
dreaptă şi emisfera stângă a creierului. Din păcate, şi aici
există un dezechilibru: majoritatea indicatorilor se
adresează posibilităţilor noastre lingvistice şi logice, sunt
liniari, atomistici, cantitativi, analitici, adresându-se deci
emisferei stângi a creierului, în care sunt localizate
operaţiile secvenţiale. În schimb, nu dispunem de prea
mulţi indicatori a căror înţelegere să fie localizată în
emisfera dreaptă a proceselor de intuiţie. Întrucât
indicatorii calitativi sunt legaţi mai mult de activitatea
intuitivă decât de cea raţională, se poate uşor înţelege de
ce indicatorii intuitivi se află abia la început. Un echilibru
între activitatea emisferei drepte a creierului şi activitatea
emisferei stângi este un simptom de sănătate a persoanei
respective. O sarcină importantă a cercetării în domeniul
indicatorilor este tocmai păstrarea acestui echilibru şi
instaurarea pe baza lui a unei atitudini generoase relativ la
dilema intuitiv/raţional.
10. Metodele de elaborare a indicatorilor. Sunt
elaboraţi de oamenii simpli? de factorii de decizie? de
933

Solomon Marcus

specialişti? (din ce domeniu?). Prin colaborarea unora
dintre aceste categorii? Cum se poate realiza aceasta?
Deoarece doar oamenii sunt în măsură să aprecieze gradul
de satisfacere a nevoilor lor, reacţia maselor este
importantă pentru posibilitatea ameliorării ulterioare a
indicatorilor. De aceea, odată cu problema adecvării
indicatorilor, trebuie să punem şi problema capacităţii lor
de a fi înţeleşi şi acceptaţi. Aceasta din urmă este una dintre
problemele-cheie legate de indicatorii sociali şi
individuali.
11. Cum să prelucrăm indicatorii? Ce se întâmplă
când diferiţi indicatori furnizează informaţii opuse despre
acelaşi proces? Cum putem articula diverşi indicatori
referitori la acelaşi proces? Cum putem elabora un calcul
al indicatorilor? Cum putem agrega indicatorii individuali,
luând în considerare şi posibilele incompatibilităţi ale
diferitelor cerinţe? De exemplu, cum putem atenua
imposibilitatea (pusă în evidenţă de Gh. Păun, Restricţii în
problema indicatorilor sociali, în Metode matematice în
problematica dezvoltării, coordonator Solomon Marcus,
Editura Academiei R.S. România, pp. 133-162) de a
agrega indicatorii individuali astfel încât procesul de
agregare să fie simultan monoton, non-compensatoriu şi
anticatastrofic? Fenomenul aminteşte de faimoasa teoremă
a lui K.J. Arrow (Social choice and individual values,
ediţia a doua, Yale University Press, New Haven, 1963),
934

Rãni deschise

după care nu ar fi posibilă o agregare rezonabilă a unor
preferinţe individuale într-o preferinţă colectivă. Cum în
majoritatea cazurilor indicatorii nu intervin individual,
există o cale de a investiga sistemele de indicatori? Care
sunt perspectivele şi primejdiile unei prelucrări automate
a indicatorilor sociali? Pentru a răspunde acestor dificile
întrebări, trebuie să luăm în considerare unele rezultate
recente privind natura antinomică şi paradoxală a unor
aspecte ale agregării opiniilor, indicatorilor etc. Unele
restricţii obiective care acţionează în acest domeniu arată
în ce măsură ştiinţa poate contrazice intuiţia şi simţul
comun. Colaborarea dintre factorii de decizie şi oamenii
de ştiinţă este – cel puţin în acest sens – indispensabilă.
12. Indicatorii în căutarea proceselor despre care dau
seama. Trebuie să pornim de la ceea ce este evident,
vizibil, pentru a face inteligibil ceea ce nu este vizibil.
Uneori, indicatorii sunt percepuţi chiar înainte ca procesele
cărora le sunt asociaţi să fie înţelese. Acesta este itinerarul
obişnuit atunci când se explică indicatorii copiilor.
13. Procese în căutarea indicatorilor. Pentru unele
procese calitative este nevoie de indicatori foarte sofisticaţi.
Poate apărea însă o contradicţie între complexitatea acestor
indicatori şi înţelegerea lor de către oamenii simpli.
Procesele viitoare necesită indicatori deja existenţi.
14. În ce măsură pot fi indicatorii derutanţi?
Indicatorii sunt implicaţi în procesele de comunicare şi,
935

Solomon Marcus

prin urmare, sunt supuşi tuturor pericolelor acestor
procese. Este nevoie de informaţii cu privire la context
pentru a înlătura posibila ambiguitate a majorităţii
indicatorilor. În special indicatorii statistici pot fi
manipulaţi uşor, deoarece oamenii simpli şi cei „mai puţini
simpli” nu cunosc exact condiţiile de validitate a unei
statistici ştiinţifice (amintim o binecunoscută glumă a lui
Von Mises: există trei feluri de minciuni: cele vinovate,
cele motivate şi... statisticile). Exemple semnificative în
legătură cu unii indicatori foarte importanţi, dar derutanţi,
se află în lucrarea lui Valaskakis şi Martin (op. cit.).
15. Indicatorii nu pot fi înţeleşi decât într-o viziune
în acelaşi timp sistemică şi semiotică, în care circularităţile
devin un fenomen natural.
O dezvoltare a ideilor de mai sus poate fi găsită în
cartea noastră Provocarea ştiinţei (Colecţia „Idei
contemporane”, Editura Politică, Bucureşti, 1987).

936

