Literatura de informare ştiinţifică
Dezbatere realizată de revista Contemporanul,
nr. 24 (1701), 15 iunie 1979
Înregistrând, în anii din urmă, o creştere
spectaculoasă – în număr de titluri, în substanţă şi în aria
de investigaţie – literatura de informare tehnico-ştiinţifică
şi-a cucerit un statut de prestigiu în câmpul de forţe al
culturii. Fapt semnificativ: multe cărţi prezentând cele
mai noi teorii şi realizări din domeniul ştiinţei şi tehnicii
se epuizează la scurt timp după apariţie. Editurile şi-au
diversificat mereu producţia, în momentul de faţă apărând
o serie de colecţii care, prin însuşi titlul pe care-l poartă,
demonstrează amploarea sferei lor de răspândire: Ştiinţa
pentru toţi la Editura Ştiinţifică şi Enciclopedică, Idei
contemporane la Editura Politică, Practicum la Editura
Tehnică sau Cristal la Editura Albatros. Pentru o discuţie
sinceră asupra unor aspecte de larg interes, privind
relaţia dintre acest gen literar şi actualitatea vieţii noastre
culturale, am invitat la o masă rotundă pe:
– Călin Dimitriu, şef de secţie la Editura Albatros;
– dr. Radu Iftimovici, cercetător ştiinţific, Institutul
de virusologie;
– prof. univ. dr. Solomon Marcus;
266

Rãni deschise

– dr. Mircea Măciu, director al Editurii Ştiinţifice şi
Enciclopedice;
– Ion Simion, redactor şef al Editurii Politice.
S.M.: Aş propune să începem prin a ne preciza
termenii şi sfera discuţiei. Cartea, sau mai general
literatura de informare, constituie, socot, o continuare
directă a literaturii didactice, urmând, în funcţie de scopul
urmărit de expeditor, să se adreseze fie unui grup restrâns,
unei brezle profesionale, fie unui public care poate veni
din diferite direcţii. Îndeobşte acesta din urmă e sensul care
se dă „literaturii de informare” şi asupra căruia înţeleg să
ne oprim.
În legătură cu literatura „de popularizare”, aş remarca
un fenomen observabil şi în literatura didactică: o parte
dintre sacini sunt preluate de mijloacele de comunicare în
masă, cum sunt presa, radioul şi televiziunea. Emisiuni ale
televiziunii ca Telenciclopedia şi Mai aveţi o întrebare?
sunt exemple în această privinţă. Faptul a permis
diversificarea literaturii de informare, astfel că se poate
vorbi de adevărate „niveluri” de informare. Un nivel care
şi-a găsit deja o expresie fericită este informarea de cultură
ştiinţifică generală, din colecţia Ştiinţa pentru toţi a Editurii
Ştiinţifice şi Enciclopedice. Sau, pentru un public mai
puţin numeros, dar de cele mai variate profesiuni, colecţia
Idei contemporane a Editurii Politice, deopotrivă serioasă
267

Solomon Marcus

şi atractivă. O carenţă care s-ar cuveni remediată o
constituie situarea nehotărâtă a unor cărţi undeva între
cercetare şi informare. Şi apoi se scrie prea lung. Sunt încă
prea puţine broşurile de câteva zeci de pagini care să
prezinte o informaţie esenţială într-o anumită problemă. În
această privinţă, Editura Albatros sau Editura Tehnică au
câteva realizări remarcabile, dar ar putea face mai mult.
Este îngrijorător de mare numărul „umaniştilor”
lipsiţi de cultură ştiinţifică, fie ea cât de elementară.
Se remarcă uneori o anumită lipsă de adresă a cărţilor
(şi revistelor), după categoria de cititori avută în vedere.
Astfel, comentariul unei cărţi literare se poate adresa
profesioniştilor literaturii, dar poate fi şi un act de orientare
a gustului public. Desigur, poate fi şi una şi alta, dar nu
acesta e cazul în general. Există, de obicei, între cele două
finalităţi, o deosebire de problematică, de cultură, pe care
autorul o presupune, de „jargon” între un text care-şi
propune un schimb de experienţă între scriitori sau critici
în probleme de creaţie şi tipologie literară, şi un text care
formulează pentru un public larg o judecată de valoare
despre o carte recent apărută. Din păcate, o demarcaţie
între textele destinate profesioniştilor literaturii şi cele
orientate spre îndrumarea gustului public nu pare să intre
suficient în preocuparea editurilor şi redacţiilor. Situaţia
poate deruta şi genera „greşeli de adresă”. M-am oprit
asupra acestei probleme, pentru a semnala un fenomen mai
268

Rãni deschise

adânc, o lipsă mai generală de precizare a funcţiilor şi
îndatoririlor. Şi de aici poate şi reticenţa unor oameni de
ştiinţă de primă mărime.
După cum a reieşit clar din intervenţiile
participanţilor la masa rotundă organizată de revista
noastră, literatura de informare ştiinţifică, răspunzând
unei nevoi sporite de cultură ştiinţifică, are misiunea de a
ţine la curent cu progresele ştiinţei categorii dintre cele
mai diverse ale publicului, în special din rândul tinerilor.
Momentul actual al acestei activităţi de înaltă răspundere
se caracterizează prin diversitatea producţiei de carte, în
funcţie de nivelul de pregătire al publicului căruia i se
adresează. Se prefigurează direcţiile de ridicare a
literaturii de informare ştiinţifică la o nouă calitate,
editurile vădind o preocupare mai susţinută, atât pentru
selecţionarea cunoştinţelor care se transmit, cât şi pentru
modul în care acestea sunt prelucrate – o largă
accesibilitate şi o „atractivitate de tip artistic”.

269

