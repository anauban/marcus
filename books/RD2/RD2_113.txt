Tipuri de înţelegere în cunoaşterea
contemporană
Cartea Interferenţelor, Editura Ştiinţifică
şi Enciclopedică, Bucureşti, 1985, pp. 151-159
Varietatea extraordinară de moduri de înţelegere
existente în cunoaşterea contemporană dă impresia că am
intrat într-un fel de anarhie intelectuală. Pentru a stabili o
ordine în această – aparentă – anarhie vom degaja câteva
dicotomii/antinomii ale cunoaşterii pe care se va baza
discuţia ulterioară. Alegerea dicotomiilor este rezultatul
sintezei unei întregi serii de cercetări recente, aparţinând
unor domenii foarte diferite: filozofie, ştiinţă, artă, tehnică.
Desigur că, bazându-ne analiza pe dicotomii, forţăm,
exagerăm lucrurile, dar numai pentru a scoate în evidenţă
ideile esenţiale.
Ne vom referi întâi la dicotomia (I) empiric-reflexiv.
Aspectul reflexiv mai poate fi numit şi intern, de elaborare
intelectuală; aspectul empiric, legat de contactul cu lumea
exterioară, poate fi numit extern. Această dihotomie este
foarte veche – i-a împărţit pe filosofii Greciei antice în două
tabere –, dar şi-a păstrat actualitatea până în zilele noastre,
schimbându-şi doar vestmintele, limbajul. Dicotomia (I) a
796

Rãni deschise

intrat într-o epocă nouă prin lucrările a doi mari savanţi care
au revoluţionat psihologia secolului XX, anume, Noam
Chomsky şi Jean Piaget. Piaget, ale cărui cărţi
fundamentale au fost traduse şi în româneşte, şi-a legat
numele de epistemologia genetică. Noam Chomsky a creat
fundamentele unei teorii generative care iniţial părea să
intereseze numai lingvistica: gramaticile generative
introduse de el au stat la baza limbajelor de programare;
atât sintaxa, cât şi, mai nou, semantica, se studiază cu
ajutorul lor. Lucrările Simpozionului de la Royaumont, de
acum câţiva ani, au fost o adevărată confruntare între
epistemologia genetică, al cărei accent cade pe latura
empirică, externă, dobândită, şi teoria generativă, care pune
accentul pe macanismele înnăscute ale creierului.
O altă dicotomie fundamentală, al cărei suport
biologic s-a cristalizat abia în secolul nostru, este (II)
intuitiv-discursiv (logic). Se ştie în momentul de faţă că
există o relativă specializare a emisferelor creierului uman:
activităţile noastre intuitive, emoţionale, afective, sunt
controlate cu precădere de emisfera cerebrală dreaptă, pe
când activităţile discursive, logice, de limbaj – corespund
mai ales emisferei cerebrale stângi. Dicotomia (II) este
aceea dintre emisfera cerebrală dreaptă şi emisfera
cerebrală stângă, adică, în esenţă, dicotomia nonsecvenţial
– secvenţial. Activităţile de natură secvenţială sunt cele de
prelucrare de simboluri sau, în general, care se referă la
797

Solomon Marcus

obiecte de orice tip care vin unul după altul. Activităţile
nesecvenţiale sunt cele de concomitenţă; într-o intuiţie
avem o cuprindere „dintr-odată ” a unei anumite realităţi
şi nu o ordonare.
Ţinând seamă de cele două dicotomii menţionate,
putem înţelege o serie de lucruri departe de a fi banale: se
poate, de pildă, arăta, printr-un exemplu care interesează
pe toată lumea, cel al proceselor de învăţare, cum se
manifestă deosebirea dintre punctul de vedere empiric şi
cel reflexiv. Prin teoria matematică a învăţării se înţelegea,
până acum vreo 12 ani, exclusiv teoria probabilistă, bazată
pe procese Markov şi mai cu seamă pe teoria lanţurilor cu
legături complete, introdusă de Onicescu şi Mihoc în 1935.
Această teorie probabilistă a învăţării este de fapt
modelarea matematică a interacţiunilor dintre stimuli şi
răspunsuri, ca tip de activitate empirică în sensul lui
Piaget; de aceea se şi numeşte „stimulus – response
theory”. Dar interacţiunea stimul-răspuns este numai
aspectul vizibil, aparent, al învăţării. O asemenea
reprezentare este evident limitată: experienţa noastră ne
poate furniza numai un număr finit de stimuli. Or,
învăţarea unei noţiuni – de exemplu aceea de număr
natural – presupune o infinitate de stimuli şi răspunsuri.
Ca urmare, prin explicarea proceselor de învăţare numai
pe cale empirică, nu putem înţelege cum reuşim să învăţăm
concepte. Trebuie să existe ceva care, adăugat laturii
798

Rãni deschise

empirice, să dea posibilitatea unei extrapolări: aici
intervine componenta raţional-reflexivă. Desigur, aceasta
este o chestiune binecunoscută; problema constă în
exprimarea ideii într-un model matematic.
Reprezentând funcţionarea creierului uman sub forma
unei gramatici universale, teoria topologică a învăţării
modelează trecerea de la învăţarea empirică, relativă la
primii termeni ai unui obiect de învăţat, la învăţarea
conceptuală, relativă la infinitatea de termeni ai şirului
care exprimă obiectul de învăţat în forma sa potenţială.
Teoria probabilistă şi cea topologică a învăţării sunt, în
acest sens, complementare. Dicotomia empiric-reflexiv
poate fi pusă în evidenţă şi prin relaţiile dintre competenţă
şi performanţă, deci dintre infinit şi finit, esenţiale în
psihologia modernă. Competenţa este întotdeauna infinită
şi potenţială, ca latură reflexivă, pe când performanţa se
leagă de activităţi concrete, particulare, de tipul
empiricului.
Desigur, pentru a reprezenta personalitatea umană în
integralitatea ei, ar fi necesar să luăm în considerare şi
alte dicotomii – cum ar fi, de exemplu, conştientinconştient. De asemenea, se pune problema de a alege
dicotomiile esenţiale în caracterizarea activităţii
intelectuale, de a stabili care sunt cele primordiale şi care
derivate, ordinea în care trebuie să le considerăm etc.
Discuţia noastră se va limita însă la dicotomiile (I) şi (II).
799

Solomon Marcus

Niciunul dintre elementele acestor dicotomii nu există
în stare pură. Vom analiza în continuare cele patru
elemente care rezultă din combinarea elementelor
dicotomiilor empiric-reflexiv şi intuitiv-discursiv. La
intersecţia dintre reflexiv şi discursiv se situează modul de
înţelegere (I) analitic-sintetic al realităţii (ca la Democrit
sau Descartes, de exemplu). Disciplinele speculative, ca
matematica, fizica teoretică etc., aparţin cu precădere
tipului analitic-sintetic. Este interesant că, în ultimul timp,
multe discipline îşi dezvoltă o componentă speculativă,
intrând astfel în zona analiticului: aşa s-a întâmplat, de
exemplu, cu lingvistica. Din combinaţia reflexivului cu
intuitivul rezultă un mod de înţelegere pe care îl vom numi,
împrumutând cuvântul din limba engleză, (2) holistic –
adică cel care se referă la întreg. Trebuie subliniat faptul
că „holistic” nu este echivalent cu „sintetic”: sinteticul este
o altă faţă a analiticului, refacerea întregului din cioburi,
pe când holisticul corespunde unei cuprinderi integrative
directe, imediate (de imediateţe), neprecedate de o
înţelegere analitică. Holisticul este, cu precădere, domeniul
artisticului, dar aici intră şi filosofi ca Parmenide, Zenon
şi Platon. Intersecţia discursiv-empiric dă naştere (3)
experimentalului, unde-i putem plasa şi pe unii filosofi
empirişti; la intersecţia intuitiv-empiric apare ceea ce
putem numi, folosind un cuvânt ce se întâlneşte rar în
limba română, (4) experienţialul (vezi Heraclit, Hegel,
800

Rãni deschise

Bergson). Atât domeniul experimentalului, cât şi cel al
experienţialului se referă la experienţă, dar există o
deosebire importantă între ele: experimentalul este modul
de înţelegere bazat pe experienţe provocate de noi,
deliberat, pe când în experienţial se includ experienţe pe
care le trăim, naturale, spontane. Fizica experimentală,
diferite discipline tehnologice aparţin experimentalului.
De gândirea experienţială se prevalează disciplinele socialumaniste, ştiinţifice sau semi-ştiinţifice. Experienţialul a
intrat într-o actualitate deosebită datorită studiului
culturilor orientale, în care rolul său este primordial.
Este interesant faptul că, în ultimele decenii, fiecare
dintre cele patru tipuri de înţelegere a declanşat o ofensivă
formidabilă asupra celorlalte.
Disciplinele de tip experienţial (social-umaniste) şi
experimental şi-au dezvoltat o componentă teoretică,
speculativă. Ofensiva analiticului asupra experimentalului
se manifestă în dezvoltarea componentei teoretice a fizicii,
biologiei, chimiei. Analiticul a influenţat experienţialul
prin intermediul experimentalului: vezi cazul ştiinţelor
sociale; metoda modelării matematice a pătruns în
domeniul experimentalului, apoi al experienţialului – unde
iniţial se foloseau metode de studiu pur descriptive.
Influenţa analiticului asupra holisticului s-a produs prin
experimental şi experienţial, după cum se vede din multe
cercetări, inclusiv ale românilor Matila Ghyka şi Pius
801

Solomon Marcus

Servien. Într-o ultimă fază, analiticul şi-a permis
imixtiunea direct în holistic: a fost nevoie de o maturizare
a disciplinelor pentru a se trece de la o ofensivă indirectă
la una directă a analiticului asupra celorlalte zone.
Invers, se poate pune în evidenţă ofensiva holisticului
şi experienţialului spre analitic şi experimental. Un
exemplu în acest sens este importanţa dată, în
matematicile fundamentale şi aplicative, studiului
impreciziei. Cunoscuta antinomie pascaliană: „esprit
géométrique-esprit de finesse”, reprezenta deosebirea
fundamentală dintre gândirea riguroasă, dar incapabilă de
nuanţe, şi cea de „fineţe”, bogată în nuanţe, dar dominată
de imprecizie. Astăzi, domeniul fineţei este înţeles, în mare
măsură, datorită matematicii moderne: este posibil să
facem o listă de aproximativ 20 de tipuri de imprecizii şi
să dezvoltăm, pentru fiecare, un model matematic adecvat.
Lista cuprinde, de exemplu, imprecizia de natură aleatoare
(stohastică), imprecizia „fuzzy” – descrisă prin teoria
mulţimilor vagi, ambiguitatea (cum este omonimia în
limbă), generalitatea ş.a. Considerăm ofensiva holisticului
spre analitic de mare importanţă. Urmărind istoria
ultimilor 200 de ani de matematică, observăm că multe
rezultate remarcabile au fost găsite altfel decât pe cale
analitică. S-ar putea spune că logica este numai vestmântul
cu care matematica iese în lume, în societate, sub care ea
se prezintă cititorului. De fapt, cele mai multe rezultate
802

Rãni deschise

sunt pur şi simplu găsite – după cum observă Poincaré şi
Hadamard – prin mijloace nesecvenţiale, de tip intuitiv
sau holistic. Să găseşti un rezultat în matematică este, în
fond, un act de imaginaţie. Moisil spunea că „o teoremă
este un sentiment” – şi, într-adevăr, descoperirea în
matematică este de aceeaşi natură (psihologică) ca invenţia
artistică. Se confundă însă, de obicei, „găsirea”, cu
verificarea şi confirmarea: şi astfel predăm studenţilor un
tip de matematică scoasă din contextul ei natural, nevie,
prefabricată. Trebuie să remarcăm un fapt esenţial: prin
natura sa, fiinţa umană pare să aibă nevoie de un echilibru
între activităţile de tip intuitiv (emisfera cerebrală dreaptă)
şi cele de tip discursiv (emisfera stângă), precum şi între
reflexiv şi empiric. Unul dintre motivele cele mai
importante pentru care psihiatrii sunt atât de solicitaţi este
faptul că se pune – inclusiv în procesul de învăţământ – un
accent exagerat pe analitic şi aceasta nu se poate face decât
în dauna holisticului şi experienţialului.
În fond, trecerea de la gândirea empirică la cea
raţională, de la modul de învăţare probabilist la cel
tipologic, nu este diferenţa generată de realizarea
inducţiei?
S.M.: În ultimele decenii, procesul de inducţie a fost
mult clarificat. Trebuie subliniat faptul că inducţia
completă a matematicienilor nu este inducţie, ci tocmai
opusul ei: inducţia completă în matematică este, de fapt,
803

Solomon Marcus

deducţie – dovadă că principiul ei este o teoremă care se
demonstrează în cadrul axiomaticii lui Peano a teoriei
numerelor naturale. Inducţia – aşa cum este ea practicată
cu precădere în domeniul experimentalului – este trecerea
de la un număr finit de observaţii la cazul general. Dar
această trecere este grevată de antinomii atâta timp cât
încercăm să o explicăm în cadrul teoriei probabilităţilor.
Există, în acest sens, antinomii celebre: una dintre ele
aparţine lui C. Hempel. Aceste antinomii pleacă de la ideea
că avem în cap o ipoteză teoretică, pe care o proiectăm
asupra lecturii datelor experimentale. De asemenea, relaţia
empiric-reflexiv nu e una de consecutivitate: întâi
strângem date, observăm şi pe urmă generalizăm şi facem
teorie (asta e reprezentare rudimentară). În procesul de
inducţie, latura empirică şi cea teoretică se presupun
reciproc.
Credeţi că există o posibilitate cât de cât constructivă
de a face matematică altfel decât analitic?
S.M.: Sigur că da. În momentul de faţă avem foarte
multe exemple. Sunt atâtea cărţi şi articole în care
atomisticul, analiticul sunt ponderate de o intuiţie directă
a fenomenelor: de pildă cărţile lui G. Polya (unele traduse
în româneşte). Sursele ininteligibilităţii matematicii
constau în modul obişnuit de predare; matematica nu e
înţeleasă fiindcă e predată pe dos decât cum e găsită. Ceea
ce trebuia să fie o plăcere – devine un chin, o pedeapsă.
804

Rãni deschise

Dispare recompensa spirituală. Tratatul lui Bourbaki voia
să sublinieze ipostaza matematicii ca disciplină structurală
(proces paralel cu cel din alte discipline, de exemplu
lingvistica). Noi am absolutizat acest lucru şi l-am
transformat în mod de a scrie monografii şi manuale – şi
astfel matematica s-a secătuit. Nici idei nu mai sunt.
Nimic. Definiţiile sunt scoase din buzunar: rar se discută
de ce se introduce o anumită definiţie şi nu alta; nu se
discută sursele – de ce studiem aceste noţiuni şi nu altele.
Nu se discută, deci, contactul cu exteriorul. Singurul lucru
care interesează e rigoarea demonstraţiei. În cel mai bun
caz, arătăm că un anumit model, o teoremă, un concept se
pot particulariza pe nişte lucruri din fizică sau alte
discipline. Dar asta e insuficient.
Există vreo şcoală matematică bazată pe celălalt
punct de vedere decât cel analitic – care să fi dat rezultate
bune?
S.M.: Nu e vorba de a exclude analiticul, ci de a-l
echilibra cu holisticul. Mă refeream la modul în care
matematica iese din lume, la modul în care încearcă să facă
prozeliţi. Aici suferă matematica un mare eşec – fiindcă
îşi absolutizează propriul ei jargon şi astfel se izolează de
lume, de societate – de tot – şi devine un fel de sectă care
foloseşte un limbaj esoteric. Acesta e adevărul amar, care
se constată în toată lumea şi care s-a discutat şi la ultimul
congres de matematică.
805

Solomon Marcus

În ciuda acestui eşec, matematica are şi un mare
succes: tocmai prin latura sa analitică matematica a avut
un impact puternic asupra altor domenii.
S.M.: E un succes, dar nu social. E un succes din
punctul de vedere al istoriei interne a ştiinţelor. Dar, în
timp ce în viaţa ei internă matematica e foarte bogată, se
dezvoltă, apar noi domenii, relaţia cu exteriorul este în
involuţie. Matematica nu mai este înţeleasă de public şi,
cum spunea Moisil, „Omul, când nu înţelege, e contra”.
Apare un sentiment de frustrare în faţa lucrului pe care nu
îl înţelegi şi astfel îl suspectezi.
Cum vedeţi Dvs. acea poezie modernă care are
tendinţa de a renunţa chiar la logica gramaticală?
S.M.: Deosebirea dintre logica ştiinţei şi cea a artei
este, cred, următoarea: ştiinţa se dezvoltă în cadrul unei
logici prestabilite; arta îşi dezvoltă ad-hoc propria ei
logică. Opera de artă ne propune o logică inedită,
construită prin mijloacele ei interne. Deci ceea ce pare
abolirea ordinii existente nu e decât instaurarea unei alte
ordini, care nu e dată explicit, ci trebuie sesizată chiar din
opera respectivă.
Se teoretizează pe parcurs?
S.M.: Nu se teoretizează pe parcurs, căci arta este
anticonceptuală. Luaţi, de exemplu, poezia care la noi pare
cea mai anarhică, a lui Nichita Stănescu. Unii au spus
despre ea că este pur şi simplu o aiureală. Nu e. Descoperi
806

Rãni deschise

acolo că întotdeauna este subiacentă o ordine instaurată de
text. În măsura în care nu ar exista această ordine, această
logică subiacentă, nu ar mai fi vorba de poezie. Cuvântul
logică e folosit aici în sens generalizat. Prin logică
înţelegem acum un ansamblu de reguli. Dovadă că textele
poeţilor se supun unor reguli este că se pot foarte uşor
parodia. Şi puteţi observa – de exemplu, la revelion, când
apar parodii la adresa poeţilor – că sunt parodiaţi mai ales
acei poeţi care par anarhici.
Dvs. vedeţi o logică implicită în stil?
S.M.: Cuvântul „stil” a devenit un cuvânt periculos.
În teoria lui Pius Servien apare următoarea idee, aparent
paradoxală: în poezie nu există probleme de stil, pentru că
nu există sinonimie, un lucru nu se poate spune decât
într-un singur fel. Marca marii poezii este tocmai impresia
că nimic nu poate fi modificat. În ştiinţă, e exact invers. În
matematică, de exemplu, un lucru poate fi spus într-o
infinitate de moduri. Problema stilistică înseamnă să alegi,
din această infinitate, acel enunţ care prezintă anumite
avantaje din punct de vedere retoric, psihologic. De altfel,
cuvântul „stil” nici nu prea mai e folosit de literaţi. Ţine
de o viziune cu care ne-a obişnuit şcoala – să vedem
împărţită analiza poetică în stil şi conţinut, viziune care
acum e depăşită.
Maniera holistică, ce îşi face apariţia în matematică,
a reuşit oarecum să-i dea o unitate?
807

Solomon Marcus

S.M.: Un exemplu. Aţi auzit probabil de modul în
care s-a demonstrat, cu ajutorul calculatorului, faimoasa
conjectură a celor patru culori. Se poate vedea aici chiar
criza analiticului şi rolul holisticului. În acest caz,
matematicienii s-au găsit în faţa unei demonstraţii pentru
care aveau o înţelegere analitică, dar nu şi una holistică.
Ajungi să-ţi dai seama ce important e un lucru abia atunci
când îţi lipseşte. Eram obişnuiţi, când citeam o teoremă,
să ne facem o reprezentare a ei, să-i înţelegem „de ce”-ul.
Prin calculator, apare un divorţ între analitic şi holistic.
Există pericolul să dezvoltăm un tip de cunoaştere în care
analiticul să nu mai aibă acoperire holistică. Nevoia de
echilibru analitic-holistic e o nevoie organică a fiinţei
umane. Ce vom face ca să recuperăm acest decalaj? Căci,
deocamdată, am exacerbat capacitatea noastră analitică,
producând un decalaj în dauna holisticului.

808

