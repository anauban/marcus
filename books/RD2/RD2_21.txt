Olimpiadele
Scânteia Tineretului, nr. 8, 24 noiembrie 1977
În domeniul matematicii, există la noi o frumoasă
tradiţie a concursurilor şcolare. Concursurile Gazetei
matematice au stimulat preocupările ştiinţifice ale
tineretului timp de peste patruzeci de ani, lansând
numeroase talente, din care s-au recrutat ulterior mulţi
dintre matematicienii şi inginerii noştri de seamă.
Olimpiadele de matematică organizate în ultimele decenii
continuă, mai ales prin etapa lor finală, pe ţară, tocmai
aceste concursuri care au animat viaţa noastră ştiinţifică
în prima jumătate a secolului. Dar, evident, cei care au
conceput aceste întreceri în domeniul ştiinţei sau
literaturii, prin analogie cu întrecerile sportivilor, nu au
intenţionat să adauge, pur şi simplu, la şirul lung de
controale la care oricum trebuie să se supună orice elev
pentru a putea fi promovat, încă un examen. Intenţia nu a
fost de a mări cu încă o unitate numărul şi aşa destul de
mare de teze, extemporale şi examene orale ale elevilor.
Şi totuşi... Dacă nu toţi elevii participă la olimpiade, nu e
mai puţin adevărat că selecţionarea participanţilor la
prima etapă, locală, cu caracter de masă, se face de multe
ori pe cale birocratică, prin desemnarea lor de către
168

Rãni deschise

profesori. Participarea la olimpiadă se produce de multe
ori nu atât ca rezultat al unei opţiuni exprimate de elev,
cât pentru îndeplinirea unei sarcini care-i este trasată.
Acest fapt e simptomatic pentru întreaga desfăşurare a
olimpiadelor şcolare.
Întorcându-ne la semnificaţia originară a acestui
frumos cuvânt – OLIMPIADA – şi la adjectivul OLIMPIC
pe care l-a generat, intrăm imediat într-o atmosferă de
sărbătoare şi fast, de maiestuos şi impunător pe care în
mod tradiţional olimpiadele sportive au creat-o. Aceste
întreceri nu au fost gândite ca o activitate standard, de
testare rutinară a unor capacităţi minimale ale
concurenţilor.
Nu controlul unui bagaj minimal de cunoştinţe şi
deprinderi, ci o întrecere a talentelor şi a pasiunilor,
pentru a se crea în acest fel o emulaţie generală, de
natură să ambiţioneze pe cât mai mulţi elevi să lucreze
la randamentul lor maxim; aceasta îşi propun
olimpiadele.
Răspund azi olimpiadele şcolare la aceste deziderate?
Se ne referim mai întâi la problemele propuse, de la
începutul secolului până azi, la diferite concursuri şi
olimpiade şcolare; multe dintre aceste probleme au fost
strânse în două volume, unul aparţinând lui T. Roman, O.
Sacter şi Gh. D. Simionescu (perioada 1900 – 1967), iar
al doilea lui L. Panaitopol şi C. Ottescu (1968 – 1974).
169

Solomon Marcus

Sunt printre ele multe probleme frumoase, elegante,
formulate lapidar, pline de ingeniozitate, vizând calităţile
intelectuale ale tinerilor: spirit de observaţie, îndemânare
combinatorie, capacitate de deducţie logică, putere de
abstracţie şi analogie. Dar nu mai puţin adevărat este şi
faptul că un mare număr de probleme sunt lipsite de o
motivaţie convingătoare, fapt care le conferă un simplu
caracter de joc, în genul problemelor de şah. Să se fi
interpretat în acest fel analogia cu olimpiadele dedicate
jocurilor sportive? În cei optzeci de ani care ne despart de
primele concursuri şcolare, matematica a parcurs o
evoluţie impresionantă, dar ea se reflectă foarte palid în
problemele date la olimpiade şi alte concursuri. Cu greu
ar recunoaşte cineva în aceste probleme mutaţiile aduse de
viziunea structurală şi de cea algoritmică, rolul social
crescând al matematicii, aplicabilitatea ei universală.
Desigur, aici ar intra în discuţie în primul rând programele
şi manualele şcolare; dar nu e mai puţin adevărat că
olimpiadele, mai ales în etapa pe ţară, îşi pot permite o
libertate mai mare în alegerea problemelor.
Mai e însă şi un alt aspect. Problemele de olimpiadă
nu s-au diferenţiat suficient, ca factură, de problemele
curente. Nu avem în vedere aici diferenţa în ceea ce
priveşte gradul de dificultate (această diferenţă a fost, în
general, observată), cât diferenţă în gradul de atractivitate.
Prea multe dintre problemele propuse la olimpiade sunt
170

Rãni deschise

aride şi nu-şi dezvăluie nicio semnificaţie cognitivă. Le
propunem elevilor să rezolve nenumărate tipuri de ecuaţii,
dar destul de rar sunt ei invitaţi să stabilească modul în
care aceste ecuaţii iau naştere dintr-o problemă reală. Şi
totuşi, elevii de azi sunt cei care vor trebui, mâine, să
constuiască modele matematice pentru problemele
energetice, de materii prime, ecologice, de hrană sau
informaţionale cu care omenirea e din ce în ce mai mult
confruntată.

171

