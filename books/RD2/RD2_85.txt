Rigoare şi competenţă
Contemporanul, nr. 31 (1916), 29 iulie 1983, p. 6
Fiind vorba despre rigoare, să procedăm în mod
riguros. Dicţionarul explicativ al limbii române dă în
dreptul acestui cuvânt următoarea explicaţie: asprime,
severitate, stricteţe, străşnicie. Dintre aceşti patru termeni,
cel care se asociază mai natural cu competenţa este
stricteţe, în dreptul căruia, în acelaşi dicţionar, stă scris:
rigurozitate, severitate, asprime. Eşuând în încercarea de
a folosi dicţionarul, să folosim totuşi sugestiile lui. La ce
se referă stricteţea? Îmi amintesc de o inscripţie pe un zid:
„Murdăria strictă”. De unde provine caracterul ilariant?
Stricteţea presupune anumite reguli care trebuie respectate.
Putem spune, deci, că rigoarea constă în conformitatea cu
un anumit sistem de reguli. Fără precizarea acestui sistem,
sensul rigorii nu este clar. Putem înţelege astfel şi
caracterul istoric al rigorii. În matematică, de exemplu,
exigenţele de rigoare au evoluat mult. Sistemul de reguli
care stau la baza gândirii şi limbajului matematic a fost
mereu ameliorat, în aşa fel încât ceea ce ieri era considerat
forma supremă a rigorii astăzi aparţine matematicii
informale, intuitive, chiar naive. A se vedea, de exemplu,
teoria naivă a mulţimilor, expresie a matematicii celei mai
627

Solomon Marcus

riguroase existente în urmă cu o sută de ani, şi teoria
axiomatică a mulţimilor, edificată în secolul nostru.
În funcţie de ce criterii se modifică sistemul de
reguli? În funcţie de evoluţia teoriilor ştiinţifice, a
exigenţelor metodologice şi teoretice, a preciziei cerute în
aplicaţii. Orice mutaţie în materie de rigoare a fost
determinată de o anumită criză, constând fie în apariţia
unor antinomii, fie a altor dificultăţi, cum ar fi
imposibilitatea de rezolvare a unor probleme importante,
capacitatea explicativă redusă a teoriilor existente etc.
Trecerea la un nivel superior de rigoare a fost întotdeauna
însoţită de o reconsiderare a fundamentelor şi de o
renovare a limbajului ştiinţific. În acelaşi timp, trebuie să
observăm că, într-o anumită măsură, tipul de rigoare
adoptat la un anumit moment de un domeniu sau altul al
cunoaşterii are un caracter convenţional. Dar, de fapt, un
acelaşi domeniu lucrează concomitent cu mai multe
niveluri de rigoare, după natura problemelor abordate.
Astfel, în matematică cercetările privind fundamentele sau
problemele de logică matematică adoptă un grad superior
de rigoare faţă de cele privind ecuaţiile diferenţiale sau
teoria grupurilor. Necesitatea folosirii concomitente a mai
multor grade de rigoare este determinată şi de faptul că
este nevoie să se menţină contactul cu sursele intuitive ale
conceptelor, teoriilor şi problemelor. Unii oameni de ştiinţă
chiar definesc rigoarea ca detaşare de consideraţiile
628

Rãni deschise

intuitive; din acest punct de vedere, riguros ar fi acel
demers care, respectând anumite reguli logice, nu riscă să
se contamineze de nesiguranţa şi imprecizia intuiţiilor
noastre.
Iată însă că despre rigoare se vorbeşte foarte mult şi
în artă, în literatură, în activităţile practice. Un singur
exemplu. Într-un eseu despre Ion Barbu, Marin Mincu
afirma (pp. 288-289): „Construcţiile sistematice ale criticii
par să demonstreze riguros intuiţii mai vechi şi
scepticismul unora în ceea ce priveşte justificarea acestor
sisteme derivă de aici. Dar empirismul, chiar când e
sprijinit pe bun gust, eşuează adeseori într-o rutină
obositoare, căci numai cu talentul în critică nu se ajunge
prea departe. În general, refuzul metodei divulgă refuzul
rigorii. Se face abstracţie de un lucru elementar în
asemenea cazuri; nu intuiţiile lipsesc, ci demonstraţia
coerentă a acestora într-un sistem articulat”. Observăm
aici, alături de opoziţia riguros-intuitiv, introducerea în
discuţie a unui alt aspect: echivalarea rigorii cu adoptarea
unei metode. Riguros ar fi deci ceea ce se reclamă de la o
anumită metodă. Încercând să dezvoltăm mai departe
această idee, putem spune că rigoarea intră în opoziţie cu
procedurile ad-hoc. Marcel Pop Corniş, (în Luceafărul din
26 martie 1983, p. 6) preia citatul de mai sus ca o replică
la punctul de vedere al lui Şerban Cioculescu, după care
progresul criticii nu constă în metoda generală, ci în
629

Solomon Marcus

„l’esprit de finesse” al fiecărui critic, în puterea lui de
pătrundere personală şi de expresie cuceritoare. În acest
fel, rigoarea este echivalentă cu acel „spirit geometric”
pe care Pascal îl opunea spiritului de fineţe. Acum ştim
că această opoziţie este de fapt opoziţia dintre activitatea
emisferei stângi şi aceea a emisferei drepte a creierului
uman şi că echilibrul şi conlucrarea acestor două tipuri de
activitate cerebrală este nu numai salutar în orice activitate
creatoare, ci şi necesar pentru sănătatea noastră
psiho-somatică. Mai exact, emisfera cerebrală stângă
controlează cu preponderenţă activităţile secvenţiale
(limbajul şi logica în primul rând) iar cea dreaptă se referă
în primul rând la activităţile de concomitenţă
(preponderente în emoţii şi intuiţii, unde imediateţea şi
globalitatea sesizărilor sunt esenţiale). Un indice important
al calităţii vieţii este echilibrul dintre aceste două tipuri de
activitate. Multe dintre anomaliile culturii, învăţământului,
activităţilor creatoare rezultă din nerespectarea acestui
echilibru care, după cum s-a putut vedea mai sus, este în
bună măsură echilibrul dintre rigoare şi intuiţie, dintre
rigoare şi procedare ad-hoc. Oricum am înţelege rigoarea,
ea îşi are sediul cu precădere în emisfera cerebrală stângă.
Dat fiind însă că, aşa cum am văzut, există diferite tipuri
de rigoare, problema este a nu se impune unei activităţi o
rigoare de un alt tip decât acela care i se asociază ei în mod
natural. Desigur, evoluţia cunoaşterii umane poate
630

Rãni deschise

promova, în anumite domenii, tipuri de rigoare considerate
ieri inadecvate, dar aceste transformări nu pot fi impuse în
mod brutal. Intervine esenţial procesul de învăţământ,
colaborarea interdisciplinară, deschiderea faţă de nou,
capacitatea de ieşire din rutină etc.
Ne-am apropiat astfel de problema competenţei. În
Dicţionarul explicativ al limbii române citim despre
competenţă: capacitate a cuiva de a se pronunţa asupra
unui lucru, pe temeiul unei cunoaşteri adânci a
problemei în discuţie; capacitate a unei autorităţi, a unui
funcţionar etc., de a exercita anumite atribuţii. Lăsând
de o parte aspectul administrativ-juridic, competenţa, în
sensul de mai sus, nu poate fi dobândită decât prin
fuziunea unor factori înnăscuţi cu alţii, dobândiţi. Un rol
esenţial îl are aici procesul de învăţare creatoare, care,
bazându-se pe mecanismele înnăscute ale creierului şi
orientând în mod adecvat activitatea empirică de
interacţiune a stimulilor şi răspunsurilor, conduce la
formarea unor competenţe superioare. Succesul învăţării
este condiţionat de adecvarea experienţei umane la
particularităţile individuale înnăscute (echilibrul dintre
empiric şi reflexiv) şi de stimularea conlucrării
armonioase a celor două emisfere cerebrale (echilibrul
dintre logic-verbal şi intuitiv-emoţional). Este deci clar
că dobândirea unei competenţe implică şi dobândirea
unui tip corespunzător de rigoare.
631

Solomon Marcus

Într-un sens mai modern, competenţa este definită în
relaţie cu performanţa. Competenţa unui subiect în raport
cu o anumită activitate este ansamblul posibilităţilor de
care el dispune în această privinţă. Performanţa este
modalitatea efectivă de desfăşurare a activităţii respective.
Relaţia competenţă-performanţă ar fi deci relaţia
potenţial-actual. Cunoaşterea vocabularului şi gramaticii
unei limbi ar fi competenţa vorbitorului acelei limbi, în
timp ce folosirea ei efectivă ar defini performanţa sa. În
mod vizibil, competenţa revine la un ansamblu de reguli,
deci este supusă unei anumite rigori; iar respectul acestor
reguli condiţionează în bună măsură calitatea
performanţei noastre lingvistice.
Când partidul nostru, secretarul său general, tovarăşul
Nicolae Ceauşescu, militează pentru promovarea
competenţei, implicit militează pentru profesionalitate şi
pentru rigoare, pentru randament superior şi productivitate
superioară. (Nota mea, Solomon Marcus: Această din
urmă frază nu a existat în manuscrisul meu.) Creşterea
gradului de creativitate este inevitabil legată de trecerea la
o competenţă superioară.

632

