A găsi sau numai a verifica?
Ghidul candidatului la admiterea în învăţământul
superior, nr. 30 (6), 1987, pp. 28-30
Între gândire şi rutină
Se spune că matematica formează gândirea. Dar tot
matematica ne pune la dispoziţie diferite procedee
rutinare, a căror aplicare aproape mecanică se mulţumeşte
cu un efort intelectual minim. Această situaţie derutantă
îşi are o explicaţie simplă. Etapa de maturitate a oricărei
discipline matematice este calculul, dar orice calcul se
supune unor restricţii care, pentru a putea fi depăşite, fac
necesar un nou aparat conceptual şi demonstrativ. Inovaţia
şi rutina alternează mereu în matematică; nu numai că
alternează, dar şi coexistă. Auzim de multe ori despre
cutare elev sau student că nu ştie să calculeze. Însă viciul
se află chiar în această separare a laturii algoritmice de
latura conceptuală a matematicii. Matematica este o
activitate de cunoaştere, nu un simplu joc cu simboluri.
Ce valoare culturală poate avea un calcul efectuat cu
elemente fără o semnificaţie clară şi care nu poate fi
interpretat, în fiecare etapă, a sa, în raport cu condiţiile
problemei în discuţie? În măsura în care un calcul nu are
958

Rãni deschise

nevoie de referiri la un anumit context teoretic, efectuarea
sa poate fi transferată maşinii şi ar fi nedrept ca oamenii
să-şi piardă vremea cu el. Ce rost ar avea, de exemplu, să
pretindem unui om să calculeze 2 la puterea de exponent
egal cu factorial de zece? Pentru a nu mai vorbi de faptul
că, pentru o valoare mai ridicată a exponentului, nici cele
mai puternice calculatoare disponibile nu ne-ar putea
scoate din încurcătură. Însă, tocmai în vederea depăşirii
acestei limite, suntem stimulaţi să ieftinim costul operaţiei
respective, cu alte cuvinte să imaginăm un algoritm de
complexitate mai redusă, având ca obiect calculul
puterilor lui 2.
Câteva probleme simple
Care este cel mai mic număr pătrat perfect, superior
lui 100? Deoarece 100 este pătratul lui 10, numărul căutat
va fi pătratul lui 11, deci 121. Care este cel mai mare
număr pătrat perfect, superior lui 100? Un atare număr nu
există, deoarece şirul numerelor naturale este nemărginit.
Care este cel mai mare număr divizibil cu 7, inferior lui
100 şi care diferă printr-o unitate de un număr prim? Cel
mai mare număr divizibil cu 7, inferior lui 100, este 98
(=70+28), care diferă printr-o unitate de numărul prim 97.
Care este cel mai mic număr inferior lui 100, divizibil cu
11 şi care diferă prin 3 de cel mai apropiat număr prim?
959

Solomon Marcus

Se vede uşor că numărul 44 (care diferă prin 3 atât de 41,
cât şi de 47, amândouă numere prime) răspunde problemei.
Exerciţii de acest fel sunt la îndemâna oricui, ele
reprezintă pragul de jos al exigenţei în materie de gândire
matematică. Totuşi, nu este greu de sesizat deosebirea dintre
ele şi altele, de tipul: să se arate că 147 este divizibil cu 3; să
se rezolve ecuaţia x²-3x+1=0, unde simpla aplicare a unei
reţete este suficientă pentru obţinerea rezultatului. Exerciţii
de tipul celor din urmă, chiar dacă sunt însuşite, sunt foarte
puţin relevante din punct de vedere educaţional, ele dau o
imagine falsă despre natura şi scopul matematicii. Să ne
întoarcem deci la celelalte probleme, care, independent de
gradul lor de dificultate, impun o cât de mică iniţiativă
personală. Care este ordinea de mărime a numerelor 1; 1,000;
0,999…? Egalitatea primelor două este observată de mulţi
elevi, dar justificarea este de multe ori nefericită („zero după
virgulă nu are valoare”, ca şi cum numerele 1,05 şi 1,5 ar fi
egale). În ceea ce priveşte egalitatea dintre primul şi al treilea
dintre numerele propuse mai sus, mult mai puţini sunt cei
care o observă şi o pot justifica. Este vorba de simplul fapt
al reprezentării lui 0,999… ca sumă infinită de tipul 0,9 + 0,09
+ 0,009 + 0,0009+ …, deci ca sumă a progresiei geometrice
infinite 9/10 + 9/100 + 9/1000 + … + 9/10ⁿ + …,
care se vede uşor că are ca sumă pe 1.
Este important să se recunoască natura numerelor,
independent de modul particular în care sunt ele exprimate.
960

Rãni deschise

Nu sunt puţini elevii care cred că numărul π este raţional,
deoarece este egal cu …3,14, uitându-se faptul că 3,14
reprezintă numai o aproximare a lui π, despre care se ştie
încă din secolul al XVIII-lea că este iraţional (iar în secolul
trecut s-a stabilit şi transcendenţa sa). Despre 1/0 mulţi
elevi cred că este egal cu zero, cu 1 sau cu… infinit; iar
dintre cei puţini care răspund corect că împărţirea prin zero
nu este posibilă, numai unii pot să şi justifice răspunsul.
Despre radical din minus unu, elevii unei clase a şasea au
dat cele mai extravagante răspunsuri, numai răspunsul
natural „nu ştiu”, cuvenit unei întrebări la care încă nu
învăţaseră să răspundă, a fost evitat. Ignoranţa nu este
periculoasă dacă este conştientizată şi există dorinţa de a
o înlătura. Aşa cum, întâlnind un cuvânt necunoscut, este
firesc să ne interesăm de semnificaţia sa, în faţa unui
simbol matematic nou nu trebuie să ne ascundem
necunoaşterea.
Rolul observaţiei
Care este termenul următor în şirul 1, 2, 3, 6, 11, 20,
37,… ? O atare formulare trebuie înţeleasă în modul
următor: să se găsească o regulă de succesiune în care să
se încadreze cei 7 termeni specificaţi şi să se stabilească,
pe baza ei, termenul de rang 8. O observare atentă a
succesiunii celor şapte termeni dezvăluie faptul că,
961

Solomon Marcus

începând cu al patrulea termen, fiecare termen este suma
celor trei termeni imediat precedenţi. Rezultă deci că
termenul imediat următor este 68.
Cu excepţia problemelor de Analiză matematică,
chestiunile care urmează au fost selecţionate dintr-un set
de probleme propuse recent în unele institute de
învăţământ din Anglia. Unele dintre ele sunt cunoscute şi
publicului nostru, ceea ce arată că, în esenţă, tipurile de
probleme sunt aceleaşi peste tot. Ele solicită gândirea
activă şi n-ar fi rău dacă, după examinarea lor, cititorul ar
încerca să construiască, la rândul său, probleme
asemănătoare.
Care este termenul următor în şirul 1, 2, 4, 6, 10, 12,
16, 18, 22,…? Observăm că şirul considerat se obţine din
şirul numerelor prime 2, 3, 5, 7, 11, 13, 17, 19, 23,…-,
scăzând pe unu din fiecare termen.
Să ne punem acum întrebări similare despre şirurile:
(a) 19, 21, 23, 25, 31, 34, 103,…; (b) 1, 2, 4, 8, 17, 35, 71,
152, 314, 628,…; (c) 1, 2, 2, 3, 2, 4, 2,…
În şirul (a), numărul 19 este scris, succesiv, în baza
10, în baza 9, în baza 8, în baza 7, în baza 6, în baza 5, în
baza 4, deci termenul următor va fi dat de scrierea lui 19
în baza 3:
19 = 2x3² + 0x3¹ + 1x30, deci termenul următor este
201. În şirul (b), termenii reprezintă, în scrierea în baza 9,
pe 2, pe 2², 2³,…,210, deci termenul următor este dat de
962

Rãni deschise

scrierea lui 211 în baza 9; nu este greu de văzut că se obţine
numărul 1357. În şirul (c), observăm că 2 apare exact ca
valoare a fiecărui termen de rang prim, de unde sugestia
(care se confirmă) că termenul de rang n ar putea fi
numărul divizorilor lui n; deci termenul următor este 4,
adică numărul divizorilor lui 8.
Să ne imaginăm acum mai multe numere dispuse
circular; la nord 58, la sud 20, la est 16, la nord-est 37, la
sud-est 4, la nord-vest 89 iar la sud-vest 42. Care este
numărul situat la vest, cunoscându-se faptul că sensul de
parcurgere este cel direct (adică invers mersului acelor de
ceasornic)? Este poate mai greu de observat aici că fiecare
termen se obţine ca sumă a pătratelor cifrelor care
alcătuiesc termenul precedent: 20 = 42 + 22, 4 = 22 + 02, 16
= 42, 37 = 12 + 62, 58 = 32 + 7², 89 = 5² + 8², deci termenul
plasat la vest este 8² + 9² = 145.
A pune în ecuaţie
În 1707, Newton a discutat următoarea problemă (cu
adaptările de rigoare): 12 vaci mănâncă 4/3 km² de iarbă în
4 săptămâni, 21 de vaci mănâncă 10 km² de iarbă în
9 săptămâni; câte vaci mănâncă 24 km² de iarbă în
18 săptămâni? (se presupune că iarba se află iniţial la acelaşi
nivel pe toată suprafaţa considerată, iar creşterea ierbii se
produce în mod uniform pe întreaga suprafaţă). Presupunând
963

Solomon Marcus

că la început iarba are înălţimea de 1 cm şi creşte cu x unităţi
pe săptămână şi admiţând că fiecare vacă mănâncă
echivalentul a y km² de iarbă de altitudine egală cu
1 cm, obţinem relaţiile (12 x 4)y= 10/3 (1+4x) şi (21 x 9)y
= 10 (1+9x). Problema constă în găsirea lui z pentru care
18yz=24(1+18x). Aici se încheie partea interesantă a
problemei; rezolvarea sistemului obţinut de trei ecuaţii cu trei
necunoscute se face automat (de exemplu, rezolvând sistemul
primelor două ecuaţii de gradul întâi cu două necunoscute şi
introducând soluţiile astfel obţinute în relaţia a treia, pentru
a-l obţine pe z). Constatăm deci că punerea în ecuaţie este o
etapă preliminară rezolvării sistemului. Această etapă cere
iniţiativă personală, concretizată aici în primul rând în
alegerea judicioasă a necunoscutelor x, y şi z. Punerea în
ecuaţie este un exerciţiu de o deosebită valoare intelectuală,
deoarece ne pregăteşte pentru acele activităţi denumite azi
modelare matematică. Nu mai este vorba de un exerciţiu de
virtuozitate matematică în sine, ci de relaţia matematicii cu
lumea; modelarea matematică este un act de invenţie prin care
gândirea matematică se articulează cu varietatea nelimitată a
lumii fizice, biologice, umane şi sociale.
O altă problemă care reclamă capacitatea de punere în
ecuaţie este următoarea: Să se găsească un număr inferior lui
100, astfel încât pătratul numărului este numărul răsturnat al
pătratului răsturnatului numărului iniţial. Cum exprimăm
algebric acest enunţ în cuvinte? Este ca şi cum ar trebui să
964

Rãni deschise

traducem un text dintr-o limbă în alta. Din faptul că numărul
căutat este inferior lui 100 deducem că reprezentarea zecimală
joacă un rol anumit în raţionament. Notând cu a cifra zecilor
şi cu b cifra unităţilor numărului căutat, pătratul numărului
căutat va fi (10a+b)²=100 a²+20ab+ b². Pătratul numărului
răsturnat va fi (10 b+a)²=100 b²+20ab+ a². Din faptul că (ab)²
trebuie să fie egal cu răsturnatul lui (ba)², deducem că a², b²
şi 2ab trebuie să fie cifre care să nu depăşească pe 9. Avem
deci următoarele posibilităţi: a=1, b=2; a=2, b=1; a=1, b=3,
a=3, b=1; cazul a=b=2 conduce la coincidenţa numărului cu
răsturnatul său. (Am exclus din discuţie posibilitatea prezenţei
lui zero. Chestiunea poate fi extinsă la numere inferioare lui
1.000 şi se găsesc drept soluţii perechile (102, 201), (112.
211), (103, 301), (122, 221), (113, 311)).
Atenţie la ipotezele ascunse!
A. Ce vârste au cei trei copii ai tăi? B. Produsul
acestor vârste este egal cu 36. A. Dar suma? B. Este egală
cu numărul de la casa vecinului. A. Mai am nevoie de o
informaţie! B. Copilul cel mare cântă la pian.
Cum a reuşit A să decidă, pe baza acestui dialog,
vârstele celor trei copii ai lui B? Care sunt aceste vârste?
Problema pare de tipul „Un vapor are 30 m lungime
şi 10 m lăţime; care este vârsta căpitanului?” Ea nu este
totuşi de acest tip, după cum vom vedea imediat.
965

Solomon Marcus

Să ne imaginăm în situaţia lui A. Deoarece vârsta este
considerată în ani întregi, nu intră în discuţie decât divizorii
lui 36. Aceşti divizori sunt, în ordine crescătoare, 1, 2, 3, 4,
6, 9, 12, 18, 36. Produsul 36 poate fi realizat în următoarele
feluri: 1, 2, 18; 1, 1, 36; 1, 3, 12; 1, 4, 9; 1, 6, 6; 2, 2, 9; 2,
3, 6; 3, 3, 4; sumele corespunzătoare acestor posibilităţi
sunt, respectiv, 21, 38, 16, 14, 13, 13, 11, 10. Din modul în
care este formulată informaţia privind numărul de la casa
vecinului, rezultă că nu valoarea acestui număr intervine în
raţionament, ci numai faptul ca atare al cunoaşterii valorii
sumei celor trei vârste pe care vrem să le aflăm. Această
observaţie trebuie corelată cu o alta: persoana A constată
că numai pe baza cunoaşterii produsului şi sumei nu poate
identifica vârstele copiilor. Singura posibilitate compatibilă
cu această situaţie este ca suma vârstelor să fie aceeaşi
pentru două combinaţii diferite de câte trei divizori ai lui
36, având ca produs pe 36. Examinarea combinaţiilor
inventariate mai sus arată că singurele triplete distincte
furnizând aceeaşi sumă sunt (1, 6, 6) şi (2, 2, 9). În acest
moment, îşi face efectul ultima informaţie furnizată de B.
Numai că această informaţie este mai bogată decât avem
nevoie şi, ceea ce este mai derutant, formularea ei pune
accentul tocmai pe partea ei nerelevantă pentru problema
în discuţie. Într-adevăr, faptul că unul dintre copii cântă la
pian are tot atâta legătură cu problema cât are legătură
vârsta căpitanului cu dimensiunile vaporului. Însă
966

Rãni deschise

presupoziţia exprimată cu acest prilej – aceea a existenţei
unui cel mai mare copil – este suficientă pentru a selecţiona
tripletul (2, 2, 9). Celălalt triplet de sumă 13, (1, 6, 6) se
exclude prin faptul că nu furnizează un „cel mai mare
copil”. Aici intervine şi o altă situaţie, care nu mai este de
ordin matematic, dar care nu rămâne fără urme matematice.
Într-adevăr, dacă doi copii au amândoi 6 ani, atunci aceştia
formează un cuplu de gemeni; pentru considerente de ordin
biologic, altă posibilitate nu există (excludem posibilitatea
ca bărbatul B să aibă copii din căsătorii diferite, caz în care,
desigur, cei doi copii de 6 ani ar putea fi departajaţi de
câteva luni care nu se reflectă în cifra rotundă de 6).
Vom da acum un alt exemplu de problemă în care o
ipoteză neexplicită, o presupoziţie mai ascunsă decât aceea
din problema anterioară, are un rol decisiv în obţinerea
soluţiei. Să presupunem că avem 31 de piese de domino,
de dimensiuni 2 x 1. Dorim să le aşezăm pe suprafaţa unei
table de şah, deci de dimensiuni 8 x 8, cu pătrăţele
alternativ albe şi negre. Fiecare piesă de domino va ocupa
două pătrate vecine ale tablei de şah. În mod inevitabil,
două pătrate vor rămâne neacoperite (diferenţa de la
31 x 2 la 64); se pune problema dacă există un aranjament
pe baza căruia pătratele neacoperite să fie plasate în două
vârfuri opuse (în diagonală) ale tablei de şah. În căutarea
răspunsului, există riscul de a ne angaja în nesfârşite
tentative combinatoriale. Este suficientă însă o simplă
967

Solomon Marcus

observaţie, pentru ca imposibilitatea unui aranjament de
tipul celui preconizat să apară cu toată claritatea. Numai
că această observaţie este condiţionată de luarea în
considerare a unei restricţii la care se supune tabla de şah
şi de fapt orice tablă de pătrate alternativ albe şi negre, de
dimensiuni 2n x 2n (n număr întreg pozitiv); două pătrate
aşezate la extremităţile unei diagonale au obligatoriu
aceeaşi culoare. Însă o piesă de domino ocupă totdeauna
două pătrate de culori diferite (deoarece aceste pătrate sunt
vecine); cum numărul total al pătratelor albe este egal cu
cel al pătratelor negre, rezultă că cele două pătrate rămase
libere au obligatoriu culori diferite.
Convergenţa şi continuitatea, în haine noi
Să ne referim la definiţia convergenţei unui şir (an)
către valoarea a: Pentru orice b>0 există N natural, astfel
încât dacă n>N atunci │an-a│< b. Să introducem acum o
modificare în această formulare, înlocuind ultima
inegalitate prin inegalitatea slabă de mai mic sau egal. Este
formularea astfel obţinută echivalentă cu proprietatea de
convergenţă? Răspunsul este afirmativ. Notând cu (1) şi
(2) cele două formulări, să arătăm mai întâi că (1) implică
(2). Într-adevăr, din │an-a│< b rezultă │an-a│≤ b şi, prin
ipoteză, din n>N rezultă │an-a│< b; prin tranzitivitate,
constatăm că n>N rezultă │an-a│≤ b, adică tocmai
968

Rãni deschise

proprietatea (2) care trebuia stabilită. Reciproc, să arătăm
că din (2) rezultă (1). Din │an-a│≤ b rezultă │an-a│< 2b
şi deoarece, conform lui (2), inegalitatea n>N atrage cu
sine │an-a│≤ b, rezultă, prin tranzitivitate, că pentru orice
b>0 există N astfel încât din n>N rezultă │an-a│< 2b,
adică tocmai proprietatea (1) (înlocuirea lui b cu 2b nu
schimbă situaţia, deoarece dacă b este un număr pozitiv
arbitrar, 2b este şi el un număr pozitiv arbitrar şi reciproc).
Fie acum proprietăţile (3): Pentru orice b>0 există d>0
astfel încât dacă │x - x0│<d atunci │f(x) – f(x0)│ <b şi
(4) obţinută prin înlocuirea, în (3), a inegalităţii │x - x0│<d
cu inegalitatea │x - x0│≤d. Să arătăm echivalenţa dintre (3)
şi (4). Dacă (4) are loc, atunci din │x - x0│≤d rezultă
│f(x) – f(x0)│ <b; însă < implică ≤, deci prin tranzitivitate
obţinem faptul că din │x – x0│ <d rezultă │f(x) – f(x0)│<b,
adică proprietatea (3). Reciproc, să admitem pe (3), cu alte
cuvinte din │x – x0│ <d rezultă │f(x) – f(x0)│ <b . Însă
din│x – x0│≤ d/2 rezultă │x – x0│ <d, deci, prin,
tranzitivitate, din │x – x0│≤ d/2 rezultă │f(x) – f(x0)│ <b,
adică avem proprietatea (4) (înlocuirea lui d prin d/2 nu
schimbă situaţia; d/2 parcurge mulţimea numerelor pozitive
exact atunci când d parcurge aceeaşi mulţime). Avem astfel
două variante distincte ale proprietăţii de continuitate.
Fie acum proprietatea (5): Pentru orice număr raţional
b strict pozitiv există un număr raţional d>0 astfel încât
din │x – x0│ <d rezultă │f(x) – f(x0)│ <b. Vom arăta că (5)
969

Solomon Marcus

este echivalentă cu (3). Să presupunem că are loc (5) şi fie
b0 un număr real > 0. Deoarece în orice interval există
numere raţionale, va exista un număr raţional b astfel încât
0 < b < b0. Conform lui (5), există d raţional > 0 pentru
care din │x – x0│ <d să rezulte │f(x) – f(x0)│ <b, deci cu
atât mai mult │f(x) – f(x0)│ <b0. Există deci, pentru orice
b0 real pozitiv, un număr real (chiar raţional) d>0 pentru
care din │x – x0│ <d să rezulte │f(x) – f(x0)│ <b0, adică
tocmai proprietatea (3). Reciproc, să admitem proprietatea
(3). Fie b0 un număr raţional pozitiv; fiind raţional, b0 este
şi real, deci există conform lui (3), un d real > 0 pentru
care din │x – x0│ <d să rezulte │f(x) – f(x0)│ <b0. Fie
d0 un număr raţional, 0 < d0 < d. Din │x – x0│ <d0
rezultă │x – x0│ <d, deci, prin tranzitivitate, rezultă şi
│f(x) – f(x0)│ <b0, deci are loc proprietatea (5).
Fie acum proprietatea: Pentru orice b≥0 există d>0
astfel încât, dacă │x - a│<d, atunci │f(x) – f(a)│ <b.
Definiţia continuităţii lui f în a a fost aici tulburată prin
înlocuirea inegalităţii stricte b>0 cu inegalitatea slabă b≥0;
modificare suficientă nu numai pentru a desfigura
proprietatea de continuitate, dar şi pentru a obţine o
proprietate imposibilă, în sensul că nicio funcţie nu o
satisface. Într-adevăr, pentru b=0 inegalitatea impusă lui f
devine falsă oricum am alege pe f.
Ce se întâmplă dacă proprietatea de continuitate este
tulburată prin intervertirea, în partea a doua, a valorilor b
970

Rãni deschise

şi d? Fie deci proprietatea: Pentru orice b>0 există d>0
astfel încât din │x - a│< b rezultă │f(x) – f(a)│ < d. Nu
este greu de văzut că orice funcţie mărginită satisface
această condiţie (dacă │f(x)│< M, atunci putem lua
d=2M).
Toate aceste chestiuni arată că modificări mici în
expresia unui concept pot determina schimbări majore de
sens. Dar oare nu se întâmplă la fel şi în vorbirea obişnuită,
unde o simplă virgulă poate schimba complet sensul unor
fraze?
Puţină gândire combinatorică
11 obiecte notate, cu litere de la A la K, trebuie
ordonate în aşa fel încât D şi E să fie alături, iar G şi H să
fie separate. Care este probabilitatea de a obţine o astfel
de ordonare, dacă se procedează la întâmplare?
Numărul total al permutărilor de 11 obiecte este factorial
de 11 (= 11 x 10 x 9 x 8 x 7 x 6 x 5 x 4 x 3 x 2). Numărul
acestor permutări în care D şi E se află alăturate este dublul
lui factorial de 10 (pentru fiecare din alăturările DE şi ED,
considerată ca un singur obiect, avem un număr de posibilităţi
egal cu numărul permutărilor de 10 obiecte, deci factorial
de 10). Numărul de permutări în care vom avea alături atât
pe D şi E, cât şi pe G şi H va fi, deci, egal cu produsul lui 4
cu factorial de 9. Însă, deoarece ne interesează acele permutări
971

Solomon Marcus

în care D şi E sunt alături, dar G şi H nu, va trebui să
considerăm diferenţa (10 ! x 2) – (9 ! x 4) = 9 ! x 16, deci
probabilitatea căutată va fi raportul (9 ! x 16) / (11 !) = 8/55.
Problema pe care o propunem acum se referă la un
cub care poate fi tăiat în 27 de cuburi mai mici, cu ajutorul
a şase secţiuni plane. Putem realiza aceeaşi operaţie
folosind un număr mai mic de secţiuni plane, dar
compensând aceasta cu posibilitatea ca între două
secţionări diferite două piese să poată schimba locul între
ele? Răspunsul este negativ, deoarece pentru a separa
cubul de la mijloc sunt necesare exact şase secţionări, câte
una de fiecare faţă.
Să ne referim acum la o problemă de dezvoltare
zecimală a unui număr. Fie un număr s cuprins între 0 şi 1,
obţinut în modul următor: pe locul de rang n după virgulă
figurează ultima cifră (deci cifra unităţilor) din scrierea
zecimală a lui n. Avem deci s=0,12345678901234567890…
Însă din s=1/10+2/102+3/103+… obţinem, prin
multiplicare cu 10, 10s=1+2/10+3/102+4/103+… Scăzând
pe s din 10s, obţinem 9s=1+1/10+1/102+1/103+…
=1,1111…=10/9, deci s=10/81=0,123456790 unde punctele deasupra lui 1 şi 0 marchează începutul şi sfârşitul
perioadei, cu alte cuvinte, secvenţa 123456790 se repetă
indefinit. Observăm absenţa cifrei 8.
Pentru a trece la o altă problemă, să ne imaginăm un
şir de încăperi numerotate consecutiv. Să presupunem că,
972

Rãni deschise

în momentul iniţial, toate camerele sunt descuiate, dar
cheile lăsate în broască. În momentul următor, încuiem
toate camerele de rang par. Apoi, manipulăm cheile
camerelor de rang divizibil cu 3, încuind pe cele descuiate
şi descuind pe cele încuiate. Continuăm în acest fel, deci
la etapa a n-a manipulăm cheile camerelor de rang divizibil
cu n, încuind pe cele descuiate şi descuind pe cele încuiate.
Există camere care rămân descuiate după acest şir infinit
de manipulări? În cazul afirmativ, care sunt acestea? Este
clar că rămân descuiate exact acele camere ale căror chei
au fost manipulate de un număr impar de ori. Însă numărul
de manipulări coincide cu numărul de divizori distincţi pe
care-i admite rangul camerei. Problema deci revine la
găsirea acelor numere naturale care admit un număr impar
de divizori. Însă singurele numere de acest fel sunt
pătratele perfecte 1, 4, 9, 16, 25, 36,… Într-adevăr, într-un
produs de forma pa x qb x … unde p,q… sunt numere
prime distincte, numărul de factori este egal cu (a + 1) x
(b + 1) x …, iar acest număr este impar numai dacă a,b,…
sunt numere pare.
Şi o recomandare la geometrie
Este greu de găsit, în materia de geometrie, un capitol
mai important decât cel privind transformările geometrice:
translaţia, rotaţia, simetria, izometria, omotetia,
973

Solomon Marcus

inversiunea şi atâtea altele, care depăşesc programa
şcolară. Transformările geometrice intervin în mod esenţial
în mecanică, astronomie, fizică, chimie, biologie,
inginerie, dar intervin şi în disciplinele umaniste şi sociale,
de la lingvistică până la muzică (Mihai Brediceanu şi-a
trecut o teză de doctorat pe tema transformărilor
geometrice şi topologice în compoziţia muzicală). Iată
numai unul dintre motivele pentru care considerăm foarte
bine-venită publicarea recentă, la Editura Ştiinţifică şi
Enciclopedică (1987), a cărţii lui Laurenţiu Duican şi Ilie
Duican Transformări geometrice (culegere de probleme),
exemplară prin varietatea şi bogăţia materialului.

974

