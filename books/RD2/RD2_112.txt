Matematica şi alegerea profesiunii
Columne, revista Liceului Agroindustrial Slobozia,
1985, p. 37
Alegerea unei profesiuni se face în bună măsură în
funcţie de relaţia în care te afli cu matematica.
De obicei, elevii buni la matematică, cei care au
încredere că vor putea face faţă unor discipline
matematizate, se îndreaptă spre facultăţi de profil ingineresc,
cibernetico-economic sau spre ştiinţe pozitive (matematică,
fizică, chimie); ceilalţi se îndreaptă cu precădere spre
specialităţi la care matematica sau este absentă, sau are o
prezenţă mai redusă (medicină, drept, agronomie, filologie,
filozofie-istorie, geologie-geografie etc.)
Desigur, excepţiile de la această regulă nu lipsesc şi
este de prevăzut că ele vor deveni tot mai numeroase,
transformând regula existentă într-o regulă nouă. Dar nu
vrem să discutăm aici legitimitatea acestei strategii într-o
perspectivă mai îndepărtată, în care nicio profesie nu va
mai putea evita matematica şi calculatoarele.
Fapt este că, în momentul de faţă, diferitele
specializări existente în învăţământul superior se disting
după exigenţele mai mari, în unele cazuri, mici sau cu totul
absente în altele, în ceea ce priveşte prezenţa matematicii
794

Rãni deschise

la examenul de admitere. Faţă de această situaţie, este
important pentru fiecare elev să evalueze corect
posibilităţile sale de a face faţă unor discipline de structură
matematică; aceste discipline nu se reduc la cele strict
matematice, cum ar fi algebra, analiza sau geometria, ci
includ o mare varietate de ştiinţe ale naturii (mecanică,
fizică teoretică, chimie teoretică) şi de discipline
inginereşti sau economice. S-ar părea că problema este
simplă şi că notele obţinute la matematică în şcoală, sunt
suficiente pentru a pune diagnostic corect asupra
capacităţii de gândire matematică a unui elev. Desigur,
aceste note nu pot fi ignorate, dar nici nu pot fi
transformate într-un indicator sigur al randamentului pe
care un tânăr îl va obţine la matematică, în învăţământul
superior.
Matematica predată în învăţământul superior este, în
cele mai multe cazuri, foarte diferită ca structură şi grad
de abstractizare de matematica pe care elevii o practică în
şcoală.
Lucrul acesta este mai greu vizibil în manuale. De
exemplu, între manualele de analiză matematică din
ultimele două clase de liceu şi cursul de analiză
matematică din anul întâi al unei facultăţi inginereşti de
construcţii, deosebirea este mai degrabă cantitativă decât
structurală.

795

