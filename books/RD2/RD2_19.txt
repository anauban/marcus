Matematica şi candoarea
Viaţa studenţească, 29 martie 1977
Cunoaşteţi Lecţia de aritmetică ( în original Page
d’écriture) a lui Jacques Prévert? Iat-o, într-o traducere
care sper că nu trădează prea mult textul original: Doi şi
cu doi fac patru/ patru şi cu patru opt/ opt şi cu opt fac
şaisprezece.../Repetaţi! spune profesorul/ Doi şi cu doi
patru/ patru şi cu patru opt/ opt şi cu opt fac şaisprezece/
Dar iată pasărea-liră/ cum trece pe cer/ un elev o vede,/ o
aude,/ o cheamă:/ salvează-mă/ joacă-te cu mine/ pasăre!/
Atunci pasărea coboară/ şi se joacă cu elevul/ Doi şi cu
doi patru.../ Repetaţi! spune profesorul/ şi elevul se joacă/
pasărea se joacă cu el.../ Patru şi cu patru opt/ opt şi cu
opt fac şaisprezece/ iar şaisprezece şi cu şaisprezece cât
fac oare?/ Nu fac nimic şaisprezece şi cu şaisprezece/ şi,
mai ales, nu fac treizeci şi doi,/ oricum am lua-o/ şi pleacă
amândoi./ Şi elevul ascunde pasărea/ în pupitrul său/ şi
toţi elevii/ ascultă cântecul ei/ şi toţi elevii/ ascultă această
muzică/ iar opt şi cu opt, la rândul lor se duc/ şi patru şi
cu patru şi doi şi cu doi/ se fac nevăzuţi/ şi unu şi cu unu
nu mai fac nici unu nici doi/ se cărăbănesc şi ei/ Şi
pasărea-liră cântă/ şi profesorul strigă:/ Termină odată
paiaţă!/ Însă toţi ceilalţi elevi ascultă cântecul/ şi pereţii
159

Solomon Marcus

clasei se prăbuşesc încet./ Şi geamurile redevin nisip/
cerneala redevine apă/ pupitrele sunt din nou copaci/ creta
e din nou faleză/ iar tocul e din nou pasăre.
Apărut în urmă cu peste 40 de ani, într-un moment în
care spaimele şi oboseala războiului nu se risipiseră încă
şi când nu credeam că mă voi mai putea înfiora în faţa unei
ficţiuni, poemul lui Prévert rămâne, pentru mine, la fiecare
nouă lectură, la fel de revelator şi de interogativ.
În 1962, am citit, într-o carte a Agathei GrigorescuBacovia (Bacovia, viaţa poetului, Editura pentru
Literatură, 1962, p. 62), această evocare a anilor de
gimnaziu ai lui Bacovia: „Profesorul se urca pe catedră cu
un aer plictisit, striga catalogul, punea câte un elev să
spună lecţia ad litteram... Suna clopoţelul şi altul intra la
alt obiect, fără să explice... Cel de matematică făcea
problemele singur la tablă, nu lucra cu clasa. Apoi le da
acasă problemele ce trebuia să le rezolve cum s-or
pricepe... Cu matematica se necăjea foarte mult. Îmi
spunea poetul, cu sinceritate, că nu a reţinut din liceu nicio
teoremă...” (Efectul la distanţă: În 1930, între Bacovia şi
Dimitrie Pompeiu, marele nostru matematician, avea loc
următorul dialog:
– Dumneavoastră sunteţi profesor de matematică?
– Da, dar matematica iubeşte şi înţelege poezia...
– Poezia nu prea iubeşte matematica şi nu prea o
înţelege...)
160

Rãni deschise

Doi ani mai târziu, am aflat următoarele despre
Eminescu Mihai, elev în vârstă de 12-13 ani (G. Călinescu,
Viaţa lui Mihai Eminescu, Editura pentru Literatură, 1964, p.
60): „În primul rând nu se împăca cu matematicile şi de aceea
se învoise cu Const. Ştefanovici, care a şi devenit profesor de
matematici, ca acesta să-i facă temele iar el să-i spună
poveşti”. G. Călinescu reproduce apoi cuvintele poetului: „Eu
ştiu chinul ce l-am avut însumi cu matematicile în copilărie
din cauza modului rău în care mi se propunea... N-ajunsesem
nici la vârsta de douăzeci de ani să ştiu tabla pitagoreică,
tocmai pentru că nu se pusese în loc judecata, ci memoria. Şi
deşi aveam o memorie fenomenală, numere nu puteam învăţa
deloc pe de rost, încât îmi intrase-n cap ideea că matematicile
sunt ştiinţele cele mai grele de pe faţa pământului”.
Exemplele ar putea continua. Voi prefera însă să-mi
amintesc că în urmă cu 20 de ani am citit la gazeta de perete
a Facultăţii de Matematică următoarele versuri aparţinând
unui student pe nume Mircea Trifu: A venit primăvara,
prieteni/ Sunt vesel./ Vă propun să lăsaţi pentru o clipă/
inegalităţile Bessel/ Şi să priviţi pe fereastră,/ Afară./ Nu
ştiu dacă înţelegeţi/ Importanţa evenimentului:/ E
primăvară!/ Nu ştiu dacă ştiţi să purtaţi o floare./ Nu-mi pot
da seama./ Asta se învaţă cu totul altfel/ Decât funcţia
Gamma./ Nici eu nu ştiu/ Dar vom învăţa împreună./ E drept
că nu există ”Manual/ Pentru cel care-nvaţă singur/ Să
aducă flori”,/ Dar există o imensitate de lumini/ Şi culori./
161

Solomon Marcus

Strada e plină de fete frumoase/ Cu ochi luminoşi... Vom fi
la-nceput, desigur, stângaci/ Şi – de ce să nu spunem? –
puţin caraghioşi/ Dar vor trece toate,/ Ca fierbinţeala unei
răceli de-o zi/ Şi vom termina prin a şti/ Să purtăm cu
emoţie/ şi puţină nesiguranţă, o floare/ Hai să uităm o clipă/
Că ne pricepem – cât de cât – la geometrie/ Şi la funcţii
reale/ Şi dimineaţa, ca-n copilărie/ La şcoala primară/ Să
ne ridicăm în picioare:/ Bună dimineaţa, primăvară!
Mai rămâne să spun că în urmă cu vreo 14 ani, într-o
convorbire cu Sânziana Pop, publicată în Luceafărul,
prestigiosul compozitor Dimitrie Cuclin, recunoscând
structura matematică a muzicii, considera totuşi că un
compozitor nu trebuie s-o ia în seamă, pentru ca nu cumva
să fie anulată acea stare de candoare fără de care creaţia
artistică nu poate fi concepută.
Fără îndoială, tipologia proceselor de creaţie poate fi
foarte variată şi poate că unul din tipuri corespunde celui
la care se referă Cuclin. Dacă nu cumva opinia sa este
reflexul îndepărtat al unei şcolarităţi cu profesori de
matematică din încrengătura pe care am evocat-o mai sus.
Dar dacă chiar un student matematician crede că starea de
candoare a celui care oferă o floare s-ar putea destrăma la
primul contract cu universul matematic, o întrebare: Oare
creaţia matematică ar fi cu putinţă fără candoare?

162

