Matematica, „industria grea” a ştiinţei
Ştiinţă şi tehnică, anul XIX, seria a II-a, nr. 12, 1967, p. 8, 31
Matematica este o ştiinţă în care predomină
cercetarea fundamentală; ea a fost comparată uneori cu
industria grea, care-şi manifestă utilitatea pe cale indirectă,
mediată, producând maşinile cu ajutorul cărora se fabrică
bunurile de consum.
Legături mai directe cu producţia sunt dezvoltate la
Centrul de statistică din Bucureşti şi la Institutul de calcul
din Cluj. Aceste institute de cercetări au încheiat
convenţii de colaborare cu diferite întreprinderi şi
instituţii în vederea aplicării metodelor statistice moderne
şi a tehnicii moderne de calcul în procesul de producţie.
Alături de unele rezultate pozitive, trebuie observat că
unele ministere, instituţii şi întreprinderi manifestă o
timiditate nejustificată faţă de aplicarea unor metode noi,
promovate de ştiinţă, şi solicită în mod insuficient
sprijinul matematicienilor. Neîncrederea, incapacitatea
de a depăşi activitatea rutinară sunt piedici care se cer
înlăturate.
Pentru elaborarea planurilor anuale, cincinale şi a
schiţelor de perspectivă şi urmărire a modului în care se
realizează prevederile acestora, este necesar un flux
35

Solomon Marcus

informaţional raţional pe toate treptele de organizare.
Perfecţionarea metodelor de cunoaştere şi analiză a
realităţilor din economie se poate realiza prin introducerea
pe scară tot mai largă a metodelor moderne de calcul...
Multiple date şi fapte rezultate din practica noastră şi a
altor ţări arată că în această direcţie au o importanţă
deosebită introducerea şi promovarea metodelor
matematice de calcul şi analiză, precum şi utilizarea largă
a tehnicii electronice de calcul, cu ajutorul cărora sunt
posibile rezolvarea unor probleme de mare complexitate,
efectuarea într-un termen scurt a mai multor variante de
plan şi stabilirea soluţiilor optime.
Trebuie să observăm însă că în promovarea
legăturilor dintre matematică şi celelalte ştiinţe, dintre
matematică şi producţie există şi piedici care ţin de
organizarea uneori defectuoasă a procesului de
învăţământ. Apariţia domeniilor interdisciplinare
reclamă organizarea unui învăţământ interdisciplinar,
care să dea posibilitatea formării unor specialişti cu un
adevărat profil mixt, corespunzător actualei etape de
dezvoltare a ştiinţei. În locul unor secţii stabile şi
consolidate, cu un examen de admitere specific şi cu un
plan de învăţământ cu adevărat interdisciplinar, de-a
lungul tuturor anilor de studiu, facultăţile noastre dispun,
în cel mai bun caz, de cursuri speciale la alegere, care în
ultimii ani de studiu dau studenţilor o iniţiere cu totul
36

Rãni deschise

insuficientă pentru ca ei să se poată forma în spirit
interdisciplinar.
Multiple sunt căile prin care matematica românească
asimilează ştiinţa matematică mondială, căutând să fie
mereu la zi. Acest proces de asimilare începe chiar de pe
băncile universităţii; cursurile care se predau aici îl aduc
pe student în anticamera cercetării matematice curente.
Bibliotecile primesc zilnic cele mai noi reviste şi cărţi de
pe toate meridianele globului. Asupra acestor reviste şi
cărţi se apleacă zilnic privirile matematicienilor, începând
cu studentul care-şi pregăteşte un referat pentru cercul
ştiinţific până la profesorul cărunt.
Dar lucrurile nu se opresc aici. În fiecare zi, poştaşul
depune la Institutul de matematică al Academiei o mare
cantitate de scrisori şi pachete, expresie a corespondenţei
deosebit de bogate pe care matematicienii noştri o întreţin
cu matematicienii de peste hotare. Contactele personale,
vizitele reciproce, planurile de colaborare între Academii
stabilesc un flux continuu de la matematicienii noştri la cei
de peste hotare şi invers.
Deosebit de important este faptul că noi reuşim, în
matematică, nu numai să importăm, să asimilăm valori,
dar să şi exportăm, pe piaţa cea mai pretenţioasă a ştiinţei
mondiale, unele produse ale activităţii noastre de cercetare
în domeniul matematicii. Numeroşi matematicieni români
sunt invitaţi de către mari universităţi din străinătate să
37

Solomon Marcus

predea sau să conducă seminarii ştiinţifice, să conlucreze
cu matematicieni străini, să prezinte comunicări şi să
participe la congrese sau seminarii de specialitate.
Numeroşi matematicieni români sunt solicitaţi de către
prestigioase edituri străine să publice monografii
matematice de specialitate.
Periodice de prestigiu de pe toate meridianele
globului, din Canada până în Japonia, publică articole ale
matematicienilor români. Dar, chiar pentru articolele şi
cărţile publicate în româneşte, interesul este atât de mare,
încât o editură din Statele Unite a publicat de curând un
dicţionar şi o gramatică pentru iniţierea cititorului de limbă
engleză în limba textelor matematice româneşti.
Şcoala matematică românească se menţine la un nivel
care-i dă posibilitatea să intre în competiţie cu cele mai
înaintate şcoli matematice de peste hotare (SUA, URSS,
Franţa, Anglia, RFG, Japonia etc.). În prima parte a
secolului nostru, clasici ai matematicii româneşti, ca
Dimitrie Pompeiu, Gh. Ţiţeica, Traian Lalescu şi apoi
Simion Stoilow au creat o operă de pionierat ştiinţific, prin
care ştiinţa noastră a dobândit un deosebit prestigiu în
lume. Sub influenţa acestora s-au format maeştri
recunoscuţi ai şcolii noastre matematice actuale,
academicienii Miron Nicolescu, V. Vâlcovici, O.
Onicescu, G. Vrănceanu, G. Mihoc, T. Popoviciu, Gr.
Moisil, N. Teodorescu, C. Jacob. În jurul acestor
38

Rãni deschise

personalităţi s-au format şcoli puternice, în cadrul cărora
absolvenţi ai Universităţii, studenţi dintre cei mai străluciţi,
lucrează alături de reprezentanţii generaţiei de mijloc. Un
învăţământ matematic universitar de cea mai înaltă
calitate, o excepţională activitate de îndrumare şi
conlucrare, de ucenicie ştiinţifică a începătorilor pe lângă
cei deja formaţi au creat tinerelor noastre talente
matematice o mare capacitate de orientare către
problemele esenţiale, de adaptare la metodele noi care sunt
în curs de elaborare. Această „transmitere a ştafetei” de la
o generaţie la alta se concretizează, de multe ori, în studii
elaborate în colaborare de matematicieni aparţinând unor
generaţii foarte diferite. Nu este vorba aici de colaborări
întâmplătoare, ci de faptul că tinerii preiau de la profesorii
lor problematica şi metodele de lucru, căutând să le ducă
pe o treaptă mai înaltă şi formând, la rândul lor, puternice
colective de cercetare.
Tânăra generaţie de matematicieni a reuşit nu numai
să menţină şi să dezvolte moştenirea ştiinţifică a
maeştrilor, dar să şi iniţieze cercetări în numeroase
domenii relativ mai recente, în care deci nu putea exista o
tradiţie. Vom da exemplul topologiei algebrice, în care
foarte tineri cercetători au reuşit să se facă de pe acum
cunoscuţi şi apreciaţi în cercurile de specialişti din
numeroase ţări, fiind mereu invitaţi pentru conferinţe şi
schimb de experienţă.
39

Solomon Marcus

Una din tendinţele manifestate ale evoluţiei ştiinţei în
ultima vreme este aceea a dezvoltării tot mai impetuoase
a domeniilor interdisciplinare, a creşterii rolului metodelor
matematice în cele mai variate domenii. Oameni de ştiinţă
dintre cei mai diferiţi se văd puşi în faţa unor probleme
apropiate. Este de prevăzut că acest proces se va accentua
şi matematica va fi din ce în ce mai datoare celorlalte
ştiinţe.

40

