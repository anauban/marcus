Lectură, inducţie şi circularitate
Ramuri, nr. 9 (267), 15 septembrie 1986, p. 12
Într-un articol mai vechi, „Lingvistica, ştiinţă pilot”
(Studii şi cercetări lingvistice, vol. 20, 1969, no. 3,
pp. 235-245), pornind de la faptul că limbajul articulat este
termenul fundamental de referinţă în studiul oricăror alte
limbaje umane (aşa cum spaţiul euclidian este termenul de
referinţă în studiul oricăror altor spaţii pe care ştiinţa sau
arta le reclamă), căutam să legitimez rolul metodologic
deosebit pe care lingvistica îl are faţă de orice obiect,
ştiinţific sau artistic, în care este implicată o structură de
limbaj. Cercetările pe care le evocăm aici se înscriu în
programul trasat atunci, deoarece operează un transfer de
concepte şi metode din lingvistică în alte sisteme
semiotice. Roland Barthes a avut dreptate; din punct de
vedere metodologic, întreaga semiotică este subordonată
lingvisticii. În ceea ce priveşte afirmaţia lui Roman
Jakobson, după care lingvistica este matematica ştiinţelor
sociale, putem adera la ea ca la o metaforă având
următoarea semnificaţie: lingvistica este tot atât de
importantă pentru ştiinţele sociale, cât este matematica
pentru ştiinţele naturii. Dacă însă înţelegem aceasta în
sensul că, pentru domeniul social, lingvistica înlocuieşte
901

Solomon Marcus

matematica, atunci greşim profund şi bănuim că nici
Jakobson nu s-a gândit la aşa ceva. Ori de câte ori este
vorba de un transfer de concepte sau metode dintr-un
domeniu în altul, matematica, dacă nu este indispensabilă,
este în orice caz utilă, deoarece modelarea matematică
permite conceptelor şi metodelor să se separe de aspectele
conţinutistice ale domeniului din care ele provin, pentru a
căpăta acea relativă independenţă datorită căreia ele pot
trece în alte domenii decât cel de origine. Aşa s-a întâmplat
cu conceptul de entropie (transferat din fizică în teoria
informaţiei) sau cu trăsăturile distinctive binare
(transferate din fonologie în diferite ştiinţe sociale). Este
adevărat că, uneori (a se vedea, de exemplu, cercetările lui
Claude Lévi-Strauss), acest transfer este realizat cu
ajutorul lingvisticii structurale; chiar şi în aceste cazuri se
apelează, pentru desăvârşirea descrierilor, la matematicieni
(a se vedea articolul lui André Weil pe marginea
cercetărilor lui Lévi-Strauss). Dar rolul matematicii în
ştiinţele sociale nu se reduce la cel de catalizator al unor
transferuri.
Să revenim la lectura generativă. Ea este un exemplu
tipic de transfer al unei metode lingvistice în domenii
nelingvistice, transfer realizat cu ajutorul diferitelor
modele matematice de gramatici generative. Numai că, în
acest caz, raza de acţiune este mult mai mare decât o
bănuia chiar şi Roman Jakobson. Într-adevăr, lectura
902

Rãni deschise

generativă nu se referă numai la domeniul umanist-social
(fie el ştiinţific sau artistic), ci şi la ştiinţele naturii şi la
activităţile practice ale oamenilor. Două exemple.
Maniera în care unităţile izoprene sunt concatenate în
structurile chimice izoprenoide aciclice comportă o lectură
generativă mai complicată, cu ajutorul gramaticilor
picturale de tipul web.
O lectură generativă interesantă comportă acele jocuri
sportive care, cel puţin în principiu, nu sunt limitate în timp
(Gabriela Sfarti, „Tennis game as a language”, Revue
Roumaine de Linguistique, vol. 25, 1980, no. 3). Nu
ne-am referit la teatru, care, datorită bogăţiei de exemple
pe care le oferă, va face obiectul unei discuţii speciale.
Exemplele pot continua, din domenii dintre cele mai
variate (artă culinară, film, vise, mituri etc.).
Atât analogia cu şirurile numerice, cât şi situaţiile
discutate mai sus sau la care s-a făcut referinţă pun în
evidenţă legătura strânsă care există între lectura
generativă şi raţionamentul prin inducţie. După cum se
ştie, inducţia constă în trecerea de la câteva cazuri
particulare la cazul general. Lectura generativă presupune
şi ea o ridicare de la particular la general, deoarece o regulă
generativă dă posibilitatea să se repete indefinit, deci să se
generalizeze, un fenomen care, în opera considerată, apare
numai de un număr finit de ori, uneori o singură dată, deci
este de natură particulară.
903

Solomon Marcus

Dar dacă între inducţie şi lectura generativă legătura
este atât de strânsă (aproape că putem spune că a doua este
un caz particular al celei dintâi), devine inevitabilă problema
modului în care se manifestă, în cadrul lecturii generative,
faimosul paradox al inducţiei, pus în evidenţă de Carl
Hempel şi Nelson Goodman. Aşa cum am arătat în
Paradoxul (Editura Albatros, 1984), inducţia trebuie
interpretată nu ca o trecere de la particular la general, ci ca
o interacţiune a particularului cu generalul. Atâta vreme cât
particularul este exprimat cu ajutorul limbajului şi al
conceptelor, printre presupoziţiile lui intră, măcar ca o
sugestie, un anumit mod de a-l citi, deci de a-l generaliza.
Când scriu 1, 2, 3, am şi sugerat o progresie aritmetică, ideea
ei se află deja în capul meu atunci când mi se cere s-o
continui. Aşa se explică faptul că 99% din cei care citesc
şirul finit 1, 2, 3, îl interpretează drept şirul numerelor
naturale. Din punctul de vedere al lecturii generative,
aceasta înseamnă că autorul dispune de multe posibilităţi
de a influenţa lectura operei sale, de a o orienta în direcţia
pe care el o preferă sau de a miza alternativ pe mai multe
direcţii, în funcţie de diferite straturi ale publicului cititor.
Această capacitate a autorului de a prevedea nivelurile de
lectură ale operei sale condiţionează de multe ori şi
succesul public. Astfel, este probabil că Umberto Eco a
intuit faptul că romanul său Numele trandafirului va fi luat,
de către o anumită categorie de cititori, ca un simplu roman
904

Rãni deschise

poliţist şi, mai mult, că şi-a organizat romanul pe baza
acestei strategii. Chiar dacă nu a procedat în mod deliberat,
o anumită stare de spirit şi un anumit nivel de informaţie,
cultură şi inteligenţă existente într-o bună parte a
publicului cititor făceau inevitabilă această situaţie. Dar
starea de spirit şi nivelul de cultură dintr-o societate includ
şi pe cititor. Desigur, ele sunt restricţii de natură generală,
care privesc pe toţi (autorul, la rândul său, ştie, şi cel puţin
inconştient, nu poate să rămână neinfluenţat de această
realitate socială); ceea ce interesează aici este faptul că, în
alegerea aspectelor prin care citeşte un text, cititorul este
considerabil limitat de o serie de factori pe care el, în
procesul lecturii, nu-i poate influenţa şi cu atât mai puţin
controla. Aceşti factori, în parte sub controlul autorului,
restrâng atât de mult alegerea pe care cititorul o poate
efectua, ca punct de plecare în construirea unor prelungiri
şi variante, încât aproape că-i impun cititorului (la anumite
niveluri de informaţie şi cultură) anumite reguli, anumite
variante şi prelungiri. Cu alte cuvinte, pentru un atare
cititor, presupoziţiile lecturii sunt atât de puternice, încât
aproape includ regulile cu care el îşi va construi această
lectură. În ceea ce-l priveşte pe cititorul avizat, el va fi mai
puţin tiranizat de aceste presupoziţii, dar nici el nu va putea
neutraliza efectul lor. În mare, paradoxul lecturii
funcţionează într-o formă asemănătoare cu paradoxul
inducţiei. Sensurile şi regulile sunt construite prin lectură,
905

Solomon Marcus

pe baza operei şi în limitele create de aceasta, dar în acelaşi
timp opera şi autorul caută să impună anumite sensuri şi
anumite reguli. Particularul operei conţine, întrucâtva, în
presupoziţiile sale, modul în care opera, în infinitatea ei
potenţială, va fi generată prin lectură. Pentru o persoană
care nu are în câmpul obişnuit al atenţiei noţiunea de
număr prim şi poate că a şi uitat definiţia ei, lectura şirului
1, 2, 3 ca şir al numerelor prime se exclude de la sine. Cu
atât mai puţin probabilă devine lectura acestui şir ca şir al
lui Fibonacci, noţiune foarte puţin cunoscută. În acest fel,
lectura de tipul „şirul numerelor naturale” aproape că se
impune, aşa cum lectura „roman poliţist” s-a impus în bună
măsură şi ea în cazul romanului lui Eco, pentru un mare
număr de cititori.
Werner Heisenberg („Quantum Mechanics and a Talk
with Einstein, 1925-1926”, în Physics and Beyond, Harper
and Row, New York, 1971, pp. 59-69) relatează că,
vorbindu-i lui Einstein, la prima sa întâlnire cu acesta,
despre procedarea ştiinţei de la observări şi măsurări la idei
generale, autorul teoriei relativităţii i-a replicat că tocmai
reciproca este adevărată: omul de ştiinţă pleacă, în
investigaţia sa, de la o anumită prejudecată, de la un
anumit model, pe care le proiectează asupra celor
observate; cu alte cuvinte, ceea ce este observat este, în
bună măsură, determinat de ideea preconcepută cu care
observatorul pleacă la drum. Cine a avut dreptate:
906

Rãni deschise

Heisenberg sau Einstein? Acum ştim că amândoi. Aşa cum
am arătat în Paradoxul (pp. 143-144), comentând
contribuţiile lui Carl Hempel şi Nelson Goodman, natura
inducţiei este esenţial circulară, interacţiunea dintre
observator şi observat, dintre subiect şi obiect, dintre
lectură şi operă fiind esenţială.
Două aspecte ni se par importante în cele de mai sus.
Unul se referă la universalitatea lecturii generative, altul
la inevitabilitatea ei. Matematica şi chimia, biologia şi
economia, psihologia şi praxiologia, informatica şi
lingvistica, istoria şi relaţiile internaţionale, sportul şi arta
culinară, pe de o parte, folclorul şi teatrul, poezia şi
romanul, muzica şi artele vizuale, pe de altă parte, au intrat
deja în raza de acţiune a lecturii generative. Reviste ca
Revue Roumaine de Linguistique – Cahiers de
Linguistique Théorique et Apliquée, Poetics (Olanda),
Semiotica (USA.), Ars Semeiotica (USA.), Foundations of
Control Engineering (Polonia), Degrés (Belgia) au
publicat numeroase studii de acest fel, în mare parte
aparţinând unor cercetători români.
Caracterul inevitabil al lecturii generative rezultă din
natura intelectului uman, din modul în care se articulează
empiricul cu teoreticul şi secvenţialul cu nesecvenţialul.
Situaţia observatorului în fizica cuantică şi lămurirea
statutului logic al inducţiei au contribuit esenţial la
cristalizarea universalităţii şi inevitabilităţii lecturii
907

Solomon Marcus

generative. Practicăm această lectură chiar atunci când
dorim să ne luăm distanţa faţă de ea. Lectura generativă
poate fi, desigur, superficială sau profundă, cu o mai mică
sau mai mare capacitate explicativă, conştientizată sau
implicită, descoperită a posteriori sau adaptată a priori.
Putem respinge o anumită lectură generativă, nu lectura
generativă ca atare. Cu cât această lectură devine mai
explicită, cu atât ea devine mai vulnerabilă, deoarece
devine mai clar şi mai probant ce anume lasă ea deoparte.

908

