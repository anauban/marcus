Matematica, matematicienii şi lumea modernă
Simpozion internaţional, Revista Astra, nr. 1(114),
martie 1979, pp. 4-5
Redacţia: Procesul revoluţionar de integrare a
învăţământului românesc cu cercetarea şi producţia a
implicat şi implică în continuare o dezvoltare deosebită
a studiului ştiinţelor matematice. De altfel, dovadă a
dinamismului vieţii noastre sociale, matematica
românească ocupă un rol însemnat în ansamblul ştiinţelor
contemporane, contribuind la dezvoltarea acestora atât
prin lucrări de cercetare fundamentală (de ex. în teoria
operatorilor, în teoria potenţialului, în topologia
diferenţială, în studiul spaţiilor vectoriale normate, în
teoria categoriilor etc.), cât şi prin unele de cercetare
aplicativă (de ex. în plasticitate, în cercetarea
operaţională, în biologia matematică, în lingvistica
matematică, în prospectivă etc.). Date fiind acestea,
precum şi faptul că Universitatea din Braşov a găzduit în
ultima vreme câteva simpozioane de interes naţional şi
internaţional, revista noastră a adresat mai multe întrebări
unor personalităţi marcante ale matematicii româneşti şi
străine care au luat parte la lucrările lor. Au avut
amabilitatea să răspundă invitaţiei noastre: prof.
341

Solomon Marcus

Dr. Gh. Chiş, de la Universitatea „Babeş-Bolyai” din
Cluj-Napoca, dr. H. S. Krishnaswamy, de la Universitatea
din Bangalore, India, prof. dr. doc. Solomon Marcus, de
la Universitatea din Bucureşti, acad. Gh. Mihoc, prof. dr.
doc. Jean Mittas, de la Universitatea „Aristotel” din
Salonic, Grecia, conf. Dr. Liviu Sofonea, de la
Universitatea din Braşov, dr. Wieslaw Stanczak, de la
Institutul de informatică al Academiei poloneze de ştiinţe
din Varşovia, acad. N. Teodorescu, ing. M. Wali Tabiat,
de la Institutul politehnic din Kabul, Afganistan, prof. dr.
doc. F. Ünlü, de la Universitatea Ege, Turcia.
Întrebările noastre se refereau la locul şi rolul
matematicii în şcoală, aplicaţiile matematicii în viaţa
socială, influenţa matematicii asupra gândirii umane,
matematica şi marile probleme ale omenirii.
S.M.: Matematica şcolară trece în întreaga lume
printr-o perioadă grea, de căutări care nu de puţine ori
eşuează. Singura ei şansă, de a-şi regăsi rostul ca
disciplină de cunoaştere a realităţii şi de formare a
gândirii, este de a se orienta spre faptele ale căror surse
intuitive, semnificaţii, motivaţii, pot fi prezentate fără
amânare, într-o formă simplă, care se depărtează de
limbajul natural numai atât cât este necesar. Matematica
şcolară trebuie să-şi manifeste de la primii paşi virtuţile
ei interdisciplinare, lucru perfect posibil, dar încă
insuficient valorificat.
342

Rãni deschise

Popularitatea actuală a matematicii se explică în
primul rând prin considerabila utilizare socială a maşinilor
electronice de calcul. Dar societatea nu ştie încă decât în
mică măsură ce să facă cu matematicienii, utilizând
deocamdată numai o mică parte din potenţialul lor creator,
iar matematicienii găsesc încă greu un limbaj comun cu
ceilalţi specialişti. Eforturile care se fac în acest sens din
ambele direcţii nu au rămas nici până acum fără rezultat.
Dacă dăm de o parte numeroasele manifestări de snobism
matematic (şi el, în fond, o formă de simpatie!), rămân
totuşi destule exemple semnificative de contaminare reală
a creaţiei ştiinţifice, tehnice sau artistice de gândirea
matematică autentică.
Se pun mari speranţe în aportul matematicii la
soluţionarea problemelor dezvoltării sociale. Nici
rezultatele obţinute nu sunt de neglijat. În urmă cu un an,
a apărut, sub egida Academiei de ştiinţe sociale şi politice,
un volum dedicat modelelor matematice în studiul
relaţiilor internaţionale; acum se află în curs de apariţie un
alt volum, dedicat modelelor matematice în studiul
organizaţiilor internaţionale. Am participat în mai 1978 la
o consfătuire internaţională privind problema nevoilor
umane şi mi-am dat seama că matematica oferă în această
privinţă nu numai un cadru conceptual adecvat, dar şi un
instrument esenţial pentru evidenţierea parametrilor care
intervin în studiul calităţii vieţii şi al indicatorilor sociali.
343

Solomon Marcus

Studiul problemelor dezvoltării pune în mişcare ramuri
dintre cele mai moderne ale matematicii calitative, cum ar
fi topologia, logica matematică, teoria catastrofelor, teoria
matematică a învăţării.

344

