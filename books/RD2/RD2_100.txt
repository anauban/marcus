Semiotica, azi (I)
Ramuri, nr. 2(248), 15 februarie 1985, pp. 1-4
Menită deopotrivă să entuziasmeze şi să scandalizeze,
semiotica este un ansamblu de preocupări ale căror
antecedente merg departe în trecut, dar care s-au cristalizat
în dezvoltări sistematice abia în a doua parte a secolului
precedent, pentru a lua amploare şi a se generaliza în
ultimele decenii. Având ca obiect de studiu geneza şi
funcţionarea sistemelor de semne, în natură şi societate,
semiotica este peste tot, fără ca aceasta să însemne că orice
este semiotică. Într-adevăr, semnul nu este o calitate în sine
a unui obiect, ci o funcţie pe care obiectul o poate dobândi
sau pierde. Rezultă că potenţialul semiotic al unui obiect
este cu atât mai interesant, cu cât el este capabil să trimită
la obiecte diferite de el. Rezultă că semiotica este o
preocupare esenţialmente relaţională şi nu întâmplător ea
valorifică ideile, rezultatele şi metodele disciplinelor în
care gândirea structurală a căpătat o anumită maturitate:
lingvistica, matematica, teoria informaţiei, teoria
sistemelor, biologia, psihologia. În momentul de faţă,
semiotica are toate caracteristicile exterioare ale unei
discipline actuale: îi sunt dedicate numeroase periodice şi
monografii, congrese, simpozioane şi conferinţe, iar în
706

Rãni deschise

numeroase universităţi sunt organizate cursuri şi seminarii
de semiotică. În 1984, a avut loc la Palermo cel de-al
treilea congres al Asociaţiei Internaţionale de Semiotică,
după ce, în 1979, avusese loc la Viena cel de-al doilea
congres, iar în 1974, la Milano, primul congres al acestei
Asociaţii. Volumele congreselor de la Milano şi Viena
arată clar amploarea acestor preocupări şi evoluţia lor
vertiginoasă. Dacă actele congresului de la Milano ocupă
un volum de peste 1 200 de pagini, actele congresului de
la Viena ocupă trei volume totalizând în jur de 2 000 de
pagini. Secţiunile acestor volume dau seamă despre
varietatea preocupărilor de semiotică. Aspecte generale,
fundamente, semiotică lingvistică, limbaje ştiinţifice,
metode exacte în estetică, semantică şi pragmatică,
semiotică literară, iconicitate şi arte vizuale, cinema,
televiziune, teatru, arhitectură, muzică, semiotica culturii,
comportamente neverbale, psihopatologie. Încă din
deceniul al şaptelea, se evidenţiază câteva centre care
polarizează atenţia semioticienilor. În URSS, Moscova
(V.V. Ivanov) şi Tartu (I.M. Lotman); în Polonia, Varşovia
(I. Pelc); în Cehoslovacia, Praga (P. Sgall), Bratislava
(I. Horeky) şi Brno (I. Osolsobe); în RFG, Stuttgart
(M. Bense); în Italia funcţionează Centrul Internaţional de
Semiotică şi Lingvistică de la Urbino, unde se organizează
în fiecare vară stagii internaţionale şi se desfăşoară o
bogată activitate editorială reflectată şi în volumul de faţă;
707

Solomon Marcus

la Paris trebuie relevate în primul rând preocupările din
jurul lui A.J. Greimas (a se vedea volumul Sémiotique
L’école de Paris, Classique Hachette, 1982, sub redacţia
lui J.C. Coquet); în SUA, focarul principal al preocupărilor
de semiotică se află la Universitatea Indiana din
Bloomington (T.A. Sebeok), iar în Canada trebuie relevată
în primul rând activitatea Cercului de Semiotică din
Toronto (P. Bouissac). Practic, cercetările de semiotică se
desfăşoară, în forme mai mult sau mai puţin organizate, în
mai toate ţările europene şi americane, ca şi în unele ţări
asiatice (în special în Japonia) şi africane.
Au luat amploare publicaţiile de semiotică. Trebuie
să menţionăm în primul rând: revista Semiotica, organ al
Asociaţiei Internaţionale de Semiotică; volumele publicate
de grupul de la Tartu, sub denumirea Semiotiké: Trud po
znakovym, iniţiate în 1965 de I.M. Lotman şi publicate în
continuare fără întrerupere; revista Significaçao: Revista
Brasileira de Semiotica apare la Rio de Janeiro; revista
poloneză Studia Semiotyezne, revistele germane Zeitschrift
für Semiotik şi Semiosis, publicaţia franceză Actes
sèmiotiques, revista belgiană Degrés, revista internaţională
Kodikas Code (publicată la Tübingen de Editura Günter
Narr); revista italiană Versus, revista Ars Semeiotica, organ
al Societăţii americane de semiotică, Le Journal Canadien
de Recherche Sémiotique/The Canadian Journal of
Research in Semiotics, care apare la Edmonton, în Canada,
708

Rãni deschise

Cahiers internationaux du symbolisme, publicate în
Belgia, iar Szemiotikai Tájékoztató este publicată de
Academia Maghiară de Ştiinţe începând cu anul 1975. Să
adăugăm la acestea o serie de publicaţii care, fără a fi
exclusiv dedicate semioticii, au contingenţe puternice cu
semiotica. Aici intră reviste ca Poetics, revistă de teoria
literaturii publicată la Amsterdam, Poetics Today,
publicată în Israel, Communications, revistă publicată de
Asociaţia internaţională de ştiinţa comunicării, Quarterly
Journal of Speech, publicată în Statele Unite, Sprache im
Tehnischen Zeitatler, care a dedicat, de exemplu, un număr
special, în 1968, semioticii filmului. La fel, Musique en
jeu, la Paris, a dedicat în 1971 şi 1973 numere speciale
semioticii muzicale, iar vol. 5, nr. 2 din 1973 al revistei
Sociologie et société este dedicat semiologiei sociale;
revista franceză Langages publică frecvent articole de
interes semiotic.
Un rol important îl au, în literatura de informare în
domeniul semioticii, dicţionarele de semiotică.
Deocamdată, putem menţiona dicţionarul în limba
germană publicat de Max. Bense şi Elisabeth Walther,
Wörterbuch der Semiotik (Kiepenheuer & Witsch, Köln,
1973), tradus în spaniolă: La Semiótica, Guida alfabetica,
Editorial Anagrama, Barcelona, 1975, dicţionar care
urmăreşte cu precădere terminologia introdusă de unul
dintre fondatorii semioticii, Charles Sanders Peirce. Un alt
709

Solomon Marcus

dicţionar important este cel publicat de A.J. Greimas şi J.
Courtes, Sémiotique. Dictionnaire raisonné de la théorie
du langage, Hachette, 1979, care prezintă în primul rând
terminologia folosită de şcoala lui Greimas. Să mai
menţionăm: Oswald Ducrot, Tzvetan Todorov,
Dictionnaire encyclopédique des sciences du langage,
Seuil, Paris, 1972.
Un dicţionar enciclopedic de semiotică, elaborat de
un colectiv internaţional, având ca redactor responsabil pe
Thomas A. Sebeok, se află în curs de publicare şi va apărea
cel mai târziu în 1986, la Editura Mouton, Olanda. Să mai
observăm că unele dicţionare elaborate fără intenţia
explicită de a le aşeza sub semnul semioticii pot fi totuşi
considerate dicţionare semiotice. Un exemplu în acest sens
este Dictionnaire des symboles. Mytes, rêves, couturnes,
gestes, formes, figures, conleurs, nombres, publicat de Jean
Chevalier şi Alain Gheerbrant la editurile Robert Laffont
şi Jupiter în 1969, ediţie revăzută şi corijată în 1982.
Expuneri sistematice ale semioticii sunt deocamdată
rare, iar cele care există sunt de obicei partizane ale unui
anumit curent, ale unei anumite linii de gândire, cum se
întâmplă şi cu dicţionarele semnalate mai sus. Fenomenul se
explică prin mai multe împrejurări, dintre care vom discuta
aici câteva. Izvoarele semioticii sunt de o mare eterogenitate,
mergând de la filosofie la lingvistică şi de la logică la biologie
şi medicină, de la Platon la filosofia indiană, de la Leibniz la
710

Rãni deschise

Auguste Comte, de la Humboldt la John Stuart Mill şi de la
Jean Poïnsot (1589-1644), cu al său Treatise on Signs, până
la John Loske. Dintre lucrările care fac istoricul semioticii,
menţionăm aici: Charles W. Morris, Writings on the General
Theory of Signs, în colecţia „Approaches to Semiotics” 16,
Mouton, The Hague, 1971, volumele antologii publicate de
Alain Rey sub denumirea Théorie du signe et du sens
(Klincksieck, Paris, începând cu 1973), lucrarea lui Roman
Jakobson Coup d’oeil sur le développement de la sémiotique.
Studies in Semiotics, Bloomington, Indiana, 1975 şi Thomas
A. Sebeok. Semiotics; A Survey of the State of the Art, în
„Current Trends in Linguistics”, vol.12, Mouton, Haga, 1974,
pp. 211-264 şi The Semiotic Web: a Chronicle of prejudices,
în „Bulletin of Litterary Semiotics” vol.2, 1975, pp. 1-63.
Toate aceste lucrări confirmă eterogenitatea despre care am
vorbit mai sus, dar ele trebuie considerate în ansamblul lor,
deoarece numai astfel se obţine un tablou cât de cât complet
al cristalizării semioticii. Aşa se face că, chiar asupra naturii
semioticii (este ea o disciplină, o metadisciplină, o metodă, o
atitudine?), există mari divergenţe, statutul ei controversat
determinând şi o mare varietate de soluţii în ceea ce priveşte
rolul şi locul ei în învăţământ. Fapt este însă că în deceniul al
şaptelea, când comanda socială a determinat o considerabilă
înviorare a preocupărilor de semiotică, disciplina cea mai
avansată, având ca obiect un sistem de semne în
funcţionalitatea sa semiotică, era lingvistica. Dacă adăugăm
711

Solomon Marcus

la aceasta faptul că înseşi tradiţiile istorice ale semioticii erau
într-o măsură importantă legate de studiul limbajului,
înţelegem de ce primii care au abordat, în anii ‘60,
problematica semiotică, au fost lingviştii şi cei care, fără să
fie lingvişti, erau totuşi puternic marcaţi de metodologia
lingvistică. Pentru a da numai câteva exemple, vom aminti că
Roman Jakobson, Thomas A. Sebeok, V.V. Ivanov, Alain Rey,
E. Benveniste, Roland Posner, Pierre Guiraud, Jean-Jacques
Nattiez, I.I. Revzin, pentru a nu mai vorbi de Ferdinand de
Saussure, sunt toţi lingvişti, iar alţii, ca Roland Barthes şi
Umberto Eco, au fost puternic marcaţi de metodologia
lingvistică, Roland Barthes fiind mereu citat cu faimoasa sa
declaraţie după care semiologia este subordonată, din punct
de vedere metodologic, lingvisticii (în contrast cu o afirmaţie
a lui Ferdinand de Saussure, după care lingvistica ar fi un
capitol al unei ştiinţe mai generale a semnelor).
Probabil că această perspectivă lingvistică, într-o
perioadă în care lingvistica devenea, după expresia lui
Roman Jakobson, „matematica ştiinţelor sociale”, datorită
avansului ei metodologic, a contribuit mult la discrepanţa
care se crease, o bună bucată de vreme, între semiologia
franceză şi semiotica anglo-saxonă. Prima s-a aşezat de la
început sub semnul ideilor unui mare lingvist, Ferdinand de
Saussure, a doua se reclama de la Ch.S. Peirce,
matematician, logician şi filosof, şi Ch. Morris, filosof.
Faptul era, până la un punct, natural, dar devenea anacronic
712

Rãni deschise

din momentul în care fiecare dintre părţi ignora sau
minimaliza tradiţiile care nu-i aparţineau. Astfel, o lucrare
ca La sémiologie, a lui Pierre Guiraud (Presses Univ. de
France, Paris, 1971), minimalizează tradiţiile anglo-saxone,
în timp ce un eminent cercetător ca Thomas A. Sebeok,
polemizând cu unele exagerări ale rolului lui F. de Saussure
în dezvoltarea semioticii, minimalizează totuşi contribuţiile
lingvisticii structurale europene (F. de Saussure, L.
Hjelmslev, A.J. Greimas). Între timp, discrepanţa s-a mai
atenuat, iar la al treilea congres al Asociaţiei internaţionale
de semiotică (Palermo, iunie 1984) au fost aleşi, în mod
echilibrat, în Comitetul executiv al Asociaţiei, reprezentanţi
ai ambelor tendinţe. Concomitent, s-a produs o mai mare
deschidere a Occidentului faţă de contribuţiile Europei de
Est (de exemplu, lucrările lui Lotman, Ivanov şi Pelc sunt
editate în mai multe ţări din Europa Occidentală şi în Statele
Unite), a cercetătorilor germani faţă de cei francezi, italieni
şi americani, a celor francezi faţă de cei anglo-saxoni (o
carte ca aceea a lui G. Deledalle – Théorie et pratique du
signe – prezintă publicului francez semiotica peirce-iană) şi
a americanilor faţă de europeni (Greimas şi Barthes, de
exemplu, au fost traduşi în engleză). Literatura semiotică,
originală sau tradusă, publicată în diferite ţări, arată în ultima
vreme o puternică tendinţă de părăsire a poziţiilor sectare,
de deschidere faţă de abordările complementare, chiar dacă
uneori în polemică cu ele.
713

