Dialog cu Solomon Marcus
Vatra, Târgu Mureş, serie nouă (1971), anul XVI,
nr. 183, p. 6, nr. 6, iunie 1986
Solomon Marcus: Înainte de a vă asculta prima
întrebare, daţi-mi voie, stimate Dan Culcer, să vă exprim
bucuria de a face un dialog pentru revista Vatra, pe care o
citesc de multă vreme cu interes, considerând-o una dintre
cele mai bune publicaţii de cultură din ţara noastră. Vă
previn însă că, matematician fiind, am tendinţa de a lua
întrebările care mi se adresează la modul lor pur denotativ,
fapt care ar putea să imprime răspunsurilor mele o notă de
prea mare seriozitate şi chiar gravitate. Mă supun probabil
unei limitări inevitabile. Există o istorioară care mi se pare
că se potriveşte aici. Cum reacţionăm în faţa unui enunţ
nou? Dacă-l înţelegem, dar nu-l putem demonstra,
comentariul nostru poate deveni substanţa unui articol
într-o revistă de fizică; dacă însă îl putem demonstra,
atunci, chiar dacă nu-l înţelegem, contribuţia noastră este
la locul ei într-o revistă de matematică. Dincolo de aspectul
ei anecdotic, această vorbă de spirit surprinde o realitate
profundă. Matematicianul, obsedat de corectitudine
formală, de igiena logică a gândirii sale, poate pierde din
vedere substanţa lucrurilor.
864

Rãni deschise

Dan Culcer: De unde, din cine vă trageţi, stimate
Solomon Marcus?
S.M.: M-am născut la 1 martie 1925, în oraşul Bacău,
ca al optulea (şi ultimul) copil al lui Alter Marcus, meseriaş
croitor şi apoi mic comerciant, şi al Simei Marcus, casnică.
Doi dintre cei opt copii au murit în timpul Primului Război
Mondial, victime ale unei epidemii de tifos exantematic.
După cum mi s-a relatat, unul dintre aceştia doi fusese
supradotat la matematică şi îşi uimise învăţătorii.
Amintirile mele cele mai vechi mă plasează într-un
mediu familial complicat de tot felul de conflicte şi
dificultăţi. Criza economică l-a determinat probabil pe tatăl
meu, croitor deosebit de apreciat în oraşul meu natal,
să-şi suplimenteze practicarea meseriei sale cu un mic
negoţ de mărunţişuri, pentru a face faţă dificultăţilor
financiare implicate de întreţinerea unei familii atât de
mari, în timp ce mama mea trudea din greu, trebuind să se
ocupe singură de hrănirea şi îngrijirea noastră (bunicii mei
dispăruseră destul de timpuriu: pe niciunul dintre ei nu am
apucat să-l cunosc). În aceste condiţii, copilăria mi-a fost
marcată de tot felul de privaţiuni. Am crescut oarecum în
voia întâmplării, în colbul străzii Iernii, care corespunde
spatelui actualului Teatru de Stat „George Bacovia” din
Bacău. Acolo mi-am petrecut verile şi iernile, zilele de
vacanţă şi orele libere ale zilelor de şcoală. Mediul şcolar
mă atrăgea în mod deosebit; abia aşteptam să treacă
865

Solomon Marcus

vacanţa pentru a merge din nou la şcoală. Nu mi-au plăcut
nici cărţile de aventuri, nici cele poliţiste, devorate de cei
mai mulţi din generaţia mea. Mă fermecau poeziile lui
George Coşbuc şi ale lui Dimitrie Bolintineanu şi poveştile
lui Ion Creangă. Îmi plăcea foarte mult să mă uit în ziare,
iar în revistele pentru copii urmăream cu precădere
lucrurile hazlii. Făcusem un ziar al străzii, pe care l-am
scris singur cu creionul, iar la şcoală ţineam un catalog
paralel cu cel „serios”, care mă fascina în primul rând
pentru că mi-era inaccesibil.
D.C.: Ce întâmplare din copilărie v-a rămas în
memorie?
S.M.: Eram prin clasa a doua primară, când, trecând
întâmplător, cu încă doi copii, prin faţa Ateneului din
Bacău, observăm că lumea se precipită în interior. Curioşi,
ne strecurăm şi noi, fiind toleraţi de controlor, dar orientaţi
spre balcon. La un moment dat, lumina se stinge, se ridică
o cortină şi o poveste începe să se înfiripe în faţa noastră.
Eram numai ochi şi urechi. Nu înţelegeam mare lucru, dar
posibilitatea de a crea întâmplări şi personaje într-o altă
ordine decât aceea curentă, pur şi simplu, m-a hipnotizat.
Aşa am descoperit teatrul. Aveam să aflu după aceea că era
vorba de un turneu la Bacău (care pe vremea aceea nu avea
teatru propriu) cu piesa Omul care a văzut moartea, de
Victor Eftimiu. De atunci, pasiunea pentru teatru nu m-a
părăsit niciodată, iar, ca elev, visul meu era să devin actor.
866

Rãni deschise

D.C.: V-aţi „născut” matematician sau aţi vrut, au
vrut părinţii dvs. să fiţi altceva? Cum aţi devenit
matematician? Şcoli, profesori. Unde lucraţi, profesaţi, în
ce cadru instituţional?
S.M.: În copilărie şi în perioada şcolarităţii nu am
manifestat niciun interes special pentru matematică, decât,
dacă se poate considera astfel, faptul că simţeam mereu
nevoia de a desena cifre. Tatăl meu, ager la minte, dar puţin
instruit, şi mama mea, cu numai patru clase primare, în plus
şi foarte ocupaţi, nu s-au gândit la calea mea în viaţă decât
după ce trecusem bacalaureatul (în toamna anului 1944).
Fiind clasificat întâiul, aveam dreptul să intru la orice
facultate fără examen de admitere. Atunci am fost avertizat
de tatăl meu ca nu cumva să mă duc în altă parte decât la
inginerie sau medicină. Era însă prea târziu, deoarece în
vara anului 1944, citind pentru prima oară despre geometria
neeuclidiană, nu mai puteam rezista tentaţiei de a încerca
să studiez matematica. Aşa am devenit un student
privilegiat de a-i putea audia, la Facultatea de Ştiinţe din
Bucureşti, în anul întâi pe Octav Onicescu, Gheorghe
Vrănceanu şi Miron Nicolescu, iar în anul al doilea pe
Simion Stoilow, Victor Vâlcovici, Dan Barbilian şi
Alexandru Ghica. În momentul în care am ascultat prima
lecţie de Analiză matematică a profesorului Miron
Nicolescu, am ştiut că nu mă voi mai despărţi de
matematică niciodată. În 1946-1947 ar fi trebuit să îl am
867

Solomon Marcus

profesor pe Gr.C. Moisil, dar tocmai plecase ambasador la
Ankara. L-am descoperit mult mai târziu, dar cu mult folos
pentru mine. În 1950, profesorul Nicolae Teodorescu m-a
chemat ca asistent la Catedra de Matematică a Institutului
Politehnic din Bucureşti, unde amcondus, de asemenea,
seminarii la cursurile profesorilor Nicolae Ciorănescu şi
Nicolae Racliş. În 1953, am renunţat la activitatea de la
Politehnică, deoarece, între timp, în 1951, profesorul Miron
Nicolescu mă alesese ca asistent al său la Catedra de
Analiză matematică a Universităţii din Bucureşti. La
această catedră am funcţionat fără întrerupere, parcurgând
toate treptele ierarhiei universitare: asistent, lector,
conferenţiar şi, în 1966, profesor. Doctoratul mi-l
susţinusem în 1956, cu acelaşi profesor cu care îmi
trecusem lucrarea de diplomă: Miron Nicolescu. Titlul tezei
de doctorat: „Funcţii monotone de două variabile”. În 1967,
am primit titlul ştiinţific de doctor docent în ştiinţe, la
Universitatea din Bucureşti, pentru întreaga mea activitate
ştiinţifică. În acelaşi loc mă aflu şi azi ca profesor.
D.C.: Cum aţi ajuns la poetica matematică? Ce aţi
vrut să demonstraţi prin aceste cercetări (ipoteze de lucru),
încotro vă îndreptaţi acum? Descrieţi traiectoria dvs.
ştiinţifică.
S.M.: Datorită unui frate pasionat de literatură, am găsit
în casă, ca adolescent, pe Arghezi şi pe Baudelaire, pe
Călinescu (Istoria literaturii române) şi pe Rilke. Astfel, ca
868

Rãni deschise

elev, am descoperit în poezie una dintre raţiunile
fundamentale ale existenţei, poate bucuria ei supremă. De
atunci, urmăresc cu consecvenţă aventurile limbajului, un
stimulent în acest sens fiind şi convorbirile mele continue cu
soţia mea, Paula Diaconescu, conferenţiar la Facultatea de
Limba şi Literatura Română. După o activitate ştiinţifică de
mai mulţi ani, în domeniul Analizei matematice, de care mă
îndrăgostisem ca student în special pentru subtilitatea şi
profunzimea cu care sunt explorate procesele cu o infinitate
de etape, dezvăluindu-se aspecte care surprind intuiţia
curentă, a venit, pe la sfârşitul deceniului al şaselea,
momentul prielnic pătrunderii masive a metodelor
matematice în domeniul lingvisticii. Am reacţionat fără
ezitare, reuşind astfel să public în 1963 prima monografie de
lingvistică matematică, tradusă, în forme ameliorate şi în
diferite variante, la New York şi la Paris, ca şi la Moscova şi
la Praga. Un stimulent puternic l-a constituit pentru mine
invitaţia pe care mi-a adresat-o marele lingvist Roman
Jakobson de a face o prezentare de ansamblu a lingvisticii
matematice în numărul special dedicat lingvisticii de
publicaţia UNESCO International Journal of Social
Sciences, număr coordonat de Jakobson. Am fost, de
asemenea, onorat cu invitaţia de a redacta, pentru seria
internaţională de volume Current Trends in Linguistics
(coordonator Th.A. Sebeok), raportul asupra lingvisticii
matematice în Europa. A fost, prin 1960 şi după, un moment
869

Solomon Marcus

de suprem entuziasm, susţinut de mari intelectuali ca Al.
Rosetti, Gr.C. Moisil, Em. Petrovici şi Tudor Vianu, datorită
cărora se creaseră la noi în ţară, atât la filologie, cât şi la
matematică, primele cursuri de lingvistică matematică, la
care, după scurt timp, s-au adăugat şi cursuri de poetică
matematică. În 1966 am predat pentru filologie un curs de
teatrologie matematică. Unul dintre studenţii de atunci,
Mihai Dinu, avea să-şi susţină mai târziu o teză de doctorat
pe această temă. De ce poetica matematică? Pentru că este
vorba de asocierea celor două forme supreme de concentrare
a expresiei, de densitate a limbajului. Prin 1963, profesorul
Constantin Drâmbă îmi atrăsese atenţia asupra lucrărilor lui
Pius Servien, peste numele căruia se aşternuse la noi, de mai
multă vreme, o tăcere de mormânt. Unele semnalări mai
vechi ale numelui său nu erau în niciun caz de natură să
atragă atenţia asupra modernităţii teoriei sale. L-am studiat
şi am hotărât să-i preiau ştafeta. În 1965 am publicat articolul
cu titlul provocator: „Un precursor al poeticii matematice:
Pius Servien”. Pe de altă parte, dintr-un articol apărut în Le
Monde aflasem despre moartea lui Matila C. Ghyka, un alt
om de cultură român acoperit la noi de tăcere, dar menţionat
frecvent în lucrări de specialitate din Occident. Până atunci,
nici nu ştiam de existenţa lui. Am citit imediat cu atenţie ceea
ce am găsit din opera lui în bibliotecile noastre. Mi-am dat
seama de importanţa sa şi am reacţionat imediat, în diferite
articole (în Viaţa Românească etc.), introducându-l şi pe
870

Rãni deschise

Ghyka în contextul corespunzător. De vreo zece ani mi-am
extins preocupările în domeniul semioticii şi al aplicaţiilor
matematicii în disciplinele sociale, cu precădere în
problematica dezvoltării globale a omenirii. Nu ascund că
m-a interesat întotdeauna ecoul lucrărilor mele, al rezultatelor
pe care le-am obţinut. Am acumulat vreo 100 de caiete în
care am notat modul în care aceste rezultate au fost reluate,
continuate, criticate, generalizate, menţionate, lăudate sau
negate. Aceste ecouri, răspândite în reviste şi cărţi din
nenumărate ţări ale lumii, provin, desigur, în cea mai mare
parte, de la autori care au preocupări apropiate de ale mele.
Este, deci, o chestiune de elementară corectitudine în munca
ştiinţifică faptul de a le lua în considerare în preocupările
mele ulterioare. Activitatea de cercetare este un continuu
dialog cu ceilalţi specialişti din domeniul tău de activitate,
este ca o ştafetă în care trebuie să fii mereu atent de la cine
ai de preluat ceva şi ce anume poţi tu transmite altora.
Este regretabil faptul că nu se mai practică azi bunul
obicei de altă dată ca universitarii să publice în mod
periodic câte o broşură în care să-şi prezinte activitatea
creatoare, rezultatele obţinute, ecoul lor în lumea ştiinţifică
etc. Tineretul studios are dreptul să cunoască personalitatea
celor care îi educă şi-i instruiesc.
D.C.: Care sunt scriitorii dvs. preferaţi? Motivaţi.
S.M.: Nu-mi vin acum în minte toţi. Adolescenţa mea
a fost puternic marcată de scrierile lui Rainer Maria Rilke,
871

Solomon Marcus

pe care şi acum îl citesc cu aceeaşi emoţie. Am primit ca
un adevărat şoc Duhovnicească de Tudor Arghezi, ca şi
De-a v-aţi ascuns şi Cartea cu jucării ale aceluiaşi. Am
fost hipnotizat de Jocul secund al lui Ion Barbu, m-au
impresionat Oscar Wilde, Edgar Poe, Rimbaud, Valéry.
Am fost bolnav de Eminescu, Blaga şi Bacovia, m-au
răvăşit poemele în proză ale lui Baudelaire. Am trăit sub
vraja lui G. Călinescu. Îmi plac Nichita Stănescu şi Marin
Sorescu, Ana Blandiana, Ileana Mălăncioiu şi Mircea
Dinescu. Îi savurez pe Geo Bogza, pe Alexandru Paleologu
şi pe Nicolae Manolescu. Îi recitesc mereu pe Caragiale,
Beckett, Eugen Ionescu şi Teodor Mazilu. Şi mai e tânăra
generaţie de scriitori, atât de bogată în talente, mai sunt
atâţia alţii de care nu mi-am amintit în acest moment.
Să-mi motivez aceste preferinţe? Întrebare grea,
pentru care nu am forţa necesară acum.
D.C.: Cum definiţi „gândirea” dogmatică? Aţi fost
vreodată în situaţia de a o practica sau de a vă supune ei?
S.M.: Gândirea dogmatică este o sintagmă
oximoronică. Am cunoscut această schimonosire a gândirii
autentice în diferite variante, începând cu aceea fascistă,
care mi-a întunecat adolescenţa. Armele mele contra acestui
flagel au fost totdeauna umorul şi ironia. Însă gândirea
dogmatică nu se lasă totdeauna uşor recunoscută,
travestindu-se în forme dintre cele mai viclene. Se întâmplă
ca abia ulterior să realizăm caracterul ei dogmatic.
872

Rãni deschise

Mărturisesc că în deceniul al cincilea am fost şi eu păcălit
uneori de această viclenie. Frecventarea matematicii şi a
poeziei m-a ajutat mult să mă apăr, să-mi păstrez
autenticitatea. În 1949, pe baza relatării mele autobiografice
privind lecturile din Arghezi şi Baudelaire, mi se pusese
diagnosticul: infectat de literatură burgheză decadentă.
D.C.: Ce rol are spiritul utopic în viaţa noastră? Aţi
gândit vreodată în acest spirit?
S.M.: Înţeleg prin utopie orice scenariu de viitor care
nu rezultă din extrapolarea stărilor trecute sau prezente. Sub
această formă, utopia pătrunde tot mai mult în studiile
viitorologilor şi se legitimează ştiinţific, dar nu pe baza
ştiinţei clasice, ci a unor teorii mai recente, cum ar fi teoria
catastrofelor, analiza sistemică, holonomia. Există aici unele
puncte de contact cu filosofii şi viziuni extrem orientale.
Poate însă că trebuie să modificăm conceptul de utopie în
modul următor: utopia este orice reprezentare a viitorului
care nu rezultă din analiza ştiinţifică a evoluţiilor posibile.
Oricum am proceda, cred că discursul utopic are un
rol important, iar spiritul utopic este un ingredient esenţial
al oricărei culturi; dar, ca orice ingredient, proporţiile şi
contextul său, interacţiunea sa cu reprezentările neutopice
trebuie ţinute sub un control riguros.
Sovata, iulie 1985.

873

