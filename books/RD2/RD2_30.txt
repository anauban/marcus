Matematica şi societatea
Contemporanul, 12 ianuarie 1979
La cel de al treilea Congres Internaţional de
Educaţie Matematică (Karlsruhe, august 1976) şi la
recentul Congres Internaţional al Matematicienilor
(Helsinki, august 1978) numeroşi participanţi şi-au
exprimat preocuparea pentru modul în care se dezvoltă
raporturile matematicii şi matematicienilor cu societatea.
Educaţia matematică şi cercetarea matematică nu mai
sunt simple probleme de breaslă, ci chestiuni care
afectează o mare parte din mediile sociale.
Matematicienii, pe de o parte, beneficiarii potenţiali ai
matematicii, pe de altă parte, fac eforturi pentru a veni
unii în întâmpinarea celorlalţi. Rezultatele nu sunt
neglijabile şi privesc toate domeniile, de la cercetare la
activităţile practice. Specialişti provenind din cele mai
variate profesii aspiră la ameliorarea culturii lor
matematice, intră în echipe interdisciplinare din care nu
lipsesc matematicienii şi informaticienii.
În acelaşi timp însă, în afirmarea rolului social al
matematicii (al ştiinţei în general) se manifestă şi unele
stări de tensiune. Astfel, un volum publicat în 1976 de
Consiliul ştiinţific al Canadei, referitor la impactul
230

Rãni deschise

contemporan al matematicii, scoate în evidenţă faptul că
mulţi matematicieni încearcă un sentiment acut de
izolare, din cauza dificultăţii de a corela activitatea lor
ştiinţifică cu nevoile societăţii şi de a o explica
publicului. Constatarea este cu atât mai gravă, cu cât
Canada este una din ţările cu cel mai mare număr de
matematicieni (în raport cu populaţia) şi, în general, cu
o matematică fundamentală şi aplicativă de o foarte bună
calitate. Nu trebuie supraestimate nici proporţiile reale
ale adeziunii la gândirea matematică. De multe ori, în
câmpul de forţe al culturii, matematica are o prezenţă
mai mult decorativă decât de substanţă, proliferează o
anume formă de snobism matematic. Nu putem analiza
aici cauzele acestor fenomene, natura acestor dificultăţi,
de care învăţământul matematic nu e deloc străin, cert
este însă că ele nu pot anula impactul real pe care
gândirea matematică autentică îl are asupra lumii
contemporane.
Alături de atitudinile de adeziune, uneori în
polemică cu ele, apar atitudini de contestare a
legitimităţii prezenţei matematicii în anumite sfere de
activitate, nemulţumiri faţă de expansiunea matematicii,
nu numai în zone care până mai ieri îi păreau interzise,
dar chiar în disciplinele cu care ea întreţine legături
devenite tradiţionale. Un strălucit inginer hidroenergetician care, ca profesor, a format cincizeci de generaţii
231

Solomon Marcus

de specialişti, întrebat fiind cum vede actuala programă
în disciplina sa, este de părere că se dă o prea mare
atenţie matematicii, în detrimentul formării unei gândiri
inginereşti. Să vedem aici o critică la adresa tipului de
matematică pe care o învaţă viitorii ingineri? Un
prestigios critic literar îşi exprimă dezaprobarea faţă de
„panmatematicismul contemporan, care a invadat şi
filologia”. Un medic cibernetician vede în matematică o
traducere care nu-l transformă pe traducător (adică pe
matematician) în autor. Această reducere a matematicii
la un limbaj e manipulată după voie. Orice rezultat
matematic trebuie să poată fi exprimat şi în limbajul
uzual – pretind, în mod nejustificat, unii: deci utilizarea
formulelor matematice n-ar fi legitimă decât în interiorul
breslei matematicienilor. Matematica n-ar merita (ca
engleza, de exemplu) să fie învăţată pentru a fi utilizată
direct de către cei care au nevoie de ea. Capacitatea
matematicii de a furniza modele valabile concomitent
pentru fenomene de natură diferită este interpretată de
unii ca o slăbiciune, astfel de modele neconducând la
specificitatea fenomenului studiat. Se eludează astfel
faptul că, de câteva decenii încoace, tocmai această
strategie a analogiei este cea care a dus la obţinerea celor
mai interesante rezultate. Teorema lui Gödel, privind
imposibilitatea formalizării complete a aritmeticii, este
folosită, în mod abuziv, ca argument împotriva
232

Rãni deschise

„extrapolării unor proceduri din domeniul ştiinţelor
exacte în cel al ştiinţelor umaniste”. De fapt, o mare
parte din cercetarea contemporană este bazată tocmai pe
astfel de transferuri metodologice.
Nu putem nega faptul că metodele matematice
actuale se dovedesc uneori insuficient de adecvate, iar
rezultatele obţinute nu sunt totdeauna relevante în raport
cu problema studiată. Din astfel de nemulţumiri se naşte
ambiţia de a înlocui un model matematic cu un altul, mai
bun. Contestările critice, efectuate nu din exterior şi la
modul global şi aprioric, ci provenind din efortul de a
înţelege şi folosi un anume instrument matematic într-o
anumită problemă, sunt motorul întregii activităţi de
injectare a gândirii matematice în cultura contemporană.
Să mai adăugăm faptul că, nu de puţine ori,
prezenţa esenţială a gândirii matematice trece
neobservată. De exemplu, o ştire apărută recent în presă
informa despre confirmarea experimentală a existenţei
undelor gravitaţionale: câţi dintre cititori ştiau că ipoteza
existenţei acestor unde a fost elaborată cu ajutorul
instrumentului matematic?
Legea învăţământului, intrată recent în vigoare la
noi în ţară, oglindeşte principalele cerinţe privind
difuzarea amplă a gândirii matematice în întreaga arie a
activităţilor economico-sociale. Desigur, într-o anumită
măsură, legea sancţionează o realitate pozitivă,
233

Solomon Marcus

existentă. Dar, în acelaşi timp, o lege nu poate crea
singură o realitate nouă, ea poate numai să instaureze
anumite structuri de natură să orienteze activitatea
noastră. Imensul impact actual al matematicii asupra
întregii vieţi sociale solicită, de aceea, astăzi, mai mult
decât oricând, un spirit cooperativ, pentru ca
mentalităţile diferite să se completeze, în loc să se
înfrunte.

234

