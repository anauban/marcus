Întrecerea celor mai buni
Viaţa Studenţească, anul XXX, nr. 15 (1091),
miercuri, 16 aprilie 1986, p. 3
În zilele de 3-5 aprilie 1986, Universitatea din
Timişoara a găzduit etapa finală a Concursului profesional
ştiinţific studenţesc de matematică.
Manifestare organizată sub auspiciile Ministerului
Educaţiei şi Învăţământului şi Consiliului U.A.S.C.R.,
acest concurs aduce în fiecare an în competiţie o parte
dintre studenţii cei mai buni la matematică, din primii doi
ani de studiu ai universităţilor şi ai institutelor politehnice
sau economice. Câteva sute de concurenţi, repartizaţi în
zece profiluri (două universitare, trei economice, patru
politehnice şi unul pedagogic), şi-au încercat puterile, şi
în acest an, în rezolvarea unor probleme care depăşesc, ca
grad de dificultate, chestiunile propuse în mod curent la
examene.
Dar competiţia studenţilor a prilejuit şi o întâlnire a
câtorva zeci de matematicieni, profesori dintre cei mai
experimentaţi de la institutele de învăţământ superior de
unde proveneau concurenţii. S-a putut astfel angaja un
dialog profund privind structura problemelor propuse spre
rezolvare, modalitatea de apreciere a soluţiilor, urmărirea
841

Solomon Marcus

în continuare a celor mai buni. Multe institute de
învăţământ superior au privit cu toată seriozitatea acest
concurs. Astfel, Institutul Politehnic din Bucureşti, nu
numai că a trimis la concurs pe unii dintre cei mai buni
studenţi, care anterior repurtaseră victorii la olimpiadele
internaţionale de matematică ale elevilor, dar au delegat,
ca membri în Biroul concursului, pe patru dintre cele mai
prestigioase cadre didactice, în frunte cu şefii celor două
catedre de matematică din institut, profesorii Valter Olariu
şi Octavian Stănăşilă. Ne-am aflat astfel la un mic congres
privind problemele matematicii de performanţă. Punctul
culminant l-a constituit festivitatea de premiere, la care
câteva zeci de studenţi au primit premii şi menţiuni, iar o
parte dintre aceştia au fost selecţionaţi pentru lotul lărgit
din care se va selecţiona echipa reprezentativă a ţării
noastre la Balcaniada de matematică din acest an, de la
Atena.
Este plăcut să constatăm o anumită consecvenţă a
talentelor. Elevii de ieri, victorioşi la olimpiadele
(naţionale şi internaţionale) de matematică ale elevilor,
s-au aflat acum, de asemenea, printre victorioşi. Nu puţine
au fost cazurile în care soluţiile au surprins plăcut pe
profesori. Astfel, după concurs, profesorul timişorean
Mircea Reghiş dorea să stea de vorbă cu studenţii
Mironescu şi Mandache, din anul al doilea al Facultăţii de
Matematică din Bucureşti, care rezolvaseră o problemă
842

Rãni deschise

delicată de ecuaţii diferenţiale, iar, în ceea ce mă priveşte,
am fost plăcut impresionat de soluţia impecabilă dată unor
probleme nestandard de analiză matematică, de către
studenţii timişoreni Răzvan Gelca (premiul întâi la profilul
universitar, anul întâi; câştigător al olimpiadei
internaţionale de matematică de la Helsinki) şi Adrian
Stoica. Să mai remarcăm că două dintre premiile
importante la profilul politehnic au revenit unor studenţi
ai Academiei Militare. De la un concurs al elitelor, ca cel
de la Timişoara, aşteptăm soluţii noi, care nici măcar nu
au intrat în prevederile celor care au propus problemele;
aşteptăm idei îndrăzneţe, pline de fantezie. Acestea nu au
lipsit, chiar dacă nu au fost pe măsura aşteptărilor.
Dar la exigenţe mari trebuie să corespundă şi un spor
de fantezie în structura problemelor propuse. Anul acesta,
a existat o preocupare mai mare pentru propunerea unor
probleme mai variate, mai netipice, care să provoace
într-o măsură mai mare iniţiativa personală a candidaţilor.
S-a constatat că o problemă nestandard, chiar dacă este mai
uşoară, este rezolvată de mai puţini concurenţi decât una
mai grea, dar pe linia celor existente în manuale şi în
culegeri. În schimb, cei puţini care le rezolvă răspund la
probleme nestandard prin soluţii nestandard. Prin aceasta,
concursul îşi atinge scopul său suprem; a-i aduce pe
concurenţi în pragul activităţii de cercetare, în care vor
trebui, în orice caz, să intre în anul al treilea de studii.
843

Solomon Marcus

Există uneori tendinţa de a propune candidaţilor o
cantitate prea mare de probleme. Trebuie să recunoaştem că
nici de această dată nu am evitat această greşeală, de
exemplu la profilul universitar, anul întâi, unde concurenţii
s-au aflat aproape în unanimitate în criză de timp.
Concurentul Octav Cornea (Universitatea din Bucureşti)
motiva în acest fel, şi pe drept cuvânt, tratarea superficială
a problemei de geometrie: „aveam unele idei privind aspecte
adiacente la problemele de analiză, dar a trebuit să renunţ
din lipsă de timp” (obţinuse totuşi premiul al III-lea).
Din discuţiile care au avut loc a rezultat că este
necesară o îndrăzneală mai mare şi mai multă fantezie în
formularea problemelor. Concurenţii provin din institute
variate, cu interese matematice foarte diferite. Acest fapt nu
se reflectă suficient în structura problemelor. Potenţialul
interdisciplinar al matematicii se reflectă foarte slab, dacă
nu deloc, în problemele propuse. Există apoi aspectul
propriu-zis al gândirii matematice, cu multele ei nuanţe:
deductivă, algoritmică, inductivă, strategică, generalizatoare, combinatorică, euristică, analogică, selectivă,
binară, triadică, infinită, recursivă, probabilistă etc. Câte
dintre acestea se reflectă în problemele propuse? În literatura
matematică din ţara noastră a existat şi există o adevărată
inflaţie de culegeri de probleme, dar cele mai multe rămân
circumscrise unor rutine bine stabilite. Excepţiile se numără
pe degete: Ion Cuculescu, Olimpiadele internaţionale de
844

Rãni deschise

matematică ale elevilor (Editura Tehnică, 1984);
Sorin Rădulescu şi Marius Rădulescu, Teoreme şi probleme
de analiză matematică (Editura Didactică şi Pedagogică,
1982), unele dintre lucrările lui Gh. Sireţchi şi încă
vreo două.
Matematica, ştiinţa în general, nu sunt probe contra
cronometru, ca la sport, ci reclamă o reflecţie îndelungată
şi profundă. Mulţi profesori au fost de părere că este prea
mult, de exemplu, să li se ceară concurenţilor ca, în decurs
de trei ore, să abordeze chestiuni atât de diferite ca
orientare, cum ar fi cele de algebră, analiză şi geometrie.
Poate că alegerea uneia sau a două probe din trei ar fi mai
judicioasă, aceasta presupunând însă că probele nu au loc
în acelaşi timp. S-a subliniat, de asemenea, latura culturală
a concursului, care ar trebui să prilejuiască şi o întâlnire
între profesori şi studenţi, pe teme dintre cele mai actuale
privind matematica în lumea contemporană. Dar nu cumva
aspectul cultural al matematicii ar trebui să se reflecte şi
în problemele propuse?
Universitatea din Timişoara s-a dovedit o gazdă
excelentă, începând cu conducerea ei (rectorul universităţii
s-a interesat tot timpul de desfăşurarea concursului, iar
prof. Emil Boroş, prorectorul Universităţii, preşedintele
Biroului Concursului, a condus cu pricepere şi tact
lucrările acestuia, ajutat fiind de prof. Constantin Popa,
şeful catedrei de matematică). Tot la Timişoara va fi
845

Solomon Marcus

antrenat lotul românesc pentru Olimpiada Internaţională
de Matematică a elevilor, lot care va fi selecţionat, pe baza
rezultatelor etapei finale a olimpiadei de matematică,
desfăşurate la Arad, în a doua săptămână a lunii aprilie.
O activitate febrilă, de o deosebită răspundere,
deoarece trăim într-o lume care are tot mai multă nevoie
de talente şi de vocaţii, de mai mult şi mai bine în toate
privinţele.

846

