Lingvistica matematică, azi
Ramuri, nr. 8(254), 15 august 1985, p. 9
Modul obişnuit de urmărire a transformărilor şi
perspectivelor unei discipline este acela al desprinderii
dinamicii relaţiilor dintre problemele pe care disciplina şi
le pune şi rezultatele pe care ea le obţine. Simptomul
vitalităţii unei discipline este capacitatea ei de a-şi pune
probleme; cum spune, într-un articol relativ recent, Paul
Halmos, problemele sunt inima matematicii. De fapt,
problemele sunt inima oricărei discipline ştiinţifice, nu
numai a celor matematice. Lingvistica matematică, la care
ne referim în articolul de faţă, a fost discutată de mai multe
ori din punctul de vedere al problemelor, rezultatelor,
metodelor şi aplicaţiilor ei. Autorul rândurilor de faţă s-a
angajat de câteva ori într-un astfel de itinerar. Interesul
pentru disciplinele tinere (lingvistica matematică s-a
născut la sfârşitul deceniului al şaselea al secolului nostru),
pentru evoluţia şi perspectivele lor a fost întotdeauna mai
mare decât pentru disciplinele cu o istorie seculară sau
milenară. Ca în orice familie, şi în familia ştiinţelor,
nou-născuţii se bucură de o atenţie mărită, toţi comentând
cu cine seamănă şi ce le va aduce viitorul. Dacă pentru o
disciplină milenară cum este matematica, sau chiar pentru
742

Rãni deschise

una seculară, cum este analiza matematică, unul sau două
decenii reprezintă un interval de timp neînsemnat, pentru
o disciplină născută în urmă cu 25 de ani, o perioadă de 15
ani sau chiar de un deceniu este imensă. Ne propunem în
cele ce urmează: a) să confruntăm situaţia de azi a
lingvisticii matematice cu cea anterioară şi cu modul în
care întrevedeam ieri viitorul care astăzi devine trecut, şi
b) să încercăm să desprindem unele tendinţe actuale ale
lingvisticii matematice, nu atât în raport cu problematica
abordată, cât în ceea ce priveşte schimbarea de mentalitate.
Este interesant de urmărit modul în care a evoluat
statutul lingvisticii matematice ca disciplină. Până în urmă
cu 10-15 ani, se discuta dacă lingvistica matematică este
lingvistică sau matematică, preponderent fiind punctul de
vedere după care domeniul este interdisciplinar. În anii ‘60
se credea că acest caracter interdisciplinar se va accentua.
Acum se constată că denumirea însăşi de lingvistică
matematică este mai puţin utilizată, fără însă ca aceasta să
însemne o diminuare în importanţă a domeniului ca atare.
Două procese au avut loc, ele fiind şi acum în plină
desfăşurare. Pe de o parte, lingvistica matematică s-a
descompus în componente care s-au constituit treptat în
domenii distincte, relativ independente, pe de altă parte,
unele dintre aceste componente au fost ulterior asimilate
fie de lingvistică, fie de matematică, fie de informatică, logică sau filosofie. Astfel, partea despre teoria gramaticilor
743

Solomon Marcus

generative şi transformaţionale s-a descompus într-o
componentă matematică, teoria limbajelor şi gramaticiIor
formale şi gramatica transformaţională, asimilată de
lingvistică drept unul dintre capitolele ei de bază. Partea
privind utilizarea calculatorului în lingvistică s-a constituit într-o disciplină relativ independentă, lingvistica
computatională, cu propriile ei reviste si congrese; totuşi,
în ultima vreme, se observă interesul crescând pentru
această nouă disciplină, din partea lingvisticii şi a
informaticii. La ultimul congres international al lingviştilor
(Tokio, august 1982), o secţiune specială a fost intitulată
„Lingvistica şi calculatorul”. În ceea ce priveşte informatica, este de observat că unul dintre domeniile ei de
bază, inteligenţa artificială, se interesează tot mai mult de
limbile naturale, fie în ceea ce priveşte dialogul omcalculator, fie relativ la simularea pe calculator a dialogului
om-om. În timp ce lingvistica computaţională este
preocupată cu precădere de probleme care vin din tradiţia
îndelungată a lingvisticii structurale şi a lingvisticii
istorice, informatica vizează o problematică nouă, care
depăşeşte de obicei frontierele limbilor naturale. O
amploare deosebită au luat-o metodele logicii matematice
în studiul semanticii şi pragmaticii limbilor naturale.
Această orientare a fost asimilată de lingvistica modernă
şi reprezintă acum o parte organică a ei. Faptul acesta este
atestat de simpla parcurgere a revistelor şi a monografiilor
744

Rãni deschise

de lingvistică teoretică din ultimul deceniu. Trebuie însă
observat că orientarea logică în lingvistică a fost cu precădere corelată cu gândirea unor clasici ca Frege, Russell,
Carnap, Wittgenstein şi cu aceea a unor logici neclasice de
tipul logicilor modale, deontice şi fuzzy, şi mai puţin de
aceea dezvoltată de Kleene, Gödel, Turing, Church,
Markov şi atâţia alţii, privind recursivitatea,
calculabilitatea, decidabilitatea şi toate celelalte fenomene
prin care logica matematică modernă se constituie drept
baza teoretică a informaticii. Implicit, legătura lingvisticii
cu informatica nu este încă efectuată la nivelul gândirii
lingvistice teoretice. Fapul acesta pare cu atât mai curios,
cu cât lingvistica generativă, dezvoltată începând cu
deceniul al şaselea, are una din sursele ei în acea teorie a
sistemelor formale care a alimentat atât linia de gândire a
lui Hilbert şi Gödel, cât şi pe aceea a lui Kleene şi Turing.
O evoluţie interesantă a avut-o teoria modelelor
analitice în lingvistică. Dacă prin unele capitole ale ei
această teorie a făcut joncţiunea cu teoria semigrupurilor
libere, prin altele ea s-a legat organic de unele capitole ale
lingvisticii, ca fonologia, morfologia şi sintaxa. Un
exemplu semnificativ în acest sens este cartea lui F.H.H.
Kortland asupra fonemului (Mouton, Haga, 1972). Să
observăm că unele direcţii ale teoriei modelelor analitice,
cum este aceea a configuraţiilor sintactice, prezintă
dificultăţi matematice care nu au fost încă depăşite.
745

Solomon Marcus

Cea mai veche orientare matematică în lingvistică,
aceea relativă la utilizarea metodelor probabiliste şi
informaţionale, a fost în mare măsură asimilată de
lingvistică. De la cărţile prea elementare, ca acelea ale lui
Pierre Guiraud, sau prea sofisticate, (Gustav Herdon), în
care gândirea lingvistică şi cea probabilistă nu reuşeau să
se întâlnească în mod efectiv, s-a reuşit să se atingă o fază
nouă, pentru care dau seama cărţi ca acelea ale lui Charles
Muller şi, mai ales, Barron Brainerd. Din păcate, domeniul
lingvisticii statistice a fost, într-o anumită măsură,
compromis de un număr mare de manifestări diletante,
care au îndepărtat atenţia multor lingvişti de la aceste
metode.
Paralel cu această evoluţie eterogenă a compartimentelor lingvisticii matematice, care existau încă de la
naşterea acesteia, trebuie să consemnăm apariţia şi
dezvoltarea unor compartimente noi, în bună măsură privind
mecanisme generative de tip nechomskian. Avem în vedere,
în primul rând, teoria sistemelor Lindenmayer, care, plecând
de la modelarea unor fenomene de dezvoltare în biologia
celulară, s-a constituit repede într-un capitol de matematică
relativ independent, filiaţiile sale cu teoria gramaticilor
generative chomskiene neputând fi însă eludate.
Putem spune, în ansamblu, că amploarea luată de
lingvistica matematică se manifestă numai parţial într-o
formă interdisciplinară; în bună măsură ea este camuflată
746

Rãni deschise

de integrarea unor capitole ale ei în disciplinele anterior
constituite, ca matematica sau lingvistica. Altele sunt
înghiţite acum de informatică, poate unele vor intra în
logică. În orice caz, acest proces este însoţit de un altul.
Matematica şi lingvistica înseşi îşi extind propriul lor
obiect. Astfel, matematica îşi lărgeşte domeniul cu teorii
sugerate de biologie, lingvistică, economie (a se vedea
teoria catastrofelor), iar lingvistica, definită până ieri ca
studiu al limbilor naturale, îşi extinde treptat interesul şi
asupra celor artificiale (logice, de calculator etc.).
Orientarea promovată în acest sens de Richard Montague
este semnificativă, de altfel ea era aşteptată încă de la
începutul lingvisticii matematice. În schimb, a surprins
aşteptările procesul prin care lingvistica s-a dovedit
capabilă să asimileze într-un timp scurt metodele logice
care până mai ieri îi erau străine (chiar dacă antecedente
ale lor pot fi urmărite departe în trecut). Prin aceasta se
dezvăluie şi caracterul în parte convenţional a ceea ce
numim interdisciplinaritate. Putem spune că lingvistica
matematică îşi dovedeşte, într-o anumită măsură,
maturitatea tocmai prin capacitatea ei de a se autodizolva,
de a se lăsa absorbită de venerabilele discipline care i-au
stimulat apariţia. Aceasta nu înseamnă însă că majoritatea
lingviştilor se contaminează cu probleme de formalizare;
metodele logice în lingvistică rămân o zonă specială a
lingvisticii, iar o revistă de „Theoretical Linguistics”
747

Solomon Marcus

rămâne în continuare neinteresantă pentru un număr destul
de mare de lingvişti.
Un alt fenomen interesant în ultima vreme, care
probabil se va accentua, este rolul matematicii de
catalizator al legăturilor lingvisticii cu celelalte discipline.
Prin structuralism, lingvistica a putut colabora cu
antropologia (Claude Lévi-Strauss), cu poetica (Roman
Jakobson), cu psihanaliza (J. Lacan) şi cu alte câteva
discipline; prin gramatica generativă ea şi-a lărgit
considerabil legăturile interdisciplinare; faptul a fost
posibil însă datorită transferului pe care-l permite
generalizarea de natură matematică. Au fost dezvăluite
astfel legături profunde ale lingvisticii cu teoria limbajelor
de programare, cu genetica, teoria învăţării, cu teatrul,
muzica şi literatura. Acest rol de „al treilea” al matematicii
ar putea să fie detectat şi în raport cu alte discipline (a se
vedea, de exemplu, legăturile, prin intermediul modelului
matematic al entropiei, dintre fizică şi procesele de
comunicare).
O privire nouă a adus matematica şi în legătură cu
situarea lingvisticii faţă de dicotomia discret-continuu.
Lingvistica structurală clarificase, în urmă cu mai multe
decenii, faptul că trecerea de la vorbire la limbă constituie
înlocuirea unei reprezentări continue cu una discretă.
Deoarece, după Ferdinand de Saussure, obiectul
lingvisticii este limba, nu vorbirea, modelele matematice
748

Rãni deschise

adoptate în lingvistică au fost multă vreme de natură
discretă. Algebra şi logica matematică păreau să fie
suficiente în această privinţă. Iată însă că din ce în ce mai
mult s-a conturat un proces surprinzător, acela al
aproximării discretului lingvistic prin modele continue sau,
altfel spus, de scufundare a celui dintâi în cele din urmă.
Topologia a pătruns în lingvistică în mai multe etape.
Topologiile, de obicei nemetrizabile, asociate diferitelor
relaţii binare studiate în morfologie şi sintaxă, s-au dovedit
a avea uneori semnificaţii deosebite. Diferite tipuri de
distanţe introduse în mulţimea frazelor unei limbi sau în
mulţimea părţilor ei au condus la probleme delicate, în
bună măsură nerezolvate. Ulterior, în studiul fenomenelor
de repetiţie în lingvistică, au pătruns metodele topologiei
algebrice, care asociază unui text diferite complexe
simpliciale. Prin teoria matematică a catastrofelor,
pătrunde în lingvistică topologia diferenţială. Noutatea,
aici, constă şi în faptul că nu mai este vorba de nivelul
morfologic, nici de cel sintactic, ci de nivelul semantic al
limbilor naturale. Relevanţa cu care René Thom a analizat
semantica verbului francez a marcat o cotitură în teoria
lingvistică. Gonit din limbă, refugiat în vorbire, conţinutul
lingvistic reapare prin schimbarea strategiei de investigare.
După René Thom, renunţarea la matematica continuă în
studiul limbii ar apărea la fel de nefericită ca renunţarea la
metodele analizei matematice în studiul numerelor prime.
749

Solomon Marcus

Dar, vorbind despre analiza matematică, trebuie să
observăm că din această ramură a matematicii, lingvistica
matematică, împrumută în ultima vreme nu atât rezultate
(deşi nici acestea nu lipsesc), cât concepte, idei şi metode.
Sistemele de ecuaţii cu necunoscute limbaje, teoremele de
punct fix, teoria măsurii, măsurile exterioare Caratheodory,
seriile formale s-au dovedit importuri bine inspirate ale
unor idei de analiză matematică astăzi devenite familiare
în teoria limbajelor şi a automatelor.
În prima fază de dezvoltare a lingvisticii matematice,
trei erau cu precădere criticile aduse acestei discipline:
limitarea la problemele limbii scrise, neglijarea semanticii
şi neglijarea aspectelor diacronice ale limbii. Prima critică
se baza pe faptul că una dintre preocupările principale ale
lingvisticii matematice era traducerea automată, interesată
exclusiv de limba scrisă. Între timp, însă, lingvistica
matematică s-a dovedit capabilă să cuprindă aspecte
esenţiale ale fonologiei, ale recunoaşterii formelor şi ale
foneticii. Cea de a doua critică a intrat şi ea în desuetudine,
astăzi semantica şi pragmatica aflându-se în centrul de
preocupări ale modelării logice în lingvistică. Cum rămâne
cu cea de a treia critică? Ea a devenit depăşită prin
impactul tot mai mare pe care calculatorul îl are asupra
lingvisticii istorice, asupra filologiei şi a dialectologiei.
Reviste de genul Computers and the Humanities (Statele
Unite) şi Linguistic and Literary Computing (Anglia) sunt
750

Rãni deschise

semnificative în acest sens. Să mai observăm că la
conferinţele internaţionale de lingvistică computaţională,
care se ţin o dată la doi ani, sunt prezentate numeroase
cercetări de istoria limbii.
Singura barieră care a mai rămas în faţa lingvisticii
matematice este ignoranţa.

751

