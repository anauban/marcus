Omagiu limbii române
Dialog cu Gabriela Melinescu,
Luceafărul, anul XV, nr. 16 (520),
15 aprilie 1972, pp. 1-6
Gabriela Melinescu: Am citit cartea dvs. Poetica
matematică. Mi se pare o carte unică prin strălucita ei
luciditate de a încerca să abordeze problemele limbajului
poetic cu ajutorul matematicii. Am extras din carte, pe
măsură ce am citit-o, după cum în urmă cu câţiva ani de
zile Nichita Stănescu a extras din textele teoretice ale lui
Cantemir pasaje întregi pe care le publicăm separat, ca pe
nişte poeme de un tip straniu.
Căutând să vă întâlnesc pentru acest interviu, am
fost şi la Universitate. M-am uitat pe toate orarele din
hol – am observat că ţineţi un curs „funcţii reale” la anul
II între orele 16 şi 18, în Amfiteatrul VI la Institutul
Pedagogic. Am vorbit cu câţiva studenţi. I-am întrebat
dacă au examen cu dvs. Am observat că, la auzul
numelui dvs., îi traversează un frison: astfel am aflat că
sunteţi un profesor fermecător, dar destul de sever; daţi
câte un 6 sau câte un 5 şi foarte rar nota 10. Acelaşi lucru
se spunea şi despre Dan Barbilian şi poate despre Lewis
Carroll...
91

Solomon Marcus

Ştiu că aţi scris monografii dedicate explorării
sistematice a limbajului: Lingvistică matematică,
Gramatici şi automate finite, Algebraic Linguistics
(Academic Press, New York), Introduction mathématique
á la linguistique structurale (Dunod, Paris), Algebraické
modely jazyka (Academia Praha), ştiu că de curând aţi sosit
din America.
Vă rog, îmi daţi voie, pentru început, să definim
matematica prin citatul pus ca emblemă la Poetica
matematică?
„Matematica este sora şi auxiliara necesară a artelor
şi este atinsă de nebunie şi geniu” (Marston Morse).
S.M.: Mai întâi vreau să vă spun că vă urmăresc de
multă vreme prin reviste, nu pot uita acele superbe creaţii
lexicale din Bobinocarii. Îmi aduc aminte şi de o poezie
mai veche, Cenuşereasa, care mi-a dat un fior.
G.M.: Vă mulţumesc foarte mult.
S.M.: Acum, în vederea acestui dialog am luat toate
volumele dvs. să-mi dau mai bine seama pe ce limbă
trebuie să vă vorbesc. V-am citit mai toate cărţile, de la
Ceremonie de iarnă la Fiinţele abstracte şi Boala de
origine divină şi am descoperit cu o imensă bucurie că mai
tot ce mi s-a întâmplat mie în matematică vi s-a întâmplat
şi dvs. în poezie. De fapt, singurul lucru probabil nou pe
care vreau să vi-l spun este acesta: că universul
matematicii şi cel al poeziei au o arhitectură foarte
92

Rãni deschise

asemănătoare, că dincolo de unele aspecte exterioare, care
par să le opună, ele au o esenţă comună.
Cuvintele marelui matematician Marston Morse mi
se par deosebit de profunde. Poezia este un amestec de
luciditate şi vrajă, am putea spune că luciditatea este
replica pe care poezia o dă nucleului ei de vrajă, pentru
a-l putea reliefa. Luciditate înseamnă aici organizare
(deliberată sau nu) a textului la diferitele sale niveluri.
Studiul acestei organizări nu poate fi dezvoltat cu fineţea
necesară fără ajutorul matematicii, deoarece organizare
înseamnă structură, iar structurile sunt obiectul
matematicii. Tocmai de aceea matematica e sora şi
auxiliara necesară a artelor.
G.M.: Dar nebună şi genială?
S.M.: Da, matematica e de mai multe ori nebună şi
genială: pentru că poate prefigura toate arhitecturile
posibile, deci nu numai pe cele existente, ci şi pe cele
potenţiale; pentru că ne dă posibilitatea să sesizăm
existenţa unor fiinţe de un grad atât de ridicat de
abstractizare, încât ele nu pot fi obţinute efectiv, nu pot fi
construite (de exemplu vă puteţi imagina un teritoriu care
nu are întindere? în matematică se demonstrează că există
aşa ceva, dar în acelaşi timp se demonstrează că un astfel
de teritoriu ne este inaccesibil); pentru că ne dă
posibilitatea să scrutăm infinitul cu ajutorul finitului. Dar
dvs. ştiţi mai bine decât mine că toate acestea se pot
93

Solomon Marcus

întâmpla în poezie, doar aţi scris atât de frumos despre
copiii fără trup, despre expediţia dumneavoastră afară din
lucruri şi tot dumneavoastră aţi scris „Calc pe întinse
cumpene/ sunt capătul unui echilibru total/ doresc o apă să
mă arunce/ în aripile unui animal”.
„Ca şi poezia, matematica nu se poate povesti”
G.M.: Deşi cuvântul „matematică” are alt înţeles
pentru dvs. decât pentru foştii elevi, totuşi vreau să vă
spun că eu am încredere în matematică, adică nu mi-e
frică de ea. N-am fost o elevă stălucită şi am avut spaimă
de profesorii de matematică ce ne-au format (celor mai
slabi evident) complexul matematicii. Totuşi, am rămas
cu o fascinaţie reală pentru orice nouă descoperire a
ştiinţei în care aflam cu stupoare că matematica a fost
folosită ca instrument. Deci o consider suverană printre
ştiinţe, după cum consider poezia suverană pentru arte.
Citind Poetica matematică, am ştiut că matematica, din
punctul de vedere al limbii, e vie, deci sigură ca orice
lucru abstract.
Spuneţi-ne ce loc ocupă matematica în viaţa noastră,
este ea cea mai mare instituţie, depind oare destinele ţărilor
(aşa cum se spune) de realizările acestei ştiinţe, de
realizările acelui corp de elită, de matematicieni? Are
gândirea ştiinţifică în secolul nostru un rol hotărâtor?
94

Rãni deschise

S.M.: Matematica ocupă un loc imens în viaţa
noastră, dar influenţa ei se exercită foarte indirect şi de
aceea mulţi nu o observă. S-a spus că matematica e
industria grea a ştiinţei şi ştiţi prea bine că puţini oameni
se gândesc că pentru ca hainele pe care le poartă să poată
fi obţinute, a fost nevoie de oţel şi de minereuri. De
exemplu, matematica influenţează nemijlocit gândirea
teoretică din fizică, aceasta influenţează fizica
experimentală, aceasta la rândul ei disciplinele inginereşti,
tehnica, apoi producţia de bunuri materiale. Vedeţi că sunt
multe verigi intermediare.
„De exemplu, medicul vine la matematician
cu o anumită problemă şi îi spune: Dacă se
rezolvă această problemă, s-ar putea găsi
pe baza ei leacul cancerului: matematicianul
nu-i rezolvă această problemă, dar îi pune
la îndemână un instrument pe baza căruia medicina
progresează mai repede.”
G.M.: Într-un interviu acordat revistei noastre,
academicianul Alex. Rosetti spunea că matematica nu
înlocuieşte nimic, că ea ajută numai, de pildă în ceea
ce priveşte dicţionarele şi că traducerea automată a
dat faliment, că prin ea se pot traduce numai cărţi
tehnice.
95

Solomon Marcus

Care e idealul matematicii, vrea ea să înlocuiască
ceva, să zicem, chiar artele? Poate să privească un poet cu
ochi buni un computer?
S.M.: Într-adevăr, matematica nu înlocuieşte nimic,
dar, aş adăuga, nici nu poate fi înlocuită prin altceva. Este
imposibil să se fixeze o limită apriorică posibilităţilor
matematicii. Aceasta înseamnă că odată dobândită o
anumită rază de acţiune a matematicii, există posibilitatea
de a o extinde. Matematica este pentru artă un model de
construcţie în care părţile se articulează într-un întreg. În
măsura în care opera de artă e organizată, artistul poate
învăţa mult din matematică. Orice operă de artă conţine o
anumită cantitate de matematică. Numărul parametrilor
care determină structura unei opere de artă, chiar dacă finit,
este destul de mare.
„...între muzica muzicii şi muzica versului nu există
nicio legătură mai adâncă şi frumuseţea sonoră
a versului nu se poate produce independent
de înţelesul pe care îl au vorbele.”
G.M.: Prin matematică, se poate face o cercetare
cantitativă a operei literare, dar un studiu calitativ, valoric
nu se poate obţine decât prin compararea acestor cantităţi,
adică a unei literaturi cu alta. Dar pot fi comparabili
autorii? După ce criteriu?
96

Rãni deschise

S.M.: Compararea a doi autori sau a două opere
literare nu se poate face în mod serios fără angajarea în
discuţie a tuturor parametrilor. Aceasta implică utilizarea
unor instrumente matematice foarte fine, iar operaţiile
logice corespunzătoare nu pot fi efectuate fără ajutorul
calculatorului electronic. Acest tip de cercetare de
literatură comparată a devenit de mulţi ani o activitate
obişnuită în multe centre universitare din lume şi există
reviste (cum ar fi Computers and the Humanities)
consacrate acestor cercetări. Sunteţi poet şi nu puteţi
tăgădui că în procesul de elaborare a poemelor dvs.
problemele de tehnică nu sunt de neglijat. Ce structuri
prozodice preferaţi, pe care le evitaţi, sunteţi sigură că
puteţi face aceasta (cu ochiul liber) în cele mai bune
condiţii? Mulţi artişti se încred nemăsurat în intuiţie, dar
cred că aşa cum un sculptor trebuie să cunoască temeinic
proprietăţile materialului cu care lucrează, e bine ca şi
poetul să ştie ce latenţe zac în structurile pe care le
manipulează, în ce constă realmente alegerea pe care el o
face şi dacă nu cumva se supune fără să-şi dea seama unor
constrângeri mult mai puternice decât s-ar părea.
Matematica şi calculatorul îl pot ajuta pe poet să înţeleagă
mai bine partea de tehnică şi de rutină a muncii sale, pentru
a lua distanţa necesară faţă de ele. Cu doi-trei ani înainte
de a muri, Perpessicius a publicat un articol în care a
atras atenţia că a sosit momentul ca studierea operei
97

Solomon Marcus

eminesciene să depăşească stadiul observaţiilor directe,
rezultat al unor simple impresii, şi să se treacă la folosirea
calculatorului electronic, pentru a se inventaria în mod
exhaustiv toate formele şi structurile pe care Eminescu
le-a utilizat. Concomitent, s-a elaborat la Institutul de
Lingvistică al Academiei şi un Dicţionar de frecvenţe al
operei lui Eminescu. Cu aceste două instrumente,
Dicţionarul de frecvenţe şi calculatorul electronic,
eminescologia ar putea trece la un stadiu superior. Avem
acum o catedră de eminescologie la Facultatea de Limbă
şi literatură română. Va da ea oare urmare acestui
imperativ? Ruşii au făcut de mult acest lucru cu Puşkin,
italienii cu Dante, englezii cu Shakespeare...
„O operă poetică de mare valoare ne dă totdeauna
impresia că în ea nimic nu se poate modifica, nimic
nu poate fi clintit de la locul său, nimic nu poate fi
suprimat sau adăugat.”
G.M.: În legătură cu traducerea automată, e adevărat
că textele literare nu se pot traduce?
S.M.: Da, un cuvânt şi despre traducerea automată.
Ea nu a eşuat, dar, ca orice cercetare, are sinuozităţile ei,
fluxurile şi refluxurile normale în orice investigaţie.
E adevărat că nu se pot traduce textele literare la calculator.
Dar aş dori să vă întreb, fără calculator ştim să traducem
98

Rãni deschise

texte literare? Ceea ce i se reproşează calculatorului că nu
poate să facă, nici omul nu poate să facă.
„Nu se poate sugera decât prin cuvinte precise”
G.M.: De fapt cine sunt matematicienii, aparţin unei
lumi deosebite faţă de a celorlalţi oameni? Vorbesc ei ca
şi ceilalţi aceeaşi limbă? Spuneaţi strălucit despre Ion
Barbu că unele expresii ale limbii comune capătă o
accepţie inedită, influenţate de matematică. Matematicienii
folosesc un limbaj esenţializat, formule, cod, folosesc şi
limba obişnuită. Are aceeaşi semnificaţie pentru
matematicieni limba în care vorbim.
S.M.: Cred că matematicienii dau o atenţie mai mare
aspectelor silogistice din limba comună, utilizează cu mai
multă grijă cuantificatorii logici, unele distincţii cum ar fi
aceea dintre condiţia necesară şi condiţia suficientă şi unii
termeni ca „derivată”, ca metafore în raport cu unii termeni
matematici.
Treptat, noi şi noi termeni matematici sunt transferaţi
în limba comună. Acum câteva sute de ani, „logaritm” era
un termen de specialitate, azi e un cuvânt în limba comună.
Cuvântul „algoritm” nici măcar nu figura în Dicţionarul
limbii române moderne, astăzi îl cunoaşte aproape toată
lumea. Mai interesant e poate procesul invers, de transferare
a unor cuvinte din limba comună în limbajul matematic.
99

Solomon Marcus

Acesta este de fapt punctul de plecare al metaforei
matematice. Ion Barbu a utilizat cu deosebită intensitate
aceste transferuri în însemnările sale de algebră. Sunt
convins că studierea sistematică a textelor sale matematice
va dezvălui o latură profundă a activităţii sale poetice.
„Tocmai pentru că este neechivalabilă, expresia
poetică poartă o încărcătură incomparabil
mai bogată decât expresia ştiinţifică.”
G.M.: În limba română mor ciudat cuvintele, „mor
ciudat” pentru că ele au şansa să învieze peste sute de ani,
reabilitate sau înviate de un mare scriitor.
În matematică se întâmplă aşa ceva? Moare ceva, se
demotează ceva, cum se supravieţuieşte?
S.M.: S-a spus că orice limbă este un depozit imens
de metafore moarte. Cuvintele au o origine metaforică, dar
metafora mamă moare la scurt timp după naşterea
cuvântului-fiu. De aici mai departe, cuvintele limbii
comune supravieţuiesc rutinei prin dezvoltarea conotaţiilor.
„Un drum este un lanţ, dar reciproca nu este adevărată”
G.M.: Ce sunt conotaţiile?
S.M.: Funcţia denotativă a unui cuvânt este aceea pe
care o defineşte dicţionarul, orice utilizare a unui cuvânt
100

Rãni deschise

cu o funcţie alta decât aceea denotativă este, prin definiţie,
o utilizare conotativă. Dar orice conotaţie este un act
irepetabil, deoarece reluarea ei e moartea ei, e
transformarea ei într-o nouă denotaţie. Miracolul poeziei
constă în capacitatea de a cristaliza, de a da o relativă
stabilitate unor conotaţii. Evoluţia limbajului de la starea
lui comună la cea poetică e bazată pe conotaţie, metafora
intervine doar în măsura în care conotează. Evoluţia
limbajului poetic modern se face în sensul diminuării
rolului metaforei. În matematică, cuvintele au o evoluţie
mai puţin zbuciumată, dar nu mai puţin interesantă.
Limbajul matematic nu poate conota şi caută să
compenseze prin metaforă ceea ce nu poate obţine prin
conotaţie. În limbajul natural mor mereu conotaţii şi se
nasc altele; în limbajul matematic se acumulează mereu
noi metafore, fără însă ca cele vechi să dispară, ele doar
se demodează. Dezvoltarea limbajului matematic se face
în sensul construirii unor metafore de grad din ce în ce mai
ridicat: metafore, metafore la metafore, metafore la
metafore de metafore ş.a.m.d. Este de fapt procesul
obişnuit de abstractizare şi generalizare care caracterizează
matematica. Caracterul fluent, anticonceptual, al
limbajului poetic, concretizat în moartea imediată a
conotaţiilor care abia s-au născut se opune caracterului fix
al limbajului matematic, concretizat în persistenţa
metaforei vechi alături de cea care abia s-a născut. Dar
101

Solomon Marcus

procesul de dezvoltare a metaforelor este aici nelimitat,
aşa cum nelimitat este şi limbajul poetic. Deci în timp ce
limbajul poetic se depărtează de metaforă şi se îndreaptă
spre metonimie, se îndepărtează de semnele iconice şi se
apropie de cele simbolice, limbajul matematic parcurge o
evoluţie inversă, el tinde tot mai mult spre metaforă şi
iconicitate.
„Limbajul poetic stă sub semnul vrajei,
cel matematic sub semnul lucidităţii”
G.M.: Lingvistica are nevoie de matematică pentru
că este logică şi numai astfel poate folosi ca mijloc de
cercetare matematica. Dar literatura nu este logică şi,
paradoxal, acum cu ajutorul matematicii ne-aţi demonstrat
că se poate studia literatura.
Ştiinţa clarifică noţiunile, iar arta le aprofundează,
făcându-le misterioase şi de neelucidat. Nu este studiul
matematic al poeziei posibil, dar paradoxal?
S.M.: Există o logică a limbii, dar limba nu este
logică; de exemplu, propoziţia logică este cu totul altceva
decât propoziţia lingvistică. Logica este un atribut al
matematicii, dar ea nu exprimă esenţa matematicii. Logica
este doar igiena matematicii; conţinutul matematicii îl
constituie studiul structurilor. Tocmai de aceea lingvistica
şi studiul literaturii au nevoie de matematică. Atât în
102

Rãni deschise

limbă, cât şi în artă ne aflăm în prezenţa unor structuri. În
ştiinţă, ca şi în artă, se deschid şi se amplifică mereu noi
mistere. Orice problemă rezolvată este terenul unor
întrebări mai dificile decât cele precedente. Raportul
dintre numărul întrebărilor şi cel al răspunsurilor creşte
pe măsură ce ştiinţa progresează, am putea spune că
maturitatea şi gradul nostru de cultură se măsoară prin
capacitatea noastră de a ne nedumeri, de a ne uimi, deci
din acest punct de vedere nu există nimic paradoxal în
studiul matematic al poeziei; ca şi poezia, matematica
generează neîncetat mistere.
Matematica este regenerată de mulţimea acelor
întrebări care se nasc mereu, la infinit, şi care vitalizează
ştiinţa matematicii.
G.M.: Dar cu ce puteţi compara postulatele în limbă?
S.M.: Aceasta este o întrebare care preocupă pe mulţi
cercetători ai limbajului. Dicţionarele lingvistice obişnuite
au un caracter circular, tocmai pentru că nu conţin cuvinteaxiome. Lingvistica modernă a introdus postulatele atât în
teoria gramaticală (sub formă de elemente ale dicţionarului
neterminal), cât şi în semantică, sub formă de trăsături
semantice diferenţiale. Postulatele lingvistice sunt nişte
metaelemente, cu alte cuvinte ele nu participă la formarea
frazelor, ci numai la generarea lor.
G.M.: Există în matematică absurd, cum există şi în
limbă? Dar suprarealism şi gratuitate?
103

Solomon Marcus

S.M: Cred că unele preocupări de logică matematică
şi fundamentele matematicii corespund suprarealismului
din literatură. Gratuitate există în matematică, atunci când
matematicianul nu e atent decât la corectitudinea
silogistică a consideraţiilor sale, uitând de finalitatea lor,
rămânând indiferent la capacitatea de reflectare a
conceptelor cu care lucrează şi la semnificaţia problemelor
pe care şi le pune.
G.M.: Ca matematician, ce credeţi despre viitorul
limbii române? Limbile tind să se universalizeze, limbile
marilor state. E posibilă universalizarea limbilor? Sau vom
vorbi pur şi simplu în forme matematice?
S.M.: Într-adevăr, are loc un proces de apropiere a
limbilor. Colegii mei matematicieni din multe alte ţări
mi-au spus de multe ori că pot înţelege textele româneşti
de matematică. În trecut, lucrul acesta era mai greu. De
exemplu, în trecut, pentru „mulţime” şi „ansamblu” se
spunea „grămadă” şi „gloată”. Savanţii încearcă să
elaboreze limbaje internaţionale şi chiar cosmice.
Deocamdată, singura cale mai interesantă în elaborarea
limbajelor internaţionale este aceea a limbajelor ştiiţifice
formalizate.
G.M.: Ce semnificaţie are pentru dvs. numărul, se
poate compara numărul cu cuvântul în limbă?
S.M.: Cred că poetul Nichita Stănescu are dreptate:
numerele sunt cuvinte. Am în vedere numerele raţionale,
104

Rãni deschise

care, chiar dacă au fost cândva nişte simboluri artificiale,
deci în afara limbilor naturale, au devenit, prin uz
necontenit, elemente ale limbilor naturale. Nu acelaşi lucru
se poate spune despre numerele iraţionale şi în special
despre cele transcendente. Acestea rămân încă, pentru
multă vreme, în afara limbilor naturale.
G.M.: Aş vrea să reţin pentru mine: numere raţionale,
iraţionale, transcendente, imaginare...
S.M.: În lingvistica matematică mulţimea numerelor
întregi se reprezintă ca un limbaj pe un vocabular format
dintr-un singur element, fiecare frază a limbajului
reprezentând un număr întreg. Deşi în principiu limbajul
matematic se poate dispensa de expresiile limbii naturale,
matematicienii preferă să utilizeze simboluri şi formule
numai atunci când parafrazarea lor prin cuvinte ar fi
neeconomică. Ca şi omul de rând, matematicianul
gândeşte prin intermediul cuvintelor. Şi cred că limbajul
articulat este faţă de celelalte limbaje ceea ce spaţiul
euclidian este în raport cu diferitele spaţii abstracte pe care
le studiază matematica sau pe care le poate imagina un
artist. Toate tipurile de creaţie umană se bazează pe
capacitatea noastră de creaţie verbală.
G.M.: Matematica exclude ideea de divinitate, de
dumnezeu?
S.M.: E adevărat că matematica porneşte de la
postulate şi postulatele sunt, prin natura lor, date. A asimila
105

Solomon Marcus

existenţa lor cu ideea de divinitate nu mi se pare legitim,
deoarece considerarea unor fapte ca date e rezultatul unei
viziuni aposteriori asupra structurilor matematice, aceasta
se petrece în ordinea sistematică, nu şi în ordinea genetică,
euristică a lucrurilor.
G.M.: Pe baza matematicii, nouă, oamenilor, s-ar
putea să ne meargă mai bine?
S.M.: De multă vreme, oamenilor le merge mai
bine datorită matematicii. Binefacerile tehnicii moderne
sunt, indirect, şi un rezultat al folosirii metodelor
matematicii. Mai rămâne însă ca matematica să fie şi un
prilej de bucurie spirituală, deocamdată măcar pentru
profesionişti ai culturii. Matematica este încă pentru
mulţi scriitori un simbol al rigidităţii, al şablonului, al
lipsei de nuanţă.
„... Simularea matematică ori cibernetică a poeziei
poate deveni într-un viitor apropiat un adevărat
«cui al lui Pepelea» în casa criticii literare.”
G.M.: Exageraţi, numai pentru cei care au rămas în
trecut, deşi pretind că sunt contemporanii noştri. Am fost
emoţionată zilele trecute, când, răsfoind o revistă, am aflat
că graţie calculelor matematice ale unui ordinator a fost
posibilă obţinerea fotografiei color a unui ciclon. Aşadar,
radiografia în amănunt a urgiei.
106

Rãni deschise

S.M.: S-au făcut afirmaţii hazardate despre
matematică şi despre calculatoare, încât mă întreb ce-ar
trebui să se întâmple pentru ca prejudecata disciplinelor
exacte opuse celor umaniste să dispară? Scriitorii aspiră la
o cunoaştere integrală a lumii; ei nu pot rămâne indiferenţi
la expansiunea culturală pe care o trăieşte matematica
modernă.
G.M.: Nici nu rămân. Chiar şi cei care au rămas în
trecut, având aceste prejudecăţi, vor fi târâţi până în
realitate şi siliţi să fie martorii descoperirilor fabuloase ale
ştiinţei din secolul nostru. La ce vă gândiţi, ce idei noi
aveţi?
S.M.: Lucrez acum la o carte în care încerc să arăt că
lingvistica a devenit în ultima vreme la fel de expansionistă
ca şi matematica. Deviza cărţii este ideea pe care am
menţionat-o mai sus: Limbajul articulat este la baza
capacităţii noastre de a crea şi manipula limbaje de orice fel.
„Verbul e un cuvânt privilegiat, aşa cum eroul e
un personaj privilegiat”
G.M.: Făcând excepţie de Poetica matematică, v-a
inspirat vreodată un poet sau o poezie, vreo teoremă
matematică?
S.M.: O poezie nu poate inspira, nemijlocit, o
teoremă, dar poezia ca atare dezvoltă imaginaţia şi gustul
107

Solomon Marcus

matematicianului pentru demonstraţii elegante. O poezie
poate sugera o problemă, de exemplu poezia
Correspondences de Baudelaire sugerează cercetarea
izomorfismelor dintre structurile plastice şi cele sonore.
Dar şi reciproca e adevărată; poate că existenţa unor
proporţii identice în muzică şi în arhitectură (cum ar
fi numărul de aur) a sugerat celebrele sinestezii
baudelairiene.
„Limbajul poetic îl domină pe om, îl menţine pe
acesta în starea «religioasă» a vrajei”
G.M.: Ca şi limba, matematica are sens. Este oare şi
ea un izvor de afectivitate?
S.M.: Aceasta depinde de temperamentul fiecărui
matematician. Unii matematicieni trăiesc doar viaţa internă
a matematicii. Alţii fac dese incursiuni în zonele de contact
ale matematicii cu celelalte discipline şi cu lumea
exterioară. Primii trăiesc mai intens frumuseţile
arhitecturale ale matematicii, alţii savurează mai cu seamă
procesul de matematizare ca atare.
G.M.: Vreau să vă întreb: de fapt, vă face matematica
fericit?
S.M.: Ca şi arta, matematica ne poate da mari bucurii
şi tristeţi fecunde. Dar numai dragostea ne poate face
fericiţi.
108

