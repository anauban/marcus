Matematica, faţă cu viitorul ei
Viaţa studenţească, anul XII, nr. 43,
20 decembrie 1967, pp. 6-7
Am urmărit îndeaproape lucrările subsecţiei de
Analiză şi Ecuaţii diferenţiale a secţiei de Matematică,
Fizică şi Mecanică a celei de a IX-a Conferinţe pe ţară a
cercurilor ştiinţifice studenţeşti, care a avut loc recent la
Universitatea din Timişoara. Mărturisesc că nu fără emoţie
i-am urmărit strânşi laolaltă pe aceşti studenţi, către care se
îndreaptă atâtea speranţe. Şcoala matematică românească
este astăzi cunoscută în întreaga lume. Interesul pe care ea
îl stârneşte este atât de mare, încât în Statele Unite ale
Americii s-a publicat de curând un mic manual de limba
română, special dedicat matematicienilor de limbă engleză
care au de consultat articole de matematică publicate în
limba română. Va fi oare menţinut acest nivel, îşi va
menţine matematica românească şi în viitor faima pe care
o are acum? Multe fapte recente ne îndreptăţesc să credem
acest lucru şi printre ele se numără şi recenta conferinţă de
la Timişoara. Acum, după ce ea s-a încheiat, putem spune
că ea nu a dezminţit aşteptările. Multe dintre comunicările
prezentate – şi nu numai cele premiate sau menţionate –
s-au situat la nivelul maximei exigenţe, ele urmând să fie
52

Rãni deschise

publicate în periodice ştiinţifice de specialitate, alături de
articole ale matematicienilor formaţi, care se află departe
de începutul carierei lor ştiinţifice. Marea varietate tematică
a comunicărilor a inclus probleme de Analiză clasică, dar
şi probleme de Analiză funcţională. Este remarcabil faptul
că mulţi dintre autorii comunicărilor şi-au pus singuri
problemele pe care le-au studiat şi au imaginat singuri căile
de rezolvare, rolul conducătorului ştiinţific reducându-se
în acest caz la verificarea lucrării şi la observaţii de detaliu.
Aceasta este forma supremă a activităţii ştiinţifice
studenţeşti, forma în care studentul confirmă că şi-a însuşit
în cel mai înalt grad gândirea ştiinţifică, o trăieşte şi este în
stare s-o ducă mai departe, sesizând singur semnele de
întrebare şi fiind în stare să dezvolte întreaga sa iniţiativă,
în scopul obţinerii unui răspuns.
Multe dintre comunicările prezentate continuă tradiţii
preţioase ale şcolii matematice româneşti. Astfel,
comunicarea tov. Ionel Ţevy (premiul I), student în anul
al IV-lea la Universitatea din Bucureşti, a readus în
actualitate conceptul de continuitate a unei funcţii
multiforme, introdus în urmă cu patru decenii de către
matematicianul român de renume mondial Florin
Vasilescu, iar comunicările studenţilor clujeni s-au situat
pe linia preocupărilor de teoria aproximării funcţiilor şi
analiză numerică, ale şcolii matematice clujene, de sub
conducerea acad. prof. Tiberiu Popoviciu.
53

Solomon Marcus

Deosebit de semnificativă a fost prezenţa la
conferinţă a unor studenţi din anul întâi, care şi-au elaborat
lucrările prezentate încă în urmă cu vreo opt luni, când mai
erau elevi de liceu. Astfel, Eugen Popa (premiul al doilea),
de la Universitatea din Iaşi, a dat o nouă caracterizare a
spaţiilor uniforme, lucrarea sa fiind publicată în revista
Studii şi cercetări matematice a Academiei, iar Dan
Voiculescu (menţiune) de la Universitatea din Bucureşti a
elaborat o lucrare privind o relaţie de echivalenţă între
şiruri de numere naturale, lucrare care urmează, de
asemenea, să fie publicată. Ambele comunicări au produs
o frumoasă impresie, prin ţinuta lor ştiinţifică şi prin
prezentarea competentă şi degajată.
În zilele conferinţei, i-am putut urmări pe autorii
comunicărilor, nu numai între zidurile Universităţii, dar şi
în orele libere, pe străzile Timişoarei. Am observat că
discuţiile ştiinţifice îi pasionau; îşi relatau unii altora
conţinutul comunicărilor şi nu lipsea spiritul critic ascuţit
(care, din păcate, în cadrul şedinţelor de comunicări s-a
manifestat mai puţin, poate şi din cauza prezenţei
inhibitoare a cadrelor didactice). Este semnificativ faptul
că unul dintre primele drumuri pe care le-au făcut mulţi
dintre aceşti studenţi, imediat după sosirea la Timişoara, a
fost la librăria din centrul oraşului, pentru a vedea dacă nu
cumva se mai găsesc aici unele cărţi ştiinţifice la care ţin
mult şi care în librăriile bucureştene s-au epuizat prea
54

Rãni deschise

repede. Multă seriozitate, interes autentic pentru ştiinţă,
ore multe petrecute în sălile de lectură ale bibliotecilor sau
căminelor, iată ce-i caracterizează pe aceşti studenţi, pe
care cât mai mulţi colegi ai lor ar trebui să-i urmeze.
Lucrurile frumoase nu ne permit însă să trecem cu
vederea unele aspecte negative. Mai întâi, trebuie să
observăm slaba reprezentare a comunicărilor privind
domenii interdisciplinare, ca economia matematică,
biologia matematică, lingvistica matematică, domenii care
se află azi în plină dezvoltare. Trebuie, de asemenea, să
observăm că modul de prezentare a comunicărilor nu a fost
întotdeauna corespunzător. Unii autori de comunicări
expun numeroase odetalii cu caracter tehnic şi nu pot
renunţa la ele, chiar când sunt invitaţi să o facă, fără însă
a fi în stare să desprindă linia generală a unei demonstraţii
şi să explice în mod convingător interesul problemei
studiate.
Fără îndoială că viitoarele sesiuni ştiinţifice
studenţeşti vor marca noi progrese, iar ştacheta exigenţei
se va ridica tot mai sus, asigurând astfel de perspective din
ce în ce mai îmbucurătoare dezvoltării ştiinţei în patria
noastră.

55

