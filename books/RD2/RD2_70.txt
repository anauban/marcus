Matematica şi problemele societăţii
Era socialistă, nr. 13, 5 iulie 1982, pp. 22-24
Plenara lărgită a Comitetului Central al P.C.R. de la
începutul lunii iunie a.c. a reafirmat şi accentuat rolul tot mai
important al ştiinţei în construirea societăţii socialiste
multilateral dezvoltate. De asemenea, Congresul Educaţiei
politice şi culturii socialiste a abordat sub diverse aspecte
această problemă. Dacă, în principiu, acordul asupra ponderii
importante a ştiinţei în viaţa societăţii este unanim, căile
concrete de transformare a ştiinţei într-o forţă socială sunt,
deocamdată, numai parţial cunoscute şi valorificate. Această
problemă este cu deosebire spinoasă în ceea ce priveşte
matematica. Ei îi sunt consacrate consideraţiile care urmează.
Există cel puţin două aspecte esenţiale de care trebuie să
se ţină seamă în înţelegerea rolului social al ştiinţei, în
particular al matematicii. Primul se referă la apariţia
problemelor şi crizelor cu caracter global privind hrana,
materiile prime, energia, explozia demografică, mediul
ambiant, cursa înarmărilor ş.a.m.d.; al doilea aspect se referă
la revoluţia tehnico-ştiinţifică şi la actuala revoluţie industrială.
Legătura indisolubilă dintre aceste două aspecte a fost
analizată în numeroase lucrări, o parte dintre ele aflându-se la
527

Solomon Marcus

dispoziţia cititorilor noştri, în monografii de prestigiu (cele mai
multe publicate în colecţia „Idei contemporane” a Editurii
politice). În mod particular se remarcă diferitele rapoarte către
Clubul de la Roma, traduse în româneşte, ca şi cartea
Revoluţiile industriale în istoria societăţii, publicată în 1981.
Legătura principală dintre cele două aspecte
menţionate mai sus este concretizată în următoarea
întrebare: în ce măsură ne ajută ştiinţa să răspundem
într-un mod corespunzător la alternativa „ a supravieţui sau
nu” ce stă în faţa omenirii? Iată o problemă pe care fiecare
disciplină ştiinţifică şi-o pune şi încearcă s-o rezolve. Dacă
la o creştere geometrică a necesităţilor posibilităţile de a
le satisface vor creşte doar în progresie aritmetică, atunci
impasul, în loc să fie înlăturat, se va adânci. Trebuie să
găsim deci răspunsuri noi, mai eficiente, la întrebările
cărora ieri le-am dat un răspuns mai puţin eficient1.
În ce măsură ajută matematica la înţelegerea şi
rezolvarea problemelor globale ale omenirii? În ce măsură
şi în ce fel este implicată matematica în noua revoluţie
industrială? Iată întrebări pe care lumea de azi le adresează
matematicii şi pe care matematicienii înşişi şi le adresează.
Acel model latino-american al lumii – publicat şi la noi
sub titlul „Catastrofă sau o nouă societate?” – se vrea a fi
1

A se vedea, în acest sens, J. Botkin, M. Elmandjra, M. Maliţa, Orizontul
fără limite al învăţării, Editura Politică, 1981.

528

Rãni deschise

un model matematic nu din snobism, ci pentru că nu se
poate altfel. Acest model, concentrat în jurul satisfacerii
nevoilor umane fundamentale, atrage atenţia asupra
implicării inevitabile a matematicii în procesul de
înţelegere a actualelor probleme globale ale omenirii. De
altfel, matematica este esenţial implicată şi în unele
rapoarte către Clubul de la Roma, iar cercetările privind
energia, ecologia, explozia demografică, înarmarea şi
dezarmarea sunt, intr-o anumită fază a lor, profund
matematizate.
Nu alta este situaţia în ceea ce priveşte noua revoluţie
industrială. Această revoluţie, cu consecinţe adânci în ceea
ce priveşte forţele şi relaţiile de producţie, are drept una
dintre principalele ei componente mutaţiile aduse de
calculatoarele electronice. Dar calculatorul electronic
modern, cu variantele sale, de la calculatorul de buzunar
cu program la microprocesoare şi marile calculatoare
electronice, nu înseamnă numai o performanţă de ordin
tehnic, ci şi o schimbare radicală a tipului de gândire. Dacă
realitatea fizică a calculatorului, cu miniaturizările sale
recente, care ne uimesc, este şi expresia strălucită a
inteligenţei tehnice, ceea ce pune la lucru aceste maşini
revine la anumite programe care, la rândul lor, se bazează
pe anumite modele matematice, de natură algoritmică.
Fără gândire algoritmică nu poate fi concepută actuala
revoluţie industrială, iar fără a ne prevala de ceea ce
529

Solomon Marcus

această revoluţie ne pune la dispoziţie nu putem spera să
rezolvăm problemele globale care ne stau în faţă.
Nu încape îndoială că, printre problemele globale ale
omenirii, se află şi ştiinţa. Dar ştiinţa are, la rândul ei,
propriile sale probleme globale, şi una dintre acestea este
matematica. De fapt, matematica a devenit o problemă
globală nu numai a ştiinţei, ci a întregii culturi;
expansiunea interdisciplinară a matematicii este o mărturie
în acest sens.
Dar, aşa cum globalizarea unor probleme
fundamentale ca cele privind energia, ecologia, cursa
înarmărilor a dus la tot atâtea crize la nivel planetar,
creşterea razei de acţiune a matematicii şi a răspunderilor
ei faţă de societate (ca şi ale societăţii faţă de ea) este
confruntată cu o criză pe care o parcurg relaţiile
matematicii cu lumea de azi.
Vom numi acest fenomen „criza matematicii”, dar
trebuie să fie clar, de la început, că nu este vorba de o criză
internă, de creştere, a matematicii. Din punctul de vedere
al vieţii ei interne, matematica progresează impetuos, după
cum o arată revistele, cărţile şi diferitele manifestări de
specialitate. Vitalitatea matematicii este tot mai mare, se
dezvoltă noi şi noi domenii, apar probleme noi, unele
deosebit de grele şi de interesante. Criza la care ne referim
are în vedere raporturile matematicii cu societatea, cu
publicul, cu învăţământul, cu ansamblul culturii umane.
530

Rãni deschise

Este vorba de o criză de receptare şi de utilizare a
matematicii, de transmitere a ei către noile generaţii.
Desigur, această criză nu poate rămâne fără consecinţe
asupra vieţii interne a matematicii.
Conştientizarea nevoii de matematică
Fără îndoială, este întrucâtva paradoxală această
situaţie. Nu se vorbeşte oare de un număr bun de ani
despre pătrunderea matematicii peste tot, despre faptul
că nu mai rămân aproape niciun domeniu, nicio sferă de
activitate socială care să nu fie afectate de gândirea
matematică? Într-adevăr, aşa este, numai că această
expansiune a matematicii se manifestă la nivelul
cercetării şi al posibilităţilor, în timp ce dificultăţile apar
atunci când este vorba de implementarea gândirii, a
metodelor şi rezultatelor matematicii în învăţământ, în
industrie, în agricultură, în planificare şi organizare, în
cultură. Dificultăţile apar, de asemenea, în ceea ce
priveşte capacitatea societăţii de a conştientiza şi de a
explicita nevoia ei de matematică, de a formula
problemele pe care ea le adresează matematicii, ca şi în
ceea ce priveşte capacitatea matematicii de a sesiza
problemele matematice ascunse în atâtea şi atâtea
întrebări grave pe care le formulează tehnica, economia,
cultura, viaţa de fiecare zi.
531

Solomon Marcus

Să încercăm să aprofundăm această situaţie.
Matematica devine tot mai abstractă şi mai complexă, se
transformă tot mai repede, tehnicile ei devin tot mai
complicate, jargonul ei tot mai special. În acest sens, se
poate spune că ea „se aristocratizează” tot mai mult. Chiar
obiectul ei este controversat, matematicienii, ca şi filosofii,
nu se mai înţeleg asupra modului în care trebuie definită
matematica. Suntem departe de vremea în care matematica
se reducea la studiul relaţiilor cantitative şi al formelor
spaţiale din lumea înconjurătoare. Universurile imaginare
ale matematicii le concurează azi pe cele ale artei.
Dar această „evadare” a matematicii nu este decât
rezultatul evoluţiei procesului de cunoaştere. Excelând în
dezvoltarea posibilităţilor sale de cunoaştere, matematica
a înregistrat însă mai puţine succese în dezvoltarea
posibilităţilor ei de comunicare cu potenţialii beneficiari.
Invadând mai toate domeniile de activitate, matematica ar
fi trebuit să devină din ce în ce mai „democratică”, mai
populară, mai accesibilă şi mai agreabilă. Limbajul ei ar fi
trebuit să fie deprins cu convingere şi cu plăcere de către
mai toţi cetăţenii, şi în special de către tineri. Acest lucru
însă nu prea se întâmplă.
Aşadar, în acest dialog insuficient al matematicii cu
celelalte domenii, cu societatea în ansamblul ei, se află
sursa crizei la care ne referim. Desigur, ar fi greşit să se
creadă că matematicienii nu şi-au pus această problemă.
532

Rãni deschise

Nu numai că şi-au pus-o, dar au şi obţinut rezultate
remarcabile. Colaborarea matematicii cu atâtea şi atâtea
alte domenii, cu industria şi cu agricultura, cu medicina şi
cu lingvistica, cu economia şi cu ingineria, cu sociologia
şi cu muzica este rezultatul unor eforturi atât din partea
matematicienilor, cât şi a nematematicienilor. Numai că
rezultatele obţinute sunt mult inferioare nevoii obiective
de matematică pe care societatea o are acum, ca şi
posibilităţilor reale pe care matematica actuală le oferă.
Din păcate, societatea, prin instituţiile ei, nu reuşeşte încă
să conştientizeze această nevoie crescândă de matematică.
Îngrijorarea care se manifestă în rândul
matematicienilor – mai cu seamă al celor care sunt
interesaţi în procesul de învăţământ matematic şi în
relaţiile matematicii cu publicul, cu potenţialii ei
beneficiari – este vizibilă. Ea s-a manifestat puternic la
ultimele congrese internaţionale de educaţie matematică
(1976 în R.F.G. şi 1980 în S.U.A.), ca şi la ultimul congres
internaţional al matematicienilor (Helsinki, 1978). O carte
recent apărută şi intitulată Matematica mâine2 aduce în
atenţie punctele de vedere ale mai multor specialişti în ceea
ce priveşte dezvoltarea matematicii în anii care vin. Chiar
titlurile unora dintre studiile care alcătuiesc acest volum
2

Lynn Arthur Steen (sub redacţia), Mathematics Tomorrow, Springer
Verlag, 1981.

533

Solomon Marcus

constituie o provocare adresată cititorului şi ne lasă să
bănuim caracterul lor profund polemic, născut dintr-o
adâncă nemulţumire: „Matematica aplicată este o
matematică rea”; „A rezolva ecuaţii nu este totuna cu a
rezolva probleme”; „Citiţi-i pe maeştri”; „Când
matematica devine propagandă”; „Adevărata criză
energetică”; „Femeile şi matematica”; „Declinul analizei
şi creşterea matematicii discrete”; „Cum să vindem
matematica?”.
Impasul la care ne referim este al întregii noastre
planete. Dar, desigur, există diferenţe de la o ţară la alta,
datorate deosebirilor de grad de dezvoltare economică, de
orânduire socială şi de nivel de dezvoltare a ştiinţei în
general, a matematicii în special. Ne interesează însă, în
primul rând, forma pe care problemele de mai sus o capătă
în ţara noastră.
În aprilie a.c. a avut loc o sesiune ştiinţifică a
Academiei Republicii Socialiste România cu tema
„Matematica în lumea de azi şi de mâine”. Nu întâmplător,
sesiunea a fost iniţiată de Comisia de prognoză a
Academiei. Această sesiune a reunit, alături de
matematicieni, pe unii dintre acei specialişti din fizică,
biologie, inginerie, ştiinţe social-umaniste, pentru care
matematica aparţine organic metodologiei de cercetare.
Dar va trebui, cred, să ne mai întâlnim într-un context mai
larg, din care să nu lipsească reprezentanţi ai industriei,
534

Rãni deschise

comerţului şi agriculturii, ai unor ministere şi institute
centrale, ai unor academii de ştiinţe şi uniuni de creaţie, ai
elevilor şi studenţilor, ai părinţilor lor.
Inteligenţa umană şi formarea gândirii matematice
Toate prognozele privind următoarele decenii sunt de
acord asupra nevoii crescânde de tehnică şi de gândire
matematică. Dar niciuna nu este în stare să detalieze
această idee generală. Una dintre dificultăţi constă în aceea
că nu suntem încă în stare să precizăm interacţiunile
matematicii cu celelalte discipline, ale matematicii cu
societatea. În consecinţă, nu ştim să evaluăm dependenţa
de matematică a viitoarelor profesii. Dar este cert că
problemele privind mediul ambiant, energia, eficienţa
producţiei, planificarea, decizia, prognoza, informaţia,
comunicarea nu vor putea fi abordate fără o matematică
adecvată.
Despre rolul matematicii într-un domeniu sau altul
s-a scris mult şi ne propunem aici să insistăm asupra
acestei chestiuni. Dar dacă problemele grave care stau în
faţa omenirii reclamă un tip nou de învăţare, creativă,
prospectivă şi participativă, atunci este vital să înţelegem
că chestiunea principală este aceea a formării gândirii
matematice, ca o componentă esenţială a inteligenţei
umane. Gândirea matematică, azi, înseamnă, deopotrivă,
535

Solomon Marcus

gândirea algoritmică şi combinatorie, deductivă şi
probabilistică, analogică şi generalizatoare, inductivă şi
sistemică. Înţelegem oare cum se vor schimba poimâine
economia şi cultura ţării noastre dacă mâine vom reuşi să
punem în mâinile fiecărui elev un calculator de buzunar
programabil? Explozia de inteligenţă şi fantezie stimulată
în acest fel ar putea avea efectul descoperirii unor noi surse
de energie.
Şi totuşi, în mod bizar, noi, în loc să pregătim această
etapă printr-o educaţie corespunzătoare, prin jocuri
combinatoriale, strategice şi algoritmice, diferenţiate după
vârste, ne-am orientat spre însuşirea unor aspecte formale
care, în cel mai bun caz, reuşesc să răspundă la o serie de
întrebări pe care elevul nu şi le-a pus niciodată. Prin
distanţa mare dintre promisiune şi îndeplinirea ei, dintre
efortul pretins şi recompensa oferită, prin stilul ei artificial,
prin eludarea caracterului ludic pe care-l presupune orice
proces de învăţare, până la o anumită vârstă, matematica
şcolară – în ciuda unor progrese în chestiuni de
corectitudine şi rigoare, de modernizare a tematicii şi
limbajului – se află în faţa unui eşec.
Influenţa calculatorului electronic asupra dezvoltării
matematicii în ultimele decenii a fost imensă. Un spirit
nou, o problematică nouă, accente noi au apărut până şi în
cele mai vechi şi venerabile discipline matematice, ca să
nu mai vorbim despre noile discipline, create ca urmare a
536

Rãni deschise

dezvoltării calculatoarelor electronice. Nu este aici locul
de a intra în detalii, dar mi se pare semnificativă
constatarea lui E.P. Miles jr. (în cartea Matematica mâine,
menţionată anterior), după care faptul că în S.U.A.
departamentele de informatică sunt separate de cele de
matematică reprezintă o consecinţă a incapacităţii multor
departamente de matematică de a-şi incorpora, ca o nouă
componentă, informatica.
Desigur, informatica nu este numai matematică, ea
are o importantă componentă inginerească, dar, sub
aspectul gândirii pe care o promovează şi pe care o
presupune calculatorul electronic, acesta se reclamă, în
primul rând, de la matematică. Formarea unei astfel de
gândiri ar trebui să înceapă de la vârsta de 3-4 ani, cu
ajutorul unor jocuri adecvate, ce-l introduc pe copil în
funcţionarea de tip algoritmic, ca şi prin observarea
mediului înconjurător, care abundă în obiecte de tip
maşină, cu reguli care conduc de la anumite intrări la
anumite ieşiri. Această preocupare ar trebui să existe în
întregul învăţământ (primar, gimnazial, liceal şi superior)
şi la toate disciplinele şcolare, deoarece gândirea
informatică este esenţială în învăţarea matematicii şi în
aceea a gramaticii, în inginerie şi în economie, în
planificare şi contabilitate, în muzică şi sport.
În acest sens, cred că ar trebui să revizuim întregul
mod de a concepe tipurile de jocuri pentru copii, structura
537

Solomon Marcus

revistelor rebusiste, unele rubrici distractive din reviste,
din programele de radio şi televiziune. Oare aceste canale
de comunicare cu publicul, în special cu tineretul, n-ar
putea fi folosite într-o mai mare măsură pentru dezvoltarea
gândirii problematice şi strategice, combinatoriale şi
algoritmice? Enunţarea unei probleme atrăgătoare, în
pauza de un minut dintre două emisiuni, organizarea unor
mici concursuri ar putea stimula pe tineri (şi oare numai
pe ei?) să se orienteze spre anumite preocupări atât de
importante în societatea de mâine.
Probleme numeroase stau în faţa învăţământului
matematic în ceea ce priveşte structura programelor şi
manualelor, stilul de predare a matematicii. Din păcate,
modernizarea predării matematicii a fost de multe ori
înţeleasă unilateral, sub aspectul exclusiv al formalismului
matematic, al limbajului şi simbolismului de ultimă oră.
Din acest punct de vedere, introducerea mulţimii vide la
clasa întâi este tipică pentru un întreg stil care ignoră tot
ceea ce psihologia învăţării a acumulat (în particular,
dezvoltarea stadială a copilului, cu contribuţiile atât de
importante ale lui J. Piaget).
Dar greşelile se prelungesc în clasele superioare, unde
potenţialul educaţional al matematicii este considerabil
redus, prin eludarea aspectelor genetice, istorice, culturale
şi interdisciplinare. În acest fel, izolarea socială a
matematicii devine inevitabilă. Participând, în toamna
538

Rãni deschise

anului 1981, la un simpozion internaţional asupra
interdisciplinarităţii, simpozion organizat de Centrul
European UNESCO pentru Învăţământul Superior, din
Bucureşti, mi-am putut da din nou seama cât de adâncă şi
de amplă este încă neînţelegerea potenţialului
interdisciplinar al matematicii. Această neînţelegere este
uneori chiar arborată cu mândrie, interdisciplinaritatea
fiind redusă la o modă de care au nevoie diletanţii, pentru
a putea trece drept specialişti.
Interdisciplinaritatea – strategii şi implicaţii
Cum oare ne-am mai putea descurca fără matematică
în rezolvarea marilor probleme sociale ale lumii noastre?
Inima ştiinţei o constituie problemele care-i stau în faţă şi
pe care ea trebuie să le rezolve. Probleme ca cele privind
mediul ambiant sau criza energetică sunt esenţial
interdisciplinare şi ele reclamă o nouă viziune, o nouă
organizare a învăţământului, în care orientarea după
discipline să se combine cu orientarea după probleme. Un
atare deziderat trebuie luat în seamă la toate nivelurile
învăţământului.
Noi începem cam devreme împărţirea pe discipline,
ignorând caracterul sincretic pe care cultura l-a avut în cea
mai mare parte a istoriei ei. Nu ar fi oare mai potrivit ca în
primele clase elevii să înveţe o singură materie, după un
539

Solomon Marcus

singur manual, care să le permită cunoştinţa cu natura,
observarea şi compararea obiectelor, formarea intuiţiilor
şi deprinderea limbii? Iar în ceea ce priveşte învăţământul
superior, nu cumva formaţia interdisciplinară –
actualmente posibilă numai în ultimii ani de studii, şi
atunci doar într-o măsură redusă – ar trebui coborâtă spre
primii ani, permiţându-i tânărului bacalaureat să asocieze
de la început geografia cu biologia şi cu matematica sau
fizica cu istoria şi cu informatica? Aceste strategii au dat
bune rezultate în alte părţi şi par mai apropiate de
exigenţele sociale actuale. În particular, educaţia
matematică iniţială s-ar dezvolta în legătură organică cu
observarea naturii şi cu formarea intuiţiilor, iar gândirea
interdisciplinară ar fi considerabil favorizată.
În ceea ce priveşte favorizarea preocupărilor
interdisciplinare, învăţământul a rămas mult în urmă şi la
nivelul său postuniversitar. Domeniile interdisciplinare
sunt încă, în mare parte, un fel de teritoriu clandestin, mai
degrabă tolerat cu superioară îngăduinţă decât încurajat şi
ajutat. Majoritatea domeniilor interdisciplinare nu
figurează încă în nomenclatorul de specialităţi la doctorat.
Un doctorat în informatică a devenit posibil abia din 1981.
Posibilitatea unui doctorat în biomatematică a existat, dar
a fost suprimată, iar o teză recentă în acest domeniu a fost
pusă sub semnul întrebării, fiind apreciată exclusiv din
punctul de vedere al unei teze de matematică pură.
540

Rãni deschise

În ceea ce priveşte tezele de doctorat cu subiecte mai
îndrăzneţe, care asociază matematica şi calculatorul unor
domenii umaniste, situaţia este şi mai critică, fiecare
descurcându-se cum poate. Astfel, pentru că nu există
doctorat în muzicologie matematică, un compozitor a fost
nevoit să-şi prezinte lucrarea sa ca o teză de doctorat în
matematică!
Desigur, devenind tot mai importantă, matematica
este şi o modă. Ea este invocată de snobi şi de diletanţi.
Simbolismul matematic devine uneori o simplă modalitate
de a-i intimida pe profani (dar oare nu este încurajat acest
fenomen chiar de structura unora dintre manualele în
vigoare?). Însă, dincolo de toate aceste neajunsuri, lumea
contemporană, prin problemele pe care trebuie să le
rezolve, ne atrage atenţia că matematica devine inevitabilă
în toate compartimentele sociale cât de cât importante.
Părinţii simt acest lucru şi supraveghează cu atenţie (din
păcate, de multe ori fără pricepere) educaţia matematică a
copiilor. Profesorii sunt neliniştiţi, dar din această nelinişte
va rezulta, fără îndoială, o matematică mai umană.
Relaţia dintre fundamental şi aplicativ, dintre tradiţie
şi modernitate îi preocupă intens pe matematicieni, autori
ai manualelor de matematică. Dar cei care conduc
întreprinderi industriale, centre de calcul, cooperative
agricole de producţie, institute de proiectare, instituţii de
cultură vor face, sperăm, efortul de a înţelege spectrul larg
541

Solomon Marcus

al posibilităţilor oferite de gândirea matematică nu numai
în materie de analiză a datelor (deşi şi aici sunt încă atâtea
posibilităţi nefructificate), ci şi în ceea ce priveşte
modelarea matematică şi analiza calitativă.
Deocamdată, preocupările matematice ale tineretului
sunt determinate, într-o anumită măsură, de exigenţele
manifestate la diferitele concursuri de admitere (treapta
întâi şi treapta a doua în liceu, învăţământ superior ş.a.).
Din păcate, aceste concursuri ignoră aspecte fundamentale
ale matematicii. Problemele cu funcţie cognitivă sunt
înlocuite cu exerciţii lipsite de orizont cultural,
dimensiunea interdisciplinară şi aplicativă este aproape
total neglijată, accentul pe artificii de calcul şi trucuri
devine uneori decisiv. În acest climat, au proliferat tot felul
de culegeri de probleme, inegale ca valoare, care măresc
deruta.
Unele observaţii sunt de făcut şi în ceea ce priveşte
formarea matematicienilor, cercetarea şi promovarea
cercetării în domeniul matematicii. După opinia unui
cunoscut matematician, G.H. Hardy, matematica, mai mult
decât orice ştiinţă sau artă, este o ocupaţie de om tânăr.
Dacă un absolvent foarte bun, care a demonstrat
capacitatea sa de cercetător, nu are posibilitatea să-şi
dezvolte în continuare această capacitate la locul unde a
fost repartizat, există riscul ca mai târziu el să nu mai poată
fi recuperat pentru cercetare. Ca membru al comisiilor de
542

Rãni deschise

admitere la doctorat, de-a lungul anilor, am putut verifica
un adevăr pe care nu avem voie să-l uităm: cu foarte puţine
excepţii, candidaţii, cu cât erau absolvenţi mai vechi, cu
atât se prezentau mai slab, unii dintre ei dovedind a fi
pierdut limbajul matematic ca atare.
Matematica românească, cu tradiţii glorioase şi
realizări recunoscute în întreaga lume, va găsi, fără
îndoială, calea de rezolvare a unor probleme atât de
spinoase ca cele de mai sus. Sprijinul întregii noastre
societăţi va fi decisiv în această privinţă.

543

