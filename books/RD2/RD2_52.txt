Comanda socială supremă
Viaţa studenţească, nr. 34(806), anul XXIV,
22 octombrie 1980, p. 2
Să ne imaginăm o ţară a cărei constituţie garantează
dreptul de a supune orice problemă unor dezbateri
parlamentare nelimitate. Membrii parlamentului ajung
repede la concluzia că, în pofida avantajelor pe care le
prezintă, nelimitarea în timp a dezbaterilor este
dăunătoare, deoarece face imposibilă luarea unei decizii.
Se propune atunci un amendament cerând introducerea
unei durate limită în dezbaterea oricărei probleme. Însă
şi acest amendament cade sub incidenţa regulii care
permite o discuţie nelimitată, motiv pentru care el nu
poate fi adoptat. Iată un sistem care nu găseşte în el însuşi
posibilitatea de a-şi depăşi dificultăţile. Mecanismul său
de funcţionare conţine în sine propria sa ineficacitate.
Acest joc fără sfârşit, discutat de Paul Watzlawick,
Janet Helmick Beavin şi Don D. Jackson (Pragmatics of
Human Communication, Faber and Faber, Londra, 1968,
p. 235) este semnificativ pentru multe situaţii care pot să
apară în viaţa personală sau în cea socială.
Cum se poate sparge cercul vicios din exemplul de
mai sus? Fie recurgându-se la un arbitru nesupus
376

Rãni deschise

restricţiilor sistemului parlamentar (dar există totdeauna
un atare arbitru? este de dorit existenţa sa?), fie printr-un
act de violenţă, care nu se mulţumeşte cu un simplu
amendament, ci care răstoarnă constituţia existentă. Autorii
menţionaţi analizează diferite situaţii din domeniul
psihiatriei, în care ieşirea dintr-o anumită dificultate, de
exemplu, într-o familie supusă unei situaţii conflictuale, se
obţine prin intervenţia unui terapeut care, cu ajutorul unor
metareguli, modifică regulile existente în familia
respectivă. Dar, chiar prin aceasta, sistemul iniţial, cel al
familiei, s-a lărgit, înglobându-l şi pe terapeut. Treptat,
terapeutul se implică tot mai mult în relaţiile sale cu
pacienţii. S-ar părea că în acest fel forţa sa terapeutică se
măreşte. Însă, după cum arată autorii menţionaţi mai sus,
experienţa multor specialişti a condus la constatarea că,
cel puţin în unele cazuri (de exemplu, cel al unei familii
în care se află un schizofrenic), implicarea prea puternică
a mediatorului diminuează capacitatea sa terapeutică.
Acesta este, de obicei, momentul în care terapeutul simte
nevoia să se consulte cu un coleg al său de profesie,
care-l ajută să se detaşeze de sistemul în care s-a implicat
şi să-şi recapete funcţia de supraveghere şi modificare a
regulilor prin metareguli.
Să examinăm acum modul în care actualul sistem
mondial reuşeşte să depăşească, prin metareguli adecvate,
regulile care nu se dovedesc viabile sau operaţionale.
377

Solomon Marcus

Dispunem de numeroase organizaţii internaţionale
(într-un număr mai redus, ele au existat şi înainte de Al
Doilea Război Mondial), dintre care cea mai importantă
este Organizaţia Naţiunilor Unite. Îndeplinesc ele rolul de
mediator, de terapeut al atâtor situaţii bolnave existente azi
în lume? Răspunsul este în acelaşi timp afirmativ şi
negativ. Este afirmativ, pentru că aceste organizaţii
constituie un mijloc esenţial de menţinere şi dezvoltare a
dialogului internaţional şi de promovare a metodei
tratativelor în rezolvarea litigiilor dintre state. Dar el este
negativ, în sensul că acest rol de mediator nu este acela al
unui outsider, al unui arbitru care se află în afara jocului,
deci neimplicat în problemele pe care trebuie să le scoată
din impas. Într-adevăr, în raport cu problemele globale cu
care este astăzi confruntată omenirea, este greu de acceptat
că ar putea exista un outsider capabil să elaboreze
metareguli pe baza cărora sistemul mondial actual ar putea
depăşi propriile sale reguli de funcţionare, pentru a trece
la altele, mai rezonabile. Orice organizaţie internaţională
este, prin forţa împrejurărilor, alcătuită nu din marţieni, ci
din reprezentanţi ai unor grupuri sociale care trăiesc pe
planeta noastră, deci aparţin chiar sistemului care se află
în criză; această implicare în sistemul bolnav este
transferată inevitabil şi reprezentanţilor diferitelor grupuri
sociale. Un mediator-terapeut din afară pur şi simplu nu
există. Din acest motiv, nu putem pune în analogie
378

Rãni deschise

problemele globale generate de boli ale structurii sociale
a societăţii cu problemele personale sau de microgrup
generate de unele boli psihice (pentru acestea din urmă
posibilitatea mediatorului plasat în afara jocului este
esenţială). Lumea contemporană trebuie să se resemneze
cu situaţia ei autoreferenţială, ea trebuie să înveţe să fie
propriul ei terapeut. Ieşirea din actualul sistem
internaţional, din actuala „regulă de joc” nu poate fi
realizată decât printr-o implicare cât mai profundă a
tuturor, în problemele sistemului.
Dar nu cumva organizaţiile internaţionale pot fi un
terapeut din afară în cazul conflictelor locale? Desigur, în
acest caz, ele au o şansă mai bună, după cum s-a şi văzut
într-un număr de cazuri. Poate că acesta este unul dintre
motivele pentru care un al treilea război mondial a fost evitat
până acum, cu preţul a sute de războaie aşa-zise „mai mici”.
Dar toate investigaţiile confirmă faptul că războaiele mici
manifestă o tendinţă tot mai puternică de a se transforma
într-unul mare. Deci, pentru a evita o conflagraţie mondială,
trebuie să punem capăt şi războaielor mai puţin mari,
oricărui fel de recurgere la forţă în rezolvarea problemelor
internaţionale. Este comanda socială supremă a unei lumi
care devine din ce în ce mai mică.

379

