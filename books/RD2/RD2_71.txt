Pentru un dialog generos
între toate compartimentele culturii
Flacăra, nr. 28, 16 iulie 1982, p. 9, 14
Răspuns la ancheta realizată de Tania Radu, Cultura
română îşi regăseşte cărţile fundamentale
Sub semnul unor întrebări orientative: 1) În ce
măsură recuperarea valorilor trecutului cultural
contribuie la redimensionarea prezentului? 2) Care sunt
liniile de forţă ale acestui proces? 3) Ce raport se poate
stabili între politica editorială, preocupările
cercetătorilor specialişti şi „orizontul de aşteptare” al
publicului? 4) Ce pete albe ale istoriei culturii române
au rămas necartografiate şi care sunt şansele lor de
rezolvare? 5) Ce înseamnă judecată şi prejudecată în
acest domeniu? 6) Ce cărţi şi-a regăsit cultura română în
ultimii ani? – sub semnul acestor întrebări deci,
deschidem paginile revistei opiniilor unor reputaţi
oameni de litere, pentru a încerca să conturăm o
perspectivă asupra evenimentelor editoriale care au
marcat viaţa literară a ultimilor ani.
Au mai răspuns: Zoe Dumitrescu Buşulenga, Valeriu
Râpeanu şi Paul Cornea
544

Rãni deschise

Una dintre cele mai puternice tradiţii ale istoriei
noastre culturale se referă la caracterul preponderent
integrativ, sincretic, pe care l-a avut cultura românească
încă de la începuturile ei. Fenomenul este cu deosebire
semnificativ pentru secolele al XVIII-lea şi al XIX-lea.
Personalităţi ca Dimitrie Cantemir, Costache Conachi,
Gh. Asachi, Ion Heliade Rădulescu, Ion Ghica, Simion
Marcovici marchează atât dimensiunea umanistă
(filosofică sau literară) a culturii noastre, cât şi pe aceea
ştiinţifică şi tehnică. Mai târziu, cea mai mare
personalitate a învăţământului românesc, Spiru Haret, se
manifestă puternic atât în domeniul mecanicii cereşti
(printr-o strălucită teză de doctorat la Sorbona), cât şi în
cel al mecanicii sociale (cu o carte care şi-a devansat
timpul cu cel puţin o jumătate de secol). Toate aceste
tradiţii sunt insuficient cunoscute şi dezvoltate.
Congresul al doilea al educaţiei politice şi culturii
socialiste a subliniat, între altele, necesitatea îmbogăţirii
continue a orizontului de cunoaştere şi înţelegere a
fenomenelor din natură şi societate şi a accentuat
importanţa dezvoltării interesului pentru cunoaşterea
istoriei naţionale. La întâlnirea acestor două idei
directoare se află şi formaţia interdisciplinară, de
colaborare a diferitelor compartimente ale culturii,
colaborare care constituie o moştenire istorică a culturii
româneşti, a cărei însemnătate creşte odată cu revoluţia
545

Solomon Marcus

ştiinţifică tehnică pe care o parcurgem. Cum s-a
manifestat în vremea din urmă, sub aspect editorial, acest
dialog? Unele edituri ca Editura Academiei, Editura
Ştiinţifică şi Enciclopedică, Albatros, Junimea, Editura
Politică şi-au câştigat mari merite în stimularea acestui
dialog, ele au adus în atenţia noilor generaţii unele opere
interdisciplinare de mare valoare. Persistă însă unele
carenţe, care se cer înlăturate. În această perspectivă,
discutăm câteva apariţii editoriale semnificative ale
ultimilor ani.
Vom evoca mai întâi două apariţii de estetică
matematică: Estetica lui Pius Servien (1975, după ediţia
franceză din 1953) şi Estetica şi teoria artei, de Matila C.
Ghyka (1981, selecţie de texte din diferite lucrări
publicate, în cea mai mare parte între 1930 şi 1940, în
limba franceză), amândouă la Editura Ştiinţifică şi
Enciclopedică. Am scris de mai multe ori (şi în mod
special în cartea noastră Din gândirea matematică
românească, Editura Ştiinţifică şi Enciclopedică, 1975)
despre importanţa operei acestor autori.
Publicarea în româneşte a lucrărilor menţionate
constituie un eveniment ştiinţific şi editorial. Însă
întârzierea cu care s-a produs editarea acestor clasici ai
esteticii ştiinţifice ar fi trebuit să fie compensată de o grijă
deosebită în realizarea unor ediţii cât mai reprezentative.
Din păcate, din Servien s-a publicat numai o parte foarte
546

Rãni deschise

mică din importanta sa operă, iar ediţia Ghyka din 1981
nu s-a bucurat din partea editurii de atenţia cuvenită. Până
şi expresia numărului de aur, care se află în centrul
investigaţiei, apare în ediţia românească în mod sistematic
denaturată (a se vedea de exemplu paginile 51, 64, 100,
252, 253, 255, 258, 341, 342, 351, 357, 365). Lui Ghyka i
se atribuie faptul de a fi apelat la cibernetică (p. 494) întrun moment în care aceasta nu exista. Ignorând
terminologia matematică românească, traducătorul se
referă la proporţii disjuncte (p. 35), teoria ansamblurilor
(p. 219) şi la serie (p. 267), când, de fapt, este vorba de un
şir. Cum a fost posibil ca la o lucrare cu o structură
matematică atât de pronunţată să nu existe un control
ştiinţific efectuat de un matematician? Ca să nu mai
vorbim de faptul că lipseşte din ediţia din 1981 ceea ce în
primul rând aşteptam de la ea: urmărirea evoluţiei ideilor
lui Ghyka până în zilele noastre.
Un alt fenomen editorial care ne-a atras atenţia în
ultimii ani este amploarea pe care a căpătat-o interesul
pentru opera matematicianului poet Dan Barbilian – Ion
Barbu. Editura Didactică şi Pedagogică şi Editura
Tehnică au publicat o parte importantă a operei
matematice – cea ştiinţifică şi cea didactică – a lui Dan
Barbilian, dar o parte la fel de importantă a rămas încă
nepublicată. În schimb, s-a publicat aproape în întregime
opera literară a lui Ion Barbu. Apariţia în 1970 la Editura
547

Solomon Marcus

Albatros a operei poetice a lui Ion Barbu rămâne un
moment de referinţă. Paginile de proză ale lui Ion Barbu
fuseseră editate în 1968 (la Editura pentru literatură) de
Dinu Pillat. Ulterior, au apărut numeroase exegeze ale
operei poetice. Astfel, pentru a ne referi numai la cele
din 1980 încoace, vom menţiona contribuţiile valoroase
ale lui Şerban Foarţă (Eseu asupra poeziei lui Ion Barbu,
Facla, 1980), Mircea Scarlat (Ion Barbu, poezie şi
deziderat, Albatros, 1981), Marin Mincu (Ion Barbu,
eseu despre textualizarea poetică, Cartea Românească,
1981), Dinu Pillat (Ion Barbu, ediţia a doua, Minerva,
1982), Alexandru Ciorănescu (Ion Barbu, Twayne
Publishers, Boston, 1981). Astfel, am câştigat mult în
profunzime, în înţelegerea poetului Ion Barbu şi în
situarea operei sale în istoria literară, dar am progresat
mai puţin în descifrarea semnificaţiilor operei
matematice a lui Dan Barbilian. O surpriză plăcută a
constituit-o singura carte pe coperta căreia figurează atât
Dan Barbilian, cât şi Ion Barbu: Pagini inedite (ediţie
îngrijită de Gerda Barbilian, V. Protopopescu şi Viorel
Gh. Vodă), Albatros, 1981, cuprinzând soluţii ale unor
probleme de geometrie, alternând cu scrisori de un vădit
interes literar. Totuşi, Dan Barbilian şi Ion Barbu rămân
încă personalităţi distincte, relativ independente. Deşi
cei mai mulţi exegeţi au recunoscut existenţa unei
influenţe a matematicianului asupra poetului, această
548

Rãni deschise

influenţă nu s-a arătat a fi decisivă, reducându-se în cele
mai multe analize la semnalarea unor termeni
matematici utilizaţi metaforic în poeziile lui Ion Barbu.
Cât despre influenţa poetului asupra matematicianului,
aceasta a intrat şi mai puţin în atenţia cercetătorilor. În
această situaţie, nu mai trebuie să ne mire că până şi o
lucrare de o remarcabilă acurateţe, cum este ediţia lui
Romulus Vulpescu a lui I. Barbu, conţine unele
inadvertenţe sub aspectul implicării matematicianului şi
a matematicii (a se vedea de exemplu pp. XIV, 136, 316,
323, 344). O întrebare îşi aşteaptă răspunsul: au fost Dan
Barbilian şi Ion Barbu doi creatori materializaţi
întâmplător în aceeaşi persoană sau exprimă ei două
aspecte ale uneia şi aceleiaşi personalităţi, pe care
urmează abia să o descoperim?
O altă manifestare editorială de prestigiu, care ne-a
atras atenţia în ultimii ani, este seria volumelor de eseuri
ale profesorului Mircea Maliţa. Ne vom opri aici asupra
celor două volume din Idei în mers (vol. I, 1975; vol. II,
1981), publicate la Editura Albatros, continuare a mai
vechilor eseuri din ciclul Aurul cenuşiu. Mircea Maliţa
manifestă o deschidere totală spre dialogul artei cu ştiinţa,
al tehnicii cu omul, al problemelor globale urgente ale
omenirii cu istoria ei milenară, o extraordinară capacitate
de ordonare a ideilor în jurul câtorva nuclee, de proiectare
a faptelor într-un vast context cultural, social şi istoric.
549

Solomon Marcus

Iată, de exemplu, al doilea volum din Idei în mers, unde
omul de azi este analizat în perspectiva a zece probleme
vitale, constând în relaţiile sale cu trebuinţele, hrana,
munca, tehnologia, oraşul, oceanul, ecologia, armele, noua
ordine economică şi cultura. Preocuparea de a dezvolta
motivaţiile diferitelor curente de gândire, atitudini, decizii;
pertinenţa consideraţiilor; formularea lapidară; perspectiva
filosofică şi istorică; alternarea consideraţiilor generale
neutre cu confesiunea autobiografică; aducerea faţă în faţă
a unor teze opuse, alăturare a unor fapte eterogene, în
vederea argumentării unor idei mai mari, mai generoase;
stăpânirea unei informaţii complexe, la zi, dar şi
distanţarea faţă de ea; inserarea fenomenului românesc în
context internaţional; organizarea materialului în jurul
unor idei directoare; caracterul polemic; echilibrul dintre
reflecţia filosofică, observaţia sociologică-politologică,
gândirea generalizatoare-sistematizatoare şi referirea la
evenimente particulare; iată numai o parte din virtuţile
acestor eseuri în care omul de ştiinţă şi scriitorul, omul
politic şi patriotul sunt deopotrivă prezenţi.
Într-o lume în care diferite excese, refuzuri şi
îngustimi de vederi tind să fragmenteze cultura,
diminuându-i astfel rolul ei social, Mircea Maliţa se
numără printre cei, din păcate încă puţini, care promovează
o viziune echilibrată şi totalizatoare, de dialog generos
între toate compartimentele culturii.
550

