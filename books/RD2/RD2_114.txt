La Colocviile revistei Studii şi cercetări de
documentare, vol. XXVIII, 1986, nr. 1-2
Ministerul Educaţiei şi Învăţământului. Oficiul
de informare documentară pentru învăţământ
Întrebări din partea Redacţiei (George Corbu):
Pentru întreaga noastră activitate ştiinţifică,
tehnologică şi de învăţământ, intrarea în cel de-al optulea
cincinal (1986-1990) reprezintă o etapă calitativ
superioară, caracterizată prin sublinierea mai pregnantă a
rolului ştiinţei, al cercetării în asigurarea progresului
multilateral al patriei.
În perioada care urmează, România va parcurge un
stadiu nou de progres şi civilizaţie, urmând să devină, din
punct de vedere economic, ţară mediu dezvoltată. Ştiinţa
şi învăţământul – factori fundamentali ai edificării noii
orânduiri pe pământul românesc – vor trebui să slujească
acestui ţel măreţ, angajându-se cu toate forţele într-o operă
revoluţionară de mari proporţii, de cutezătoare deschideri
şi salt în viitor.
La primul Congres al Ştiinţei şi Învăţământului
(28-29 noiembrie 1985), în cuvântarea programatică,
de amplă şi profundă înrâurire a gândirii şi faptei
noastre comuniste, a secretarului general al partidului,
809

Solomon Marcus

preşedintele republicii, tovarăşul Nicolae Ceauşescu,
precum şi în cuvântarea de încheiere rostită de tovarăşa
academician doctor inginer Elena Ceauşescu, s-au indicat
atât noile obiective strategice, cât şi căile şi direcţiile de
acţiune în vederea împlinirii importantelor sarcini ce
revin oamenilor de ştiinţă şi din învăţământ, din
hotărârile Congresului al XIII-lea al partidului, pentru ca
ştiinţa, devenită forţă de producţie, să contribuie şi mai
hotărât la realizarea prevederilor planului de dezvoltare
economică şi socială a ţării în următorii cinci ani şi, în
perspectivă, până în anul 2000.
S-a afirmat, încă o dată, cerinţa ca ştiinţa şi
învăţământul românesc să se situeze pe cele mai înalte
trepte ale performanţelor cunoaşterii, iar în unele domenii
să ocupe primele locuri, participând activ la noua revoluţie
ştiinţifică şi tehnică.
În acest cadru, pornind de la ideea că pentru a se
obţine performanţe trebuie să existe o informare cât mai
completă, rolul formării şi documentării, ca gen specific
de activitate, sporeşte continuu. Din perspectiva
prezentată, am conceput colocviile iniţiate de revista
noastră. Am apelat, în continuare, la personalităţi
bine-cunoscute ale vieţii cultural ştiinţifice şi didactice
româneşti, pentru a-şi exprima opiniile în această
problemă deosebit de actuală, pornind de la următoarele
întrebări:
810

Rãni deschise

1. Documentele de partid pun accentul pe o puternică
integrare a ştiinţei în viaţa socială. Ce rol conferiţi, în
procesul integrării învăţământului cu cercetarea şi
producţia, informării, documentării şi informaticii?
2. În valoroasa dumneavoastră activitate, ce rol aţi
acordat informării şi documentării ştiinţifice? Cum vedeţi
informarea şi documentarea din punctul de vedere al unui
cadru didactic şi, respectiv, al unui cercetător?
3. Dată fiind experienţa dumneavoastră, ce
recomandări aţi avea de făcut elevilor, studenţilor, cadrelor
didactice şi cercetătorilor, sub raportul metodelor şi
tehnicilor de muncă intelectuală? Cum vedeţi relaţia
informare – creativitate?
4. Care sunt, în opinia dumneavoastră, perspectivele
învăţământului asistat la calculator?
Iată răspunsurile lui Solomon Marcus:
A devenit un truism să se spună că cercetarea şi
documentarea au un rol fundamental în învăţământ şi
cercetare. Dincolo însă de această formulare generală,
mergem la manifestările efective ale acestei noi realităţi
informaţionale? Într-un ritm în unele locuri mai lent, în alte
locuri mai rapid, lumea se îndreaptă spre o societate
informaţională. În unele ţări, încă în urmă cu vreo cinci ani,
mai mult de jumătate din forţa de muncă era ocupată în
diferite faze şi tipuri de prelucrare a informaţiei. În aceste
811

Solomon Marcus

condiţii, ştiinţa şi învăţământul trebuie să facă faţă unui flux
de informaţie de o intensitate fără precedent. Însă ştiinţa şi
învăţământul superior nu se pot mulţumi să fie beneficiare
ale fluxului de informaţie, ele au datoria de a prelucra această
informaţie şi de a produce la rândul lor informaţie. Între cele
două procese, cel al absorbţiei şi cel al producerii de
informaţie, există o legătură organică. Între capacitatea de
absorbţie a informaţiei provenite din alte surse şi capacitatea
de creare a unei informaţii noi, deci de constituire într-o nouă
sursă de informaţie, există o interacţiune puternică.
Cercetarea ştiinţifică este asemenea unei ştafete.
Numai că în sport îi vezi alături de tine pe ceilalţi
alergători, care te stimulează şi îţi atrag atenţia asupra
valorii fiecărei fracţiuni de secundă pierdute sau câştigate.
În ştiinţă, nu sunt numai câţiva, ci o mulţime de alergători
care au un obiectiv asemănător cu al tău; însă aceşti
alergători nu sunt decât rareori direct vizibili, existenţa lor
este percepută indirect şi de multe ori cu întârziere. Acest
fapt poate ascunde caracterul de competiţie pe care îl are
cercetarea ştiinţifică. A avea acces rapid la informaţia cea
mai recentă şi mai valoroasă înseamnă posibilitatea de a
prelua mai repede ştafeta de la cei care au produs această
informaţie. A fi apt să comunici în mod operativ lumii
ştiinţifice rezultatele pe care le-ai obţinut înseamnă
capacitatea de a preda ştafeta mai departe cu rapiditate şi,
deci, cu şanse mai mari de prioritate.
812

Rãni deschise

Concluzia este clară. Trebuie nu numai să primim
revistele, monografiile şi tratatele de specialitate cele mai
proaspete, dar să şi dispunem de un sistem propriu de
publicaţii ştiinţifice care să prezinte cu operativitate
specialiştilor din întreaga lume rezultatele cercetătorilor
români. Accentuez aici importanţa revistelor. Adoptând o
metaforă militară, putem spune că, în bătălia cercetării
ştiinţifice, linia întâi a frontului este vizibilă în revistele de
specialitate; acolo găsim ştiinţa care se face sub ochii
noştri, cu toate ezitările şi tatonările ei, cu întrebările
rămase încă fără răspuns. Monografiile, tratatele şi, mai cu
seamă, manualele se constituie în spatele frontului,
deosebit de important, dar insuficient pentru câştigarea
bătăliei.
De ce fac aceste precizări aici? Deoarece vreau, prin
aceasta, să atrag atenţia asupra faptului că sursele de
informare şi documentare pe care le punem la dispoziţie
studenţilor nu se pot reduce la cursuri şi manuale. Cu cât
vor fi mai mulţi studenţii care vor lua contact cu lumea
revistelor (inclusiv a revistelor internaţionale de referate)
şi cu cât acest lucru se va întâmpla mai devreme, nu abia
în ultimul an de studii, cu atât mai bine îi vom pregăti pe
absolvenţii noştri să fie capabili de un randament creator
în profesie. Lucrările de diplomă şi lucrările de
specializare dau, în mare măsură, seama de acest fenomen,
de reuşitele şi eşecurile lui. Este însă necesar ca studenţii
813

Solomon Marcus

buni şi, mai ales, cei foarte buni să fie atraşi în activitatea
de cercetare, pe baza contactului cu revistele, încă din
primii ani de studii. Aceasta presupune accesul fără
dificultăţi al studenţilor la revistele aflate în bibliotecile de
specialitate. Din păcate, la unele biblioteci există încă
bariere administrativ-birocratice, care îngreunează accesul
studenţilor la reviste şi monografii; măsuri de acest fel
pornesc tocmai din mentalitatea după care studenţilor care
n-au ajuns încă în ultimul an de studii le sunt suficiente
cursurile şi manualele.
În acelaşi timp, este necesar ca încă din clasele liceale
elevii să fie învăţaţi cum se alcătuieşte şi se foloseşte o
bibliografie relativă la o anumită chestiune şi cum se face
referinţă la o sursă bibliografică folosită. Modul de
prezentare a multor comunicări la sesiuni ştiinţifice din
şcoli şi facultăţi arată că există încă multă ignoranţă în
această privinţă. Revistele şcolare abundă în texte în care
cu greu se poate distinge partea inedită de partea preluată,
neindicându-se de obicei cu exactitate sursele folosite.
Uneori, referinţa se face doar pentru o mică parte din ceea
ce s-a preluat.
2. Încă din anii mei de ucenicie în munca ştiinţifică,
am dat o atenţie specială informării bibliografice cât mai
complete. Parcurgeam, încă de atunci, sistematic, revistele
internaţionale de referate (Mathematical Reviews,
Zentralblatt für Mathematik, Referativnîi Jurnal, Bulletin
814

Rãni deschise

Signalétique), conspectând rubricile care mă interesau.
Urmăream, de asemenea, revistele de specialitate. Evident,
aceasta presupunea posibilitatea de a înţelege texte de
specialitate în toate limbile internaţionale. Franceza o
ştiam bine încă din şcoală, iar pe baza unor rudimente de
rusă, engleză şi germană, pe care mi le însuşisem fie în
şcoală, fie din iniţiativă personală, mi-am consolidat
cunoştinţele de limbi străine chiar prin contactul cu textele
de specialitate. Fireşte, a intervenit şi avantajul
vocabularului relativ redus şi standardizat pe care-l
foloseşte matematica şi al structurii gramaticale
simplificate a limbajului matematic. În ceea ce priveşte
limbile italiană şi spaniolă, am constatat că, în ciuda
absenţei unei pregătiri anterioare, pot înţelege textele de
specialitate tipărite în aceste limbi.
Am acumulat peste o sută de caiete de tip repertoriu
în care am înregistrat o imensă bibliografie, nu numai de
lucruri care mă interesau, dar şi despre ceea ce bănuiam
că mă va putea interesa într-un viitor mai depărtat. Aceasta
se întâmpla în urmă cu 30-35 de ani, iar acel viitor mai
depărtat de atunci a devenit trecutul apropiat de azi; pot
confirma că, în mare măsură, nu m-am înşelat. Ori de câte
ori m-am angajat în cercetarea unei probleme, prima etapă
a fost aceea a consultării sistematice a rubricii
corespunzătoare din revistele de referate. Am constatat
astfel că multe aspecte interesante au scăpat unui mare
815

Solomon Marcus

număr de autori din cauza faptului că nu aveau o
reprezentare adecvată a contribuţiilor anterioare în
problema de care se ocupau. Situaţia s-a complicat atunci
când am inclus în preocupările mele unele domenii
interdisciplinare; aici, sistematizarea bibliografiei este mai
puţin pusă la punct. Astfel, pentru lingvistica matematică
a trebuit să mă deprind a urmări publicaţii din domenii
dintre cele mai variate: matematică, lingvistică,
informatică, logică, inginerie, psihologie etc.
Însă bibliografia, odată înregistrată, trebuie organizată
în aşa fel încât să poată fi folosită în diferite feluri. Uneori
am nevoie de tot ceea ce a scris un anumit autor, alteori
îmi trebuie ce anume s-a publicat într-o chestiune mai
specială, cum ar fi coerenţa textuală sau continuitatea
simetrică. Posibilitatea de a ţine sub control şi a putea
prelucra o informaţie aflată în sute de caiete greu poate fi
realizată manual. Calculatoarele personale încep să joace
în această privinţă un rol capital. De asemenea, articolele
de sinteză pe diferite probleme devin de necesitate vitală.
3. Cei care intră acum în cercetare şi au intenţii
serioase în această privinţă trebuie să ţină seama de
amploarea deosebită pe care a căpătat-o difuzarea
informaţiei ştiinţifice, de ritmul extraordinar al acumulării
de rezultate noi. Nu ne mai putem mulţumi cu o informare
de tip tradiţional. Mintea noastră are tot mai mult nevoie
de proteze şi auxiliare care să-i amplifice posibilităţile. Aşa
816

Rãni deschise

se şi explică faptul că apar mereu lucrări privind igiena
muncii intelectuale în noile condiţii create de revoluţia
ştiinţifică şi tehnică. La Editura North Holland
(Amsterdam), V. Stibic a publicat în 1980 (cu reluare în
1983) lucrarea Personal Documentation for Professionals
(214 pagini), iar în 1982 (cu reluare în 1983), lucrarea
Tools of the Mind (300 de pagini), destinate să-i ajute nu
numai pe cercetători, ci şi pe toţi intelectualii să-şi
organizeze mai bine activitatea.
Ce înseamnă a-ţi organiza mai bine o activitate
intelectuală? Înseamnă, în primul rând, să separi partea de
rutină, de repetare a unor operaţii stereotipe, transferând-o
pe cât posibil unor mijloace automate, consacrându-te în
schimb cât mai mult unei activităţi creatoare. Numai că, în
practica vieţii cotidiene, distincţia rutină – creaţie este
înlocuită printr-un continuum de situaţii intermediare.
Astfel, o problemă practică este capacitatea de eliminare a
obiectelor devenite inutile, de simplificare a teoriilor care
nu mai pot fi păstrate la gradul anterior de detaliu, de triere
a cunoştinţelor deja acumulate şi de articulare a lor cu cele
nou primite. Capacitatea umană de memorare şi de
prelucrare a informaţiei este limitată, dar nici nu este încă
folosită cu un randament superior. Există o considerabilă
inerţie în a ne desprinde de reprezentările dobândite şi în a
le reconsidera prin selecţie şi comprimare; şi mai mare este
această inerţie în cazul instituţiilor. Astfel, pentru a ne referi
817

Solomon Marcus

la programele de învăţământ în domeniul matematicii, deşi
s-au operat unele comprimări judicioase (cum ar fi cele
privind trigonometria), numeroase alte comprimări se lasă
încă aşteptate. În schimb, unele idei de combinatorică,
logică, grafuri, jocuri, probabilităţi, teoria informaţiei,
algoritmi, limbaje nu şi-au găsit încă locul, în ciuda faptului
că gradul lor de atractivitate şi orizontul lor cultural sunt
superioare celor pe care le prezintă destul de multe dintre
actualele chestiuni de matematică şcolară. Analiza
matematică ocupă şi ea un loc exagerat în programa
şcolară, fiind bine verificat că ceea ce elevii asimilează de
fapt la această disciplină este derizoriu faţă de ceea ce este
preconizat.
Dacă informarea operativă este atât de importantă,
este ea şi posibilă? Există în bibliotecile noastre informaţia
de care avem nevoie? Răspunsul este oarecum paradoxal:
nu există cât ar fi necesar, dar cea existentă nu este
suficient folosită.
Informaţia existentă este sub necesităţi. În ultimele
decenii s-a observat un proces de diversificare a
publicaţiilor ştiinţifice. De exemplu, în domeniul
matematicii, de la reviste generale de matematică, în care
îşi au loc toate ramurile matematicii moderne, s-a trecut
tot mai mult la reviste specializate: de algebră, de
geometrie diferenţială, de topologie, de analiză reală, de
analiză funcţională etc. Dacă vechile reviste puteau fi, în
818

Rãni deschise

general, obţinute prin schimb cu publicaţiile noastre de
matematică, noile reviste, având un profil specializat, nu
mai acceptă schimbul cu o revistă care numai în mod
incidental publică articole de profilul respectiv; în aceste
condiţii, ele trebuie cumpărate. O situaţie similară o
reprezintă domeniile nou apărute, de natură
interdisciplinară. Astfel, de o mare importanţă sunt
revistele internaţionale de informatică, de inteligenţă
artificială, de recunoaştere a formelor etc. Ele trebuie
cumpărate, deoarece nu-s acceptate la schimb contra unor
reviste care nu au acelaşi profil sau măcar un profil
apropiat.
Pentru procurarea acestor reviste, altfel decât prin
cumpărarea lor cu plata în valută, există o singură cale: să
restructurăm sistemul nostru de publicaţii ştiinţifice în
limbi internaţionale, în aşa fel încât să-l adaptăm la noua
situaţie, mărind astfel capacitatea de schimb a revistelor
noastre. De exemplu, nu există o revistă românească de
informatică matematică în limbi internaţionale, deşi forţele
ştiinţifice existente la noi în această ramură sunt destul de
puternice.
Informaţia existentă nu este suficient folosită.
Semnale în acest sens am primit din partea directorului
Bibliotecii Academiei R.S. România, din partea Institutului
Naţional de Informare şi Documentare (INID) şi din partea
altor organisme. Am putut însă constata şi personal acest
819

Solomon Marcus

lucru. La INID se găseşte acel Science Citation Index, care
permite să se urmărească utilizarea fiecărei lucrări
publicate. Câţi îl consultă? La Biblioteca Academiei R. S.
România sunt anunţate lunar noutăţile care intră în
bibliotecă, printre ele destule demne de interes, dar sălile
bibliotecii nu-s foarte populate. Numeroase alte biblioteci
pe care le vizitez prezintă o situaţie asemănătoare. Am
impresia că numai o mică parte dintre tinerii noştri
cercetători şi-au format obiceiul de a conspecta cu
regularitate noutăţile, în primul rând revistele
internaţionale de referate. Un student care se află la primii
paşi în cercetare primeşte de la îndrumătorul său
bibliografia pe care trebuie s-o consulte. În mod normal,
ar trebui ca după doi, trei ani să devină capabil să se
orienteze singur în literatura de specialitate. De multe ori,
nu se întâmplă aşa. Sunt destui absolvenţi care au terminat
facultatea de 3, 4, 5 sau chiar 10, 15 ani, care vin să-l
întrebe pe fostul lor profesor ce mai este nou în domeniul
lor de interes. Există mulţi absolvenţi, chiar dintre cei
prestigioşi, care nici măcar nu află de existenţa revistelor
internaţionale de referate, iar unii află de existenţa lor, dar
nici măcar nu ştiu cum arată ele.
Voi relata o întâmplare semnificativă în ordinea de
idei de aici. După publicarea cărţii mele Timpul (Editura
Albatros, Bucureşti, 1985, 386 p.), mulţi au fost cei care
şi-au exprimat mirarea faţă de faptul că am putut avea
820

Rãni deschise

acces la o informaţie atât de bogată şi recentă. Am fost
considerat beneficiarul norocos şi privilegiat al unei
informaţii care nu se află la îndemâna celor mulţi. Un
recenzent al cărţii s-a exprimat, textual: „Utilitatea cărţii
lui S. Marcus este evidentă, ea oferind o cantitate foarte
mare şi diversă de informaţie pe care majoritatea cititorilor
noştri n-ar avea de unde să şi-o procure. Elocvent este, de
pildă, faptul că S. Marcus a participat la Paris la două
dezbateri asupra gândirii lui St. Lupasco (în 1982 şi 1983),
bucurându-se şi de privilegiul de a-l avea ca interlocutor
preţ de câteva ore pe St. Lupasco însuşi” (România
literară, anul XVII, nr. 40, 3 octombrie 1985, p. 10). Care
este realitatea? Cea mai mare parte, peste 80% din
conţinutul cărţii, se bazează pe surse de informaţie aflate
în Bibliotecile bucureştene: Biblioteca Academiei R. S.
România, Biblioteca Centrală Universitară, Biblioteca
Institutului de Etnologie şi Dialectologie, Biblioteca
Institutului de Filozofie etc. În particular, tot ceea ce
relatez cu privire la Lupasco se bazează pe cărţile sale bine
cunoscute; întâlnirea personală cu Lupasco a adus doar o
anumită distribuire a accentelor. Traian Podgoreanu,
autorul recenziei, poate fi liniştit; accesul său la
bibliografia în chestiune a fost şi rămâne posibil în
continuare. Dar o atare bibliografie nu ne aşteaptă gata
făcută. Noi suntem cei care trebuie să ne îndreptăm spre
cartotecile bibliotecilor, spre revistele internaţionale de
821

Solomon Marcus

referate, spre actele congreselor internaţionale, pentru a le
examina şi pentru a ne extrage ceea ce ni se pare relevant
în problema care ne interesează. Eu nu am găsit o
bibliografie asupra timpului gata întocmită; am alcătuit-o
singur şi sunt convins că ea poartă amprenta intereselor şi
capacităţii mele de sesizare a problemelor timpului.
Amploarea şi prospeţimea bibliografiei pe care o utilizez
au putut da impresia unor recenzenţi ca Alin Teodorescu
(Viaţa Studenţească, anul XXIX, nr. 43 (1067), 23 oct.
1985, p. 9), insuficient antrenaţi în rigorile cercetării
ştiinţifice şi fără o cultură sistematică în problema în
discuţie, că lucrarea noastră realizează „un inventar up-todate al încercărilor de a duce investigaţia timpului dincolo
de bunul simţ elementar şi de modele statornicite”.
Am putea da şi alte exemple de tipul celui de mai sus.
O imensă informaţie ne aşteaptă în biblioteci; ea trebuie
consultată cu regularitate, cu un anume simţ al ordinii şi
cu o intuiţie selectivă mereu trează. Această informaţie,
spre deosebire de cea transmisă pe canale audiovizuale, nu
vine ea spre noi, ci aşteaptă să ne îndreptăm noi spre ea; şi
tot spre deosebire de o bună parte din informaţia care ne
asaltează prin canalele mass-mediei, informaţia din
bibliotecă există numai într-o formă latentă, potenţială; ea
se actualizează numai prin efortul nostru.
Mai există un aspect important, asupra căruia trebuie
să atragem atenţia. O mare parte din producţia ştiinţifică,
822

Rãni deschise

de fapt, partea ei cea mai vie şi mai proaspătă, circulă
printre specialişti sub formă orală, sub formă de preprinturi
sau scrisori, de publicaţii în tiraje cvasiconfidenţiale,
multiplicate pe alte căi decât tiparul. Această situaţie este
determinată de necesitatea unei comunicări mai rapide
între specialişti. Tiparul se dovedeşte tot mai lent şi mai
greoi, incapabil să ţină pasul cu ritmul vieţii ştiinţifice
moderne. La multe reviste tipărite, un articol aşteaptă
câţiva ani pentru a fi publicat (în această situaţie se află şi
principala revistă românească de matematică, Revue
Roumaine de Mathématiques Pures et Apliquées). Unele
reviste, ca International Journal of Computational
Linguistics, au fost miniaturizate prin filmare, dar
deocamdată procesul acesta este şi el lent. Aceste reviste
se păstrează mai uşor, ocupând un loc derizoriu, dar se şi
citesc mai greu, cu ajutorul unui aparat special.
În condiţiile nou create, în care comunicarea
ştiinţifică dă naştere unui întreg folclor, cum este numită
circulaţia orală a rezultatelor, în condiţiile în care
comunicarea se face în mare măsură prin publicaţii cum
sunt cele evocate mai sus, care nu au intrat încă într-un
sistem clar de schimburi şi nici nu sunt recenzate în
revistele internaţionale de referate, devin tot mai
importante, esenţiale, participarea cercetătorilor la
reuniunile ştiinţifice internaţionale şi contactele personale,
prin întâlniri şi corespondenţă directă, între cercetători.
823

Solomon Marcus

Vom da un exemplu mai recent, dar multe ale situaţii
similare ar putea fi invocate. În mai 1984 am efectuat o
călătorie de două săptămâni la Universitatea din
California, Santa Barbara, unde a avut loc o întâlnire
internaţională de Analiză matematică. Ideile, rezultatele şi
opiniile culese acolo, preprinturile, extrasele, notele după
conferinţe şi seminarii, cu care am umplut vreo două
caiete, nu numai că m-au impulsionat în propria mea
activitate de cercetare, dar mi-au dat posibilitatea să-mi
ameliorez în câteva puncte esenţiale cursul meu de Analiză
matematică de la Universitatea din Bucureşti şi să propun
unor studenţi şi cercetători teme noi de studiu, care au
trezit un mare interes şi despre a căror existenţă nu aveam
anterior decât informaţii foarte fragmentate. Fiecare
participare la o manifestare ştiinţifică internaţională are ca
rezultat aducerea în ţară a unei considerabile informaţii
calde, de natură să-i stimuleze în special pe tinerii
cercetători.
De o manieră generală, pot afirma că o mare parte din
literatura de specialitate pe care se bazează lucrările
efectuate de studenţii, doctoranzii şi alţi tineri pe care-i
îndrum face parte din biblioteca mea personală, realizată
datorită legăturilor personale cu oamenii de ştiinţă din
diferite ţări. Este deci clar cât de important devine să
înlesnim şi să amplificăm contactele ştiinţifice
internaţionale.
824

Rãni deschise

Relaţia dintre informaţie şi creativitate a trecut în
secolul nostru prin transformări profunde. Pe de o parte,
creativitatea (cel puţin cea ştiinţifică) nu se mai poate
actualiza în realizări majore în afara unei informaţii bogate
şi la zi. Nimeni nu se mai aşteaptă azi (în afara diletanţilor)
ca problema lui Fermat să fie rezolvată prin trucuri de
matematică elementară, iar rezultatele parţiale obţinute
recent în rezolvarea acestei probleme angajează
instrumente puternice de matematică superioară. Pe de altă
parte, nici informaţia nu mai poate fi acumulată în marea
ei varietate şi bogăţie dacă nu există o viziune creatoare
asupra ei. Pe marginea acestui punct este necesar să
insistăm. În ultimele decenii s-a constatat o deplasare
puternică a disciplinelor descriptive spre structuri
explicative şi organizatoare. O serie de ştiinţe ale naturii,
tradiţional dominate de o imensă informaţie descriptivă,
se orientează spre viziuni sistematice fără de care
informaţia respectivă nu poate fi articulată, organizată şi
ierarhizată; mai mult, nu poate fi nici măcar reţinută,
nefiind înţeleasă.
Însă creativitatea nu trebuie considerată, cum se
întâmplă de obicei, numai în ipostazele ei superioare,
concretizate în creaţii care se bucură de o recunoaştere
publică. Formele creativităţii se repartizează într-o ierarhie
foarte bogată şi nuanţată. Poate chiar termenul de
creativitate nu este fericit ales. Creativitatea include orice
825

Solomon Marcus

iniţiativă personală, orice atitudine lipsită de prejudecăţi,
decurgând dintr-un act de gândire personală. Aceste forme
elementare ale creativităţii, în care are loc dezlipirea,
desprinderea de procedeele standardizate, de actele de
rutină, de clişeele de gândire şi de exprimare, trebuie
stimulate şi cultivate. În aceste forme incipiente,
creativitatea poate fi educată şi dezvoltată la aproape orice
persoană şi la orice stadiu de informare şi cultură.
Realizarea acestui deziderat este o comandă socială vitală
pentru omenire. Un rol fundamental revine aici şcolii. O
imensă literatură îi este dedicată de către pedagogi şi
psihologi, în contrast cu puţinul care se realizează, de fapt.
Desigur, unele statistici ne pot contrazice. Numărul mare
de cercuri ştiinţifice din şcoli şi facultăţi, de sesiuni de
comunicări ştiinţifice, de reviste şcolare ar putea să dea o
impresie opusă. Adevărul este că activităţile menţionate
întreţin o regretabilă confuzie între informaţie şi creaţie,
între preluat şi inedit. Sub denumirea de cerc ştiinţific se
ascund de foarte multe ori simple activităţi de informare
sub formă de referate. Ele au la bază diferite articole şi
cărţi de specialitate care, foarte frecvent, nici măcar nu
sunt menţionate. Într-o revistă a unui prestigios liceu
bucureştean, am găsit două articole care constau în pasaje
preluate aproape întocmai din lucrarea mea Poetica
matematică, fără însă a se menţiona acest lucru. Am asistat
la numeroase comunicări ştiinţifice ale elevilor, unde abia
826

Rãni deschise

prin întrebările mele ulterioare am putut afla care a fost
sursa de informaţie folosită. Este foarte important ca tinerii
să fie educaţi în spiritul unor distincţii riguroase între ideile
şi rezultatele preluate de la alţi autori şi propriile lor idei
şi rezultate. Aşa cum copilul distinge între o jucărie
care-i aparţine şi alta care e a prietenului său, el trebuie să
înveţe treptat să distingă între un lucru făcut cu capul lui
şi altul, gândit de altcineva. Altfel, desigur, neintenţionat,
creăm premise favorabile lenei de gândire şi furtului
intelectual. Tinerii trebuie să fie deprinşi ca, atunci când
se ocupă de o problemă, să se informeze mai întâi care este
stadiul cunoştinţelor în problema respectivă; să nu
confunde prezentarea acestor cunoştinţe cu eventuala
contribuţie inedită pe care ei ar putea-o adăuga. În
redactarea unei comunicări, a unui articol, trebuie să
înceapă prin a menţiona care sunt rezultatele cunoscute,
mai importante, de la care au plecat şi sursele bibliografice
pe care le-au consultat.
Am impresia că nu se înţelege destul de bine rostul
activităţilor suplimentare, de cerc ştiinţific şi de organizare
a unor sesiuni de comunicări. Rostul lor nu este atât
informativ, cât formativ. Elevii şi studenţii primesc, în
cadrul activităţilor uzuale, o imensă informaţie şi nu este
cazul s-o amplificăm şi mai mult prin activităţi
suplimentare. Însă finalitatea unui cerc ştiinţific de
matematică, de exemplu, nu este de a le mai da
827

Solomon Marcus

participanţilor încă vreo 30 de teoreme, pe lângă cele vreo
50 pe care le au în manual; raţiunea acestor cercuri este de
a-i implica pe participanţi într-o activitate cu un mai mare
coeficient de iniţiativă personală decât îl au activităţile
şcolare şi studenţeşti curente.
Aici, unii aduc următoarea obiecţie. Un elev de liceu
sau un student în primii ani de facultate (şi chiar în ani mai
mari) nu are suficientă pregătire pentru a face cercetare
ştiinţifică autentică; el trebuie deocamdată să se informeze,
să se cultive, pentru ca mai târziu să poată face cercetare.
Au loc astfel două exagerări de sens opus. Prima exagerare
constă în folosirea abuzivă a denumirilor de comunicare
ştiinţifică, cercetare ştiinţifică pentru activităţi de fapt mult
mai modeste (într-un număr din Viaţa Studenţească din
noiembrie 1985, alcătuit de studenţii din Cluj-Napoca,
cineva afirmă, despre studenţii unei anumite facultăţi, că
sunt toţi implicaţi în cercetare ştiinţifică); a doua exagerare
constă în teoretizarea imposibilităţii cercetării ştiinţifice
înainte de ultimii ani de facultate sau chiar de absolvire.
Desigur, o sintagmă ca cercetare ştiinţifică este pretenţioasă
şi n-ar trebui să fie folosită cu atâta uşurinţă, dând naştere
unei adevărate inflaţii. Aceasta nu înseamnă însă că
cercurile ştiinţifice din şcoli şi facultăţi nu sunt importante.
La orice nivel de cultură ştiinţifică pot fi stimulate activităţi
de căutare, de iniţiativă personală care, independent de
valoarea şi noutatea rezultatelor obţinute, au un
828

Rãni deschise

considerabil rol formativ. Experienţa mea cu elevii din
clasele superioare şi cu studenţii din primii ani de facultate
mi-a confirmat că, de multe ori, dacă sunt îndrumaţi cu
grijă, aceştia pot obţine rezultate originale, publicabile în
revistele de specialitate. La sesiunea ştiinţifică din 1985 a
Facultăţii de Matematică a Universităţii din Bucureşti, doi
elevi ai clasei a XII-a din Bucureşti, Octav Cornea şi Liviu
Daia, au prezentat astfel de comunicări unanim apreciate.
Eminenţi tineri matematicieni, ca Alexandru Zaharescu,
Alexandru Nica, Victor Nistor, Sorin Popescu şi Cristian
Sima, au prezentat, când erau încă studenţi în anul întâi,
comunicări care au fost imediat publicate în Revue
Roumaine de Mathématiques Pures et Apliquées. Faptul că,
uneori (de fapt, de cele mai multe ori), nu se obţin rezultate
publicabile nu înseamnă că activitatea respectivă a fost
inutilă. Este însă important ca tinerii să se antreneze pe
probleme vii, care se află efectiv în direcţia de interes a
ştiinţei actuale, nu pe probleme artificiale, bazate pe trucuri,
cum sunt încă, din păcate, multe dintre chestiunile propuse
la olimpiade şi la concursuri ştiinţifice studenţeşti.
Dar poate că mai important decât cele de mai sus este
faptul că, în realitatea zilnică a vieţii şcolare, elevii sunt
prea des invitaţi să reproducă şi să aplice formulări clişeu
şi procedee de rutină decât să reflecteze asupra unor situaţii
inedite. Cel puţin la orele de matematică, am putut verifica
de nenumărate ori această situaţie.
829

Solomon Marcus

4. Insinuându-se tot mai mult în procesul de
învăţământ, calculatorul rămâne totuşi o simplă unealtă;
însă o unealtă care, stimulând înţelegerea naturii
algoritmice a unei mari părţi din comportamentul uman,
poate crea premise de competiţie victorioasă cu propria
noastră rutină.
Rămâne să vedem ce probleme imprevizibile vor
apărea în procesul implicării efective a calculatorului în
învăţământ.

830

