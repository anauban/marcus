Formarea de diplomaţi universitari
cu profil mixt: matematico-umanistic
Gazeta Învăţământului, 19 februarie 1971
Problema pe care Ministerul Învăţământului a
ridicat-o recent, cu privire la necesitatea formării de
specialişti cu profil mixt, vine în întâmpinarea unor
cerinţe dintre cele mai imperioase ale momentului actual.
Învăţământ cu profil interdisciplinar există de vreo 10 ani
în numeroase ţări, în forme dintre cele mai variate.
Experienţa acumulată ne permite să adoptăm acum soluţii
mai judicioase, să evităm procedee care s-au dovedit
inadecvate, dar să fim conştienţi de faptul că şi măsurile
pe care le vom lua acum au un caracter de experiment,
că ele sunt susceptibile de a fi reconsiderate chiar într-un
viitor apropiat. La noi în ţară au avut loc, încă în urmă
cu vreo 8-9 ani, discuţii între lingvişti şi matematicieni,
care s-au cristalizat într-un proiect de program al unei
secţii de lingvistică matematică a Facultăţii de Limbă şi
Literatură Română, cu şcolarizare din anul I; din păcate,
acest proiect a rămas doar pe hârtie.
Acum situaţia este, din toate punctele de vedere,
favorabilă unei rezolvări operative. Societatea de Ştiinţe
Matematice a organizat o consfătuire pe tema aici în
85

Solomon Marcus

discuţie şi s-a putut vedea că matematicienii, lingviştii,
istoricii şi literaţii consideră deosebit de importantă
punerea în practică a măsurilor preconizate de către
Ministerul Învăţământului. Aş dori să fac, în această
privinţă, observaţii şi propuneri.
Toată lumea va fi de acord că o problemă atât de
delicată ca aceea a învăţământului interdisciplinar trebuie
abordată în etape. S-a crezut la un moment dat că această
problemă poate fi rezolvată printr-un număr de cursuri cu
caracter postuniversitar sau prin introducerea unui curs sau
a două cursuri speciale în ultimii ani de facultate. Şi la
Facultatea de Matematică-Mecanică, s-au organizat astfel
de cursuri, cu unele rezultate sporadice, dar acum este clar
că ele nu constituie nici pe departe învăţământul
interdisciplinar de care avem nevoie. Un adevărat
învăţământ interdisciplinar trebuie să aibă în vedere: 1. o
reconsiderare a programelor din liceele de cultură generală
şi, în special, a profilului secţiilor; 2. organizarea unui
învăţământ universitar interdisciplinar; 3. organizarea unor
cursuri şi seminarii postuniversitare în vederea formării de
cercetători în domeniile interdisciplinare.
În ceea ce priveşte punctul 1, trebuie să observăm că
actualele secţii ale ultimelor clase din liceele de cultură
generală sunt încă organizate după principiul importanţei
scăzute a matematicii pentru domeniile umanistice.
Manualele de matematică pentru secţia umanistică nu au
86

Rãni deschise

o concepţie proprie (cum nu au, de altfel, nici cele pentru
secţia reală). Rupi dintr-un manual universitar un număr
suficient de mare de foi şi ceea ce rămâne e tocmai bun ca
manual pentru secţia umanistică sau pentru clasele cu
profil clasic. Diferenţele ar fi deci de ordin pur cantitativ.
Cât de departe este această concepţie de spiritul vremii
noastre! Cât de mult se opune ea formării unor absolvenţi
de liceu care să fie pregătiţi pentru un învăţământ
universitar interdisciplinar şi să se simtă atraşi de o carieră
interdisciplinară! Se impune grabnic reconsiderarea
programelor şi manualelor într-un spirit contemporan,
consecvent cu generalizarea utilizării metodelor
matematice în toate domeniile de cunoaştere (fie ele
ştiinţifice sau artistice). Aceasta înseamnă că într-o vreme
în care un lingvist, un istoric, un economist, un muzicolog
şi un filolog au nevoie de aproape tot atâtea cunoştinţe de
matematici ca un inginer, secţia umanistică şi clasele cu
profil clasic se pot distinge de secţia reală nu prin
cantitatea de matematică pe care ele o conţin, ci prin tipul
de matematică de care au nevoie. S-ar putea discuta,
eventual, punctul de vedere conform căruia secţia reală
trebuie să pună accentul pe matematica continuă (în primul
rând analiza matematică), în timp ce alte secţii au nevoie
în special de matematică discretă (logică, algebră, grafuri,
probabilităţi discrete etc.). Nu discutăm aici legitimitatea
acestui punct de vedere, dar nu este exclusă nici
87

Solomon Marcus

posibilitatea ca însăşi menţinerea secţiilor reală şi
umanistică să se dovedească inutilă, problema esenţială
fiind aceea a schimbării, în proporţie însemnată, a
conţinutului actualei programe de liceu.
În ceea ce priveşte punctul 2, vom obseva că, în liniile
ei mari, cultura matematică de care are nevoie un lingvist
sau un filolog este în acelaşi timp cultura matematică de
care are nevoie un istoric, un muzicolog, un semiotician,
un antropolog, un arheolog sau un specialist în teoria
literaturii. Această cultură matematică include elemente de
teoria mulţimilor, numere cardinale, logică matematică,
structuri algebrice, topologie, teoria jocurilor, probabilităţi
şi teoria informaţiei, grafuri, elemente de maşini de calcul,
limbaje de programare, sisteme formale şi limbaje formale,
maşini matematice. Din acest motiv, considerăm că
propunerile făcute de acad. prof. Gr. C. Moisil1, în ceea ce
priveşte programa de matematică a primilor trei ani de
facultate pentru formarea licenţiaţilor în specialitatea
matematică-istorie, sunt valabile, în cea mai mare parte, şi
pentru specialitatea matematică-lingvistică. Evident, vor
fi unele deosebiri în ceea ce priveşte cursurile la alegere
din anii superiori; dar această problemă se va pune abia
1

Gr.C. Moisil: Formarea de diplomaţi universitari cu profil mixt:
matematică istorie, în Gazeta învăţământului, nr. 1074 din 5
februarie 1971.
88

Rãni deschise

peste vreo doi ani şi nu e necesar s-o discutăm acum.
Diplomaţii în matematică-lingvistică vor putea fi utilizaţi
şi ca profesori de liceu, fie de matematică, fie de limba şi
literatura română, respectiv o limbă străină. Acest fapt va
fi de natură să influenţeze în mod binefăcător învăţământul
din liceele de cultură generală, elevii vor fi obişnuiţi de la
o vârstă fragedă cu ideea că studiul matematicii şi studiul
limbilor şi al literaturii nu numai că nu se opun, dar au
chiar nevoie unul de altul.
Actualele programe de învăţământ menţin alături
discipline foarte depărtate ca obiect şi ca mod de abordare
(cum ar fi istoria literaturii şi fonologia în cadrul
facultăţilor de filologie) şi menţin la distanţă discipline
foarte apropiate, fie ca obiect, fie ca mod de abordare (cum
ar fi fonologia – predată doar studenţilor filologi – şi logica
matematică şi teoria probabilităţilor şi a informaţiei,
predate doar studenţilor matematicieni). Prin noile măsuri
preconizate de Ministerul Învăţământului, multe discipline
vor fi aduse în contextul lor natural, micşorându-se astfel
decalajul încă mare care există între caracterul esenţial
interdisciplinar al culturii actuale şi frontierele rigide ce
separă încă disciplinele tradiţionale ale învăţământului (fie
el liceal sau universitar).
În ceea ce priveşte punctul 3, Laboratorul de
semiotică de pe lângă Universitatea din Bucureşti
desfăşoară, începând cu luna octombrie a anului 1970, o
89

Solomon Marcus

activitate ce se înscrie ca un preludiu de bun augur al
etapei postuniversitare de care va avea nevoie
formarea specialiştilor interdisciplinari de tipul
matematică-umanistică.

90

