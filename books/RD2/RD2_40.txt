Ştiinţa şi creaţia artistică
Contemporanul, nr. 39 (1716), 28 septembrie 1979, p. 2
Ştiinţa s-a insinuat în domeniul artei atât în ceea ce
priveşte comentariul, analiza ei, cât şi sub aspectul
procesului de creaţie ca atare. Multă vreme, ca urmare a
caracterului preponderent sincretic al culturii, problema
raporturilor dintre ştiinţă şi artă, dintre creaţia ştiinţifică şi
cea artistică nu a fost explicit formulată şi nu a constituit
un obiect de controversă. Pe măsură însă ce cunoaşterea
s-a specializat şi s-a diferenţiat, problema statutului
gnoseologic şi semiotic al ştiinţei şi al artei a devenit o
preocupare din ce în ce mai insistentă. Tendinţa generală
a fost aceea de a aşeza arta şi ştiinţa sub semnul unor
opoziţii, cât mai nete, mai radicale.
Am avut curiozitatea de a inventaria o parte dintre
aceste opoziţii (peste cincizeci) în volumul Din gândirea
matematică românească (Editura Ştiinţifică şi Enciclopedică,
1975). Ideea că istoria, teoria, critica artei trebuie să se
prevaleze de cuceririle cele mai moderne ale ştiinţei a
câştigat teren. Mulţi autori sunt de acord că metalimbajul
necesar înţelegerii operei de artă trebuie să-şi ia distanţă faţă
de limbajul în care această operă este exprimată. Mai greu
se acceptă faptul că însuşi actul de creaţie artistică pretinde,
284

Rãni deschise

din ce în ce mai mult, o înţelegere lucidă, analitică, explicită
a mijloacelor de creaţie, a posibilităţilor şi servituţilor
materiei pe care artistul urmează s-o transforme în limbaj.
E interesant de menţionat, în acest sens, o distincţie
operată de Dimitrie Cuclin (într-un dialog publicat în urmă
cu câţiva ani în Luceafărul). Prestigiosul compozitor nu
punea în discuţie structura matematică a muzicii (această
structură este prea evidentă pentru a putea fi contestată), ci
relevanţa ei în procesul de creaţie muzicală. Cuclin era de
părere că un compozitor preocupat de însuşirile matematice
ale materiei sonore îşi anulează, chiar prin acest fapt, starea
de candoare, de emoţie pe care se întemeiază opera de artă.
Această concepţie a căpătat o largă răspândire, ca urmare a
acestei forme extreme pe care intervenţia matematicii a
căpătat-o prin folosirea calculatorului electronic. În culturile
ultraspecializate din ultima sută de ani ca şi în sistemele
educaţionale din ce în ce mai precare, promovate de structuri
sociale incapabile să alinieze învăţământul cu nevoile
oamenilor, cultura umanistă cu cea ştiinţifică, calculul a
căpătat conotaţii dintre cele mai negative, cel puţin din
punctul de vedere al creaţiei artistice. Calculatorul electronic
a devenit astfel pentru unii, în mod cu totul nejustificat, un
adversar al artei autentice, încărcate de valori umaniste.
Din păcate, şi în cultura noastră s-a manifestat
influenţa acestei orientări – dacă nu dăunătoare, cel puţin
sterile – având ca efect (intenţionat sau nu) reducerea şi
285

Solomon Marcus

uneori chiar anularea colaborării – atât de necesare! – a
artei cu ştiinţa. Cultura românească a dat culturii
universale pe doi dintre cei trei mari pionieri ai esteticii
matematice: G. Birkhoff, Pius Servien (Coculescu) şi
Matila Ghyka. O sinteză ca aceea operată de René
Huyghe în Formes et forces, de l’atome à Rembrandt,
apărută în 1971 la Flammarion, demonstrează cu
deosebită putere faptul că artele vizuale se dezvoltă în
măsura în care îşi asumă rezultatele profunde pe care
ştiinţa le-a obţinut în studiul evoluţiei formelor. Îmi
stăruie în atenţie exemplul lui Maurits Cornelis Escher.
Evoluţia artei sale este aceea a unui artist care ajunge la
matematică hrănindu-se din propria sa artă iar bucuriile
acestei arte devin organic solidare cu structurile
matematice pe care ea le pune în mişcare. Referindu-se
la ciclul său de lucrări Limite circulare, Escher analizează
progresul realizat de la una la alta, explicându-l tocmai
prin modul inegal în care a realizat matematic acel
echilibru, pe care artistul îl caută cu fervoare, între
simplitate şi complexitate. Cercetări recente au pus în
evidenţă cât calcul se află ascuns în sculpturile lui
Brâncuşi. Mişcarea artei moderne spre o dezvoltare cât
mai mare a funcţiei simbolice a dus inevitabil la o
cuantificare care măreşte considerabil rolul structurilor
geometrice. Mărturisirile unui artist de excepţie ca Adina
Caloenescu, a cărei expoziţie a reţinut, nu demult, atenţia
286

Rãni deschise

tuturor comentatorilor, converg spre aceeaşi utilizare
deliberată a structurilor matematice.
Un proces asemănător are loc în muzică. Compozitori
de o deosebită valoare ca Mihai Brediceanu, Aurel Stroe,
Anatol Vieru, Ştefan Niculescu, Nicolae Brânduş, Lucian
Meţianu, Octav Nemescu ne-au dat posibilitatea să
pătrundem în laboratorul lor de creaţie şi să constatăm că
fiecare dintre ei a dat o deosebită atenţie aspectelor
matematice şi/sau semiotice ale materiei sonore; s-a
prevalat de ele în actul creaţiei şi tocmai prin aceasta
muzica lor a câştigat în originalitate şi valoare.
Într-o cultură totală, ca aceea pe care o preconizăm şi
pentru care milităm, niciun fel de graniţe preconcepute nu
pot stăvili elanul creator al talentelor autentice, iar talentele
nu pot rămâne la o dezvoltare spontană, ele au nevoie de
toate cunoştinţele pe care le reclamă domeniul lor de
manifestare. Într-un moment în care generalizarea
învăţământului de zece ani tinde să creeze, pentru întregul
nostru tineret, un orizont de cultură care va include
elementele fundamentale ale unei formaţii deopotrivă
ştiinţifice şi artistice, ideologice şi morale, este legitimă
dorinţa de a promova o viziune culturală cât mai
cuprinzătoare, de colaborare a tuturor compartimentelor
culturii.

287

