Bricolajul ştiinţificului şi al poeticului
Viaţa Studenţească, anul XXXII, nr 11 (1191),
miercuri, 16 martie 1988, p. 2
Este posibilă comunicarea între domenii foarte
depărtate, prin intermediul unor concepte umane? De
multă vreme s-a dat acestei întrebări un răspuns afirmativ,
însă riscurile unor transferuri de idei, metode, rezultate
între domenii eterogene nu trebuie subestimate. Teorema
de incompletitudine a lui Gödel a fost de mai multe ori
victima unor transferuri grăbite. La fel s-a întâmplat cu
teoria relativităţii şi cu principiul incertitudinii al lui
Heisenberg. Probabil că, în cazuri de acest fel, îşi face
efectul faptul că terminologia utilizată se prevalează de
cuvinte ale limbii comune. Profanii preiau sensul comun,
ignorând accepţiunea specială pe care aceste cuvinte o
capătă în ştiinţă. Termeni ca relativitate sau incertitudine
oferă prin excelenţă capcane de acest fel.
Invitat să comenteze împrejurarea de mai sus, René
Berger („L’ancien et le nouveau: la révolution quantique
et ses conséquences”, dialog cu Sven Ortoli, în La science
face aux confins de la connaissance, Editions du Félin,
Paris, 1987, pp. 135-148) observă că, în fizică, relaţia de
incertitudine are un sens precis, dar pentru mulţi profani
1080

Rãni deschise

ea nu este decât un mod de a spune că „nu poţi fi sigur de
nimic”. Însă, Berger se întreabă dacă nu cumva astfel de
ambiguităţi sunt necesare pentru a face posibilă
comunicarea. Există şocuri culturale prea violente pentru
a fi acceptate. Dar şocul ne stimulează să reflectăm, să
revenim asupra unui termen care ne-a reţinut atenţia.
Oamenii de ştiinţă se prevalează şi ei de metafore; ce sunt,
oare, termeni ca energie sau forţă? Singurul lor privilegiu
este faptul de a identifica aceste metafore de o manieră
unică. Însă şi profanii identifică termeni de acest fel în mod
oarecum uniform, ghidându-se după accepţiunea lor în
limbajul comun. Chestiune complicată!
Ortoli îl provoacă apoi pe Berger cu o altă veche
controversă: de ce vorbim de progres în ştiinţă, dar nu şi
în artă? De ce putem afirma un progres de la Newton şi
Einstein, dar nu şi de la Brueghel la Picasso? În ultimă
instanţă, este omul de ştiinţă un creator, ca şi artistul?
Berger consideră că, aici, cuvântul progres este folosit
în mod nepotrivit. În ştiinţă, progresul constă în
confirmarea experimentală a unei teorii, în defavoarea altei
teorii, dar aceasta din urmă îşi păstrează valabilitatea
într-un anumit cadru (a se vedea situaţia geometriei
euclidiene sau a mecanicii lui Newton). Se mai poate vorbi
atunci de progres? Ortoli invocă faptul că fizicienii vorbesc
de un progres în trecerea de la considerarea separată a
forţei gravitaţionale şi a celei electromagnetice la
1081

Solomon Marcus

unificarea lor, în cadrul teoriei relativităţii, şi apoi în
trecerea de la această unificare la unificarea recentă a
forţelor electromagnetice cu cele salbe. Berger recunoaşte
aceasta, dar observă că în exemple de acest fel este vorba
de progres în cunoaşterea unui proces determinat. Cu totul
alta era semnificaţia în secolul al XIX-lea, când se credea
că ansamblul realului este adecvat unei abordări ştiinţifice.
Acum ştim că ştiinţa nu are acces decât la o parte din
realitate. Berger are în vedere aici faptul că fizica
particulelor elementare nu se poate prevala de metoda
experimentală, nedispunând, pentru vizualizarea unor
fenomene, decât de anumite sisteme de ecuaţii. Dar, fără
experiment, mai este fizica o ştiinţă? se întreabă unii, iar
răspunsul variază. Să menţionăm că recent a fost pus în
chestiune şi statutul de ştiinţă al geometriei (de către
Saunders Mac Lane), dar nu în sensul că geometria n-ar fi
ştiinţă, ci în acela că ar fi mai mult decât atât.
Aici intervine o nouă provocare a lui Ortoli. Mulţi
fizicieni (şi matematicieni, cu atât mai mult) afirmă că ceea
ce-i ghidează, în elaborarea unei teorii, este factorul estetic,
de exemplu, simetria şi simplitatea. Ortoli crede, însă, că
factorul estetic în ştiinţă nu poate fi identificat cu cel din
artă; cel dintâi este accesibil numai unui număr redus de
specialişti, pe când cel de al doilea vizează un public mai
larg. Însă deosebirea pe care Ortoli o are în vedere nu
rezistă la o analiză mai atentă. Potenţialul estetic al unei
1082

Rãni deschise

teorii ştiinţifice se poate actualiza la un număr mai mic sau
mai mare de subiecţi, în funcţie de condiţiile de acces la
teoria respectivă, condiţii care la rândul lor depind de o
seamă de factori sociali, cum ar fi organizarea
învăţământului, condiţiile de viaţă etc. Tot aşa, potenţialul
estetic al unei opere de artă se poate actualiza mai mult sau
mai puţin, în funcţie de complexitatea operei, nivelul
educaţiei estetice a publicului etc. Nu se poate afirma că
operele ştiinţifice sunt mai puţin accesibile decât cele
artistice. Se pot da exemple dintre cele mai variate.
Geometria euclidiană, prin difuzarea largă în rândul
populaţiei şcolare, şi-ar putea dezvălui atributele estetice
unui număr foarte mare de oameni, în timp ce publicul care
are acces la opera unui poet mai dificil, cum este Ion
Barbu, nu este prea numeros.
René Berger, şi el în dezacord parţial cu Ortoli asupra
acestui punct, are o viziune diferită. Nu trebuie confundate
sensibilitatea estetică, abordarea estetică şi conţinutul
esteticii. Deşi nu sunt izomorfe, o teorie ştiinţifică şi o
operă de artă se asociază, în realitatea indivizilor umani,
cu una şi aceeaşi dimensiune, aceea de a simţi că avem a
face cu ceva care este nu numai adevărat, ci şi frumos, şi
la care aderăm. Deci modul de abordare, calea de acces
sunt aceleaşi pentru artist şi pentru savant, chiar dacă
obiectul lor de interes este foarte diferit. Artiştii manifestă
o intuiţie în sensul bergsonian al termenului, care nu este
1083

Solomon Marcus

prea diferită de aceea a savanţilor. Şi unii, şi alţii se
orientează după un simţ al armoniei. Atâta vreme cât este
încă vie, o teorie ştiinţifică are un suflet care te poate
acapara, întocmai ca o operă de artă. Faptul acesta este
foarte vizibil în cazul teoriei lui Newton. La presocratici,
ca Empedocle, Anaxagora, Heraclit, Protagoras sau
Democrit, întâlnim, de asemenea, o teorie care este în
acelaşi timp o viziune poetică a realităţii, asociată cu o
expresie verbală prodigioasă. Atât arta, cât şi ştiinţa sunt
dominate de afectivitate, nu se poate spune că prima este
caldă, iar a doua este rece (Victor Săhleanu şi-a intitulat o
carte, în mod desigur polemic şi retoric, Arta rece şi ştiinţa
fierbinte). Capacitatea noastră ştiinţifică şi cea poetică se
unesc într-un fel de bricolaj, după o expresie a lui Claude
Lévi-Strauss. „Fizica cuantică mă umple de admiraţie şi
emoţie, întocmai ca şi orice mare operă de artă. Nu
deosebesc emoţia ştiinţifică de cea artistică; ele au o
rădăcină comună, constând în dimensiunea lor estetică”,
observă Berger.

1084

Editura Spandugino, volumul al doilea,
Tipărit la Crescento Grup, Bucureşti,
021.2222.021, office@cgrup.ro

