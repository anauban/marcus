Informatica în umanistică
Viaţa studenţească, 13 iunie 1979
Câteva fapte recente, destul de puţin sau deloc
cunoscute în afara unui cerc restrâns de specialişti, sunt
semnificative pentru modul în care, în ultimii ani, cultura
noastră se înscrie în tendinţa generală de colaborare a
disciplinelor umaniste, artelor şi literatrurii cu ştiinţele
exacte şi cu tehnica. Le vom relata într-o ordine
întâmplătoare.
La Consiliul Culturii şi Educaţei Socialiste a luat
fiinţă un oficiu de calcul, în vederea inventarierii şi
prelucrării automate a datelor privind patrimoniul artistic
naţional. După ce, în 1976, publicase o Introducere în
teoria mulţimilor pentu muzicieni, profesorul Dinu
Ciocan a încredinţat, spre multiplicare, Litografiei
Conservatorului „Ciprian Porumbescu” din Bucureşti, un
curs de gramatici generative pentru muzicieni. Actele
Primei Conferinţe Internaţionale pentru prelucrarea
automată a datelor şi documentelor de istoria artei (Pisa,
septembrie 1978) au apărut de curând şi cuprind, între
altele, un grupaj de trei contribuţii româneşti: Grigore
Arbore Popescu – Marian Gheorghe: Clasificare în opera
lui Piero della Francesca; Emanuel Crivăţ, Mariela
258

Rãni deschise

Crivăţ, Marian Gheorghe, Doina Gheorghe: Prelucrare
automată a datelor privind unele monumente de
arhitectură; Gheorghe Păun: O gramatică a structurilor
arhitecturale. În „Buletin mathématique de la Société des
sciences mathématiques de la R.S.R.”, vol. 20 (70), nr. 4,
1978, Cristina Mateizel şi Emanuel Crivăţ propun un
algoritm de lectură a Ansamblului Văcăreşti, iar Anca
Manolescu întreprinde o clasificare automată a caselor
ţărăneşti de lemn. Pia Teodorescu Brînzeu, în noiembrie
1978, şi Mihai Dinu, în martie 1979, au susţinut teze de
doctorat privind strategia matematică în teatru, iar Sorin
Niţă, în mai 1979, a susţinut o teză de doctorat privind
studiul matematic al filiaţiei manuscriselor Cronicii Ţării
Româneşti. La Vălenii de Munte a avut loc, în zilele de
26 -27 mai 1979, o consfătuire cu tema „Aplicaţii ale
matematicii în istorie şi arheologie” (cu peste 40 de
participanţi). În programul sesiunii ştiinţifice a Facultăţii
de matematică din Bucureşti (28 mai - 2 iunie 1979) au
figurat, între altele, şi următoarele comunicări: Dănuţ
Iancu: Clasificarea poeziilor din volumul Plumb de G.
Bacovia; Mihaela Petrache: Modelarea matematică a
unor idei din teoria culorii; Vladimir Marina: Aspecte
generative ale formei şi culorii în pictură. Să mai
amintim preponderenţa gândirii geometrice deliberate la
unele expoziţii recente de pictură (expoziţia ceva mai
veche a Adinei Caloenescu ne stăruie şi acum în amintire)
259

Solomon Marcus

şi a gândirii algoritmice la unii compozitori care s-au
aflat recent în atenţia publicului (de exemplu, Lucian
Meţianu). Să mai amintim, în sfârşit, că în programul
celui de-al doilea Congres al Asociaţiei internaţionale de
studii semiotice (Viena, iulie 1979) sunt înscrise câteva
zeci de comunicări româneşti, cele mai multe dintre ele
aflându-se la răscrucea ştiinţelor cu artele şi literatura.
Numitorul comun al unor activităţi atât de diverse
este efortul interdisciplinar de apropiere a exegezei
umaniste de rigorile şi tehnicile pe care le pune la
dispoziţie actuala revoluţie ştiinţifică şi tehnică. În această
privinţă, însă, situaţia de ansamblu este, la noi, încă
nesatisfăcătoare. Avem încă de depăşit unele dificultăţi
importante. Mai întâi, trebuie să observăm informaţia încă
foarte lacunară, superficială, a multor umanişti, în ceea ce
priveşte perspectivele oferite de noile ramuri ale ştiinţei şi
de mijloacele moderne de prelucrare automată a datelor.
Multe relatări şi comentarii (şi aşa sporadice) în acest
domeniu sunt scrise şi acum după şablonul din urmă cu 20
de ani: undeva pe glob câteva firi extravagante încearcă să
compună muzică sau poezii cu ajutorul calculatorului, dar
în această privinţă omul nu va putea fi depăşit niciodată
de calculator. Procupată mai degrabă de ceea ce nu poate
decât de ceea ce poate face calculatorul, această filosofie
simplistă a contaminat şi pe unii informaticieni, care
ignoră problemele grave cu care este confruntată
260

Rãni deschise

informatica în umanistică. Conducerea Institutului central
pentru conducere şi informatică a sesizat această anomalie
şi stimulează acum, în rândul cercetătorilor săi,
preocupările de inteligenţă artificială din care nu lipsesc
problemele privind aspectele algoritmice ale comunicării
umane. Acelaşi institut acordă un ajutor esenţial Oficiului
de calcul al Consiliului Culturii şi Educaţiei Socialiste.
Preocupările pentru prelucrarea automată a limbilor
naturale şi a celorlalte tipuri de date care apar în
disciplinele umaniste nu lipsesc nici de la Centrul de calcul
al Universităţii din Bucureşti; dar ele pot fi considerate
derizorii în raport cu numeroasele facultăţi şi institute
umaniste din cadrul acestei universităţi, care ar fi trebuit
de mult să se constitue în unul dintre principalii solicitatori,
dintre principalii clienţi ai Centrului de calcul. Să nu uităm
că drepul, istoria, filologia şi literatura noastră abundă în
probleme nerezolvate de datare, de localizare, de
paternitate, de etimologie, de filiaţie a unor documente, de
înrudire a unor idiomuri, probleme care, toate, comportă
operaţii masive de prelucrare automată a datelor. La nivel
instituţional, se impune în această privinţă, iniţiativa de a
realiza măcar concordanţele marilor opere ale culturii
româneşti, începând, fireşte, cu aceea a lui Eminescu.

261

