Dialog cu Ion Biberi: Lumea de azi
Colecţia Reporter XX, Editura Junimea, Iaşi, 1980, 270 p.
Convorbirile reunite în această carte constituie
reproducerea ciclului de emisiuni „Lumea de azi”,
organizat săptămânal (marţi, orele 21), între 29 mai şi 30
decembrie 1969, pe programul I al Radiodifuziunii Române.
Transcrierea textului s-a făcut sub supravegherea
directă a redactorului Dona Roşu, căreia îi revine meritul
şi responsabilitatea exactităţii.
I.B.: În ce constă noutatea, fie a metodei, fie a
rezultatelor dumneavoastră, considerabile, în chipul cel
mai general, al soluţiilor pe care le daţi, sau, mai ales, a
problemelor ale căror date le puneţi, fără să mai încercaţi
totuşi să le daţi soluţii ultime? Forţez modestia
dumneavoastră!
Sunt, totuşi, cele o mie cinci sute – două mii de citări
ale operei dumneavoastră în diferite lucrări în străinătate
şi sunt numeroasele lucrări pe care le-aţi publicat în marile
case de edituri din New York, Paris...
S.M.: Metodele pe care le-am folosit aş încerca să le
reduc, într-un mod, bineînţeles netemeinic...
I.B.: Cât mai pe înţelesul tuturor!
419

Solomon Marcus

S.M.: ...spunând că m-a interesat, de pildă, mi-a plăcut
să încerc să pun alături metode şi noţiuni, în aparenţă
disparate. Adică mi-a plăcut să încerc să folosesc în anumite
probleme, instrumente în aparenţă depărtate. Am încercat
să folosesc în studiul funcţiilor, metode din teoria
numerelor, care este un domeniu de matematică discretă.
I.B.: Cu alte cuvinte, iertaţi-mă, eu cred că... cel mai
bun lucru de a cuprinde în adâncul ei o realitate este de a
ieşi din ea şi de a o privi şi sub altă incidenţă. Sau, s-o
priveşti din puncte de vedere diferite, nu?
S.M.: Este o căutare a unor legături ascunse.
I.B.: A două realităţi disparate... ?
S.M.: ... în aparenţă disparate.
I.B.: Fără îndoială!
S.M.: Poate e un mod de manifestare a metaforei în
ştiinţă.
I.B.: Sau în cubism, în pictură, este atitudinea care
priveşte un obiect din faţete diferite! Nu? Nu-i acelaşi
lucru? Sau fac o comparaţie...
S.M.: Comparaţia nu este forţată, şi ar putea să fie
aprofundată. Mi-a plăcut, de asemenea, să reiau unele
noţiuni şi rezultate considerate clasice şi care, în aparenţă,
nu mai prezentau ceva problematic.
I.B.: Care erau definitiv statornicite.
S.M.: În aparenţă, cel puţin. Da. Şi mi-a plăcut să le
reconsider, să încerc să scutur praful de pe ele...
420

Rãni deschise

I.B.: Şi să aduceţi noutatea...
S.M.: ...Să încerc să le readuc în actualitate...
I.B.:..Privindu-le sub o incidenţă nouă.
S.M.: ...şi trebuie să vă mărturisesc că, în această
privinţă, mi-a produs o deosebită satisfacţie faptul că,
reluând o clasă de funcţiuni introdusă de matematicianul
român Dimitrie Pompeiu încă în urmă cu 60 de ani, am
reuşit să le readuc în actualitate, şi, în ultimii ani, un
număr de matematicieni din Statele Unite, din Italia, din
Polonia, din India şi din alte ţări, au publicat cercetări în
acest domeniu, mergând pe urma lucrărilor mele. De
asemenea...
I.B.: Iertaţi-mă că vă întrerup, trebuie să fi fost pentru
domnia voastră, o mare bucurie.
Faptul că lucrurile acestea reprezintă un punct de
plecare, că dumneavoastră, lucrând la acest birou, scrieţi
lucruri care răsună de-a lungul şi vastitatea lumii şi, că, o
seamă de creiere ilustre de matematicieni şi de specialişti
de mare orizont se apleacă asupra acestor lucrări şi
dumneavoastră le fecundaţi şi le daţi o nouă direcţie, o
nouă orientare. E un lucru care, pentru mine, este
răscolitor, şi îmi daţi voie să vă aduc toată expresia
sentimentelor mele de...
S.M.: Da, e foarte plăcut să constaţi că oameni de la
celălalt capăt al pământului reiau problemele de care te-ai
ocupat şi... E un fel de ştafetă, care se transmite.
421

Solomon Marcus

I.B.: Da, da, şi în acelaşi timp o dovadă a unităţii
gândirii...
S.M.: ...Şi solidarităţii umane...
I.B.: ...Şi a solidarităţii intelectuale şi afective,
umane.
S.M.: Da. De asemenea, mi-a plăcut să mă ocup de
problemele puse de alţi matematicieni, ca Borel, Hausdorff,
Sierpinski şi alţii.
I.B.: Adică nu aţi dat numai, aţi şi primit. Şi e firesc!
S.M.: A, nu. Cred că am primit mult mai mult decât
am dat! Unora dintre aceste probleme am reuşit să le dau
soluţii definitive, altora, soluţii parţiale. În fond, însă, chiar
dacă soluţia este definitivă, orice soluţie conţine în
germene o nouă problemă. Şi, în fond, ştafeta asta nu se
încheie niciodată. Mi-a plăcut să pun, la rândul meu,
probleme. Am pus multe probleme, şi, cele mai multe
dintre ele au fost reluate de alţi matematicieni...
I.B.: Şi rezolvate, probabil.
S.M.: Da. Au fost multe dintre ele rezolvate, multe
dintre ele rezolvate de matematicieni străini, care şi-au
publicat rezultatele în reviste româneşti.
I.B.: Da. Atunci, v-aţi limitat în această parte a
activităţii dumneavoastră care nu se încheie, fireşte,
reprezintă chiar prima fază a activităţii şi gândirii
dumneavoastră, v-aţi limitat la ceea ce aş numi eu... în
sfârşit... în mod cu totul rudimentar şi poate inexact, la
422

Rãni deschise

matematica pură. Da. Pe cât sunt informat, şi aşa reiese
cel puţin din celelalte lucrări ale dumneavoastră din ultimii
ani, aţi folosit metoda matematică în studiul altor realităţi,
ca un punct de plecare şi i-aţi dat o nouă structurare, şi aţi
ajuns, ca de pildă, în poezie, în lingvistică, în teatru, la un
mijloc de investigaţie. Reprezintă aceasta o aplicare
propriu-zisă a matematicii, sau mă exprim eu greşit?
S.M.: Aş vrea, mai întâi, fiindcă dumneavoastră îmi
amintiţi de un moment care pentru mine a însemnat foarte
mult, şi anume momentul acesta în care am trecut de la
preocupările de matematică pură la unele preocupări pe
care dumneavoastră le numiţi de matematică aplicată, eu
le-aş numi... poate că ar fi mai bine să le numim preocupări
interdisciplinare, pentru că se întâmplă lucrul următor:
când spunem matematica aplicată avem impresia că e
vorba de folosirea unor instrumente matematice deja
existente, pentru investigarea unor probleme
extramatematice. Or, aici e vorba, cred, în fond, de ceva
mai mult. Pentru că lingvistica nu a găsit gata făcută
matematica de care avea nevoie.
I.B.: Dar nici fizica! Fizica reprezenta o sudură!
S.M.: Da. E adevărat că, într-o anumită măsură,
matematica s-a dezvoltat şi în funcţie de necesităţile fizicii.
Cu toate că, în cazul lingvisticii, fenomenul a avut o
amploare mai mare.
I.B.: Mai mare, fără îndoială!
423

Solomon Marcus

S.M.: Compartimente mari de matematică s-au
dezvoltat pentru a servi lingvisticii: teoria limbajelor
formale…
I.B.: Şi atunci, îmi daţi voie! Întrevăd aici o a doua
mare criză de conştiinţă, o nouă deschidere, şi care
reprezintă un fel de revoluţie sufletească, aceea a trecerii
de la matematica pură la noua preocupare...
S.M.: Fără a părăsi preocupările de matematică pură!
I.B.: Ei bine, nu găsiţi că această... nouă orientare a
reprezentat şi o readaptare a dumneavoastră?
S.M.: Da. De fapt, aş putea spune că există două
feluri de activitate matematică.
Există activitate matematică ce constă în prelucrarea unor concepte matematice deja existente, şi în
funcţie de rezultatele obţinute, de introducere a unor noi
concepte matematice şi, acesta este tipul de activitate
care m-a solicitat în teoria funcţiilor. Dar mai există şi
un alt tip de activitate matematică: acela de modelare
nemijlocită a unei realităţi extramatematice, o modelare
în care particip la naşterea unor noţiuni matematice
extrase direct din realitatea exterioară. Şi trebuie să
mărturisesc că m-a tentat foarte mult acest al doilea tip
de activitate, pe care teoria funcţiilor nu mi-l putea oferi.
Pentru că...
I.B.: Nu este, în acelaşi timp, o legătură cu
continuitatea şi cu... (audiţie neclară).
424

Rãni deschise

S.M.: Da. Voi vorbi imediat şi de acest lucru. Vedeţi,
teoria funcţiilor reale e un domeniu care s-a născut încă
la începutul secolului şi care în urmă cu vreo douăzeci
de ani îşi încheiase, în liniile mari, fixarea noţiunilor de
bază. Nu mai rămâneau decât anumite linii de pensulă,
anumite finisări de construcţie care trebuiau îndeplinite.
În timp ce lingvistica matematică ne-a oferit această
participare nemijlocită la naşterea unor noţiuni
matematice noi, dintr-o realitate mai umană, mai
complexă. Şi, cum aţi amintit dumneavoastră, aceasta a
fost o mare dificultate, a trebuit să trec de la continuu la
discret. Teoria funcţiilor este o matematică... a
continuului, în timp ce lingvistica solicită o matematică
discretă, o matematică finită. Şi, din acest motiv, a fost
pentru mine o anumită dificultate de adaptare. Pentru că
mă formasem în domeniul matematicii continue.
I.B.: Către ce vârstă a avut loc această „reciclare”,
cum se spune…?
S.M.: În urmă cu vreo 10-11 ani, deci pe la vârsta de
33 de ani.
I.B.: Ceea ce dovedeşte în acelaşi timp o mare
vitalitate, o mare vigoare intelectuală, şi, în acelaşi timp,
refuzul de a vă fixa pe lucrul deja dobândit. Neliniştea
aceasta sufletească...
S.M.: Vedeţi, n-aş putea să spun că am reuşit să duc
la capăt cu deplin succes acest proces de adaptare, şi vă
425

Solomon Marcus

mărturisesc că nu mă consider un om de ştiinţă cu adevărat
interdisciplinar. Mă consider doar un matematician, care
după ce a contemplat...
I.B.: Dar sunteţi în plină evoluţie!
S.M.: Sunt în plină evoluţie. Dar, deocamdată, mă
consider doar un matematician care, după ce a contemplat
lunga rază de acţiune a matematicii în domenii ca fizică,
mecanică şi, în general, în ştiinţele naturii, a dorit să vadă
exercitându-se această acţiune în domenii mai umane,
mai... apropiate de ceea ce numim umanistică.
I.B.: Aţi avut, probabil, două dificultăţi: a trebuit să
treceţi probabil sub raport matematic printr-un studiu
special de matematica automatelor. Şi a trebuit să vă
aprofundaţi cunoştinţele de lingvistică.
S.M.: A trebuit să învăţ pur şi simplu lingvistică, şi a
trebuit să învăţ un număr de teorii matematice, ca logica
matematică, teoria automatelor, care sunt foarte departe de
vechile mele preocupări de teoria funcţiilor.
I.B.: Îmi daţi voie să subliniez semnificaţia
sufletească şi în acelaşi timp cu caracter pedagogic pentru
tineretul actual, această posibilitate şi reviviscenţă a
domniei voastre şi de nemulţumire de a se menţine pe
vechile poziţii şi nevoia de a trece dincolo, de a cuceri
noi...
S.M.: Cred că este un efort pe care îl desfăşoară mulţi
oameni de ştiinţă azi, pentru că interesul pentru
426

Rãni deschise

interdisciplinaritate mi se pare a fi una dintre caracteristicile ştiinţei actuale.
I.B.: V-aş fi recunoscător să vă statorniciţi, succesiv,
pe cele trei domenii pe care... le-am evocat, şi în special
în lingvistica matematică, dându-ne câteva indicaţii
generale asupra dobândirilor, asupra rezultatelor obţinute.
S.M.: În lingvistica matematică am încercat să
modelez unele categorii lingvistice, ca aceea a părţii de
vorbire, a cazului, a genului gramatical, unele categorii
sintactice, relaţia de subordonare... Cu referinţe în special
în limba română, însă uneori, ca în cazul genului
gramatical, şi la alte limbi naturale, cum se vede în
monografiile pe care le-am publicat.
I.B.: Şi pe care v-aş ruga respectuos să binevoiţi a mi
le numi.
S.M.: Am publicat o carte de lingvistică algebrică în
Statele Unite, o alta, Introducere matematică în lingvistica
structurală, în Franţa; o carte de modele algebrice în
lingvistică în Cehoslovacia (apare chiar acum)…
I.B.: A fost scrisă în româneşte şi tradusă mai târziu…?
S.M.: E o situaţie mai complexă: o parte provine
dintr-o carte în româneşte, o altă parte este inedită.
Bineînţeles, am publicat un număr de cărţi în limba
română, printre care Gramatici şi automate finite, pentru
care am fost onorat cu un premiu al Academiei, şi îmi va
mai apărea o carte în Uniunea Sovietică, în anul viitor. În
427

Solomon Marcus

toate aceste cărţi am încercat să construiesc modele
algebrice ale principalelor categorii ale sintaxei şi ale
morfologiei. Însă, mai departe, am căutat să trec la unele
probleme ale limbajului poetic, care ştiu că v-a suscitat şi
pe dumneavoastră într-o carte recentă.
I.B.: Da, am avut o mare bucurie de a vă cita acolo
prin contribuţia dumneavoastră, pe care am socotit-o
hotărâtoare, dar asupra căreia n-am putut strărui. Era vorba
de o lucrare generală a matematizării poeziei, în sfârşit, a
explicaţiei poeziei, printr-o incidenţă matematică.
S.M.: Aş vrea să încerc să spun în câteva cuvinte cum
am ajuns să mă intereseze aceste lucruri.
I.B.: Vă rog.
S.M.: Vedeţi, un matematician preferă formele
concentrate de limbaj şi aş putea spune că asta este una
dintre trăsăturile lui dominante, deoarece tocmai în această
concentrare (şi a poeziei şi a matematicii), rezidă funcţia
euristică a limbajului matematic. Aceeaşi preferinţă, cum
aţi remarcat, există şi în domeniul poeziei. Numai că, în
timp ce în matematică această concentrare a expresiei e
realizată prin folosirea unor limbaje artificiale, toată
simbolistica matematică, ce în fond e un limbaj
internaţional, în poezie această concentrare se realizează
prin mijloace exclusive ale limbilor naturale. Această
situaţie mi s-a părut că poate trezi unui matematician
curiozitatea de a studia limbile naturale.
428

Rãni deschise

I.B.: Ce înţelegeţi prin limbi naturale, vă rog?
S.M.: Adică româna, franceza...
I.B.: Acuma, ce se întâmplă? Trebuie să facem aici –
dar... ne-am duce prea departe... – o stratificare între
limbajul vechi... pe care îl foloseşte adeseori poetul, şi
limbajul de circulaţie zilnică, de care poetul în general se
fereşte.
S.M.: Da, e adevărat că limbajul poetic, în fond, este
un concept ideal de limbaj, către care tinde orice poet...
I.B.: Şi este o creaţie a fiecărui poet în parte... mă
rog... Cum spune Paul Valéry, este creaţia unui limbaj
înlăuntrul unui limbaj.
S.M.: Da.
I.B.: În general!
S.M.: Cred că în această privinţă, Pius Servien a adus
idei foarte preţioase.
I.B.: Coculescu.
S.M.: Da, e fiul acelui astronom şi om de cultură
român, Coculescu. Trebuie să mărturisesc că tocmai aceste
idei au constituit pentru mine punctul de plecare în
cercetările de poetică, şi de fapt aceste cercetări m-au adus
din nou, pe o altă cale, la problema infinitului. Pentru că
se întâmplă lucrul următor: orice limbaj este infinit, însă
există o deosebire mare între infinitul din limbajul ştiinţific
şi infinitul din limbajul poetic. Semnificaţia ştiinţifică este
discretă. Ţine de ceea ce numim noi în matematică
429

Solomon Marcus

infinitul numărabil, care e sugerat, de pildă, de un şir de
puncte disparate. De ce? Pentru că fiecare frază în limbajul
ştiinţific are o semnificaţie univocă.
I.B.: În timp ce în... ?
S.M.: În timp ce în limbajul poetic fiecare frază are
o semnificaţie care se metamorfozează de la un moment
la altul şi de la un loc la altul. Şi asta îi conferă un caracter
continuu. În fond, semnificaţia poetică fiind continuă, ea
evocă ceea ce în matematică se numeşte infinitul
nenumărabil, infinitul de puterea continuului, care e
sugerat de pildă de... o linie continuă, de o trăsătură
continuă de creion.
I.B.: Şi atunci, această cercetare asupra poeticii, din
unghi matematic, este încă în plină elaborare. Sau...
S.M.: Această idee, despre care tocmai am vorbit, am
încercat s-o pun la baza unui model matematic al opoziţiei
dintre limbajul ştiinţific şi limbajul poetic.
I.B.: Da. Cu alte cuvinte, ce se întâmplă? În timp ce
poetul creează şi ascultă fără să-şi dea seama, ascultă în
mod instinctiv de unele orientări matematice, de anumite
scheme matematice, dumneavoastră n-aţi făcut altceva
decât să descoperiţi aceste scheme matematice şi să le...
S.M.: Am încercat să le explicitez...
I.B.: Să ne arătaţi structura...
S.M.: ...să descriu contradicţia care există între
caracterul discret al expresiei poetice şi caracterul
430

Rãni deschise

nediscret al semnificaţiei poetice. Mi se pare că această
contradicţie conţine ceea ce numim inefabilul poetic.
I.B.: Da! E o perspectivă tulburătoare. Vă mulţumesc
pentru această foarte interesantă prezentare a gândirii
domniei voastre. Îmi îngăduiţi, în concluzie, să observ că
lucrările dumneavoastră (şi poate că acesta este şi sensul
activităţii dumneavoastră de până acum, care nu ştiu cum
se va desfăşura mai târziu) reprezintă un fel de tendinţă de
unificare: unificare prin activitatea interdisciplinară de care
vorbeaţi, şi, în acelaşi timp, unificare prin spiritul de
sinteză pe care l-aţi provocat la cercetătorii care au urmat
studiul dumneavoastră.
S.M.: Aş vrea numai o mică completare să mai fac.
Dacă cercetările de lingvistică matematică au putut lua la
noi în ţară o amploare destul de mare, cred că acest lucru
se datoreşte în bună măsură condiţiilor prielnice care s-au
creat prin înţelegerea pe care am găsit-o la academicianul
profesor Moisil, la academicianul profesor Rosetti, şi care
au creat toate înlesnirile pentru ca... mulţi tineri talentaţi,
matematicieni şi lingvişti, să se îndrepte către aceste
preocupări.

431

