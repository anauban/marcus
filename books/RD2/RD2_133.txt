Comunitatea profesională a informaticienilor
Viaţa Studenţească, anul XXXI, nr. 43 (1171),
miercuri, 28 octombrie 1987, p. 2
Să urmărim, în continuare, ideile lui J. Gruska în
legătură cu organizarea cercetărilor teoretice de
informatică şi coordonarea lor cu activitatea practică.
Este nevoie de o organizare puternică, în stare nu
numai să realizeze cooperarea dintre cercetătorii
teoreticieni, ci şi să îndrume interacţiunea acestora cu
practicienii, cu viaţa socială. Asociaţia Europeană pentru
Informatica Teoretică ar putea îndeplini aceste funcţii. Ar
trebui folosită, în această privinţă, experienţa mai veche a
cooperării dintre fizica teoretică şi aplicaţiile ei.
De ce nu este societatea capabilă să utilizeze în măsură
suficientă potenţialul pus la dispoziţie de tehnologie? Din
cauză că nu există un număr suficient de mare de cercetători
de valoare ridicată; iar aceasta se întâmplă deoarece
educaţia informatică a tineretului (atât cea din liceu, cât şi
cea universitară) nu se face încă în mod satisfăcător.
Experienţa recentă în domeniul ingineriei de programe, al
inteligenţei artificiale şi al arhitecturii calculatoarelor arată
că progresul în aceste direcţii este esenţial condiţionat de o
mai bună înţelegere a fundamentelor, înţelegere care nu se
937

Solomon Marcus

poate realiza decât în procesul educaţional. Dar instrucţia
informatică a tineretului se află încă într-un stadiu incipient,
sub presiunea prea puternică a practicismului, a limbajelor
de programare şi a tehnologiei, fiind copleşită de tot felul
de detalii neesenţiale.
În ultimii 20 de ani, au intervenit schimbări mari în
ceea ce priveşte tipul de teorie care orientează dezvoltarea
informaticii, dar programele didactice înregistrează încă
slab aceste modificări de optică. Se întâmplă frecvent ca
în cursurile de bază să figureze chestiuni mai potrivite
pentru un curs neobligatoriu, destinat celor avansaţi şi,
reciproc, unele chestiuni fundamentale îşi găsesc locul
numai în cadrul unor cursuri la alegere. Gruska este de
părere că, din acest punct de vedere, învăţământul
matematic este mai realist. Nimeni nu se gândeşte să
predea teoria axiomatică a mulţimilor studenţilor din anul
întâi. În ciuda rolului important al logicii în matematică,
numai unele noţiuni de bază ale logicii intră în programa
obligatorie de matematică. Gruska aderă la această politică
a programelor de matematică, pe care unii matematicieni
ar dori-o modificată. În schimb, consideră, şi ar fi greu să
nu-i dăm dreptate, că achiziţiile informaticii în materie de
complexitate şi de semantică, principale forţe directoare
în dezvoltarea actuală a informaticii, îşi găsesc încă prea
puţin loc şi prea târziu în programele didactice de
informatică. Destul de frecvent, teoria este văzută, în
938

Rãni deschise

procesul educaţional, mai degrabă ca o gimnastică a
creierului, decât prin ceea ce ea reprezintă conceptual şi
metodologic. Gruska nu vede o deosebire esenţială între
situaţia actuală a teoriei în învăţământul informatic şi
întâmplarea pusă pe seama lui Peano, care, într-un curs de
matematică pentru studenţii ingineri, a consacrat aproape
un semestru întreg… axiomaticii numerelor naturale.
Informatica actuală manifestă concomitent o tendinţă
de unificare (a teoriei, soft şi hard; a memoriei, logicii şi
comunicării) şi una de diversificare (a aplicaţiilor şi
produselor). Unificarea cere şi permite orientarea
procesului educaţional spre chestiunile de fundamente, spre
teorie (concurenţă, semantică), şi nu spre produse (sisteme
de operare, limbaje de programare), cum se întâmplă încă
frecvent. Diversificarea face necesară o mare varietate de
cursuri pentru avansaţi, cursuri care trebuie să ţină seama
de faptul că o teorie va fi realmente utilizată numai cinci
sau zece ani după ce suficient de mulţi studenţi care şi-au
însuşit teoria respectivă au intrat în activitatea practică.
Cum se pot rezolva aceste probleme? Este necesară
o concepţie unitară, la nivelul întregii comunităţi mondiale,
a celor ce lucrează în domeniul informaticii, o concepţie
suficient de flexibilă pentru a nu împiedica diferenţe
impuse de condiţii locale.
Gruska consideră necesară formarea a patru tipuri de
specialişti în informatică. Primul tip este cel al teoreticianului
939

Solomon Marcus

care se ghidează după criterii estetice de simetrie, simplitate
şi putere expresivă, întocmai cum procedează şi
matematicianul teoretician. Al doilea tip se dedică pentru un
timp îndelungat, eventual pentru toată viaţa, unui domeniu
mai restrâns, dar foarte important, pe care-l aprofundează şi
în care abordează principalele probleme deschise. Al treilea
tip se plasează la frontiera dintre teorie şi aplicaţii, abordând
probleme teoretice puse de practică sau valorificând teoria
în unele aplicaţii, dezvoltând modele, concepte, metode,
tehnici de demonstraţie, obţinând rezultate pe baza acestora.
Al patrulea tip necesar este cel al teoreticianului capabil să
se transfere – temporar sau definitiv – într-un domeniu al
practicii, după ce a acumulat suficiente cunoştinţe, tehnici şi
experienţă.
Independent de tipul căruia îi aparţine, cercetătorul
informatician trebuie să fie atent la tot ceea ce survine mai
important în teoria şi practica informatică, deoarece
informatica se află la o răscruce, orice extrapolare putând
fi înşelătoare. Cât de adânc se poate implica un teoretician
în practica informatică? El poate conduce conceptual şi
chiar organizaţional un proiect practic, dar nu poate
pretinde să realizeze singur un produs comercial de succes,
pentru care este necesară o vastă experienţă în ingineria
soft, în ergonomie şi în comerţ.
Cum se vinde teoria, cum se realizează transferul ei
în practică? Problema trebuie abordată la nivelul întregii
940

Rãni deschise

comunităţi profesionale a informaticienilor. La nivel
instituţional sau chiar al unei ţări mai mici, încercarea de
a realiza acest transfer poate aduce prejudicii cercetării
teoretice, absolut vitală pentru informatică. Nu orice
produce teoria se poate vinde, de exemplu teoremele şi
demonstraţiile rareori se vând bine, deşi sunt foarte
importante. Se vând sau se transferă practicii paradigme,
moduri de gândire, modele, teorii, concepte, metode,
notaţii şi, în special, unele produse secundare, dacă sunt
transferate sistematic, pe o cale adecvată şi în timp util.
Nu trebuie să se pretindă că orice produs al teoriei se va
dovedi util, cum se promite uneori, pentru a da satisfacţie
celor care fac presiuni asupra teoriei, cerându-i să se
dovedească utilă.

941

