Potenţialul interdisciplinar al matematicii
În volumul Interdisciplinaritatea în ştiinţa contemporană,
Editura Ştiinţifică şi Enciclopedică, 1980
Printre diferitele discipline care definesc efortul de
cunoaştere ştiinţifică a realităţii înconjurătoare, matematica
ocupă, fără îndoială, un loc aparte. Cu greu se mai poate
accepta astăzi definirea matematicii prin obiectul ei, aşa
cum se proceda în secolul trecut. Nici reducerea ei la o
metodă de gândire nu este posibilă, deşi matematica este,
într-o măsură însemnată, un ansamblu de metode de
explorare a fenomenelor celor mai variate. Este matematica
o disciplină de punct de vedere? Răspunsul ar putea fi
afirmativ în măsura în care ar fi tautologic, deci numai dacă
simplul fapt de a folosi un model matematic ar fi identificat
cu adoptarea unui punct de vedere. Dar cum s-ar împăca o
atare convenţie cu indiferenţa, cu caracterul neutru al
faptelor matematice în raport cu diferitele interpretări care
li se pot atribui? Punctul de vedere ia naştere abia în
momentul în care modelul matematic este comentat,
motivat sau criticat în raport cu adecvarea şi relevanţa sa
faţă de o anumită realitate naturală sau socială.
Ezitarea pe care o avem în tentativa de a defini
matematica pare să contrasteze cu uşurinţa cu care
355

Solomon Marcus

recunoaştem un text matematic. Faptul se explică prin
particularităţile deosebit de pregnante ale limbajului
matematic. Este interesant faptul că, pe măsură ce
matematica şi-a diversificat obiectul şi metoda,
„jargonul” ei s-a tehnicizat din ce în ce mai mult, atât ca
terminologie, cât şi din punctul de vedere al utilizării
unor elemente artificiale, deci care depăşesc mijloacele
limbilor naturale.
În ce constă diversificarea obiectului matematicii? De
la studiul relaţiilor cantitative şi al formelor spaţiale,
matematica a evoluat spre studiul diferitelor tipuri de
structuri şi sisteme, al aspectelor dinamice, de proces, al
problemelor de strategie şi interacţiune. În mod
corespunzător, s-au diversificat şi metodele matematice.
De la cele numerice, aritmetice şi geometrice s-a trecut la
cele algebrice şi analitice, apoi la cele combinatorii,
probabiliste, logice, topologice şi decizionale, pentru ca
acum să fie toate reconsiderate în lumina metodelor
computaţionale.
De unde şi prin ce procedee îşi creează matematica o
problematică atât de bogată şi de variată? Ce îi asigură
împrospătarea necontenită a ideilor şi metodelor?
Răspunsul nu se lasă uşor întrevăzut. Dacă în cazul unor
discipline ca fizica, chimia, biologia, istoria, economia sau
sociologia motivările se referă nemijlocit la anumite clase
de fenomene ale naturii sau societăţii, a căror cercetare
356

Rãni deschise

prezintă o necesitate evidentă, legitimitatea matematicii,
în forma atât de abstractă şi de tehnicizată pe care a
dobândit-o astăzi, se lasă mai greu descifrată. Aşa se şi
explică numeroasele fenomene de contestare a
matematicii, ca mişcare intelectuală de amploare în
societatea contemporană, în timp ce niciunui specialist în
vreo disciplină a naturii sau a societăţii nu i-a trecut prin
minte să conteste utilitatea rezultatelor fizicii, ale chimiei
sau biologiei. În faţa unui profan, matematica îşi
legitimează mai greu problematica şi rezultatele, chiar
dacă acest profan este inginer, economist sau biolog.
Legăturile matematicii cu realitatea sunt mai indirecte, mai
greu sesizabile decât în cazul fizicii, chimiei sau biologiei.
În acelaşi timp, matematica posedă, într-o măsură mai
mare poate decât orice altă disciplină, capacitatea de a
surprinde, de a contrazice prin rezultatele ei intuiţiile
comune. Este greu de găsit o ştiinţă care să fie atât de
bogată în rezultate şi entităţi paradoxale cum este
matematica.
Dincolo de toate ezitările pe care le avem în
formularea unei definiţii atotcuprinzătoare a matematicii
moderne, un fapt este incontestabil, chiar dacă el
scandalizează pe unii oameni de cultură (inclusiv unii
matematicieni), în special din domeniile umaniste şi
sociale: matematica se hrăneşte din întreaga cunoaştere
umană şi o afectează pe aceasta în întregime, propunând,
357

Solomon Marcus

ca să spunem aşa, un numitor comun al tuturor
compartimentelor ei. Matematica este deci o punte de
legătură între toate disciplinele. Îndreptându-şi atenţia
spre aspectele care manifestă o relativă independenţă faţă
de natura substanţei studiate, reţinând numai forma
acesteia, indiferent de faptul dacă substanţa şi forma pe
care le avem în vedere (ne referim aici la clasicele
dicotomii propuse în urmă cu câteva decenii de către
lingvistul danez Louis Hjelmslev) aparţin unui anume
conţinut sau unei anumite expresii, matematica devine o
ştiinţă a transferurilor şi analogiilor de idei, construcţii,
rezultate, metode, teorii sau… analogii (pentru a folosi o
butadă a matematicianului polonez Stephan Banach).
Potenţialul interdisciplinar al matematicii constă deci nu
numai în faptul că ea se află peste tot, că devine inevitabilă
în orice disciplină care a ajuns la o maturitate suficientă
pentru a-i permite să-şi separe aspectele structurale,
formale de cele substanţialiste, ci şi pentru că legăturile
dintre discipline nematematice sunt de multe ori realizate
prin intermediul matematicii. Matematica este un
catalizator al unui număr imens de procese de comunicare
între discipline naturale sau/şi sociale. Discipline de punct
de vedere, ca teoria sistemelor şi semiotica, dar şi
discipline mai speciale, ca lingvistica, îşi dezvoltă virtuţile
aplicative în special prin intermediul matematicii. În acest
fel, matematica obligă celelalte discipline la o mai
358

Rãni deschise

profundă cunoaştere reciprocă, la o mai profundă legătură
şi, eventual, la înscrierea lor într-o viziune mai
cuprinzătoare, care este însăşi viziunea interdisciplinară
promovată de evoluţia recentă a cunoaşterii umane. La
acest proces participă atât ştiinţele, cât şi artele, atât
domeniile inginereşti (fie ele energetice sau
informaţionale) cât şi activităţile practice din economie,
industrie şi din celelalte sfere ale vieţii sociale.
Unele precizări sunt de făcut aici. Mai întâi, trebuie să
observăm că, în evoluţia ei multimilenară, matematica nu
se află acum pentru prima oară în situaţia de a influenţa toate
celelalte domenii, stimulând colaborarea lor. Această situaţie
a fost de fapt starea firească a matematicii până în urmă cu
vreo cinci sute de ani. Antichitatea şi Renaşterea au stat sub
semnul universalităţii structurii matematice a întregii culturi
umane. Această viziune se prelungeşte, prin Pascal şi
Leibniz, de exemplu, până în perioada clasicismului, dar tot
prin Blaise Pascal îşi afirmă apusul, prin acea dicotomie care
avea să devină celebră: „esprit géométrique-esprit de finesse
(spiritul geometric-spiritul de fineţe). Să observăm însă că,
în timp ce universalitatea gândirii matematice era, în
Antichitate şi în Renaştere, efectul unei insuficiente separări
a disciplinelor, deci consecinţa unui sincretism general al
ştiinţelor, artelor şi meşteşugurilor, astăzi această
universalitate este rezultatul unui proces opus. Cunoaşterea
umană a ajuns la un grad atât de înalt de specializare, de
359

Solomon Marcus

diferenţiere a domeniilor şi subdomeniilor, încât
comunicarea între ele, integrarea lor în noi viziuni
integratoare devin vitale. Devin ele şi posibile? În cazul
afirmativ, care sunt motivele pentru care acest proces de
colaborare şi sinteză interdisciplinară nu s-a petrecut, de
exemplu, în urmă cu o sută de ani, ci abia în a doua jumătate
a secolului nostru? Acest fapt este legat atât de dezvoltarea
matematicii, cât şi de dezvoltarea celorlalte discipline.
Matematica secolului trecut era încă predominant numerică
iar celelalte discipline, cu excepţia mecanicii, astronomiei
şi fizicii, care erau de multă vreme beneficiare ale
matematicii numerice, în special ale calculului diferenţial şi
integral şi ale teoriei ecuaţiilor diferenţiale, nu erau încă în
măsură să folosească metodele şi rezultatele analizei
matematice. Biologia, de exemplu, abia la sfârşitul secolului
al XIX-lea şi începutul secolului nostru a început să
beneficieze de metodele teoriei ecuaţiilor diferenţiale, prin
cercetările lui Vito Volterra privind lupta pentru existenţă,
dusă de diferite specii de animale. Disciplinele vieţii şi ale
societăţii beneficiau de mult de rezultatele matematicii
numerice şi ale geometriei, dar la nivelul la care acestea
fuseseră dezvoltate cu multe secole în urmă. Să amintim, de
exemplu, faptul că muzica a cunoscut una dintre primele
analize numerice încă în Antichitate, prin Pitagora. Numărul
de aur (1 +
) / 2 care apare în problema împărţirii unui
segment în două segmente inegale, astfel încât raportul
360

Rãni deschise

dintre segmentul mare şi cel mic este egal cu raportul dintre
segmentul total şi cel mare, a fost cunoscut şi el încă din
Antichitate, fiind identificat ulterior în numeroase opere de
arhitectură şi sculptură.
Prin Kepler, numărul de aur a fost corelat cu şirurile
lui Fibonacci (definite prin proprietatea fiecărui termen
de a fi suma celor doi termeni imediat precedenţi),
stabilindu-se faptul că şirul rapoartelor de termeni
consecutivi dintr-un şir al lui Fibonacci converge către
numărul de aur. S-a constatat că o mare parte din
fenomenele de creştere din lumea organică (de exemplu
în botanică şi în anatomia umană) se desfăşoară după
şiruri ale lui Fibonacci, iar dacă utilizarea instinctivă a
proporţiei de aur a dominat o mare parte din arta
antichităţii, a Renaşterii şi chiar a zilelor noastre,
numeroase sunt şi cazurile utilizării sale deliberate (s-a
pretins că experienţe psihologice au arătat că elevii
preferă, între mai multe segmente divizate prin câte un
punct, pe cele divizate după proporţia de aur). În secolul
al XIX-lea, a atras cu deosebire atenţia legea lui Weber şi
Fechner, conform căreia omul percepe lumea
înconjurătoare după o lege logaritmică, în sensul că
senzaţia este logaritmul excitaţiei. Cu alte cuvinte, la un
şir de excitaţii măsurabile, aflate în progresie geometrică,
senzaţiile noastre de răspuns, presupuse şi ele măsurabile,
variază în progresie aritmetică. În acest fel, matematica
361

Solomon Marcus

elementară a devenit relevantă nu numai pentru biologie,
dar şi pentru psihologie. Rezultate de acest fel s-au
acumulat destul de multe şi de semnificative, dar ele
păreau să se constituie în manifestări izolate, care nu
permiteau încă să se vorbească de relevanţa matematicii
în raport cu disciplinele omului. Pentru ca impactul
matematicii asupra acestor discipline să devină atât de
esenţial încât să se poată vorbi de o biologie matematică
şi de o psihologie matematică, a trebuit să se mai scurgă
câteva decenii, timp în care, pe de o parte, matematica
şi-a dezvoltat instrumente noi, apte pentru studiul calităţii
şi al fineţii, al impreciziei şi întâmplătorului, iar biologia
şi psihologia, pe de altă parte, au reuşit să-şi precizeze
unele unităţi şi structuri fundamentale, să-şi dezvăluie
unele aspecte combinatorii şi topologice, în care
importantă este în primul rând relaţia în care intră anumite
elemente, nu natura lor. Dar, evident, între cele două serii
de procese, cele din matematică, pe de o parte, cele din
biologie şi psihologie, pe de altă parte, a apărut uneori un
decalaj destul de mare. Astfel, teoria lanţurilor Markov,
iniţiată în matematică la începutul secolului nostru, a
trebuit să aştepte cincizeci de ani pentru a deveni cadrul
teoretic al studiului proceselor probabiliste de învăţare, în
timp ce dezvoltarea celulară a organismelor filamentoase
a devenit un obiect de studiu al teoriei matematice a
automatelor abia în deceniul al şaptelea al secolului
362

Rãni deschise

nostru, deoarece această teorie a apărut târziu, odată cu
dezvoltarea informaticii.
Ceea ce am spus mai sus despre biologie şi psihologie
este adevărat şi pentru chimie, pentru lingvistică, pentru
economie şi pentru celelalte ştiinţe sociale. Aristocratismul
matematicii constă în faptul că ea devine utilă altor
discipline în măsura în care acestea din urmă sunt suficient
de evoluate în trecerea lor de la studiul aspectelor
substanţialiste la cel al aspectelor relaţionale, structurale,
sistemice. O disciplină ca istoria a trebuit, de aceea, să
aştepte multe secole pentru a putea beneficia de sprijinul
matematicii. Dar nu e mai puţin adevărat că nici
disciplinele nematematice nu găsesc totdeauna o
matematică gata făcută, aşa cum a găsit teoria relativităţii
un sprijin în geometriile neeuclidiene dezvoltate anterior
ei sau cum a beneficiat studiul proceselor de învăţare de
studiul proceselor Markov şi de cel al lanţurilor cu legături
complete, acestea din urmă fiind introduse în 1935, deci
cu vreo douăzeci de ani mai devreme. Exemple
concludente sunt, din acest punct de vedere, lingvistica,
biologia celulară şi biologia moleculară; ele au stimulat
matematica să dezvolte un nou compartiment al ei, teoria
limbajelor formale, cu ramificaţii recente de o mare
complexitate şi rază de acţiune, ca teoria sistemelor
Lindenmayer (esenţială în domeniul biologiei celulare) şi
teoria gramaticilor picturale (care stă la baza punctului de
363

Solomon Marcus

vedere sintactic în problemele de recunoaşterea formelor,
cu consecinţe importante în biologie, chimie, arhitectură).
Este deci clar că universalitatea actuală a gândirii
matematice este de cu totul altă natură decât aceea din
Antichitate sau din perioada Renaşterii. În timp ce atunci
matematica era nedezlipită de celelalte discipline, ca
urmare a unei insuficiente diferenţieri a domeniilor de
cercetare, a unei insuficiente autonomizări a acestora,
acum impactul matematicii se explică tocmai ca o reacţie
la o diferenţiere prea accentuată a disciplinelor de studiu.
Dar ceea ce este comun ambelor etape – şi acest fapt ni se
pare fundamental – este nevoia internă pe care studiul
oricărei clase de fenomene o manifestă de a se prevala de
gândirea matematică.
Această universalitate a gândirii matematice a fost
interpretată de unii autori ca o pretenţie a matematicienilor
de a institui o hegemonie a matematicii asupra celorlalte
domenii ale cunoaşterii. Poate că unii matematicieni sunt
realmente stăpâniţi de această stare de spirit; ea ar fi
simetrică faţă de starea de spirit diametral opusă, a celor
care se simt complet intimidaţi de limbajul matematic şi
care, pentru acest motiv, îl resping („omul, când nu
înţelege, este contra”, observa Gr. C. Moisil) sau, pentru
exact acelaşi motiv, îl absolutizează, văzând în el un
instrument miraculos. În fapt însă, universalitatea
matematicii este complet echilibrată de aservirea ei faţă de
364

Rãni deschise

celelalte discipline. Este greu de răspuns dacă matematica
restituie surorilor ei mai mult decât acestea i-au
împrumutat. Dar nici nu este important acest lucru; ceea
ce este important este faptul că matematica pune la
dispoziţie celorlalte discipline un produs prelucrat, esenţial
diferit de cel pe care ea l-a împrumutat de la acestea.
Întregi domenii ale matematicii s-au născut sub presiunea
altor discipline. Exemplul calculului diferenţial şi integral,
ale cărui concepte fundamentale provin din necesitatea de
a se rezolva unele probleme de fizică, este bine cunoscut.
Teoria matematică a stabilităţii îşi are şi ea originea în
mecanică, după cum teoria matematică a distribuţiilor îşi
are originea în unele „funcţii imposibile” cu care lucrau
fizicienii. Un exemplu mai recent este teoria matematică
a catastrofelor, pe care René Thom a edificat-o în legătură
cu modelarea fenomenelor discontinue din biologie şi din
lingvistică, dar care ulterior a afectat ansamblul ştiinţelor
sociale, de la economie la studiul războaielor. Teoria
matematică a jocurilor de strategie a fost iniţiată de John
von Neumann şi Oskar Morgenstern în cursul celui de-Al
Doilea Război Mondial, sub impulsul problemelor
economice şi al celor de tactică şi strategie puse de
conflagraţia cu Germania hitleristă. Dar nu numai
motivaţia iniţierii unei preocupări trebuie luată în
considerare, ci întregul metabolism pe care o disciplină
matematică îl stabileşte, de-a lungul dezvoltării ei, atât cu
365

Solomon Marcus

celelalte discipline matematice, cât şi cu realitatea din afara
matematicii. Teoria matematică a probabilităţilor s-a
născut din studiul jocurilor de noroc, dar ea nu ar fi căpătat
o însemnătate atât de mare dacă nu ar fi fost alimentată
necontenit cu probleme, idei şi sugestii venite din
mecanică, fizică, biologie, economie, lingvistică (mai e
oare nevoie să amintim că teoria lanţurilor Markov s-a
născut din studiul alternării vocalelor şi consoanelor în
Evgheni Oneghin a lui Puşkin?) şi din celelalte discipline
social-umaniste, deci practic din totalitatea disciplinelor,
pentru că nu există domeniu din care să lipsească factorul
întâmplare.
Această aservire a matematicii faţă de celelalte
domenii are mai multe aspecte. Un aspect se referă, aşa
cum am văzut, la presiunea pe care o exercită asupra
matematicii problematica altor discipline. Din acest punct
de vedere, matematica devine de multe ori o „aşteptare
frustrată”, deoarece ea are modul ei propriu de a formula
problemele şi, din acest motiv, foarte rar preia tale quale
o problemă, dând astfel impresia că se ocupă de o altă
problemă decât aceea care i s-a propus. Modalitatea
matematică de a formula întrebările este rezultatul unor
transformări aplicate întrebărilor iniţiale, transformări care
le fac uneori pe acestea din urmă de nerecunoscut. Pentru
profan, aceste transformări capătă un caracter ezoteric,
fiind uneori privite ca un mod de a eluda problema reală.
366

Rãni deschise

De aici şi butada lansată de unii ingineri, după
care matematicienii sunt acei oameni care-ţi rezolvă
orice problemă… cu excepţia celor de care ai nevoie.
Într-adevăr, un inginer are uneori impresia că trebuie să
rezolve o anumită ecuaţie diferenţială, dar se poate
întâmpla ca aceasta să nu fie posibil sau/şi necesar. Modul
de a privi pe care-l promovează teoria calitativă a ecuaţiilor
diferenţiale îi furnizează informaţii care pot părea
irelevante sau pe care nu ştie încă în ce fel să le folosească.
De asemenea, se poate întâmpla ca, pe baza unei intuiţii
înşelătoare, să căutăm o soluţie cu proprietăţi care nu pot
fi satisfăcute; preocuparea matematicianului pentru
obţinerea unei teoreme corespunzătoare de existenţă poate
părea sterilă. Vom relata aici o întâmplare de acest fel,
legată de cercetarea matematică în domeniul indicatorilor
sociali. Se ştie că numărul parametrilor care intervin în
studiul calităţii vieţii este deosebit de mare, generând o
cantitate mare de indicatori sociali, globali sau individuali,
a căror stăpânire şi prelucrare comportă multe dificultăţi.
S-a pus, de aceea, problema găsirii unei agregări
convenabile, în sensul grupării lor convenabile, în aşa fel
încât fiecare grupă de indicatori să poată fi înlocuită
printr-un singur indicator (eventual chiar un indicator
selecţionat din grupa respectivă), care să sintetizeze, să
reţină semnificaţiile tuturor indicatorilor din grupă. În mod
natural, s-au formulat anumite exigenţe faţă de aplicaţia
367

Solomon Marcus

care asociază fiecărei grupe un indicator unic. Astfel, i
s-a cerut să fie o aplicaţie prin intermediul căreia, la
modificări mici ale indicatorilor individuali, corespunde o
modificare mică a indicatorului sintetic. I s-a mai cerut
acestei aplicaţii să fie, în raport cu fiecare variabilă (deci
în raport cu fiecare indicator individual), o aplicaţie
monotonă pe porţiuni).
În sfârşit, i s-a cerut să fie necompensatoare, în sensul
că nu totdeauna doi indicatori se pot compensa reciproc
(de exemplu, libertatea nu poate fi acordată în schimbul
hranei şi nici hrana în schimbul libertăţii). S-a demonstrat
însă că o astfel de agregare a indicatorilor pur şi simplu nu
există, deci nu are sens să o căutăm. Un astfel de rezultat
nu putea fi intuit, iar cercetarea sociologică efectuată cu
metode tradiţionale nu avea cum să ajungă la el.
Un alt aspect al dependenţei matematicii de celelalte
discipline se referă chiar la dezvoltarea internă a
matematicii. O reprezentare simplificată a acestei
dezvoltări cuprinde trei faze, ca în activitatea unui automat
ale cărui intrări provin din exterior, fiind apoi supuse
acţiunii interne a automatului, acţiune care se converteşte
în anumite ieşiri spre exterior. Realitatea este însă mai
complexă. Viaţa unei teorii matematice nu interferează cu
realitatea exterioară ei numai în etapa ei iniţială şi în etapa
ei finală, ci se află într-o continuă confruntare cu această
realitate. Desigur, aceasta se întâmplă nu numai cu teoriile
368

Rãni deschise

matematice, ci cu orice teorie ştiinţifică. Apare, însă, aici
o particularitate a teoriilor matematice, pe care trebuie s-o
discutăm pentru semnificaţia ei general filozofică şi pentru
însemnătatea ei practică. Se ştie că teoriile matematice
moderne au o structură axiomatic deductivă; tratatul lui
Nicolas Bourbaki, care expune într-un astfel de cadru o
mare parte din teoriile matematice, este semnificativ în
acest sens. Într-o teorie axiomatic-deductivă există o
tentaţie irezistibilă de a te lăsa antrenat de combinatorica
deductivă a rezultatelor obţinute, în vederea obţinerii unor
noi şi noi rezultate. Desigur, de foarte multe ori această
dezvoltare a mecanismului deductiv nu este lăsată să se
efectueze la întâmplare; o anumită intuiţie a rotunjimii unei
teorii, un sentiment estetic al armoniei diferitelor
proprietăţi obţinute, al coeziunii întregului edificiu logic
care se conturează sunt tot atâtea supape prin care
matematicianul controlează consistenţa construcţiei la care
se angajează. De multe ori, aceste criterii logico-estetice
nu au dat greş, iar teoriile care li s-au conformat şi-au
demonstrat, mai devreme sau mai târziu, vitalitatea. Dar
nu e mai puţin adevărat că în alte numeroase cazuri a fost
nevoie să se suplimenteze criteriul logico-estetic cu cel al
confruntării cu situaţii particulare semnificative, din natură
sau din societate, această confruntare fiind profitabilă atât
pentru studiul situaţiilor particulare considerate, cât şi
pentru dezvoltarea într-o direcţie interesantă a teoriei
369

Solomon Marcus

matematice. După Paul Halmos, interesul unei teorii nu
poate fi dovedit în absenţa unor exemple semnificative.
Teoria matematică a deciziilor a fost stimulată de
posibilitatea abordării unor dileme reale (pentru a folosi
un exemplu românesc, ne vom referi la studiul păcii
separate cu Germania în timpul Primului Război Mondial,
studiu în care teoria deciziilor s-a dovedit a fi un
instrument exemplar), care au orientat însăşi dezvoltarea
acestei teorii. Teoria echilibrului în grafuri s-a putut aplica
în studiul evoluţiei unor situaţii conflictuale reale sau de
ficţiune (în special în domeniul folclorului, al structurilor
narative culte, al teatrului), dar în acelaşi timp exemplele
studiate au învederat necesitatea extinderii acestei teorii la
cazul în care relaţiile umane sau sociale sunt susceptibile
de mai mult decât două valori (pozitiv şi negativ).
Exemplele pot continua, dar ne vom opri numai asupra
unuia dintre ele, relativ la informatica matematică,
exemplu cu atât mai semnificativ cu cât informatica a
revoluţionat posibilităţile de prelucrare a datelor şi de
stimulare automată a proceselor intelectuale de natură
algoritmică, oferind astfel ştiinţelor omului şi societăţii
perspective pe care ele nici măcar nu le-au bănuit. Se ştie
că informatica s-a dezvoltat pe baza unei considerabile
activităţi experimentale, dar în acelaşi timp cu sprijinul
unor instrumente ale logicii matematice, aritmeticii şi
algebrei. S-au conturat astfel două tendinţe, una
370

Rãni deschise

constructivă, cu accentul pe motivaţiile legate direct de
problematica informaticii, alta axiomatică şi structurală,
corelată în primul rând cu aspectele algebrice generale ale
fundamentelor informaticii. Prima este legată de numele
unui Michael Arbib, a doua de acela al lui Samuel
Eilenberg. Prima pune accentul pe probleme şi soluţii, a
doua pe metodă. Prima e mai degrabă logico-aritmetică, a
doua e algebrică. Prima e dominată de construcţiile
algoritmice, a doua de cele generalizatoare, axiomatic
deductive. Prima este preocupată în primul rând de
motivaţiile computaţionale, a doua de cele algebrice.
A doua reflectă în primul rând nevoia matematicii de a
integra, pe cât posibil, în teoriile structurale deja existente
sau, cel puţin, de a raporta la acestea noile achiziţii ale
matematicii aplicate, prima reflectă demersul inductiv şi
constructiv al cercetătorului preocupat în primul rând de
relevanţa modelului matematic într-o problemă care nu
este numai a matematicii. Între cele două puncte de vedere
s-au consumat unele polemici, dar acum ştim că ele nu se
exclud, ci se completează, reprezentând două momente
distincte, amândouă necesare, ale demersului ştiinţific.
Un alt exemplu semnificativ din punctul de vedere al
necesităţii unui contact permanent cu lumea vie a
fenomenelor reale îl oferă teoria probabilităţilor, la care
ne-am mai referit în acest studiu. În urmă cu câteva
decenii, această teorie a fost axiomatizată cu ajutorul
371

Solomon Marcus

teoriei matematice a măsurii (A. N. Kolmogorov). Dar
intuiţiile şi problematica acestei teorii au continuat să se
alimenteze din contactul direct cu experienţa. În particular,
disciplinele umaniste au constituit un stimulent deosebit
pentru dezvoltarea acestei teorii.
S-a afirmat de multe ori, mai cu seamă de către cei
care lucrează în domeniul ştiinţelor sociale, că aceste ştiinţe
au nevoie de o matematică nouă, care abia trebuie
elaborată, deoarece matematica existentă provine în primul
rând din problematica ştiinţelor naturii. Acest deziderat este
de obicei formulat ca urmare a insuficientei relevanţe a
unora dintre modelele matematice utilizate în momentul de
faţă în economie şi în sociologie. Desigur, aşa cum am
arătat mai sus, matematica îşi îmbogăţeşte metodele ca
urmare a contactului ei mai strâns cu disciplinele sociale;
dar, în acelaşi timp, trebuie să observăm că posibilităţile
actuale ale matematicii nu au fost nici pe departe
exploatate, la dimensiunea lor reală, în investigarea
fenomenului social. Un imens potenţial interdisciplinar al
matematicii rămâne încă neutilizat. Am putea spune că
chiar la nivelul matematicii elementare, chiar la nivelul
gândirii matematice din care tehnicile matematice
propriu-zise sunt absente, se pot obţine numeroase rezultate
de o deosebită relevanţă, în aproape orice domeniu de
activitate. Poate că aici se află cele mai bogate resurse
interdisciplinare ale matematicii, dar este greu să le
372

Rãni deschise

evaluăm, tocmai pentru că sunt „nevăzute”. Ni se întâmplă
de multe ori, nouă, matematicienilor, ca în urma unei
discuţii cu un inginer, medic sau economist care ne solicită
o consultaţie, să constatăm că îi putem fi de folos nu prin
furnizarea unei tehnici matematice, ci a unui anume mod
de a ordona faptele. Matematica este o excelentă şcoală de
formare a gândirii în etape, care ordonează lucrurile în
ordinea complexităţii lor, care ordonează problemele după
dificultatea lor, care dezvoltă spiritul metodic, de distingere
a faptelor date de intuiţie şi experienţă de cele care decurg
logic din ele. Matematica dezvoltă gândirea recurentă, ne
învaţă să abordăm studiul proceselor cu o infinitate de etape
prin reducerea lor la procesele cu un număr finit de etape.
Cu ajutorul matematicii finite, ne dezvoltăm gândirea
combinatorie. Analiza matematică ne formează gândirea
procesuală, discipline ca teoria probabilităţilor şi teoria
jocurilor stimulează gândirea strategică, manipularea
factorului aleator în studiul comportamentului uman. Tot
matematica ne educă gândirea analogică, ne dezvoltă
capacitatea de a descoperi o structură comună în fenomene
aparent foarte diferite. Matematica este o şcoală a
descoperirii unor simplităţi profunde în unele fenomene
care la prima vedere copleşesc prin multitudinea de
parametri de care ele depind. Clarificările conceptuale atât
de necesare în probleme sociale, cum ar fi cele legate de
studiul nevoilor umane, al calităţii vieţii, unde circulă un
373

Solomon Marcus

mare număr de accepţiuni ale aceluiaşi termen, se obţin cu
ajutorul gândirii matematice. Dacă am şti cum anume toate
aceste potenţialităţi ale gândirii matematice pot fi făcute
efective, educate, transmise tineretului care se pregăteşte
să intre în profesie, am realiza o cotitură în sporirea
randamentului profesional, o explozie de inteligenţă
comparabilă cu ceea ce în domeniul energiilor reprezintă
valorificarea noilor surse (nucleară, solară etc.). Desigur,
uneori gândirea matematică se manifestă ca o înclinare
naturală, alteori este poate anihilată sau redusă din cauza
unei educaţii necorespunzătoare sau pur şi simplu a lipsei
de educaţie. Alteori, o persoană înzestrată cu o gândire
matematică deosebit de dezvoltată poate contribui cu idei,
sugestii, procedee surprinzătoare, chiar dacă nu are o
cultură matematică. Să evocăm, în această ordine de idei,
pe Francis Galton care, în secolul trecut, fără a fi
propriu-zis un matematician, a uimit lumea specialiştilor
prin câteva idei de pionierat, care nu au putut fi ignorate de
teoria modernă a statisticii. În acelaşi timp, matematicienii
înşişi produc uneori lucrări de o mare densitate de gândire
matematică, dar fără a utiliza matematica în sensul tehnic
al cuvântului. Ciclul de lucrări ale lui Gr. C. Moisil privind
gramatica mecanică a limbii române este semnificativ în
acest sens. Desigur, nu pledăm pentru un divorţ al gândirii
matematice de limbajul matematic modern, dar nu putem
nici eluda faptul că, de multe ori, limbajul matematic este
374

Rãni deschise

folosit în absenţa gândirii matematice, aducându-se în acest
fel mari prejudicii colaborării matematicii cu celelalte
discipline. Cea mai bună şansă a matematicii de a-şi
valorifica potenţialul ei interdisciplinar este obţinută prin
demonstrarea caracterului inevitabil pe care utilizarea ei o
are, prin eliminarea tuturor manifestărilor de intimidare a
profanilor cu ajutorul unei terminologii şi a unor simboluri
ermetice, care nu se justifică în mod natural, în raport cu
scopul urmărit. Nu rareori, reprezentările simbolice şi
formulele sunt un balast, un mod de a goni intuiţia directă,
de a epata pe profani. Matematica devine astfel un nou tip,
deosebit de periculos, de demagogie. Dar în acelaşi timp
trebuie să convingem pe specialiştii de orice fel că limbajul
matematic le poate folosi cel puţin tot atât de mult ca o
limbă străină de circulaţie internaţională, mai mult chiar,
ca cea mai importantă limbă după cea maternă. Orice
beneficiar potenţial al matematicii care nu poate înţelege şi
folosi limbajul ei, rezumându-se la preluarea acelor
rezultate care pot fi traduse în limbajul natural, va fi mereu
handicapat, nu va avea acces la articulaţiile interne ale
gândirii matematice. Iar dacă legea învăţământului
proclamă obligativitatea educaţiei matematice pentru orice
tânăr, este firesc, în prelungirea acestei situaţii, ca orice
profesionist să utilizeze efectiv un limbaj pe care l-a învăţat
timp de cel puţin zece ani.

375

