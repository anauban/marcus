Unde începe ştiinţa?
Viaţa Studenţească, anul XXX, nr. 38 (1114),
miercuri, 24 septembrie 1986, p. 2
În orice discuţie despre ştiinţă, distincţia dintre
ştiinţele naturii şi ştiinţele sociale a devenit curentă. Din
bogata literatură dedicată acestei probleme, vom reţine
acum un articol al profesorului de ştiinţe politice de la
Universitatea din Michigan, J. David Singer (Social
Science and Social Problems, Society, vol. 16, 1979, nr. 5,
pp. 49-56). În polemica sa, Singer are în vedere pe acei
autori care consideră că disciplinele sociale nu pot dobândi
statut de ştiinţă din două motive, unul referindu-se la
complexitatea prea mare a fenomenelor sociale, iar celălalt
constând în faptul că aceste fenomene nu pot fi aduse în
laborator şi supuse unor experimente controlate.
O contestaţie atât de radicală obligă, fireşte, la o
discuţie a termenilor de bază. Singer consideră că, pentru
înţelegerea naturii cercetării întreprinse de disciplinele
sociale, un rol fundamental îl au conceptele de ştiinţă,
cunoaştere, teorie şi raţionalitate. Pentru Singer, o
cercetare este ştiinţifică dacă foloseşte ipoteze explicite,
procedee repetabile, tehnici precise de observare şi
măsurare. Pentru a fi mai exacţi, trebuie să spunem că
895

Solomon Marcus

Singer se referă la „raţionamente matematice sau
statistice”, ca şi cum raţionamentul statistic n-ar fi o specie
a raţionamentului matematic. Singer recunoaşte că,
invocând procedee repetabile, observarea, măsurarea şi
raţionamentul statistic, devine aproape inevitabilă referirea
la experiment. Punctul său de controversă are în vedere
pretenţia că numai în laborator sunt posibile experimente.
Imensa cantitate de cunoaştere acumulată şi codificată de
arheologi, astronomi, etologi, geologi şi zoologi, de
exemplu, este în mare măsură de natură experimentală, dar
numai o parte relativ mică a ei a rezultat dintr-o cercetare
de laborator. O situaţie similară o prezintă cercetarea
efectuată de psihologi, sociologi, economişti şi politologi.
Concluzia lui Singer este: caracterul ştiinţific al unei
cercetări nu e determinat de locul în care ea se desfăşoară
(într-un laborator sau aiurea), ci de natura ei.
Desigur, laboratorul nu poate fi o condiţie obligatorie
pentru orice cercetare ştiinţifică. S-ar putea ca autorii (din
păcate, nenumiţi) cu care polemizează Singer să aibă în
vedere o idee de laborator mai largă decât aceea
considerată în ştiinţele naturii, dar chiar şi în acest caz este
vorba, fără îndoială, de o cercetare experimentală, deci de
o cercetare în care experienţele sunt provocate, observate
şi controlate de către cercetător. Conştient de faptul că în
domeniul social este mai dificilă provocarea unor
experienţe (de aceea se şi foloseşte calificativul
896

Rãni deschise

experienţial pentru a distinge natura multor experienţe
sociale de aceea experimentală a multor experienţe din
ştiinţele naturii). Singer defineşte caracterul ştiinţific al
unei cercetări prin condiţii care nu mai cer prezenţa
obligatorie a unor experimente. În schimb însă, se pun alte
condiţii restrictive, de natură metodologică, în special
folosirea instrumentului matematic pentru prelucrarea şi
interpretarea rezultatelor. În afară de aceste condiţii
formulate explicit, mai intervin în formularea lui Singer şi
alte restricţii, de natură mai mult sau mai puţin implicită.
Este vorba de o supunere totală la obiect, în încercarea de
a-l descoperi cât mai bine. Obiectul preexistă celui care-l
caracterizează. Totul începe cu observarea sa atentă şi cu
efectuarea unor măsurători privind datele observate.
Urmează un proces de inducţie şi de generalizare, de
interpretare a rezultatelor. Nu se poate nega că aceasta este
structura multor activităţi de cercetare ştiinţifică, în
botanică sau în zoologie, de exemplu. Naturalistul suedez
din secolul al XVIII-lea Carl von Linné este, prin
cercetările sale taxinomice privind plantele şi animalele,
un exemplu tipic în această privinţă.
Dacă autorii cu care Singer polemizează se reclamă
evident (nu ştiu dacă şi explicit) de la viziunea empirică a
lui Francis Bacon, care preconiza o cercetare ştiinţifică
bazată pe observaţie şi experiment, nici Singer nu se
depărtează prea mult de această concepţie; reţine
897

Solomon Marcus

observaţia, dar abandonează obligativitatea experimentului.
Prin aceasta, Singer nu face decât să-l adapteze pe Bacon
la specificul fenomenelor sociale. În plus, îl şi
modernizează, impunând exigenţele de precizie a
mijloacelor de observare şi măsurare (ceea ce presupune a
fi la pas cu tehnica modernă), de prelucrare şi interpretare
matematică a rezultatelor (ceea ce presupune luarea în
considerare a nivelului atins de matematică). Să observăm
că această ultimă condiţie îl îndepărtează numai parţial pe
Singer de Bacon, deoarece metoda deductivă, respinsă de
Bacon, apare la Singer nu numai în partea fundamentală,
de concepţie, ci şi în faza finală, de interpretare.
Pare stranie, în această ordine de idei, condiţia iniţială
din formularea lui Singer, stipulând folosirea unor ipoteze
explicite.
Această condiţie apare ca un corp străin în contextul
celorlalte condiţii. Explicitarea ipotezelor ţine de o altă
mentalitate, care se asociază cu metoda modelării şi în care
cercetarea ştiinţifică include şi acte de invenţie, nu numai
de descoperire. Avem în vedere folosirea metodelor
ipotetic-deductive, modele care trebuie să treacă examenul
unei anumite capacităţi explicative a fenomenelor studiate.
Dacă trebuie să căutăm printre clasici un reprezentant al
acestei atitudini, l-am alege pe Johannes Kepler, care a
elaborat modelul orbitelor eliptice ale planetelor.
Aproximarea fenomenelor prin modele explicative de
898

Rãni deschise

natură ipotetic-deductivă este una dintre cele mai puternice
metode folosite azi în ştiinţele sociale şi este curios că
Singer nu se referă la ea, cu atât mai mult cu cât el
subliniază faptul că scopul principal al cercetării ştiinţifice
nu este de a prevedea, ci de a explica.
Dar nu cumva metoda modelării ar putea fi
considerată un caz particular al celei preconizate de
Singer? Răspunsul este categoric negativ. Într-un model
matematic, componenta formală intervine în partea
iniţială, de concepţie, în timp ce rezultatele observaţiei
servesc fie pentru motivarea modelului, fie pentru testarea
capacităţii sale explicative. La Singer, dimpotrivă, partea
matematică intervine a posteriori, nu în elaborarea
rezultatelor, ci numai în interpretarea lor.
O condiţie pentru ca informaţia să devină cunoaştere
ştiinţifică este, pentru Singer, proprietatea ei de a fi
comunicată, deci reprezentată cu ajutorul unor simboluri
verbale, numerice, grafice sau electronice. Comunicabilitatea unei informaţii asigură posibilitatea testării ei
de către diverşi specialişti şi eventuala ei acceptare în
corpul cunoaşterii ştiinţifice; acest corp al cunoaşterii
validate constituie pentru Singer o teorie. Într-o altă
formulare, teoria este pentru el un corp de propoziţii care
oferă o explicaţie credibilă a unor fenomene observabile,
sunt logic compatibile, ansamblul lor fiind consistent cu
orice altă cunoaştere relevantă; în plus, sunt formulate
899

Solomon Marcus

într-un limbaj controlabil şi, în cea mai mare parte, deja
testate cu succes. Faţă de o atare înţelegere a teoriei, Singer
consideră ca ştiinţele sociale au la activul lor doar un număr
redus de performanţe teoretice. Dar, adăugăm noi, modul
său de a concepe teoria nu este lipsit de unele echivocuri şi
obscurităţi. Teoria sa are ceva dintr-un model şi ceva
dintr-o teorie, lipsindu-i câte ceva din fiecare dintre ele.

900

