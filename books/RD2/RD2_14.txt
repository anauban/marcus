Ştiinţa în afara culturii?
Era socialistă, 1 noiembrie 1973
Una din caracteristicile epocii noastre este creşterea
ponderii pe care o are ştiinţa în toate sferele de activitate
umană şi socială. Ca matematician, am fost impresionat de
tirajul deosebit de mare pe care îl are Gazeta matematică,
seria B (dedicată elevilor), de popularitatea de care se
bucură această revistă printre elevi. Cărţile de ştiinţă dispar
repede din librării. În multe locuri din ţară, elevii editează
reviste ştiinţifice. Am văzut câteva numere din Revista
matematică a elevilor din Timişoara, în care probleme
solicitând perspicacitatea alternează cu prezentarea unor
noţiuni de matematică modernă, a unor crâmpeie din viaţa
unor mari matematicieni, cu glume şi cu vorbe de spirit.
Am văzut revista elevilor de la Liceul „Nicolae Bălcescu”
din Capitală, o revistă în care elevii tratează probleme
dintre cele mai actuale, cum ar fi colaborarea
interdisciplinară. Am participat în primăvara acestui an la
o sesiune ştiinţifică a elevilor din Ploieşti, unde un elev a
prezentat un proiect de minicalculator. Îmi este familiară
activitatea ştiinţifică a studenţilor. Dacă în trecut cercurile
ştiinţifice studenţeşti cuprindeau doar o parte foarte mică
a studenţilor, astăzi ele sunt frecventate activ de către un
130

Rãni deschise

mare număr de studenţi. La Facultatea de MatematicăMecanică a Universităţii din Bucureşti, anul al V-lea
(format prin selecţionarea studenţilor care au terminat
primii patru ani cu medii foarte mari) este în întregimea sa
un adevărat cerc studenţesc, alcătuit din peste 50 de
studenţi, fiecare dintre ei formând obiectul preocupării
personale a unui cadru didactic.
Nu pledez pentru o cultură unilaterală, exclusiv
ştiinţifică. În consens cu documentele partidului nostru,
înţeleg necesitatea unei dezvoltări armonioase a
personalităţii umane, dezvoltare care nu putea fi
concepută în vechea orânduire. Reorganizarea recentă a
învăţământului general obligatoriu, eşalonat pe o
perioadă de zece ani şi alcătuit din ciclul primar, ciclul
gimnazial şi prima treaptă a liceului, îşi propune să
asigure, într-o concepţie unitară şi echilibrată, pregătirea
ştiinţifică şi umanistă, tehnică şi practică a tinerei
generaţii. Rămâne însă de văzut dacă acest echilibru este
realmente avut în vedere de către toţi factorii de cultură
din ţara noastră.
Milităm pentru un proces de educaţie permanentă,
pentru formarea unor oameni care, de-a lungul întregii lor
vieţi, sunt animaţi de dorinţa, de curiozitatea de a înţelege
fenomenele din jur, sunt apţi de acea reînnoire morală şi
intelectuală fără de care suntem repede depăşiţi în
profesiune ca şi în viaţa cetăţenească. Nu ne poate fi deci
131

Solomon Marcus

indiferent modul în care diferitele modalităţi de dezvoltare
a culturii în ţara noastră îşi conjugă acţiunea, pornind de
la aceeaşi concepţie unitară şi echilibrată, în cadrul căreia
diferitele componente ale culturii se completează reciproc.
Orice perturbare a echilibrului dintre aceste componente,
dintre componenta naţională şi cea general umană, dintre
componenta politico-ideologică şi cea moral-cetăţenească,
dintre componenta teoretică şi cea practică, dintre cultura
filosofică şi cea de specialitate, dintre cultura tehnicoştiinţifică şi cea literar-artistică, riscă să frâneze procesul
de împlinire a unei societăţi socialiste multilateral
dezvoltate, societate care are nevoie de oameni la fel de
multilateral dezvoltaţi, cu antene sensibile la tot ceea ce
exprimă capacitatea creatoare a personalităţii umane.
Publicaţiile noastre au adus de multe ori în discuţie
probleme privind raporturile dintre diferitele componente
ale culturii socialiste. Dezbaterile pe probleme de etică şi
echitate socialistă, de corelare a teoriei cu practica, de
împletire a tradiţiilor naţionale cu tot ceea ce este mai
valoros în cultura altor popoare se inserează, toate, în
această grijă pentru o dezvoltare concomitentă a tuturor
aspectelor pe care le prezintă cultura unui popor.
Fruntaşi ai vieţii noastre artistice şi literare şi-au
manifestat de multe ori îngrijorarea faţă de posibilitatea
unor roboţi intelectuali, capabili să mânuiască maşini şi
aparate de înaltă tehnicitate, dar insensibili la frumuseţea
132

Rãni deschise

artei. Ei au insistat, pe drept cuvânt, asupra necesităţii
de a nu se neglija educaţia artistică a cetăţenilor,
indiferent de profesiunea căreia ei se dedică. Îmi aduc
aminte cu emoţie de intervenţiile, în acest sens, ale
acelui cărturar prestigios care a fost Tudor Vianu. În
jurul anilor 1960, când se discuta, la Universitate,
organizarea unor cursuri interdisciplinare de aplicare a
matematicii în lingvistică, Tudor Vianu atrăgea atenţia
asupra necesităţii ca cercetarea limbii şi literaturii să-şi
păstreze nucleul ei umanist, oricât de mult ar fi
modificată prin „metode exacte”.
Însă problema corelării diferitelor laturi ale culturii
mai are şi un alt aspect, asupra căruia aş dori să mă opresc
în rândurile care urmează. Acest aspect se referă la
ponderea pe care cultura ştiinţifică o are în ansamblul
culturii noastre. Corespunde ea oare ponderii reale pe care
o are ştiinţa în societatea contemporană?
După cum se ştie, procesul de diferenţiere a
disciplinelor a durat multe veacuri, unele ştiinţe şi arte şiau câştigat târziu un statut autonom. Nu întâmplător aceiaşi
cugetători erau, în acelaşi timp, filosofi, oameni de ştiinţă
şi chiar artişti sau scriitori. Aristotel era filosof, logician,
psiholog şi zoolog; Leonardo da Vinci era pictor, sculptor,
arhitect, mecanician, fizician, astronom, botanist,
anatomist şi inginer; Descartes era filosof, matematician
şi fizician; Pascal era matematician, fizician, scriitor şi
133

Solomon Marcus

filosof; Lomonosov era fizician, geolog, teoretician al
literaturii, lingvist şi poet.
La originile culturii umane se află o deplină simbioză
a ceea ce numim azi modalitatea ştiinţifică, modalitatea
artistică şi modalitatea filosofică de cunoaştere a lumii.
Abia ultimele secole au adus diferenţierea pregnantă a
acestor modalităţi. Ţara noastră a cunoscut o dezvoltare
capitalistă foarte târzie, ultimele urme ale feudalismului
fiind înlăturate abia după cel de-al Doilea Război Mondial.
Aşa se face că dezvoltarea ştiinţei româneşti a fost mult
întârziată faţă de dezvoltarea celorlalte domenii ale
culturii. Într-o ţară „eminamente agricolă”, cu o clasă
stăpânitoare formată în mare parte din moşieri, într-o ţară
în care nevoia acută a industrializării era în mod sistematic
escamotată, nici progresul ştiinţei şi tehnicii nu putea găsi
o bază solidă de dezvoltare. Aşa se face că noi am moştenit
de la vechea orânduire o cultură lipsită de echilibrul
necesar între compartimentele ei, o cultură care era
apanajul unei minorităţi, o cultură care punea accentul pe
anumite valori „imuabile”, filosofice sau artistice, dar în
cadrul căreia marile cuceriri ale ştiinţei nu prea îşi găseau
loc. Am moştenit o mentalitate conform căreia a fi un om
cult înseamnă doar a şti ceva despre dialogurile lui Platon,
despre lucrurile în sine ale lui Kant, desprea teatrul lui
Shakespeare, despre poeziile lui Eminescu, despre
sculptura lui Michelangelo şi despre muzica lui Beethoven,
134

Rãni deschise

dar nu şi despre teoria lui Darwin sau despre numerele
transfinite ale lui Cantor. Am moştenit o mentalitate
conform căreia putea fi ruşinos ca, într-o discuţie de
societate, să se constate că ignor proza lui Marcel Proust,
dar nu era deloc jenant să ignor ideile de bază ale
geometriilor neeuclidiene sau raportul dintre mecanica
clasică şi cea relativistă.
Mai e oare nevoie să argumentăm cât de anacronică
a devenit o astfel de mentalitate? Mai e oare nevoie să
pledăm pentru valoarea culturală a marilor idei din ştiinţa
contemporană? Mai poate fi azi considerată ştiinţa ca un
simplu corp de cunoştinţe de specialitate, de care au a se
interesa doar cei care o profesează?
Concomitent cu procesul de diferenţiere şi
specializare din ce în ce mai mare a disciplinelor –
ştiinţifice sau artistice –, asistăm, în ultimele decenii, la
un proces de colaborare din ce în ce mai intensă între
discipline dintre cele mai deosebite. În aceste condiţii, au
luat naştere o serie de domenii pluridisciplinare care
stimulează circulaţia ideilor ştiinţifice, transformând
multe noţiuni şi rezultate până mai ieri „de strictă
specialitate”, în elemente de cultură generală de care
nicio inteligenţă contemporană nu se poate dispensa.
Noţiuni ca „algoritm” sau „entropie” erau până mai ieri
într-atât „de strictă specialitate”, încât cuvântul
„algoritm” nici nu figura în Dicţionarul limbii române
135

Solomon Marcus

moderne, apărut în 1958, iar cuvântul „entropie” figura,
în acelaşi dicţionar, doar cu semnificaţia sa din fizică.
Astăzi însă aceste noţiuni au contaminat toate domeniile
şi niciun om cult nu se mai poate lipsi de ele, deoarece
construcţiile algoritmice intervin în orice proces
susceptibil de a fi programat la un calculator electronic,
iar procesele entropice apar pretutindeni în universul
fizic, ca şi în cel informaţional.
Prin această tendinţă de integrare a disciplinelor, se
creează deci o varietate din ce în ce mai bogată de idei şi
metode ştiinţifice care se situează la întâlnirea mai multor
drumuri ale cunoaşterii şi care, chiar prin acest fapt, sunt
implicate în probleme dintre cele mai diverse, solicitând
atenţia multor categorii de specialişti. Acest „numitor
comun” al ştiinţelor pretinde cu insistenţă să fie cunoscut
de către toţi membrii unei societăţi care vor să înţeleagă
universul în care trăiesc şi să-l transforme, în mod
deliberat, într-un sens dinainte stabilit. Tocmai aceasta este
ipostaza în care ne aflăm. Marile transformări pe care
le-am efectuat sau le proconizăm în continuare, în
economie, în întreaga viaţă socială, sunt prelungirea
firească a unor procese naturale de evoluţie şi progres,
procese care pot fi înţelese şi stimulate numai pe baza unei
educaţii şi culturi ştiinţifice, pe baza unei inteligenţe
înarmate cu toate strategiile pe care ştiinţa le-a elaborat în
lunga ei confruntare cu secretele naturii.
136

Rãni deschise

Am amintit toate aceste adevăruri nu pentru că ele nu
ar fi cunoscute, ci pentru că nu toate instituţiile noastre de
cultură desprind din ele consecinţele necesare. Nu este
oare de natură să genereze o anumită dezorientare în
mintea atâtor tineri, elevi sau studenţi, faptul că ei constată,
pe de o parte, că învăţământul este orientat în special spre
însuşirea ştiinţei, în timp ce, pe de altă parte, atâtea reviste
care se intitulează „de cultură” fie că nu acordă ştiinţei
niciun loc, fie că-i acordă un loc cu totul meschin? Mă
număr printre cei care gustă dezbaterile pe teme literare şi
le acordă însemnătatea cuvenită. Dar aceasta nu mă
împiedică să mă întreb dacă se poate susţine cu argumente
serioase ideea că problema criticii literare este problema
numărul unu a culturii româneşti, aşa cum ar rezulta din
locul pe care ea îl ocupă în multe publicaţii culturale (nu
exclusiv literare!). Oare nu s-a observat că rubricile
ştiinţifice, şi aşa debile, din unele reviste de cultură, au
dispărut cu totul? Că apariţiile editoriale în domeniul
ştiinţific pendulează de foarte multe ori între cărţi foarte
specializate şi cărţi de popularizare din care cu greu se
poate afla mai precis depre ce este vorba, neglijându-se
acele cărţi care, fără a fi scrise într-un jargon prea special,
îşi propun totuşi să dea o idee reală şi o perspectivă
autentică despre o anumită direcţie de cercetare ştiinţifică,
să conducă pe cititor la formarea treptată a unor noi moduri
de înţelegere şi abordare intelectuală şi practică a unor
137

Solomon Marcus

aspecte din ce în ce mai ascunse şi, poate, mai paradoxale
ale lucrurilor?
Desigur, nu este rău că se scriu cărţi care tratează despre
implicaţiile filozofice ale unei noi ramuri a ştiinţei, despre
viaţa şi activitatea celor care au creat-o, dar acestea nu pot
înlocui cărţile în care asemenea noi ramuri sunt efectiv
prezentate, cu o deschidere cât mai largă către cei al căror
interes ar putea fi captat. Astfel de cărţi ar avea o mare valoare
formativă, ele ar dezvolta, cu alte mijloace decât manualele
şcolare sau universitare, procesul de educaţie permanentă în
direcţia căruia învăţământul nostru desfăşoară un efort imens,
de formare a unor capacităţi de învăţare, deci de desprindere
a unor metode şi semnificaţii, a unor ierarhii şi finalităţi, în
dauna simplei inventarieri de obiecte, de date şi de proprietăţi.
Dar chiar şi cărţile care apar, câte reviste de cultură
le înregistrează? În 1972 a apărut, sub titlul Matematică şi
viaţă, o culegere de articole ale lui S. Stoilow, unul dintre
cei mai mari matematicieni pe care i-a dat poporul român.
Nu-mi amintesc să fi întâlnit măcar o singură consemnare
a acestei cărţi de o mare valoare etică, ştiinţifică şi
filosofică. Recentul centenar al altor doi mari
matematicieni, Dimitrie Pompeiu şi Gheorghe Ţiţeica, a
trecut şi el aproape neobservat. Se poate oare crede că nu
este încă aptă cultura românească să asimileze aceste valori
ştiinţifice, să şi le încorporeze, alături de marile sale valori
filosofice, literare şi artistice?
138

Rãni deschise

Trebuie să recunoaştem că oamenii de ştiinţă au şi ei
partea lor de răspundere în crearea situaţiei de mai sus.
Unii dintre ei ies rar în arena socială sau nu fac efortul
necesar pentru crearea unui interes public în jurul marilor
idei şi marilor personalităţi ştiinţifice. Dacă fiecare om de
ştiinţă ar rupe măcar o mică parte din timpul său pentru
a-l dedica acestor preocupări, s-ar înregistra un progres
important în ceea ce priveşte apropierea ştiinţei de public.
Dar aceasta nu este suficient. Se pare că există o
neconcordanţă între numărul mare de activişti culturali de
formaţie filosofico-literar-artistică, pe de o parte, şi
numărul foarte mic de activişti culturali de formaţie
ştiinţifică, pe de altă parte.
Nu am făcut statistica, dar este foarte puternică
impresia că zdrobitoarea majoritate a redactorilor de la
Radio şi Televiziune, ai editurilor şi revistelor de cultură,
precum şi din alte sectoare de activitate culturală provin
din prima categorie de mai sus. În aceste condiţii, multe
sarcini privind educaţia ştiinţifică permanentă sunt
încredinţate unor activişti care nu au o pregătire
corespunzătoare şi cele mai bune intenţii se volatilizează.
Rubrici iniţial ştiinţifice se metamorfozează până la
dispariţie (a se vedea situaţia revistei Contemporanul), o
emisiune radiofonică de tipul Viaţa cărţilor, care ar trebui
să se refere, în mod echilibrat, la diferitele categorii de
cărţi, este de fapt o emisiune a cărţilor literar-artistice şi,
139

Solomon Marcus

rareori a cărţii filosofice; iar o revistă ca Săptămâna
culturală a Capitalei este de fapt „Săptămâna
literar-artistică a Capitalei”.
Am mai scris despre această situaţie, credeam atunci
că la originea ei sunt simple neglijenţe. Am constatat însă,
observând unele reacţii, că este vorba de o mentalitate a
unor activişti culturali care consideră, pur şi simplu, că
ştiinţa nu face parte din cultură. Exemplele, în acest sens,
sunt din păcate numeroase. Dar nu este oare cazul să ne
întrebăm cum se conjugă o atare atitudine cu efortul uriaş
pe care alţi factori de cultură (cum ar fi învăţământul) îl
desfăşoară tocmai în direcţia unei educaţii şi culturi în
cadrul cărora gândirea ştiinţifică ocupă un loc de bază?
Cum se articulează o atare mentalitate cu mentalitatea
dominantă a societăţii noastre, conform căreia numai
folosind toate cuceririle importante ale gândirii ştiinţifice
contemporane putem înainta cu succes în construirea unei
vieţi îmbelşugate?
Nu mă voi ocupa aici de unele rezistenţe mai subtile
la promovarea unei gândiri ştiinţifice, de unele căi
artificiale de a opune ştiinţa filosofiei sau artelor, de a
se contesta posibilitatea unor interferenţe ale acestor
modalităţi de cunoaştere, invocându-se, de exemplu,
pericolul alterării purităţii artei. Mă voi opri aici, nu însă
fără a-mi permite să sugerez: necesitatea recrutării unui
mai mare număr de activişti culturali din rândul
140

Rãni deschise

absolvenţilor unor facultăţi cu profil ştiinţific;
necesitatea unui accent mai mare pe educaţia ştiinţifică
autentică, în dauna unor simple prezentări de rezultate
spectaculoase, dar lipsite de o valoare formativă;
necesitatea stabilirii unui echilibru, în cadrul
publicaţiilor noastre culturale, al emisiunilor de radio şi
televiziune, în cadrul tuturor sectoarelor de activitate
culturală, între cultura ştiinţifică şi aceea literar-artistică;
necesitatea integrării ştiinţei ca o componentă
fundamentală a culturii noastre.

141

