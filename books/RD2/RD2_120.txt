Cărţi destin
Opinia studenţească, organ al Consiliului U.A.S.C.
din Centrul Universitar, Iaşi nr. 3-4 (92-93),
anul XIII, 1986, p. 18
Există, în literatura noastră (sau universală), un
oarecare text (poezie, proză, filozofie, eseistică, cugetare
sapienţială, istorie…) căruia să-i atribuiţi o semnificaţie
cu totul deosebită din punct de vedere estetic, etic ori
mintal, care să vă fi determinat vocaţia ori păşirea pe o
anume cale a vieţii?
Dacă un asemenea text hotărâtor pentru
desfăşurarea dvs. existenţială ori ideatică există, sunteţi
rugat a-l preciza, arătând motivele pentru care a putut
juca un rol însemnat în viaţa dvs. sau de ce îi atribuiţi o
valoare excepţională, de nu şi unică. A existat pentru dvs.
un asemenea text-cheie, text în care să vă fi regăsit, care
să vă fi luminat calea vieţii, care să vă fi pus brusc în
rezonanţă cu lumea care să vă reprezinte?
Adolescenţa şi tinereţea mi-au fost marcate de un text
al lui Rainer Maria Rilke: Scrisori către un tânăr poet. Am
în vedere mai ales acea scrisoare în care Rilke îl sfătuieşte
pe tânărul său corespondent să nu persiste în încercările
sale poetice decât dacă simte că nu ar putea trăi altfel.
862

Rãni deschise

Cunoşteam poezia lui Rilke, tocmai atunci apăruse o
culegere de traduceri din Buch der Blider, pe care o
urmăream paralel cu originalul german; simţeam că Rilke
făcea exact ceea ce îi spunea acelui tânăr să facă. Într-un
moment în care eram ispitit de tot felul de preocupări,
reflecţiile lui Rilke, pătrunse de gravitate şi profunzime,
au constituit pentru mine o revelaţie şi mi-au dat bucuria
descoperirii unei adevărate busole a vieţii.
Am interpretat însă atitudinea sa într-un mod mai
general: criteriul suprem în alegerea unei activităţi
creatoare este imposibilitatea de a trăi în afara ei. Să faci
acel lucru care te cheamă cu o forţă acaparatoare,
exclusivistă, care nu-ţi oferă nicio alternativă; acel lucru
căruia simţi că eşti capabil să i te dăruieşti cu întreaga ta
vitalitate, cu întreaga ta inteligenţă şi unde randamentul
tău este optim, spre propriu-ţi folos şi al celor din jur.
În căutarea şi aflarea acestui lucru mă străduiesc să-i
ajut pe tinerii de azi, investind în acest scop experienţa pe
care am acumulat-o în patruzeci de ani de activitate
universitară.
Cuvintele lui Rilke m-au urmărit în fiecare etapă a
vieţii. Dacă, uneori, împrejurări vitrege m-au abătut de la
această cale, am suferit, dar nu am cedat.

863

