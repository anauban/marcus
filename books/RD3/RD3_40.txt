Inteligența – o nouă relație complexă
între om și mașină
Contemporanul, nr. 44 (1825),
30 octombrie 1981, pp. 6‑7
(Răspund: Mihai Drăgănescu, Constantin Arseni,
Solomon Marcus)
Redacția: Atunci când vorbim de revoluția
tehnico‑științifică, înțelegem un întreit salt calitativ
realizat în sfera cunoașterii și a creației, în întreaga
viață socială – știința devenind o importantă forță de
producție – în investigația filozofică, neobosită încercare
a omului de a‑și înțelege locul și rolul în realitatea
naturii și a societății, în univers. O demonstrează, printre
altele, rodnicele și numeroasele discuții purtate în jurul
relației dintre inteligența naturală și cea artificială, care
au reunit specialiști din cele mai diferite domenii, într‑un
efort comun de a depăși inerentele limite ale actualului
stadiu de evoluție a științei, de a deschide noi perspective
cunoașterii, sporii eficienței ei sociale.
În acest context ne par mai viabile ca oricând
cuvintele lui Marx care scria: „Cu timpul, științele
naturii vor îngloba în ele știința despre om, după cum
243

Solomon Marcus

știința despre om va îngloba în sine științele naturii, va
fi o singură știință.” Conexiunile dintre diferite științe,
dintre știință și filozofie se stabilesc tot mai pregnant,
spre câștigul fiecăreia în parte, în sistemul de principii
ale materialismului dialectic și istoric, care deschide un
larg evantai de posibilități pentru armonizarea teoriei
științifice și filozofice cu practica socială. Iar faptul că
nu întotdeauna realizările ating nivelul dorințelor și
aspirațiilor este, credem, și un semn încurajator deoarece
decalajul dintre realitate și ideal, tensiunea dintre
ele, dinamizează strădaniile omului spre cunoaștere
și înfăptuire, subordonate în chip necesar unor valori
umaniste.
Comunicarea dintre om și mașinile inteligente
este, în esență, un dialog al omului cu sine însuși
mediat de una dintre cele mai strălucite realizări ale
gândirii și creației sale; sensul acestei comunicări
este acela al perfectibilității; finalitatea – ameliorarea
realității umane. Desfășurată, în principal, în orizontul
științei și al tehnicii, acest gen de comunicare are
implicații esențiale în cel al filozofiei actuale. Rodnicia
dialogului la care ne referim solicită, cu necesitate,
interdisciplinaritatea, ce poate înmulți, astfel, premisele
de dezvoltare ale fiecăruia în parte și ale tuturor. Acesta
este imperativul pe care îl menține în fața savanților
și a specialiștilor cu cele mai variate preocupări cea
244

Rãni deschise

de‑a doua revoluție industrială, prefațată de actuala
revoluție științifico‑tehnică, în interiorul căreia
microelectronica, informatica, cibernetica au un rol
determinant. Posibilitățile teoretice și practice ale
dezvoltării acestor domenii – cele care s‑au transformat
deja în realizări, cele care mai stau sub semnul „viitorilor
dezirabili” – au concentrat intervențiile participanților
la simpozionul Inteligența artificială și robotica –
matematicieni, informaticieni, lingviști, psihologi,
medici, filozofi, reflectare a stadiului înalt atins de
cercetarea fundamentală și aplicativă românească din
acest sector prioritar al științei contemporane. În aceste
pagini publicăm textele prescurtate a trei dintre cele
mai interesante comunicări susținute cu acest prilej.
Autorii lor, oameni de știință de prestigiu, detaliază unele
aspecte importante ale impactului științei și tehnicii
contemporane în viața socială, conturează noi tendințe în
investigația științifică și filozofică.
Solomon Marcus: Statutul limbajelor artificiale
Așa cum a fost operată în lingvistică distincția
limbă‑limbaj, aceasta nu a avut în vedere sistemele
artificiale de comunicare. Limbajul a fost conceput
drept o anumită facultate, o aptitudine umană, în timp
ce limbile sunt realizări ale acestei aptitudini. Iată însă
că prin termenul limbaje artificiale desemnăm sisteme
245

Solomon Marcus

de comunicare de tipul limbajelor de programare, al
limbajelor logice, al limbajelor de comunicare cosmică
(LINCOS) etc. În fapt, în domeniul sistemelor de
comunicare umană, altele decât limbajul articulat,
problema operării unei distincții de genul limbă‑limbaj
nu numai că nu a fost încă rezolvată, dar nici măcar nu
a fost încă formulată explicit. Ar trebui, de exemplu,
să urcăm de la limbajele de programare particulare,
existente, la trăsăturile comune lor și celor care ar
putea fi create ulterior, dar se pare că teoria limbajelor
de programare nu este încă suficient de avansată pentru
a face față acestei investigații. În aceste condiții vom
folosi termenii de limbă și limbaj în mod alternativ,
conformându‑ne terminologiei în circulație, fără nicio
pretenție de rigoare.
Statutul limbajelor artificiale trebuie, deci, clarificat
în raport cu cel al altor câtorva tipuri fundamentale de
limbaje, cum ar fi: limbile naturale, limbajele umane,
limbajele formale, limbajul genetic etc.
Vom considera ca incluse în limbajul uman atât
limbajul articulat, cât și limbajele artificiale de tipul
calculului propozițional cu n variabile sau al Algolului.
Aici trebuie însă operate noi distincții. Limbajul
calculului propozițional (ca și limbajul calculului cu
predicate) este un limbaj formal, adică un limbaj cu
statutul unui sistem formal în sensul logicii matematice
246

Rãni deschise

(Hilbert). Limbajele formale se opun limbilor naturale
prin mai multe trăsături, dintre care cea mai importantă
ni se pare următoarea: într‑un limbaj cum este cel al
calculului propozițional noțiunea de secvență corect
formată este riguros definită, în timp ce într‑o limbă
naturală nu se poate da o condiție suficientă pentru ca o
secvență oarecare să fie corect alcătuită.
Nu orice limbaj uman artificial este un limbaj
formal. Deosebit de relevantă este, în această privință,
situația limbajelor computaționale, mai precis, a
limbajelor de programare. În mod aproape spontan,
aceste limbaje au fost considerate, la apariția lor, limbaje
formale și, probabil, nu sunt puțini cei care le consideră
și acum așa, ca urmare a unei tratări superficiale a acestei
chestiuni. Pe măsură ce s‑a dezvoltat pragmatica acestor
limbaje, adică relația lor cu programatorii, a devenit tot
mai clar statutul intermediar al acestor limbaje, care prin
anumite trăsături se așază în apropierea limbilor naturale,
în timp ce prin altele intră în categoria limbajelor
formale.
Ce se întâmplă cu domeniul limbajelor artificiale,
altele decât cele computaționale? Aici intră multe
limbaje logice și multe coduri, dar nu și un limbaj cum
este cel matematic. Acesta din urmă are o componentă
naturală și una artificială (alcătuită din simboluri și
formule). Relația dintre aceste componente are ceva
247

Solomon Marcus

comun cu fenomenele de contact dintre două idiomuri,
mult studiate în lingvistică. Componența naturală
prevalează de obicei asupra celei artificiale. Este aceasta
din urmă de natură pur formală? Este greu, deocamdată,
de răspuns. Probleme similare apar și în structura
eterogenă a limbajului folosit în chimie. Cercetările
asupra limbajelor științifice s‑au concentrat mult asupra
aspectelor de vocabular și de notație, neglijându‑se
problemele de sintaxă și de semantică. În ceea ce
privește modul de articulare a sintaxei componentei
naturale cu sintaxa componentei artificiale, nu s‑a
întreprins încă nicio cercetare sistematică.
Limbile naturale, în ciuda aparenței lor de libertate
totală, se supun unor restricții puternice, dar ascunse. Un
exemplu în acest sens este restricția pusă în evidență de
Victor Yngve, care limitează la șapte lungimea oricărei
structuri regresive (de tipul „foarte frumoasă carte”) pe
care o poate manipula creierul omenesc. Este interesant
faptul că același număr șapte apare și în lucrarea
Inteligența socială a profesorului Mihai Drăgănescu,
substratul comun fiind probabil legat de fenomenul
psihologic discutat de George A. Miller (The magical
number seven), în urmă cu vreo 30 de ani. O atare
restricție acționează și în limbajele umane artificiale?
Avem în vedere, firește, relația acestor limbaje nu cu
memoria calculatorului, ci cu aceea a omului. Ar trebui
248

Rãni deschise

însă mai întâi elucidat în ce măsură se poate vorbi despre
o relație de dependență sintactică, pe baza căreia să
capete sens structurile regresive (cu dependențe de la
stânga la dreapta) în limbajele artificiale.
S‑a emis ipoteza (R. Montague) că nu există nicio
deosebire structurală esențială între limbile naturale
și cele artificiale formale, și unele și altele admițând
modele matematice de același tip, în sensul că, în
măsura în care devin obiect riguros de studiu, limbile
naturale sunt de natură formală (titlul unui articol al lui
R. Montague este Engleza ca limbaj formal). Aceasta ar
duce la o competență lingvistică incluzând atât naturalul
cât și artificialul. Spre deosebire de competență, care
se referă numai la generarea secvențelor corecte,
performanța lingvistică manipulează multe secvențe
semigramaticale. În acest sens, după cum observa și
profesorul Constantin Arseni într‑o altă ordine de idei,
performanța poate întrece competența.

249

