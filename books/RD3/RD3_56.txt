În urmă cu șapte mii de ani
Viața studențească, anul XXXI, nr. 36 (1164),
9 septembrie 1987, p. 2
Antropolog, membru al Academiei Naționale de
Medicină din Mexic, laureat al unui premiu internațional
pentru pace (Spania, 1968), Santiago Genovés a
prezentat la colocviul UNESCO de la Veneția, din
1986, un raport orientat spre integrarea științei cu
umanismul (Le vol humain; la science, la technologie et
l’humanisme, leur évolution, leur chronologie et leurs
rapports avec la culture, în volumul Colloque de Venise.
La science face aux confins de la connaissance: le
prologue de notre passé culturel, UNESCO, Paris, 1987,
pp. 109‑113), ale cărui idei principale vom căuta să le
desprindem în cele ce urmează.
Genovés consideră că gândirea științifică se
sprijină pe următoarele patru principii: validitate
(rezultatele sunt valabile sau nu după cum ipoteza de
plecare este sau nu bună); reproductibilitate (observatori
diferiți pot ajunge la aceleași rezultate); posibilitatea
verificării intersubiective (susținătorii și adversarii unei
teze cad de acord asupra criteriilor de verificare); dintre
344

Rãni deschise

două explicații la fel de convingătoare o preferăm pe
cea mai simplă (legea lui Occam). Ghidat de aceste
principii, D.R. Hofstadter (Gödel, Escher, Bach. An
eternal golden Braid, Basic Books, New York, 1979) a
identificat o analogie profundă între muzica lui Bach,
litografiile lui Escher și teorema de indecidabilitate a
lui Gödel. Genovés crede că știința, dacă vrea să nu
aibă frontiere, trebuie să adopte o abordare integrală,
fără să abandoneze, însă, specializarea, care este umană
și normală. În același timp, știința, pentru a‑și păstra și
dezvolta capacitatea imaginativă, trebuie să țină legătura
cu domeniile umaniste, cu zborurile lor în imaginar.
Genovés schițează istoria planetei noastre, modul
în care au apărut peștii, amfibiile, reptilele, păsările,
mamiferele, primatele, oamenii. De la primate la om
trecerea a fost realizată prin unealtă. Noi am ajuns
la verticalitate prin interacțiunea creierului, mâinilor
și uneltelor. (Să amintim totuși că după alți autori nu
unealta pur și simplu, ci unealta de ordinul al doilea,
adică unealta cu ajutorul căreia obținem o altă unealtă,
este semnul distinctiv al omului.) Întreaga noastră
evoluție, în ultimele cinci milioane de ani, se bazează
pe realizări tehnologice foarte simple. De cinci milioane
de ani ne ocupăm de cules și de vânătoare, strângem
rădăcini și frunze, vânăm și mâncăm alte animale;
mâncăm orice lucru comestibil întâlnim în cale.
345

Solomon Marcus

Care erau șansele de supraviețuire și evoluție ale
primilor hominizi măsurând 80 de cm, cântărind 40 de
kg, ale căror organe de simț nu puteau rivaliza cu ale
marilor carnivore preistorice și care nu se puteau compara
cu acestea nici în ceea ce privește viteza de deplasare?
Toate datele de care dispunem, directe (M. Keyes Roper,
A survey of the evidence for the intrahuman killing in
the Pleistocene, Current Anthropology, vol. 10, 1969,
pp. 427‑460) sau deduse (R.E. Leakey, The making of
mankind, M. Joseph Ltd., London, 1981), converg către
faptul că primii oameni nu ar fi supraviețuit și evoluat
dacă s‑ar fi ucis între ei și dacă nu ar fi cooperat intens.
A urmat apoi ceea ce Genovés numește marea
revoluție a omenirii; este vorba de revoluția agricolă,
înfăptuită în urmă cu șapte mii de ani. S‑a petrecut
atunci un lucru extraordinar de important. Pentru prima
oară, omul nu mai trăia de pe o zi pe alta, ci putea să
agonisească. Putea să‑și facă rezerve de hrană, găsind
astfel răgazul de a se gândi la viața lui, la ceea ce el este
și la ceea ce ar vrea să fie. Așa a apărut cultura, „zborul
uman”, cum o numește Genovés. De la tehnologie omul
își ia zborul spre știință și umanism. Pentru prima oară,
în urmă cu 7.000 de ani, în funcție de factori geografici,
de succesiune, de tenacitate în muncă, de inteligență,
personalitate, structură demografică, izolare sau neizolare,
fertilitate naturală a solului etc., oamenii se repartizează în
346

Rãni deschise

două categorii: cei care dispun de rezerve alimentare și cei
care nu dispun, trebuind să trăiască de pe o zi pe alta ca
strămoșii lor.
Dar zborul uman, cultural n‑ar fi fost posibil fără
zborul animal anterior. De la acesta din urmă, cu întreaga
sa odisee, de la amphioxus la pești, de la pești la amfibieni,
de la amfibieni la păsări, reptile și mamifere am moștenit
nonconformismul, nevoia de a merge mereu mai departe.
Datele recente ale paleontologiei, teoriile asupra evoluției
umane, furnizate de etologie, de neurofiziologie, de
fiziologie, de preistorie și de istorie, de antropologie și
de genetică concordă asupra unui fapt foarte important:
animalele nu atacă, în sensul uman al cuvântului; ele se
mulțumesc să mănânce. Generalizarea și instituționalizarea
violenței și a uciderii, în interiorul aceleiași specii, sunt
proprii singurei ființe care s‑a ridicat spre cultură, omul. În
contrast cu cercetătorii care susțin că agresiunea și violența
sunt înnăscute la om (a se vedea Lorenz, Ardrey, Storr,
Leyhausen, Eibl‑Ebesfeldt și, mai recent, J.Q. Wilson –
R. Herrnstein, Crime and human nature, 1985), Genovés
susține că omul a început să fie agresiv exact atunci când
a devenit o ființă culturală, adică în urmă cu 7.000 de ani.
Dar, așa cum a știut să inventeze războiul, omul poate
inventa și pacea. În condiții de pace, nu vor mai exista
frontiere în calea cunoașterii umane, fie ea științifică,
tehnologică sau umanistă.
347

