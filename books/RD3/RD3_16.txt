Opera științifică a lui Dan Barbilian
Tribuna României, revistă editată de
Asociația „România”, anul IV, nr. 59,
15 aprilie 1975, p. 6
În preajma celei de a 80‑a aniversări a nașterii
marelui matematician român Dan Barbilian – una
și aceeași persoană cu poetul Ion Barbu –, Editura
tehnică a publicat cel de‑al treilea volum al operei sale
didactice, cuprinzând unele cursuri speciale predate
la Universitatea din București. Cele două volume
anterioare, publicate în 1968 și, respectiv, în 1971, au
cuprins textul unor cursuri de geometrie elementară și de
algebră elementară, toate predate înainte de anul 1944.
Prezența cuvântului „elementară” în denumirea
unora dintre cursurile lui Barbilian nu trebuie să înșele.
Când, în 1932, a ocupat, prin concurs, conferința
de Matematici elementare și geometrie descriptivă,
Barbilian a precizat că domeniul matematicilor
elementare corespunde chiar dezvoltării istorice a
matematicii în Antichitatea greacă, în Evul Mediu, în
perioada Renașterii și în prima parte a erei moderne,
până pe la mijlocul secolului al XVII‑lea, când geometria
86

Rãni deschise

analitică a lui Descartes și calculul diferențial și integral
al lui Newton și Leibniz marchează o eră cu totul nouă
în dezvoltarea matematicii. Dar dacă principalele teorii
elementare erau deja încheiate către mijlocul secolului
al XVII‑lea, o nouă renaștere a doctrinelor elementare
– cel puțin în ceea ce privește geometria – se observă
abia în a doua jumătate a secolului al XIX‑lea. Alături
însă de obiectul elementar al matematicii există și o
gândire matematică elementară, care se manifestă,
după Barbilian, prin „tendința de a concepe adevărurile
matematice noi tot în limitele vechilor tipare, familiare
și armonioase.” Matematicile elementare sunt, pentru
Barbilian, „matematici de întemeiere”, iar spiritul
elementar, în sensul precizat mai sus, reprezintă un fel
de „clasicism al matematicilor”. Barbilian își propune
– și cât de frumos reușește! – nu numai să prezinte
matematicile elementare, „acele cunoștințe care,
asemenea și dimpreună cu ilustrele «Elemente» ale
lui Euclid – de unde își trag numele – formează Carta
însăși, constitutivă a matematicilor aplicate”, dar să și
„favorizeze” dezvoltarea gândirii elementare, motivând
că „spiritul matematic de cercetare, în momentele lui
de valabilă activitate, procedează elementar, adică prin
combinare de părți simple, luate în număr finit.”
Am insistat asupra acestui punct de doctrină din
opera lui Barbilian, deoarece el explică, în bună măsură,
87

Solomon Marcus

și obiectul, și metoda multora dintre cursurile sale
republicate în cele trei volume menționate. Mai mult, o
bună parte din opera științifică a lui Barbilian (înțelegând
prin aceasta opera publicată sub formă de articole sau
monografii) stă sub semnul aceleiași „elementarități”
care nu are nimic comun cu spiritul rudimentar cu care,
cum observă Barbilian, gândirea curentă deseori o
confundă. Este locul aici să amintim că paralel cu opera
didactică se întreprinde și republicarea operei științifice
originale a lui Barbilian, din care au apărut, la Editura
didactică și pedagogică, două volume (unul consacrat
geometriei, altul algebrei) în 1967 și un al treilea,
în 1970, consacrat lucrărilor cu caracter elementar.
Prefațatorul acestui din urmă volum, academicianul
Gheorghe Vrănceanu, precizează că „deși aceste Note
sunt cu caracter elementar, expunerea lor face apel în
cele mai multe cazuri la teorii matematice înalte.”
Un alt punct important de doctrină în opera lui
Barbilian se referă la axiomatică și a făcut obiectul
lecției sale de deschidere din 1942. Textul acestei lecții
este reprodus în volumul al treilea al operei didactice.
„Mai puțin stearpă decât critica literară față de literatura
propriu‑zisă, Axiomatica se apropie totuși de cea
dintâi prin preocuparea de a construi sinteze valabile
nu între unități simple ci între unități compozite, puse
la îndemână de o activitate anterioară.” Așa cum
88

Rãni deschise

critica este „arta de a gândi prin cărți”, matematica
este, pentru Barbilian, arta de a gândi prin teoreme, iar
axiomatica, arta de a gândi prin doctrine. „O doctrină a
doctrinelor, o matematică la puterea a doua”, axiomatica
se referă la marile sinteze de fapte matematice. Mecanica
newtoniană, mecanica relativistă, topologia, teoria lui
Galois a ecuațiilor, teoria funcțiilor automorfe sunt, pentru
Barbilian, alături de discipline mai vechi ca geometriile
euclidiene, neeuclidiene, proiective, circulare, doctrine
matematice (sunt, toate, extensiuni ale așa numitelor
geometrii ale programului de la Erlangen). O doctrină
matematică este, pentru Barbilian, un sistem închis de
cunoștințe, în sensul că oricare ar fi numărul teoremelor
dezvoltate în interiorul ei nu părăsim niciodată un cadru
prealabil fixat; în ceea ce privește exemplele date, acest
cadru este obținut de faptul că toate rezultatele pot fi
interpretate ca invarianți ai grupului fundamental. O
situație diferită o prezintă discipline ca Analiza (cu
excepția anumitor ramuri, cum ar fi teoria ecuațiilor
diferențiale a lui Lie), Teoria probabilităților, numeroase
capitole ale Algebrei; acestea sunt sisteme deschise de
cunoștințe, de aceea scapă Axiomaticei. Ele sunt încă
discipline doar axiomatizante (nu axiomatizate!), iar
Barbilian se întreabă dacă ele vor atinge acea perfecțiune a
disciplinelor axiomatizate, care, pentru el, se confundă cu
doctrinele programului de la Erlangen.
89

Solomon Marcus

Astăzi știm că răspunsul la întrebarea de mai sus
este negativ sau, mai de grabă, că întrebarea trebuie altfel
formulată. Dar distincțiile fine introduse de Barbilian își
păstrează în întregime interesul. Mai mult, raza lor de
acțiune se mărește acum, odată cu pătrunderea gândirii
matematice în toate domeniile de cercetare.
Nu putem încheia aceste rânduri fără a menționa
râvna deosebită a profesorului N. Mihăileanu, îngrijitorul
operei didactice și autorul unui studiu introductiv, care
aduce multe informații utile în condițiile în care multe
dintre cursurile lui Barbilian n‑au mai rămas decât în
exemplare izolate.

90

