Șahul și lingvistica
Știință și tehnică, nr. 7, iulie 1988, pp. 18‑19
Faptul că pentru lămurirea structurii de limbaj
autori dintre cei mai cunoscuți, ca Ferdinand de
Saussure și L. Wittgenstein, au simțit nevoia să recurgă
la analogia cu jocul de șah are o semnificație deosebită,
care nu putea să nu rețină atenția cercetătorilor. Un
prestigios lingvist contemporan, Joseph Greenberg (Is
language like a chess game?, în volumul Language,
Culture and Communication, publicat de A.S. Dil la
Editura Stanford University Press, 1971) a supus unui
examen amănunțit analogiile propuse de F. de Saussure,
constatând că ele rezistă tuturor testelor constituite de
dezvoltarea contemporană a lingvisticii, în cei peste 50
de ani care‑l despărțeau de data publicării Cursului de
lingvistică generală al celebrului genovez. Șahul este
exemplul ideal prin care explicăm esența unor opoziții
fundamentale în lingvistică, de tipul sincronie‑diacronie
sau limbă‑vorbire. Nu este vorba aici numai de faptul
de a explica un lucru mai complicat prin unul mai
simplu, ci și de încă cel puțin două aspecte esențiale.
Limbajul, în ciuda componentei sale ludice, este dominat
474

Rãni deschise

de necesitate și finalitate, în timp ce șahul se înscrie în
întregime sub semnul ludicului; apoi, în bună măsură,
ca o consecință a situației pe care tocmai am semnalat‑o,
regulile șahului sunt explicite, în timp ce regulile limbii
sunt, în cea mai mare parte, implicite, operația de
identificare a lor fiind anevoioasă și cu rezultate care
variază în timp și în spațiu.
Este însă legitim să ne întrebăm dacă rolul șahului
nu poate fi preluat de orice alt joc. Nu este greu de
văzut că, în raport cu alte jocuri foarte răspândite, șahul
oferă avantajul varietății pieselor sale, în consonanță cu
varietatea elementelor lingvistice; avantajul gradului
relativ sofisticat al regulilor care guvernează mișcarea
pieselor și al caracterului lipsit de orice ambiguitate
al acestor reguli. Astfel, jocuri precum Go sau table
prezintă piese sub forma unui alfabet format dintr‑un
singur element. Șahul prezintă o dublă varietate, de ordin
material și structural, întocmai ca și limba.
Interesul pentru analogia limbă‑șah a crescut în
ultimele decenii datorită faptului că structuri de tipul
celor lingvistice au fost identificate și în alte domenii,
cum ar fi etnologia (Claude Lévi‑Strauss), psihanaliză
(J. Lacan), genetică (Roman Jakobson) și multe altele.
A apărut astfel posibilitatea de a extinde considerabil
câmpul de manifestare a metaforei șahiste. În același
timp, aceste tentative au condus la o reconsiderare mai
475

Solomon Marcus

riguroasă a însăși structurii jocului de șah. Acest joc a
cunoscut, de‑a lungul vremii, numeroase variante, așa
cum rezultă din lucrarea lui Harold j. Murray (A History
of Chess, Clarendon Press, Oxford, 1913) sau din
lucrarea mai recentă a lui John Gollon (Chess Variations:
Ancient, Regional and Modern, E. Tuttle, Rutland,
Vt. 1968). Aflăm astfel că au existat 27 de variante ale
jocului de șah, fapt care ne sugerează o modalitate
simplă de identificare a structurii de bază a acestui joc:
găsirea elementelor comune celor 27 de variante.
Orizontal, vertical și diagonal
Să începem cu configurarea tablei de șah. Dintre
cele 27 de variante, numai șase prezintă configurația
8x8. Însă la aproape toate variantele, tabla de joc este
dreptunghiulară (nu neapărat pătrată) și prezintă două
familii de drepte paralele, cele dintr‑o familie fiind
perpendiculare celor din cealaltă familie. Excepție de la
această structură fac șahul bizantin, jucat pe o suprafață
rotundă, și o variantă a șahului chinezesc, la care
suprafața de joc este hexagonală.
În cele mai multe variante bazate pe intersecții de
drepte echidistante, piesele ocupă pătratele formate de
dreptele respective, în așa fel încât fiecare dintre pătrate
este accesibil cel puțin unei piese. (Există jocuri care se
desfășoară pe o suprafață de tipul aceleia de la șah, dar în
476

Rãni deschise

care jumătate dintre pătrate nu sunt accesibile nici unei
piese. Într‑o variantă chinezească, suprafața de joc este
în formă stelată, cu intersecții formând unghiuri de câte
45 de grade. Piesele ocupă doar punctele de intersecție.)
O atare structură impune o serie de restricții de la care
nu se va putea sustrage nici un joc care li se supune.
Într‑adevăr orice mutare a unei piese dintr‑un loc în altul
vecin trebuie, în condițiile date, să fie sau perpendiculară
(pe una dintre laturile suprafeței dreptunghiulare de joc)
sau în diagonală (formând un unghi de 45 de grade cu
baza).
Față de opoziția perpendicular‑diagonal, este
natural să se creeze câte o piesă pentru fiecare dintre
cele două posibilități. După cum se știe, aceste piese
sunt turnul și nebunul. Există aceste piese în toate
variantele jocului de șah (cu excepția celor menționate)?
Pentru a răspunde la această întrebare trebuie mai întâi
să precizăm în ce condiții două variante ale jocului de
șah sunt efectiv distincte; aceasta se întâmplă atunci
când în fiecare dintre cele două variante există o piesă
ale cărei mișcări sunt diferite de ale oricărei piese din
cealaltă variantă. Aplicând acest criteriu, cele 27 de
variante inițiale se reduc la 20. În fiecare dintre aceste
20 de variante esențiale există o piesă echivalentă cu
turnul modern, în sensul că se poate deplasa numai
orizontal sau vertical. Tot așa, în fiecare dintre cele 20 de
477

Solomon Marcus

variante există o piesă dedicată exclusiv deplasărilor în
diagonală. Dincolo însă de aceste fapte, apar deosebiri
de detaliu. Astfel, într‑o variantă europeană medievală,
în una islamică și într‑una japoneză, piesa dedicată
deplasărilor orizontale sau verticale se poate muta numai
într‑un pătrat vecin, orizontal sau vertical. Într‑o variantă
islamică această piesă nu se poate muta mai mult de
două pătrate, pe orizontală sau pe verticală, iar într‑o
variantă spaniolă medievală ea nu se poate muta decât pe
distanța a trei pătrate, orizontal sau vertical. În ceea ce
privește piesele care se deplasează exclusiv în diagonală,
este interesant faptul că în numai opt variante există o
piesă echivalentă cu nebunul modern.
Transgresarea opoziției perpendicular‑diagonal
Orice opoziție este până la urmă transgresată.
Era firesc să fie transgresată și opoziția
perpendicular‑diagonal. Cum? Prin crearea unor piese
care mediază cei doi termeni, fiind orientate în aceeași
măsură spre mișcări perpendiculare, ca și spre mișcări
în diagonală. După cum se știe, în șahul modern,
aceste piese sunt regele și regina; însă în timp ce regele
realizează dezideratul formulat la modul minimal,
regina îl realizează la modul maximal. Care este,
din acest punct de vedere, situația calului? În cărțile
de șah pentru începători publicate la noi, mișcările
478

Rãni deschise

calului sunt descrise cu ajutorul literei L: două pătrate
orizontal sau vertical, urmate de un pătrat pe direcția
perpendiculară primei. Să observăm însă că o descriere
perfect echivalentă este aceea care prevede pentru cal o
mutare constând în deplasarea cu un pătrat pe orizontală
sau verticală, urmată de deplasarea, tot cu un pătrat,
înainte, pe diagonală. Apare astfel echilibrul pe care
calul îl realizează, în felul său specific, între orientarea
pe perpendiculară și orientarea pe diagonală; deci, din
acest punct de vedere, calul intră în aceeași categorie cu
regele și regina. Deosebirea dintre cal, pe de o parte, rege
și regină, pe de altă parte, constă în faptul că, în timp
ce calul realizează concomitent (deci printr‑o singură
mutare) echilibrul dintre perpendicular și diagonal,
regele și regina realizează acest echilibru în mod
alternativ. Folosind terminologia lingvisticii moderne,
am putea spune că perpendicularul și diagonalul se
află în relație sintagmatică în cazul calului, și în relație
paradigmatică în cazul regelui și al reginei. Dar poate că
mai adecvat putem exprima distincția dintre cal, pe de
o parte, și rege și regină, pe de altă parte, referindu‑ne
la faptul că în cazul calului echilibrul menționat se
realizează individual, prin fiecare mișcare în parte, în
timp ce la rege și regină acest echilibru se realizează
numai global, prin ansamblul mișcărilor posibile. Dar
acest echilibru global este numai de natură potențială:
479

Solomon Marcus

ceea ce se actualizează pe tabla de joc poate constitui o
derogare de la echilibrul menționat. Ar fi interesant de
urmărit acest aspect al pragmaticii șahiste.
Cum se prezintă lucrurile în variantele mai vechi
ale șahului? Este interesant faptul că în șase dintre
aceste variante regelui i se permite, la prima sa mutare,
comportamentul unui cal, în timp ce în nicio variantă
nu i se permite comportamentul unei alte piese decât
calul. Apropierea structurală dintre cal și rege este deci
recunoscută.
Calul – o problemă de finețe
Urmează acum o problemă de finețe. Neutralitatea
calului în raport cu cele două posibile orientări,
perpendiculară și în diagonală, ar pretinde să nu
existe o ordine preferențială în succesiunea acestor
două orientări; cu alte cuvinte, daca admitem ordinea:
deplasare cu un pătrat pe perpendiculară, urmată de
o deplasare cu un pătrat pe diagonală, atunci trebuie
să admitem și ordinea inversă – deplasare cu un pătrat
pe diagonală, urmată de o deplasare cu un pătrat pe
perpendiculară. Numai în acest fel calul realizează
într‑adevăr o mediere „nepărtinitoare” între cele două
orientări. Nu este însă greu de văzut că acest deziderat
poate fi realizat numai dacă permitem calului să treacă
și peste pătratele ocupate de alte piese. Tocmai acest
480

Rãni deschise

privilegiu explică denumirea de cal; este vorba de o piesă
care sare peste orice fel de pătrate, libere sau ocupate.
Nu mai puțin de 17 variante ale șahului conțin câte o
piesă de tipul calului modern, în timp ce una dintre
variante recurge la o piesă care cumulează mișcările
calului şi ale reginei din șahul modern. Michael P.
Carroll (A structuralist look of chess, Semiotica, vol.
31, 1980. nr. 3‑4), care ne ghidează în itinerarul de față,
reprezintă mișcarea calului ca o structură diacronică
a perpendicularității şi diagonalei, opusă structurii
sincronice a acelorași elemente, la rege si regină. Însă
structura sincronică este aici iluzorie, deoarece nici
regele, nici regina nu realizează concomitent cele doua
tipuri de orientări. În ambele cazuri este vorba do o
structura diacronica, însă la niveluri diferite: la nivelul
fiecărei mutări in parte, în cazul calului; ia nivelul
ansamblului mutărilor, în cazul regelui şi reginei.
Pionii – un compromis între turn și regină
Este natural acum să ne întrebăm daca nu se
poate realiza o a doua mediere între piesele cu mișcări
perpendiculare şi piesele din prima mediere sau între
piesele cu mișcări în diagonală și cele din prima mediere.
Această mediere de ordinul al doilea ar putea fi realizată
cu piese care în general se deplasează orizontal sau
vertical, dar posedă și o capacitate limitată de deplasare
481

Solomon Marcus

în diagonală, fie că se deplasează în diagonală, dar
dispun și de o oarecare posibilitate de deplasare
orizontală sau verticală. Prima situație este, evident,
realizată de pionii șahului modern, care se deplasează în
general pe verticală, dar nu pot captura o piesă adversă
decât mişcându‑se în diagonală. Dintre cele 20 de
variante de șah, 18 posedă o piesă de tipul pionului. Dar
chiar cele două variante în care nu există o atare piesă
dețin o piesă oarecum asemănătoare pionului. Astfel,
în varianta japoneza Shogi există o piesă echivalentă
turnului modern, care dacă reușește să ajungă în
teritoriul advers (ultimele trei rânduri opuse) va capătă
dreptul suplimentar de a se deplasa câte un pătrat, în
diagonală, în orice direcție.
Ce se întâmplă însă cu posibilitatea unui compromis
între mișcarea în diagonală şi cea din prima mediere?
Șahul modern nu o mai cunoaște, dar ea a existat în
unele variante mai vechi. Astfel, în varianta japoneză
Shogi exista o piesă de tipul nebunului modern, care,
în plus, se poate deplasa şi vertical sau orizontal, dar
numai câte un pătrat. Într‑o altă variantă veche, exista
așa‑numitul elefant, care se poate deplasa câte un pătrat,
în diagonală. în oricare dintre cele patru direcții sau un
pătrat înainte.

482

Rãni deschise

O tentativă de axiomatizare
Să încercam acum să sintetizam discuția de mai
sus. Regulile jocului de șah se înfățișează sub forma
unui sistem axiomatic. Admitem trei axiome inițiale
privind structura suprafeței de joc şi relația ei cu
piesele de joc. Axioma 1. Suprafața de joc constă
într‑o familie de pătrate obținute prin două familii
de drepte paralele, fiecare dreaptă din prima familie
fiind perpendiculară fiecărei drepte din a doua familie.
Axioma 2. Piesele de joc ocupă și se deplasează în
pătratele introduse prin axioma 1. Axioma 3. Fiecare
pătrat de pe suprafața de joc este accesibil cel puțin
unei piese. Acest sistem de axiome pune în evidență
două orientări privilegiate, în ceea ce privește direcția
de mișcare: orientarea perpendiculară (orizontală sau
verticală) şi orientarea în diagonală. Piesele şi regulile
de joc sunt, în mare măsură, consecința firească a acestor
două orientări fundamentale. O primă categorie de
piese li se asociază în mod exclusiv; piesele de ordinul
întâi sunt cele cu mișcări exclusiv perpendiculare și
cele cu mișcări exclusiv în diagonală. Considerente
de simetrie conduc la alegerea a câte două piese din
fiecare dintre aceste două tipuri, deci două turnuri şi
doi nebuni. O a doua categorie de piese realizează
un echilibru între perpendicular şi diagonal. Aceste
piese de al doilea ordin sunt la rândul lor, de două
483

Solomon Marcus

tipuri, după cum echilibrul menționat se realizează de
fiecare mutare în parte sau numai global, de ansamblul
potențial al deplasărilor piesei considerate. Primul tip
este reprezentat de cai (din considerente de simetrie,
în număr de doi); al doilea tip este, la rândul său,
de două subtipuri după cum posibilitatea deplasării
perpendiculare sau în diagonală este minimală, fiind
redusă la un singur pătrat (regele din șahul modern)
sau maximală, limitată doar de dimensiunile suprafeței
de joc (regina din șahul modern). Deoarece primul
tip este reprezentat de două piese, tot două piese vor
fi afectate şi celui de‑al doilea tip, deci câte o piesă de
fiecare subtip (un rege şi o regină). Totalizăm astfel
opt piese, număr care sugerează dimensiunea de 8x8 a
suprafeței de joc. O a treia categorie de piese se referă
la un echilibru, un compromis între piesele de prima
categorie şi cele de a doua categorie. Piesele de al
treilea ordin astfel obținute sunt, la rândul lor, de două
feluri, după cum prima categorie este reprezentată prin
turnuri (perpendicularitate) sau prin nebuni (diagonală),
dar numai primul fel (pionii) este reprezentat în șahul
modern. Numărul pionilor este sugerat de dimensiunea
8x8, câte un pion în fața fiecărei piese din prima sau a
doua categorie.

484

Rãni deschise

Dincolo de logic și combinatorial
Procedarea de mai sus a căutat să aducă în
prim‑plan aspectul logic şi combinatorial al regulilor
jocului de șah, măsura în care aceste reguli se impun
aproape de la sine, de îndată ce admitem un minim de
premise, dintre cele mai simple. Dar chiar din această
prezentare s‑a desprins și ideea că aspectul logic nu
poate fi sesizat fără luarea în considerare a aspectului
istoric. De asemenea, rămân numeroase aspecte care nu
pot fi înțelese şi explicate fără referire la fenomene de
ordin psihologic, social şi cultural. De ce sunt pătratele
în alb‑negru? Care este funcția psihologică a acestui
contrast? De ce ne oprim la piese de a treia categorie
(pionii) şi nu procedăm, în continuare, la introducerea
unor piese de a patra categorie, care să medieze
între categoriile imediat anterioare? Cum se explică
dispunerea inițială a pieselor pe tabla de joc, inclusiv
dimensiunea 8x8? Este evident că în aceste alegeri
intervin şi considerente de ordin entropic‑informațional,
privind capacitatea creierului uman de a prelucra
informația, gradul de complexitate pe care îl putem
stăpâni şi controla. Însă aceste restricții de ordin
informațional care pun frâu tendinței combinatoriale,
practic nelimitate, nu știm să fi fost explicate în cazul
jocului de șah. Într‑un moment în care teoria matematică
a informației cunoaște mari progrese, cercetări de acest
485

Solomon Marcus

fel nu se vor putea lăsa mult așteptate. Mai sunt apoi
aspectele legate de denumirile pieselor de șah, denumiri
a căror evoluție pune inerent în discuție originea
jocului de șah. Aspectele combinatoriale, strategice
și psiho‑sociologice intră toate în interacțiune. Putem
despărți regulile combinatoriale ale regelui de faptul că
prin capturarea regelui advers se obține victoria?

486

