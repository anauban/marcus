Meridiane noetice
Contemporanul, nr. 36 (1817), 4 septembrie 1981
La interval de câteva luni au apărut două noi
volume ale publicației aparținând Comitetului român de
istorie și filozofie a științei din cadrul Academiei R.S.R.,
publicație cu inspirata denumire de Noesis*. Fiecare
volum este organizat în două secțiuni: una mai amplă,
dedicată istoriei științei, alta mai restrânsă, dedicată
filozofiei științei.
O preocupare constantă, urmărită încă de la primul
volum, se referă la apărarea unor priorități mondiale
ale științei românești. Astfel, centenarul lui Gogu
Constantinescu, creatorul sonicității, prilejuiește un
articol al profesorului Matei Marinescu, în vol. VII, și
un altul al academicianului Ștefan Bălan, în vol. VIII.
Nicolae Perciune discută (în vol. VII) contribuțiile
de pionierat ale lui Augustin Maior privind realizarea
sistemelor de telefonie multiplă, iar academicianul
Ștefan Milcu face (în vol. VIII) o sinteză a activității lui
C.I. Parhon, autor al primului tratat de endocrinologie
din lume (1909) și subliniază că Bucureștii au găzduit
* Noesis, vol. VII, VIII.

232

Rãni deschise

primul congres european de endocrinologie (1939).
Cele mai multe studii se referă la aspecte,
evenimente și personalități din istoria științei românești.
În vol. VII, Amilcar Vasiliu se ocupă de agricultura
geto‑dacilor sub Burebista; Olga Necrasov îl prezintă
pe Ioan Borcea; inițiator al cercetărilor românești de
oceanologie; Gh. Brătescu analizează preliminariile
creării facultăților de medicină din București și Iași;
Teofil Gridan face istoricul cercetărilor geologice în
Munții Semenic; H.H. Stahl se ocupă de D. Gusti și
dezvoltarea sociologiei; Yolanda Eminescu, Maria
Ioana Eremia și C. Damian semnalează primul congres
internațional ținut în România; acesta a avut loc la data
de 8 septembrie 1906 și s‑a referit la proprietatea literară
și artistică. Amelia Ionașcu stabilește rolul Societății de
drept din Oradea în dezvoltarea învățământului juridic
din Transilvania. Radu Pantazi analizează personalitatea
lui Simion Bărnuțiu, academicianul Ștefan Pascu se
ocupă de înființarea și organizarea universității românești
de la Cluj‑Napoca, iar Ioan Orghidan plasează educația
și gândirea pedagogică românească în context istoric
european. În vol. VIII, Romulus Cristescu reliefează
contribuțiile matematicianului Alexandru Ghika,
Constantin Macarovici și Magda Petrovanu urmăresc
dezvoltarea chimiei pe teritoriul României în secolul
al XIX‑lea, Matei Oroveanu semnalează un cercetător,
233

Solomon Marcus

inventator și constructor român prea puțin cunoscut
în domeniul aeronauticii, George Grămăticescu
(1888‑1913); Radu Codreanu îl prezintă pe Alexandru
N. Vițu, fondator al fiziologiei experimentale în
România; N. Ceapoiu marchează centenarul nașterii
agronomului Constantin Sandu‑Aldea; B. Duțescu
și N. Marcu periodizează învățământul universitar
medical în București; I.M. Ștefan îl prezintă Const. C.
Giurescu în ipostaza de istoric al științei și tehnologiei
românești, Maria Cârciumaru și Maria Bitiri analizează
semnificația picturilor rupestre din Valea Someșului.
Într‑o sinteză deosebit de cuprinzătoare, academicianul
Ștefan Bălan prezintă dezvoltarea cercetărilor de istoria
științei și tehnologiei în România. Dintre studiile de
filozofie a științei le vom menționa, din vol. VII, pe cel
al academicianului Octav Onicescu, privind funcția
epistemologică a științei. Alte cercetări, nu mai puțin
interesante, aparțin, în vol. VII, lui Gh. Gheorghiev, T.
Toro, V. Soran și Ana P. Fabian, Al. Tănase, S. Ghiță și
Stela Maria Ivaneș, iar în vol. VIII lui Al. Tănase, Ilie
Pârvu, Crizantema Joja și Călina Mare.
Este vizibil că publicația Noesis a devenit cu totul
insuficientă pentru a găzdui toate cercetările românești
valoroase de istoria și filozofia științei. Unele studii
interesante, de acest profil, apar în alte publicații,
științifice sau filozofice, periodice sau neperiodice. Din
234

Rãni deschise

acest punct de vedere, o publicație anuală importantă,
dar puțin cunoscută și slab difuzată, o reprezintă Analele
Academiei R.S. România. Recent a apărut volumul
relativ la anul 1979.
Este păcat că difuzarea acestor publicații nu este pe
măsura grijii puse în pregătirea lor.

235

