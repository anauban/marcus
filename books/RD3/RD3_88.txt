Eșecuri pilduitoare
Almanah Scânteia tineretului, 1989, pp. 54‑55
Perioadă nefastă a fost o bună parte din tinerețea
mea, care a coincis cu ascensiunea fascismului și
cu cel de‑al Doilea Război Mondial. În acei ani,
problema principală care îmi marca existența era aceea
a supraviețuirii. A te afla într‑un punct limită, la vârsta
marilor visuri și proiecte, a marilor ambiții și speranțe,
iată situația dramatică a generației mele. La 23 August
1944 mă aflam într‑un oraș de provincie, încă înfricoșat
de un posibil bombardament. Mi‑au trebuit câteva zile
pentru a mă dezmetici și a înțelege noua realitate. Mă
consideram un răsfățat al sorții, care mi‑a cruțat viața
într‑o vreme de maximă incertitudine și amenințare.
Știam că tot ceea ce aveam să fac de atunci înainte urma
să se afle sub semnul unei înalte responsabilități. Un
sentiment de gravitate a existenței umane m‑a urmărit
de atunci necontenit și poate că din acest sentiment am
extras tăria morală de a mă angaja în proiecte care cereau
un efort susținut, o concentrare a unor forțe deopotrivă
intelectuale și afective. Din această înțelegere a vieții ca
angajare totală a rezultat și modul meu de a mă apropia
602

Rãni deschise

de profesia de matematician, în care am văzut de la
început nu numai o profesie, ci și o înțelegere specifică,
rațională și emoțională a existenței umane.
Față de această încercare majoră la care m‑a supus
istoria, mai contează are diferite accidente școlare,
conflictele cu unii oameni sau cu unele instituții? Mai
contează oare acestea, chiar dacă au reușit să‑mi întârzie
realizarea unor proiecte, să‑mi fure o bună parte din
timpul pe care l‑aș fi vrut afectat activității intelectuale?
Insuccese au fost multe, dar am avut mereu încredere
în proiectul meu pe termen lung: o încredere alimentată
de pasiunea pe care am căpătat‑o încă din adolescență
pentru universurile de ficțiune ale poeziei și ale științei.
Uneori îi aud pe cei din generația mea criticându‑i
pe tinerii de azi pentru ușurința de care dau dovadă. „Ei
n‑au cunoscut războiul, n‑au avut greutăți, cred că totul li
se cuvine.” N‑ar mai rămâne, după această logică, decât
să le dorim tinerilor de azi să cunoască și ei un război!
Este adevărat, există destui tineri care, până la 18‑20
de ani, nu simt în fața lor niciun obstacol, nu parcurg
nicio experiență care să‑i pună într‑un fel la încercare,
să le solicite un efort deosebit. Dar această situație nu
este în ordinea naturală a lucrurilor, ci o consecință a
unui tratament necorespunzător din partea familiei și a
celorlalți educatori. Într‑o dezvoltare normală, individul
uman parcurge, în fiecare etapă a existenței sale,
603

Solomon Marcus

începând chiar cu nașterea sa, o experiență conflictuală.
Am prezentat pe larg această problemă în Timpul
(Editura Albatros, 1985, pp. 241 – 257) și nu vreau să mă
repet. Accidentele care rezultă din diferitele calamități
ale naturii, din conflicte cu oameni sau cu instituții pot
desigur agrava situația în mod dramatic. Chiar în cadrul
profesiei mele cunosc tineri care au trăit aceste situații
limită, de exemplu, doi dintre ei și‑au pierdut vederea,
unul la sfârșitul anilor de liceu, iar celălalt la sfârșitul
anilor de studenție. Amândoi au găsit în ei resursele
necesare de a se afirma în mod strălucit ca matematicieni
creatori. Exemple de acest fel ar trebui să se afle mereu
în atenția acelor tineri (dar și a educatorilor lor) care
la sfârșitul anilor de liceu nu manifestă niciun interes
deosebit de a se pregăti pentru o profesie anume și
sunt gata să încerce oriunde: la Drept, la Medicină sau
mai știu eu unde, numai să devină studenți. Cei mai
mulți dintre tinerii care ajung în această situație nu sunt
victimele unor obstacole exterioare, întâlnite în cale,
ci, dimpotrivă, plătesc pentru faptul că nu au văzut
la timp obstacolul din propria lor mentalitate. Ei s‑au
dezvoltat pe calea unei corectitudini mediocre, evitând
confruntarea cu probleme grele, dar interesante, a căror
rezolvare i‑ar fi umplut de satisfacție și le‑ar fi stimulat
dorința de dobândire a unei priceperi superioare. Nefiind
puși în situația de a testa de unde le poate veni bucuria,
604

Rãni deschise

răsplata spirituală, ei nu și‑au cultivat nicio pasiune
pentru o activitate creatoare.
Adolescenții și tinerii m‑au impresionat întotdeauna
prin curajul imaginației lor și prin încrederea naivă în
posibilitatea transpunerii în realitate a construcțiilor
lor imaginare. Folosesc aici atributul naivă fără nicio
nuanță peiorativă. Cred că tocmai această naivitate
(care nu are nimic în comun cu prostia) este una dintre
însușirile de seamă ale tinereții. Nu este greu de observat
că marii creatori, fie ei poeți sau savanți, rămân și mari
naivi, de‑a lungul întregii lor vieți. Simptomul cel mai
clar al tinereții îl constituie preponderența preocupării
pentru viitor față de aceea pentru trecut, a așteptării și
a speranței față de amintire și resemnare, a făuririi de
planuri față de alcătuirea de bilanțuri. Rămânem tineri
atâta vreme cât nu suntem obsedați de frustrările la care
ne‑a supus viața și nu suntem paralizați în acțiunile
noastre de un scepticism pentru care găsim ușor o
justificare în lunga noastră experiență.
A fi tânăr înseamnă a fi capabil ca, în fiecare
dimineață sau măcar din când în când, dar nu prea rar,
să realizezi acea stare de prospețime a gândurilor și a
simțurilor care‑ți permite să trăiești ca și cum ai lua
lucrurile de la capăt, înseamnă a face totul cu bucurie și a
avea naivitatea să crezi că timpul care ți se așterne în față
depinde esențial de capacitatea ta de a avea nevoie de el.
605

EDITURA SPANDUGINO, 2013

