Cuvânt înainte la volumul
Proiectarea securității sistemelor complexe.
Metode matematice și tehnici de calcul interactiv
de Alexandru A. Popovici
Editura Științifică și Enciclopedică,
București, 1988, 320 pagini
Rândurile care urmează se vor o prezentare nu
atât a cărții de față, cât a autorului ei. Pe Alexandru A.
Popovici l‑am cunoscut în urmă cu aproape douăzeci de
ani, când era student și mai ales în perioada în care își
elabora lucrarea de diplomă. Seriozitatea și multiplele
sale interese intelectuale m‑au impresionat de la
început. L‑am urmărit de atunci cu unele intermitențe,
dar nu mi‑a scăpat bogata sa activitate publicistică
(are la activul său câteva zeci de lucrări științifice), iar
unele discuții mai lungi la care l‑am provocat mi‑au dat
posibilitatea să‑l cunosc și să‑l înțeleg mai profund, nu
numai sub aspect profesional și cultural, ci și din punct
de vedere afectiv.
Evoluția lui Alexandru A. Popovici merită
să fie discutată în semnificație ei mai generală.
După ce absolvă Facultatea de Matematică, unde
510

Rãni deschise

dobândește o gândire matematică riguroasă și o cultură
matematică remarcabilă, lucrează în mediul ingineresc
și informatic, în special în cadrul Colectivului de
ingineria sistemelor și cibernetică industrială de la
Institutul Politehnic București, impregnându‑se de
problematica și mentalitatea inginerească și tehnologică
și consolidându‑și, în același timp, formația sa de
informatician, configurată încă din anii de studenție.
Domeniul către care s‑a îndreptat cu precădere și
care face obiectul cărții de față, securitatea sistemelor
complexe, este de o deosebită importanță științifică
și socială, dându‑și mâna aici o vastă și multilaterală
cultură matematică, o viziune inginerească matură
în domeniul fiabilității și securității sistemelor și
o experiență solidă în ceea ce privește suportul
informatic al proiectării asistate de calculator. La
triada inginerie‑matematică‑informatică se adaugă un
al patrulea element, cel social‑economic, esențial din
punctul de vedere al organelor de planificare și decizie.
Înțelegerea unor laturi atât de diferite ale sistemelor
tehnologice actuale reclamă o pregătire solidă în toate
cele patru direcții menționate. Alexandru A. Popovici
este (cel puțin la noi în țară) unul dintre puținii care
o are. Dacă matematica a învățat‑o la Universitate,
ingineria și informatica, la Institutul Politehnic,
cultura economică și‑a format‑o singur, prin lectură,
511

Solomon Marcus

prin practica contractuală și prin relații cu potențialii
beneficiari ai contractelor de colaborare în care a fost
implicat. Nu s‑a mulțumit numai cu atât, curiozitatea sa
mergând până la fundamentarea teoretică a faptelor, prin
modelarea matematică a teoriei economice. Contribuția
sa la volumul colectiv Modele matematice și semiotice
ale dezvoltării sociale (Editura Academiei R.S.R., 1986)
este revelatoare în acest sens.
Ca și cum acestea nu ar fi fost de‑ajuns, Alexandru
A. Popovici și‑a extins și mai departe investigația; cui i‑ar
veni să creadă că autorul cărții de față, atât de meticulos
și de „uscat” în analiza fenomenului tehnologic, este unul
și același cu autorul unor studii privind folclorul, teatrul
și romanul? De fapt, în această ultimă ipostază l‑am
descoperit, în urmă cu douăzeci de ani, pe Alexandru
A. Popovici, iar nevoia de a reveni periodic la aceste
preocupări nu‑l va părăsi probabil niciodată.
Gândul că frecventarea unor domenii atât de variate ar
implica o anumită superficialitate a investigației își găsește
un fericit contraexemplu în cazul pe care îl discutăm.
În tot ceea ce face, Alexandru A. Popovici investește o
desăvârșită personalitate, care începe cu temeinicia și
modernitatea informației de care se prevalează. Aceasta se
poate vedea și în cartea de față, unde sinteza este rezultatul
unor acumulări îndelungate, concretizate în numeroase
studii publicate în reviste de specialitate.
512

Rãni deschise

Întreaga evoluție a lui Alexandru A. Popovici este
exemplară și simptomatică pentru orientările actuale, în
care știința și ingineria, teoria și practica, disciplinele
naturii și cele umanist sociale, cele ale energiei și cele
ale informației își atenuează deosebirile și colaborează în
sprijinul unei înțelegeri globale și sistemice a proceselor
din natură și societate.

513

