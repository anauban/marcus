Provocarea informației
Ateneu, nr. 7, 1987, p. 10
Să primim, dar să și producem informație.
A devenit un truism să se spună că informarea și
documentarea au un rol fundamental în învățământ și
cercetare. Dincolo însă de această formulare generală,
mergem la manifestările efective ale acestei noi realități
informaționale? Într‑un ritm în unele locuri mai lent, în
alte locuri mai rapid, lumea se îndreaptă spre o societate
informațională. În unele țări, încă de acum vreo cinci ani,
mai mult de jumătate din forța de muncă era ocupată în
diferite faze și tipuri de prelucrare a informației. În aceste
condiții, știința și învățământul trebuie să facă față unui
flux de informație de o intensitate fără precedent. Însă
știința și învățământul superior nu se pot mulțumi să fie
beneficiare ale fluxului de informație, ele au datoria de a
prelucra această informație și de a produce la rândul lor
informație. Între cele două procese, cel al absorbției și cel
al producerii de informație, există o legătură organică.
Între capacitatea de absorbție a informației provenite din
alte surse și capacitatea de creare a unei informații noi,
există o interacțiune puternică.
369

Solomon Marcus

Cercetarea științifică este asemenea unei ștafete.
Numai că în sport îi vezi alături de tine pe ceilalți
alergători, care te stimulează și îți atrag atenția asupra
valorii fiecărei fracțiuni de secundă pierdute sau
câștigate. În știință, nu sunt numai câțiva, ci o mulțime
de alergători care au un obiectiv asemănător cu al
tău; însă acești alergători nu sunt decât rareori direct
vizibili, existența lor este percepută indirect și de multe
ori cu întârziere. Acest fapt poate ascunde caracterul
de competiție pe care îl are cercetarea științifică. A
avea acces rapid la informația cea mai recentă și mai
valoroasă înseamnă posibilitatea de a prelua mai repede
ștafeta de la cei care au produs această informație.
A fi apt să comunici în mod operativ lumii științifice
rezultatele pe care le‑ai obținut înseamnă capacitatea de
a preda ștafeta mai departe cu rapiditate și, deci, cu șanse
mai mari de prioritate.
Concluzia este clară. Trebuie nu numai să ne bazăm
pe revistele, monografiile și tratatele de specialitate cele
mai proaspete, dar să și folosim cu pricepere sistemul
propriu de publicații științifice care să prezinte cu
operativitate specialiștilor din întreaga lume rezultatele
cercetătorilor români. Accentuăm importanța revistelor.
Adoptând o metaforă militară, putem spune că în bătălia
cercetării științifice linia întâi a frontului este vizibilă
în revistele de specialitate; acolo găsim știința care se
370

Rãni deschise

face sub ochii noștri, cu toate ezitările și tatonările ei,
cu întrebările rămase încă fără răspuns. Monografiile,
tratatele și, mai cu seamă, manualele se constituie în
spatele frontului, deosebit de important, dar insuficient
pentru câștigarea bătăliei.
Este deci clar că sursele de informare și
documentare pe care le punem la dispoziția tinerilor
studioși nu se pot reduce la cursuri și manuale. Cu cât
vor fi mai mulți tineri care vor lua contact cu lumea
revistelor (inclusiv a revistelor internaționale de referate)
și cu cât acest lucru se va întâmpla mai devreme, nu abia
în ultimul an de studii, cu atât mai bine îi vom pregăti
pe absolvenții noștri să fie capabili de un randament
creator în profesie. Lucrările de diplomă și lucrările
de specializare dau, în mare măsură, seama de acest
fenomen, de reușitele și eșecurile lui. Este însă necesar
ca cei buni și, mai ales, cei foarte buni să fie atrași
cât mai curând în activitatea de cercetare, pe baza
contactului cu revistele.
În același timp, este necesar ca încă din clasele
liceale elevii să fie învățați cum se alcătuiește și se
folosește o bibliografie relativă la o anume chestiune și
cum se face referința la o sursă bibliografică folosită.
Modul de prezentare a multor comunicări la sesiuni
științifice din școli și facultăți arată că există încă multă
ignoranță și neglijență în această privință. Revistele
371

Solomon Marcus

școlare abundă în texte în care cu greu se poate distinge
partea inedită de partea preluată, neindicându‑se, de
obicei, cu exactitate sursele folosite. Uneori, referința se
face doar pentru o mică parte din ceea ce s‑a preluat.
Acumulare, selecție, comprimare. Cei care
intră acum în cercetare și au intenții serioase în această
privință trebuie să țină seama de amploarea deosebită
pe care a căpătat‑o difuzarea informației științifice,
de ritmul extraordinar al acumulării de rezultate noi.
Nu ne mai putem mulțumi cu o informare de tip
tradițional. Mintea noastră are tot mai multă nevoie de
proteze și auxiliare care să‑i amplifice posibilitățile.
Așa se și explică faptul că apar mereu lucrări privind
igiena muncii intelectuale în noile condiții create
de revoluția științifică și tehnică. La Editura North
Holland (Amsterdam), V. Stibic a publicat în 1980
(cu reluare în 1983) lucrarea Personal Documentation
for Professionals (214 pagini), iar în 1982 (cu reluare
în 1983), lucrarea Tools of the Mind (300 de pagini),
destinate să‑i ajute nu numai pe cercetători, dar și pe toți
intelectualii să‑și organizeze mai bine activitatea.
Dar ce înseamnă a‑ți organiza mai bine o
activitate intelectuală? Înseamnă în primul rând să
separi partea de rutină, de repetare a unor operații
stereotipe, transferând‑o pe cât posibil unor mijloace
372

Rãni deschise

automate, consacrându‑te în schimb cât mai mult
unei activități creatoare. Numai că, în practica vieții
cotidiene, distincția rutină‑creație este înlocuită printr‑un
continuum de situații intermediare. Astfel, o problemă
practică este capacitatea de eliminare a obiectelor
devenite inutile, de simplificare a teoriilor care nu mai
pot fi păstrate la gradul anterior de detaliu, de triere a
cunoștințelor deja acumulate și de articulare a lor cu
cele nou primite. Capacitatea umane de memorare și
de prelucrare a informației este limitată, dar nici nu
este încă folosită cu un randament superior. Există o
considerabilă inerție în a ne desprinde de reprezentările
dobândite și în a le reconsidera prin selecție și
comprimare; și mai mare este această inerție în cazul
instituțiilor astfel, pentru a ne referi la programele de
învățământ în domeniul matematicii, deși s‑au operat
unele comprimări judicioase (cum ar fi cele privind
trigonometria), numeroase alte comprimări se lasă încă
așteptate. În schimb, unele idei de combinatorică, logică,
grafuri, jocuri, probabilități, teoria informației, algoritmi,
limbaje nu și‑au găsit încă locul, în ciuda faptului că
orizontul lor cultural și gradul lor de atractivitate sunt
superioare celor pe care le prezintă destul de multe din
actualele chestiuni de matematică școlară.

373

Solomon Marcus

Să mărim capacitatea de schimb a revistelor
noastre. Există în bibliotecile noastre informația de care
avem nevoie? Răspunsul este oarecum paradoxal: nu
există cât ar fi necesar, dar cea existentă nu este suficient
folosită.
În ultimele decenii s‑a observat un proces de
diversificare a publicațiilor științifice. De exemplu,
în domeniul matematicii, de la reviste generale de
matematică, în care își au loc toate ramurile matematicii
moderne, s‑a trecut tot mai mult la reviste specializate:
de algebră, de geometrie diferențială, de tipologie, de
analiză reală, de analiză funcțională etc. Dacă vechile
reviste puteau fi, în general, obținute prin schimb cu
publicațiile noastre de matematică, noile reviste, având
un profil specializat, nu mai acceptă schimbul cu o
revistă care numai în mod incidental publică articole de
profilul respectiv.
Informația existentă nu este suficient folosită.
Semnale în acest sens am primit din partea directorului
Bibliotecii Academiei R.S. România, din partea
Institutului Național de Informare și Documentare
(INID) și din partea altor organisme. Am putut însă
constata și personal acest lucru. La INID se găsește acel
Science Citation Index care permite să se urmărească
utilizarea fiecărei lucrări publicate. Câți îl consultă?
374

Rãni deschise

La Biblioteca Academiei R.S. România sunt anunțate
lunar noutățile care intră în bibliotecă, printre ele
destule demne de interes, dar sălile bibliotecii nu sunt
foarte populate. Numeroase alte biblioteci pe care le
vizitez prezintă o situație asemănătoare. Am impresia
că numai o mică parte din tinerii noștri cercetători și‑au
format obiceiul de a conspecta cu regularitate noutățile,
în primul rând revistele internaționale de referate. Un
student care se află la primii pașii în cercetare primește
de la îndrumătorul său bibliografia pe care trebuie s‑o
consulte. În mod normal, ar trebui ca după doi, trei ani
să devină capabil să se orienteze singur în literatura de
specialitate.

375

