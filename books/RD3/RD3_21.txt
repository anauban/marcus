Matematica și muzica
Tribuna României, anul IV, nr. 67, 15 august 1975, p. 6
În ultimii ani, se dezvoltă la București preocupări
interesante privind folosirea matematicii și a
calculatoarelor în compoziția muzicală și în analiza
operelor muzicale. Inițiatorii acestor preocupări au fost
câțiva dintre cei mai importanți compozitori români,
Ștefan Niculescu, Aurel Stroe și Anatol Vieru. Ștefan
Niculescu a publicat în revista Muzica un studiu foarte
matematizat asupra sintaxei muzicale. Aurel Stroe a
publicat în aceeași revistă, în urmă cu câțiva ani, un
ciclu de patru articole privind clasele de compoziții
muzicale din punct de vedere matematic; același autor
a preconizat și a folosit calculatorul în compoziția
muzicală. Anatol Vieru, creator al unor compoziții
rezultate din proiectarea unor regularități din domeniul
structurilor matematice în cel al structurilor sonore
(Ciurul lui Eratostene), a elaborat de curând o amplă
lucrare care, plecând de la reprezentarea scărilor
muzicale, adică a modurilor, propune un model
matematic al gândirii muzicale intervalice. Ideea de
bază: noi percepem, odată cu sunetele, și intervalele unei
113

Solomon Marcus

secvențe melodice. Distincția dintre aceste două clase de
obiecte muzicale se traduce, la Anatol Vieru, în distincția
dintre moduri și structuri modale. Dacă proprietatea de
bază a modurilor este complementaritatea, caracteristica
de seamă a structurilor modale este simetria. Cu alte
cuvinte, compoziția muzicală valorifică cu predilecție
modurile complementare și structurile modale simetrice.
Mecanismul relațiilor dintre sunete și intervale este
deschis cu ajutorul unei operații de compunere cunoscută
în muzică sub numele de transpoziție.
O lucrare de mari proporții, care a făcut și
obiectul unei teze de doctorat în… matematică, este
aceea a cunoscutului dirijor Mihai Brediceanu despre
transformările topologice și mecanismele generative în
creația muzicală. Structurile sonore sunt reprezentate
într‑un spațiu timp‑frecvență, căruia i se mai adaugă
ulterior o a treia dimensiune, de natură neprecizată.
Transformările structurilor sonore devin transformări
în aceste spații de două sau trei dimensiuni. Se arată că,
astfel, compozițiile clasice revin la compunerea unor
transformări geometrice simple, de tipul translațiilor,
al rotațiilor, al simetriilor inversiunilor sau omotetiilor.
Rând pe rând sunt angajate în discuție transformări din
ce în ce mai complicate, de la omologii și transformări
conforme până la transformări topologice generale,
dezvăluindu‑se inepuizabilele lor posibilități de
114

Rãni deschise

utilizare în creația muzicală. Mecanismul generator
al acestei creații constă în producerea de secvențe de
transformări cu ajutorul unor mașini generative care pot
fi, în particular, și mașini fizice, de tipul calculatoarelor
electronice moderne. De altfel, Mihai Brediceanu a și
utilizat calculatorul, în cadrul unei colaborări cu centrul
de calcul al Universității din Syracuse – New York.
Tot un punct de vedere generativ este și cel care
stă la baza lucrărilor efectuate de unii matematicieni
români. Îl vom menționa mai întâi pe Bogdan Cazimir,
care a arătat că regulile generative de tip Lindenmayer
(utilizate în biologia celulară pentru simularea modului
de dezvoltare a celulelor într‑un organism filamentos)
permit o descriere printr‑un procedeu uniform a tuturor
tipurilor de transformări melodice. Această similitudine
între două domenii în aparență atât de depărtate –
transformările celulare și transformările melodice – are
o profundă semnificație filozofică; ea urmează încă să
fie analizată. Un al doilea exemplu interesant este cel al
menuetelor din Albumul pentru Anna Magdalena, Bach.
Florentina Simionescu a analizat tipurile de repetiții care
apar în aceste menuete, la nivelul strofelor, al perioadelor
și al frazelor muzicale, punându‑se în evidență, pentru
fiecare nivel al fiecărui menuet în parte, tipul de
„gramatică” (în sensul lui Chomsky) care le generează.

115

Solomon Marcus

Vom mai menționa, printre altele, în încheiere,
originala idee a compozitorului Lucian Mețianu
privind rolul pătratelor latine în creația muzicală;
cercetările statistice ale lui Tudor Misdolea privind
diferiții parametri ai unei structuri muzicale; cercetările
compozitorului Nicolae Brînduș, încă nematematizate,
dar aproape de matematizare, cu intuiții, înaintea altora,
privind rolul gramaticii muzicale; cercetările Manuelei
Aprahamian privind calculul intrărilor posibile într‑un
canon de J.S. Bach.
O situație oarecum aparte o prezintă cercetările
lui Mihai Dinu și Karin Helvig privind raporturile
dintre libret și muzică sau dintre textul unei piese și
cel al libretului operei corespunzătoare (analizându‑se
în special unele opere de Mozart, precum și libretul lui
Renzo Rossellini, scris pe baza piesei lui Arthur Miller,
A view from the bridge).
Unele dintre cercetările de mai sus au fost publicate
în revista Muzica și în Cahiers de linguistique théorique
et appliquée. Altele sunt în curs de apariție în aceste
reviste, în unele reviste românești de matematică, în
„Actele Primului Congres al Asociației Internaționale de
Semiotică” (Milano, iunie 1974), în revista americană
Semiotica și în alte publicații. Dacă până nu de mult
matematicienii și muzicienii au lucrat mai mult separat,
azi ei colaborează intens. O mare pare din aceste
116

Rãni deschise

colaborări se realizează în cadrul „Secției de studiul
sistemelor” a Universității din București, unde în cursul
anului universitar care s‑a încheiat au fost organizate
numeroase dezbateri în domeniul semioticii matematice
muzicale. Unele dintre rezultatele obținute sunt încă de
pe acum cunoscute și apreciate de specialiști străini (din
Canada, Italia, Statele Unite și din alte țări).

117

