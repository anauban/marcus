Protocronism și sincronism
Luceafărul, anul XXI, nr. 43 (861),
28 octombrie 1978, p. 3
Participă la dezbatere: Edgar Papu, Ov. S.
Crohmălniceanu, Pompiliu Marcea, Solomon Marcus,
Paul Anghel, Nicolae Dragoș, M. Ungheanu.
Solomon Marcus:
Am urmărit cu mare interes anul trecut cele
două numere dedicate de revista Luceafărul temei
protocronismului și sincronismului, dar aș vrea să observ
că discuția era poate prea mult colorată de specificul
disciplinei dvs., lucru explicabil de altfel, deoarece
contextul ei a fost cel al unei discuții de literatură.
Chestiunea îi interesează însă nu numai pe literați și din
acest punct de vedere limbajul ei specializat, „jargonul”
ei, a provocat dificultăți de înțelegere celor care nu erau
literați. Din felul cum au fost alese cărțile enumerate
la început de Mihai Ungheanu înțeleg că există dorința
de a extinde discuția la întreaga cultură românească,
ceea ce cred că va fi un câștig. Cred că efortul nostru ar
trebui să meargă către identificarea unui numitor comun
al literaturii cu alte discipline și domenii de activitate
212

Rãni deschise

intelectuală. Prefața cărții Din clasicii noștri făcea
observația că literatura e ultima care vine să se alăture
tentativei de a stabili prioritățile culturale românești.
Știința se dezvoltă mai vizibil decât literatura, în etape
bine marcate, fiecare etapă nouă bazându‑se pe cea
precedentă, de unde concluzia că în știință problema
priorităților, a protocronismului se rezolvă foarte
simplu. Nu este însă așa. În aceeași prefață, profesorul
Edgar Papu dă mai multe exemple din știință, dar de
valori inegale, pentru că nu toate exemplele citate
se datorează unor cercetări efectuate de specialiști.
În demonstrarea protocronismului avem nevoie de o
adevărată cercetare, nu de reluarea unor informații
neverificate. Cartea lui Radu Iftimovici este exemplară
în această privință; verifică informații aproximative.
Cercetarea poate să infirme unele dintre „amintirile”
noastre, dar poate să descopere și situații noi, de care
nu știam nimic. Este necesar să ne bizuim numai pe o
cercetare la sursă. Altfel, vine cineva cu un exemplu slab
și oferă argumente împotriva ideii, cum s‑a întâmplat în
cursul discuției despre protocronism. Unii au încercat
ca printr‑un singur exemplu, mai puțin rezistent, să
răstoarne întreaga teorie.
Problema cea mai stringentă este următoarea: în
jurul ideii de protocronism s‑au adunat foarte multe
exemple. Întrebarea care se pune este: aceste situații
213

Solomon Marcus

particulare reprezintă rezultate accidentale sau expresia
unui fenomen sistematic? Și dacă este vorba de un
fenomen sistematic, trebuie să vedem care este resortul
acestui fenomen. În ce măsură se poate vorbi de ghinion
sau de noroc în cazurile de ignorare sau de validare?
Discutând cazurile din știință, numeroase, fenomenul e,
după constatările mele, sistematic.
(…)
Orice organism intră în metabolism cu mediul
exterior. Orice cultură intră în metabolism cu alte
culturi. Ceea ce se produce în orice țară într‑un domeniu
de activitate reprezintă numai unu la sută față de ce
se produce în acel domeniu în lume. Ceea ce trebuie
să luăm de la alții este mult mai mult. Iar ceea ce dă o
cultură este mai puțin decât poate primi de la celelalte
culturi la un loc.
(…)
Din afară, poate să pară că protocroniile în știință
sunt mai bine delimitabile, dar lucrurile nu stau astfel și
voi reveni asupra acestui aspect. Întrebarea mea este: nu
putem vorbi de prioritatea originalității?
(…)
Eugen Lovinescu a văzut acest proces necesar de
osmoză a culturilor, dar în mod unilateral, pentru că
această perioadă a teoriei sincronismului trebuie văzută
în context mondial. Deceniile patru, cinci și șase sunt
214

Rãni deschise

ultimele decenii bolnave și pentru marile culturi ale
lumii. Osmoza nu funcționează în cazul lor. În domeniul
lingvisticii, de pildă, americanii îl ignorau pe Ferdinand
de Saussure, iar francezii îl ignorau pe Bloomfield și
pe Sapir. În acea perioadă cultura franceză își concepea
existența în chip aproape autarhic; la fel cultura
americană. A fost, fără îndoială, o situație maladivă
care avea inevitabil consecințe asupra întregului context
cultural al lumii. În momentul de față situația e mai
bună. Metabolismul culturilor e mai sănătos decât
înainte. Este de la sine înțeles că orice cultură dă și
primește. Dar problema care se pune acum este în ce
măsură căpătăm conștiința a ce putem să dăm, în ce
măsură știm că putem da, în ce măsură reușim să dăm, în
ce măsură această potențialitate devine realitate.
(…)
Cum reacționează prefațatorul față de această
lacună? (întrebare adresată lui M. Ungheanu)
(…)
Susțin că nu este un accident faptul că n‑a fost
recunoscut.
(…)
Pun problema astfel: care este mecanismul
promovării și recunoașterii valorilor? Care este
mecanismul aducerii lor în conștiința culturală a
omenirii? Insuccesele nu sunt accidentale. Am examinat
215

Solomon Marcus

numeroase situații și am observat caracterul lor
sistematic. Exemplul relativ la Gherea nu e un caz izolat.
Acum vreo zece ani s‑a tradus la noi o carte, Teorii
moderne ale învățării, în care contribuția remarcabilă
a școlii românești de teoria probabilității e ignorată.
Totuși, ediția românească nu s‑a sesizat de acest fapt.
S‑a tradus din franceză Istoria generală a științei (sub
redacția lui René Taton), din care cele mai multe
contribuții românești lipsesc. Notele adăugate la ediția
românească s‑au sesizat numai parțial de aceste absențe.
Un exemplu semnificativ este cel al distanței introduse
de Dimitrie Pompeiu. Felix Hausdorff o consemnează
în celebrul său tratat de teoria mulțimilor, modificând‑o
ușor; ulterior, însă, numeroși matematicieni (printre care
și români) nu mai atribuie această distanță lui Pompeiu,
nici măcar lui Pompeiu‑Hausdorff, ci exclusiv lui
Hausdorff. Handicapul unei culturi mici (ca întindere,
nu ca valoare) este acela că, de cele mai multe ori,
când vine cu o anumită contribuție, o lansează izolat,
nesusținută de o întreagă școală, de un sistem publicitar
corespunzător. Am ilustrat acest fenomen prin mai
multe exemple în volumul Din gândirea matematică
românească. Problema este în mare măsură de politică
a culturii. În momentul de față, de exemplu, cu toate
progresele realizate, avem un sistem deficitar de reviste
științifice în limbi de circulație internațională, iar aceste
216

Rãni deschise

reviste sunt insuficient diferențiate, insuficient utilizate
în schimbul cu reviste străine; capacitatea noastră de a
lansa prin propriile publicații un rezultat, de a‑l duce mai
departe în conștiința specialiștilor este mult mai mică în
momentul de față decât a altor țări. E adevărat că abia
de vreo 20‑30 de ani au luat naștere la noi adevărate
școli științifice. Până atunci, marile valori se manifestau
aproape izolat. Un Gabriel Sudan nu a putut rezista
întregului front al școlii germane a lui Hilbert, școală
în cadrul căreia s‑a manifestat Ackermann. În acest fel,
prioritatea primului exemplu de funcție recursivă, care
nu e primitiv recursivă, i‑a aparținut lui Ackermann, ea
nici măcar n‑a fost împărțită cu Sudan, care publicase
primul un atare exemplu. Abia acum, după o luptă de
câțiva ani, am obținut ca revista internațională Historia
Mathematica să consemneze contribuția lui Sudan, la
același rang cu aceea a lui Ackermann.
(Continuarea anchetei în numărul 44 (862) al
revistei Luceafărul, din 4 noiembrie 1978)
În esență, problemele legate de alternativa
protocronism/sincronism, de raportul dintre originalitate
și preluare, revin la una și aceeași chestiune a producerii
și a promovării valorilor, a aducerii lor în conștiința
publică. Nu e vorba de dispute semantice, nu e o ceartă
pentru cuvinte; e vorba de înțelegerea și ordonarea
217

Solomon Marcus

faptelor de cultură. Astfel, distincția dintre un precursor
al unui domeniu și fondatorul acestuia este esențială;
primul aparține perioadei de acumulări îndelungate
care pregătesc o nouă teorie, cel de al doilea strânge
laolaltă elementele acumulate, ridicându‑le din starea lor
disparată și latentă, organizându‑le într‑un mod explicit,
coerent și unitar. Arhimede e numai un precursor al
calculului diferențial și integral; fondatorii acestui calcul
sunt Newton și Leibniz. Prioritatea, dimpotrivă, se
referă la unul și același rezultat, obținut însă la momente
distincte, de autori diferiți. Desigur, în disciplinele
social‑umaniste (fie ele științifice sau artistice) problema
echivalenței a două rezultate, idei, metode comportă
dificultăți mai mari decât în științele exacte. E adevărat
și faptul că originalitatea, mai cu seamă în artă, se
dezvăluie prin unicate, nu prin fapte de repetiție. Dar nu
e mai puțin adevărat că unicul mod de a studia valorile
originale, de a le încadra într‑un sistem de referință, este
inserarea lor în diferite categorii, în diferite serii de ordin
logic, filozofic, social, cultural sau estetic. Nici faptele
artistice sau literare nu se sustrag (cel puțin deocamdată)
acestei constrângeri. Teme, stiluri, curente, metode,
toate intră astfel în matca inevitabilă a ordonărilor care
comportă priorități și anticipări.
(…)

218

Rãni deschise

Voi relua, pentru semnificațiile sale deosebite,
prioritatea – descoperită cu o întârziere de 50 de ani –
pe care matematicianul român Gabriel Sudan o are
asupra primului exemplu de funcție recursivă, neprimitiv
recursivă. Este vorba de un rezultat clasic, consemnat în
toate cărțile de logică matematică modernă, dar atribuit
peste tot lui Ackermann. Am prezentat radiografia
acestui caz în Din gândirea matematică românească
(1975). Vă rog acum să‑mi permiteți să prezint ceea ce
a urmat, veți vedea că lucrurile sunt semnificative pentru
întreaga problemă pe care o discutăm.
Un singur curs universitar românesc a consemnat (în
treacăt și timid) această prioritate. Cei mai mulți studenți
ai noștri învață, deci, în continuare că Ackermann e
autorul exclusiv al exemplului în cauză. Concomitent,
un articol privind această prioritate a fost trimis revistei
internaționale Historia Mathematica. Inițial, această
revistă a refuzat să‑l publice, obiectând că articolul lui
Sudan (publicat într‑o revistă românească purtând data
ianuarie‑iunie 1927) nu avea specificată data primirii la
redacție. A urmat o corespondență complicată, în care s‑a
putut arăta, prin referire la texte publice, că Ackermann
avea cunoștință, în momentul elaborării articolului său, de
cercetarea lui Sudan, după cum și Sudan avea cunoștință
de preocupările lui Ackermann. În aceste condiții,
publicarea a fost acceptată cu următorul amendament:
219

Solomon Marcus

Ackermann și Sudan sunt, deopotrivă, autorii primului
exemplu de funcție recursivă, neprimitiv recursivă.
Obiecția privind absența datei de primire la redacție
a articolului lui Sudan e îndreptățită. Consemnarea datei
de primire ține de abecedarul alcătuirii unei reviste
științifice. Acest fapt, în aparență mărunt, e simptomatic
pentru o întreagă stare de lucruri nesatisfăcătoare.
Ne‑am supus unei prime confruntări internaționale.
Ea ne‑a obligat să revizuim unele argumente, să întărim
altele. Vor urma, fără îndoială, alte confruntări pentru
a obține ca, treptat, noile monografii, sinteze, tratate
de logică matematică să insereze, alături de numele lui
Ackermann, pe cel al lui Sudan. Va fi probabil un drum
lung, nici nu suntem siguri că acest drum va fi parcurs
până la capăt.
(…)
Dacă vrem să ne încredințăm de autenticitatea
unei valori științifice, să‑i apreciem amploarea, trebuie
să acceptăm ca testarea ei să se facă pe baza unei
confruntări cu întreaga comunitate științifică mondială,
trebuie să fim pregătiți și pentru eventualitatea
infirmării unor ipoteze, să fim deschiși unor posibile
revizuiri. Trebuie nu să așteptăm această confruntare,
ci să ne pregătim pentru ea, s‑o provocăm, să venim în
întâmpinarea ei, dacă vrem să evităm pe viitor fenomene
de tip Paulescu, Sudan, Gherea. O atare confruntare face
220

Rãni deschise

parte din procesul firesc de comunicare între culturi. Ea e
necesară nu numai pentru testarea și validarea valorilor,
ci și pentru producerea lor. Ea se face de la egal la egal,
fiecare cultură având obligația de a lua în considerare
achizițiile celorlalte culturi, dar și dreptul de a pretinde
celorlalte culturi să țină seama de ceea ce ea le poate da
și de a participa la testarea și validarea valorilor pe care
ele le propun. Această atitudine nu are nimic comun cu
aceea pe care profesorul Edgar Papu o numește atât de
sugestiv „conștiință retardatară” și care, din păcate, încă
persistă ca o realitate deloc neglijabilă.
(…)
Uneori, modelul acestor cărți e preluat și în
redactarea lucrărilor românești. Conștiința retardatară
nu prea are încredere în capacitatea proprie de creație,
iar, în cel mai bun caz, dacă totuși o acceptă, consideră
că validarea valorilor este de competența exclusivă
a câtorva centre culturale străine. Desigur, această
atitudine e numai rareori mărturisită explicit, dar
numeroase situații concrete o trădează. O parte din vină
o are și comunicarea deficitară în interiorul propriei
noastre culturi. Uneori, nu sesizăm anumite valori
tocmai pentru că ne aflăm prea aproape de ele. Unde
oare să încadrăm exemple ca cele care urmează? O
importantă contribuție românească, pe deplin acreditată
în lumea științifică (dar care s‑a zbătut mulți ani pentru a
221

Solomon Marcus

fi recunoscută), se referă la teoria matematică a învățării
și este legată de numele profesorilor O. Onicescu,
G. Mihoc și M. Iosifescu. Valorosul Dicționar de
psihologie, recent apărut la editura Albatros, ignoră
această contribuție, deși consacră un lung articol teoriilor
învățării. Un alt exemplu: interesanta Introducere
în semiologia literaturii, publicată recent la Editura
Univers, ignoră semiologia literară românească, deși
tratează probleme în care autorii români au adus
contribuții deosebite. De altfel, numeroase prefețe la
cărți de teoria literaturii, cărți traduse din limbi străine,
evită să realizeze joncțiunea cu cercetările românești
corespunzătoare. Subaprecierea de către unii tineri (și
mai puțin tineri) a maeștrilor criticii literare românești,
la care se referea profesorul Pompiliu Marcea, face deci
parte dintr‑un fenomen mai amplu, în care se înscrie și
minimalizarea contribuțiilor românești recente de teorie
și semiologie literară. Unele dintre aceste contribuții sunt
mai degrabă cunoscute și folosite (fiind traduse) în afara
României.
(…)
Nu cumva și acum riscăm ca abia peste 10, 20 sau
50 de ani să descoperim unele priorități apărute în anul
1978? Nu cumva sub ochii noștri apar valori pe care nu
le punem în evidență în mod adecvat și, din acest motiv,
ele nu sunt receptate corespunzător de cultura noastră
222

Rãni deschise

și, cu atât mai mult, nu sunt receptate în lume? Desigur,
e greu să se facă o evaluare precisă a unor contribuții
foarte recente. Sunt însă convins că unele manifestări de
originalitate în cultura noastră actuală ar căpăta o șansă
mai bună în competiția internațională a valorilor dacă am
găsi posibilitatea de a le aduce într‑un mod mai pregnant
și mai organizat în atenția specialiștilor. Mă gândesc, de
pildă, la splendida echipă de compozitori și muzicieni
care se manifestă de mai mulți ani la interferența
semioticii muzicale cu informatica și teoria limbajelor
(un Brediceanu, un Stroe, un Vieru, un Niculescu, un
Mețianu, un Ciocan, un Nemescu, un Brânduș). Ei s‑au
manifestat fiecare pe cânt propriu, în câte o revistă, nu
dintre cele mai citite, dar ar merita mult mai mult, pentru
că ei constituie un adevărat front care trebuie lansat ca
atare. Exemple de acest fel mai sunt.
(…)
Mă voi referi acum la afirmația tovarășului Mihai
Ungheanu: „Să urmărim succesul intern al valorilor,
decisiv pentru cel extern” și la aceea a tovarășului Adrian
Marino din Prezențe românești și realități europene:
„Nimeni nu se poate impune în străinătate dacă nu s‑a
impus mai întâi în țară.” Urmărirea succesului intern
al valorilor mi se pare o adevărată comandă socială.
Promovarea unei valori la scară națională asigură
formarea acelui front de susținere de care au nevoie
223

Solomon Marcus

valorile pentru a putea intra în universalitate. Desigur,
nu toate valorile sunt de aceeași mărime, dar toate au
dreptul să intre în competiție, iar rezultatul acestei
competiții poate aduce surprize care ne obligă la
reevaluările necesare. Global, condiționarea succesului
internațional de succesul la scară națională corespunde
realității. Individual, însă, se știe că lucrurile sunt mai
nuanțate. Cunoaștem destule personalități care s‑au
impus în lume înainte de a se impune în țara lor. Ar fi
regretabil să neglijăm această posibilitate. Cultura
românească, de exemplu, nu a receptat încă toate valorile
pe care ea le‑a produs. (Un singur exemplu: a intrat oare
Matila Ghyka, un nume de prim rang în studiul evoluției
formelor, în conștiința noastră culturală? Nici măcar nu
e consemnat în Mic dicționar enciclopedic.) Desigur,
ar fi de dorit ca locul pe care o personalitate îl ocupă în
cultura românească să nu fie în discordanță cu locul ei
în cultura universală. Anomaliile care există în această
privință provin fie dintr‑o greșită evaluare a lor la noi sau
în străinătate, fie din deficiențe care apar în procesul de
comunicare între culturi.

224

