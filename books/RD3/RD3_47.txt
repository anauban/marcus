Despre matematică și viață
Valențe, revista Liceului nr. 2,
Municipiul Onești, 1984, pp. 5‑6
Interviu realizat de prof. Maria Grigoriu.
M.G.: Stimate tovarășe profesor, sunteți una din
personalitățile științifice românești cunoscute pe plan
național și internațional. Fiu al județului Bacău, păstrați
desigur amintiri trainice despre începuturile studiilor
dvs. aici, despre dascălii pe care i‑ați avut. Ce calități
credeți că ar trebui să aibă un profesor de matematică
pentru a‑și face înțeles și îndrăgit obiectul?
S.M: La Liceul de băieți, azi Liceul „George
Bacovia”, am beneficiat de mulți profesori care m‑au
impresionat prin profesionalitatea și conștiinciozitatea
lor și cărora le păstrez o frumoasă amintire.
În ceea ce privește arta profesorului de matematică
de a‑și face îndrăgit obiectul, cred că ea constă în a ști
cum să oferi o recompensă (intelectuală) celui pe care
l‑ai invitat la un anumit efort (intelectual). Pedagogia
matematică suferă încă de insuficiența motivațiilor pe
baza cărora dorința de a parcurge raționamente și calcule
aride ar părea justificată.
291

Solomon Marcus

M.G.: Cercetările dvs. deosebit de interesante
într‑un domeniu de graniță al cunoașterii, legarea
disciplinelor umaniste de științele matematice, ceea
ce părea până acum vreo 10 ani greu de realizat și de
înțeles, au stârnit un viu interes. Sunteți autorul lucrării
Poetica matematică, apărută în 1970, tradusă în mai
multe limbi. Ați putea să ne spuneți ce contribuție aduce
acest studiu în analiza literaturii?
S.M: La 13 ani de la apariția Poeticii matematice,
este cazul ca despre contribuțiile ei la analiza literară
să nu vorbească autorul, ci ecourile pe care cartea le‑a
stârnit. Capitolele care au avut cel mai larg ecou printre
specialiști sunt cel privitor la analiza limbajului poetic
în raport cu cel științific și capitolul final, care propune
un model matematic pentru strategia personajelor în
teatru. Desigur, acest ecouri includ și contestații, rezerve,
nedumeriri, dar de cele mai multe ori sub forma unor
critici fertile. Eu însumi m‑am distanțat polemic de
versiunea din Poetica matematică a raporturilor dintre
limbajul științific și cel poetic, așa cum se poate vedea în
cartea mea Din gândirea matematică românească.
S‑au manifestat însă și reacții care printr‑un efort
minim căutau să obțină o contestație maximă. Unii
eseiști și critici literari consideră, în mod generic,
metodele matematice drept o manifestare snoabă, prin
care, într‑un limbaj critic se enunță adevăruri bine
292

Rãni deschise

cunoscute.de obicei, autorii unor astfel de aprecieri au
prudența de a rămâne la afirmații generale, globale.
De altfel, ca unul care a dobândit o lungă experiență
în aprecierea matematicii în domeniul social‑umanist,
trebuie să observ că acest tip de reacție apare ori de
câte ori gândirea matematică începe să pătrundă într‑o
anumită disciplină. Cei care nu mai au resursele ori
disponibilitatea care să le permită o anumită deschidere
față de noile metode își caută instinctiv o justificare la
adăpostul căreia atitudinea lor de respingere să apară ca
motivată.
M.G.: Care sunt avantajele folosirii matematicii
în înțelegerea mai profundă a picturii, sculpturii, așa
cum reies acestea din lucrarea colectivă Semiotica
matematică a artelor vizuale?
S.M: Sculptura lui Brâncuși, de exemplu, pictura
lui Mondrian, Mânăstirea Văcărești, problemele de
estetică industrială, proiectele de arhitectură apar ca
rezultat al activității unor gramatici mult diferite de cele
care generează limbile naturale. Însă realizarea acestei
idei cere eforturi considerabile. Sunt puse în mișcare
domenii moderne ale matematicii, ca teoria categoriilor
(în special pentru înțelegerea teoriei formei și culorii în
pictură).
M.G.: În cadrul seriei de studii editate de Academia
R.S.R. sub titlul Probleme globale ale omenirii, a apărut
293

Solomon Marcus

în 1982 volumul Metode matematice în problematica
dezvoltării, al cărui coordonator sunteți.
Dinamica psihologiei individuale, a relațiilor
sociale, incertitudinea relațiilor internaționale în ceea ce
privește dezvoltarea pot accepta un studiu matematic?
S.M: Lucrarea Metode matematice în problematica
dezvoltării este fructul unei activități colective pe care
am coordonat‑o timp de mai mulți ani, în cadrul unui
contract al Universității din București cu Universitatea
Națiunilor Unite (Tokio). Mă întrebați dacă metodele
matematice se pot acomoda cu incertitudinile viitorului.
Deslușesc în această întrebare o presupoziție foarte
răspândită, după care gândirea matematică este adecvată
numai studiului unor fenomene precise și univoce. Însă
matematica a depășit, în ultimele decenii, restricțiile
logice aristotelice (guvernate de principiul identității, cel
al noncontradicției și cel al terțului exclus), adaptându‑se
prin strategii din ce în ce mai variate și mai complexe,
la situații paradoxale, nuanțate și imprevizibile, atât de
frecvente în procesele actuale de dezvoltare. De altfel,
o disciplină matematică clasică, teoria probabilităților,
cucerise de multă vreme domeniul întâmplătorului.
Geometriile neeuclidiene, situațiile puse în evidență în
termodinamică, în fizica relativistă și, mai ales, în cea
cuantică, cele întâlnite în biologia modernă au condus
tot mai mult la promovarea în știință a unor logici
294

Rãni deschise

neclasice, care se depărtează tot mai mult de intuiția
comună. Cu acest antrenament, matematica era desigur
pregătită să abordeze domeniul delicat al modelării
viitorului, al manipulării indicatorilor de dezvoltare.
Cartea mea, Paradoxul, care apare acum la Editura
Albatros și serialul Timpul, pe care‑l public de peste
doi ani în Viața studențească, sunt elocvente în această
privință. Ele continuă să dezvolte cartea la care v‑ați
referit în întrebarea dvs. am putea spune că istoria i‑a dat
lui Blaise Pascal, cu a sa dihotomie esprit géométrique
– esprit de finesse, o replică neașteptată. Gândirea
geometrică (adică matematica) nu numai că nu se mai
opune spiritului de finețe, dar finețea este analizată și
explicată prin ea.
M.G.: Și acum, aproape de finalul discuției noastre,
mă gândesc la o întrebare mai sentimentală: din tot ce s‑a
scris despre dvs., ce v‑a plăcut mai mult, ce vi s‑a părut
mai adevărat?
S.M: Îmi plac cei care mă critică și mă contestă,
făcând efortul de a mă înțelege. Nu‑i apreciez în schimb
pe criticii care mistifică faptele, sunt de rea credință (C.
Sorescu).
Dar Al. George, spre exemplu, e o companie plăcută
în polemică. Textul lui e delicios. Nu‑l suspectez de rea
credință.

295

