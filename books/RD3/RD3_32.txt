Știința românească în lume
Contemporanul, nr. 21 (1646), 26 mai 1978, pp. 2‑4
Orice discuție despre răspândirea în lume a științei
românești trebuie să țină seama de statutul diferențiat
pe care îl are știința în raport cu literatura și arta. Dacă
pentru literatură modul principal de afirmare este cartea,
pentru știință el este articolul de revistă. Însă nu e mai
puțin adevărat că în ultima vreme rolul cărții științifice
a crescut mult. Față de acumularea din ce în ce mai
rapidă a unor rezultate noi (și, odată cu acestea, creșterea
în importanță a revistelor internaționale de referate),
monografiile de sinteză sunt din ce în ce mai vitale și
mai apreciate de specialiști.
În ultima vreme, are loc în lume un fenomen
interesant care, probabil, va avea o amploare din ce în
ce mai mare. Tiparul nu mai poate să facă față presiunii
informaționale. La cele mai multe reviste, decalajul
dintre data primirii articolului la redacție și data
publicării sale e în creștere, ajungând uneori la 3‑4 ani.
Cărțile, de asemenea, așteaptă din ce în ce mai mult până
văd lumina tiparului. Institutele științifice, universitățile
recurg din ce în ce mai mult la mijloace de multiplicare
197

Solomon Marcus

mai rapide decât tiparul, mulțumindu‑se în schimb
cu un tiraj mai restrâns. Multe rezultate circulă sub
formă orală mult timp înainte de a fi fixate într‑un text
multiplicat; ia naștere un adevărat folclor științific. Unele
periodice experimentează forme noi de multiplicare,
cât mai reduse ca volum, pentru că problema spațiului
de depozitare devine tot mai acută pentru biblioteci.
Astfel, revista American Journal of Computational
Linguistics, organul Societății americane de lingvistică
computațională, se publică sub formă de microfilm.
Are loc o diversificare din ce în ce mai accentuată a
periodicelor de specialitate, aproape fiecare capitol al
unei științe beneficiind de propria sa revistă. Apar multe
reviste noi, dedicate domeniilor interdisciplinare în
formare.
Față de aceste transformări, trebuie să dăm dovadă
de multă elasticitate, în așa fel încât metabolismul
științei noastre cu știința din alte țări să funcționeze
cât mai bine. Numeroasele discuții din ultima vreme
despre diferite priorități științifice românești care nu au
căpătat recunoașterea cuvenită sunt instructive și sub
acest aspect. Ideea Editurii Academiei de a publica o
carte dedicată priorității lui Paulescu în ceea ce privește
descoperirea insulinei este deosebit de lăudabilă. Dar din
examinarea cazului Paulescu, ca și a atâtor altor situații
similare, trebuie să desprindem învățăminte privind
198

Rãni deschise

o mai bună difuzare a rezultatelor noastre științifice,
pentru a preveni noi pierderi de priorități. Știința noastră
este încă relativ tânără, nu trebuie să uităm că înainte
de război existau, ce‑i drept, savanți români de mare
valoare, dar organizarea instituțională, la nivel național,
a cercetării științifice este de dată mai recentă. Cărțile
științifice erau înainte de război o raritate. Astăzi,
numărul monografiilor științifice de matematică, de
exemplu, este la noi de ordinul sutelor, poate chiar
al miilor: numai monografiile publicate de români la
edituri străine depășesc cifra de 100. Dar aceasta nu ne
împiedică să observăm că unele posibilități de coeditare
cu edituri străine au fost ratate din motive birocratice
și că mecanismul însuși de ducere a tratativelor între
editurile românești și cele străine în vederea difuzării
cărților românești în străinătate și a traducerii în
românește a unor cărți străine este încă foarte greoi,
prelungindu‑se uneori de‑a lungul a câtorva ani și
împiedicându‑se câteodată de chestiuni mărunte. Multe
posibilități promițătoare nu au fost încă suficient tratate.
De exemplu, avem motive serioase să credem că, în
țările de limbi latine din Europa și din America de Sud,
cartea științifică românească ar putea fi difuzată uneori
chiar în limba română.
Cel puțin la fel de importantă e problema
periodicelor științifice românești. Modul în care au fost
199

Solomon Marcus

ele organizate în urmă cu vreo 20‑25 de ani corespundea
necesităților de atunci. Între timp însă, nu am ținut
întotdeauna pasul cu modificările survenite, despre care
am vorbit în prima parte a acestui articol. Astfel, faptul
că nu avem o revistă românească de informatică în
limbi străine ne împiedică să dăm prezenței românești
în acest domeniu o afirmare mai convingătoare. Mai e
și un alt aspect. O revistă științifică e un fel de monedă
cu care cumpărăm revistele științifice de profil similar
din alte țări. Dar astăzi nu se mai poate obține o revistă
străină de informatică sau matematică în schimbul unei
reviste științifice care nu e profilată pe același domeniu.
Deci, este necesară o diversificare corespunzătoare a
periodicelor științifice românești pentru a mări în acest
fel valoare lor de schimb.
În raport cu răspândirea tot mai mare a publicațiilor
științifice netipărite, nu putem spune că am dezvoltat
în mod corespunzător aceste mijloace de multiplicare,
pentru a câștiga în operativitate, în ceea ce privește
anunțarea rezultatelor noi pe care le obținem. Institutul
național pentru creație științifică și tehnică (INCREST)
publică preprinturi cu rezultatele cele mai proaspete;
specialiștii din alte țări iau imediat cunoștință de ele.
Este păcat că acest procedeu nu capătă o mai mare
răspândire în universitățile și institutele noastre de
cercetare.
200

Rãni deschise

Știința românească este azi mult mai cunoscută în
lume decât în trecut în primul rând pentru că producția
noastră științifică a crescut simțitor. Astfel, numai în
numărul pe decembrie 1977 al revistei internaționale de
referate în domeniul matematicii sunt recenzate 38 de
lucrări românești. Unele monografii de sinteză publicate
de cercetători români au devenit cărți de referință pentru
specialiștii din întreaga lume; mă gândesc, de exemplu,
la monografia lui Ciprian Foiaș (în colaborare cu Sz.
Nagy) asupra operatorilor în spații Hilbert, la aceea a lui
Sergiu Rudeanu privind ecuațiile booleene sau aceea a
lui Silviu Guiașu despre teoria informației și aplicațiile
ei. Este necesar, prin urmare, nu numai să menținem
această cotă de afirmare a științei românești în lume, ci și
s‑o dezvoltăm în continuare cât mai mult cu putință.

201

