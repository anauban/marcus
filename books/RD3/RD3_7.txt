În prim‑plan matematica
Revista Colocvii, anul V, nr 12 (54),
decembrie 1970, pp. 4‑5
Vasile Ștefănescu: Vă este cunoscut, probabil, că
suprasolicitarea elevilor la matematică este determinată
și de volumul sporit de cunoștințe care se transmit în
cadrul unor discipline matematice (algebră, geometrie,
trigonometrie) ce se predau astăzi în școală. Predarea
acestor cunoștințe este urmată, în mod frecvent, de
exerciții, uneori greoaie și artificiale, care răpesc
mult timp. Considerați, din acest punct de vedere,
că reorganizarea predării acestor discipline ar crea
posibilități pentru introducerea în programe a unor
cunoștințe care au o valoare mai mare în ceea ce privește
cultura generală matematică a elevilor?
Solomon Marcus: Mi‑am spus de mai multe ori
părerea în această privință; a se vedea, de exemplu,
articolul meu Sugestii pentru o înnoire a predării
matematice în școala de cultură generală, publicat în
Gazeta matematică seria A, în anul 1960. În esență, cred
că problema se pune în modul următor: modernizarea
predării matematicii în școala de cultură generală este,
44

Rãni deschise

în primul rând, o chestiune de eliminare, și numai în
al doilea rând o chestiune de adăugare. Cred, cu alte
cuvinte, că sarcina cea mai urgentă a învățământului
matematic este aceea a eliminării imensului balast cu
care este încărcată programa, cu care sunt încărcate
manualele, cu care este copleșită mintea elevilor. La
geometrie și algebră, dar mai ales la trigonometrie, o
cantitate imensă de chestiuni lipsite de semnificație și de
perspectivă se transmite în mod rutinar de la manualele
vechi la cele noi, de la programele vechi la cele noi, de
la profesorii vârstnici la cei tineri. O listă aproximativă
a lor am dat‑o de mai multe ori, ultima oară cu prilejul
unei emisiuni la televiziune în urmă cu aproximativ
un an. Sugerez Ministerului Învățământului formarea
urgentă a unei comisii care să inventarieze toate aceste
chestiuni care se mențin ca niște adevărați paraziți ai
programelor și ai manualelor de matematică, scutind de
ele măcar generațiile noi de elevi. Mă ofer să funcționez
ca membru al acestei comisii. Cred că simplul fapt al
degrevării programei și manualelor de aceste chestiuni
inutile ar constitui un progres imens al învățământului
matematic, un pas esențial în procesul de modernizare,
chiar dacă, deocamdată, nu ne putem încă decide ce
chestiuni noi ar trebui introduse în loc. În orice caz, cred
că ceea ce trebuie introdus în programă e mai puțin decât
ceea ce trebuie scos. Lucrurile care trebuie introduse
45

Solomon Marcus

(programare, matematica unui calculator electronic,
grafuri, numere cardinale, elemente de geometrie
neeuclidiană, elemente de teoria jocurilor, elemente de
teoria informației, funcții recursive, decidabilitate etc.)
sunt mai interesante, mai plăcute, mai utile și mai ușoare
decât cele care trebuie scoase.
V. Șt.: Am avut deosebita plăcere să urmăresc la
televizor o instructivă lecție de analiză, pe care ați ținut‑o
în cadrul programului pentru școli. Se cunoaște că prin
contribuția Catedrei de Analiză a Facultății de matematică
din București școala de azi beneficiază de o programă și
un manual apreciate atât în țară, cât și peste hotare.
Dar, așa cum dumneavoastră ați arătat, în rezolvarea
unor probleme se mai comit erori într‑o serie de lucrări
destinate analizei în școli. Care sunt, după părerea
dumneavoastră, principalele direcții în care se manifestă
mai pregnant necesitatea ridicării reale a nivelului
științific al predării analizei în liceu?
S.M.: Cred că trebuie pus accentul pe acele
chestiuni de analiză care modelează nemijlocit aspectele
fundamentale ale lumii fizice: funcția, derivata,
integrala și ecuațiile diferențiale. Un concept cum este
cel de continuitate, foarte important în laboratorul
intern al matematicianului, este mai puțin important
din punctul de vedere al culturii generale. Este, de
asemenea, important să se puncteze surprizele pe care
46

Rãni deschise

le aduc procesele cu o infinitate de etape, așa cum sunt
cele pe care le are în vedere Analiza matematică; să se
pună în evidență faptul că intuiția noastră, formată prin
observarea unor procese cu un număr finit de etape, este
de mai multe ori contrazisă atunci când avem în vedere
mulțimi infinite.
Numeroasele greșeli care se comit încă în
formularea și rezolvarea problemelor și exercițiilor
de analiză matematică provin din faptul că în tratarea
acestor chestiuni aplicative se pierde din vedere
controlul, la fiecare pas, al legitimării teoretice a pasului
respectiv; se creează astfel iluzia că rezolvarea practică
a problemelor se poate face ignorându‑se definițiile și
teoremele.
În felul acesta, elevul încetează să mai creadă în
importanța rigorii matematice, atât de mult trâmbițată
din partea teoretică, și își reprezintă matematica sub
forma unei colecții de rețete.
V. Șt.: Ridicarea nivelului științific al predării
matematicii în școală și adaptarea conținutului
disciplinelor matematice noilor cuceriri din domeniul
științelor matematice sunt imperative majore ale epocii
în care trăim. Care sunt direcțiile principale teoretice
și aplicative din domeniul științelor matematice care ar
avea un pronunțat caracter formativ, dacă s‑ar preda în
liceu într‑o versiune mai elementară?
47

Solomon Marcus

S.M.: Am enumerat, în cadrul răspunsului la prima
întrebare, unele dintre domeniile noi care ar trebui
înglobate în programa școlilor de cultură generală.
Uneori s‑a făcut greșeala de a se considera, mărturisit
sau nu, că scopul principal al școlii de cultură generală
este pregătirea elevilor pentru învățământul superior.
Unele manuale poartă vizibil amprenta acestei optici.
Însă, după cum se știe, majoritatea absolvenților
acestor școli nu trec în învățământul superior. Ei au
nevoie de o cultură matematică generală din care să
nu lipsească un număr de noțiuni, rezultate și metode
prin care matematica influențează viața modernă și
devină utilă celorlalte discipline. Problema nu se pune
dacă să insistăm asupra teoriei sau asupra aplicațiilor,
pentru că numai o pedagogie defectuoasă a putut crea
iluzia că teoria și aplicațiile se pot dezvolta altfel
decât concomitent și că s‑ar putea efectua aplicații
fără luarea în considerare, la fiecare pas, a definițiilor
și a teoremelor. Problema este de a se implanta în
obișnuințele de gândire ale elevilor acele structuri și
procedee prin care matematica modelează aspecte dintre
cele mai variate ale lumii reale.
V. Șt.: Ce ne puteți spune despre modul în care ar
trebui să fie pregătiți elevii pentru a face față exigențelor
din institutele de învățământ superior?

48

Rãni deschise

S.M.: Examenele de matematică, la admiterea
în învățământul superior, păcătuiesc de foarte multe
ori prin insistența cu care promovează utilizarea
artificiilor și a trucurilor nesemnificative, dobândite
prin frecventarea unor meditatori care reduc matematica
la anumite procedee rutinare pe bază de rețete.
Candidații și meditatorii lor se suprasolicitează reciproc
în rezolvarea a cât mai multe probleme cu cercuri,
triunghiuri și ecuații trigonometrice derizorii, atât ca
valoare informativă, cât și ca valoare formativă. Această
structură defectuoasă a examenelor de admitere exercită
o influență profund dăunătoare asupra multor elevi,
orientându‑i pe o cale greșită. De aici, situația frecventă
a candidaților cu note mari la admitere, care ulterior
devin studenți mediocri sau submediocri.
Examenele de admitere trebuie să se orienteze,
ca și întregul învățământ matematic, spre înțelegerea
și utilizarea metodelor generale, a noțiunilor și a
rezultatelor cu cea mai înaltă valoare de cunoaștere.

49

