Criteriul estetic în știință
Viața Studențească, anul XXXII, nr. 12 (1,192)
23 martie 1988, p. 2
În autobiografia sa, Albert Einstein se referă la un
moment dat la modul în care se desfășoară procesul de
învățământ, dar o face cu un spirit critic foarte ascuțit:
„Este un adevărat miracol faptul că metodele moderne
de învățământ nu au înăbușit cu totul curiozitatea de
a cerceta; această mică și delicată plantă are nevoie de
stimulente și nu se poate dezvolta fără libertate.”
Acest citat a fost invocat de Vladimir Igorevich
Arnold atunci când a fost întrebat (Smilka Zdravkovsks,
Conversation with Vladimir Igorevich Arnold, The
Mathematical Intelligencer, vol. 9, 1987, nr. 4, pp 28 –
32) cum s‑a comportat Andrei Nikolaevich Kolmogorov
ca îndrumător al său.
Este vorba de doi mari matematicieni ai secolului
nostru, Arnold fiind discipol al lui Kolmogorov.
Acesta din urmă (între altele, și membru al Academiei
R.S.R.), cu rezultate capitale în numeroase domenii
alte matematicii, de la analiză matematică la teoria
probabilităților și de la mecanică la logică matematică,
430

Rãni deschise

a decedat în toamna lui 1987. În afară de opera sa
științifică propriu‑zisă (din care nu lipsesc nici unele
contribuții decisive în lingvistica matematică (un model
algebric al cazului gramatical) și în poetica matematică
(teoria matematică a ritmului)), Kolmogorov are
mari merite în formarea unei noi generații de mari
matematicieni, ca Y.G. Sinal, D.V. Anosov, Y.I. Manin,
S.P. Novikov și cel la care ne referim acum. Arnold
consideră că, în relațiile cu studenții și colaboratorii săi,
Kolmogorov urma cu consecvență (deliberat, sau nu)
reflecția lui Einstein. El nu explica nimic, mulțumindu‑se
să pună probleme, fără a insista asupra lor. Acorda
studenților o independență completă, neobligându‑i
niciodată să se ocupe de un lucru anume, dar așteptând
totdeauna cu mare interes o replică de la ei. „Spre
deosebire de alți profesori ai mei, Kolmogorov se
remarca prin marele său respect pentru personalitatea
studentului” observa Arnold, care și‑a însușit această
lecție și a aplicat‑o, la rândul său, în formarea
propriilor săi studenți, dintre care unii au devenit nume
binecunoscute specialiștilor. „Numărul problemelor care
mă interesează este mult mai mare decât al chestiunilor
cărora mă pot efectiv dedica; studenții mei preiau o parte
din ceea ce nu pot eu aborda”.
Arnold este necruțător cu obiceiul multor
matematicieni care complică în mod inutil limbajul
431

Solomon Marcus

matematic; aceștia, în loc să spună „Petia și‑a spălat
mâinile”, se exprima astfel: „Există un u inferior lui
zero, astfel încât imaginea lui u prin aplicația naturală u
tinde spre Petia(u) aparține mulțimii mâinilor murdare și
există un v superior lui u dar nu mai mare decât zero,
astfel încât imaginea lui v prin aplicația de mai sus
aparține complementarei mulțimii mâinilor murdare.”
După părerea lui Arnold, această situație, departe de a
fi o raritate, este foarte frecventă, iar articole ca acelea
ale lui Milnor sau Smale sunt simple excepții care
confirmă regula. Rămâne de văzut dacă amploarea
acestei anomalii este aceea pe care o pretinde Arnold,
dar de existența ei nu ne putem îndoi. Mai îngrijorător
este faptul că această utilizare patologică a jargonului
matematic s‑a răspândit în special printre începătorii
în ale matematicii, pentru a nu mai vorbi de elevi și
studenți, care de multe ori sunt incapabili să formuleze o
definiție sau o teoremă cu cuvintele limbii naturale. Însă
Arnold are în vedere limbajul articolelor din revistele
de matematică; el pretinde că nici nu prea mai citește
aceste articole, ci ia cunoștință de conținutul lor prin
intermediul relatărilor făcute de studenți sau prieteni. În
schimb, îi înțelege mai bine pe matematicienii secolului
trecut, în special pe Poincaré, iar cei mai moderni i se
par cei din secolul al XVII‑lea. Cei 200 de ani care‑i
separă pe Huygens și Newton de Riemann și Poincaré
432

Rãni deschise

i se par un pustiu matematic umplut numai cu calcule. În
faimoasele Principia ale lui Newton, Arnold a descoperit
o teoremă privind topologia integralelor abeliene, rămasă
neobservată, deoarece Newton și‑a devansat epoca cu vreo
două secole, iar în unele probleme chiar cu trei secole.
De exemplu Newton se referea la problema solidului de
revoluție cu rezistență minimă în mișcarea de‑a lungul
axei sale, într‑un mediu foarte rarefiat. Newton și‑a dat
seama că extremumul nu este neted aici, ci prezintă o
ruptură. Acum, această problemă este foarte actuală în
astronautică. Cum gândește un matematician? Cum se
formează el? Arnold crede că gândește predominant
geometric, bazându‑se, în căutările sale, mai degrabă
pe desene decât pe formule. Prima problemă de care‑și
amintește, din vremea școlarității sale: „Două bătrâne
pleacă simultan, una din A spre B, cealaltă din B spre
A. Ele se întâlnesc la orele 12 amiază, prima ajungând
apoi în B la orele 16,00 iar cealaltă ajungând în A la
orele 21,00. La ce oră au plecat la drum?” Problema era
propusă într‑un moment în care nu se studia încă algebra
iar soluția, de natură mai degrabă fizică, bazată pe ideea de
similaritate, l‑a impresionat, ca ori de câte ori, mai târziu,
i s‑au dezvăluit legături între lucruri în aparență disparate
(ca, de exemplu, puntea care leagă teoria curbelor
algebrice reale cu aritmetica formelor pătratice, prin
intermediul topologiei varietăților cu patru dimensiuni).
433

Solomon Marcus

Dezvoltarea matematicii a arătat că ramurile ei cele
mai abstracte, mai speculative se pot dovedi importante
și utile. Gr. C. Moisil relata surprinderea cu care a aflat
că logica matematică se aplică în inginerie. Acum, lumea
matematică este surprinsă de legăturile teoriei numerelor
cu criptografia și cu alte activități practice. Există un
criteriu în evaluarea șanselor unei teorii matematice?
Ca și unii predecesori iluștri, Arnold crede că singurul
criteriu operant aici este cel estetic. S. Weinberg (p. 728
din numărul pe octombrie 1986 al publicației Notices
of the American Mathematical Society) este de părere
că eficacitatea matematicii se explică prin faptul că
unii matematicieni și‑au vândut sufletul diavolului,
primind în schimb informații anticipate asupra tipului
de matematică ce se va dovedi importantă pentru știință.
Arnold crede însă (și aici părerea sa se apropie de aceea
a unor Dieudonné și Halmos) că partea cea mai mare
a producției matematice contemporane nu satisface
exigențele de ordin estetic și nu‑și va dovedi niciodată
vreo utilitate. Poate că așa a fost totdeauna și că aceasta
este o condiție indispensabilă pentru dezvoltarea părții
necesare a matematicii, conchide Arnold.

434

