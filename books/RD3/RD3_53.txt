Declarația de la Veneția
Viața studențească, anul XXXI, nr. 32 (1159),
5 august 1987, p. 2
Între 3 și 7 martie 1986, a avut loc la Veneția, din
inițiativa UNESCO, un colocviu cu tema Știința și
limitele cunoașterii. Un număr de 17 oameni de știință
de renume internațional (printre care doi laureați ai
Premiului Nobel), reprezentând discipline dintre cele mai
diverse, au realizat un dialog considerat în momentul
de față una dintre cele mai importante manifestări ale
gândirii științifice și filozofice actuale. Recent, lucrările
acestui colocviu au văzut lumina tiparului (Colloque de
Venise. La science face aux confins de la connaissance:
le prologue de notre passé culturel. Rapport final,
UNESCO, 1987, 177 pp.; La science face aux confins
de la connaissance. La Déclaration de Venise. Colloque
international. Editions du Félin, Paris, 1987, 196 pp.).
Vom prezenta în cele ce urmează unele idei
centrale care au fost expuse la Veneția. Vom începe cu…
sfârșitul, prezentând punctele importante care apar în
Declarația de la Veneția, comunicatul final al colocviului.
Participanții, animați de un spirit de deschidere și de
328

Rãni deschise

punere sub semnul întrebării a valorilor timpului de față,
au căzut de acord asupra următoarelor șase puncte.
1. Suntem martori ai unei foarte importante revoluții
științifice, în special în fizică și biologie. Această revoluție
determină mari transformări în logică, epistemologie
și în viața de fiecare zi (prin intermediul aplicațiilor
tehnologice). Însă, în același timp, se constată un
important decalaj între noua viziune a lumii provocată de
științele naturii și valorile încă predominante în filozofie,
în științele umaniste și în viața socială modernă. (Acest
decalaj este semnalat și accentuat în prefața directorului
general UNESCO la Raportul final al Colocviului de la
Veneția.) Valorile umane sunt încă într‑o largă măsură
tributare determinismului mecanicist, pozitivismului
sau nihilismului. Un atare decalaj este foarte dăunător,
constituind o amenințare la adresa speciei umane.
2. Cunoașterea științifică a ajuns, prin chiar
dezvoltarea ei internă, la acele limite la care ea poate
începe dialogul cu alte forme de cunoaștere. În ciuda
diferențelor fundamentale dintre știință și tradiție, ele nu
se află în opoziție, ci într‑o relație de complementaritate.
Întâlnirea neașteptată și îmbogățitoare dintre știință
și diferitele tradiții ale lumii permite să se întrevadă
cristalizarea unei viziuni noi asupra umanității, a unui
nou raționalism.

329

Solomon Marcus

3. Refuzând orice sistem închis de gândire,
orice nouă utopie, se constată urgența unei cercetări
cu adevărat interdisciplinare, în cadrul unui schimb
dinamic între științele „exacte”, științele „umane”,
artă și tradiție. Într‑un anume sens, această abordare
transdisciplinară este înscrisă chiar în creierul nostru,
prin interacțiunea celor două emisfere ale sale. Studiul
concomitent al naturii și al imaginarului, al universului
și al omului ar putea astfel să ne apropie mai bine de
real, permițându‑ne să facem față mai ușor diferitelor
provocări ale timpului nostru.
4. Prin prezentarea liniară a cunoștințelor,
învățământul convențional al științei ascunde ruptura
dintre știința contemporană și viziunile depășite ale
lumii. Este urgentă cercetarea unor noi metode de
educație, în cadrul cărora formele cele mai avansate
ale științei să se armonizeze cu marile tradiții culturale.
UNESCO ar putea fi o organizație adecvată promovării
acestor idei.
5. Provocările timpului nostru – primejdia
autodistrugerii speciei umane, provocarea informaticii,
a geneticii etc. – accentuează responsabilitatea socială
a oamenilor de știință. Chiar dacă oamenii de știință nu
pot decide cum să se aplice propriile lor descoperiri, ei
nu trebuie să asiste pasiv la folosirea nesăbuită a acestor
descoperiri. Amploarea provocărilor actuale impune, pe
330

Rãni deschise

de o parte, informarea riguroasă și permanentă a opiniei
publice, pe de altă parte, crearea unor organe de orientare
și chiar de decizie de natură pluri‑ și transdisciplinară.
6. Se exprimă speranța că UNESCO va urmări
această inițiativă, stimulând o reflecție dirijată spre
universalitate și transdisciplinaritate.
Scriitorul și editorul francez Michel Random,
autor a numeroase lucrări privind relația dintre știință
și tradiție, trasează liniile directoare ale colocviului
de la Veneția și, în mod special, caracterizează spiritul
întâlnirii și al Declarației de la Veneția și consideră că
acest colocviu marchează o cotitură în istoria științei
și a filozofiei. Pozitivismul secolului al XIX‑lea se
estompează. Imaginarul omului și al lucrurilor este
nelimitat, iar universul, ca și matematica, este un
câmp al posibilului. Nu mai este vorba de o cărămidă
fundamentală, ci de un tot care este el însuși o cărămidă
a unui tot și mai cuprinzător ș.a.m.d. Aceasta este
viziunea holistică preconizată de știința actuală. Noul
realism al fizicienilor constă în a nu mai separa natura
interioară de natura exterioară a lucrurilor. Universul ne
apare în funcție de ochelarii prin care‑l privim, dar el își
trăiește propria sa realitate, dincolo de observațiile care
substituie naturii realului conceptele noastre.
Se preconizează astfel un mod de a vedea situat
la toate granițele: ale observației, ale definiției, ale
331

Solomon Marcus

conceptelor de spațiu‑timp, ale exprimabilului, ale
observabilului, ale comprehensibilului. Nu mai suntem
cosmonauți ai universului, ci universul este cel care, ca
o bandă a lui Möbius, ne întoarce asupra noastră și ni se
relevă.

332

