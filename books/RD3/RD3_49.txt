Timp și paradox
Ateneu, nr. 5 (210), mai 1987, pp. 4 și 7
Interviu realizat de Constantin Călin.
C.C.: Într‑o recentă lucrare susțineți că paradoxul
constituie azi o ipostază a unei stări de sănătate, de
normalitate. Cum trebuie să înțelegem afirmația dvs.?
S.M.: Teza mea relativă la paradox este mult
mai puternică. Nu numai că paradoxul a devenit, în
multe cazuri, un simptom al unei stări de sănătate, dar
incapacitatea de a detecta aspectul paradoxal‑antinomic
al unui proces mai complex trebuie interpretată ca un
semn al faptului că nu am realizat o înțelegere profundă
a procesului respectiv. Desigur, avem în vedere aici
nu numai paradoxurile logico‑sintactice, ci și pe cele
semantice și pe cele pragmatice. Orice cititor atent al
cărții mele, Paradoxul (1984), înțelege că am scris
această carte tocmai pentru a pune în evidență acest
nou statut al paradoxului. Teorema lui Gödel, teoria
relativității, mecanica cuantică sunt tot atâtea fenomene
de normalitate, de inevitabilitate a paradoxului în
matematică și fizică. Prin teoria sistemelor și prin
viziunea sistemică, paradoxul devine forma obișnuită
303

Solomon Marcus

de a fi a fenomenelor complexe din biologie și din
științele social‑umaniste. St. Lupasco a demonstrat
că atât dereglările de tip schizofrenic, cât și cele de tip
paranoic rezultă dintr‑o abolire a stării antinomice, de
tensiune între forțele contradictorii, de omogenizare
și eterogenizare, care se manifestă la nivelul
transmițătorilor neuronali ai sinapselor, caracteristice
unei stări normale. În ultimă instanță, aceste forțe
sunt cele macrofizice și cele biologice. Exemplele pot
continua la nesfârșit. A devenit un loc comun faptul că
arta are o structură autoreferențială, deci paradoxală.
Dar, dincolo de toate aceste exemple, există astăzi
pentru orice observator atent al experienței de fiecare zi,
al evenimentelor care au loc în lume posibilitatea unei
percepții directe a paradoxurilor pragmatice subiacente
vieții sociale actuale. Problemele globale ale omenirii,
hrana, energia, ecologia și atâtea altele nu pot fi înțelese
decât într‑o viziune sistemică, în care logica lui „da și
nu” este înlocuită cu una antinomică. Multe tendințe
care se observă în lume se autostimulează, dar se și
autosabotează în același timp (a se vedea, de exemplu,
Modele matematice și semiotice ale dezvoltării sociale,
Editura Academiei, București, 1986).
C.C.: Ați scris o pasionantă carte despre timp. Am
sesizat chiar în prefața ei o undă de lirism, surprinzătoare
sub condeiul unui savant, atunci când vă referiți la
304

Rãni deschise

timpul uman. Ce valoare dați propriului dvs. timp?
S.M.: Înaintarea în vârstă cere o stăpânire tot mai
bună a timpului, un control tot mai riguros al percepției
temporale. Fără aceste măsuri de precauție, putem
deveni victime ale unui timp liniar, generator de panică,
sub obsesia momentului final care se apropie. Cu știință
și înțelepciune însă, putem transforma această viziune
liniară, catastrofică, într‑una circulară, a unui prezent
continuu, în care accentul cade pe periodicitate unor
activități individuale și sociale. Așa cum viața este o
insulă de descreștere a entropiei într‑un ocean de creștere
entropică, prin antrenament adecvat putem schimba
viteza de scurgere a timpului subiectiv, împotriva
tendinței generale, puse în evidență de Thomas Mann,
după care la bătrânețe duratele scurte (orele, zilele) trec
încet, iar cele lungi (anii) trec repede, în contrast cu
ceea ce se întâmplă în anii de tinerețe. Într‑adevăr, pe
ce se bazează această situație? Pe faptul că în tinerețe
acuitatea impresiilor este mai puternică (fapt care explică
diferența de percepție a duratelor scurte), iar la bătrânețe
raportarea duratei evenimentelor la întreaga durată trăită
micșorează considerabil perioadele lungi, ajungându‑se
la bine cunoscuta impresie concretizată în metafore ca
„anii zboară”. Dar prin participare, prin antrenament
al atenției, al observației și al muncii intelectuale, prin
lectură și prin elaborare proprie putem menține, și uneori
305

Solomon Marcus

chiar amplifica, acuitatea unor percepții. În carte, l‑am
citat pe Diderot: „Munca, între alte avantaje, îl are și pe
acela că scurtează zilele, dar lungește viața.”
În Muntele vrăjit, Thomas Mann ne spune, prin
vorbele personajului său Joachim: „Timpul trece foarte
încet când ești cu ochii pe el.” Tocmai în evitarea
acestei situații constă cea mai bună strategie a percepției
temporale. Desigur, există activități care trebuie
efectuate la oră fixă sau să aibă o durată prestabilită.
Controlul ceasului este atunci inevitabil. Dar dincolo
de această urmare exterioară, care intră de obicei în
reflexele noastre, timpul este bine folosit exact atunci
când nu este urmărit. Așa cum preocuparea de a fi
spontan este o piedică în calea spontaneității, obsesia
timpului care se scurge sabotează folosirea sa adecvată.
C.C.: Eu și cei care vor citi textul acestui interviu
vom spune că timpul consumat cu lectura lui e un
„timp câștigat”. Mă îndoiesc însă că același lucru l‑ați
putea spune și dvs. De aici, două întrebări: ce înțeles
dați expresiei „timp pierdut”? Și: care e atitudinea dvs.
față de activitățile de popularizare (în special față de
literatura de acest gen)?
S.M.: Metafora timpului pierdut este foarte
ambiguă, ea se poate referi la timpul deja trăit sau
consumat, deci la tot ceea ce ține de trecut în sens
cronologic (așa cum se întâmplă la Marcel Proust)
306

Rãni deschise

sau la intervale temporale utilizate nepotrivit. Gradul
de inadecvare diferă. Pot considera timp pierdut orice
interval temporal utilizat altfel decât doream. O atare
mentalitatea poate deveni stresantă. Dacă, de exemplu,
îmi doream ca între orele 16 și 20 să citesc o anumită
carte și sunt împiedicat să fac aceasta, pot reacționa în
moduri foarte diferite. Mă pot enerva, nemaifiind în
stare să dau randament într‑o altă activitate pe care
aș putea‑o desfășura în perioada respectivă. Pot însă
manifesta o anumită flexibilitate, un anumit spirit de
inițiativă, trecând la o altă activitate, de asemenea utilă,
chiar agreabilă. O mare parte din timpul pierdut își are
originea în timpul de așteptare. Starea de așteptare
creează o disponibilitate pe care, din păcate, puțini știu
să‑o folosească. În așteptarea unui eveniment, unii
își mobilizează întreaga atenție asupra evenimentului
respectiv; în acest fel, așteptarea devine, subiectiv, tot
mai lungă și mai stresantă. Alții, mai inteligenți, știu să
treacă la o activitate concomitentă, de exemplu, lectura
unei cărți sau a unei reviste. Există o distincție netă
între timpul trăit ca pierdut și timpul care apare ca atare
într‑o viziune a posteriori. Examinarea atentă a acestei
discrepanțe este foarte instructivă pentru modul în care
ne organizăm activitățile viitoare. Există și un timp viitor
ipotetic pierdut, pe care nu totdeauna reușim să‑l evităm.
Se poate însă întâmpla ca acest timp să nu fie trăit ca
307

Solomon Marcus

pierdut. Am cunoscut tineri care s‑au îndreptat spre o
anumită facultate sub presiunea părinților, dar împotriva
propriilor dorințe și care, după câteva luni de activitate
studențească, s‑au atașat de specialitatea respectivă și au
dat un randament bun.
În ceea ce privește „activitățile de popularizare”,
cred că este necesară o precizare. Există două accepțiuni
ale „activității de popularizare”. Prima se opune
activității originale, de cercetare, a doua are în vedere
accesibilitatea pentru un public mai larg. Însă din
necesitatea „democratizării informației științifice”, deci
a explicării mai simple a unor idei și rezultate care, în
jargonul de specialitate, sunt greu de pătruns, s‑a dedus
un fel de reciprocă, după care tot ceea ce este inteligibil
pentru un public mai larg reprezintă o activitate de
popularizare, deci lipsită de originalitate, iar textele
inteligibile sunt, cu o mare probabilitate, texte profunde.
Cred că această mentalitate trebuie părăsită, însăși ideea
activităților de popularizare părându‑mi‑se depășită
de realități. Ceea ce se urmărește prin cărțile de știință
scrise în limba naturală este, de multe ori, degajarea
aspectelor de largă semnificație culturală ale științei.
Din aceste cărți, specialiștii învață uneori mai mult decât
profanii. Cărți ca cele ale lui Gr. C. Moisil, Îndoieli și
certitudini sau Știință și umanism, sunt bogate în idei
originale, care s‑au constituit în puncte de plecare ale
308

Rãni deschise

unor cercetări interesante. Uneori din snobism, alteori
din comoditate (sau nepricepere) se întâmplă ca un
specialist să abuzeze de jargonul domeniului său, să
folosească acest jargon mult peste limitele necesare
și permise, complicând astfel inutil lucrurile și ratând
motivațiile și explicațiile de rigoare. Această tendință
face ravagii mai ales printre tineri. Un student eminent
mi‑a înmânat recent un manuscris al unei lucrări
originale, în care, pentru a spune într‑o bună limbă
naturală că o anumită mulțime A este infinită a recurs
la o inegalitate e care nici nu o pot reproduce aici, dar
măcar unii mă vor înțelege: Cardinalul lui A este mai
mare sau egal cu alef zero.
C.C.: Există, fără îndoială, inclusiv la noi, un
interes puternic pentru semiotică. Să fie acest fapt o
modă intelectuală sau o manifestare cu perspective de
viață lungă?
S.M.: Semiotica a plătit și ea tributul utilizării
excesive a unui jargon specializat. Din această cauză,
unii au văzut în semiotică o manifestare de snobism
și au respins‑o. Însă interesul pentru semiotică are
motivații profunde, iar dezvoltarea ei vertiginoasă este
ireversibilă. Unii vor să acrediteze ideea că semiotica a
fost o modă a anilor ’70, care între timp a trecut, după
cum și structuralismul ar fi fost o modă a anilor ’60,
care între timp și ea ar fi trecut. Acest mod de a vedea
309

Solomon Marcus

este caracteristic unei culturi jurnalistice, fără o bază
mai serioasă, fără cunoașterea a ceea ce se publică
în revistele de specialitate. În anii ’60 a existat o
necesitate retorică de accentuare și explicitare a atitudinii
structuraliste. Între timp, cercetarea științifică din diferite
domenii s‑a impregnat de mentalitatea structuralistă
și nu a mai fost nevoie ca această mentalitatea să fie
subliniată. Mai mult, s‑a trecut la o viziune superioară,
aceea sistemică; această viziune include ca o
componentă absolut necesară gândirea structuralistă.
Ceva similar s‑a întâmplat și cu lingvistica matematică;
gândirea logico‑matematică a intrat în viața naturală,
de fiecare zi a lingvisticii, nemaifiind nevoie ca ea să
fie subliniată printr‑o denumire specială. Revirimentul
semioticii ar putea să urmeze o evoluție asemănătoare.
C.C.: Pentru simetria discuției vă propun, în
încheiere, să ne întoarcem la literatură: credeți că
aplicarea metodelor științifice va duce cândva la
elucidarea completă a „misterului” creației literare și
artistice, că ne va justifica întru totul sentimentul valorii?
Ce soartă rezervați în Republica dvs. impresioniștilor,
foiletoniștilor și eseiștilor, respectiv celor care se închină
încă intuiției și se conduc după busola propriului lor
gust?
S.M.: Metodele științifice nu‑și propun să înlăture
misterul creației literare, dar ne ajută să nu confundăm
310

Rãni deschise

inefabilul cu ignoranța. Există un mister care crește din
bogăția cunoașterii și altul care este efectul mizerabil al
inculturii. Fără a ne prevala de tot ceea ce cunoașterea a
dobândit, indiferent în ce domeniu al ei, putem fi oricând
victime ale unor confuzii de tipul celor menționate. Dacă
rămânem la o viziune repartizată pe cutiuțe și sertare
între care nu prea există canale de comunicare, nu putem
înțelege mare lucru nici din creația literar‑artistică.
Cunoașterea întreagă trebuie să se hrănească egal din
empiric și teoretic, din discursiv și intuitiv.

311

