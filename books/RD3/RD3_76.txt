Două însemnări despre Ion Barbu
România literară, anul XXI, nr. 53,
29 decembrie 1988, p. 8
O analogie între Ion Barbu și Dan Barbilian
Legătura dintre matematician și poet poate fi
urmărită fie încrucișat, căutându‑l pe poet în scrierile
matematice și pe matematician în scrierile literare, fie
în paralel, urmărind eventualele analogii între evoluția
scriitorului și aceea a matematicianului. Să ne oprim,
aici, asupra celei de a doua strategii. În evoluția poetului
Ion Barbu au fost distinse, în general, trei faze. Faza
inițială de aderență la „energetismul teluric”, la „materia
în ebuliție” și la „tensiunea dinamică din univers” (Dinu
Pillat) este aceea pe care Tudor Vianu o așază sub
semnul parnasianismului, dar în același timp vitalismul
nietzschean este puternic prezent aici. Această fază își
are drept corespondent, în evoluția matematicianului
Dan Barbilian, pasiunea pentru geometrie, o adevărată
obsesie a formelor vizuale, în deceniul al treilea.
Cea de a doua fază, în evoluția poetului, este
dominată de viziunea oriental‑balcanică. Formele
naturii capătă aici localizare istorică și geografică, dar
504

Rãni deschise

în același timp această carnalitate se impregnează de
elementul oniric și fantastic, convertindu‑se într‑un
univers mitic. Această fază a poeziei lui Ion Barbu își
are corespondent într‑o fază clar conturată din evoluția
matematicianului Barbilian. Este vorba de teza sa de
doctorat privind reprezentarea canonică a adunării
hipereliptice și de toate interferențele gândirii sale
geometrico‑algebrice cu analiza matematică, deci cu
fenomenele de continuitate (de exemplu, articolul
relativ la metricile riemanniene, publicat în 1936, sau
cele privind relația dintre geometriile neeuclidiene și
teoria funcțiilor, metricile suprafețelor riemanniene
închise sau spațiul riemannian al formelor binare cubice,
toate publicate în 1938). Găsim în toate aceste lucrări
preocuparea pentru o fuziune organică a formei, a
structurii cu individualizările lor în universuri concrete ,
bine localizate, în speță suprafețele și spațiile Riemann.
Cea de a treia fază este, atât pentru poet cât și
pentru matematician, etapa de supremă abstracție și
decantare a esențelor, de maximă concentrare a expresiei
și lapidaritate a formulărilor: faza din Joc secund
corespunde perfect celei din Algebra axiomatică. Așa
cum poetul din Joc secund și‑a luat distanța față de cel al
începutului vitalist, algebra axiomatică a fost considerată
faza supremă de investigație matematică. Nu putem
adera însă la atitudinea polemică a lui Barbilian din
505

Solomon Marcus

Algebra axiomatică, față de Ion Barbu din Joc secund,
atitudine în virtutea căreia prima ar fi o continuare pe un
plan superior a celei de‑a doua.
Unde‑l căutăm pe scriitor?
La Biblioteca Facultății de Matematică a
Universității din București s‑a organizat, în urmă
cu vreo doi ani, o vitrină cu lucrări științifice ale lui
Dan Barbilian. Printre exponate se aflau și unele
extrase oferite cu dedicație de către autor colegului
său, profesorul Victor Vâlcovici. Două dintre aceste
dedicații mi‑au reținut atenția. Una este semnificativă
pentru formația științifică a matematicianului‑poet
și pentru modul în care vedea schimbul de idei cu
colegii săi. Ea se află înscrisă pe un extras din articolul
Metrischkonkave Verbände publicat în Disquisitiones
Math‑ et Physicae, vol. 5, 1946, fasc. 1‑4, cu numărul
de înregistrare la Biblioteca Central Universitară B.C.U.
2668 (045). Dedicația poartă data de 23 decembrie 1946
și are următorul conținut: „Domnului Victor Vâlcovici –
elevului meu în Algebră axiomatică – de la elevul său în
mecanică. Dan Barbilian.” După cum se observă, relația
maestru‑discipol a funcționat aici în ambele sensuri. Se
știe că Dan Barbilian manifesta un viu interes pentru
axiomatica mecanicii, despre care a și făcut unele
expuneri și a elaborat o interesantă lucrare. Dar, evident,
506

Rãni deschise

acesta nu era domeniul său principal, teritoriul pe care
se putea mișca în voie; știa atunci să se așeze în bancă
și să‑i urmărească pe maeștri. Aceasta este marca de
calitate a oricărui savant autentic. La rândul său, Victor
Vâlcovici, ca și toți ceilalți matematicieni români, a
recunoscut ascendentul lui Dan Barbilian în domeniul
algebrei moderne, domeniu pe care l‑a servit ca un
adevărat maestru, timp de câteva decenii.
Mai interesantă însă mi s‑a părut cea de a doua
dedicație. Ea este oferită pe un extras din articolul
Einordnung von Lobatscheffsky’s Massbestimmungen
in eine allgemeine Jordanische Metrik, Comptes
Rendus du II‑ème Congrès des Mathématiciens des
Pays Slaves, Praga, 1934, cu numărul de înregistrare
la Biblioteca Central Universitară B.C.U. 2616 (045).
Dedicația poartă data de 28 mai 1935 și are următorul
conținut: „Domnului V. Vâlcovici – matematicianului și
scriitorului – respectuos omagiu D. Barbilian.”
Ce poate să însemne calificativul de scriitor asociat
numelui prestigiosului matematician și mecanician
Victor Vâlcovici? Nu se cunoaște ca venerabilul profesor
să fi publicat vreun text cu statut literar, să fi avut la activ
vreo poezie, vreo schiță, vreo nuvelă sau vreun roman,
nici să‑și fi încercat pana în comentarea fenomenului
literar (deși ar fi putut s‑o facă; îmi aduc aminte cu câtă
finețe mi‑a vorbit odată despre poezia lui Panait Cerna,
507

Solomon Marcus

pe care‑l cunoscuse personal). Atunci la ce scriitor se
referă Barbilian? O parcurgere a listei de publicații
al lui Vâlcovici pune în evidență câteva ipostaze ale
personalității sale: matematicianul, mecanicianul,
omul școlii, istoricul științei, filozoful științei și omul
de cultură generală, capabil să dezvolte teme de mare
interes științific în fața unui public intelectual larg.
Este, deci, clar că Barbilian l‑a descoperit pe scriitorul
Vâlcovici tocmai în aceste texte lipsite de intenție
literară, texte mergând de la articolele științifice originale
până la cele de popularizare, de la cele didactice la cele
de istoria și filozofia științei, de la scrierile care dezbat
problemele învățământului la cele economico‑sociale.
Bănuim însă că Barbilian avea în primul rând în
vedere textele științifice ale colegului său. Rigoarea și
concentrarea expresiei nu‑l împiedicau pe Vâlcovici
să participe cu emoție la itinerarul științific pe care‑l
urmărea, să fie sugestiv și să strecoare acea ușoară
îndoială care să lase loc și unor eventuale căi alternative,
pe care auditoriul sau cititorii să le poată testa. Tocmai
în acest elan al trăirii aventurii științifice l‑a descoperit
Barbilian pe scriitorul Vâlcovici.
Dar atunci, nu constituie faptul de mai sus un
adevărat avertisment privind modul în care trebuie
privită activitatea de scriitor, nu totdeauna o activitate
deliberată, care să poarte eticheta literarului, ci de multe
508

Rãni deschise

ori o manifestare care numai prin rezultate, nu și prin
intenție, aparține literaturii?
Dacă Barbilian ne invită – prin dedicația adresată
lui Vâlcovici – să‑l căutăm pe scriitor și în textele cu
statut științific, oare nu se aplică această recomandare cu
atât mai mult scrierilor matematice ale lui Barbilian? Nu
cumva există, alături de poetul Ion Barbu, și un poet Dan
Barbilian, cei doi fiind de fapt cele două fețe ale uneia și
aceleiași monede?
Involuntar, Barbilian ne‑a trasat un întreg program
de exegeză a textelor sale din punct de vedere literar.

509

