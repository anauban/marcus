Literatură și știință
Ateneu, august 1989, p. 6
A‑ți asuma viața timpului tău
1. Ce loc ocupă omul de știință în galeria
personajelor literaturii contemporane?
Sunt frecvente personajele literare cu statut de
cercetător științific, profesor, savant, dar foarte rar sunt
ele implicate în această calitate în conflictul literar.
Este vorba mai degrabă de o identificare exterioară, de
poziție socială; a se vedea romanele Vestibul și Iluminări
de Alexandru Ivasiuc. Desigur, există și alte situații, ca
acelea în care un mare scriitor face biografia unui mare
savant sau aceea a literaturii SF, care ar comporta, o
discuție specială, pe care nu o putem aborda aici.
2. În ce măsură se folosește literatura
contemporană de achizițiile recente ale științei?
Există în această privință un termen de referință
greu de atins: Eminescu. Am analizat în altă parte
(Viața studențească, septembrie 1988 – ianuarie 1989)
orizontul științific eminescian și modul în care acesta
a marcat creația poetică eminesciană. Un alt exemplu
559

Solomon Marcus

de o asemenea amploare și profunzime, în literatura
română, nu mai putem da. În cultura franceză, Paul
Valéry a fost considerat unul dintre scriitorii puternic
marcați de cultura științifică a timpului lor (mai cu seamă
după publicarea Caietelor sale). Problema nu este „de
a te folosi de achizițiile științelor contemporane”, ci de
a simți nevoia să‑ți asumi viața timpului tău în toată
complexitatea ei, cu întregul ei orizont de cunoaștere. A
tinde către aceasta, așa cum s‑a întâmplat cu Eminescu
(la care s‑a adăugat o deosebită capacitate de sesizare
a unor idei majore în știință și filozofie) înseamnă a
conferi operei literare umanitatea supremă la care ea
trebuie să râvnească. Literatura română are o tradiție
frumoasă în această privință. În acest sens, dintre
scriitorii secolului trecut putem menționa pe Ion Heliade
Rădulescu, Ion Ghica, Ion Creangă, B.P. Hasdeu, iar
în secolul nostru Lucian Blaga, Camil Petrescu și Ion
Barbu se reclamă și ei de la o viziune culturală totală.
Din păcate, mulți dintre scriitorii noilor generații și‑au
restrâns considerabil amploarea respirației epice, lirice
sau dramatice, întorcând spatele culturii științifice,
nesesizând faptul că această cultură este o componentă
organică a umanismului actual. Mai grav, unii scriitori
și filozofi caută chiar o motivație teoretică a acestei
atitudini, identificând știința actuală cu o formă de
dezumanizare. Am discutat cu alte ocazii despre George
560

Rãni deschise

Steiner. Recent, o atitudine similară, chiar mai radicală, o
manifestă filozoful francez Michel Henry (La barbarie,
Bernard Grasset, Paris, 1987), care consideră că
actualmente, pentru prima oară în istoria omenirii, știința
se îndepărtează de cultură. Dar chiar dacă nu se exprimă
într‑un mod atât de radical, mulți oameni de cultură de
formație umanistă adoptă, practic, o atitudine similară.
3. Care dintre ramurile științei au reprezentări
adecvate în poezia, proza, dramaturgia contemporană?
Problema nu este de a analiza pe discipline cum
se reflectă știința în literatură; ar fi un mode superficial,
fragmentar, de a vedea lucrurile, tocmai într‑o problemă
care cere o înțelegere globală, interactivă. În ceea ce
privește poezia, am fost impresionat de ciclul Laus
Ptolemaei, marcat în mode organic de către poetul
Nichita Stănescu pentru studiul infinitului matematic
(teoria numerelor cardinale transfinite), în care‑l
inițiase profesorul său de matematică în anii de școală.
Despre modul stângaci și superficial în care unele opere
dramatice au încercat să se prevaleze de fenomenul
pătrunderii calculatorului în diferite sfere de activitate
umană am discutat în Gândirea algoritmică (Editura
Tehnică, 1982, pp. 100 – 103).
În compensație, aș menționa câteva versuri din
frumosul poem, Omagiu pentru Gödel, al lui Hans
561

Solomon Marcus

Magnus Enzensberger (poem tradus în românește de
Marin Sorescu în Tratat de inspirație): „orice călăreț
posibil / deci chiar și Münchausen/ deci chiar și tu însuți
este un subsistem/ al unei mlaștini destul de extinse/
și un subsistem al acestui subsistem/ este smocul tău
de păr/ acest mijloc de înălțare/ pentru reformiști și
mincinoși”.
4. Ce modificări ați sesizat în discursul unor
scriitori de azi, determinate de descoperirile științifice
din ultimele decenii?
Am impresia că reacția cea mai pregnantă a
scriitorilor la valul de descoperiri științifice din secolului
nostru si la expansiunea socială a științei este aceea de
distanțare, rezervă și teamă. Noile generații de scriitori
par însă a depăși această veche prejudecată și a se
orienta spre o atitudine mai deschisa față de progresul
cunoașterii. În această ordine de idei aș observa, ca
un fenomen simptomatic, numărul tot mai mare de
informaticieni și matematicieni care fac o literatură de
bună calitate. Citiți proza scurtă a lui Gheorghe Păun
(Generoasele cercuri, Editura Albatros, 1989) și vă
veți convinge că un matematician de mare valoare nu
numai că nu este dezumanizat de matematica sa, dar
înregistrează cu acuitate mișcări dintre cele mai fine
ale sufletului omenesc. Tot în unele lucrări literare ale
562

Rãni deschise

lui Gheorghe Păun sunt surprinse aspectele umane
esențiale ale unor cercetări din domeniul informaticii sau
matematicii.
Un alt exemplu semnificativ de acest fel este cel
al poetei Irina Gorun, matematician și informatician
de deosebit talent. Decedată prematur, la vârsta de 32
de ani, Irina Gorun a debutat în literatură în 1968 iar
în 1972 a primit premiul „Ienăchiță Văcărescu”. Iat‑o,
scriind despre Eminescu: „Încununarea pietrei lui cu
flori/ e cea mai pașnică/ și mai nostalgică serbare/ în
cetatea ce freamătă de amintiri și de viitor./ Făptura
și numele lui, ce nu se pot pipăi,/ sunt primele noastre
noțiuni abstracte,/ înainte de Semn, înainte de Număr”.

563

