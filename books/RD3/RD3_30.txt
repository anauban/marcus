Viitorul… între gratuitate și responsabilitate
Orizont, anul XXIX, serie nouă, nr. 15 (526),
13 aprilie 1978, pp. 4‑5
Spre comunitatea de cultură
Într‑o epocă de atât de mare instabilitate a
instituțiilor, relațiilor și organizațiilor sociale, de
evoluție atât de rapidă a ideilor și a metodelor, este
foarte important ca în pregătirea viitorului să prezentăm
tineretului valorile perene ale civilizației și ale culturii.
Tineretul va înțelege astfel că dincolo de evoluțiile
rapide, de suprafață, există o stabilitate a năzuințelor
umane și că acest sentiment de stabilitate și durabilitate
nu‑l putem obține decât cu ajutorul istoriei. Progresul
științific, de exemplu, nu poate fi înțeles decât prin
examinarea modului în care legile, rezultatele, teoriile
cunoscute se inserează drept cazuri particulare în noile
descoperiri.

192

