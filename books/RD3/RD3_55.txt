Medicină, sociologie, morală
Viața studențească, anul XXXI, nr. 35 (1163),
2 septembrie 1987, p. 2
Jean Dausset, laureat al Premiului Nobel pentru
medicină, atrage atenția într‑un raport recent (L’impact
culturel des innovations scientifiques. La médicine
prédictive et son impact sociologique, în volumul
Colloque de Venise. La science face aux confins de
la connaissance: le prologue de notre passé culturel,
UNESCO, Paris, 1987, pp. 91‑94) asupra unei
metamorfoze prin care trece medicina. Dacă timp de
secole medicina și‑a propus în primul rând să îngrijească
și să vindece, astăzi ea urmărește cu precădere să
prevină. Dar pentru a preveni este nevoie de realizarea
unei predicții. Așa s‑a născut medicina predictivă, fază
inițială a medicinei preventive.
Două mai cu seamă sunt aspectele medicinei
predictive. Este vorba mai întâi de predicția unei
boli deja existente la fetus, dar care devine manifestă
abia după naștere. O atare predicție corespunde unui
diagnostic prenatal. Este vorba apoi de predicția unei
anumite susceptibilități față de o boală care s‑ar putea
339

Solomon Marcus

declanșa fie imediat după naștere, fie mai târziu, de‑a
lungul vieții. În evoluția acestor două noi domenii
de cercetare, un rol esențial îl are genetica, ale cărei
progrese recente permit studiul patrimoniului genetic
al unei persoane sau chiar al unui fetus. Pentru acesta
din urmă există de pe acum posibilitatea de a preleva,
începând cu a opta, a noua sau a zecea săptămână de
sarcină, un mic fragment de placentă pe baza căreia
patrimoniul genetic poate fi stabilit.
Nu este greu de întrevăzut ce consecințe sociale ar
putea avea identificarea precoce a trăsăturilor normale
sau patologice ale unui copil pe cale să se nască. Nu mai
puțin, dezvăluirea unei predispoziții pentru o anumită
boală poate avea un impact individual și social deloc
neglijabil.
Diagnosticul prenatal se aplică bolilor congenitale
și celor genetice datorate alterării unei singure gene, ca
hemofilia sau miopatiile. Malformațiile congenitale și
bolile ereditare constituie, în țările industrializate, una
dintre primele cauze de mortalitate și de morbiditate
la copii. Dacă până nu de mult un diagnostic cert era
foarte rar posibil în cazuri de acest fel, astăzi, din ce în
ce mai mult, geneticienii dispun de „sonde” genetice
specifice sau aproape specifice genei răspunzătoare de
boală. Deocamdată, aceste sonde mai greșesc uneori,
reperând o genă vecină celei bolnave, dar dificultăți de
340

Rãni deschise

acest fel vor fi depășite. Din păcate însă, aceste progrese
în materie de diagnostic nu sunt încă însoțite de progrese
similare în ceea ce privește mijloacele terapeutice
necesare, tratamentul care trebuie aplicat. Sunt lesne
de înțeles implicațiile delicate, de ordin individual,
moral și social, ale acestei situații. Apare chiar
problema oportunității impunerii unor teste sistematice
ansamblului populației. Astfel, în Sicilia, depistarea
talasemiei (sau anemiei mediteraneene) prin mijloace
genetice are un caracter sistematic. Drept rezultat,
numărul copiilor homozigoți a scăzut la minimum. În
general s‑a constatat că dacă o afecțiune gravă este
resimțită de întreaga populație, aceasta acceptă și chiar
pretinde măsuri de natură globală și sistematică. Apare
însă și aspectul pecuniar al problemei. Dacă depistarea
cazurilor de boală nu este prea costisitoare, îngrijirea
copiilor atinși de talasemie este foarte scumpă, iar
efectul este, în cel mai bun caz, prelungirea vieții până
la pubertate. Insula Cipru a fost confruntată cu această
problemă.
Și mai complexă este situația în cazul bolilor
datorate nu unei singure gene, ci unui ansamblu de
gene, care favorizează apariția anumitor boli. Aici intră
diabetul juvenil, care necesită un tratament continuu cu
insulină. Gena vinovată de această boală este, probabil, o
variantă a grupelor de țesuturi (HLA). Prin determinarea
341

Solomon Marcus

grupei de țesuturi putem cunoaște procentajul de risc
pentru un individ de a contracta această afecțiune. Acest
risc este de 50% pentru fratele geamăn al unui bolnav,
15‑20% pentru un frate sau o soră având aceeași grupă
de țesuturi ca bolnavul. Prin mijloace genetice se poate
preciza considerabil riscul de contractare a bolii în
diferite grupuri sociale. Astfel, în Franța se știe că unul
sau doi copii din 3.000 vor deveni diabetici în copilărie
sau adolescență. Acum se vizează distrugerea masivă a
acelor celule ale pancreasului care secretă insulină. Va
trebui oare să se instituie un control sistematic asupra
tuturor copiilor în ceea ce privește riscul de contractare
a diabetului? Decizia implică parametri medicali,
psihologici, sociologici și financiar. În orice caz, diabetul
juvenil va servi drept model în abordarea altor afecțiuni,
mai puțin răspândite dar mai grave, cum ar fi scleroza în
plăci.
Este perfect plauzibil ca în curând să se treacă la
elaborarea unor adevărate cataloage de mărci genetice
asociate anumitor afecțiuni, în funcție de structura
ADN‑ului, molecula‑suport a programului nostru
genetic. Poate că în acest fel se va putea stabili, pentru
fiecare individ, un „profil genetic”, adevărată prognoză
a bolilor care‑l amenință. Dar iarăși ne confruntăm cu
o problemă socială și morală: este oare bine ca orice
persoană să fie avertizată de bolile care o pândesc
342

Rãni deschise

chiar și în aceste cazuri (cum ar fi unele tumori) în care
știința nu este încă în măsură să le prevină? Există fără
îndoială un interes social de a repertoria calitățile și
defectele genetice ale indivizilor: dar corespunde aceasta
interesului fiecărui individ în parte?
Jean Dausset merge mai departe și își
exprimă convingerea că nu numai unele boli, dar și
comportamentul uman se află sub controlul mai multor
gene. Cunoașterea corespondenței dintre anumite
combinații de gene și anumite comportamente ar avea
consecințe extraordinare: dar nu cumva aceste consecințe
ar putea fi manipulate în scopuri nedorite?
Raportul lui Dausset este o pledoarie pentru ceea
ce el numește o „mișcare universală a responsabilității
oamenilor de știință”.

343

