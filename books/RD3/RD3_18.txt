Cum formăm și cum utilizăm competențele
Era socialistă, anul LV, iunie, nr. 11, 1975, p. 45‑47
Corelarea concomitentă a procesului de învățământ
atât cu cercetarea cât și cu producția constituie o
problemă mult mai complexă decât ar putea să pară la
prima vedere. Nesesizarea acestei complexități duce
uneori la deformări dăunătoare din punct de vedere
instructiv‑educativ. De fapt, cercetarea și producția
prezintă o trăsătură comună, a cărei dezvoltare ar
trebui să determine o fuzionare a lor. Și într‑un caz și
în celălalt, trebuie să reacționăm în mod adecvat la o
situație inedită, la o situație care nu se mulțumește cu
folosirea unor șabloane.
În mod corespunzător, două activități pe care
le desfășoară studenții noștri, cercetarea științifică și
practica productivă, sunt complementare, dar în același
timp ar trebui să tindă către o contopire. Din păcate,
de multe ori numai activitatea de cercetare este văzută
sub semnul unei opțiuni personale și al unei creativități
spirituale, practica productivă fiind considerată cu
precădere sub semnul constrângerii și al rutinei. La
această optică greșită contribuie și unele cadre didactice
93

Solomon Marcus

care, în repartizarea studenților la practică, nu țin seama
suficient de preferințele, de aptitudinile lor, sub motivul
că luarea în considerare a acestor factori personali nu
ar fi compatibilă cu imperativele sociale și cu disciplina
necesară vieții universitare. Am auzit chiar următoarea
apreciere: cercetarea științifică s‑a făcut până acum
după preferințe personale, putându‑se permite luxul
de a înlocui o problemă pe care nu o putem rezolva cu
o alta mai ușoară; integrarea cu producția ne cere însă
să trecem peste preferințele personale, obligându‑ne
să ținem seama de imperativul social și să rezolvăm
anumite probleme, indiferent de gardul lor de dificultate
și de simpatia noastră pentru ele.
Cu o asemenea caricatură a tabloului real al situației
riscăm să compromitem și cercetarea, și integrarea,
și – ceea ce este mai grav – viziunea tineretului nostru
asupra dialecticii obiectivului și subiectivului, despre
compatibilitatea perfectă dintre comanda socială și
dezvoltarea personalității umane în societatea noastră
socialistă. Comanda socială a momentului de față
este atât de complexă și de nuanțată încât permite –
și chiar cere – dezvoltarea unei game foarte variate
de personalități. Opțiunile tineretului nostru sunt în
deplină concordanță cu cerințele pe care i le pune în
față societatea, dar aceste opțiuni trebuie cunoscute și
examinate în fiecare caz în parte. Numai comoditatea
94

Rãni deschise

noastră, a educatorilor, ar putea fi o frână în această
privință.
Ajungem astfel la problema centrală a procesului
instructiv‑educativ: cum formăm competențele pe care
le reclamă revoluția tehnico‑științifică, dezvoltând în
același timp, în rândurile tineretului nostru, conștiința
social‑politică corespunzătoare? Cum putem obține, de
la cei pe care îi formăm, randamentul maxim de care
sunt ei capabili?
Forma supremă a activității noastre cu tinerii este
îndrumarea personală, munca diferențiată și migăloasă
cu fiecare dintre ei în parte, muncă în care avem
posibilitatea să investim întreaga noastră personalitate
culturală, politică și umană. Aceasta este, de altfel,
munca cea mai dătătoare de satisfacții; ea șterge
granițele dintre activitatea de cercetare și aceea de
învățare sau de practică productivă, dintre aspectele
profesionale și cele afectiv‑umane. Întreaga personalitate
a tânărului se modelează sub înrâurirea celui care‑l
formează cu atenție și răbdare, în lungi discuții și
confruntări. Din păcate, unele cadre didactice neglijează
tocmai această latură esențială a activității lor. Este trist
ca după 10‑15 sau 20 de ani de activitate un profesor
să nu fi îndrumat primii pași în profesie, în cercetare ai
nici unui student, să nu‑și fi format nici un discipol care
să‑i poarte amprenta și care să‑l întreacă tocmai pentru
95

Solomon Marcus

că‑l continuă. Discuțiile cu absolvenții din seriile mai
vechi confirmă, toate, că lucrul cel mai de preț cu care
ei au rămas din anii petrecuți în universitate este legat
de deprinderile căpătate în cadrul activităților care au
rezultat dintr‑o opțiune și care le‑au solicitat o anumită
inițiativă, stimulată de exemplul personal al celui care i‑a
îndrumat.
Numeroase alte probleme, în aparență diferite de
aceea discutată mai sus, se înscriu de fapt în același
context al calității muncii de îndrumare. Voi menționa
doar două dintre ele. Prima se referă la contractele de
cercetare. Ne‑am obișnuit să apreciem aceste contracte
numai după valoarea lor în bani. Dar un contract încheiat
de către o instituție de învățământ sau de cercetare cu o
unitate productivă, un contract care se vrea o expresie
a integrării învățământului cu cercetarea și producția ar
trebui să aducă, în afară de bani, un spor de cunoaștere
– care ar putea fi consemnat într‑un articol științific –,
precum și un spor instructiv‑educativ în ceea ce‑i privește
pe participanții studenți sau doctoranzi. Fără îndoială că
multe contracte îndeplinesc aceste condiții. Dar de ce oare
se poate citit atât de rar într‑o revistă științifică un articol
în care să se spună că problema tratată sau rezultatele
obținute au făcut obiectul cutărui contract?
Aceeași ordine de idei ne conduce și la cea de‑a
doua problemă pe care doream s‑o discut. De ce oare o
96

Rãni deschise

bună parte dintre comunicările prezentate la sesiunile
studențești nu ajung niciodată într‑o formă scrisă,
care să poată fi inserată măcar într‑un buletin științific
studențesc, dacă nu într‑o revistă tipărită? Ar trebui, cred,
să nu se mai accepte la sesiunile studențești decât lucrări
redactate și verificate în prealabil, în așa fel încât textul
lor să poată fi consultat de către studenți și profesori,
iar prezentarea lor orală să se poată transforma într‑o
dezbatere în deplină cunoștință de cauză. În nici un caz
nu se poate concepe – așa cum în mod regretabil s‑a
întâmplat până acum – ca unele comunicări studențești
să fie chiar premiate în absența oricărui text care să
permită un control corespunzător al lucrării.
Este lesne de înțeles că aceste neajunsuri pe care
le‑am semnalat își au în bună măsură originea tot într‑o
insuficientă îndrumare directă, continuă și profundă a
tinerilor, într‑o înlocuire a contactelor vii și autentice
cu o activitate de rutină, desfășurată aproape în virtutea
unor automatisme.
Să atenuăm șocul intrării în profesie
Încheierea școlarității (liceale sau universitare) și
repartizarea într‑un anume loc de muncă reprezintă unul
dintre momentele‑cheie din viața unui tânăr. Ne‑am
obișnuit atât de mult cu faptul că, la sfârșitul studiilor,
absolventul capătă un loc de muncă încât îl considerăm
97

Solomon Marcus

în firea lucrurilor, uitând că în multe alte țări absolvenții
nu se bucură de acest drept. În vizitele pe care le‑am
întreprins în diferite universități din Occident, întrebarea
care revenea cel mai des în discuțiile mele cu studenții
se referea tocmai la posibilitatea pe care o au studenții
români de a găsi un loc de muncă după absolvire. La
„State University College of Arts and Science” din
Plattsburgh (statul New York), curiozitatea studenților
a fost atât de mare încât o discuție întâmplătoare cu
ei a trebuit să fie urmată de o expunere amplă, în care
am explicat și exemplificat întregul mecanism al vieții
noastre universitare, iar șeful departamentului de
matematică de acolo, prof. William Hartnett, a cerut apoi
să fie trimis în România, în cadrul schimburilor culturale
româno‑americane, pentru a vedea la fața locului
organizarea învățământului nostru (vizita sa a avut loc în
1973).
Însă în ultima vreme, în legătură cu repartizarea
absolvenților, au apărut unele dificultăți, care sunt în
primul rând dificultăți inerente unui anumit proces
de dezvoltare. Revoluția tehnico‑științifică, grefată
pe specificul societății noastre socialiste, duce la o
schimbare radicală a profesiilor și a ponderii lor în viața
socială. Pentru a da doar un exemplu, voi observa că,
în timp ce într‑un trecut nu prea îndepărtat profesia de
matematician aproape se confunda cu aceea de profesor
98

Rãni deschise

de matematică, astăzi doar o parte dintre absolvenții
facultăților de matematică îmbrățișează cariera didactică,
mulți fiind repartizați în industrie, în centre de calcul,
în instituții economico‑administrative, în institute
de proiectare ș.a.m.d. Această metamorfoză rapidă a
profesiilor îngreunează evaluarea necesităților ulterioare
de absolvenți și, mai cu seamă, creează probleme
delicate în ceea ce privește adaptarea absolvenților la
specificul locului lor de muncă și capacitatea cadrelor
de conducere de la aceste locuri de muncă de a‑i utiliza
într‑un mod corespunzător.
După cum se știe, de un număr de ani s‑a adoptat
o nouă strategie în repartizarea absolvenților; aceștia
parcurg mai întâi un stagiu de trei ani în producție.
Avantajele acestei noi strategii sunt evidente, dar acum,
la mai mulți ani de la aplicarea ei, este, cred, necesară
o reevaluare care să corecteze anumite stângăcii
inerente unei experiențe de o atât de mare noutate. Este
mai întâi necesară o mai atentă stabilire a locurilor de
repartizare pentru stagiu, pornind de la o concepție
clară, consecventă și cât mai cuprinzătoare despre ceea
ce trebuie înțeles prin producție. Mi se pare limpede,
de exemplu, că locurile de stagiu în producție nu pot fi
limitate – cel puțin în cazul anumitor specialiști – la cele
care se referă nemijlocit la „procesul creării bunurilor
materiale” (pentru a reproduce definiția cuvântului
99

Solomon Marcus

producție din Micul dicționar enciclopedic); ele ar
trebui să cuprindă toate tipurile de „ucenicie” pe care
urmează să le parcurgă un absolvent pentru a deveni nu
doar un bun profesionist „în principiu” și „în sine”, ci un
specialist efectiv utilizabil în condițiile noastre concrete.
Astfel, pentru un absolvent al Facultății de matematică,
cu o lucrare de diplomă în domeniul teoriei operatorilor
(teorie cu puternice implicații în fizică), o bună ucenicie
ar constitui‑o un stagiu la un institut de fizică, după cum
pentru un absolvent de sociologie ar fi recomandabil
un stagiu ca sociolog de întreprindere sau ca membru
al unei echipe care efectuează anchete sociologice pe
teren. Evident, niciuna dintre aceste activități nu se
referă în mod nemijlocit la procesul de creare a bunurilor
materiale, dar aceasta nu alterează legătura lor profundă
cu procesul în discuție. O asemenea interpretare
largă a stagiului în producție este reclamată de o
situație obiectivă. Procesul de desfășurare a revoluției
tehnico‑științifice cuprinde componente variate, dintre
care unele, deși esențiale, sunt doar în mod indirect
legate de crearea bunurilor materiale. Se întâmplă aici
un fenomen asemănător celui care are loc în procesul
de industrializare, unde industria bunurilor de consum
este susținută de o industrie de ordinul al doilea, care
este însă tocmai industria grea. Firește, nu este vorba
aici de ordinul de importanță, ci de ordinul de legătură,
100

Rãni deschise

de caracterul mediator (dar cât de esențial!) al implicării
industriei grele în producția bunurilor de consum.
Se manifestă și alte defecțiuni, care nu pot fi trecute
cu vederea. Unele posturi vacante nu apar la repartizare,
ele fiind acoperite prin supliniri. În general, sistemul
suplinirilor este dăunător din multe puncte de vedere; el
nu ar trebui folosit decât în cazuri extreme. În schimb,
alte posturi care apar la repartizare se dovedesc a fi
fictive. Apoi, unele întreprinderi și instituții, deși știu
cu mult timp înainte că le vor fi repartizați absolvenți,
nu se pregătesc pentru primirea acestora; se creează
astfel o perioadă – uneori destul de mare – în care
proaspătul repartizat nu primește nicio orientare, este
folosit la întâmplare, de obicei în activități derizorii.
Se subapreciază imensul potențial de inițiativă, de
gândire proaspătă, creatoare, cu care vine să lucreze un
absolvent.
Aici se manifestă însă și o deficiență care provine
din modul de desfășurare a practicii productive a
studenților. Această practică ar trebui să constituie un
antrenament al studentului pentru inițiativele pe care
le va putea desfășura la viitorul său loc de muncă. S‑a
vorbit la un moment dat despre niște posibile repartizări
ale studenților care ar urma să înceapă să se acomodeze
cu viitorul loc de muncă încă de la începutul ultimului
an de facultate. Ar fi o experiență foarte interesantă;
101

Solomon Marcus

consider că e necesar să se folosească toate mijloacele
pentru a evita – sau măcar a atenua – discontinuitatea
care se produce de multe ori în momentul prezentării
absolventului acolo unde a fost repartizat.
Am în vedere aici și o problemă mai largă. Nu este
vorba doar de șocul psihologic pe care îl poate constitui
pentru absolvent debutul său în profesie, ci și despre o
utilizare corespunzătoare a absolvenților, o utilizare la
nivelul competenței lor maxime. Își poate permite oare
țara noastră, care face eforturi atât de mari pentru o
dezvoltare pe toate planurile, să utilizeze un mare umăr
de specialiști cu pregătire superioară în activități care
corespund doar unei pregătiri medii? Dacă, de exemplu,
un absolvent al unei facultăți de matematică este utilizat
ca programator, el va da un randament egal (sau poate
chiar inferior!) cu cel al absolventului unui liceu de
informatică sau al unei școli de programatori. Cum se
fructifică atunci lungul șir de ani de studii superioare,
care pe de o parte comportă din partea statului cheltuieli
importante, pe de altă parte îl obligă pe tânăr să intre
în producție cu câțiva ani mai târziu? Desigur, unele
fenomene de acest fel sunt temporar inevitabile, dar
perpetuarea lor s‑ar solda cu pagube pe care nici țara cea
mai bogată din lume nu poate să și le permită.
Am abordat în cele de mai sus doar câteva aspecte
ale unor probleme pe care le consider de o mare
102

Rãni deschise

însemnătate și complexitate, încercând să aduc mărturia
unei experiențe personale. Am convingerea că de
modul în care experiențele de acest fel vor fi însumate
și generalizate depinde în mare măsură rezolvarea cu
succes a problemelor în discuție.

103

