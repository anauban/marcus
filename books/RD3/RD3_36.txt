Istoria științei față de știința istoriei
Contemporanul, nr. 35 (1816), 28 august 1981, p. 3
Ideea istoricității faptelor de știință și‑a croit
drum cu multă dificultate. Adevărul – în general – și
adevărul științific în mod special au fost considerate
multă vreme a avea un caracter absolut, deci nesupus
influenței timpului. Însă evoluția teoriilor privind spațiul
și timpul, structura materiei și a universului, originea
vieții și ereditatea au acreditat din ce în ce mai puternic
o reprezentare dialectică a dezvoltării științei. Adevărul
științific s‑a dovedit a avea o infinitate de straturi, fiecare
descoperire generând probleme noi, mai numeroase și
mai grele decât cele anterioare. Știința nu este un stoc
fix de întrebări, pe cale de epuizare odată cu progresele
cercetării. Epuizarea întrebărilor este posibilă pentru
un anumit capitol al științei, dar nu pentru știință în
ansamblul ei. Elucidând anumite mistere, cercetarea
științifică ajunge la altele, care opun o rezistență din
ce în ce mai mare. Întocmai ca poetul, savantul „nu
strivește corola de minuni a lumii”, ci dimpotrivă „cu
lumina sa sporește a lumii taină”. Numai că accesul
la taine științifice din ce în ce mai mari devine posibil
226

Rãni deschise

numai după elucidarea unor taine de un grad mai mic de
dificultate. Această mișcare antinomică a vieții științifice
este dezvăluită și confirmată mereu atât de evoluția
științelor naturii, cât și de evoluția științelor sociale.
Dar dacă istoria științei s‑a putut ridica de la o
preocupare periferică, de înregistrare pedantă a unor
date, la una de mare anvergură teoretică și culturală,
aceasta a fost posibil datorită faptului că știința istoriei
a căpătat, la rândul ei, o perspectivă teoretizatoare și o
complexitate fără precedent. Așa cum s‑a putut vedea la
Congresul internațional de istorie, care s‑a ținut în 1980
la București, istoria este preocupată astăzi de evoluția
tuturor formelor de activitate socială, de mecanismele
profunde care o explică. Acest accent deosebit asupra
capacității explicative a teoriei istoriei (și să nu uităm
că știința românească, prin cercetări ca acelea ale lui
A.D. Xenopol, a avut în această privință un rol de
pionierat) este corelat cu o altă preocupare, care capătă
de vreo două decenii o amploare din ce în ce mai mare,
aceea privind cercetarea viitorului. Caracterul predictiv
al științei a fost de multe ori eludat în ceea ce privește
științele sociale și, în particular, în ceea ce privește
istoria. Acum, comanda socială exercită o presiune
considerabilă asupra științelor istorice, obligându‑le
să‑și întărească funcția predictivă. Necesitatea ca
istoria științei și a tehnicii să‑și amelioreze capacitatea
227

Solomon Marcus

explicativă și predictivă este impusă de actuala revoluție
științifică și tehnică, de caracterul din ce în ce mai
colectiv și mai planificat al cercetării științifice, de rolul
tot mai important pe care‑l capătă știința în întreaga
viață socială și, în mod deosebit, în rezolvarea marilor
probleme globale cu care este confruntată actualmente
omenirea. Fără îndoială că toate aceste aspecte se vor
manifesta cu pregnanță și în cadrul lucrărilor celui de‑al
XVI‑lea Congres internațional de istoria științei, care se
desfășoară la București, cu tema atât de generoasă Știință
și tehnologie, umanism și progres.
Preocupările românești de istoria științei sunt vechi
și numeroase. Cercetători dintre cei mai prestigioși și‑au
consacrat o parte din activitatea lor acestor probleme, iar
unii li s‑au dedicat în întregime. De asemenea, istoria
științei și a tehnicii românești i‑a preocupat pe unii
dintre marii noștri istorici, care i‑au dedicat investigații
îndelungate. Desigur, acestea s‑au referit la o epocă mai
îndepărtată, a cărei înțelegere nu pretindea cunoștințe
avansate de știință și tehnică.
Care este situația astăzi? Comitetul de istoria
și filozofia științei din cadrul Academiei R.S.R.
desfășoară o activitate intensă, concretizată în cercuri
de studii, sesiuni științifice, numeroase volume (printre
care o colecție de prezentare sintetică, pe ramuri, a
istoriei științei din țara noastră) și o publicație în limbi
228

Rãni deschise

internaționale, NOESIS. Un număr important de
volume vor mai apărea în viitorul apropiat. Un sprijin
fundamental a fost acordat, în această privință, de
Consiliul Național pentru Știință și Tehnologie, care a
alcătuit un plan de publicare a unor lucrări fundamentale
de istorie a științei și tehnicii românești, printre care și
unele monografii dedicate unor mari personalități, unele
dicționare și enciclopedii.
Rămân, însă, încă multe de făcut. Spațiul editorial
de care dispun cercetările de istorie a științei este
insuficient față de cerințele actuale și de viitor. Formarea
specialiștilor în istoria științei nu este urmărită suficient
de sistematic, o parte dintre cercetări rămânând încă pe
seama unor autori al căror entuziasm nu este dublat de o
competență corespunzătoare. Unii specialiști manifestă o
insuficientă înțelegere a semnificației sociale, culturale și
de specialitate a acestor cercetări, cărora le acordă un rol
periferic.
Nu putem trece cu vederea nici o anumită
rămânere în urmă a preocupărilor de istoria științei și
tehnicii față de cele privind istoria filozofiei, a artei și a
literaturii. Decalajul nu este numai de ordin cantitativ,
ci privește și gradul de aprofundare. Lucrările de
istoria științei prezintă încă, de multe ori, un caracter
predominant descriptiv, cu accent pe aspecte tehniciste,
de inventariere a unor date, cu o insuficientă perspectivă
229

Solomon Marcus

socio‑culturală și filozofico‑ideologică și cu o tendință
retorică de uniformizare hiperbolică a valorilor. Un
atare nivel a fost de mult depășit de numeroase lucrări
de istoria artei și literaturii. Spiritul critic, de ierarhizare
a valorilor, operează încă insuficient în domeniul istoriei
științei. Astfel de neajunsuri se manifestă pregnant în
unele monografii dedicate unor personalități ale științei,
monografii care păcătuiesc de multe ori printr‑un ton
constant apologetic, care ucide viziunea critică și
împiedică situarea lucidă a personalității respective în
istoria disciplinei sale.
Lasă încă de dorit și capacitatea de a corela evoluția
unei discipline cu aceea a celorlalte discipline și a
societății și culturii în cadrul cărora aceasta s‑a dezvoltat.
Avem încă o înțelegere crudă a ceea ce s‑ar putea
numi o sociologie a științei (abia preocupările recente
de scientică îi acordă o atenție mai mare) și a ceea ce
știința a reprezentat, în diferite epoci, sub aspectul ei de
componentă a culturii epocii respective. Nu dispunem
încă de o viziune clară a modului în care evoluția științei
a influențat evoluția filozofiei, artelor și literaturii și a
modului în care aceasta din urmă a influențat‑o pe cea
dintâi. Nu am cercetat încă suficient de atent efectul
primei revoluții industriale asupra evoluției științei,
tehnicii și celorlalte domenii ale culturii în secolul
trecut și în prima jumătate a secolului nostru. Este și
230

Rãni deschise

acesta unul din motivele pentru care descifrăm încă
cu oarecare dificultate acțiunea și semnificațiile celei
d‑ea doua revoluții industriale, cu toate consecințele ei
sociale și culturale. Pentru o cultură ca a noastră, care nu
beneficiază de o limbă cu largă răspândire internațională
și care a suportat vicisitudinile repetate ale unei istorii
deosebit de vitrege, importanța cercetărilor de istoria
științei este amplificată de necesitatea de a lupta pentru
apărarea priorităților ei științifice, în condițiile în care
fenomenul de frustrare s‑a manifestat în mod frecvent.
Reparațiile și corectările de rigoare nu vor putea veni
decât ca urmare a unui dialog deschis cu întreaga lume
științifică, a unei participări intense la viața științifică
internațională. Înțelegerea mecanismelor care au putut să
ducă la relații științifice internaționale care au prejudiciat
interesele anumitor culturi mai mici (ca întindere, nu
ca valoare) este importantă nu numai pentru stabilirea
adevărului istoric, ci și pentru a se evita repetarea unor
situații similare. Generațiile viitoare nu ne‑ar ierta dacă,
chiar apărând priorități ca acelea ale lui Vuia, Paulescu
sau Sudan, nu facem destul pentru ca astfel de nedreptăți
să nu se mai repete.
Congresul actual de la București este un bun prilej
de afirmare a științei noastre în lume.

231

