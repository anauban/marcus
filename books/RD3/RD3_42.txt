Interdisciplinaritatea
în învățământul superior
Amfiteatru, anul XVI, nr. 12 (192), decembrie 1981, p. 9
În perioada 24‑26 noiembrie 1981 a avut loc la
sediul Centrului european UNESCO pentru învățământul
superior (CEPES), București, un colocviu internațional
privind interdisciplinaritatea în învățământul superior.
Directorul general al UNESCO a fost reprezentat prin
domnul Audun Ofjord, directorul CEPES. Inaugurat în
prezența unor reprezentanți de seamă ai Ministerului
Afacerilor Externe (ambasador Emilian Manciur),
ai Ministerului Educației și Învățământului (director
prof. Vasile Cătuneanu), ai Comisiei naționale române
UESCO (președinte prof. George Ciucu) și a directorilor
unor organisme O.N.U cu sediul la București, colocviul
a beneficiat de participarea unor specialiști de valoare:
Chadwick Alger (U.S.A.), Thor Einar Hanisch
(Norvegia), Patrick Leary (Anglia), Eviatar Nevo
(Israel), Paolo Orefice (Italia), Janos Paloceez (Ungaria),
Lev Alexandrovich Promin (U.R.S.S.), Ljubomir
Radanovic (Iugoslavia), Wilhelm Vosskamp (R.F.G.),
iar ca observatori: I. Căplănuș, Dan Lăzărescu, Alain
258

Rãni deschise

Mouchoux (Franța), Solomon Marcus, Ioan Marinescu,
Edmond Nicolau, Tudor Nicolov (Bulgaria), Alexandru
Singer, Constantin Staiculescu și Virgil Stancovici. Au
mai fost prezente și alte persoane din partea Ministerului
Educației și Învățământului, a CEPES și a altor instituții.
Inaugurat prin cuvintele de salut rostite de domnul
Audun Ofjord (director CEPES) și de prof. Vasile
Cătuneanu (directorul Direcției pentru învățământul
superior din M.E.I.), colocviul a trecut la alegerea unui
birou, format din W. Vosskamp (președinte), L.A. Pronin
(vicepreședinte) și T.E. Hanisch (raportor). A urmat
apoi prezentarea de către A. Ofjord a documentului
de lucru. Bazat pe o informație bibliografică bogată și
pe experiența recentă a unor țări de structuri și nivele
dintre cele mai variate, acest document a urmărit mai
întâi evoluția fenomenului de interdisciplinaritate.
După un proces de continuă diferențiere a disciplinelor,
de‑a lungul a aproximativ trei sute de ani (secolele
XVII‑XIX), cunoscând apogeul în prima jumătate
a secolului nostru, s‑a cristalizat treptat fenomenul
de inadecvare a structurilor monodisciplinare
la nevoile reale ale învățământului și cercetării.
Învățământul continuă să formeze absolvenți din ce
în ce mai specializați, în timp ce societatea pretinde
tot mai pronunțat absolvenți flexibili și adaptabili
la situații neprevăzute, care presupun cunoașterea
259

Solomon Marcus

mai multor discipline. Rezultatele spectaculoase din
biologie și genetică, presiunea în sensul schimbării,
venind din inginerie (unde vechile distincții dintre
ingineria mecanică, electrică și civilă deveneau
perimate) și din științele sociale (de exemplu din
antropologie), avansul matematicii și calculatoarelor
duceau toate la descoperirea unor afinități și înrudiri
neașteptate. Treptat, s‑au cristalizat diferite forme de
colaborare între discipline, care au condus, într‑un
recent document UNESCO, la o ierarhie de patru
faze: multidisciplinaritatea (simpla alăturare a
unor discipline, fără o legătură aparentă între ele),
pluridisciplinaritatea (alăturarea unor discipline aflate
în interconexiune), interdisciplinaritatea (ca formă de
cooperare între discipline diferite, în vederea abordării
unor probleme a căror complexitate reclamă combinarea
mai multor puncte de vedere) și transdisciplinaritatea
care vizează crearea unui cadru mai larg, permițând unui
întreg grup de discipline să fie aduse împreună. Astfel,
construcția unei case are nevoie de participarea mai
multor profesii, dar mai puțin de interferența lor, în timp
ce interdisciplinaritatea pretinde un proces de integrare a
unor domenii distincte și dezvoltarea unui limbaj comun.
Interdisciplinaritatea se dezvoltă ca urmare, pe de
o parte, a unei presiuni interne dezvoltării științelor, pe
de altă parte, a unei presiuni externe, pe care societatea
260

Rãni deschise

o exercită asupra științei. Corelarea acestor două
imperative va duce la un echilibru între învățământ
și cercetare, pe de o parte, și nevoile profesionale
actuale, pe de altă parte. Presiunea internă este, în bună
măsură, o reacție la tendința de fragmentare necontenită
a domeniilor, o expresie a nevoii de a recupera prin
interconexiune și cooperare ceea ce se pierde prin
fragmentare. Sunt aduse aici în sprijin argumentele lui
Erich Jantsch (Prospects, vol. 10, 1980, nr. 3, p. 305).
Însă interdisciplinaritatea nu este numai un
răspuns la dezvoltarea internă a științei, ci și o reacție
la presiunea exercitată de evoluția recentă a societății
umane. Diversificarea profesiilor a fost multă vreme
în convergență cu pregătirea oferită de instituțiile de
învățământ. Însă, în ultimele decenii, s‑a cristalizat o
cerință nouă, aceea a formării unor profesioniști capabili
să abordeze aceeași realitate din mai multe puncte de
vedere. Unui psiholog, de exemplu, i se cere acum o
capacitate de adaptare la probleme psihologice variate,
cum ar fi cele care apar într‑o întreprindere, cele din
viața școlară sau cele din psihoterapie. Aceste exigențe
au impus o reconsiderare a întregului sistem educațional
și de învățământ, de anticipare a situațiilor noi care pot
să apară.
De o deosebită importanță este, în această privință,
comanda socială imediată. Învățământul superior trebuie
261

Solomon Marcus

să asigure, prin absolvenții săi, îndeplinirea activităților
de înaltă calificare impuse de comunitatea socială în car
acesta este integrat. Rezolvarea unor probleme specifice
ale acestei comunități nu mai poate fi asigurată de
către una sau alta dintre disciplinele tradiționale. Este
nevoie de o reorganizare a învățământului, în raport
cu problemele majore ale zilelor noastre, cum ar fi
urbanismul, ecologia sau mediul ambiant. Este nevoie
de o cooperare pe bază contractuală între instituțiile de
învățământ superior și diferite instituții și întreprinderi
productive.
Obstacolele care stau în calea interdisciplinarității
sunt localizate la nivelul structurilor, al persoanelor
(profesori, administratori și studenți), al pedagogiei și
al mijloacelor. Structurile existente, de tipul catedrelor,
departamentelor și facultăților datează din perioada
premergătoare marilor transformări pe care știința și
societatea le‑au cunoscut în ultimele decenii. Relativa
lor stabilitate vine în contradicție cu schimbările rapide
care intervin în structura profesiilor și a meseriilor.
Și mai dificilă este transformarea structurilor mentale
ale persoanelor implicate în procesul de învățământ.
Unele cadre didactice, prizoniere ale unei formații
conservatoare, privesc interdisciplinaritatea ca o
aventură diletantă. Este nevoie să se promoveze o nouă
mentalitate, de modestie, deschidere și curiozitate, de
262

Rãni deschise

dorință a dialogului și de capacitate de asimilare și
sinteză. Problema are și un aspect moral, de acceptare
a muncii alături de colegi provenind din domenii dintre
cele mai variate.
Dacă rezistența multor cadre didactice față de
cerințele interdisciplinarității poate fi explicată prin
considerente de ordin psihologic și de rutină, mai
surprinzătoare sunt neînțelegerea, lipsa de interes pe
care, într‑o serie de țări, studenții le arată față de aceste
cerințe. Unele programe interdisciplinare au eșuat
din lipsă de „clientelă”. Se pare că și aici și‑au făcut
efectul inerția structurilor, deprinderile acumulate, grija
pentru ziua de mâine, teama de ceea ce pare o aventură
în condițiile creșterii șomajului în rândurile multor
absolvenți de învățământ superior. La acestea se mai
adaugă un fapt. Mulți dintre patroni, administratori,
directori ai diferitelor organisme sociale sunt insuficient
de conștienți de schimbările rapide care se produc și de
sensul în care acestea se efectuează.
Din punct de vedere pedagogic, diferitele
alternative care au fost considerate în ultimii ani se
referă la următoarele strategii posibile: a) combinarea
unor discipline teoretice cu altele, practice; b) gruparea
separată a disciplinelor teoretice față de cele practice;
c) combinarea unor științe exacte cu unele domenii
social‑umaniste; d) subordonarea, ca factor de rigoare, a
263

Solomon Marcus

unor metode ale științelor exacte, față de unele domenii
social‑umaniste; e) gruparea unor discipline după
similaritatea obiectului lor de studiu; f) combinarea unor
domenii cât mai eterogene (matematica și muzica, teatrul
și fizica, ingineria și literatura); g) combinarea unor
metodologii, indiferent de obiectul lor. Deocamdată, nici
una dintre aceste strategii nu a obținut o victorie netă
asupra celorlalte. De multe ori, strategia este decisă de
conjunctură, de problema socială sau tehnică ce trebuie
rezolvată.
O problemă controversată este și aceea a
nivelului la care trebuie plasată cu precădere formația
interdisciplinară și a duratei acesteia. Se pare că cele
mai frecvente experiențe interdisciplinare au fost
efectuate până acum la nivel postuniversitar și au
urmărit o mișcare dinspre multidisciplinaritate spre
interdisciplinaritate.
În ceea ce privește mijloacele materiale necesare
pentru implementarea interdisciplinarității, experiența
arată că, dacă investițiile în acest sens pot să pară
nejustificate pe termen scurt, ele constituie, într‑o
perspectivă mai amplă, cea mai bună șansă de rezolvare
a unor probleme sociale importante.
Un alt raport a fost prezentat de prof. Edmond
Nicolau, care, menținându‑se în coordonatele raportului
anterior, a aprofundat unele aspecte legate cu precădere
264

Rãni deschise

de organizarea, structura și dificultățile procesului de
colaborare a disciplinelor, introducând de asemenea
diferențieri importante în raport cu tipul de economie și
de organizare socială existente în diferite țări.
Desigur, din prezentarea de mai sus ca și din
relatarea următoare a dezbaterilor și a concluziilor,
cititorul va ști să deosebească diferitele constatări și
evaluări în raport cu tipurile de societate și de organizare
pe care ele le au în vedere. Nu este greu pentru cititor
să aprecieze în ce măsură diferitele deziderate exprimate
sunt realizate în țara noastră și care este modalitatea
specifică de abordare românească a problemelor.
Discuțiile care au urmat au gravitat în jurul câtorva
chestiuni centrale: I) Prezentarea unor experiențe
interdisciplinare în învățământul universitar, care
merită să intre în atenția autorităților guvernamentale
și academice, în vederea eventualei lor generalizări; II)
Contribuția și limitele interdisciplinarității în domeniul
cercetării științifice; III) În ce măsură și pe ce căi
poate fi realizată interdisciplinaritatea în procesul de
predare, în cel de învățare, în programele universitare
și, în mod special, în comunicarea dintre domeniile
social‑umaniste, pe de o parte, și științele naturale
și tehnologice, pe de altă parte; IV) În ce măsură
poate interdisciplinaritatea să contribuie la rezolvarea
problemelor societății contemporane?
265

Solomon Marcus

O relatare a „filmului” dezbaterilor ar fi desigur
deosebit de instructivă, controversele fiind fecunde
și semnificative. Dar mai important este să subliniem
concluziile acestor dezbateri, cuprinse într‑un document
final, care va fi difuzat pe întreaga rețea internațională
UNESCO. Participanții au căzut de acord asupra faptului
că problemele globale ale lumii contemporane nu pot
fi rezolvate decât printr‑o abordare interdisciplinară,
centrată pe valorile umane, care singure pot da un
sens major educației și învățământului. Formarea
unei gândiri interdisciplinare trebuie începută cât
mai devreme, în orice caz nu mai târziu de anii de
școală. Copiii trebuie antrenați încă din primii ani
într‑o înțelegere din mai multe puncte de vedere a
lumii înconjurătoare. S‑au cristalizat trei tipuri de
interdisciplinaritate: de problemă, de metodă și de
concepte, prima fiind considerată cea mai importantă
(în raport cu probleme ca explozia demografică,
deteriorarea mediului ambiant, agresivitatea umană
etc.). Se impune o orientare mai pronunțată spre
activitatea de sinteză și aceea de rezolvare de probleme.
Deoarece structura universitară tradițională nu poate
fi transformată rapid, trebuie acordată întreaga atenție
unor experiențe interdisciplinare reușite, cum ar fi cele
efectuate la Belgrad; Bielefeld, București, Boston și
Haifa, prezentate detaliat în timpul dezbaterilor și care
266

Rãni deschise

au fost primite cu mult interes de către participanți.
Problema interdisciplinarității este în mare măsura
una de atmosferă și una de limbaj. O atitudine
deschisă și prietenoasă este o condiție a muncii în
echipă. O atare echipă își elaborează treptat un limbaj
interdisciplinar. Există o discrepanță între capacitatea
de creație a tinerilor și capacitatea lor de comunicare,
în favoarea celei dintâi. Dispunem de multe dicționare
pe specialități, dar de puține instrumente de comunicare
între specialiști diferiți (matematica și limbajele de
programare au o contribuție importantă în ameliorarea
acestei situații). În multe țări, resursele puse la dispoziția
șomerilor pentru a se putea educa în vederea încadrării
lor sociale sunt cu totul insuficiente. Abordarea holistică
este încă deficitară, fiind dominată de cea analitică, fapt
care împiedică formarea unei gândiri interdisciplinare.
Insuficienta atenție dată relevanței conceptelor și
problemelor studiate menține încă o bună parte din
actualele programe într‑o orientare mai degrabă spre
trecut decât spre viitor. Interdisciplinaritatea nu trebuie
să fie un scop în sine, ci trebuie subordonată idealurilor
de umanitate. Orice tendință elitară trebuie descurajată.
Într‑o bună măsură, societatea nu realizează
încă posibilitățile reale ale interdisciplinarității. Este
de datoria educatorilor să dezvolte cât mai multe
canale de comunicare între învățământ și societate, în
267

Solomon Marcus

vederea explicării importanței formării unei gândiri
interdisciplinare, care să reducă excesele analitice
acumulate timp de secole. Modalitatea empirică,
descriptivă trebuie să piardă teren în favoarea metodelor
predictive (în acest sens abordarea matematică fiind
esențială).
Deoarece nu există o strategie universal valabilă de
depășire a dificultăților existente, de o mare importanță
sunt exemplele concrete de proiecte interdisciplinare
efectiv realizate, prin colaborarea profesorilor cu
studenții, exemple care trebuie aduse la cunoștința unui
public cât mai larg.
Dincolo de utilitatea deosebită a schimbului de
experiență realizat cu prilejul colocviului UNESCO
relatat mai sus, trebuie totuși să observăm că, din cauza
unor deficiențe de natură organizatorică, participarea
românească la acest colocviu nu a corespuns ponderii
reale pe care învățământul nostru superior o deține în
sfera experiențelor interdisciplinare. Unele dintre cele
mai importante realizări românești în domeniu nu s‑au
reflectat în documentele și dezbaterile colocviului; de
altfel, în ciuda faptului că acest colocviu s‑a desfășurat la
București, țara noastră nu a dispus de nicio prezență cu
statut de participant, ci numai de câțiva observatori.
Se impune deci, pe viitor, o mai atentă pregătire a
unor manifestări de acest fel.
268

