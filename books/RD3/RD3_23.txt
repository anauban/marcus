Aplicații ale lingvisticii matematice
Tribuna României, anul V, nr. 83, 15 aprilie 1976, p. 6
Când o întreprindere de transporturi trebuie să‑și
planifice itinerarele ținând seama de diferitele restricții
privitoare la orarele de parcurs, apar două probleme mai
importante: 1) găsirea tuturor itinerarelor posibile în
condițiile impuse; 2) determinarea itinerarelor optime.
Dacă în ceea ce privește prima problemă instrumentul
natural de investigație îl constituie teoria grafurilor (un
graf este o mulțime de puncte dintre care unele sunt unite
printr‑o linie; în cazul nostru, punctele sunt diferitele
localități pe care le deservește întreprinderea, iar liniile
arată între ce localități se face transportul), problema a
doua se tratează prin mijloacele programării liniare.
În 1975, doi cercetători italieni, M. Mercatanti și
L. Spanedda, și – independent de ei – doi matematicieni
români, Sorin Ciobotaru și Gheorghe Păun, au observat
că problema 1) își găsește un cadru natural de rezolvare
în așa‑numita „teorie a gramaticilor generative”. Ideea
este următoarea: localitățile deservite pot fi interpretate
ca elemente ale unui alfabet, iar diferitele itinerare
posibile ale unor autobuze se prezintă ca șiruri finite
139

Solomon Marcus

de elemente ale alfabetului, deci pot fi interpretate
drept cuvinte care alcătuiesc un limbaj. Ținând
seamă de regulile de formare a cuvintelor (deci de
structura itinerarelor autobuzelor), teoria gramaticilor
generative – elaborată inițial (în urmă cu vreo douăzeci
de ani) pentru studiul limbilor naturale – permite să se
construiască o „mașină” sub forma unui număr finit
de reguli cu caracter algoritmic, cu care se obțin toate
cuvintele limbajului în discuție, și numai aceste cuvinte;
deci mașina, numită de obicei gramatică, generează
toate itinerarele posibile. Aici este important faptul că
generarea unui itinerar explică și „geneza” itinerarului.
Tocmai aici se înscrie interesul contribuțiilor semnalate:
se constată că stabilirea formei unui număr relativ
mic de itinerare permite să se deducă toate itinerarele
posibile. În acest fel, atât găsirea tuturor itinerarelor, cât
și descoperirea itinerarelor optime devin programabile la
calculatorul electronic.
Metoda poate fi aplicată și altor probleme, privind,
de exemplu, itinerarele unei biblioteci rulante, ale unei
caravane cinematografice, ale unui autocamion care
colectează maculatură sau alte deșeuri etc. De fiecare
dată este vorba de un punct mobil care trebuie să
„viziteze” anumite puncte fixe și trebuie să facă aceasta
în condiții cât mai raționale, care se stabilesc în mod
diferențiat pentru fiecare problemă. Un caz particular
140

Rãni deschise

important este acela în care fiecare punct fix trebuie
vizitat o singură dată; un exemplu în acest sens este
programul pe care și‑l face un turist pentru vizitarea
anumitor obiective turistice. Un algoritm general pentru
găsirea, în acest caz, a tuturor itinerarelor posibile a fost
dat de către Sorin Ciobotaru.
Există însă și tipuri de itinerare pentru care
problema găsirii lor algoritmice nu a fost încă rezolvată.
În această situație se află problema itinerarelor posibile
ale unui inspector care, trebuind să controleze anumite
unități, dorește, din motive psihologice, să nu repete
niciodată o anumită porțiune a itinerarului său; mai
precis, dacă prima oară a controlat, după unitatea A,
unitatea B, a doua oară va controla, după unitatea A, o
unitatea C, alta decât B.
O altă aplicație (efectuată de Gheorghe Păun) se
referă la planificarea procesului de producție industrială.
Atunci când este vorba de a se produce bunurile
B1, B2…Bn pe baza resurselor R1, R2…Rn, pentru fiecare
produs finit Bi putem construi un graf ale cărui puncte
indică etapele posibile în producerea lui Bi (o astfel de
etapă este folosirea, într‑o anumită unitate de timp – de
exemplu, într‑o zi – a uneia dintre resursele existente
de materii prime în vederea producerii lui Bi). În acest
fel, pe alfabetul format de punctele grafului considerat,
anumite „cuvinte” convenabil alese vor reprezenta
141

Solomon Marcus

diferitele moduri de obținere a produsului Bi. Două
puncte din graf sunt unite printr‑o linie dacă activitățile
pe care ele le reprezintă pot fi consecutive. Gh. Păun
reușește să demonstreze că, dacă stocul de produse
semifabricate este suficient de redus, atunci procesul
de producție industrială poate fi stimulat cu un automat
finit. Alte aplicații dezvoltate de același autor se referă la
probleme de ordonanțare privind organizarea unui atelier
sau a unei secții de producție.
Unele dintre rezultatele de mai sus au fost primite
de specialiști și vor putea fi valorificate fără o prea mare
întârziere.
Alte aplicații practice ale teoriei limbajelor se
referă la sistematizarea automată a nomenclatorului
de meserii, specialități și funcții din România (S.
Ciobotaru), ecologie (Mihai Dinu), praxiologie (Monica
Scorțaru). Toate cercetările semnalate se află în curs de
publicare în Studii și cercetări matematice, în Semiotica
și în alte periodice. Ele fac parte dintr‑un program mai
amplu, dezvoltat în cadrul școlii românești de lingvistică
matematică, privind utilizarea limbajelor formale în
cele mai variate domenii ale științei, tehnicii și artei.
Limbajele pe care omul le construiește pe cale artificială,
deliberată, repetă în esență arhitectura limbajului
articulat, care‑l distinge pe om de toate celelalte
viețuitoare.
142

