Un model cultural exemplar
Tribuna, nr. 15 (1686), anul XXXIII,
15 aprilie 1989, p. 1
Am început a‑l căuta pe Eminescu mult după ce
l‑am descoperit, după ce l‑am simțit organic în eul meu
liric. Poezia lui mi‑a apărut ca un element constitutiv
al universului. Nu‑mi pot reprezenta limba română,
care ne‑a modelat mirările, îndoielile, întrebările și
emoțiile, fără componenta eminesciană. Încorporată
naturii, limbii și istoriei românești, acțiunea pe care
o exercită spiritul eminescian asupra noastră este, ca
durată culturală, psihologică și istorică, mult mai întinsă
decât cei o sută de ani măsurați sub aspect cronologic
de la moartea poetului. Din punctul de vedere al
evenimentelor pe care le‑a generat, această acțiune ne‑a
îmbogățit într‑o asemenea măsură, a mărit atât de mult
densitatea evoluției noastre culturale, a îmbogățit atât
de îndelungata istorie și a deschis o atât de largă privire
înainte, încât perspectiva românească a căpătat aceeași
amploare ca și aceea pe care cultura italiană a căpătat‑o
prin Dante, cea engleză prin Shakespeare, cea germană
prin Goethe.
542

Rãni deschise

Abia după ce această realitate mi s‑a impus cu
forța dezlănțuirilor naturii, am început a‑l căuta și
descoperi pe Eminescu, a‑l înțelege la o mereu nouă
lectură, la o mereu altă raportate la contextul istoric și
cultural. Acum, după ce la creația sa antumă s‑a adăugat
publicarea postumelor sale, în timp ce ediția inițiată
de Perpessicius se apropie de sfârșit, și după ce ne‑a
fost adus la cunoștință, fie și fragmentar, conținutul
caietelor manuscrise ale poetului, ne întrebăm din nou ce
reprezintă Eminescu nu numai pentru literatura română,
ci și pentru ansamblul culturii românești.
Pentru un răspuns mai cuprinzător la această
întrebare mai este nevoie de un răgaz, dar mi se pare de
pe acum neîndoielnic faptul că Eminescu se constituie
într‑un model cultural exemplar, de care, în secolul
nostru, numai câțiva scriitori, printre care Blaga și
Camil Petrescu, s‑au apropiat într‑o oarecare măsură.
Am în vedere: interesul manifestat de Eminescu pentru
toate aspectele culturii, viziunea sa culturală totală;
acuitatea observației sale în domenii dintre cele mai
variate; inteligența cu care a urmărit (și s‑a impregnat
de) evoluția științei și filozofiei și cele mai recente
achiziții ale acestora; capacitatea de a intui cu exactitate
și profunzime concepte, idei și teorii dintre cele mai
delicate; convergența acestor observații și intuiții cu
unele evoluții pe care le‑au cunoscut știința, filosofia
543

Solomon Marcus

și artele secolului nostru. Dar mai mult decât toate
acestea am în vedere faptul că acest atât de bogat orizont
cultural nu a rămas ca o simplă anexă, ci a marcat
esențial întreaga creație poetică eminesciană și nu pateu
fi despărțit de valoarea acestei creații. De aceea, opera
lui Eminescu este foarte exigentă față de cititori, aceștia
trebuind să râvnească a o înțelege într‑o perspectivă
culturală cât mai complexă, pe care chiar poetul a
preconizat‑o în însemnări sau a sugerat‑o în poezii.
Dintre multele datorii pe care le avem față de cel
care atât de mult ne‑a îmbogățit, aș menționa aici numai
câteva. Dicționarul limbii poetice a lui Eminescu, Editura
Academiei, 1978 (sub redacția lui Tudor Vianu) ar trebui
reluat, dezvoltat și completat, luându‑se în considerare nu
numai opera antumă, ci și cea postumă. S‑ar putea avea
în vedere cu această ocazie și un dicționar al simbolurilor
eminesciene. Reproducerea într‑un număr rezonabil de
exemplare a caietelor manuscrise (prin facsimilarea lor,
așa cum s‑a procedat cu caietele lui Paul Valéry) nu mai
poate întârzia, dar ar trebui întocmit și un îndreptar care
să ne orienteze în labirintul miilor de pagini din aceste
caiete. De asemenea, o evidență cât mai sistematică și mai
completă a tot ce s‑a scris despre Eminescu este o condiție
importantă a calității exegezei viitoare.

544

