Semiotica vizuală:
reviste, cărți, întâlniri, articole
Arta, anul XXXVI, nr. 10, 1989, pp. 37‑38
Preocupările de semiotică vizuală se află sub
supravegherea unor structuri organizatorice dintre
cele mai variate. De curând a luat ființă Asociația
Internațională pentru Semiotica Spațiului (AISSP), care
a anunțat, pentru perioada 26 – 28 iulie 1990, o întâlnire
pe tema culturii arhitecturale și a culturii urbane, la
Universitatea din Geneva. Președinte al AISSP este
Martin Krampen (Ulm. R.F.G.), vicepreședinți sunt
Geoffroy Broadbent (Plymouth, Anglia) și William
Widdowson (Cincinnati, S.U.A.), iar secretar general
este Manar Hammad (Paris). Scopul AISSP este de a
stimula cercetările care au în vedere producția sensului
în spațiu. În 1987, s‑a organizat la Urbino (Italia) un
dialog al semioticii spațiale cu estetica iar o altă întâlnire
în 1988 a avut ca obiect relația dintre formă și substanță
în spațiu.
Organizatorul întâlnirii de la Geneva, profesorul
Pierre Pellegrino, a lansat o circulară în care explică
obiectul acestei întâlniri. El propune să se examineze
585

Solomon Marcus

atitudinile semnificative pe care o societate le dezvoltă în
modul de segmentare spațială.
Între 23 şi 24 septembrie 1989 a avut loc la Ulm
(R.F.G.) o întâlnire pe tema Identitate culturală şi
design. Lucrările au fost organizate în patru secțiuni:
mediu ambiant, producția de design, comunicare vizuală
și teorie semiotică. Dintre chestiunile abordate, relevăm
cele privind identitatea culturală, jucăriile în culturile
arabe, un sistem vizual de educație sanitară în țările în
curs de dezvoltare.
Al doilea congres internațional privind legătura
dintre text şi imagine va avea loc la Universitatea din
Zürich, între 27 şi 31 august 1990. Au fost anunțate
următoarele teme: fotografie, teatru, metodologie,
epigrafie monumentală, arte vizuale și limbaj, strategii
de interpretare, similarități formale, colaj, imagini
ale scriiturii, retorică, modernismul în artă, arta
neeuropeană, estetică şi poetică vizuală, ilustrația de
cărți, narativitatea în film, percepție vizuală şi psihologie
cognitivă. O discuție specială va fi dedicată lui James
Joyce.
La cel de al șaselea congres al Asociației
Germane de Semiotică (Universitatea din Passau, 8-11
octombrie, 1990) vor fi abordate, între altele: semiotica
şi psihologia semnelor de circulație, perspective ale
semioticii poststructuraliste, configurație şi construcție
586

Rãni deschise

la semnele muzicale, teoria şi practica designului,
poststructuralism‑deconstrucție‑postmodernism, funcția şi
interpretarea semnelor verbale şi neverbale în psihiatrie
și psihoterapie, fotografie şi percepție‑coduri vizuale.
Un simpozion internațional asupra designului a avut loc
la Helsinki (17 – 18 mai 1989). Semiotica marketingului
a făcut obiectul unei întâlniri la Copenhaga (18 – 19
mai 1989). La Universitatea din Rochester (S.U.A.),
a funcționat, între 9 iulie şi 18 august 1989, un institut
de teorie şi interpretare în artele vizuale. Un colocviu
asupra ficțiunii s‑a desfășurat la Cerisy‑la‑ Salle (Franța)
între 15 şi 22 iulie 1989. Comunicarea prin corp şi
prin îmbrăcăminte a făcut obiectul unei întâlniri la
Indianapolis (S.U.A.), între 16 şi 13 iulie 1989. Tot la
Indianapolis a funcționat, între 16 şi 22 iulie 1989, un
institut internațional privind semnificația în marketing. O
conferință internațională asupra arhitecturii, urbanisticii
şi designului s‑a desfășurat la Espoo (Finlanda) între 4 şi
6 septembrie 1989.
În cadrul Festivalului Internațional de Dans de la
Arles, Franța (20 – 23 iulie 1989) a fost organizat un
colocviu pe tema Memorie şi Uitare. Dintre chestiunile
abordate, menționăm: memoriile corpului, memoria
colectivă, memoria publicului, imagini ale dansului,
semne şi imagini ale dansului, proiecție şi discurs,
cinetica memoriei, viteza uitării. De o atenție deosebită
587

Solomon Marcus

s‑a bucurat relația dintre dans şi imagine, în jurul unor
chestiuni precise ca: mișcarea ființelor vii, esențială
atât în cinema cât şi în dans, poate face obiectul unei
definiții cuantificate? Depinde lectura acestei mișcări
de o trecere de la discret la continuu? Putem înțelege şi
descrie mișcarea şi traseele ei plecând de la o mulțime
de imagini particulare, putem modela raporturile
dans‑imagine luând în considerare mulțimi infinite de
traiectorii?
O activitate deosebită în domeniul vizual se desfășoară la Universitatea François Rabelais din Tours
(Franţa), unde între 29 şi 30 septembrie 1989 s‑a desfășurat
o discuție pe tema transformare‑metamorfoză‑anamorfoză
în cadrul preocupării mai ample de cercetare a imaginii
într‑o perspectivă semiotică.
Aceeași universitate a lansat Bulletin International
de Sémiotique de l’Image, al cărui prim număr a fost
publicat în mai 1989. Remarcăm aici un remarcabil
studiu datorat redactorului șef al publicației, Michel
Costantini, şi având ca temă anumite ipoteze de lectură
a frescei XII din ciclul consacrat de Giotto vieții lui
Francisc, în bazilica de la Assisi, în cursul ultimului
deceniu al secolului XIII. Publicația mai cuprinde o
analiză pertinentă a cărții L’origine de la perspective
publicată de Hubert Damisch, în colecția Idées et
Recherches, la editura Flammarion, Paris. Este de
588

Rãni deschise

asemenea analizată lucrarea lui Jean‑Marie Floch
Les formes de l’empreinte: Brandt, Cartier‑Bresson,
Doisneau, Stieglitz, Strand (Pierre Faniac éditeur, 1986,
Paris). Sunt apoi prezentate diferite numere tematice în
domeniul vizual, ale unor publicații recente: Imagini şi
societate în Grecia antică. Iconografia ca metodă de
analiză (Cahiers d’Archéologie Romande, 36, 1987);
Subiectivitatea în cinema (direction lacques Fontanille),
Actes Sémiotiques X, 41, martie 1987; Arta abstractă
(direction J. M. Floch & L. Régis), Actes Sémiotiques
X, 44, decembrie 1987; Comunicarea prin imagini
(Bulletin de Psychologie, vol. 41, no 388, iunie‑august
1988); Arte plastice; probleme de limbaj (La Part de
l’Oeil, 5, 1989). Apoi sunt analizate unele articole
recente dedicate vizualului. Frédéric Jameson, Lectura
fără interpretare (postmodernismul şi textul video),
în Communications 48, 1988, 105 – 120; Desiderio
Blanco, Figuri discursive ale enunțului cinematografic
(Actes Sémiotiques, Documents, IX, 90, 1987); Georges
Sauvet, Comunicarea grafică paleolitică (de la analiza
cantitativă a unui corpus de date la o interpretare
semiologică), în L’anthropologie, 1988, 92, no. 1, p.
3 – 16; Suzanne Preston Blier, Cuvinte asupra unor
cuvinte despre semne iconice: iconologia şi studiul artei
africane (Art Journal, 47, 2, 1989, p. 75 – 87); François
Bastide, Tratamentul materiei. Operații elementare,
589

Solomon Marcus

Actes Sémiotiques‑Documents IX, 89, 1987; Marie
Carani, Semiotica perspectivei picturale (Protée, 1988,
Univ. Laval, Quebec); Madeleine Arnold, Imaginea
de sinteză animată (Communication et Langages, no
77,1988).
Grupul de cercetători în domeniul vizualului,
de la Universitatea din Tours, s‑a remarcat în ultimii
ani printr‑o intensă activitate publicistică. Maurice
Brock a publicat în 1986 Istoria artei moderne, Stadiul
științelor sociale în Franța (La Découverte, Paris,
p. 414 – 417); Secretul lui „disegno” în pictura şi
în arta fortificațiilor Veneției din secolul al XVI‑lea
(în Ph. Dujardin, editor), Le secret, Presses Univ. de
Lyon, 1987, p. 17 – 30. Michel Costantini, principalul
animator al grupului, a publicat Un moment în istoria
nimbului (L’écrit‑voir, revistă de istoria artelor, no 7,
1986, p. 30 – 38); Portretul omului mort (ibidem, no
8, 1986, p. 62 – 76); Semiotica imaginii şi istoria artei
(D’art, Barcelona, 1987, p. 73 – 88); Trei abordări
ale istoriei semantice a artei grecești (conferințe,
iunie 1987, Univ. din Tours); Giotto în alb şi negru,
conferința la Ecole des Beaux‑Arts, Poitiers, 23 martie
1988. Pierre Fresnault‑Deruelle: Les images détournées
(Communications et Langage 75, Paris, 1988); Le visage
et le paysage (Humanisme et entreprise 56, 1988, Paris,
IV, Celsa); Simulacrul vorbirii în benzile desenate
590

Rãni deschise

(Europe, aprilie 1989, Paris); Jean‑Didier Urbain a
publicat în 1989, la editura Pion, Paris, L’Archipel des
morts, o carte despre sentimentul morții şi derivatele
memoriei în cimitirele Occidentului.
În Suedia, la Lund University Press, Malmö, 1988,
Göran Sonesson a publicat cartea Concepte pictoriale.
Moștenirea semiotică şi relevanța ei pentru analiza lumii
vizuale. Autorul caută un cadru corespunzător pentru
prezentarea diferitelor teorii şi tendințe divergente în
domeniul vizualului, al imaginii plastice cu deosebire,
fiind preocupat de legătura cu lumea experienței
(iconicitatea), de modul în care fragmentul vizual dă
seama despre lumea vizuală (indicialitate) şi de modul
în care imaginea produce un surplus de semnificație
(limbajul plastic şi conotația). Aceste chestiuni sunt
discutate în contextul semioticii europene şi americane
şi al ideilor care vin din psihologia cognitivă, din
lingvistică şi din filosofie. În Italia, la Giancarlo Politi
Editore, Milano, a apărut în 1986 cartea Rhetoric of
Art a lui Jorge Glusberg, în care principalele capitole
au următoarele titluri: Semnul estetic; Sociologia artei;
Teoria instituțiilor; Ruptura culturală; Retorica artei;
Semne în ecosistemele artificiale; Benzile comice şi
arta; Retorica artei video. În Revista de Estetică din
Buenos Aires se publică, în nr. 7 din 1988, articolele
lui Ch. Metz, Fotografie şi fetiș, A. Silva, Focalizarea
591

Solomon Marcus

vizuală, Rosa Maria Ravera, Arta între comunicabil şi
incomensurabil, Revista de Semiotica e Comunicacão,
care apare la Sao Paulo, fiind editată de Maria Lucia
Santaella Braga, publică în numerele pe 1988 unele
articole dedicate vizualului, ca: A. Machado, Imaginea
digitală; Ph. Menezes, Imagine‑cinema‑poezie; E.
Colin Zayas, Imaginea publicitară; Th. A. Sebeok,
Fetișul. În Les Cahiers du Département des Langues
et des Sciences du Langage, publicate de Universitatea
din Lausanne, s‑a dedicat un întreg număr (nr. 3, 1986)
reprezentării spațiului. La Universitatea din Urbino,
un document recent (sept. oct. 1988) este: Claude
Zilberberg, Arhitectură‑ muzică‑limbaj în «Eupalinos»
de Paul Valéry. În Degrés (Belgia) no 57, 1989 se
publică: Gianfranco Bettetini, Televiziune şi semiotică;
Roger Bautier, De la televiziune la telespectator; Paul
Bouissac Spațiu performativ şi spațiu mediatizat:
deconstrucția spectacolului de circ în reprezentările
televizate; Patrick Imbert, Acesta este corpul meu,
aceasta nu este o pipă. În seria de monografii a
Cercului Semiotic din Toronto (Canada) s‑a publicat
în 1989 lucrarea editată de James Braun şi Anthony
Mollica: Eseuri în semiotica vizuală aplicată. Anterior,
în aceeași serie, s‑au publicat: Fernande St. Martin,
Prolegomene la o gramatică a limbajului vizual (1985)
şi, editată de Clive Thomson, Filogenia şi ontogenia
592

Rãni deschise

sistemelor de comunicare (1985). La Universitatea
de Arte Aplicate din Viena s‑a susținut în 1989 teza de
doctorat a lui Johannes Domsich Vizualizarea ca deficit
cultural? Geneviève Cornu, de la Institutul Universitar
de Tehnologie din Lyon (Franța), a publicat Pictură şi
poezie: limbaj, producție, creație, mesaj, Bibliothèque,
Univ. de Lille, 2 volume, 1985; Scriere, pictură: de la
caligrame la pictograme (Semiotica, vol. 44, 1983, 123
– 135); Lectura imaginii publicitare (Semiotica 1985);
Evoluția polilor comunicării şi noțiunea de publicitate
de‑a lungul istoriei afișului (Semiotica, vol. 63, 1987,
269 – 287); Pentru o semiologie a operei artistice
(Semiotica, vol. 75, 1989).

593

