Rãni deschise

Lecţia de deschidere
România literară, nr. 41,
16-22 octombrie 2002

O frumoasă tradiţie, căreia i s-a pus capăt sub
comunism şi care nu a fost reluată după 1989, este aceea a
lecţiilor de deschidere în învăţământul universitar. La
8 februarie 1942, deci în plină stare de război şi parcă în
pofida acesteia, profesorul Dan Barbilian îşi ţinea, la
Facultatea de Ştiinţe a Universităţii din Bucureşti, lecţia de
deschidere consacrată problemelor generale ale axiomaticii.
A fost o adevărată piatră de hotar, marcând trecerea de la
ştiinţa clasică la cea modernă. Una dintre consecinţele acestei
lecţii a fost o corespondenţă, pe tot parcursul anului 1942,
între Dan Barbilian şi Simion Stoilow, un memorabil schimb
de idei, care arată cu câtă incandescenţă îşi pot trăi căutările
oamenii de ştiinţă autentici. (Am reprodus o parte a scrisorilor
lui Barbilian către Stoilow în cartea Simion Stoilow, publicată
în colaborare cu Cabiria Andreian Cazacu, la Editura
Ştiinţifică şi Enciclopedică, 1983). De numele lui Stoilow se
leagă o altă lecţie de deschidere care a rămas în istoria ştiinţei
33

Solomon Marcus

şi învăţământului nostru universitar, aceea ţinută la 26 aprilie
1923 la Universitatea din Cernăuţi: „Privire generală asupra
dezvoltării şi metodelor marilor teorii ale analizei
matematice” (reprodusă şi ea în volumul menţionat). Stoilow
avertiza asupra faptului că universitatea nu se poate limita la
pregătirea unor practicieni de diferite specialităţi, perspectiva
ei trebuie să fie mai amplă, să includă formarea unor oameni
de cultură care se dedică cercetării şi creaţiei intelectuale,
într-un orizont fără margini. Multe dintre minţile cele mai
înzestrate ale intelectualităţii româneşti se orientau atunci spre
Cernăuţi, a cărui Universitate atinsese, sub regimul imperial,
un nivel foarte ridicat şi acest nivel trebuia menţinut.
În numărul 6, anul IX, iunie 1942, Revista Fundaţiilor
Regale publica lecţia de deschidere la cursul de analiză
superioară şi logică matematică ţinută de profesorul Grigore
C. Moisil la Facultatea de Ştiinţe a Universităţii din
Bucureşti, cu titlul: „Tendinţa nouă în ştiinţele matematice”.
Trecerea de la cantitate la structură în evoluţia matematicii
este urmărită în toate consecinţele ei filosofice şi culturale.
Am mai prins câte ceva din atmosfera acestor lecţii de
deschidere în anii care au urmat imediat celui de-al Doilea
Război Mondial. Cum aş putea uita lecţia de deschidere a
lui Barbilian „Evariste Galois şi ideea de grup”, din 1945,
cu paralela dintre Galois şi Rimbaud? Dar pe aceea a
profesorului Miron Nicolescu, din 1945, cu recomandarea
imperativă: „Faceţi tabula rasa din tot ceea ce aţi învăţat
34

Rãni deschise

în şcoală!”? Exemplele ar putea continua, iar colegii mei
de generaţie de la drept, litere, filosofie, istorie, fizică,
chimie, biologie etc. au, desigur, amintirile lor de acest fel.
Cu schimbările intervenite începând cu anul 1948,
lecţiile de deschidere deveneau tot mai inoportune. Mai
întâi, nu mai exista libertatea de gândire şi exprimare
necesară lor, apoi nici „bugetul de timp” nu prea mai
permitea „irosirea” unei ore, totul fiind calculat în raport cu
o programă bătută-n cuie. Am rămas cu „festivitatea de
deschidere a anului universitar”, manifestare adesea puternic
politizată şi nu lipsită uneori de accente demagogice, obicei
care nu a dispărut complet nici după 1989.
Ce este o lecţie de deschidere? Are, desigur, toate
trăsăturile unei conferinţe, dar nu orice conferinţă este o lecţie
de deschidere. Cunoaştem acel tip de profesor care transformă
orice lecţie într-o conferinţă, după cum suntem familiarizaţi
şi cu genul de profesor care nu-şi scoate ochii din hârtii sau
care, chiar vorbind liber, reproduce fără schimbări
semnificative cursul scris şi multiplicat. Studenţii din anul
întâi apreciază cursul la care se pot lua notiţe bune, dar puţini
sunt cei care ştiu să noteze idei, nu cuvinte. Unii profesori
monologhează, alţii intră în dialog cu sala; unii fac şi glume,
alţii preferă o stare de permanentă gravitate. Dincolo de
această bogată tipologie a profesorilor, lecţia de deschidere
este necesară. O bună lecţie de deschidere pretinde, desigur,
o cultură mult mai vastă decât materia propriu-zisă care va fi
35

Solomon Marcus

predată. Ea creează cadrul general al cursului care demarează,
contextul său cultural şi istoric, discută semnificaţia generală
a obiectului său, presupoziţiile pe care le adoptă, legătura cu
alte domenii ale culturii, cu alte cursuri, anterioare,
concomitente sau ulterioare, dând în acelaşi timp o indicaţie
asupra stării actuale a domeniului; anticipează tipul de
dificultăţi pe care le comportă, natura şi direcţia eforturilor pe
care le reclamă. O condiţie esenţială a unei lecţii de deschidere
este capacitatea profesorului de a se situa nu în poziţia de
observator extern al domeniului, ci în aceea de cercetător
pasionat, care trăieşte intens problematica sa, viaţa internă a
disciplinei sale, dar şi metabolismul ei cu lumea.
Lecţia de deschidere este ca o carte de vizită a celui
care o ţine, ea trebuie să trezească în rândul auditoriului
curiozitatea de a merge la bibliotecă, pe urmele lucrărilor
sale, iar acum luarea de cunoştinţă s-ar putea produce mult
mai rapid şi mai eficace dacă profesorul ar anunţa de la
început o adresă electronică şi o alta pe internet. În această
ordine de idei, păstrarea echilibrului dintre informaţia de
tip clasic, cu trimitere la bibliotecă, şi aceea de tip
electronic este importantă. Dispreţul unor universitari faţă
de noile modalităţi electronice, dispreţ afişat cu superioară
ironie, chiar la unele emisiuni de radio şi televiziune, nu
este perceput favorabil de noile generaţii; dar nici
comoditatea unor pasionaţi ai electronicii care refuză să
mai ia în consideraţie sursele bibliografice care nu au statut
36

Rãni deschise

online nu este o manifestare de intelectual adevărat, de
cercetător autentic, setos de adevăr. Internetul este de dată
recentă şi nu-i putem pretinde să cuprindă întreaga zestre
culturală a omenirii. Un om de cultură al primului deceniu
al mileniului al treilea trebuie să fie cu un ochi la biblioteca
tradiţională şi cu celălalt la calculator.
Lecţia de deschidere a începutului de mileniu trei
poate, desigur, beneficia de articularea mijloacelor verbale
şi a celor vizuale, dar nu pentru a intimida auditoriul cu
planşe supraaglomerate, păstrate în atenţia privitorilor nu
mai mult de trei sau patru secunde, ci pentru o reală
împletire a cuvântului cu imaginea. Dialogul socratic poate
şi el să fie folosit, dar cere un talent şi o pregătire pe care
puţini le au şi pretinde o colaborare a auditoriului pe care
nu se poate întotdeauna conta.
La lecţiile de deschidere de la matematică veneau şi
persoane de la alte discipline, studenţi şi profesori, iar noi, la
rândul nostru, ne duceam la unele lecţii de deschidere de la
alte facultăţi. Aceste încrucişări sunt cu atât mai mult indicate
astăzi, datorită interacţiunii crescânde a disciplinelor.
Sunt convins că practica lecţiilor de deschidere nu a
dispărut total niciun moment, de-a lungul anilor dificili pe
care i-am parcurs, dar este nevoie acum de o iniţiativă prin
care ele să-şi recapete vigoarea de altă dată. Şi să nu uităm
că nici lecţiile de închidere nu ar trebui date uitării.

37

