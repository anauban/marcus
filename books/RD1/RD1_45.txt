Solomon Marcus

Semnale de alarmă
Tribuna învăţământului,
nr. 1040, 8-14 martie 2010

Cele mai multe semnale de alarmă privind starea gravă
în care se află sistemul educaţional românesc vin din afara
şcolii. Probabil că de la distanţă (fie ea spaţială sau
temporală) şi de către alţii lucrurile se văd mai bine decât
din interiorul sistemului şi de către cei direct implicaţi în el.
Chiar în aceste zile, Guvernatorul Băncii Naţionale a
României observă că nu se pot face autostrăzi atâta vreme
cât avem probleme cu plata pensiilor; iar problema
pensiilor nu va putea fi rezolvată satisfăcător, atâta vreme
cât productivitatea muncii rămâne scăzută. Parcă anticipând
această declaraţie şi pentru a o completa, Valentin Lazea,
economistul-şef al Băncii Naţionale a României, observa,
cu câteva zile înainte de declaraţia Guvernatorului, că
„avem o productivitate scăzută, cauzată de deficienţele
sistemului de educaţie”. Tot Valentin Lazea declara atunci
că „este un mit că avem o forţă de muncă educată, bine
calificată; sărăcia nu poate fi eradicată fără îmbunătăţirea
308

Rãni deschise

sistemului educaţional”. Între cele două declaraţii venite de
la Banca Naţională, a apărut o declaraţie a Ministrului
Muncii, Mihai Şeitan: „Şcoala românească nu-i pregăteşte
pe elevi pentru piaţa muncii”. Concomitent, criticul de
teatru Marina Constantinescu semnala grave carenţe ale
unor manuale şcolare, iar parlamentarul Cristian Ţopescu
ne informa că în multe dintre filmele de desene animate
vizibile la orele de maximă audienţă predomină violenţa.
De la aceste semnale primite în ultimele zile, să
trecem la semnale mai vechi. Nu de mult, aflam că 2 din
10 copii părăsesc în fiecare an şcoala (lucrul acesta se
întâmplă cu precădere la vârsta gimnaziului). În urmă cu
vreo doi ani, întrebată, în cadrul unui dialog pe Canalul
Cultural, de ce a înfiinţat un nou teatru („Teatrul fără
frontiere”), Mihaela Sârbu a răspuns: „Copiii îşi pierd
creativitata odată cu intrarea la şcoală şi noi încercăm s-o
salvăm”. Regretatul profesor Ion Mihăilescu, rector al
Universităţii din Bucureşti şi preşedinte al Asociaţiei
Rectorilor, repeta mereu că 80% dintre diplomele
universitare româneşti sunt fără acoperire; cu alte cuvinte,
posesorii lor nu au realmente competenţa conferită de
diplomă. Tot în urmă cu câţiva ani, Jonathan Scheele,
fostul reprezentant al Comisiei Europene la Bucureşti,
afirma că integrarea europeană a României urmează să se
efectueze în primul rând pe terenul educaţiei, dar sistemul
românesc de educaţie este unul învechit.
309

Solomon Marcus

Putem merge la semnale şi mai vechi, care nu şi-au
pierdut actualitatea. În Amintiri din copilărie, Ion Creangă
se referea la şcoală ca la un „cumplit meşteşug de tâmpenie”.
În Un pedagog de şcoală nouă, Lanţul slăbiciunilor şi în alte
opere ale sale, I. L. Caragiale a identificat carenţe grave ale
şcolii, care ne explică, în parte, ce sistem de educaţie i-a
produs pe eroii pieselor sale. Despre Eminescu aflăm, din
G. Călinescu, Viaţa lui Eminescu, 1964, p. 10 : „Nu-i plăcea
să-şi înveţe lecţiile şi de aceea lua note rele […]. În primul
rând, nu se împăca cu matematicile şi de aceea se învoise cu
Const. Ştefanovici (care a şi devenit profesor de matematică),
urmând ca acesta să-i facă temele, iar el să-i spună poveşti”.
Dar ulterior, din însemnările sale, să aflăm: „Eu ştiu
chinul ce l-am avut însumi cu matematica în copilărie, din
cauza modului rău în care mi se propunea […] mi se pusese
în joc nu judecata, ci memoria […]. În urmă am văzut că-s
cele mai uşoare […]. Matematicile sunt un joc cu cele mai
simple legi ale judecăţii omeneşti, numai acele legi nu trebuie
puse în joc goale şi fără un cuprins”. Ultima frază poate fi
adoptată ca slogan pentru Anul Educaţiei Matematice, cum
a fost decretat 2010, când se împlinesc 100 de ani de la
înfiinţarea Societăţii de Ştiinţe Matematice din România. Dar
nu numai matematica este vizată aici. Ansamblul
disciplinelor suferă în şcoală de păcatul de a fi puse în joc
„goale şi fără un cuprins”. Avem în vedere accentul pus pe
reguli de operare, pe procedee, în dauna semnificaţiilor şi
310

Rãni deschise

ideilor. Accentul pus prea mult pe sintaxă, în dauna
semanticii. Acest fapt duce în multe manuale şcolare la o
adevărată schizofrenie, semnalată încă în urmă cu decenii.
Iată numai o parte din avertismentele care ne-au
determinat să ne angajăm în necesara acţiune de schimbare.
Şcoala românească nu este încă pregătită să propună noi
programe şi manuale, pentru că este nevoie, în acest scop, de
un antrenament pe care nici măcar nu l-a început. Nu putem
schimba brusc structura bacalaureatului dacă nu schimbăm
în prealabil scenariul orelor de clasă, comportamentul elevului
şi profesorului aflaţi faţă în faţă. Iniţiativele trebuie să vină nu
de la Minister, ci de la oamenii şcolii. Salut crearea Alianţei
Colegiilor Centenare şi îi mulţumesc pentru acordarea
premiului Lykeion pentru anul 2009. Merg mereu în şcoli, iar
un rezultat parţial al vizitelor mele îl puteţi vedea în cartea
Educaţia în spectacol, în care îndeplinesc rolul de protagonist.
Cartea este editată de Lavinia Spandonide şi este publicată la
Editura Spandugino.
O condiţie fundamentală: şcoala nu poate fi reformată
de oameni blazaţi, plictisiţi, e nevoie de minţi capabile de
reîmprospătare permanentă, de oameni de cultură în stare să
vadă nu numai reguli şi algoritmi, ci si idei, istorie, conexiuni
între discipline; să vadă, dincolo de fapte particulare, moduri
de gândire şi de comportament. Să trăiască bucuria învăţării,
care nu se încheie niciodată. Să fie dornici de a-i privi în ochi
pe elevi şi de a-nţelege nevoile lor.
311

