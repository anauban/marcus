Rãni deschise

Viaţa este un spectacol
pe care nu mă satur să-l contemplu
realizat de Andreea Tudorică
pentru Jurnalul Naţional, 2 mai 2011

Andreea Tudorică: Când şi cum aţi descoperit matematica? Când v-aţi dat seama că este frumoasă?
Solomon Marcus: În anii de şcoală, matematica s-a
ascuns de mine. Purta o mască. Abia la universitate a lăsat
masca deoparte şi am putut să descopăr frumuseţea ei.
A.T.: L-aţi avut profesor pe Ion Barbu. Cum era el
ca profesor?
S.M.: A fost singurul meu profesor care m-a introdus
în laboratorul său de lucru, am putut vedea cum arată
matematica în stadiul ei de întrebare şi de căutare, nu
numai în cel de defilare în faţa tribunelor de spectatori.
A.T.: Spuneaţi că matematica şcolară a pierdut
latura narativă . V-a „narat” matematica profesorul Ion
Barbu?
965

Solomon Marcus

S.M.: Mai mult, ne-a dezvăluit, pe lângă latura
narativă, pe aceea dramatică.
A.T.: El spunea că „poezia este o prelungire a
geometriei, aşa că, rămânând poet, n-am părăsit niciodată
domeniul divin al geometriei”. Se poate traduce asta prin
„aşa cum matematica are nevoie de soluţii, şi poezia are
nevoie de decodificare”?
S.M.: Nu. Exact cum poezia nu se poate traduce în
limbaj comun, nici limbajul matematic nu e traductibil în cel
ordinar. Dar cuvintele au un rol esenţial în limbajul matematic.
A.T.: Ce este, de fapt, matematica?
S.M.: Are prea multe înfăţişări pentru a o putea
prinde într-o definiţie. Dar, cum bine a observat Gr. C.
Moisil, are ceva dintr-o amantă: ea se răzbună nu atunci
când o ataci, ci când o neglijezi.
A.T.: Matematica mi-a dat bătăi de cap din şcoală.
Nu sunt un om al cifrelor. Ce mi-aţi spune spune mie, la
13-14 ani, îngrozită de matematică, să mă faceţi să nu mai
fug de ea?
S.M.: V-aş spune să căutaţi cărţi bune, deci altceva
decât manualele şcolare, pentru a descoperi că matematica
este peste tot, că ea reflectă arhitectura minţii noastre şi că
are eleganţa şi subtilitatea artei.
966

Rãni deschise

A.T.: Deci matematica e în toate sau toate sunt în
matematică?
S.M.: Matematica este peste tot, dar prezenţa ei este
indirectă, e mediată de ştiinţă, de artă, de inginerie şi de
spiritualitate. Totuşi, ea nu epuizează lumea, viaţa,
personalitatea noastră. Universalitatea matematicii nu
este de natură dominatoare, imperialistă, ea este
sinergetică, nu conflictuală; gata de colaborare, nu
exclusivistă.
A.T.: Nu faceţi mare diferenţă între cifră şi cuvânt.
Unde se îmbină matematica şi literatura?
S.M.: Cifra şi cuvântul sunt implicate în alfabetele
şi vocabularele care stau la baza civilizaţiei noastre.
Matematica şi literatura se află amândouă sub imperiul
universurilor de simbolizare şi de ficţiune, ele nu pot trăi
fără metaforă şi fără capacitatea pe care o are uneori
localul de a da seamă despre global, fără capacitatea clipei
de a da seamă despre eternitate. Ele nu pot trăi fără
perspectiva infinităţii.
A.T.: Aţi spus într-un interviu că aceste prejudecăţi,
conflictul dintre poezie şi matematică sunt rezultatul unei
educaţii greşite. De ce?
S.M.: O educaţie corectă ar fi trebuit să dezvăluie
şcolarilor solidaritatea poeziei cu matematica, aşa cum am
967

Solomon Marcus

schiţat-o mai sus. Din inerţie şi comoditate, nu s-a
întâmplat acest lucru.
A.T.: Ce i-aţi spune lui Flaubert care zice că poezia
e o ştiinţă la fel de exactă ca şi geometria?
S.M.: Nu ştiu unde şi când a spus Flaubert acest
lucru, dar ştiu că Albert Thibaudet, în a sa «Fiziologie a
criticii», a spus ceva asemănător: «frumosul determină
stări imprecise, dar e realizat cu mijloace precise».
A.T.: Dar putem spune că întâlnim aceeaşi rigoare
din matematică şi în poezie sau în ficţiune chiar?
S.M.: Da, putem spune acest lucru, numai că rigoarea
poetică e de altă natură decât cea matematică; prima se referă
la calitatea sugestiei, a doua are în vedere corectitudinea logică.
A.T.: Einstein spune că „atâta timp cât matematica
se implică în realitate, rezultatele ei nu sunt sigure. Atâta
vreme cât rezultatele matematice sunt sigure, ele nu se
aplică în realitate”. Înseamnă asta un univers de ficţiune?
S.M.: Înseamnă că matematica, întocmai ca şi arta,
are nevoie de medierea unui univers de ficţiune, pentru
a-şi putea manifesta impactul asupra vieţii contingente.
A.T.: I-aţi descoperit pe Arghezi, Blaga, Rilke şi
Esenin înainte să iubiţi cifrele…
968

Rãni deschise

S.M.: I-am descoperit în adolescenţă, un frate al meu
mai mare mi-a adus în atenţie cărţile lor. De atunci, nu
m-am mai despărţit de ei niciodată.
A.T.: Cum convingeţi un profesor de matematică de
liceu sau şcoală că matematica are şi o latură narativă,
care trebuie arătată elevului?
S.M.: Am explicat mereu acest lucru, începând cu
extraordinarul exemplu al Trigonometriei lui Gheorghe
Lazăr. Renunţarea la narativitate se face în dauna sensului,
a semnificaţiei. Riscăm să reducem matematica la
dimensiunea ei sintactică.
A.T.: Şi tot dumneavoastră spuneaţi că singura
educaţie eficientă e aceea care se prevalează de joc.
Profesorii de azi probabil nu au timp, nu au stare, nu au
ştiinţă. Sunt puţini cei care folosesc jocul. A eşuat astfel
şcoala românească? Ce nu are ea, ce-i lipseşte?
S.M.: Am scris o carte întreagă pentru a răspunde
la această întrebare (Paradigme universale III. Jocul).
Fără dimensiune ludică şi fără umor, educaţia nu poate
reuşi. Numai că trebuie să descoperim valoarea jocului
într-o altă ipostază decât aceea a divertismentului. Din
păcate, în mass-media din România nu s-a aflat încă de
această distincţie, nici sistemul educaţional nu o
înregistrează.
969

Solomon Marcus

A.T.: A ucis învăţământul românesc curiozitatea şi
spiritul creator? Spuneaţi că şcoala ignoră total educaţia
frecventării internetului. E adevărat că dependenţa de
internet este asimilată uneori cu o dependenţă nocivă şi
adesea confundată cu dependenţa de jocurile pe
calculator. Tentaţiile fiind mari, un părinte sau un dascăl
va evita introducerea lucrului cu computerul din teama
generării dependenţei. Unde e pragul pentru ca internetul
să nu mai fie privit ca o „babă cloanţă”? Dacă un copil
vrea să caute o informaţie, unde e mai bine să o caute: pe
internet sau într-o carte?
S.M.: «A ucis» e un termen prea tare; de fapt, nu le
educă. Orice copil are curiozitate, problema este în ce
direcţii este ea orientată. Dacă familia şi şcoala nu reuşesc
să direcţioneze curiozitatea copilului spre înţelegerea şi
cunoaşterea naturii, a societăţii, a fiinţei umane, spre ştiinţă,
spre cultură, spre artă, spre spiritualitate, spre afirmarea
gândirii personale, atunci copilul va rămâne poate pentru
totdeauna la nivelul curiozităţilor derizorii şi din astfel de
copii se vor recruta mai târziu cei care (ca la 4 martie 2011)
vor ocupa poziţii la canale de televiziune şi vor programa
«breaking news: Irina rămâne la Iri», iar la 4 aprilie 2011
vor anunţa: «breaking news: Irina trece la Monica», şi tot
aşa cu Oana şi cu Pepe, cu Meme şi cu mai ştiu eu cine.
Din păcate, mulţi aşteaptă astfel de veşti. Şi astfel acţionăm
împotriva unei educaţii corecte. Tot aşa cu internetul.
970

Rãni deschise

Needucat, tânărul va zburda pe internet în zona distracţiilor
facile, a curiozităţilor mărunte, nedemne de atenţia noastră.
Dar internetul e ca lumea, include de toate. Fără educaţie,
te pierzi. Dacă odrasla ar afla de la părinţi, fraţi, profesori
cum anume, la momente potrivite, vizita pe internet la
adrese potrivite îţi satisface dorinţa de a înţelege şi de a
cunoaşte răspunsul la întrebări interesante, care vin în
întâmpinarea celor aflate din manual, atunci nu am mai
ajunge să condamnăm în bloc internetul şi să-l asimilăm cu
drogurile. Nu există niciun conflict între carte şi internet,
parteneri care au nevoie unul de celalalt.
A.T.: Cum credeţi că se naşte omul? Rousseau spune
că omul e bun prin natură, dar că-l corupe civilizaţia. Cu
voia dvs., eu constat o răutate atât de neşlefuită la unii
copii mai mici, care se bucură atunci când chinuie un
animal sau un alt copil.
S.M.: Creierul uman are trei componente, care au
apărut în trei etape diferite ale istoriei sale: creierul
reptilian, care controlează instinctele, creierul
paleomamifer, care răspunde de emoţii, şi neocortexul,
care se află la baza comportamentului raţional al omului.
Se consideră că trecerea de la faza a doua la cea de-a treia
s-a făcut cam pripit, motiv pentru care fiinţa umană a
rămas cu urme de comportament schizoid, pe care numai
educaţia şi cultura le pot ţine sub control. Dar cum să le
971

Solomon Marcus

ţină, dacă, iată, la 6 aprilie, pe la ora 9 dimineaţa, văd la
televiziune filmul «Dovadă de iubire», episodul 193,
precedat de explicaţia «Genul programului: dramă. Copiii
sub 12 ani îl pot viziona numai cu acordul părinţilor sau
împreună cu familia», şi chiar de la început violenţa şi
împuşcăturile sunt la ele acasă: «du-mă repede, mă
urmăreşte cutare, ştii că l-am omorât pe fratele lui», «hai
mai repede, că-şi sparg creierii», mişună la tot pasul
indivizi cu pistoale, pentru a nu mai vorbi de pleava socială
care populează întregul film. Iată ce li se recomandă
copiilor sub 12 ani. În acest fel stimulăm urmele de
comportament instinctual pe care le-aţi remarcat la copii
şi sabotăm formarea unor deprinderi pozitive.
A.T.: E o soluţie ca statul să se implice în viaţa
copilului din primii ani de viaţă?
S.M.: Ştiu că au existat proteste, totul depinde de
modul în care se implică. Într-o societate în care o mare
parte a familiilor nu acordă (din motive materiale sau/şi
culturale) grija de care pruncii ar trebui să beneficieze, cred
că implicarea statului este necesară.
A.T.: „Noi ştim că unu ori unu fac unu/ dar un inorog
ori o pară/ nu ştim cât face/ ştim că cinci fără patru fac
unu/ dar un nor fără o corabie/ nu ştim cât face/ ştim, noi
ştim că / opt împărţit la opt fac unu,/ dar un munte împărţit
972

Rãni deschise

la o capră/ nu ştim cât face./ Ştim că unu plus unu fac doi/,
dar eu şi cu tine/ nu ştim, vai, nu ştim cât facem”, spune
Nichita Stănescu în „Altă matematică”. Domnule profesor,
fac unu plus unu întotdeauna doi?
S.M.: Nici în matematică, unu plus unu nu fac
întotdeauna doi, de exemplu, în aşa-numita aritmetica
modulo 2, unu plus unu fac zero. Toate depind, ca în viaţă,
de convenţiile pe care le adoptăm.
A.T.: Aţi spus la un moment dat că „la 19 ani, după o
perioadă în care singura preocupare era să supravieţuiesc,
când am văzut că am scăpat cu viaţă, am decis să nu pierd
vremea, să folosesc fiecare clipă”. Cum aţi reuşit să
supravieţuiţi? Ce aţi decis, atunci la 19 ani, ce v-aţi zis să
faceţi cu fiecare clipă apoi, cu clipa de vieţuire?
S.M.: Am supravieţuit ca urmare a tăriei mele
sufleteşti, pe de o parte, şi a condiţiilor obiective ale
războiului, pe de altă parte (în faţa unui război care se
profila pierdut, România se orienta spre o posibilă
reconciliere cu S.U.A. şi cu Marea Britanie, fapt care a
determinat o schimbare de atitudine în politica internă).
A.T.: Şi atunci, când v-a fost cel mai frică: în timpul
celui de-al Doilea Război Mondial sau în timpul
comunismului? Când aţi suferit cel mai mult? V-a venit
vreodată să daţi bir cu fugiţii?
973

Solomon Marcus

S.M.: Frica mi-a fost cultivată de toate regimurile
dictatoriale, începând cu guvernul Gigurtu, care m-a
eliminat din şcoală, până la regimul Ceauşescu, care-mi
îngrădea libertatea de comunicare şi de mobilitate. Dar au
existat întotdeauna anumite forţe care au acţionat în sensul
apropierii de cultura românească şi am fost mereu convins
că numai în limba română mă pot exprima cu întreaga
nuanţare necesară. Pentru mine, nevoia de a mă exprima a
fost întotdeauna vitală.
A.T.: Cum vă distraţi când eraţi tânăr? Cum
înţelegeaţi distracţia?
S.M.: Distracţia, în sensul vulgar în care o vedem
practicată azi în cea mai mare parte a mass-mediei, mi-a fost
întotdeauna străină. Cartea a fost mereu aliatul meu
permanent, alternând-o pe aceasta cu nevoia de plimbare, de
contemplare şi savurare a spectacolului străzii şi al naturii.
A.T.: Spuneaţi într-un interviu că nu înţelegeţi
această pierdere de timp a oamenilor care stau la coadă,
dar în timpul acesta nu citesc o carte. Credeţi că
românului de azi îi mai arde să citească, ba mai mult, să
citească atunci când stă la coadă, când el trebuie să dea
bani în stânga şi-n dreapta, iar buzunarul îi e cam gol?
S.M.: Puneţi problema într-un mod brutal, mizând pe
cazuri limită. Dar să ştiţi că tocmai în situaţii de acest fel
974

Rãni deschise

se testează mai bine nevoia de cultură. Îmi aduc aminte,
din vremea când lucram la un Proiect al Universităţii
Naţiunilor Unite (Tokyo), că în unele ţări din lumea a treia,
cum se numeau ele atunci, nevoia de identitate este mai
puternică sau cel puţin egală cu aceea de protecţie. Nevoia
de lectură ar trebui să fie organică. Dacă ţi s-a format
nevoia de lectură, ea devine la fel de vitală ca nevoia de
mâncare.
A.T.: V-aţi plictisit vreodată?
S.M.: Niciodată; şi nici de insomnie nu am suferit.
A.T.: Ce naşte plictiseala? Se naşte ea din ignoranţă?
S.M.: Plictiseala este un produs al inculturii, al
ignorării minunilor care ne înconjoară la tot pasul.
A.T.: Dar ce este mai grav: faptul că se naşte
ignoranţa sau că ea se perpetuează şi poate naşte alţi
„monştri”?
S.M.: Mai grave decât ignoranţa sunt pierderea
capacităţii de a se mira, de a se întreba, a nevoii de a
înţelege lucrurile, de a-i înţelege pe oameni, pierderea
bucuriilor elementare, cum ar fi spectacolul diversităţii
umane, al lumii vii şi al celei inerte. Absenţa bucuriei
imense de a putea respira, de a putea privi, de a ne putea
mişca. Acestea mi se par carenţele cele mai grave, nu
975

Solomon Marcus

ignoranţa; aceasta din urmă se referă mai degrabă la
absenţa cunoştinţelor, dar constatăm mereu că multe
cunoştinţe sunt formale, deoarece nu sunt înţelese.
A.T.: Aţi făcut vreodată compromisuri?
S.M.: Cele mai multe împrejurări conduc la
compromisuri, ele revin la faptul că anumite scopuri nu
pot fi atinse concomitent, ci unele în dauna altora.
Renunţăm deci la atingerea anumitor obiective, pentru a
realiza altele. Filosofia compromisului este să obţii un
avantaj într-o anumită privinţă, cu preţul unui dezavantaj
într-o altă privinţă. Dar există o întreagă tipologie a
compromisului, uneori el este rezultatul unui act deliberat
de decizie personală, alteori îl practicăm fără a-l
conştientiza. Multe compromisuri ţin de natura lucrurilor,
nu noi le decidem, ci ele ne manipulează pe noi. Am scris
mult pe această temă.
Chiar dvs, când v-aţi referit, într-o întrebare anterioară,
la observaţia lui Einstein, conform căreia, atâta vreme cât
matematica se aplică la realitate, rezultatele ei nu sunt
sigure, iar când sunt sigure, nu se mai aplică la realitate, aţi
dat un exemplu de compromis inevitabil: aplicarea la
realitate se obţine cu preţul sacrificării certitudinii. Dar în
comunicarea de fiecare zi, vă daţi seama că protecţia unui
mesaj, evitarea alterării acestuia cer compromisul
sacrificării scurtimii mesajului, care va trebui să conţină,
976

Rãni deschise

pe lângă elemente de comunicare, şi elemente de control?
Sunt şi exemple mai complexe. Noile generaţii înţeleg greu
sau deloc faptul că sub comunism a fost nevoie ca unii
intelectuali să accepte compromisul comunicării cu puterea
politică, pentru a limita acţiunea negativă a acesteia; iar a
înţelege în ce fel unii au limitat acest compromis la strictul
necesar, pe când alţii l-au prelungit până la obţinerea unor
avantaje personale, iată o chestiune delicată, care pretinde
o examinare atentă a faptelor.
A.T.: Spuneaţi într-un interviu că în anii ’80 aţi dat
meditaţii pentru produse. Cum se ajunsese aici şi ce
primeaţi?
S.M.: A fost o perioadă în care lipseau produse
alimentare de bază (lapte, brânză, carne, legume, fructe),
iar eu, operat în 1981 de ulcer duodenal cu peritonită,
aveam nevoie să urmez un regim alimentar foarte riguros.
Am fost atunci nevoit să acord meditaţii unor elevi ai căror
părinţi aveau relaţii la ţară şi mă recompensau nu cu bani,
ci cu produse proaspete, în special lactate, zarzavaturi şi
fructe.
A.T.: V-aţi simţit vreodată umilit?
S.M.: Da, în special în perioada 1938-1944 şi în
perioada stalinistă, am discutat despre aceasta cu alte
ocazii şi nu aş mai vrea să revin.
977

Solomon Marcus

A.T.: Sunteti un tip solitar?
S.M.: Am o nevoie organică de afirmare socială, de
a mă face folositor altora, cea mai mare satisfacţie a mea
este să constat că-i pot ajuta pe alţii; de aici pasiunea mea
pentru profesia de dascăl, catedra este pentru mine un loc
privilegiat, pe care simt nevoia să-l onorez. Dar, pentru
a-mi pregăti prestaţia mea publică, am nevoie de perioade
prealabile de însingurare, de retragere în laboratorul meu
personal. Dacă-mi permiteţi un joc de cuvinte, solidaritatea
mea cu lumea se pregăteşte cu perioade în care trebuie să
rămân solitar. A rămâne solitar înseamnă pentru mine şi a
fi atent la ceea ce-mi spune subconştientul, la visele care
mă vizitează şi care uneori sunt foarte semnificative.
A.T.: V-a fost vreodată ruşine că sunteţi român şi de
câte ori aţi fost mândru că sunteţi român?
S.M.: Pentru mine, a fi român nu poate fi, în sine, nici
calitate, nici defect. Nu mi-am pus niciodată problema de
a mă ruşina sau de a mă mândri cu acest fapt. Mă pot
ruşina sau mândri numai cu ceea ce aparţine acţiunii mele
personale, cu eşecurile sau succesele mele. În acest spirit
i-am educat şi pe studenţii mei.
A.T.: Care e scutul omului Solomon Marcus în faţa
dificultăţilor vieţii? Le-aţi luat în piept sau aţi preferat să
purtaţi o mască şi să ascundeţi ce vă doare?
978

Rãni deschise

S.M.: Aş înlocui cuvântul mască prin acela de
scenariu. Trăiesc într-un orizont temporal indefinit, caut să
menţin un echilibru între amintiri şi aşteptări, între
memorie şi proiecte. Viaţa este pentru mine un spectacol
care, în fiecare dimineaţă, se schimbă radical şi nu mă satur
să-l contemplu. Scutul la care vă referiţi este pentru mine
starea de bucurie generată de lucrurile cele mai comune.
A vedea copii, tineri, animale şi copaci, a citi o pagină de
carte, a deschide internetul şi poşta electronică, a asculta
expunerea unui coleg, a prinde o melodie care vine de nu
ştiu unde, a răsfoi o revistă proaspătă, din domeniile care
mă preocupă, a savura somnul în momente de oboseală, a
fixa pe hârtie un gând care tocmai mi-a trecut prin minte,
a face un duş cald înainte de culcare, a savura un fruct pe
stomacul gol, a mă mişca printre rafturile unui magazin
sau printre tarabele unei pieţe, toate sunt transformate, în
scenariul care-mi orientează viaţa, în surse de bună
dispoziţie, dar şi de frământare şi de problematizare. Cele
mai multe gânduri inspirate nu-mi vin la masa de scris, la
biroul de lucru, ci în cele mai variate împrejurări, ca cele
descrise mai sus.
A.T.: Spuneaţi într-un alt interviu că în fiecare
dimineaţă ar trebui să ne trezim ca şi cum atunci începem
viaţa. Să avem o capacitate mare şi de uitare aşa cum
avem de memorare. Reuşiţi să faceţi asta?
979

Solomon Marcus

S.M.: Da, practic acest exerciţiu de multă vreme şi a
devenit a doua mea natură.
A.T.: Credeţi că este ceva ce poporul român a
preferat să uite pentru că nu a mai vrut să ţină minte?
S.M.: Îmi puneţi o întrebare foarte delicată. Dar puteţi
observa şi dvs că între istoricii noştri, între oamenii noştri
de cultură, această chestiune este o temă foarte
controversată, mai cu seamă în ceea ce priveşte perioada
anilor treizeci şi patruzeci ai secolului trecut.
A.T.: Dacă ar fi să vă întrebe cineva cine este
Solomon Marcus, ce aţi spune despre dvs.?
S.M.: Este întrebarea cea mai grea. S-ar putea ca
lucrurile cele mai profunde ale fiinţei mele să nici nu poată
fi spuse în cuvinte. De aceea mă pasionează semiotica,
disciplina semnelor, despre care Umberto Eco spunea că
explică posibilitatea minciunii.

980

Rãni deschise

Partea a patra –
Întâlniri cu oameni ai şcolii

981

Solomon Marcus

982

