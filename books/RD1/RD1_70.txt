Rãni deschise

Arta şi ştiinţa. Un mod posibil de a dialoga
Anchetă realizată de Cici Iordache
pentru Almanahul literar, 1973

Dialogul dintre artă şi ştiinţă se poartă de mult şi cel
mai adesea e controversat, dar niciodată încheiat. Iată de
ce am simţit nevoia să-l animăm şi noi, dintr-un unghi
posibil, şi cu o anumită referinţă la arta şi ştiinţa
românească. Am propus câtorva personalităţi ale vieţii
noastre ştiinţifice următoarele întrebări:
1. Ce a însemnat arta pentru formarea dumneavoastră
ca om de ştiinţă? Care este raportul dintre artă şi ştiinţă în
viaţa omului modern?
2. Credeţi că, sub presiunea bombardamentului informaţional la care este supus omul de astăzi, pot surveni
mutaţii fundamentale în gustul lui artistic? Cum prevedeţi
dezvoltarea artei în lumina futurologiei?
3. În ce măsură studiul ştiinţific al artei (estetica,
sociologia artei, psihologia artei, antropologia artistică
etc.), dar mai ales aplicarea metodelor matematice în
studiul artei pot revitaliza opera artistică?
531

Solomon Marcus

4. Consideraţi că tehnicizarea tot mai accentuată a
societăţii contemporane răpeşte sau acordă timp omului
pentru „consumul” artei?
5. S-a spus despre român că este înzestrat îndeosebi
pentru artă, că e născut „poet”, iar atunci când el se dedică
domeniului ştiinţelor s-a dovedit – cel mai adesea – un
iscusit inventator, şi nu întotdeauna un savant propriu-zis,
un teoretician preocupat de fundamente. Care este situaţia
astăzi, în condiţiile socialismului, când ştiinţa a devenit şi
pentru noi o problemă politică vitală?
1. Se spune că ştiinţa descoperă o realitate deja
existentă, în timp ce arta instaurează o realitate nouă. Dacă
ar fi întocmai aşa, influenţa pe care arta ar exercita-o
asupra formaţiei ştiinţifice nu ar fi atât de mare pe cât este
ea de fapt. Dar ştiinţa instaurează de multe ori universuri
noi, solicită capacitatea de invenţie a savantului. Nu
întâmplător mari matematicieni ca Henri Poincaré şi
Jacques Hadamard, care au sondat psihologia cercetării
matematice, s-au referit la ea ca la o invenţie. Matematica
conduce repede la lumi ale căror legături cu realitatea
imediată sunt din în ce mai indirecte (dar tocmai prin
aceasta mai profunde). În aceste condiţii, principala
posibilitate de orientare, principalul criteriu de apreciere a
interesului unei anumite direcţii de cercetare rămâne
gradul ei de coeziune internă şi de legătură organică cu alte
532

Rãni deschise

direcţii. Recunoaştem aici întocmai una din condiţiile
operei de artă, aceea a interdependenţei riguroase a
părţilor, a integrării ei în contextul mai larg în care este
plasată. Este deci normal ca o anumită experienţă în
frecventarea artei să-ţi dezvolte fantezia necesară
elaborării unei construcţii ştiinţifice care nu mai mimează
o realitate accesibilă senzorial, ci se dezvoltă după o
arhitectură inventată ad-hoc. În particular, intuiţia
infinitului, atât de importantă în matematică, este pregătită
de structurile recursive ale operelor de artă.
Acestea sunt doar câteva aspecte ale unor influenţe
pe care le-am simţit deseori, în activitatea mea de
matematician, chiar dacă ele sunt prea indirecte ca să poată
fi vreodată testate.
2. Marshall McLuhan ne-a avertizat asupra unor
simptome care ar sugera un răspuns afirmativ la această
întrebare. Dar nu cred că trebuie să vorbim de o „presiune
a bombardamentului informaţional”, deoarece nu cred că
marea varietate de informaţii care ne sunt azi accesibile ni
se impune ca o forţă a naturii; din ce în ce mai mult,
printr-o acţiune deliberată, noi putem să dominăm aceste
informaţii, să le prelucrăm şi să le ierarhizăm, să reţinem
unele din ele şi să ignorăm altele. Este greu de răspuns
dacă pot surveni mutaţii fundamentale în gustul artistic al
omului contemporan. Ne place să cerem prognoze, dar
533

Solomon Marcus

înainte de a ne aventura în viitorologie ar trebui poate să
ameliorăm cunoştinţele noastre de „trecutologie”; cât de
bine cunoaştem cum a evoluat gustul artistic în ultimele
decenii şi care este fizionomia lui astăzi?
În aceste condiţii, nu se pot exprima decât simple
impresii. Mi se pare că gustul artistic evoluează spre o
maximă concentrare a expresiei. În literatură, de exemplu,
cred că poezia şi eseul vor câştiga teren în dauna naraţiunilor
complicate, proza succintă şi nervoasă în dauna celei care
trenează. Mi se pare manifestă tendinţa către un spectacol
artistic total, care include deci elemente ale tuturor artelor:
poezie, muzică, teatru, coregrafie, arte vizuale.
3. Într-o măsură mare. Argumentele care se aduc
împotrivă se referă fie la conţinutul emoţional al artei, fie la
natura ei metaforică. Dar funcţia estetică nu se identifică nici
cu cea emoţională, nici cu cea metaforică. Există emoţii care
se situează în afara artei, după cum există opere de artă care
solicită nu emoţia, ci intelectul. Există artă fără metaforă şi
există metafore în afara artei. Ceea ce este esenţial operei de
artă este funcţia de centrare asupra mesajului, pentru a folosi
terminologia introdusă de Roman Jakobson. Cu alte cuvinte,
mesajul încetează să fie un simplu trimiţător la un referent,
la un context şi se aşază în centrul atenţiei. Aceasta înseamnă
că limbajul devine un material de lucru, a cărui sintaxă nu o
putem cunoaşte în profunzime fără folosirea unor tehnici de
534

Rãni deschise

cercetare a aspectelor combinatorii (deterministe sau
probabilistice) şi a recurenţelor. Prin prisma acestor exigenţe,
putem înţelege muzica formală a lui Xenakis, explorarea
grafică a notaţiei muzicale întreprinse de Mihai Brediceanu,
cercetările şi experimentele lui Aurel Stroe şi cele ale lui
Anatol Vieru, cercetările generative efectuate de Jacques
Roubaud asupra structurilor prozodice ale poeziei, cercetările
lui Robert E. Müller asupra raporturilor dintre cibernetică şi
comunicarea creatoare, cele ale lui René Huyghe asupra
formei în artă, cele întreprinse de Barron Brainerd, Mihai
Dinu şi subsemnatul asupra strategiei personajelor în teatru.
4. Evident că nu răpeşte, ci acordă. Problema este însă
dacă tehnicizarea nu anulează în om nevoia de artă. Iată, de
exemplu, ce scrie Ilya Ehrenburg în volumul VI din „Oameni,
ani, viaţă”: „În a doua jumătate a secolului al XX-lea, arta a
fost nevoită să-şi restrângă în toate ţările aria de dominaţie.
În aparenţă, s-ar fi zis, dimpotrivă, că şi-a extins-o: tirajele
romanelor au sporit aproape pretutindeni, a crescut numărul
de vizitatori la muzee şi expoziţii, cinematografia s-a întărit,
s-a născut televiziunea. Totuşi, în viaţa particulară a multor
oameni, rolul artei a scăzut. Poate că lucrul s-a întâmplat
pentru că limbajul artei a fost depăşit de cotiturile bruşte în
ştiinţă şi în viaţa socială sau poate că aceste cotituri au dus la
un oarecare indiferentism faţă de artă”. Cred că în ultimele
rânduri reproduse mai sus se află cheia răspunsului. Ultimele
535

Solomon Marcus

decenii au adus în lume schimbări revoluţionare pe plan social
şi în domeniul tehnico-ştiinţific. Pentru a se putea alinia la
aceste schimbări revoluţionare, arta îşi caută limbaje noi şi
acest proces de căutare este confundat de unii cu decăderea
artei. Dar nu arta este aceea care moare, ci un anumit mod de
a o înţelege şi de a o reprezenta.
5. În domeniul matematicii, generaţiile ulterioare celui
de-al Doilea Război Mondial au adus un mare număr de
talente şi rezultatul este că astăzi avem numeroşi
matematicieni sub 50 de ani, care duc faima ştiinţei
româneşti în lume. Pentru a mă limita la domeniul Analizei
matematice, în care am lucrat vreme îndelungată, voi
menţiona pe: Cabiria Andreian-Cazacu şi Petru Caraman,
specialişti de renume mondial în teoria funcţiilor de
variabilă complexă; Nicu Boboc, Cornel Constantinescu şi
Aurel Cornea, printre primii în lume în teoria potenţialului;
Constantin Conduneanu, cu câteva monografii traduse sau
publicate direct în S.U.A.; Romulus Cristescu, cu
contribuţii serioase în teoria spaţiilor semiordonate; Nicolae
Dinculeanu, cu o monografie de referinţă în teoria
măsurilor vectoriale din august 1972, la Universitatea din
Utah, S.U.A.; Ciprian Foiaş, Aristide Halanay şi Ivan
Singer, ale căror rezultate sunt folosite în mod curent de
către specialişti din întreaga lume; Martin Jurchescu, cu
contribuţii remarcabile în teoria spaţiilor analitice.
536

