Solomon Marcus

Discurs susţinut la Facultatea
de Matematică şi Informatică
a Universităţii din Bucureşti
Bucureşti, 5 martie 2010

Mulţumesc antevorbitorilor pentru afecţiunea şi
admiraţia exprimate, de a căror sinceritate nu mă îndoiesc,
şi în mod special îi mulţumesc doamnei Lavinia
Spandonide pentru că, fără sprijinul d-sale, nu numai că
această carte nu ar fi putut să apară, dar nici dezbaterile la
care am participat nu ar fi putut fi înregistrate şi nu ar fi
putut prinde formă scrisă. A fost nevoie de un efort
extraordinar de transfer al rostirilor mele din diverse locuri,
pentru ca apoi ele să devină text, pentru că textul scris are
cu totul alte legi decât cel vorbit. Dar acum, ca să fim în
concordanţă cu spiritul acestei cărţi în care una dintre
ideile de bază este aceea a nevoii de educare a spiritului
critic, care nu numai în momentul de faţă, ci dintotdeauna
nu se face aproape deloc în sistemul nostru educaţional,
vă mărturisesc că nu mă simt prea confortabil după atâtea
elogii care mi-au fost adresate, atâtea superlative, după
440

Rãni deschise

care nu mai urma decât să fiu purtat pe umeri şi să fiu
sanctificat. Ceea ce găsiţi în această carte, în mare măsură,
sunt lucruri discutabile, sunt opinii care au nevoie de
reacţii critice.
Problema pentru mine a fost să atrag atenţia asupra
unor răni deschise existente în sistemul nostru educaţional.
Poate vi se va părea că acest scop este prea modest. Nu,
nu e deloc modest. Am observat că foarte mulţi dintre
colegii mei implicaţi în sistemul educaţional nu văd aceste
răni. Nu le diagnostichează ca atare, deci e nevoie mai întâi
de această metaforă medicală: „E sau nu bolnav sistemul
nostru educaţional?” Şi eu spun că este grav bolnav. Mulţi
răspund la această întrebare într-un mod liniştitor, la modul
că sigur că e loc de mai bine, că întotdeauna aşa a fost, deci
mai degrabă un soi de îndemnare la linişte, la pasivitate.
În primul rând împotriva acestei stări de lucruri, acestei
pasivităţi simt nevoia să acţionez. Să reuşesc să conving
un număr cât mai mare de oameni că există în sistemul
nostru educaţional şi, implicit, în societatea românească
răni deschise de o foarte mare gravitate, care se
repercutează prin consecinţele lor asupra întregii vieţi
sociale. Dumneavoastră puteţi urmări fenomenul următor:
cum arată mass-media în momentul actual. Arată în aşa fel
încât pare că poporul român este un popor de infractori şi
de aceea vedem că cea mai mare parte a ştirilor se referă
la dări în judecată, persoane care sparg bănci, case de
441

Solomon Marcus

schimb, la cei care dau cu toporul în cap nu ştiu cui şi pe
urmă se organizează talk-show-uri la televizor şi concluzia
este că tot la educaţie se ajunge.
În ultimă instanţă, orice discuţie privind diverse situaţii
patologice duce la deficienţe în sistemul educaţional. Rolul
acestei cărţi este tocmai acela de a identifica situaţii de acest
fel, deci patologice. Sunt, de exemplu, unele lucruri care, în
ciuda gravităţii lor, au trecut aproape neobservate. Ştiţi că,
de pildă, vreme de câţiva ani, Darwin şi darwinismul au fost
scoase din programa şcolară şi s-au perindat, la vremea
aceea, vreo patru miniştri ai învăţământului şi nimeni nu a
observat nimic. A trebuit să vină Academia Română să facă
o sesiune specială, să dea un semnal de alarmă. Sau alt lucru
despre care am mai vorbit şi care a trecut neobservat:
obligarea elevului de a preda manualul la sfârşit de an. Acest
fapt trădează o optică bolnavă despre ce înseamnă a învăţa.
Imediat mi-am adus aminte ce însemna pentru mine
manualul şcolar. Simţeam nevoia să ştiu că e al meu, pentru
că însemnam pe el, când învăţam, ce înţeleg, ce îmi place,
ce nu. Acum mulţi dintre elevi nu mai simt această
necesitate şi, chiar dacă unii încă o resimt, nu au cum să o
rezolve, nu îndrăznesc să protesteze. Raţiunea era una
stupidă şi inconştientă – aceea a economisirii de bani prin
nereeditarea manualelor. Dar această pagubă în modul de a
înţelege învăţarea este imposibil să o compensezi printr-o
economie de bani.
442

Rãni deschise

Sau modul lamentabil în care arată problemele de
admitere la liceu şi problemele de la bacalaureat, în
general, nu numai la matematică. Asta trădează modul
fals de înţelegere a ceea ce trebuie să reprezinte
bacalaureatul, care acum n-ar mai trebui să fie un examen
ca acelea din cursul şcolii, în care verifici chestiuni de
ordin tehnic, ci unul prin care să te califici ca intelectual,
ca om de cultură. Nicio înţelegere a acestui fapt. Să vezi
ce reţii după ce ai uitat detaliile, cum corelezi diferitele
discipline pe care le-ai învăţat. Nimic din toate astea!
Sau totala insensibilitate faţă de acţiunea dezastruoasă,
anti-educaţională a unui număr considerabil de emisiuni
de televiziune. A nu organiza, la 15 ani după introducerea
internetului, care marchează o nouă etapă în evoluţia
societăţii, educarea frecventării internetului este o
neglijenţă pentru care plătim foarte scump mai târziu.
Frecventarea internetului este lăsată la voia întâmplării
şi apar chiar intelectuali care identifică internetul cu
drogurile, transferă dependenţa de droguri la dependenţa
de internet.
În carte veţi găsi o listă mai lungă de astfel de răni
deschise, faţă de care societatea noastră, clasa politică nu
sunt capabile să reacţioneze şi nici nu sunt conştiente de
existenţa lor. Acum voi explica de ce am adoptat metafora
„spectacolului”. Pentru că mi-am dat seama că spectacolul
îmi permite să încadrez coerent toate problemele
443

Solomon Marcus

sistemului educaţional: personaj, roluri, punere în scenă,
indicaţii de regie, conflicte, măşti, elemente de circ,
carnaval. Mai mult chiar, ai posibilitatea de a exprima
într-un limbaj uniform toate păcatele sistemului
educaţional, toate paradoxurile sale. Spre exemplu, în
anumite situaţii actorii trebuie să fie propriii lor regizori.
Spectatorii aici nu pot sta într-un singur loc ca la teatru, ci
trebuie să se mişte pentru a vedea spectacolul educaţional,
să schimbe perspectiva. La Colegiul Naţional „Spiru
Haret”, când am discutat cu elevii, s-a putut vedea că sunt
situaţii în care trebuie să iei o anumită distanţă în spaţiu şi
în timp pentru a sesiza cum sunt văzute ele la nivelul
vârstei de 14 ani, apoi să te pui în situaţia perspectivei celor
de vârsta a treia, pentru că, din păcate, aceştia îşi amintesc
de şcoală într-un mod selectiv, din cauza trecerii anilor, un
mod sedimentat al părerilor despre ceea ce a reprezentat
şcoala pentru ei. Iată de ce vorbeam despre schimbarea de
perspectivă.
Actorii importanţi lipsesc sau apar rar. E vorba de
elevi, studenţi, părinţii lor şi profesorii lor. Or, în atenţia
mass-mediei nu sunt aceştia, nu. În atenţia lor sunt
gazetarii, sunt oamenii politici, sindicaliştii. Probabil
pentru că aceştia sunt cei care finanţează mass-media şi o
întreţin. Deci tot tabloul este deformat şi asistăm la o
întreagă patologie. Vocile elevilor mai mari, nu ale celor
din primară, ci ale adolescenţilor pot spune lucruri foarte
444

Rãni deschise

interesante, dar spuneţi-mi de câte ori aţi văzut în Tribuna
învăţământului păreri de-ale elevilor sau studenţilor. Cele
mai multe voci care dau alarma la modul global că
sistemul educaţional românesc este prost sunt din afara
sistemului educaţional, cu o singură excepţie: a fostului
rector al Universităţii din Bucureşti, Ion Mihăilescu, care
a condus un număr de ani asociaţia pe ţară a rectorilor şi
care repeta neîncetat că 80% din diplomele universitare
eliberate de România sunt fără acoperire. Oamenii au acea
diplomă, dar nu ştiu să facă ceea ce scrie pe acea diplomă.
(n.n.: Acest discurs, rostit la o dată aniversară,
a coincis cu lansarea cărţii Educaţia în spectacol,
fapt care explică referirile la această lucrare.)

445

