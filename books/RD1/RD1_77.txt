Solomon Marcus

Despre critica literară
Anchetă realizată de Ion Bogdan Lefter
pentru Caiete Critice, 1984

În decembrie 1984, Ion Bogdan Lefter mi-a adresat,
din partea Caietelor critice, câteva întrebări. Iată-le, cu
răspunsurile mele (într-o variantă revăzută faţă de aceea
predată în mai 1985 lui Ion Bogdan Lefter).
– Care sunt caracteristicile momentului critic actual?
– Iată câteva aspecte care mi-au reţinut atenţia:
a) Explozia de talente tinere (atât în critica de
întâmpinare, cât şi în aceea „de aprofundare”).
b) Pronunţata tendinţă auto-referenţială. Insuficientă
raportare la celelalte domenii social-culturale.
c) Contaminarea multor cronici literare de ambiţiile,
instrumentele şi mentalitatea studiului critic.
d) Insuficienta luare în consideraţie a literaturii care
nu s-a cristalizat (încă) sub formă de carte.
e) Proliferarea aspectelor ludice şi experimentale
(a se vedea înviorarea stârnită de romanele criticilor).
582

Rãni deschise

f) Amploarea tot mai mare a criticii orale de bună calitate,
în cadrul unor cenacluri (şi aici excelează mulţi tineri).
g) Vocaţie şi vervă critică la tineri poeţi (de exemplu,
Dinu Flămând, Doina Uricariu, Grete Tartler, Mircea
Cărtărescu şi Florin Iaru). Manifestarea cea mai puternică
a vocaţiei critice şi analitice la un poet român contemporan
o găsim la Ştefan Aug. Doinaş.
h) Absenţa unor verigi de legătură, a unor stadii de
tranziţie între critica eseistică şi exegeza literară teoretică
şi metodică.
i) Insuficienta claritate în ceea ce priveşte finalitatea
şi destinatarul discursului critic (dialog între profesionişti
ai scrisului sau îndrumarea gustului public?).
j) Timiditate în situarea actului critic într-o
perspectivă interdisciplinară. Absolutizarea unei viziuni
disciplinare, coroborate cu fenomenele semnalizate la b.
k) Insuficientă toleranţă faţă de modalităţile critice
ale altora. Unii foiletonişti încearcă să-i „demoleze” pe cei
„cu metodă”, iar unii dintre aceştia din urmă îi privesc de
sus pe cei mai străluciţi reprezentanţi ai eseului literar.
l) Persistenţa unor mari valori critice (ca ieri,
Călinescu) fără o prezenţă pe măsură în actualitatea literară
europeană. Depăşirea acestei stări anacronice este o parte
a unuia dintre marile deziderate ale culturii româneşti.
– Care sunt responsabilităţile criticii faţă de
literatură?
583

Solomon Marcus

– Prima responsabilitate este aceea de a lua în
consideraţie totalitatea manifestărilor literare româneşti,
de a detecta şi stimula valorile încă în fază incipientă şi de
a orienta, în funcţie de aceasta, afirmarea prin cărţi a
talentelor. Este necesară elaborarea instrumentelor
fundamentale (monografii ale curentelor şi scriitorilor de
seamă, dicţionare şi istorii literare pe genuri, epoci etc.);
să observăm că pentru destui scriitori importanţi nu avem
încă nicio monografie critică.
O reacţie critică autentică la fenomenul literar curent
poate constitui un model pentru o atitudine critică în alte
sectoare ale vieţii social-culturale. Acest fapt conferă
criticii literare o semnificaţie socială şi intelectuală majoră.
Rămâne însă neelucidată modalitatea de trecere de la
responsabilităţile criticii în ansamblu la responsabilităţile
fiecărui critic în parte. De exemplu, putem considera
regretabil faptul că o carte interesantă rămâne nediscutată,
dar este greu să pretindem unui critic anume faptul că n-a
scris despre ea.
Unele probleme de natură mai specială trebuie, de
asemenea, luate în consideraţie. De exemplu, critica de
întâmpinare nu are (încă) forţele necesare pentru a
diagnostica şi situa apariţiile editoriale în domeniul
exegezei literare. Chiar când este vorba de o carte care nu
face caz de „metode noi”, ca aceea a lui M. Bordeianu
privind versificaţia românească, se constată că reacţia
584

Rãni deschise

critică imediată este timidă, superficială, lipsită de elan.
Ce să mai spunem despre o carte ca aceea a Emiliei
Parpală (asupra lui Arghezi), la care puţinele comentarii
care au apărut eludează de fapt substanţa lucrării? Sau
despre cartea lui G. Mandics despre Ion Barbu?
Scriitorii au, la rândul lor, datoria de a respecta
instituţia criticii, de a urmări activitatea critică, de a nu face
presiuni asupra ei şi de a nu o considera ca o activitate
subordonată activităţii literare, ci egală în importanţă cu ea.
– Cum se situează valorizarea în cadrul actului
critic?
– Judecata de valoare (exprimare improprie) este
inerentă criticii de întâmpinare, este chiar raţiunea ei de a fi.
Valorizarea care urmează imediat apariţiei unei cărţi este o
ipoteză asupra acesteia, un pariu al criticului cu cititorii, un
diagnostic rapid, pe baza unei lecturi care nu a avut răgazul
aprofundării şi distanţării. Intuiţia şi curajul au aici un rol
esenţial. Eugen Jebeleanu a pariat de la început pe Ileana
Mălăncioiu (şi pariul a fost câştigat), Nicolae Manolescu a
pariat pe Mircea Cărtărescu, Al. Ştefănescu l-a declarat pe
Matei Vişniec un mare scriitor, iar validarea lui Ioan Morar
ca poet autentic a fost proclamată fără ezitare. Este regretabil
că astfel de atitudini prompte ale criticii de întâmpinare sunt
rare, o anumită lipsă de fermitate şi operativitate
făcându-se simţită. Trebuie stimulat articolul scurt şi
nesofisticat, care situează şi valorizează de o manieră cvasi585

Solomon Marcus

telegrafică noile apariţii. Rubrica inaugurată în acest sens
de „Flacăra” (unde citesc azi, când scriu aceste rânduri,
patru tablete critice ale lui Radu G. Ţeposu) ar putea fi
imitată de cât mai multe reviste.
Am impresia că, în ceea ce priveşte proza, procesul
de valorizare este mai lent şi mai ezitant, chiar în cazul
cărţilor unor autori consacraţi (a se vedea, de exemplu,
receptarea critică a romanelor publicate în 1984 de Breban,
Buzura şi Ţoiu). Faptul că una şi aceeaşi carte este
întâmpinată foarte favorabil de către unii critici şi foarte
nefavorabil de către alţii este un avantaj în măsura în care
aceste opinii sunt prezentate şi argumentate cu bune
intenţii şi cu o anume coerenţă, stimulând astfel pe cititor
la o atitudine activă, de interpretare şi confruntare a unor
puncte de vedere diferite.
Dar procesul de valorizare nu se încheie cu critica de
întâmpinare. Putem spune chiar că pentru unii critici prima
reacţie la o apariţie editorială este menită mai degrabă să
o situeze pe harta literaturii, urmând ca abia ulterior, prin
lecturi succesive şi prin luarea în consideraţie a unui
context cât mai cuprinzător, să se elaboreze o judecată de
valoare. Există mai multe paliere ale valorizării.
După valorizarea preponderent exclamativă pe care
o propune critica de întâmpinare în raport cu ceea ce vede
cu ochiul liber, urmează valorizările reflexive ale criticii
de aprofundare. De aici mai departe pornesc mai multe
586

Rãni deschise

direcţii. Una vizează trecerea de la cărţi la scriitori, de la
scriitori la curente, grupări şi generaţii, de la cronică la
istorie literară. Alta se cristalizează în cercetări mai
tehnice, în care metoda este pe primul plan. Există
impresia că, pe măsură ce investigaţia literară se
tehnicizează, ea se depărtează de actul de valorizare.
Realitatea este alta. Prin tehnicizare, actul de valorizare
nu mai este explicit, ci implicit, nu mai are caracter a
priori, ci unul a posteriori. Orice posibilitate de a
pătrunde mai adânc în structura unei opere este implicit
şi o posibilitate de a-i sesiza mai complex şi mai profund
valoarea. În momentul de faţă se poate spune că mai toţi
marii noştri scriitori şi-au dezvăluit mai profund valoarea
prin investigaţii cu caracter mai tehnic ale operelor lor.
Pretenţia ca aceste rezultate să fie traduse în limba
comună este la fel de inconsistentă ca pretenţia de a
traduce poezia în limba comună. Desigur însă că prin
tehnicizare analiza literară îşi schimbă destinatarul, care
nu mai este publicul, ci mulţimea cercetătorilor literari.
Actul de valorizare nu-şi poate pierde niciun moment
actualitatea şi importanţa. Ierarhii facem tot timpul,
deliberat sau nu. Orice dicţionar literar, orice istorie
literară sunt obligate, chiar prin spaţiul pe care-l acordă
fiecărui scriitor, să propună o ierarhie, iar exemplele pot
continua (premiile literare instaurează şi ele lideri şi
ierarhii, cel puţin ale unui anumit moment literar).
587

