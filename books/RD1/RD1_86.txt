Solomon Marcus

Limba de sărbătoare
Interviu realizat de Irina Munteanu
pentru Jurnalul Naţional, 29 martie 2006

Academicianul Solomon Marcus, iniţiatorul pe tărâm
românesc al lingvisticii matematice, ne-a vorbit despre
maculatoarele elevilor de azi, din care limbajul natural
aproape că lipseşte, dar şi despre ţărăncile din piaţă care
răspund în engleză.
Irina Munteanu: Se vorbeşte şi se scrie mai prost
după 1989 decât înainte?
Solomon Marcus: Nu cred că situaţia s-a deteriorat, dar impresia de deteriorare vine din faptul că
fenomenele nocive sunt mult mai vizibile decât înainte,
deoarece s-a dezvoltat considerabil folosirea publică a
limbii române. Grav este faptul că uneori apar
incoerenţe, greşeli de tot felul, în texte care provin de la
instituţii onorabile (ministere, edituri, tribunale,
primării, bănci etc.). Unele formulare pe care le
completăm, unele circulare şi invitaţii conţin neglijenţe,
666

Rãni deschise

scăpări, inadvertenţe. Numele proprii ale unor
personalităţi sunt scrise de multe ori greşit.
I.M.: La ce vă referiţi când vorbiţi despre
,,dezvoltarea folosirii publice a limbii”?
S.M.: La faptul că există tot mai multe posturi de
radio şi de televiziune, tot mai multe ziare, reviste şi cărţi,
au apărut e-mailul şi internetul. În aceste condiţii, tot mai
multe persoane pot fi observate sub aspectul folosirii
limbajului vorbit sau scris.
I.M.: Ce amintiri aveţi despre folosirea limbii române
înainte de 1989?
S.M.: Ca profesor universitar, făceam dese vizite în
şcoli cu prilejul inspecţiilor de grad. Propusesem un pariu
pe care nu l-am pierdut: în orice şcoală, o privire sumară
asupra gazetei de perete şi asupra diferitelor slogane
afişate în diverse săli va descoperi cel puţin o greşeală,
fie ea de gramatică, de ortografie, de semantică sau pur şi
simplu de imprimare.
I.M.: Credeţi că astăzi lucrurile stau altfel?
S.M.: Nu cred. Mai mult, cred că şi un alt pariu are
şanse bune să fie câştigat: deschizând la întâmplare o carte,
vei găsi pe paginile respective măcar o greşeală sau cel
puţin o inadvertenţă (nu mă exclud din acest pariu).
667

Solomon Marcus

I.M.: Cine e vinovat de faptul că elevii au o
exprimare schematică de cele mai multe ori?
S.M.: Sunt mulţi factori care conduc la el; ce fel de
limbă română aude copilul vorbindu-se la el acasă? Dar
pe stradă? Dar la şcoală? Dar la televizor? Mentalitatea
conform căreia de modul de exprimare se ocupă numai
profesorul de limba română este destul de răspândită,
chiar dacă nu e afirmată explicit. Câţi profesori de
matematică sau de biologie acordă atenţie greşelilor de
exprimare, exprimării prolixe, incoerente? Modul de
exprimare şi de redactare intră în obligaţia oricărui
educator, indiferent de specialitatea sa.
I.M.: Cum s-a ajuns la această situaţie?
S.M.: Tot la şcoală ajungem. Nu se face suficient
educaţia construirii unei relatări (a unei întâmplări, a unei
amintiri, a unei discuţii etc.) sau a exprimării coerente şi
fără prea multe cuvinte de prisos a unui punct de vedere.
Cele mai multe relatări abundă în anacoluturi. În loc de
fraze cu cap şi coadă, apar frânturi de fraze. M-am uitat
în maculatoarele elevilor, am întâlnit pagini întregi în
care limbajul natural este aproape absent, înlocuit cu
simboluri de tot felul. Totuşi, un raţionament, fie el şi în
domeniul algebrei sau al chimiei, nu se poate dispensa
de cuvinte.
668

Rãni deschise

I.M.: În condiţiile actuale, în care televizorul şi
internetul ne pun în contact permanent cu cei care se
exprimă în alte limbi, iar călătoriile în străinătate sunt
incomparabil mai frecvente decât înainte, ce modificări se
observă în limbă?
S.M.: Mi s-a întâmplat ca o ţărancă mai în vârstă, de
la care cumpărasem nişte ouă, să-mi spună, după ce
numărase banii primiţi de la mine: ,,OK”. Mirat, am
întrebat-o: ,,Vorbiţi engleza?”, la care mi-a răspuns: ,,Of
course!”, dar după aceea a adăugat, pentru a risipi
nedumerirea mea: ,,Am pe fiica mea la engleză”.
Neologismele se înmulţesc, am văzut folosindu-se cuvinte
năstruşnice cum ar fi ,,computeristic”. Timpul şi uzul vor
fi sita care va separa pe cele care rezistă şi intră în limbă
de cele parazite, care vor dispărea. Acest proces este
complicat, Academia Română şi alţi factori de cultură
încearcă să-l monitorizeze, să-l îndrume. În unele reviste
există rubrici dedicate problemelor de limbă; remarcabilă
este rubrica ,,Păcatele limbii”, pe care Rodica Zafiu o ţine
în România literară.
I.M.: Noile tehnologii au o influenţă mare în evoluţia
limbii române?
S.M.: Comunicarea electronică se va răspândi tot
mai mult şi deocamdată vă pot spune că m-am
669

Solomon Marcus

familiarizat, ca şi mulţi colegi ai mei, cu folosirea limbii
române în absenţa semnelor diacritice. Se constată că
foarte rar apar ambiguităţi care să nu poată fi rezolvate
cu ajutorul contextului. Iată deci o problemă care merită
a fi studiată: ce câştigăm şi ce pierdem renunţând la
semnele diacritice?
I.M.: Cum se prezintă limba română la televizor?
S.M.: Subtitrările în limba română, la unele filme,
lasă uneori mult de dorit, ortografic şi gramatical.
Transcrierea mesajelor primite de la telespectatori, în
timpul unor talk-show-uri, scoate uneori la iveală un
adevărat masacru lingvistic. Ce să mai spunem de
vulgarităţile care apar în unele mesaje, la ore de
noapte?
I.M.: În aceste condiţii, către ce ne îndreptăm?
S.M.: Un lingvist australian, într-o carte publicată în
urmă cu vreo zece ani, ajunge la concluzia că peste vreo
patru sute de ani nu vor rezista ca limbi internaţionale
decât engleza, chineza şi spaniola.
I.M.: Şi celelalte?
S.M.: Vă voi răspunde cum mi-a răspuns un francez,
când l-am întrebat despre competiţia cu limba engleză şi a
recunoscut că engleza este de preferat, ca limbă de lucru:
670

Rãni deschise

,,Le français, c’est pour la fête!” (,,Franceza este pentru
sărbătoare!”). Şi româna va rezista, cel puţin pentru zilele
de sărbătoare!

671

