Solomon Marcus

Pornind de la un zâmbet
Interviu realizat de Irina Airinei
pentru Cronica română, 21 februarie 2006

Irina Airinei: Ce mai face academicianul Solomon
Marcus?
Solomon Marcus: După lansarea, la sfârşitul anului
2005, la Târgul Gaudeamus, a cărţii Paradigme
universale (Editura Paralela 45), aştept să apară
continuarea directă a acestei cărţi, la aceeaşi editură, la
sfârşitul lui martie: volumul Pornind de la un zâmbet.
Primul capitol este o analiză a zâmbetului în toate
subtilităţile şi ambiguităţile lui.
I.A.: Bergson a ales ca temă râsul, dumneavoastră
zâmbetul. De ce aţi ales zâmbetul?
S.M.: Nu este o înţelegere figurată, ci este actul
propriu-zis, banal de a zâmbi. Este o zonă a
subtilităţilor, a nuanţelor, care mă fascinează... nimic
precis, numai presupuneri... Fac anatomia impreciziei,
diverse tipuri de imprecizie, felul în care ele apar în
654

Rãni deschise

diverse domenii: impreciziile în ştiinţă, în artă, în
literatură, în religie. Ori de câte ori A critică pe B, B
răspunde începând prin a arăta că nu asta a vrut să
spună. Tot timpul apare neînţelegerea din cauza
diferitelor forme de imprecizie. Neatenţia este şi ea o
sursă a impreciziei.
A.I.: În ce condiţii imprecizia este utilă, în ce condiţii
imprecizia este dăunătoare? Din ce perspective realizaţi
analiza impreciziei?
S.M.: Din toate perspectivele posibile. Aici apar
aspecte fizice, biologice, psihologice, lingvistice,
literare, sociale, semiotice. Imprecizia este legată,
semantic, de aproximaţie. Am un capitol mare despre
aproximaţie. Toată lumea spune că matematica este o
ştiinţă exactă. Adevărul este că matematica este, în
primul rând, o ştiinţă a inexactităţii. De pildă,
aproximaţia este, aş spune, protagonistul scenei
matematicii. Din cele mai vechi timpuri, matematica
s-a ocupat de aproximaţii. Recurgem la ele când nu avem
nicio cale de a dobândi o cunoaştere exactă. Când
extragem rădăcina pătrată din 2, trebuie să ne mulţumim
cu aproximaţii. Vederea umană: două puncte care sunt
prea apropiate le percepem ca un singur punct. Peste tot
apar aproximaţii. Sunt aproximaţii de condamnat şi
altele care sunt foarte utile.
655

Solomon Marcus

A.I.: Rezultă că matematica este o ştiinţă a
aproximaţiilor?
S.M.: Este o ştiinţă a aproximaţiilor, dar acestor
aproximaţii căutăm să le dăm o anumită rigoare. Atunci
când fac o aproximaţie, e bine să ştiu cam de ce mărime este
eroarea pe care o comit. Dacă, în loc să am o cunoaştere
exactă, am una aproximativă, înseamnă că apare o eroare.
A.I.: Ce consecinţe are aproximaţia în viaţa de toate
zilele? Dar în politică?
S.M.: Tot timpul apar ambiguităţi, aproximaţii,
neclarităţi, deci tot timpul ne lovim de imprecizie. În
măsura în care informaţia devine, cantitativ, enormă, ea se
transformă în zgomot. În altă ordine de idei, această ceartă
între lumea musulmană şi lumea occidentală e tot
rezultatul unui astfel de bruiaj: nu funcţionează
comunicarea între aceste două lumi: fiecare decodifică
mesajele celeilalte după propriile ei coduri. Trebuie să
învăţăm să controlăm aceste fenomene, aceste procese,
pentru a micşora efectele nocive.
A.I.: Ce rol are monitorizarea ştiinţei în reducerea
bruiajului informaţional?
S.M.: Modul în care comunitatea ştiinţifică internaţională urmăreşte, controlează şi evaluează rezultatele
ştiinţifice publicate în diverse reviste de specialitate în lume
656

Rãni deschise

este o activitate extrem de importantă pentru că, iată, sunt
comunităţi naţionale care nu sunt încă în stare să se
automonitorizeze, cum, din păcate, este şi comunitatea
ştiinţifică românească. Noi aşteptăm să fim monitorizaţi la
nivel internaţional, ca să aflăm unde anume ne aflăm în
diverse domenii ale ştiinţei, în raport cu alte ţări, lucru
direct legat şi de intrarea noastră în Uniunea Europeană.
Tot ceea ce se referă la integrarea noastră sub aspect
cultural în comunitatea internaţională intră la acest capitol.
A.I.: Cum se derulează această monitorizare a
ştiinţei?
S.M.: Comunitatea profesională internaţională a
oamenilor de ştiinţă realizează această monitorizare,
diferenţiat, pe domenii: matematică, informatică, fizică
etc. Există în SUA un Institut de informare ştiinţifică, la
Philadelphia, se editează o lucrare enciclopedică – Science
Citation Index care consemnează, pentru fiecare articol
publicat într-o revistă de specialitate, care sunt autorii care
s-au referit la articolul respectiv. Lucrarea aceasta este
organizată pe trei direcţii: ştiinţe exacte, ştiinţe sociale şi
direcţia umanistă: literatura, artele... Din această lucrare
poţi afla dacă articolul ştiinţific pe care l-ai publicat
într-o revistă de specialitate şi într-o limbă internaţională
a interesat pe cineva care să-l preia, să-l dezvolte etc.
Există o altă revistă, Scientometrics, care discută criteriile
657

Solomon Marcus

de evaluare ale informaţiilor ştiinţifice. Ipoteza celor mai
lucide minţi este că aproximativ trei sferturi din ceea ce
se publică în lume nu beneficiază de niciun cititor. Fiecare
se leagănă în iluzia că el e în sfertul privilegiat. Dacă s-ar
face o cercetare de piaţă, şi la noi în ţară s-ar constata că,
într-adevăr, cele mai multe cărţi nu au, practic, niciun
cititor, în afară de cei implicaţi în procesul de publicare a
cărţii.
A.I.: Care sunt şansele României de a recepta corect
mesajele altora şi de a le transmite pe ale ei în aşa fel încât
să se facă înţeleasă?
S.M.: De nenumărate ori am atras atenţia asupra
marilor valori ale ştiinţei româneşti. Cele mai multe din
aceste valori nu sunt cele care sunt mediatizate în ţară, ci
sunt cu totul altele.
A.I.: Puteţi să exemplificaţi?
S.M.: În cartea mea, Matematica în România, pe care
am publicat-o în limba engleză în 2003, dau o listă de
câteva zeci de matematicieni români care au avut, în
ultimele decenii, mari succese în lume. La noi în ţară,
tendinţa este ca valoarea să fie dată după gradul de
mediatizare. Dar ştiinţa îşi are drumurile ei şi abia după zeci
de ani se vede care au fost valorile durabile sau nu.

658

Rãni deschise

A.I.: Ce aveţi în şantier?
S.M.: O carte despre acele personalităţi pe care le-am
întâlnit de-a lungul vieţii şi care m-au marcat. Într-un
demers autobiografic, vreau să prezint aceste personalităţi
despre care am scris cu diverse ocazii. Sunt peste o sută,
mulţi dintre ei sunt matematicieni, dar sunt şi lingvişti,
filosofi etc. Voi revizui cartea mea despre timp, cartea mea
despre paradox.
A.I.: Un gând pentru cititorii Cronicii Române?
S.M.: Să înceapă orice zi Pornind de la un zâmbet...

659

