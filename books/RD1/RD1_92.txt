Rãni deschise

Matematica, la fel de inefabilă ca poezia
Interviu de Corina Rujan, publicat în Orizont,
nr. 9, septembrie 2008

Corina Rujan: Stimate domnule academician
Solomon Marcus, vă aflaţi la Timişoara cu prilejul
celei de-a 59-a ediţii a Olimpiadei Naţionale de
Matematică. Aş vrea să vă întreb mai întâi care este
partea pozitivă a unui asemenea eveniment şi care este
rolul olimpiadei pentru formarea unui tânăr mate matician.
Solomon Marcus: Partea pozitivă o constituie
faptul că îi adună laolaltă pe copiii care s-au îndrăgostit
de matematică, pentru că toţi au venit de plăcere la
această olimpiadă. Sigur, ei au trecut prin diverse
selecţii, dar la prima etapă au venit neîmpinşi de la spate
de cineva şi au reuşit să ajungă la această fază naţională.
Deci îi avem la un loc pe cei care iubesc matematica,
pe cei care se bucură de înţelegerea matematicii.
Atrăgând atenţia asupra lor, chiar sub această formă
sportivă a olimpiadei, care nu se potriveşte din toate
767

Solomon Marcus

punctele de vedere cu natura matematicii, s-ar putea ca
în viitor şi alţi copii să se simtă atraşi şi să vină să-şi
încerce forţele la o astfel de olimpiadă. Dacă vreţi, face
parte integrantă din campania permanentă pe care
trebuie să o ducem pentru a-i atrage pe copii la
matematică.
Trebuie să fim lucizi, să vedem realitatea: din
păcate, deocamdată, cei mai mulţi elevi resping
matematica şi la întrebarea „De ce învăţaţi matematica”?
dau răspunsul descurajator: „Pentru că ni se cere la
examen, sau la admitere, sau la nu ştiu ce concurs”. Vrem
ca aceia care dau acest răspuns să devină cât mai puţini
şi chiar să dispară. Acesta este pariul nostru, al
educatorilor de matematică.
C.R.: Aţi vorbit ieri, în Aula Universităţii, şi despre
neajunsurile, totuşi, ale unei asemenea întreceri. Este
olimpiada trambulina cea mai potrivită pentru formarea
unui viitor matematician?
S.M.: Noi impunem la aceste olimpiade o seamă de
reguli, de restricţii, pentru simplul motiv că suntem
obsedaţi de următoarea idee: cum să asigurăm
obiectivitatea criteriilor de evaluare a lucrărilor, a soluţiilor
propuse de elev, în aşa fel încât atunci când unul vine cu o
contestaţie să-i putem arăta uşor de ce a obţinut un anumit
punctaj, şi nu altul.
768

Rãni deschise

C.R.: Şi există asemenea criterii obiective, chiar
dacă nu la modul absolut?
S.M.: Cred că exagerăm în această privinţă şi că am
fi putut proceda ca la gimnastică, unde e un juriu, ai cărui
membri dau note, care pot fi foarte diferite între ele. Ne
ambiţionăm să măsurăm performanţa matematică, lucru
uneori imposibil. Deci, aici, probabil mai trebuie să lucrăm.
C.R.: Există oare nişte etape pe care un elev foarte
talentat ar trebui să le parcurgă pentru a ajunge într-o
bună zi un veritabil matematician?
S.M.: Nu se poate da o reţetă în această privinţă. A fi
matematician, creator, a fi poet, a fi compozitor sunt
lucruri care ţin de imponderabile. Matematica e la fel de
inefabilă ca şi poezia. Dar putem să stimulăm, adică putem
să facem următorul lucru: să evităm acele metode, acele
obiceiuri care, în mod sigur, descurajează posibilele surse
de creativitate. De pildă, unul dintre ele este obiceiul, din
păcate, răspândit mult în şcoală, de a nu stimula, de a nu
educa nevoia şi capacitatea de a formula întrebări; deci de
a nu educa la copil nevoia şi capacitatea de a inventa
întrebări interesante, de a le spune cu voce tare şi de a fi
luat în atenţie cu aceste întrebări.
De multe ori se întâmplă ca, atunci când un elev pune o
întrebare, profesorul să caute să scape cât mai repede de ea,
de teamă că altfel nu-şi îndeplineşte planul. Aici se află una
769

Solomon Marcus

dintre rădăcinile eşecului educaţiei. N-avem voie să facem
lucrul acesta. Întrebarea elevului este lucrul cel mai important.
C.R.: Problemele propuse la olimpiadă vizează
creativitatea sau ele au preponderent un caracter tehnic?
S.M.: Şi una, şi alta. Dar punctajul are în vedere un
anumit mod de rezolvare. Deci problema este ca elevului
să-i dea prin gând.
C.R.: Cum spunea Moisil.
S.M.: Da, da. S-a întâmplat că au fost şi unii care
au încercat alte căi, chiar au reuşit pe alte căi, dar lucrul
acesta s-a întâmplat rar. Sigur că, în găsirea răspunsului,
faptul că unuia îi vine ideea fericită, altuia nu, este o
chestiune care ţine de situaţia din momentul respectiv. Se
ştie că orice om are momente în care mintea lui lucrează
mai intens şi momente în care e relativ mai adormit. Şi
sigur că de aceea se spune că cineva este în formă la un
anumit moment; ca în cazul unui sportiv, care a fost sau
n-a fost în formă în ziua respectivă, n-a prins o zi bună.
Există, asta este evident, acest dezavantaj, pentru că în
mod obişnuit, în munca ştiinţifică sau, în general, de
creaţie, în orice domeniu, dacă încerci într-o zi să scrii,
să lucrezi şi simţi că n-ai dispoziţia necesară, te opreşti
şi încerci mai târziu, a doua zi. În cazul olimpiadei, te afli
sub imperiul a trei dictaturi: dictatura locului în care te
770

Rãni deschise

găseşti – n-ai voie să-l părăseşti, dictatura duratei fixe în
care trebuie să te încadrezi şi dictatura chestiunilor care
ţi s-au dat de rezolvat. În mod obişnuit, un matematician
creator îşi alege el întrebările, problemele de care să se
ocupe. Nu stă cineva să-i impună: uite, de asta să te
ocupi.
C.R.: Fac o paranteză despre cazul lui Dan
Barbilian, care a fost olimpic, a câştigat un premiu, Ţiţeica
a fost preşedintele, a primit nişte cărţi...
S.M.: La Gazeta Matematică. Dar nu, staţi puţin. Eu
nu cred că a fost olimpic. El a rezolvat probleme. Le rezolva
acasă. Să nu confundăm lucrurile. Deci le rezolva acasă, nu
era legat de nicio constrângere, chiar dacă trebuiau rezolvate
într-un număr de zile. Nici nu existau olimpiade atunci.
C.R.: Voiam să mă refer la altceva, la creativitate.
După un timp, ştiu că şi-a dat cărţile primite ca premiu la
anticariat şi nu l-a mai interesat matematica rezolvării de
probleme, pentru că, probabil, căuta creaţia.
S.M.: Dar în ultimii ani ai vieţii i s-a retrezit această
plăcere şi a reluat unele probleme.
C.R.: Probabil aşa, ca pe un rebus înalt.
S.M.: Da, da. Nu, chestiunea aceea cu cărţile la anticariat e o chestiune întâmplătoare, nu trebuie generalizată.
771

Solomon Marcus

Să fie clar, Barbilian nu avea spontaneitate. El, şi ca
profesor, nu avea spontaneitate, nu cred că ar fi dat
randament la olimpiade, dar pe vremea şcolarităţii lui încă
nu existau olimpiade, exista însă Gazeta Matematică, unde
apăreau probleme propuse elevilor, şi el trimitea soluţii pe
baza a ceea ce lucra acasă, deci nu era legat de restricţiile
astea de la olimpiadă.
C.R.: Aţi găsit în lucrările participanţilor la
olimpiadă soluţii care arătau originalitate, soluţii cu totul
ieşite din comun?
S.M.: Eu n-am corectat. I-am întrebat pe colegii care
au corectat. De data aceasta, aşa cum mi-au spus colegii
direct implicaţi, nu. O lucrare cu totul ieşită din comun
n-a existat.
C.R.: Dar în anii precedenţi?
S.M.: Au fost.
C.R.: Şi ce s-a întâmplat cu elevii respectivi?
S.M.: Eu am o broşură care se cheamă Matematica în
România. E în limba engleză. Şi acolo, la un moment dat, arăt
pentru o serie întreagă de foşti olimpici, pentru o serie întreagă
de actuali matematicieni importanţi, trecutul lor de olimpici.
Au fost destul de mulţi. Dar nu e o regulă generală.
Avem şi matematicieni foarte importanţi care n-au
772

Rãni deschise

participat la nicio olimpiadă. Avem şi mari olimpici care
n-au devenit matematicieni, au devenit ingineri sau
altceva, dar mulţi dintre ei au devenit matematicieni
importanţi, e adevărat.
C.R.: Domnule academician, predau într-un liceu
umanist matematică şi istoria matematicii, ca opţional.
Care credeţi că ar putea fi căile prin care un profesor de
matematică în situaţia mea ar putea să atragă interesul
elevilor umanişti spre un studiu serios al matematicii?
Cum s-ar putea ajunge ca la un liceu teoretic umanist
matematica să fie totuşi o disciplină importantă?
S.M.: Da, da, la noi există încă mentalitatea conform
căreia umaniştii au nevoie de mai puţină matematică.
Complet fals. Uite, vi le iau pe rând: în ştiinţele economice,
care sunt o disciplină socio-umană, matematica este esenţială
şi cele mai multe Premii Nobel în economie s-au dat pentru
lucrări matematice; există apoi domeniul lingvisticii
matematice şi computaţionale, de mare anvergură, în care se
lucrează enorm de mult în toată lumea; există psihologia
matematică, iarăşi foarte importantă. Să nu mai vorbesc că
muzica, artele vizuale se prevalează esenţial, chiar în bazele
lor, de matematică. Chiar am scris aici, în registrul Liceului
„Ion Vidu”: „Cum se poate omite faptul că bazele muzicii
occidentale au fost puse de Pitagora, care vedea matematica
şi muzica într-o alianţă indestructibilă?”
773

Solomon Marcus

C.R.: De fapt, până la urmă, interdisciplinaritatea
aceasta este salvarea învăţământului, nu? Comunicarea
între discipline, întrepătrunderea lor sunt foarte
importante, dar ne aflăm încă foarte departe de atingerea
acestui ideal.
S.M.: Evident, sigur că da. Este departe de realitate
din cauză că nu i s-a dat atenţia cuvenită. E păcat.
C.R.: Dumneavoastră, care aveţi preocupări atât de
variate – lingvistică, matematică, poetică matematică,
semiotică, filosofie, istoria ştiinţei..., o sumedenie de
domenii frumoase, deosebit de interesante, cu ce vă
ocupaţi în prezent?
S.M.: În prezent, lucrez la corectura unui nou volum
din seria de Paradigme universale, din care am publicat
trei cărţi la editura „Paralela 45”, şi anume la o nouă
versiune a cărţii mele, Timpul, din anul 1985. Apoi, mă
pregătesc să plec la 21 mai în Finlanda, unde voi participa
la un Colocviu Internaţional de Biologie matematică şi
computaţională.
C.R.: Aş vrea să vă întreb despre libertatea din
matematică şi din artă, despre legătura dintre ele.
S.M.: Libertatea, în artă, de ce? Deci la ce se referă
această libertate? Vedeţi, faptul acesta trebuie înţeles
bine: în ce constă componenta ludică a matematicii?
774

Rãni deschise

Pentru că, în general, şcoala aici eşuează. Predăm
matematica de la ora 8 la 8:50 şi elevii intră în recreaţie
şi se pot juca. Nu. Important este să descopere jocul
chiar în ora de matematică, un joc marcat de libertate şi
gratuitate. Adică eşti liber să încerci diverse căi, fără
teamă că vei fi sancţionat pentru greşeli sau eşecuri,
exact pe dos decât e la olimpiadă. Şi să faci acest
lucru de plăcerea acestei activităţi, nu pentru că ţi se
cere, nu pentru că va trebui să dai examen pentru el, ci
pentru plăcerea spirituală, sufletească a acestei căutări
care ar trebui să se plaseze în directa continuare a
plăcerii pe care o are copilul când e curios să afle şi
mereu pune întrebări.
C.R.: Grigore Moisil spunea că „pentru un copil şi
pentru un matematician jocul e o treabă serioasă”.
S.M.: Da. El a formulat întotdeauna foarte pregnant
ideile acestea, pe care noi le formulăm într-un mod mai
nefericit. Aşa este, da. Şi-mi aduc aminte chiar că odată
era în vizită la Palatul copiilor şi o fetiţă de vreo 4 ani i-a
pus întrebarea: „Dumneavoastră vă plac visele?” Şi el a
răspuns: „Cum să nu, eu am visat odată că eram într-o
şedinţă şi când m-am trezit eram într-adevăr într-o
şedinţă”. Da, vedeţi, asta este o glumă matematică, pentru
că este o glumă care se bazează pe ceea ce se numeşte în
logică auto-referinţă.
775

Solomon Marcus

C.R.: Cu privire la abstractizarea din matematică şi
din arte, adică din literatură, arte plastice, muzică etc., ce
ne puteţi spune?
S.M.: În aceste domenii, există o serie întreagă de
idei matematice care s-au bucurat de mare succes, de
pildă: teoria haosului şi geometria fractală au pătruns în
arte. Avem muzică fractală, literatură fractală. Un
important profesor de literatură română din Statele
Unite, Virgil Nemoianu, observa că legătura literaturii
cu matematica, cu fizica modernă şi cu biologia, cu
informatica este mai puternică decât legătura literaturii
cu ştiinţele sociale. Vă daţi seama? Că te-ai aştepta să
fie invers.
C.R.: Şi Heisenberg, în Paşi peste graniţe, a abordat
această problemă şi dumneavoastră chiar l-aţi citat în
discursul de recepţie în Academie.
S.M.: Da, şi Niels Bohr, în scrierile lui filosofice. Ei
au emis multe idei. Mai e şi teorema lui Gödel.
C.R.: Că „matematica nu e perfect raţională.”
S.M.: Timp de 2000 de ani am trăit în ideea că orice
enunţ sau e adevărat, sau e fals, a treia posibilitate nu
există. Acum ştim că, în anumite condiţii, acest lucru nu
mai este valabil. Secolul al XX-lea a adus o mutaţie în
modul de a înţelege demonstraţia matematică.
776

Rãni deschise

Altă mutaţie a venit din folosirea calculatoarelor în
demonstraţiile teoremelor. Noi eram obişnuiţi ca
demonstrarea unei teoreme s-o concepem ca pe o activitate
exclusiv logică. Acum, prin pătrunderea programelor de
calculator în modul de demonstrare a unor teoreme, ideea
de demonstraţie are o componentă empirică foarte
puternică. Depindem de parametrii fizici şi computaţionali
ai calculatorului la care lucrăm şi de toate vicisitudinile
programării la calculator, de faptul că gândirea algoritmică
nu poate fi întotdeauna controlată tot prin gândire
algoritmică.
Prin ceea ce a pus în evidenţă teorema de
incompletitudine a lui Gödel şi prin promovarea
calculatorului ca instrument de demonstraţie, s-a schimbat
complet modul de a înţelege un enunţ matematic, o
demonstraţie matematică şi o teoremă.
C.R.: Teorema, care e un spectacol, nu? Aşa aţi
spus...
S.M.: Aşa au spus vechii greci.
C.R.: Acum am aflat etimologia, din discursul dvs.
de recepţie.
S.M.: Am luat cuvântul „teoremă” din franceză, dar
rădăcina e din greacă, unde „teorema” înseamnă spectacol;
iar „matematica” înseamnă, în greacă, cunoaştere.
777

Solomon Marcus

C.R.: Da, mathesis: ştiinţă, cunoaştere.
S.M.: Acesta era numele generic al cunoaşterii, da.
C.R.: Sau mathema.
S.M.: Mathema, da. Grecii au dezvoltat mult ideea
aceasta a legăturii dintre matematică şi teatru. Aţi văzut că
discursul meu de recepţie se referă la aceasta.
C.R.: Da, în Teatralitatea limbajului matematic.
S.M.: Da. De altfel, se vede lucrul ăsta, uite, chiar
când predai analiza matematică, noţiunile acestea de
convergenţă, de limită. Spui: se dă un ε pozitiv, să găseşti
un η care depinde de el; şi atunci chestiunea poate fi
concepută ca un scenariu cu două personaje, unul care
propune valori pentru ε pozitiv şi altul care trebuie să
găsească valorile corespunzătoare pentru η, răspunzând
astfel la satisfacerea inegalităţii respective. În cartea lui
Courant şi Robbins, carte importantă...
C.R.: „Ce este matematica?”
S.M.: Da, Ce este matematica? Acolo se pune în
evidenţă acest scenariu teatral, în modul de prezentare a
noţiunilor de bază ale analizei matematice.
C.R.: Foarte frumos.
S.M.: Da.
778

Rãni deschise

C.R.: Matematica = spectacol. Matematica, nu
numai teorema.
S.M.: Da, da.
C.R.: Mi-a plăcut o afirmaţie, pe care aţi făcut-o tot
în discursul dvs. de recepţie, cu privire la faptul că în
viaţa dumneavoastră profesională aţi învăţat nu numai
de la profesorii dumneavoastră, pe care i-aţi avut ca
maeştri, ci şi, nu mai puţin, de la unii dintre discipoli, de
la cei meritorii. Şi l-aţi evocat pe Vasile Ene, un caz
impresionant.
S.M.: Am ales un exemplu. Am preferat să aleg un
exemplu al unuia decedat pentru că mi-era foarte greu să aleg
dintre cei în viaţă. Am mai mulţi, nu pot spune despre niciunul
dintre ei că nu este semnificativ, iar ca să alegi unul dintre ei...
C.R.: Nu era cea mai bună soluţie.
S.M.: Şi atunci am ales unul dispărut, dar nu doar
pentru că e dispărut, ci pentru că este unic în autenticitatea
sa. Vasile Ene venea dintr-o familie atât de săracă, încât
nu avea în casa în care a copilărit nicio carte. Părinţii nu
s-au ocupat deloc de educaţia lui. Şi totuşi, în aceste
condiţii, a avut o pasiune atât de puternică de a cunoaşte,
încât a ajuns matematician. Asta mi s-a părut remarcabil.
În timp ce alţii, care au casa plină de cărţi, nu ştiu să
beneficieze de ele.
779

Solomon Marcus

C.R.: Ce înseamnă talentul!
S.M.: Am avut în istoria matematicii româneşti un caz
similar, unul care este considerat cel mai important geometru
român, Gheorghe Vrănceanu. El venea dintr-o casă de ţărani
săraci analfabeţi. Era destinat să stea cu oile la păscut, pe
câmp. De altfel, primii ani ai vieţii aşa şi i-a petrecut.
C.R.: Revenind la raporturile matematicii cu
literatura, o punte de legătură între matematică şi poezie
poate fi considerat şi mitul.
S.M.: Asta e istoria. Prima fiică a miturilor a fost
literatura. Homer. Să nu uităm că Homer trăieşte cu câteva
sute de ani înainte de primii mari matematicieni greci,
Thales şi Pitagora. Ei vin la câteva sute de ani după Homer.
Deci literatura e fiica cea mare şi matematica este fiica cea
mai tânără.
C.R.: Întotdeauna am crezut că arta are prioritate în
raport cu ştiinţa. Greşesc?
S.M.: Nu ştiu. Aşa s-a întâmplat. Literatura şi
matematica au păstrat o serie întreagă de trăsături
comune, moştenite de la mituri şi, evident, au dezvoltat
şi unele deosebiri, dar toată situaţia aceasta este foarte
complicată, am descris-o într-o serie de lucrări, întâi în
trei cărţi: Poetica matematică, în 1970, Artă şi ştiinţă,
în 1986, şi a treia, Întâlnirea extremelor, Editura
780

Rãni deschise

Paralela 45, 2005. Şi matematica, şi poezia sunt marcate
de această optimizare, cum o numesc eu, „optimizare
semiotică”, de a putea exprima cât mai mult în cât mai
puţin.
C.R.: „Maximum de gând în minimum de
cuprindere” – Gauss.
S.M.: Formidabil cum le ţineţi minte! Îmi place.
Exact, da, da. Şi observaţi că, în poezia sa, Ion Barbu
realizează o concentrare maximă, care n-a mai fost
întâlnită în poezia românească. A polemizat în acest sens
cu Arghezi, cel mai important poet român al secolului al
XX-lea.
C.R.: Vă place mai mult decât Barbu?
S.M.: Nu mai mult, ci altfel. Nu are rost să-i
ierarhizăm pe cei mari. Vă spun acum de ce m-a atras
Arghezi mai mult decât Blaga, este o chestiune pur
personală. Îmi plac cu precădere poeţii care exploatează
posibilităţile limbajului. Arghezi face lucrul acesta
într-o măsură mult mai mare decât Blaga. Blaga nu e atât
un poet de limbaj, cât unul de atmosferă, de reflecţie
filosofică. Dar punctul de polemică al lui Ion Barbu s-a
legat de această optimizare semiotică pe care o
realizează teorema în matematică, marea teoremă, şi
versul în poezie.
781

Solomon Marcus

C.R.: Dar despre Dan Barbilian, ca profesor?
S.M.: A, în cartea mea, Întâlnirea extremelor, am un
capitol mare despre Barbilian şi pagini întregi îl descriu cum
era el ca profesor. Vă invit să citiţi, e greu să spun acum în
câteva cuvinte. A fost un profesor extraordinar, un profesor de
care aveau groază studenţii mediocri şi de care se pasionau cei
talentaţi. Pentru el nu existau studenţi indiferenţi. Pe unii îi
iubea şi pe alţii îi ura. El trăia numai în atitudini extreme. Da,
da, el mereu punea întrebări, el mereu dialoga cu sala şi, dacă
vedea că un student se sustrage acestui dialog, încrunta privirea.
Odată, s-a aflat în sala de curs un vagabond, venit
acolo pentru a evita frigul de afară. La un moment dat,
Barbilian pune ochii pe el şi-i adresează o întrebare legată
de ceea ce preda. Tăcerea vagabondului îl scandalizează
pe Barbilian: „Te pomeneşti că dumneata nu ştii nici măcar
ce este un grup?” Şi atunci se ridică un student şi îi spune:
„Domnule profesor, nu vă supăraţi, dânsul nu e student,
nu e de la noi”, la care Barbilian exclamă: „Ei, da, oricât,
noţiunea de grup este o chestiune fundamentală”.
Acesta era Barbilian.
Acest dialog cu acad. Solomon Marcus a avut loc în
3 mai 2008, la Timişoara, cu ocazia celei de-a 59-a ediţii
a Olimpiadei Naţionale de Matematică. Poeta Corina
Rujan este profesoară de matematică la Liceul Teoretic
„Vlad Ţepeş” din Timişoara.
782

