Solomon Marcus

Societatea de Ştiinţe Matematice din România
Inspectoratul Judeţean Giurgiu

Conferinţa „Matematică şi cultură”
Giurgiu, 29 aprilie 2010

Iată tema care mi-a fost propusă şi din capul locului
trebuie să constatăm dificultatea acestui subiect, pentru
că nu ştim să definim nici matematica, nici cultura. Sau,
dacă vreţi, pentru cultură sunt atât de multe puncte de
vedere, încât e foarte greu să facem o alegere. După unele
dintre ele, ştiinţa este exclusă din teritoriul culturii; după
altele, dimpotrivă, ştiinţa este o componentă importantă
a culturii.
În ceea ce priveşte matematica, dificultatea este la fel
de mare. Era o vreme în care ne descurcam spunând că
matematica este studiul fenomenelor cantitative şi al
relaţiilor spaţiale, definiţie care era naivă şi pentru vremea
în care a fost propusă, dar în niciun caz ea nu mai
corespunde azi complexităţii matematicii şi atunci trebuie
să procedăm pe căi ocolite. De fapt, vrem să înţelegem mai
bine ce-i cu matematica şi cum se poziţionează ea faţă de
alte manifestări sociale, cum se prezintă ea sub aspect
istoric, la ce fel de întrebări răspunde ea şi probabil că, în
446

Rãni deschise

felul acesta, ne situăm în tema formulată iniţial:
matematică şi cultură.
Se întâmplă uneori că nu poţi să defineşti bine doi
termeni a şi b, dar poţi să defineşti mai bine relaţia dintre
a şi b. Iată, am să vă prezint mai întâi diverse feţe ale
acestui cristal care se numeşte matematica şi nu pretind că
lista acestora o epuizează, dar, în orice caz, niciunul din
aceste aspecte nu poate lipsi dintr-un portret cât de cât
semnificativ al matematicii.
Ce este matematica? Este, fără îndoială, un domeniu de
cunoaştere şi cercetare. Deci matematica îşi propune să
răspundă la anumite probleme pe care le ridică setea noastră
de înţelegere a lumii şi de a ne înţelege pe noi. Este apoi o
ştiinţă, este o artă, este o unealtă utilă în anumite situaţii, este
un limbaj. Şi aici am putea adăuga imediat, dacă ne raportăm
la înţelegerea mai ştiinţifică a ideii de limbaj, că matematica
iese din cadrul structurii de limbaj, pentru că limbajul, în
general, comportă o anumită liniaritate, un caracter secvenţial,
în timp ce matematica include şi manifestări care evadează
din liniaritate, din secvenţial şi trec în polidimensional. Deci
este un sistem de semne care trece dincolo de structura de
limbaj, este un mod de gândire, este un catalizator de
transferuri între domenii diferite ale cunoaşterii. Ca să dau un
singur exemplu, matematica însoţeşte transferul ideii de
entropie din termodinamică în teoria informaţiei. Matematica
este un fenomen social, este un joc, este uneori o modă, este,
447

Solomon Marcus

sub aspect psihologic, de multe ori, din păcate de prea multe
ori, mai cu seamă în şcoală, un mijloc de intimidare şi de
terorizare; este, de multe ori, în cercetare, o formă de snobism.
Este, uneori, o posibilă patologie, cum o numea un mare
matematician, o schizofrenie.
Dar acum, revenind la lucruri mai importante,
matematica este un mod de a înţelege lumea la nivelul ei
global, este un mod de viaţă, este uneori un mod de a ne
înţelege pe noi, de a înţelege propria noastră minte, este o
parte a vieţii noastre spirituale şi, ultima pe listă, dar
nicidecum cea mai puţin importantă, matematica este şi o
filosofie.
Fiecare dintre aceste aspecte poate forma obiectul
unei discuţii speciale şi mi s-a întâmplat să fac conferinţe,
de pildă, numai despre matematica-mod de gândire sau
despre matematica-artă. Fiecare din aceste aspecte are o
bogăţie extraordinară. Sau Matematica-mod de a înţelege
lumea. Acesta este, de pildă, lucrul care m-a apropiat pe
mine de Moisil.
Moisil este cel care mi-a dezvăluit această capacitate
a matematicii de a fi un mod de înţelegere a lumii. V-am
dat câteva exemple şi pentru fiecare dintre ele sunt pregătit
să fac o întreagă dizertaţie şi să vă provoc cu întrebări
foarte delicate. Şi dacă ne întrebăm asupra aspectelor
culturale ale matematicii, am impresia că n-avem voie să
neglijăm niciunul dintre aceste aspecte.
448

Rãni deschise

Iată cât de complexă este situaţia şi e foarte greu să
găseşti matematicieni care să fi aprofundat toate aceste
aspecte. Fiecare matematician, după tipul său de experienţă,
de trăire matematică, e mai familiarizat cu unele aspecte,
mai puţin cu altele. De pildă, acum sunt mulţi, din ce în ce
mai mulţi mari matematicieni care simt nevoia să sublinieze
aspectul artistic al matematicii: matematica-artă. Şi, din
matematica românească, pot să vă spun că unul dintre
matematicienii noştri care, în mod special, a accentuat
latura artistică a matematicii a fost Traian Lalescu.
Vă daţi deci seama că, dacă discutăm despre
matematică şi cultură, suntem obligaţi să discutăm despre
toate aceste lucruri şi nu pretind că lista este completă. Şi
asta pentru că matematica are o anumită universalitate, nu
doar în sensul că e relevantă în toate disciplinele, într-un
fel sau altul, dar şi în sensul că are, practic, o infinitate de
aspecte, e inepuizabilă. Şi, din acest motiv, mie mi se pare
că este un fapt inacceptabil de a exclude matematica din
cultură, cum se întâmplă din păcate în mod frecvent, chiar
dacă lucrul acesta nu se face explicit, pentru că oamenii se
jenează să o facă explicit.
De altfel, s-au făcut propuneri de reducere la minimum a prezenţei matematicii în programa şcolară,
cerându-se, în schimb, să se acorde locul principal, cât mai
mult loc aşa-ziselor discipline umaniste, lucru care mie mi
se pare un pleonasm. Pentru că mi se pare că orice
449

Solomon Marcus

domeniu care apare ca rezultat natural al curiozităţii
oamenilor este, prin aceasta, un domeniu umanist.
Dar trăim într-o altă lume, care rezultă, după părerea
mea, dintr-o acumulare de două secole de proliferare a unei
segmentări în discipline cât mai multe şi mai speciale, fapt
care ne-a transformat în victime ale unei viziuni
fragmentare asupra lumii şi asupra realităţii.
Există şi alte moduri de a ataca relaţia dintre
matematică şi cultură. Iată şi un aspect despre care nu am
vorbit: matematica văzută ca istorie. Putem să urmărim,
măcar pentru civilizaţia occidentală, căreia noi îi
aparţinem, modul în care a apărut matematica. Noi o luăm
de la vechii greci, dar, evident, nu neglijăm nici perioada
babiloniană mai veche.
Însă matematica, aşa cum o înţelegem noi astăzi, având
drept protagonist teorema, este o achiziţie a Greciei antice
şi deci un mod de a înţelege matematica ar fi şi acela de a
urmări istoria ei. Iar istoria ei începe într-un mod care a fost
ţinut ascuns în educaţie, şi anume: matematica a apărut ca o
fiică a vechilor mituri. Dar ea este fiica mai tânără, mai
mică, pentru că mai întâi a apărut sora ei, literatura, care este
şi ea tot o fiică a miturilor. Dar Homer îl precedă pe Pitagora
cu câteva sute de ani, deci mai întâi s-a născut literatura şi
apoi fiica mai mică a miturilor, matematica.
Şi o să vă întrebaţi cu ce drept folosesc eu aici această
metaforă: fiică. Iau deci o metaforă biologică şi o transfer
450

Rãni deschise

în domeniul istoriei culturii, şi vă răspund pe ce mă bazez:
pe faptul că şi literatura şi matematica au luat de la vechile
mituri funcţiile lor esenţiale.
Mai întâi funcţia de simbolizare, care este esenţială
în vechile mituri şi e esenţială şi în literatură, şi în
matematică. Apoi, şi literatura şi matematica au luat de la
vechile mituri nevoia de ficţiune. Observaţi că, exact cum
vechile mituri ne plasează în universuri de ficţiune,
literatura face acelaşi lucru şi aceasta o învaţă copiii la
şcoală. Dar faptul că şi matematica face acelaşi lucru a
fost ţinut ascuns. Chiar face acelaşi lucru, cum bine a fost
scos în evidenţă în dialogurile socratice despre
matematică pe care le-a publicat Alfred Renyi (carte
tradusă şi în limba română).
Şi anume ce face matematica, această matematică
bazată pe rigoare, pe teoreme? Ca să ne poată plasa
într-un domeniu al rigorii, matematica se vede nevoită să
înlocuiască universul contingent, empiric, cu unul de
ficţiune. Asta fac Elementele lui Euclid. Cum încep
Elementele lui Euclid? „Numim punct ceea ce nu are părţi,
numim linie ceea ce nu are grosime.”
Deci de la început sunt introduse în scenă două
personaje, punctul şi linia, evident, entităţi pur ficţionale,
pentru că nimeni nu a văzut în universul empiric nici
puncte şi nici linii, aşa cum sunt ele descrise de Euclid, şi
aşa au mers lucrurile până în zilele noastre.
451

Solomon Marcus

Dar atunci unii întreabă: „Nu aveţi dreptate, deoarece,
dacă matematica ar lucra în ficţiune, cum mai explicaţi
eficienţa ei în ştiinţă şi în inginerie?” Răspunsul este
simplu: ca să-şi manifeste această eficienţă, matematica
are nevoie de un proces de mediere între ea şi realitate, iar
acest proces de mediere e dat tocmai de universurile de
ficţiune în care ea operează. Acesta este paradoxul fără de
care nu înţelegem ce se întâmplă cu matematica, faptul că
matematica are nevoie de un proces de mediere între ea şi
realitatea empirică şi această mediere e realizată de
universul de ficţiune.
Putem merge mai departe. Ce mai iau literatura şi
matematica de la vechile mituri? Iau, de pildă, ambiţia
aceasta, cum se exprima Gauss şi, pe urmele lui, Barbilian,
ambiţia de a exprima cât mai mult în cât mai puţin, această
optimizare semiotică. Şi asta realizează formula matematică.
Ea reuşeşte să condenseze într-o expresie de o mare
densitate o realitate pe care altfel, fie că n-am putea-o
exprima, fie că am exprima-o într-un mod atât de complicat,
încât toată funcţia ei euristică s-ar pierde. Metafora este şi
ea un bun comun al literaturii şi matematicii.
Matematica este un spectacol. Să înţelegem deci care
sunt personajele în acest spectacol, care sunt rolurile, care
sunt actorii; sunt axiome, postulate, definiţii, probleme (dar
probleme de cunoaştere, nu problemele astea care se dau
la olimpiade), teoreme, demonstraţii, corolare, aplicaţii. Iar
452

Rãni deschise

dumneavoastră începeţi, sper, să realizaţi, în raport cu tot
ceea ce v-am spus, cât de derizorie este oferta de care
beneficiază elevii noştri. Oferta se reduce la matematicaunealtă. Toate celelalte, în măsura în care sunt prezente,
sunt ascunse, implicite, ca, de pildă, matematica-mod de
gândire. În manualul şcolar, modul de gândire numit
matematică, nu zic că nu există acolo, dar e ascuns, e doar
implicit. Pentru ca elevul să ia contact cu el, e nevoie de o
acţiune de explicitare, care nu prea se face, pentru că modul
de examinare, de control al pregătirii unui elev şi de
evaluare a acestei pregătiri se reduce la matematica-unealtă
şi la exerciţii. (Precizăm iar: A nu se confunda exerciţiile
din manual cu ceea ce numim problemele de cunoaştere ale
matematicii) Singurul lucru cu care se alege, în mod
explicit, elevul este matematica în rol de unealtă de
rezolvare a unor exercitii, care de cele mai multe ori nu au
valoare de cunoaştere, ci sunt o gimnastică în sine.
Toate celelalte aspecte lipsesc, nu mai au o prezenţă
explicită; de pildă matematica-joc. Cât de important ar fi să
apară componenta ludică a matematicii, o componentă
esenţială... Dar nu prea apare. Nu mai e timp şi nu mai e loc
de asta. Noi, pe de o parte spunem, aşa cum o făcea Ciprian
Foiaş, că teorema este cărămida matematicii; da, în teorie, dar
n-am prea văzut să se controleze la examene, la concursuri
înţelegerea teoremelor matematice. Controlăm posibilitatea
de rezolvare a unor exerciţii şi poţi să fii maestru în exerciţii
453

Solomon Marcus

de derivare, fără să fi înţeles realmente ce este derivarea unei
funcţii. Deci acesta ar fi al doilea punct de plecare într-o
înţelegere culturală a matematicii, să porneşti de la toate
personajele ei. Iată un alt exemplu: axiomele în matematică.
Euclid nu folosea termenul acesta, dar la Euclid sunt ascunse
acele entităţi pe care, mai târziu, le vom numi „termeni
primitivi” şi care ulterior, la Hilbert, apar clar. Aceştia ar fi
termenii care nu se definesc. Euclid dă drept definiţii nişte
descrieri care nu corespund unui statut real de definiţie şi care
ascund termeni primitivi, termeni care nu se definesc.
Lucrul acesta are o importanţă extraordinară, pentru că
în civilizaţia greacă apare o problemă care a mers până în
zilele noastre: distincţia dintre cunoaşterea cu rest şi
cunoaşterea fără rest. Şi iată că vine Constantin Noica şi, pe
urmele lui, Horia-Roman Patapievici şi spun: deosebirea
esenţială dintre matematică şi filosofie stă în faptul că
matematica oferă o cunoaştere fără rest, în timp ce filosofia
este o cunoaştere cu rest. Deci filosofia oferă o cunoaştere
care nu se epuizează la niciun moment, în timp ce
matematica oferă o cunoaştere care, după un număr finit de
etape, se epuizează. Iluzie! Din păcate, trebuie să
recunoaştem că matematica şcolară corespunde portretului
făcut de Noica şi Patapievici, dar matematica adevărată nu
corespunde. Dacă o să vă duceţi la faimoasa listă de
probleme propusă de Hilbert, în cele mai multe dintre aceste
probleme, greutatea lor, ponderea lor principală se află
454

Rãni deschise

tocmai la originea axiomatică a lucrurilor, adică exact acolo
unde trebuie să ne întrebăm de ce aceste axiome, şi nu altele.
Din cauza asta, lumea matematică e şi acum împărţită în
ceea ce priveşte faimoasa problemă a continuului, care este
prima problemă de pe lista lui Hilbert, şi anume dacă putem
sau nu să intercalăm un cardinal transfinit între numărabil
şi continuu. Unii zic că problema e rezolvată de Gödel şi
Cohen. Cei care răspund aşa sunt cei care consideră că
matematica e o cunoaştere fără rest; dar cei care sunt
orientaţi spre preocupări de fundamentele matematicii, cum
ar fi Martin Davis şi Raymond Smullyan şi mulţi alţii, spun
că nu, problema nu e rezolvată. Prima problemă a lui
Hilbert, pe urmele problemei lui Cantor, era tocmai de a se
găsi o fundamentare axiomatică a matematicii, pe baza
căreia întrebarea aceasta să aibă un răspuns binar, da sau nu,
adică dacă există un cardinal transfinit intermediar. Această
preocupare este foarte veche, deoarece grecii consideră că
a existat o cunoaştere perfectă, fără rest, dar care cunoaştere
s-a pierdut ulterior, iar noi acum încercăm s-o recuperăm. A
se vedea cartea lui Aram Frenkian, apărută la Paris în 1940:
La perfection est-elle au début ou à la fin?
Dar pe urmă apare cealaltă mentalitate, după care,
dimpotrivă, perfecţiunea este o nălucă şi doar drumul spre
ea este o realitate. Şi drumul acesta îl cunoaştem bine, cu
toate dificultăţile din secolul al XX-lea, când s-a constatat
că formalismul hilbertian nu poate fi dus până la capăt.
455

Solomon Marcus

Există şi o altă modalitate, o altă abordare a
matematicii, şi anume să căutăm să înţelegem
matematica sub aspectul metabolismului ei cu celelalte
domenii. Adică să înţelegem bine interacţiunea
matematicii cu fizica, chimia, biologia, cu ştiinţele
economice, cu sociologia, cu arta, cu muzica ş.a.m.d.
Vedeţi că matematica oferă acest spectacol extraordinar,
în care niciuna dintre aceste interacţii cu diverse alte
domenii nu este trivială. Am să iau numai metabolismul
matematicii cu muzica. Este imens! Toată muzica
occidentală are la bază modelul pitagoreic. Pentru
Pitagora, muzica şi matematica erau surori şi nu puteau
fi înţelese una în absenţa celeilalte. Ce află despre asta
elevul din ziua de azi? Nu află nimic. Acea bază
pitagoreică a muzicii, care sub aspect matematic este la
nivelul matematicii de gimnaziu, n-o găseşte în niciun
manual; nici măcar în manualul de matematică al elevilor
de la şcolile de muzică. Ne batem joc de istorie! Istoria
ne arată un metabolism continuu al matematicii cu
celelalte domenii şi acest metabolism este ţinut ascuns
elevilor de astăzi. Ca elev, aveam în manualul de
matematică, la progresii şi logaritmi, legea lui Weber şi
Fechner, conform căreia senzaţia este logaritmul
excitaţiei. Când excitaţiile merg în progresie geometrică,
senzaţiile respective merg în progresie aritmetică. Elevii
de astăzi nu mai găsesc în manual acest fapt. Dar, între
456

Rãni deschise

timp, tabloul s-a îmbogăţit. Între timp am aflat că timpul
subiectiv este logaritmul timpului cronologic. Şi toate
aceste lucruri sunt ignorate.
Acesta este un mod extraordinar de a vizualiza
matematica, sub aspectul relaţiilor ei externe, sub aspectul
relaţiilor ei cu celelalte domenii. În asta constă
universalitatea matematicii, care este ascunsă. O etalăm
uneori, dar o etalăm în mod gratuit, pentru că nu şi arătăm,
în fapt, în ce constă ea.
Un alt mod de a înţelege matematica ar fi şi următorul:
e un mod oarecum mai practic, dar foarte semnificativ. Să
urmărim cum se poziţionează oamenii din diverse activităţi
faţă de matematică, ce spun despre matematică actorii, ce
spun chimiştii ş.a.m.d. În urmă cu 60 de ani, făceam, între
altele, şi seminar de matematică la Facultatea de Chimie
Industrială de la Politehnică şi într-o zi m-a chemat decanul,
un chimist de altfel remarcabil, Spacu, şi mi-a spus: „Vă
rog, nu-i mai chinuiţi pe studenţii noştri, deoarece chimiştii
nu au nevoie de atâta matematică. La drept vorbind, regula
de trei simplă le ajunge.” Deci, iată, sunt atâtea şi atâtea
moduri de a vedea matematica.
Dar vă atrag atenţia, niciunul din aceste moduri nu este
suficient. Toate sunt necesare, niciunul luat singur nu este
suficient, ele vin din direcţii dintre cele mai diferite şi de aceea
e evident că e greu să le epuizăm, de aceea apropierea noastră
de matematică rămâne în permanenţă doar o aproximare.
457

Solomon Marcus

N-am vorbit aici încă despre un capitol extraordinar:
matematica–mod de gândire. Este un univers fantastic aici,
pentru că de fapt nu e un singur mod, ci o întreagă
tipologie de tipuri de gândire matematică şi chiar se spune
că nu prea există un matematician care să exceleze şi în
gândire discretă, pur algebrică, şi în gândire probabilistă,
şi în gândire transfinită, şi în gândire geometrică. Există o
tipologie întreagă a matematicienilor, după cum ei
excelează în anumite tipuri de gândire şi mai puţin în alte
tipuri de gândire. Este o întreagă hartă tipologică a
matematicienilor, pe care am prezentat-o în unele texte ale
mele. Dar iată, s-a găsit cineva care să propună un numitor
comun al tuturor tipurilor de gândire matematică, pe care
o să vi-l prezint acum. Nu sunt convins că e într-adevăr un
numitor comun, dar, în orice caz, e probabil una din
trăsăturile cele mai caracteristice de recunoaştere a unei
gândiri matematice. Insist asupra acestui fapt, pentru că,
dacă prin dezvoltările ei tehnice, prin teoremele pe care le
propune, matematica poate să aibă doar o relevanţă în
anumite locuri, prin modul ei de gândire, matematica
devine relevantă pentru orice activitate umană. De aceea
matematica-mod de gândire ar trebui să fie protagonistul
în educaţia matematică generală. E vorba de următorul
aspect: matematica procedează în etape riguros şi explicit
ordonate, cu accentul şi pe riguros, şi pe explicit;
procedând astfel, se prevalează, în modul cel mai ordonat
458

Rãni deschise

posibil, de achiziţiile realizate în etapele anterioare şi aşa
se poate explica elevului de ce, cum şi unde începem să
avem nevoie în matematică de simboluri, expresii din afara
limbajului natural; că nu mai putem face faţă situaţiilor
numai prin cuvinte, că limbajul natural se dovedeşte
insuficient. Iar experimentul e foarte simplu. Fie un binom:
(a+b); puterea a doua o putem exprima uşor în cuvinte,
dar parcă e mai simplu în simboluri. Exprimarea în cuvinte
a cubului unui binom se dovedeşte sensibil mai lungă şi
mai lipsită de expresivitate decât aceea prin simboluri. Pe
măsură ce exponentul creşte, evident, cuvintele devin din
ce în ce mai neputincioase, îşi pierd funcţia euristică.
De aceea avem nevoie de simboluri. Trebuie să
împachetăm achiziţiile etapelor anterioare, fie ele
concepte, teoreme sau alte entităţi, într-un mod cât mai
economic, de unde nevoia unui simbolism adecvat. Este
exact ce facem într-o gospodărie. Grupăm lucrurile după
tipul lor: aici e biblioteca, aici sunt cărţile de un tip, aici
de alt tip; şi la fel se procedează şi în bucătărie.
Numai că diferenţa dintre gândirea matematică şi alte
gândiri este că în matematică lucrurile au un caracter
explicit, care nu se mai regăseşte în alte părţi. Poate că se
regăseşte parţial în domeniul juridic, în care trebuie mereu
să trimiţi la articolele anterioare. E foarte interesant de
urmărit această paralelă între logica matematică şi logica
juridică. Există problema aceasta a logicii juridice în relaţie
459

Solomon Marcus

cu logica deontică. Domeniul juridic se apropie cel mai
mult, din acest punct de vedere, de gândirea matematică.
Din păcate, nu se apropie atât de mult pe cât ar fi de dorit.
Vă las pe dumneavoastră să apreciaţi legitimitatea acestei
idei, conform careia procedarea în etape riguros ordonate
ar fi principala trăsătură a gândirii matematice. În orice
caz, ea este esenţială, dar trebuie educată.
Noi nu facem lucrul acesta, nu explicităm niciodată
lucrurile. Adică, să-l aducem pe elev în situaţia în care el
să realizeze felul în care poate beneficia de acest mod de
gândire, nu doar în matematică, ci în atâtea alte domenii
în care e nevoie să procedăm gospodăreşte. Adică putem
vedea că gândirea matematică e o gândire în felul probei
de maraton în atletism. Este o gândire cu bătaie lungă, care
trimite mereu la trecutul îndepărtat. O pagină de
matematică nu trimite doar la pagina imediat anterioară,
trimite la toate paginile anterioare, şi de asta se vorbeşte
mereu de structura anaforică a limbajului matematic. În
lingvistică e o noţiune esenţială. Dar, în acelaşi timp,
trimite şi înainte, are o structură cataforică.
De aceea se dă acest exemplu: care este deosebirea
esenţială dintre un manual de geografie şi unul de
matematică? Într-un manual de geografie, dacă ai rupt
primele zece pagini, nu este atinsă prea mult înţelegerea
paginilor rămase. Într-un manual de matematică,
suprimarea primelor zece pagini poate să fie catastrofală;
460

Rãni deschise

se anulează practic tot ce urmează. Dar nu e numai asta.
Cât de importante sunt gândirea analogică, gândirea
metaforică în matematică! După cum spunea Banach
cândva, fiind întrebat ce este un matematician. „Este un
om capabil să facă analogii, dar analogii la toate nivelurile:
analogii între idei, între teoreme şi analogii între analogii.”
Şi metafora joacă un rol esenţial în matematică. Elevii
însă nu află nimic despre lucrurile astea, despre gândirea
recursivă etc.
Iată deci cum arată câteva dintre feţele matematicii.
Luând cunoştinţă de ele, realizăm şi cât de esenţială este
relaţia matematicii cu întreaga cunoaştere umană. Adică
realizăm imediat cât de mult ia matematica din afară şi cât
de mult restituie ea lumii. Dacă elevul nu înţelege acest
metabolism, nu se alege cu mare lucru din educaţia
matematică.
Aici trebuie să facem o deosebire esenţială, care din
păcate nu se prea face. Sunt două lucruri fundamental
diferite. Cercetarea matematică poate să fie un domeniu în
care un tânăr să exceleze, fără să aibă o viziune asupra
întregului tablou la care m-am referit. El poate avea
anumite calităţi intelectuale în domeniul deductiv, în
domeniul gândirii abstracte, în materie de analiză
combinatorie, poate deci să exceleze într-un anumit
compartiment al matematicii, concepută în primul rând ca
activitate sintactică. Majoritatea cercetătorilor sunt de acest
461

Solomon Marcus

fel. Adevărul este însă că marii matematicieni nu sunt doar
atât. Marii matematicieni sunt cei care se află în posesia
unei bune părţi a tabloului pe care l-am prezentat aici. Dar,
pentru educaţia matematică a masei de elevi, poate că
trebuie să ne orientăm după ceea ce un laureat al medaliei
Fields în matematică, William Thurston, propunea. El
spunea următorul lucru: în cercetare, trebuie să găseşti
teoreme noi. Dar, pentru o însuşire a ceea ce reprezintă
matematica în cultură, ar trebui să adoptăm un alt scenariu,
în care accentul să cadă pe probleme, motivaţii, istorie,
legătura cu alte domenii.
Adică trebuie să înţelegem de unde vin problemele
matematicii, să înţelegem metabolismul ei cu restul lumii,
motivaţiile care conduc la introducerea anumitor concepte,
şi nu a altora, semnificaţiile celor mai importante teoreme,
să înţelegem ce se întâmplă la interfaţa ei cu lumea, cu
istoria, să ne fie clar contextul cultural în care apar marile
achiziţii matematice. Nu ca acum, când, în manualul şcolar,
lucruri care au reprezentat momente de cotitură în istorie,
cum ar fi apariţia crizei numerelor raţionale la Pitagora,
intuirea iraţionalelor sau toate achiziţiile din geometrie,
sunt lipsite de comentariul care să degajeze semnificaţiile
necesare. Traseul acesta de la gratuitate la utilitate, care a
condus de la Apollonius (secţiunile conice) la Kepler
(traiectoriile planetelor) trebuie subliniat. Un joc al lui
Apollonius şi-a arătat eficienţa, după 2000 de ani, la Kepler.
462

Rãni deschise

Momentele artistice ale matematicii presupun acte de
contemplaţie. Dacă noi reducem educaţia matematică la
aspectul ei strict operaţional: cum să faci, cum să
procedezi, atunci golim matematica de orice sursă de
satisfacţie spirituală. Teorema lui Pitagora e învăţată ca o
chestie pur tehnică. Or, teorema lui Pitagora cere şi un
moment de contemplare. Să te opreşti, să priveşti şi să
compari; să realizezi distanţa dintre momentul de ghicire
şi momentul de confirmare prin raţionament. Nu se poate
face o educaţie matematică sănătoasă în absenţa celebrei
reflecţii a lui Blaise Pascal: „Căutăm ceea ce deja am
găsit.” Adică, de ce a căutat Pitagora o demonstraţie a
teoremei care îi poartă numele? Doar lucrul fusese deja
găsit încă de babilonieni. Şi Thales la fel, cu teorema sa.
Nu cum am citit acum, într-o revistă de matematică de la
noi, în care se întreabă cineva cum de i-a trecut prin cap
lui Thales teorema care-i poartă numele. Nu lui i-a trecut
prin cap. Intuiţia ei a existat în etapa anterioară, empirică,
observaţională a matematicii. Lucrul acesta elevii nu-l
învaţă deloc. Ei nu învaţă să sesizeze distanţa dintre
ghicire, intuiţie, observaţie, pe de o parte, şi confirmare
prin raţionament deductiv, pe de altă parte. Nu li se
vorbeşte deloc despre astfel de lucruri. Reducem
matematica la o simplă activitate deductivă de verificare
a unor enunţuri găsite de nu se ştie cine. Nu educăm deloc
la elevi capacitatea interogativă. E un decalaj extraordinar
463

Solomon Marcus

între cât de exigenţi suntem cu elevii pentru ca ei să fie
capabili să dea răspunsuri şi cât de neglijenţi suntem în
a-i educa să inventeze ei întrebări interesante, să
desluşească întrebările care se ascund într-un răspuns, de
pildă; nu doar să fie preocupaţi de răspunsurile la întrebări
care nu sunt ale lor, ceea ce duce, de cele mai multe ori, la
divorţul dintre elev şi oferta matematică pe care o are în
faţă şi, de aici, la abandonul şcolar. De aici fuga de
matematică pe care o găsim exprimată la atât de mulţi
oameni de cultură şi care a devenit proverbială –
matematica pe post de sperietoare.

464

