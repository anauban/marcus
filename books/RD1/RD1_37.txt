Solomon Marcus

Un bacalaureat sub auspicii sumbre
Tribuna învăţământului, nr. 1005,
29 iunie 2009

Ne aflăm în faţa unui nou bacalaureat. Despre cel
anterior am scris în urmă cu un an. Urmărind cu atenţie
derapajele de la tezele cu subiect unic şi de la bacalaureatul
din ultimii ani, manualele şcolare şi întregul peisaj al
învăţământului, identificăm o bună parte din păcatele grave
ale sistemului nostru educaţional. Se speculează mult pe
temele educaţiei, nu ducem lipsă de teorii şi proiecte dintre
cele mai elaborate, dar poate că este mai practic să
identificăm fenomenele clar nocive şi să le înlăturăm sau
măcar să le reducem proporţiile. La astfel de situaţii
punctuale ne vom referi în cele ce urmează. Regretabil,
viaţa şcolară le oferă din plin. Să mergem pe urmele lor.
Mai întâi, teza cu subiect unic la istorie. Mare
scandal, soldat cu demisii şi sancţiuni, în urma confuziei
dintre Armata I şi Armata II. Desigur, orice subiect propus
candidaţilor trebuie să ajungă la aceştia exact în forma
preconizată. Dar confuzia dintre I şi II a apărut parcă
252

Rãni deschise

anume pentru a sublinia modul cu totul nepotrivit de a
înţelege predarea istoriei în şcoală. Ne întâlnim cu vechea
boală, a încărcării memoriei elevilor cu detalii
nesemnificative, în dauna celor interesante. Ce interes mai
poate prezenta azi diferenţierea dintre I şi II în ceea ce
priveşte participarea Armatei Române la luptele de la
Mărăşti şi Mărăşeşti, în Primul Război Mondial?
Menţionarea numelor generalilor români care s-au
evidenţiat acolo este un semn de recunoştinţă faţă de cei
care au contribuit la crearea României Mari. Dar detaliul
privind numerotarea celor două armate nu ne îmbogăţeşte
cu nicio semnificaţie. Faptul că tocmai la acest detaliu s-a
produs confuzia care a beneficiat de o atenţie nemeritată
şi a produs atâtea victime vine să sublinieze, prin contrast
şi prin umor involuntar, dezorientarea în care se află cei
care ar trebui să vegheze la starea educaţiei în România.
Încă le vine greu să înţeleagă multora dintre cei care
decid ce anume trebuie să înveţe şcolarii că nu în aglomerarea
de tot felul de date şi informaţii constă adevărata învăţătură,
ci în explicarea unor fapte semnificative, cu valoare culturală.
Episodul la care ne-am referit mai sus a fost precedat de un
altul, poate şi mai instructiv: povestea tezei cu subiect unic la
Limba şi Literatura Română, la clasa a VIII-a. În atenţia
elevilor s-a aflat fragmentul final din Triumful talentului de
I. L. Caragiale. În două locuri, textul acestuia a fost reprodus
greşit: numele „Niţă Ghiţescu” a fost înlocuit cu „Ghiţă
253

Solomon Marcus

Niţescu”. Primele cinci întrebări se refereau la aspecte
gramaticale şi, în această privinţă, cele două greşeli nu aveau
cum să-şi facă efectul. Dar şi aici observăm vechi carenţe:
obiceiul de a se studia gramatica mai degrabă pe texte literare
decât pe texte ale comunicării cotidiene; tipuri de exerciţii cu
care şcolarii nu prea se vor întâlni în viaţă, în dauna altora,
care s-ar asocia direct cu modul în care ei comunică.
Dar să trecem la partea literară a întrebărilor. Mai
întâi, trebuie să observăm că fragmentul adus în atenţia
elevilor nu poate fi „decodat” decât prin raportare la partea
omisă din Triumful talentului. Un moment esenţial este cel
al convenţiei pe care Niţă şi Ghiţă o stabilesc: fiecare
dintre ei va semna cu numele celuilalt. Ghiţă va fi cel
avantajat prin această substituire de persoană, obişnuită în
schimbul unei sume de bani. Dar fragmentul propus a fi
comentat nu include acest pasaj, fapt care reduce
considerabil inteligibilitatea sa.
Întregul text are o structură geometrică, de simetrii şi
antisimetrii, în care situaţiile celor două personaje fac pereche
cu numele lor, parcă predestinate. Însă modul de formulare a
întrebărilor, în termeni deveniţi limbă de lemn
(„caracterizarea personajului” şi „mesajul”), nu dă deloc
impresia că s-a avut în vedere această dependenţă esenţială a
fragmentului considerat de partea anterioară lui. Dar chiar
privit independent, fragmentul în cauză este supus unor
întrebări nefericite, în care abundă stângăcii şi inadvertenţe
254

Rãni deschise

logice şi lingvistice; ele au fost identificate cu precizie de
Ştefan Cazimir (România literară, nr. 22, 5 iunie 2009, p. 3).
Nu mi-e clar dacă elevii au studiat la clasă, au avut în manual
întregul text al lui Caragiale, deci dacă s-a adoptat sau nu ca
presupoziţie cunoaşterea întregului text. Dacă răspunsul este
negativ (aşa cum ar rezulta din precizarea faptului că textul
trebuia comentat „la prima vedere”), atunci subiectul nu-şi
justifică prezenţa la o teză atât de importantă. În cazul
afirmativ, întrebările ar fi trebuit să fie cu totul altele, pentru
a-i provoca pe elevi să arate în ce fel numele celor două
personaje reflectă situaţiile în care ele se află. Actualitatea
schiţei lui Caragiale este multiplă: batjocorirea proprietăţii
intelectuale, transferul de identitate, inversarea valorilor,
corupţia manifestată explicit, prin trafic de influenţă (a se
vedea scena cu directorul care scoate din buzunar un plic şi
din el o scrisorică) şi toate încheiate într-o armonie generală.
Dar reacţia aparent nefirească a lui Niţă şi Ghiţă la anunţarea
rezultatului concursului îl va mira pe acel cititor al
fragmentului final care ignoră partea anterioară a schiţei,
cititor la care s-ar putea naşte bănuiala unei înţelegeri
prealabile între cei doi; aceasta însă în ipoteza că textul ar fi
fost reprodus corect. Cele două greşeli schimbă complet
situaţia şi intrăm într-o reţea de posibilităţi psihologice şi
logice care, poate, pretind o analiză ce depăşeşte nivelul clasei
a opta. De aceea, este greu să fim de acord cu afirmaţia
conform căreia „greşeala nu ar fi trebuit să-i încurce pe elevi”.
255

Solomon Marcus

Să rezumăm. Cele două teze cu subiect unic devenite
cunoscute întregii ţări, după ce au avut privilegiul unei
mediatizări de invidiat, ca urmare a unor greşeli în
comunicarea către elevi a textelor respective, au scos la
iveală infracţiuni incomparabil mai grave decât greşelile
la care toată lumea s-a referit. Putem spune că au fost
greşeli bine inspirate, au îndeplinit o funcţie retorică de a
atrage atenţia asupra insuficienţelor de fond ale activităţii
şcolare, asupra lipsei de orizont istoric şi cultural. Se spune
că într-o picătură se reflectă lumea. În episodul celor două
teze se regăsesc bolile grave ale învăţământului, pe toată
întinderea sa: programe nefericite, manuale aride,
incapabile de a-i contamina pe elevi de bucuria de a
înţelege lumea şi de a se înţelege pe ei, prea multă
incompetenţă şi incultură în rândul celor care au datoria
îndrumării aducerii la cultură a noilor generaţii.
Voi încheia cu semnalarea unui fapt petrecut la un
post de televiziune, ieri, 17 iunie 2009. Se discuta despre
educaţie. Cineva a afirmat că mulţi profesori de
matematică nu ştiu să demonstreze teorema lui Pitagora.
Imediat, afirmaţia a fost reluată repetat pe ecran, dar cu o
modificare simptomatică pentru gradul de cultură al celui
care a efectuat transferul, deci era complice la respectiva
emisiune: „Profesorii de matematică nu ştiu să arate
teorema lui Pitagora”.

256

