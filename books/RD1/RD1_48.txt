Solomon Marcus

Umblând prin ţară
Tribuna învăţământului,
7-13 iunie 2010, p. 10
Calendarul meu a inclus în ultimele patru săptămâni
câteva acţiuni a căror semnificaţie merită, cred, să fie
semnalată.
Mai întâi, am participat, la Iaşi, la faza finală a
Concursului „Traian Lalescu” pentru studenţi. Am
remarcat din nou tendinţa de a se propune o cantitate mare
de probleme, care cereau calcule laborioase. Să fie o
întâmplare faptul că niciun candidat nu a abordat toate cele
patru chestiuni propuse? Am mai observat că limbajul
matematic practicat de candidaţi este foarte zgârcit în
cuvinte, componenta artificială a acestui limbaj este
atotputernică, deşi, într-o educaţie matematică sănătoasă,
normală, ar trebui să existe un echilibru între folosirea
cuvintelor şi folosirea simbolurilor matematice.
Demonstraţiile au nevoie de cuvinte ca de aer, dar tocmai
aici au eşuat chiar cele mai bune dintre lucrări.
Candidaţilor li s-a cerut explicit să justifice toţi paşii pe
care-i fac, folosirea unei teoreme impune verificarea
328

Rãni deschise

îndeplinirii condiţiilor de aplicare a teoremei respective;
dar acest imperativ nu prea a fost urmat.
Concomitent cu Concursul „Traian Lalescu”, s-a
desfăşurat la Iaşi o „sesiune de comunicări”. De fapt, nu a
fost nicio comunicare, au fost câteva referate şi eseuri. Am
constatat din nou (am tot scris pe această temă) că nici
elevii, nici studenţii noştri nu află în ce constă proprietatea
intelectuală; educaţia ei nu se prea face. Prezinţi o
comunicare ştiinţifică atunci când crezi că ai ceva de spus,
că, într-o anumită problemă de cercetare, ai ceva de
adăugat, faţă de ceea ce se ştia.
Aceasta presupune o cunoaştere măcar parţială a ceea
ce s-a publicat în domeniul respectiv, iar tu, ca autor al
comunicării, eşti obligat să te delimitezi de cei de la care
preiei ştafeta, indicându-i cu claritate, împreună cu locul
şi data la care ei şi-au publicat rezultatele, specificând care
este rezultatul lor şi ce pretinzi tu că adaugi. Această
informaţie trebuie să fie furnizată chiar la începutul
comunicării, pentru ca cititorul (auditoriul) să se poată
lămuri uşor şi rapid. Niciuna dintre „comunicări” nu
îndeplinea această condiţie. Toate se mulţumeau să
colecteze informaţii din surse nu întotdeauna clar
specificate, iar rarele pretenţii de originalitate nu erau
acoperite prin argumente.
Câteva zile mai târziu, am participat, la Bucureşti, la
festivitatea de premiere a Concursului Internaţional de
329

Solomon Marcus

Matematică Aplicată Cangurul – 2010, sub auspiciile
MECTS, Fundaţiei pentru Integrare Europeană Sigma şi
Institutului pentru Dezvoltarea Evaluării în Educaţie.
Scopul acestui Concurs: să stimuleze iniţiativa unor
probleme cât mai frumoase, mai legate de viaţă. Iată un
exemplu de problemă pentru clasa a treia: «În labirintul
din figură, se află o pisică şi un şoricel. Pisica vrea să
ajungă la bolul cu lapte, iar şoricelul la bucata de caşcaval,
dar ei nu trebuie să se întâlnească niciodată. Care este
partea de labirint acoperită?» Nu putem reproduce aici
figurile la care se face trimitere, dar e greu să nu fii de
acord că o atare problemă poate să facă plăcere unui copil
de clasa a treia sau a patra; în plus, umorul care însoţeşte
textul ne aminteşte că gluma, râsul, buna dispoziţie sunt
un ingredient necesar al actului educaţional; nu cu priviri
încruntate, ci cu atitudini senine, deschise umorului,
câştigăm interesul elevilor. Cum îl câştigăm pe elev de
partea noastră? Să-i propunem lucruri care au o valoare de
cunoaştere, dar lucruri pe care el să le înţeleagă, să facem
ca ele să-l şi intereseze, apoi, pe această bază, să reuşim
să-i trezim şi plăcerea de a le face. Acesta este pariul
educaţiei, iar situaţii de tipul «uneori trebuie să înveţi şi ce
nu-ţi place », care sunt invocate din când în când, ar fi de
dorit să fie tot mai rare. Un moment neplăcut la Concursul
Cangurul – 2010 a fost acela în care s-a constatat că elevul
care primise premiul «Henri Poincaré», acordat de
330

Rãni deschise

Ambasada Franţei la Bucureşti, nu ştia cine este cel care
dă numele premiului.
O zi mai târziu, am participat, la Bucureşti, la
festivitatea de acordare a premiilor la Concursul Arhimede.
Aici, ca şi la festivitatea de deschidere a Olimpiadei
Naţionale de Matematică pentru clasele V-VI, desfăşurate
ieri, 28 mai, la Călăraşi, am beneficiat de avantajul
prezenţei în sală a celor trei tipuri de actori principali ai
actului educaţional: elevi, părinţi, profesori. La Bucureşti,
am putut dialoga direct cu mulţi elevi din clasele mici,
confirmându-mi-se precaritatea dialogului lor cu părinţii
şi bunicii. La Călăraşi, am insistat asupra diferenţei dintre
întrecerile sportive şi cele de matematică, fizică, chimie,
literatură română, istorie etc. Le-am spus că echipa de
fotbal a Călăraşiului, aflată acum în Liga a treia, nu poate
reuşi calificarea în Liga a doua decât în dauna unei echipe
din Liga a doua, care va fi retrogradată în Liga a treia. În
fotbal, ca şi în tenis, handbal etc., pentru ca o echipă să
râdă, să triumfe, trebuie ca o alta să-şi plângă eşecul.
Câştigi pe seama altuia, care pierde. Nu aceasta este
situaţia la Olimpiadele de Matematică sau de alte
discipline şcolare. Aici, reuşita unui candidat nu este
condiţionată de eşecul altuia. Toţi pot câştiga, acesta este
idealul. Deci analogia cu jocurile sportive funcţionează
numai până la un punct. Problemele de olimpiadă sunt
exerciţii de gimnastică mintală, la fel de utile ca şi cele ale
331

Solomon Marcus

trupului. Dar nu mai mult. Ar fi greşit să facem din ele
scopul principal al educaţiei şcolare. Chiar folosirea
cuvântului «întrecere» este primejdioasă aici, deoarece
pune accentul pe ideea de ierarhie inevitabilă. Obsesia lui
«cel mai», «cea mai» aduce multe prejudicii educaţiei. Va
trebui lucrat mult pentru schimbarea acestei mentalităţi.
Viaţa ne pune altfel de probleme, la care de cele mai
multe ori nu te poţi descurca pe baza folosirii directe a unor
cunoştinţe din manualul şcolar. Este nevoie de imaginaţie,
de iniţiativă. Să arăţi cu ce ai rămas după ce ai uitat multe
detalii, cum pui în legătură lucruri învăţate în locuri
diferite. De acestea ar trebui să ne amintim în mod special
în acest zile, când atenţia publică se concentrează asupra
bacalaureatului. Se va da? Nu se va da? Ne cuprind fiorii
când ne amintim de eşecul lamentabil al bacalaureatului
din anii anteriori. Un imens aparat se află implicat într-o
acţiune care riscă să fie cu o redusă relevanţă educaţională.
L-am discutat în articole publicate în Tribuna
învăţământului, la vremea respectivă. Dar mă tem că încă
nu a venit rândul acestor probleme esenţiale, suntem încă
prizonieri ai formelor fără fond.

332

