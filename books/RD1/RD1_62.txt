Solomon Marcus

Conferinţă
la Colegiul Naţional „I. L. Caragiale”
Bucureşti, 5 mai 2009

Prof. Dorothea Vieru: Stimaţi colegi, avem o bucurie
deosebită pentru ziua de astăzi, când ştim că în ultimele zile
au fost multe turbioane în viaţa noastră de şcoală, să-l avem
în mijlocul nostru pe domnul academician Solomon
Marcus, care este un eminent profesor de matematică al
şcolii româneşti de matematică şi, de asemenea, o
personalitate care iubeşte foarte mult dialogul dintre
discipline.
Este chiar sintagma domniei sale, pe care am
întâlnit-o într-una din intervenţiile în forul academic, unde
D-sa este membru titular şi un foarte important reprezentant
al ştiinţei şi al culturii româneşti, pe care îl invităm să ne
împărtăşească din gândurile sale de elev, de student, apoi,
la rândul său profesor şi, de asemenea, îndrumător de
gânduri ştiinţifice. Nu mă refer neapărat la îndrumătorul
ştiinţific în sensul strict al termenului, pentru că ştiţi că
D-sa a reuşit să pună în valoare discipline noi precum
406

Rãni deschise

lingvistica matematică, să le abordeze la momentul anilor
,
60, când erau în vogă teoria limbajelor şi structuralismul
lingvistic. Această problemă interesează două discipline
care nu sunt deloc diametral opuse, sunt chiar
complementare, şi cunoaştem lucrul acesta şi în domeniul
literaturii. Probabil că o să fie şi o astfel de întrebare şi vă
invit să-i puneţi întrebări atât despre domnia sa, cât şi
despre poetul Ion Barbu, care a fost un eminent
matematician, cu un destin aparte, din motive strict politice.
Din acest motiv, domnia sa ne poate da foarte multe
date, pentru că este un om care iubeşte foarte mult
literatura, însă puteţi să îi adresaţi întrebări din toate
domeniile. Până la urmă, este important faptul că viaţa
însăşi ne oferă diferite modalităţi de abordare prin marile
personalităţi şi, mai ales, prin marile modele.
Pentru că noi avem aici de faţă, şi cu asta am să
închei, un model care este de dorit să vă stea în faţă şi în
ochii minţii, nu doar în cei ai trupului. Pentru că false
modele sunt în jurul vostru pe toate drumurile.
Încercaţi să vă faceţi o selecţie personală, aşa cum
optaţi pentru o anumită facultate şi o anumită carieră, şi să
vă căutaţi oameni pe care aţi vrea să-i urmaţi în ceea ce au
făcut ei, pentru că există o certitudine: ceea ce ei vă pot
arăta. Şi, mai ales, păstraţi privirea deschisă pentru cultură,
pentru ştiinţă. Domnul academician aici de faţă este un
astfel de exemplu.
407

Solomon Marcus

Intimidat de numele acestui lăcaş
Solomon Marcus: Mulţumesc pentru această invitaţie,
care în acelaşi timp mă bucură şi mă intimidează. La
intrare am văzut pariurile acestui lăcaş de învăţământ,
afilierea la UNESCO, la Comunitatea Europeană, la
Germania ş.a.m.d. La fel sunt intimidat de numele acestui
lăcaş. Numele Caragiale şi numele care l-a precedat,
Maiorescu.
Nu ştiu dacă aţi reflectat asupra acestor simboluri
sau poate ele au devenit o rutină şi nu le mai acordaţi
atenţie. Dar a înţelege actualitatea lui Maiorescu, a
înţelege actualitatea lui Caragiale, a înţelege importanţa
înscrierii în cultura planetară constituie o condiţie pentru
reuşita profesională şi pentru reuşita în viaţă.
Maiorescu ne atrăgea atenţia în secolul al XIX-lea că
trebuie să fim atenţi la mesajele care ne vin din acea
Europă care a avut o şansă mai bună a istoriei şi să nu
rămânem prizonieri ai trecutului. Şi tot Maiorescu ne
atrăgea atenţia şi lansa acel slogan care nu şi-a pierdut
niciun moment actualitatea, al formelor fără fond.
Iar, în privinţa lui Caragiale, vedeţi că vrând-nevrând
ne simţim tot timpul obligaţi să aducem în atenţie diverse
replici ale personajelor lui, şi chiar sunt acolo unele lucruri
care ţin de matematică, asupra cărora poate am să mă
opresc mai târziu.
408

Rãni deschise

Dar trebuie să vă mărturisesc că primul gând, când
am intrat în această sală şi v-am privit, a fost departe în
trecut, la momentul în care eram de vârsta voastră, cândva
la sfârşitul celui de-al Doilea Război Mondial. Făcusem în
şcoală secţia clasică (aşa se chema).
Alegerea mea ca elev: secţia clasică
Clasic înseamnă accentul pe latină, pe greacă, pe
literatură etc. Şi aceasta a fost alegerea mea ca elev, pentru
că devenisem pasionat de literatură şi de filosofie încă din
anii adolescenţei. Abia la vârsta bacalaureatului am
descoperit matematica. A fost o carte despre geometrii
neeuclidiene, dar nu era o carte de şcoală!
Vedeţi, aici e drama matematicii: că mereu, şi atunci
şi azi, matematica de care te poţi îndrăgosti trebuie căutată
în primul rând în afara şcolii! Şi acesta a fost şi unul din
motivele pentru care mi-am dedicat în bună măsură viaţa
îmbunătăţirii educaţiei matematice.
Am ales matematica şi în această situaţie am intrat în
conflict cu tatăl meu, care ştia una şi bună: dacă vrei să reuşeşti
în viaţă, trebuie să mergi la Politehnică sau la Medicină.
Şi alegerea mea a fost matematica! Trebuie să
înţelegeţi că în acel moment, în 1944 – 1945, matematica
nu îţi oferea alt orizont decât acela al profesoratului într-o
şcoală. Cred că nici nu exista încă în România, şi încă prea
409

Solomon Marcus

puţin în lume, profesia de cercetător în matematică.
Această profesie s-a cristalizat abia în a doua jumătate a
secolului trecut şi am făcut o alegere care era considerată
de mai toţi din jurul meu o alegere păguboasă.
Aveam o singură motivaţie pentru această alegere, şi
anume faptul că am descoperit că matematica este la fel
de profundă ca şi literatura, dar că ea nu poate fi învăţată
decât în mod sistematic, în timp ce literatura, evident,
poate fi frecventată într-un mod mai liber, ca şi filosofia.
Şansa unor profesori extraordinari
Am avut şi marea şansă a unor profesori
extraordinari, încă din anul întâi de facultate, şi încet-încet
mi-am dat seama că lucrul cel mai important – ceea ce vă
spun acum este judecata care mi s-a cristalizat mult mai
târziu –, că esenţial în alegerea unui drum nu este
oportunitatea pe care acel drum ţi-o oferă la momentul
respectiv, ci cât de mult suflet, cât de multă energie eşti
capabil să investeşti în respectivul drum.
Deci trebuie să alegi ceea ce îţi place, ceea ce te
pasionează, indiferent de situaţia de moment a acelei
alegeri, pentru că acum, mai mult chiar decât la mijlocul
secolului trecut, profesiile sunt într-o continuă prefacere,
unele din ele dispar, apar profesii noi, chiar cele cu numele
păstrat capătă o faţă complet diferită.
410

Rãni deschise

De pildă, ceea ce înseamnă azi arhitectură are prea
puţină legătură cu ceea ce însemna ea în urmă cu 50 de
ani. Singurul element constant, singurul element invariant
care ne poate ajuta, ne poate hrăni independent de modul
în care evoluează lucrurile este felul în care dobândim
anumite moduri de gândire, anumite moduri de a înţelege
lumea şi de a ne înţelege pe noi.
Am asistat la naşterea informaticii
Ca să dau un alt exemplu: eu am asistat la naşterea
informaticii. Calculatoarele electronice au apărut la câţiva
ani după ce am devenit student la matematică, deci prin
,
48. Şi pe urmă au venit tot felul de generaţii noi de
limbaje de programare, mereu apărea alt limbaj care-l
marginaliza pe cel anterior. Cel care se mulţumea să
înveţe un anumit limbaj de programare şi nu înţelegea
întreaga logică ascunsă în spatele acestor limbaje se găsea
complet dezarmat de îndată ce se schimba limbajul pe
care el l-a învăţat.
De aceea, esenţial este să înveţi să gândeşti,
deci important e modul în care înţelegi lucrurile, şi nu
faptele individuale ca atare. Într-adevăr, se repetă
mereu acest lucru, dar de fapt, din păcate, manualele
şcolare şi programele sunt în mare măsură prizoniere,
totuşi, nu ale deprinderii unor moduri de gândire,
411

Solomon Marcus

ci ale memorizării unor date, unor sloganuri, unor
citate ş.a.m.d.
Ca să vă dau un exemplu în legătură cu matematica:
în ce constă modul matematic de gândire? Trebuie să vă
spun că numai pe baza a ceea ce se găseşte într-un manual
şcolar nu este uşor să-ţi dai seama.
În ce constă modul matematic de gândire?
Vorbim despre modul matematic de gândire, dar nu
există un singur mod. Există gândire probabilistă, gândire
combinatorială, gândire analogică, gândire deductivă,
inductivă, infinită; fiecare este o variantă de gândire
matematică. Dacă vreţi să înţelegeţi unde e secretul
universalităţii matematicii, atunci el e tocmai în acest mod de
gândire, nu în teoremele individuale pe care ea le produce, în
anumite noţiuni. Sigur, unele dintre ele sunt foarte importante,
însă importanţa ei primordială constă în faptul că te învaţă să
gândeşti în etape succesive riguros ordonate şi în care fiecare
etapă se bazează explicit pe etapele anterioare.
Adică te învaţă să fii un gospodar adevărat al învăţării
şi al înţelegerii, să împachetezi la fiecare etapă cu grijă
ceea ce ai acumulat în etapele anterioare şi apoi să
descoperi nevoia de a introduce diverse simboluri, acea
componentă artificială, şi în felul acesta matematica devine
esenţială în toate domeniile.
412

Rãni deschise

Iată deci că universalitatea ei nu este o vorbă goală;
dacă nu o înţelegem, nu ne alegem cu mare lucru.
Bacalaureatul de altădată
De pildă, dumneavoastră vă apropiaţi de momentul
bacalaureatului şi am să vă spun ce a însemnat
bacalaureatul în momentul în care l-am dat eu şi ce
înseamnă el într-o serie de ţări avansate, în Franţa de pildă,
ce înseamnă el sub altă denumire în Statele Unite ale
Americii. Bacalaureatul nu mai înseamnă să verifici din
nou dacă mai ţii minte anumite nume de scriitori, anumite
nume de opere, anumite date istorice; înseamnă să fii
capabil să pui în legătură ceea ce ai învăţat în şcoală în
locuri diferite.
De pildă, îmi aduc aminte că mie mi s-a dat la
bacalaureat, la proba orală, la franceză (şi aveam timp de
gândire vreo jumătate de oră), să aduc argumente pro sau
contra la ideea că poetul francez Charles Baudelaire este
parnasian. Nu ştiu dacă lucrurile astea se mai învaţă astăzi,
dar eu nu aveam în manual aşa ceva. În manual îl aveam
undeva pe Baudelaire, în altă parte aveam ceva despre
parnasianism, dar legătura dintre ele trebuia să o fac eu la
acel examen. Şi cam aşa arătau toate probele.
Tot pe vremea mea, numărul profesorilor de liceu era,
cred, mult mai mic decât numărul profesorilor universitari
413

Solomon Marcus

de astăzi. Erau puţine licee, câte două, trei în fiecare judeţ
şi a fi profesor de liceu confirmat era un lucru mare. Mulţi
dintre profesorii de liceu erau oameni foarte valoroşi.
Lovinescu de pildă, era profesor de liceu.
Un proces de auto-clarificare
Acum, noi ne-am îndreptat spre un învăţământ de
masă şi dumneavoastră trebuie să vă clarificaţi bine,
trebuie să vă examinaţi şi să vă decideţi:
– dacă în orizontul dumneavoastră intelectual,
cultural, există un interes autentic pentru cultură,
pentru cercetare, deci un interes să mergeţi să
faceţi o carieră corespunzătoare la universitate şi
pe urmă să treceţi prin toate etapele, să ajungeţi
până la doctorat,
– sau cumva nu există în orizontul dumneavoastră
de înţelegere, de cuprindere, această ambiţie şi nu
vă interesează decât o simplă calificare pentru o
profesie obişnuită.
Mi se pare foarte important să gândiţi serios asupra
acestui lucru.
Eu aş prefera să continuăm această întâlnire cu
întrebări pe care dumneavoastră să mi le adresaţi şi eu să vă
răspund, dar iată, vă voi arunca şi eu vreo două provocări:
în modul în care am trecut prin şcoală şi prin matematică
414

Rãni deschise

am ajuns la concluzia că de fapt această împărţire a
învăţământului în discipline, fiecare predată pe cont propriu,
este ceva artificial, ceva care nu corespunde situaţiei reale
care există azi în lume, în cultură, în societate ş.a.m.d.
Împărţirea pe discipline, pusă sub semnul întrebării
Asta explică de ce mulţi absolvenţi ai Facultăţii de
Matematică nu mai lucrează în specialitatea în care şi-au luat
diploma. Şi lucrul acesta este adevărat nu numai pentru
matematică, ceea ce arată clar că realitatea socială îi obligă
pe oameni să iasă din încorsetarea pe care le-a dat-o o anumită
disciplină pe care au frecventat-o şi să-şi încerce puterile
uneori foarte departe de această pregătire iniţială, de domeniul
pentru care au o diplomă. Asta e situaţia reală şi, când îi întrebi
„la ce ţi-a mai folosit ce-ai învăţat în matematică?”, răspunsul
este întotdeauna acelaşi: „mi-a folosit nu cutare teoremă, nu
cutare noţiune, ci mi-a folosit modul matematic de gândire”.
Iată, am să vă dau un mic exemplu, ca să vă daţi
seama unde se află societatea românească în ceea ce
priveşte eficacitatea şcolii. E un lucru despre care am scris
de mai multe ori: unul dintre evenimentele care pasionează
scena publică românească, la fiecare sfârşit de săptămână,
este 6/49. Toate canalele de televiziune ţin să ne atragă
atenţia să nu pierdem ocazia de a cumpăra un loz, că
n-avem timp decât până duminică şi că suma pusă în joc
415

Solomon Marcus

este de atâtea milioane, o sumă formidabilă. Deci
provocarea e clară: „Ce mai staţi pe gânduri? Duceţi-vă şi
cumpăraţi-vă lozuri! Şansa este extraordinară!”
Din ce cauză cei mai mulţi nu înţeleg ce şanse
de câştig au la loterie?
Care este realitatea? Dacă oamenii ar pune în mişcare
ceea ce au învăţat la şcoală, dacă nu mă înşel, ceea ce au
învăţat prin clasa a zecea, când se învaţă permutări,
aranjamente, combinări, atunci şi-ar putea da seama că şansa
de câştig la 6/49 este de aproximativ de 1 la 14 milioane.
Dar nimeni nu se gândeşte la lucrul acesta, şi inconştienţa
este atât de mare, încât îţi poţi imagina şanse iluzorii de
câştig. Să ne imaginăm că te-ai afla de pildă în troleibuz şi
la un moment dat un călător se ridică şi coboară şi, după ce
a coborât şi a plecat, îţi dai seama că şi-a uitat servieta pe
scaun şi te gândeşti ce să faci: să iei servieta cu tine, dacă
eşti un om cinstit, încerci să dai de păgubaş, cauţi să vezi ce
e în servietă. Constaţi că avea acolo lucruri importante.
Imaginaţi-vă că încercarea de a-l găsi pe păgubaş ar decurge
în modul următor: să te duci să iei cartea de telefon şi să
telefonezi la întâmplare o dată, de două ori, de trei ori, poate
nimereşti pe păgubaş. Evident, este o încercare absolut
nebunească. Ei bine, şansa de a câştiga la 6/49 este egală cu
şansa de a-l nimeri în cartea de telefon pe păgubaşul
416

Rãni deschise

respectiv. Dar iată cum oamenii nu-s în stare, cei mai mulţi,
să valorifice ceea ce au învăţat la şcoală.
Care este atunci profitul şcolii, dacă în situaţii de viaţă,
şi pot să vă dau nenumărate exemple de acest fel, nu-s în
stare să facă legătura cu ceea ce au învăţat la şcoală?
Dumneavoastră trebuie să vă gândiţi bine, pentru că există o
ofertă pe piaţă care ne poate induce în eroare: faptul că există
atât de multe universităţi şi toate ne provoacă să venim, să le
alegem, să mergem şi noi la universitate... Trebuie să vă
gândiţi bine dacă acolo vă este locul şi să încercaţi să
descoperiţi care este adevăratul dumneavoastră interes. Eu
v-aş propune să puneţi dumneavoastră întrebări acum, poate
că în felul acesta reuşim mai bine să intrăm în dialog.
Întrebare din partea unei profesoare: Apropo de
beneficiile şcolii, aş vrea să-l întreb pe domnul
academician ce crede dumnealui că mai poate să însemne
pentru voi, generaţia de astăzi, beneficiul unui om de
ştiinţă, al unui om de cultură, în raport cu un om care este
situat material mult mai bine, pentru că ştiu că asta este o
dihotomie care vă preocupă foarte tare.
Furtul intelectual
Şi, legat de aceasta, să vedem în ce măsură
considerăm omul de cultură un bun comun, furându-i
proprietatea intelectuală. Pentru că domnul academician a
417

Solomon Marcus

scris un articol despre plagiat care pe mine m-a fascinat şi
din când în când îl mai citesc şi elevilor mei. E o provocare
cam dură, care de astă dată vi se cam adresează. Dacă nu
avem nevoie de cultură şi de ştiinţă sau nu ne mai convine
prestigiul acestor discipline care, să zicem, sunt prăfuite,
atunci de ce facem apel totuşi la ceea ce alţi oameni, la un
moment dat, ne-au oferit şi, fără să-i întrebăm sau fără să
avem acest drept, le luăm din beneficiul şi din valoarea şi
harul minţii dumnealor.
Deci cum să facem să ne întoarcem spre a pune omul
de cultură şi de ştiinţă la locul ce i se cuvine, iar
dumneavoastră să încercaţi să vedeţi dacă merită cu
adevărat să vă îndreptaţi spre zona asta sau nu.
Cultura este ca luna: numai o parte este vizibilă!
Solomon Marcus: Vă mulţumesc pentru provocare, nu
ştiu dacă dumneavoastră sesizaţi un extraordinar dezechilibru
care există în societatea noastră şi care e vizibil în foarte multe
feluri. Am propus de mai multe ori această comparaţie: cultura
este ca luna. Ştiţi că luna îşi arată doar una dintre feţe. Aşa e şi
cultura. Cultura care este mediatizată şi pe care o vedem în
reviste, care ne provoacă în mass-media, este aproape exclusiv
cultura care constă în literatură, artă, cu toate speciile ei: teatru,
film etc., anumite domenii sociale legate de economie, de
sociologie, dar cultura ştiinţifică nu este mediatizată. Lucrurile
418

Rãni deschise

merg până acolo încât, de pildă, dacă moare un actor, un
scriitor, un om politic, i se consacră de multe ori nu câteva
minute, ci ore în şir pe diverse canale din mass-media.
Dacă moare un om de ştiinţă, totul se petrece într-un
anonimat total. O să vă dau doar două exemple. A murit relativ
recent Victor Toma, cel care a creat primul calculator electronic
românesc. Aţi citit măcar vreun rând într-o revistă? A fost
măcar vreun singur post de televiziune, măcar TVR-ul ăsta
public, plătit din banii noştri, care să sufle măcar o vorbă
despre moartea lui Victor Toma? Nimic. A murit unul din marii
savanţi români în domeniul aerodinamicii, Virgiliu
Constantinescu, care a fost şi preşedinte al Academiei
Române. Aţi auzit vreo vorbă? Aţi citit măcar vreun rând în
vreo revistă, într-un ziar? Iată unde ne aflăm.
Am dat acest exemplu frapant şi oarecum neplăcut,
ca să înţelegeţi cât de mare este decalajul dintre
receptivitatea la cultura ştiinţifică şi receptivitatea la
cultura aşa-zis umanistă – e un cuvânt cu totul nefericit,
pentru că şi cultura ştiinţifică e la fel de umanistă ca şi
cultura literară.
Ştiinţa, marfă nevandabilă
Deci problema este aceasta: eu cred că fiecare dintre
dumneavoastră îşi poate face acest examen, dacă în şcoală
sau pe alte căi a descoperit frumuseţea ştiinţei,
419

Solomon Marcus

extraordinara aventură pe care ţi-o oferă ştiinţa şi căreia
atâtea figuri şi-au dedicat viaţa. Merită să vă dedicaţi unei
asemenea pasiuni, independent de situaţia de moment sub
aspect financiar, social, al respectivei profesii, pentru că
mă înfior să constat că de mai multă vreme nu prea mai
vin destui studenţi de valoare la fizică şi nici la
matematică.
Mie stadiul în care se află acum multe dintre universităţile noastre mi se pare că reprezintă o situaţie care este un
simptom de boală! Ştiţi că, pe vremea comunismului, era
obiceiul ca mărfurile nevandabile să fie împachetate la un
loc cu mărfuri foarte căutate şi în felul ăsta să te oblige să
cumperi şi marfa nevandabilă. Observ că la cărţi se întâmpla
lucrul ăsta. Era pusă o carte de ideologie care nu interesa pe
nimeni, plină de sloganuri ideologice, la un loc cu un roman
foarte căutat, erai obligat să cumperi marfa nevandabilă ca
s-o poţi obţine pe cea de care aveai nevoie.
În situaţia asta se află acum universităţile noastre de
multe ori. De pildă, la Facultatea de Matematică şi
Informatică, pentru mulţi tineri matematica e marfa
nevandabilă pe care trebuie s-o accepte pentru a putea face
informatică. La Litere, disciplinele care au motivat apariţia
acestor facultăţi, studiul lingvisticii, al literaturii, sunt
pentru mulţi tineri marfa nevandabilă la care trebuie să
adere pentru a putea studia jurnalism, studii culturale.
I-am întrebat pe unii tineri, ca să vă daţi seama cât de mare
420

Rãni deschise

e dezorientarea multor tineri de vârsta dumneavoastră: „De
ce aţi ales această specialitate – studii culturale?” „Pentru
că vrem să devenim purtători de cuvânt, cum am văzut la
televizor.” O astfel de profesie nici nu există: profesia de
purtător de cuvânt. Iată deci ce înseamnă a te orienta după
un lucru pasager, care nu-ţi oferă nicio perspectivă, în loc
de a examina profund care sunt adevăratele profesii.
Dumneavoastră vă aflaţi în faţa acestei alegeri.
Educarea respectului pentru proprietatea intelectuală
Colega mea vorbea de plagiat. Aici este într-adevăr un
alt simptom de boală, pentru că lucrurile încep de la o vârstă
mult mai fragedă, de la copiii chiar de şcoală primară sau
de gimnaziu. Îmi aduc aminte de un copil de pe vremuri, un
copil de clasa a doua primară, care a venit la o revistă pentru
copii şi a spus: „am adus o poezie ca să mi-o publicaţi”. Era
o poezie în care redactorul l-a recunoscut imediat pe
Coşbuc, dar copilul acela credea că, dacă l-a scris cu mâna
lui, acel text a devenit proprietatea sa.
Fiţi foarte atenţi! Aţi văzut acum la televizor că se
vorbea de lucrări de diplomă vândute, teze de doctorat
vândute. Ce ascund toate aceste lucruri? Ascund faptul că
nu s-a făcut la timp educaţia ideii de proprietate
intelectuală. Ce înseamnă că acest briceag e al meu, şi nu
al tău, asta învaţă copilul de la o vârstă foarte fragedă. Dar
421

Solomon Marcus

ce înseamnă că o compoziţie, o compunere, un text anumit,
o idee e a mea, şi nu a ta sau e a ta, şi nu a mea?
Lucrul acesta nu se educă, nu se învaţă şi ajungem să
vedem reviste ale unor licee în care abundă texte evident
preluate din diverse surse, fără să se indice de unde au fost
preluate. Un lucru elementar: de a delimita ceea ce ai
gândit cu capul tău de ceea ce este preluat de la altcineva.
Şi această infirmitate se transmite pe urmă de-a lungul
întregii cariere, şi se fac lucrări de diplomă care au fost
gândite de altcineva, lucrări de grad ş.a.m.d.
Nevoia de a ne clarifica interesul şi personalitatea
Deci iarăşi ajungem la această nevoie de a ne defini
personalitatea, de a înţelege ce suntem, ce vrem, care ne
sunt posibilităţile reale, care ne sunt interesele reale. Nu
să ne lăsăm furaţi de anumite situaţii de moment, pentru
că cea mai mare greşeală pe care poate să o comită un tânăr
este de a-şi alege un drum pentru care nu are un interes
profund, pentru care nu simte o autentică plăcere de a-l
urma. O astfel de greşeală este o sursă de nenorocire, o
sursă de boală, o sursă de eşec în viaţă!
Îmi aduc aminte, când am intrat la Facultate, am ales
Matematica în ideea ca, dacă nu voi considera că alegerea
este cea bună, să aleg altceva după un an. Şi mulţi dintre
colegii mei au făcut acest lucru. Eu am avut şansa de a merge
422

Rãni deschise

de la început la o disciplină în care m-am descoperit foarte
repede. Dumneavoastră o să întrebaţi ce am descoperit în ea
dacă v-am spus mai-nainte că eu fusesem interesat de
literatură şi de filosofie. Am descoperit că matematica are un
numitor comun cu poezia. Şi am să vă spun un lucru pe care
nu o să-l învăţaţi la şcoală şi pe care nu-l învaţă nici studenţii
la universitate: că matematica şi literatura sunt surori, sunt
fiice ale aceleiaşi mame, miturile omenirii.
Matematica şi literatura sunt fiice ale miturilor
Ce au luat ele de la mituri? Au luat funcţia de
simbolizare, care este esenţială şi în literatură şi în
matematică, şi au luat nevoia de ficţiune. Faptul că şi
literatura şi matematica părăsesc lumea contingentă, lumea
reală, şi se plasează într-un univers de ficţiune.
De pildă, ştiţi cum încep elementele lui Euclid? Încep cu
următoarele două enunţuri: Numim punct ceea ce nu are părţi.
Numim linie ceea ce nu are grosime. Dintr-odată suntem
plasaţi într-o lume de ficţiune, pentru că nimeni nu a văzut
ceva care nu are părţi sau ceva care nu are grosime. Deci toată
matematica, exact ca şi literatura, care introduce în scenă
personaje imaginare, conflicte imaginare, face la fel. Introduce
personaje imaginare prin diverse obiecte, diverse noţiuni, şi
creează conflicte, în sensul că lansează nişte probleme,
mergem pe urmele acelor probleme şi apar situaţii conflictuale.
423

Solomon Marcus

Teorema este, am spune, inima matematicii. Ştiţi care
este etimologia cuvântului teoremă? Teorema este un
cuvânt de origine greacă. Ştiţi ce înseamnă? Una din
semnificaţiile cuvântului teoremă este aceea de spectacol.
Deci teorema a fost concepută de la început ca un
spectacol, exact ca şi teatrul. În ce constă spectacolul oferit
de o teoremă? O teoremă, înţeleasă cum trebuie şi urmărită
cu atenţie în modul în care este găsită, nu doar în modul în
care este demonstrată, este într-adevăr un spectacol. Sunt
conflicte acolo. Matematica asta pe care o vedeţi aşa în
manuale, cu enunţ, demonstraţie, este numai felul în care
iese în lume, cum se îmbracă ea ca să iasă în lume, să pară
ordonată, pusă la punct; cum se spune: ras, tuns şi frezat.
Dar, în viaţa ei internă, matematica abundă în ezitări, în
mirări, în încercări eşuate, greşeli, emoţii ş.a.m.d. Asta e
viaţa adevărată a matematicii. Moisil spunea că o teoremă
este un sentiment. Tot pe aceeaşi linie de gândire.
Vedeţi că, dacă examinaţi toată istoria culturii, aveţi
întâi vechile mituri ale popoarelor, încercarea de a pune în
legătură omul şi universul, antropos şi cosmos, şi pe urmă
vine literatura, care apare în civilizaţia greacă odată cu
Homer, şi abia câteva secole mai târziu intră în scenă
matematica lui Thales, a lui Pitagora ş.a.m.d. Vă daţi
seama ce spectacol extraordinar, care depăşeşte, care
nesocoteşte complet această împărţire şcolărească: de la
ora 9.00 la 10.00 matematică, de la 10.00 la 11.00 fizică,
424

Rãni deschise

de la 11.00 la 12.00 română ş.a.m.d. E o chestie complet
artificială şi, dacă nu reuşim să trecem în spatele lor, să
vedem ce se ascunde în dosul acestei împărţiri... dar cred
că e momentul la care dumneavoastră să puneţi întrebări.
De multe ori, matematica nu confirmă aşteptarea
intuitivă
O elevă: Care sunt marile probleme ale matematicii
la ora actuală?
Solomon Marcus: În anul 2000 s-a publicat o carte,
„Problemele Mileniului”, în care figurează o listă de
probleme pentru care se oferă, pentru fiecare în parte, un
milion de dolari recompensă pentru cel care o rezolvă. Am
să vă spun două dintre ele.
Singura problemă care s-a rezolvat este conjectura lui
Poincaré. Să vă spun în vorbe mai simple în ce constă
conjectura aceasta: constă pur şi simplu în a înţelege ce este
o sferă. Această minge după care aleargă fotbaliştii, pe care
o lovesc jucătorii de tenis şi asupra căreia îşi îndreaptă
atenţia jucătorii de biliard, ce este ea în fond ca structură?
În asta constă conjectura lui Poincaré: în caracterizarea
sferelor. În spaţii euclidiene de diverse dimensiuni.
Şi culmea, ştiţi ce se întâmplă în matematică? Şi ăsta este
unul din marile ei puncte de atracţie, mărturisesc că aici s-a
aflat şi lucrul care m-a atras pe mine în matematică, şi anume
425

Solomon Marcus

faptul că adevărurile matematicii sunt de multe ori în conflict
puternic cu ceea ce ne spun simţul comun, intuiţia, mai exact
problema caracterizării sferelor într-un spaţiu euclidian cu „n”
dimensiuni. Această problemă a fost rezolvată în mod succesiv
pentru diverse valori ale lui „n”, cu mari eforturi, şi singura
valoare a lui „n” pentru care până nu de mult nu fusese
rezolvată a fost valoarea n=3. De obicei, situaţiile cele mai
familiare sub aspect intuitiv sunt cele mai dificile sub aspect
teoretic. Şi această caracterizare a sferelor, care implică noţiuni
de topologie pe care nu le pot folosi aici, a fost rezolvată abia
în urmă cu vreo câţiva ani. Altele aşteaptă.
A înţelege infinitul
Vă dau un exemplu de o mare problemă pusă încă la
începutul secolului trecut de Hilbert, care stă şi acum
nerezolvată. Una din marile pasiuni ale matematicii este
înţelegerea infinitului. Matematica este singura ştiinţă care
te învaţă în ce fel poţi să distingi diverse tipuri de infinitate.
Noi ne aflam în acest domeniu acum 150 de ani în faza în
care se afla omenirea cu multă vreme în urmă, când se
număra aşa: 1, 2, 3 şi dincolo de trei spuneam mai mulţi.
Adică nu erau încă descoperite numerele. În stadiul ăsta
ne aflam în domeniul infinitului până în urmă cu vreo sută
şi ceva de ani, când un matematician german a reuşit să
introducă o ierarhie infinită de tipuri de infinitate.
426

Rãni deschise

Am aflat un lucru extraodinar, care a atras interesul şi al
filosofilor, şi al teologilor: că nu există un cel mai mare infinit,
aşa cum nu există un cel mai mare număr natural. Orice număr
se ia, de pildă 1000, e un număr mai mare ca el, 1001; exact
aşa se întâmplă şi în domeniul infinitului; există infinituri din
ce în ce mai mari şi, orice infinit ţi-ai da, există altul mai mare.
Problema care a rămas deschisă aici este următoarea:
s-a putut demonstra că punctele de pe o dreaptă formează
o infinitate mai mare, mai numeroasă decât numerele
naturale. Mulţimile care sunt de forma 1, 2, 3, 4, 5, ... se
cheamă mulţimi numărabile, mulţimile care sunt de forma
punctelor de pe o dreaptă se cheamă mulţimi de puterea
continuului, despre care s-a demonstrat că este mai mare
decât numărabilul. Problema care a rămas deschisă este
dacă există o infinitate mai puternică decât numărabilul şi
mai slabă decât a continuului.
S-au obţinut unele rezultate cu totul neaşteptate, cum
că această chestiune n-ar putea fi tranşată în logica aceasta
tradiţională binară, un enunţ este adevărat sau fals, şi că
pur şi simplu noi nu ştim încă să construim baza
axiomatică a matematicii. Ce înseamnă asta? Înseamnă să
ne punem întrebarea care să fie acele enunţuri pe care să
le acceptăm fără demonstraţii. Deci iată două exemple de
probleme mari.
Aşa, sigur sunt multe probleme care au enunţul
elementar, pe care-l înţelege şi un copil, şi care n-au fost
427

Solomon Marcus

rezolvate. De exemplu: aveţi un pătrat. Cum îl împărţiţi în
cinci părţi egale? Păi, ducând paralele echidistante la una
dintre laturi. Dar pe urmă vine întrebarea: este aceasta
singura modalitate de a împărţi un pătrat în cinci părţi
egale? Nu ştim să răspundem la această întrebare. Sunt
nenumărate întrebări elementare la care nu s-au putut găsi
răspunsuri.
Un elev: Să presupunem un caz ideal. Am ales
profesia de cercetător, de om de ştiinţă. Credeţi că asta
presupune un sacrificiu pentru familie?
Solomon Marcus: E adevărat că de foarte multe ori
cei care devin pasionaţi de cercetare vin din familii care
i-au făcut educaţia în acest sens, din părinţi care la rândul
lor au fost cercetători. Dar avem şi destule exemple exact
opuse.
Cel mai mare geometru al României, Gheorghe
Vrănceanu, venea dintr-o familie dintr-un sat din judeţul
Bacău, din părinţi aproape analfabeţi, care n-aveau în casă
nicio carte. Destinul său era să păzească vite toată viaţa.
Deci exact situaţia opusă. Şi acest copil de ţăran a
beneficiat de atenţia unui învăţător care l-a înţeles şi l-a
trimis la liceu la Vaslui, de acolo a ajuns la faimosul liceu
internat din Iaşi ş.a.m.d.
Am avut şi eu studenţi de acest fel, care au venit din
case fără nicio carte şi care totuşi au avut forţa de a se
afirma.
428

