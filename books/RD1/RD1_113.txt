Rãni deschise

Întâlnire cu elevi de la Colegiul Naţional
„Gheorghe Lazăr”
Bucureşti, 8 martie 2010;
articol apărut în Tribuna învăţământului în aprilie 2010,
sub titlul „De la discipline izolate la comunicarea
dintre ele”; a consemnat Mihaela Iancu

Activitatea şcolară este organizată pe discipline care
nu prea comunică între ele. Solomon Marcus a
experimentat un mod de a le traversa. Demonstraţia a
avut loc la 8 martie 2010, cu câteva zile înainte ca
Ministrul Educaţiei să anunţe includerea unor probe
transdisciplinare în structura examenelor şcolare.
Primii sosiţi s-au aşezat în rândurile din spatele sălii
de festivităţi de la Colegiul Naţional „Gheorghe Lazăr”,
din Bucureşti. Ştiau că urma o lecţie pe care Societatea
de Ştiinţe Matematice avusese ideea să o organizeze
împreună cu academicianul şi matematicianul Solomon
Marcus. Pentru liceeni, rândurile din spate erau prima
opţiune. Aşa scădeau şansele de interacţiune cu cel de la
1151

Solomon Marcus

masa pe care o vedeau în faţa lor, la o distanţă liniştitor
de mare. Şi au intuit bine, pentru că Marcus, cu ochii la
cei peste 200 de elevi de la a IX-a până la a XII-a, a spus:
„Vă previn că, dorind să interacţionez cu
dumneavoastră, voi acorda un privilegiu celor din primele
rânduri, pe care îi pot auzi mai uşor.”
În sală, s-a pornit rumoarea. Cei din faţă şi-au înălţat
trupurile înguste între braţele scaunelor, străduindu-se să
zâmbească. Cei din spate şi-au lăsat corpurile să alunece
pe speteze, satisfăcuţi. „Am scăpat”, şi-au spus. Dar ceea
ce aveau să asculte nu era chiar o lecţie şi nu era numai
despre matematică.
„M-am născut în anul 1925”, începe. „Aveam vârsta
voastră când începea o perioadă tragică.”
Liceenii din faţa lui nu au trecut prin războaie sau
catastrofe. Ca el. Dar a scăpat cu viaţă. Şi, de atunci, şi-a
spus că ar fi o crimă să ia zilele uşor, să nu folosească la
maximum această şansă. S-a trezit în el ambiţia de a se
autodepăşi. Crede în lecţiile pe care ni le predă istoria şi
crede că un om inteligent e cel care învaţă şi din
experienţele altora, nu doar din cele personale. Aşa că
întreabă, cu privirea spre un elev din primul rând:
„Ce formă de conducere avea România în 1925?”
„Regat”, se aude prompt.
„Şi cine era regele Românei?”
„Ferdinand.”
1152

Rãni deschise

„Când aveam eu 17 ani, vârsta voastră de acum, ce
se întâmpla cu România?”
„Era al Doilea Război Mondial”, răspunde o voce de
la mijlocul sălii.
„Da.”, confirmă Marcus. Apoi continuă pe un ton mai
scăzut. „Eu îmi reproşez că nu am stat de vorbă cu părinţii
mei, să aflu mai mult despre vremea lor. Şi nici nu mi-am
apucat bunicii. Dintre cei din faţă, e vreunul care are toţi
cei patru bunici în viaţă?”
O mână se ridică. „Ai stat de vorbă cu ei? Le cunoşti
viaţa?”, întreabă Marcus.
„Evenimentele majore ale vieţii”, spune un băiat cu
coafura unui punkist indecis între rebeliune şi blazare.
Marcus ridică ochii şi cuprinde sala.
„Cred că interesul vostru pentru istoria imediată nu e
prea mare. Am dreptate? Cine îmi spune?”
Elevii pasează microfonul din mână în mână, ca pe
un rău.
„Nu. De ce daţi mai departe?”
Râd şi ei, şi Marcus.
Ştie prea bine că o oarecare stare de teamă se
instalează ca un automatism de fiecare dată când sunt în
faţa unui profesor. Pentru că relaţia profesor-elev e, uneori,
compromisă în educaţie. Pentru că profesorii nu reuşesc
întotdeauna să stabilească un echilibru între poruncă,
întrebare, îndoială şi rugăminte; să-i facă pe elevi să se
1153

Solomon Marcus

pasioneze de ceva, să-i asculte, să le lase dreptul de a greşi,
libertatea de a căuta singuri răspunsuri, de a întreba.
„Vreau să vă provoc la un exerciţiu”, le spune.
„Fiecare din voi are 2 părinţi. Fiecare dintre ei are, la
rândul lui, 2 părinţi. Deci 4 bunici. Câţi străbunici avem?”
„8”, se aude din colţuri diferite ale sălii.
„16”, spune o voce din primul rând.
„Ar însemna ca fiecare bunic să aibă 4 părinţi”,
conchide Marcus. „Cam greu.”
Copiii râd odată cu el.
„8”, se aude iarăşi.
„Da. Şi dacă merg la a n-a generaţie?”
„2 la puteri consecutive”, răspunde sigur un băiat
lipit de unul din pereţii încăperii.
„Ce fel de şir formează aceste puteri?”, continuă. „O
progresie geometrică. Ştim să-i calculăm suma.”
Elevii rămân în aşteptare.
„Acum vine o întrebare mai grea. Dacă vreau să-mi
urmăresc toţi strămoşii, să-mi reconstitui ceea ce se cheamă
arborele genealogic (l-am văzut acasă la profesorul meu,
Grigore Moisil, care-şi cunoştea strămoşii până cu câteva
secole în urmă), până la ce putere a lui 2 trebuie să merg?”
Se lasă liniştea.
„Asta nu mai e de resortul matematicii”, le spune.
„Asta priveşte antropologia, genetica, lingvistica. Cum
urmărim acum problema?”
1154

Rãni deschise

Marcus i-a luat din istorie şi a alunecat cu ei, pe
nesimţite, în matematică, iar acum încearcă să le lărgească
orizontul.
„Aici e momentul în care vă poate veni în ajutor
internetul. Ce sintagmă trebuie să căutaţi pe Google?”
Răspunsul nu vine.
„Human evolution, de pildă”, îi ajută el. „Aţi învăţat
engleză, înţelegeţi ce înseamnă asta.”
A susţinut întotdeauna nevoia de a integra internetul
în educaţie, un instrument la îndemână, prin care elevii pot
compensa lacunele şcolii. Când el făcea cercetare, trebuia
să petreacă ore întregi prin biblioteci, căutând informaţii,
dar oricum nu putea aduna la fel de multe surse pe cât îi
oferă un motor de căutare, în doar câteva secunde.
Adevăratele provocări ţin de a-l folosi cât mai eficient, la
potenţialul real şi de a citi critic informaţia pe care el o
oferă, de a suspecta mereu sursele.
„Veţi vedea că, în ceea ce priveşte apariţia şi evoluţia
omului, suntem în domeniul ipotezelor, al disputelor, faţă
de care spiritul nostru critic trebuie să fie tot timpul treaz.”
Şi aşa îi pregăteşte pentru un principiu la care ţine mult.
„În justiţie, se pleacă de la prezumţia de nevinovăţie. În
educaţie, trebuie să funcţioneze prezumţia de îndoială.” Şi
când le spune asta, unii liceeni zâmbesc şi aprobă cu capul.
„Trebuie să vedeţi modul în care disciplinele
comunică între ele”, continuă cu aceeaşi dicţie de invidiat.
1155

Solomon Marcus

„Să nu izolăm disciplinele, aşa cum se întâmplă în unele
manuale.”
Marcus şi elevii ajung, în mod natural, să discute
problema eredităţii.
„Fiecare dintre numeroşii mei strămoşi poate
contribui la propria mea moştenire genetică.”
Le explică în ce fel mecanismul eredităţii şi
organizarea sistemului nervos au o structură de limbaj, de
aceea au loc transferuri metaforice de idei din lingvistică în
domeniul biologiei. O demonstrează terminologia folosită
în studiul eredităţii: „alfabet”, „cuvinte”, „dicţionar”,
„traducere”, „codificare”, „gramatică”. Lucrul acesta se
întâmplă atât în chimia eredităţii (ADN-ul şi ARN-ul sunt
cuvinte pe câte un alfabet de patru baze nucleotide), cât şi
în biologia eredităţii (orice proteină este un cuvânt pe
alfabetul celor 20 de tipuri de aminoacizi).
Elevii aflau pentru prima dată că metafora are un rol
nu doar în „operele lirice”, cum credeau ei, ci şi în ştiinţă.
Marcus le aminteşte că Lucian Blaga separa metafora
plasticizantă, care sensibilizează, de cea revelatorie, care
îmbogăţeşte semnificaţia. I-a fost greu să obţină răspunsuri
corecte din partea elevilor. Nu erau obişnuiţi cu asemenea
provocări. Continuă să le vorbească despre rolul metaforei
în matematică. Drumul de la Pitagora până la introducerea
conceptului de număr real a fost unul metaforic. Abia în
secolul XIX, oamenii au aflat cu adevărat semnificaţia a
1156

Rãni deschise

ceea ce numeau, de pildă, rădăcină pătrată a lui 2.
Probleme interesante de combinatorică apar şi dacă ne
gândim că numărul aranjamentelor cu repetiţie de patru
elemente luate câte trei ne dă dimensiunea codului genetic.
Părerea lui e că manualele împărţite pe domenii nu-şi
au rostul în şcoala primară. Vreme de aproape 2000 de ani,
cunoaşterea umană a fost adunată sub numele generic de
filosofie. Eliberarea ştiinţelor de sub tutela ei s-a întâmplat
abia în secolul al XIX-lea. Nu e întâmplător faptul că,
pentru atâta vreme, cunoaşterea a rămas sub aceeaşi
umbrelă. Şi nu poate fi ignorat transferul de concepte,
rezultate şi teorii, care se petrece azi între discipline.
„Puteţi da exemple în care aţi întâlnit logaritmul? În
alte domenii decât matematica.”
Elevii se agită, dar nu se hotărăsc asupra unui
răspuns.
„La psihologie l-aţi întâlnit?” Linişte. „Aţi auzit de
legea dată de Weber şi Fechner?” Aceeaşi linişte. „Ea
spune că, dacă un organism primeşte un şir de excitaţii care
merg în progresie geometrică, atunci senzaţiile corpului
care reacţionează la aceste excitaţii merg în progresie
aritmetică. Înseamnă că tendinţa noastră este de a domoli
ritmurile naturii, pentru că progresia geometrică merge mai
repede decât cea aritmetică.”
S-a sunat de ieşire în Colegiul Naţional „Gheorghe
Lazăr” şi câţiva din sală se ridică şi se îndreaptă spre uşă.
1157

Solomon Marcus

Odată cu ei o fată în rochie lila, care îşi face loc printre
colegi. Nu merge odată cu ceilalţi. Se mută doar, din al
şaselea rând, în primul.
Marcus nu se opreşte. Îi roagă să-şi imagineze o hartă
cognitivă, pe care, la Nord, s-ar regăsi gândirea reflexivă,
la Sud, cea empirică, la Vest, emisfera stângă a creierului,
iar la Est, emisfera dreaptă. Apoi le spune că modul actual
de a percepe educaţia înseamnă colaborarea tuturor
zonelor din această hartă. Fiecare disciplină are nevoie de
toate zonele, nu ca în trecut, când se considera că
matematica este la întâlnirea reflexivului cu emisfera
cerebrală stângă, chimia se afla la întâlnirea aceleiaşi
emisfere cu empiricul, psihologia se plasa la întâlnirea
emisferei drepte cu empiricul, iar arta la întâlnirea
emisferei drepte cu reflexivul.
„În realitatea de azi, disciplinele comunică intens. Cei
ce le despart artificial vor trebui să repare această greşeală
de-a lungul vieţii, dar le va fi greu.”
Nu e prima oară când aminteşte că metamorfoza
profesiilor e atât de mare, încât trebuie să fim pregătiţi să
lucrăm în domenii care nu există astăzi, dar vor exista,
poate, peste 10 ani. De fapt, rolul şcolii, aşa cum îl vede
Marcus, este de a ne pregăti pentru situaţii despre care nu
am învăţat. În şcoală ar trebui să se formeze un mod de
gândire, o personalitate. În faţa unei realităţi atât de
schimbătoare, singura şansă de a nu rata e aceea de a alege
1158

Rãni deschise

lucrul care ne pasionează. Altfel, uzura noastră va fi mai
rapidă, 65 de ani ni se va părea o vârstă prea înaintată
pentru pensionare, pentru că munca va fi o caznă.
„Să nu credem că, dacă suntem cronologic tineri,
chiar suntem tineri în spirit.” Câteva rânduri au rămas
goale la mijlocul sălii. Alte câteva scaune libere, ici şi colo.
Dar majoritatea elevilor stau aproape nemişcaţi. „Mulţi se
obişnuiesc să fie mahmuri o bună perioadă a vieţii lor
şcolare şi rămân mahmuri şi în profesie şi aşa le trece viaţa.
Vreţi să trăiţi mahmuri sau vreţi să trăiţi într-o stare de
trezie, de plăcere faţă de atâtea minuni ale cunoaşterii?”

1159

