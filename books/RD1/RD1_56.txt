Solomon Marcus

Urgenţele şcolii
Tribuna învăţământului,
nr. 1074, 2011

Suntem bombardaţi cu tot felul de procente
îngrijorătoare. De la Comisia Europeană aflăm că 40% din
tinerii de 15 ani sunt semianalfabeţi. Semianalfabetismul nu
e clar definit. Să fie echivalentul englezescului functional
illiteracy? Înseamnă că 40% nu sunt în stare să înţeleagă un
text mai cuprinzător decât indicaţiile care apar în traficul
rutier; nu ştiu să redacteze o cerere către o instituţie, sunt
lipsiţi de cunoştinţe elementare privind natura şi societatea,
nu pot dezvolta un raţionament. Pe de altă parte, aflăm că
75% dintre tinerii între 6 şi 17 ani folosesc internetul, deci,
combinând acest fapt cu cel relatat anterior, ni se sugerează
că cele două tipuri de alfabetizare de care are nevoie un tânăr
azi, alfabetizarea clasică (citire, scriere, vorbire în cel puţin
o limbă) şi alfabetizarea computaţională (capacitatea de
folosire a calculatorului electronic) nu merg întotdeauna
împreună. Numai că ar trebui să introducem şi noţiunea de
semianalfabetism computaţional, adică situaţia în care ştii
364

Rãni deschise

să deschizi un calculator, cunoşti operaţiile de bază care-ţi
permit să ajungi pe un anumit site, dar rămâi fixat cu atenţia
pe anumite site-uri de divertisment, bazate preponderent pe
imagini vizuale, deci care nu te obligă la eventuale lecturi
care cer un efort de gândire. Mi se pare evident faptul că
semianalfabetismele de cele două feluri merg, de cele mai
multe ori, mână în mână.
De la aceste derapaje ale minţii nu e decât un pas până
la derapajele sociale, morale. Aflăm că 60% dintre copiii
care frecventează internetul recunosc faptul că au fost
contactaţi de pedofili, iar 80% dintre adolescenţi
obişnuiesc să mintă, pretinzând că au împlinit 18 ani.
Proliferarea semianalfabetismului de orice fel este,
evident, rezultatul eşecului proiectului educaţional, despre
care am scris mereu. Derapajele de ordin psihologic,
moral, social sunt lăsate, în actuala organizare a şcolii, pe
seama dirigintelui clasei şi, desigur, pe seama familiei
elevului, dar, evident, dirigintele şi familia sunt depăşiţi
în acţiunea lor de acţiunea de sens invers a străzii, a
instituţiilor, a mass-mediei. Mereu ne parvin ştiri despre
violenţa din şcoli şi din familii. Dar acum ne va preocupa
acţiunea şcolii.
Am să încep prin a evoca o dispută în Senatul
României, la 25 ianuarie 1908, între Spiru Haret, ministrul
Cultelor şi Instrucţiunii Publice, şi senatorul Titu
Maiorescu, fost ministru la acelaşi Minister (a se vedea
365

Solomon Marcus

vol. VI din Operele lui Spiru Haret, ed. a II-a, Editura
Comunicare.ro, Bucureşti, 2009, pp. 76-91). Maiorescu îl
interpelează pe Haret «asupra direcţiunii ce se dă activităţii
învăţătorilor rurali în afară de şcoală […] El crede că este
cu totul greşită ideea de a-l îndemna pe învăţător să se
amestece în viaţa satului, că el trebuie lăsat ca prin acţiunea
asupra copiilor să lucreze la progresul cultural al
populaţiunii rurale. Crede chiar că ministrul Instrucţiunii
depăşeşte atribuţiunile sale, când se amestecă în chestiuni
de alt ordin decât cel şcolar.» Haret tocmai publicase, cu
trei ani în urmă, Chestia ţărănească, în care preconiza
pentru învăţător un rol activ în viaţa satului. Acum revine
şi precizează: «Căci dacă el (învăţătorul) este chemat să
facă educaţia copiilor, trebuie să-i înveţe nu numai
aritmetica şi gramatica, ci şi simţământul de demnitate
omenească. Aşa fiind, eu nu înţeleg deloc smerenia ca o
condiţie primordială a învăţătorului […] De altminteri,
d-sa a explicat ce înţelege prin smerenia aceasta: Să se
mărginească la şcoala lui, pur şi simplu».
Nu pot reproduce aici mai mult, dar cât de actuale
sunt aceste dezbateri! De fapt, la fiecare dintre cei doi
parteneri există un sâmbure de adevăr. Maiorescu accentua
profesionalitatea şi seriozitatea cu care trebuie învăţătorul
să se concentreze pe materia pe care o predă copiilor, iar
Haret avea în vedere funcţia general educaţională a
învăţătorului, ale cărui datorii trec dincolo de materia
366

Rãni deschise

predată, la aspectele cetăţeneşti şi morale, şi dincolo de
pereţii şcolii, la întreaga viaţă a satului, care are atât de
multă nevoie de un îndrumător, într-un moment al istoriei
în care România se afla dominată de analfabetism şi de
sărăcie. Dar apartenenţa lor la partide politice diferite
imprimă polemicii unele accente pătimaşe, pe care nu
le-am mai reprodus aici şi care îi împiedicau pe amândoi
să vadă partea de dreptate a celuilalt. Ar fi trebuit ca
generaţiile următoare, care au realizat cât de mult
datorează şi unuia, şi celuilalt, să înveţe ceva din această
lecţie. N-a fost să fie. Dezbaterile recente asupra Legii
Educaţiei au fost atât de mult otrăvite de patima
politicianistă, încât ele au împiedicat în mare măsură o
discuţie profitabilă.
La o privire retrospectivă, vom constata totuşi că şcoala
a mers mai degrabă pe direcţia preconizată de Maiorescu, a
învins mentalitatea concentrării dascălului pe disciplina
predată şi a delegării unui diriginte pentru problemele
educaţionale generale. Cred că acum avem nevoie cu
precădere de strategia preconizată de Haret. Orice dascăl
trebuie să fie şi educator, să fie deci şi diriginte: «[…]să-i
înveţe nu numai aritmetica şi gramatica, ci şi simţământul
de demnitate omenească». Adaptată la vremea noastră,
atitudinea lui Haret ne obligă să cerem fiecărui profesor să
fie capabil să interacţioneze cu profesorii de la celelalte
discipline, pentru a fi capabil să-i pregătească pe elevi în
367

Solomon Marcus

spiritul interacţiunii disciplinelor, pentru o societate
globalizată din toate punctele de vedere: informaţional,
comunicaţional, computaţional şi cognitiv. Mai înseamnă că
orice profesor trebuie să înveţe să-i educe pe elevi în
folosirea inteligentă a internetului, explicând, la momentele
oportune, cum anume poate beneficia însuşirea disciplinei
predate de apelul la internet. Mai înseamnă că accentul
trebuie să cadă într-o măsură mai mare pe nevoia de a
înţelege, pe de ce-ul lucrurilor şi într-o măsură mai mică pe
însuşirea de procedee şi informaţii.

368

