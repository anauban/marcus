Rãni deschise

Despre greşeli şi eşecuri
Idei în dialog, nr. 3, martie 2009

Cine greşeşte păcătuieşte
Trăim într-o lume în care greşeala este asimilată cu o
infracţiune, cu un păcat, în cel mai bun caz cu o
inadvertenţă. De multe ori, greşeala este ruşinoasă, iar
teama de a nu greşi îi inhibă pe mulţi oameni în încercarea
de a se implica într-o acţiune inedită. Faptul este foarte
vizibil la şcolari, mai ales la cei mici, care-şi reprimă
tentaţia de a pune întrebări de teama de a nu comite cine
ştie ce greşeală. Întregul sistem de evaluare la diferite tipuri
de examene şi concursuri este bazat pe bareme elaborate în
raport cu greşelile şi insuficienţele posibile. Referenţii care
au de stabilit calitatea unei lucrări propuse pentru publicare
într-o revistă de specialitate acordă o atenţie deosebită
eventualelor greşeli şi gradului lor de gravitate. Traficul
rutier este controlat în mod riguros prin reguli care privesc
atât pe pietoni, cât şi pe conducătorii de diferite tipuri de
vehicule, iar abaterile de la aceste reguli constituie greşeli
245

Solomon Marcus

cu statut de infracţiune şi sunt sancţionate în raport cu
gravitatea lor. Statutul negativ al greşelii este vizibil în toate
tradiţiile, în mituri, în religii, în folclor. Chiar în zicala de
origine latină conform căreia „a greşi este omenesc”
statutul negativ al greşelii este prezent, numai că i se acordă
uneori circumstanţe atenuante.
Dar nu cumva există şi o faţă pozitivă a greşelii
şi eşecului?
Vom arăta că răspunsul la această întrebare este
afirmativ. Mai mult, eludarea acestui fapt produce pagube
imense sistemului educaţional. Greşeala şi eşecul sunt
preţul inevitabil pe care trebuie să-l plătim pentru
îndrăzneala de a gândi cu capul nostru, de a nu ne mulţumi
să repetăm cele spuse de alţii. Fără dreptul de a greşi şi de
a eşua, gândirea noastră nu se poate mişca de la rutină la
iniţiativa personală şi de aici la descoperire şi la invenţie.
Mai mult, numărul eşecurilor este, de obicei, mult mai
mare decât cel al reuşitelor. Plasându-ne în cazul-limită în
care nicio încercare nu are succes, vom observa că eşecul
în încercarea de a răspunde la o anumită întrebare ascunde
de multe ori elemente interesante în raport cu o întrebare
diferită de aceea care ne-a preocupat iniţial. Vorba „unde
dai şi unde crapă” este tocmai expresia acestei situaţii.
Gândirea personală începe cu libertatea de a (ne) pune
246

Rãni deschise

întrebări. Întrebarea este forma cea mai simplă a spiritului
critic şi a creativităţii. Pare un truism, dar eşecurile în
educaţie se datorează în bună măsură evitării modalităţii
interogative, sacrificate în favoarea modalităţii imperative.
„Să se arate că” este preferat lui „Este oare adevărat că?”.
Verificarea unui enunţ stabilit de alţii este preferată
descoperirii pe cont propriu a enunţului respectiv.
Spiritul de invenţie este mai greu de evaluat pe bază
de bareme prestabilite, de aceea i se preferă cel de
conformitate cu ceea ce au inventat sau descoperit alţii.
Învăţarea autentică trece şi ea prin greşeli şi eşecuri
Dar ceea ce se întâmplă la nivelul marii creaţii
ştiinţifice sau artistice este valabil şi la nivelul proceselor
de învăţare pe care le parcurg şcolarii. Învăţarea autentică
este numai aceea activă, care te asigură că poţi reconstitui
pe cont propriu un rezultat, o idee, o noţiune, o teorie, deci
că eşti în stare nu numai s-o reproduci ca formulare, ci să
dovedeşti că ai înţeles-o, propunând o prezentare
alternativă, personală a ei, stabilindu-i explicit corelaţii
care altfel rămâneau implicite, furnizând exemple inedite.
Acest tip de învăţare creatoare nu este posibil fără
libertatea de a încerca o abordare proprie, fără dreptul de
a eşua în aceste încercări şi de a comite greşeli. Această
tatonare are toate trăsăturile unui joc, dar nu un joc de tipul
247

Solomon Marcus

şahului, în care se acţionează pe baza unor reguli
prestabilite, ci un joc în care eventuale reguli urmează a fi
găsite chiar în procesul său de desfăşurare. Jucătorul, adică
cel care învaţă, care caută, este ghidat de bucuria
curiozităţii sale şi de libertatea de care beneficiază în acest
scop, iar răsplata constă în satisfacţia de a înţelege un lucru
pe care anterior îl ignoră.
Să rezumăm: învăţarea, gândirea personală,
creativitatea presupun o punere concomitentă în mişcare a
libertăţii de întrebare şi de căutare, a dreptului de a greşi
şi de a eşua, a practicii ludice şi a spiritului critic.
Puţină etimologie
Dispunem de două cuvinte, aparent echivalente:
„greşeală” şi „eroare“, primul de origine slavă, al doilea
având o etimologie latină. La fel, în franceză, „faute” şi
„erreur”; în engleză, „mistake“ şi „error“, în germană,
„Fehler” şi „Irrtum”. Nu ne putem angaja într-o discuţie
privind diferenţierea lor semantică, dar este uşor de
observat că în româneşte „eroare” este preferat atunci când
este vorba de greşeli care nu sunt rezultatul unei slăbiciuni
omeneşti: vorbim despre eroarea de aproximare a rădăcinii
pătrate a lui doi, dar de o posibilă greşeală în identificarea
zecimalei de un anumit ordin în această aproximare.
Verbul latinesc „errare” (a se vedea şi verbul francez
248

Rãni deschise

„errer”, verbul englez „to err”) înseamnă, desigur, „a
greşi”, dar înseamnă şi „a merge încolo şi încoace, în
libertate, a te rătăci, a te înşela”, iar „errer” înseamnă „a
merge la întâmplare, fără un scop precis”. Ideea de joc şi
aceea de libertate sunt clar exprimate şi asocierea lor cu
greşeala este clară. Să observăm că limba română nu
dispune de un verb corespunzător lui „errare” sau lui
„errer”, adică asociat cu substantivul „eroare”, aşa cum
„errer” este asociat cu „erreur”.
Trei tipuri de greşeli: sintactice, semantice,
pragmatice
Uneori sesizăm mai bine semnificaţiile unui cuvânt
nu mergând la definiţie, ci căutându-i termenii opuşi. Care
ar fi opusul adjectivului calificativ „greşit”? Urmărind
diferitele contexte în care acesta apare, vom menţiona mai
întâi pe „corect“ (acesta vizează aspectul sintactic), apoi
pe „adevărat” (care vizează aspectul semantic), apoi pe
„just” (la care predomină aspectul semantic), apoi pe
„drept”, „legal”, „adecvat”, „potrivit”, „normal” (la care
predomină aspectul pragmatic), apoi pe „cert”,
„incontestabil”, „evident” (la care-i lăsăm pe cititori să se
descurce).
Distincţia sintactic-semantic-pragmatic a fost
introdusă iniţial în semiotică (Ch. S. Peirce, Ch. Morris),
249

Solomon Marcus

dar a fost adoptată şi în lingvistică, funcţionând şi în teoria
sistemelor formale a lui D. Hilbert, iar mai recent, în teoria
limbajelor de programare la calculator. Sintaxa unui sistem
formal se referă la fenomenele care au loc în interiorul
sistemului; semantica priveşte diferite interpretări posibile
ale acestuia, iar pragmatica se referă la relaţiile sistemului
cu utilizatorii săi. În limbile naturale, graniţa dintre sintaxă
şi semantică nu este riguroasă, nici aceea dintre semantică
şi pragmatică. Va rezulta că nici diferenţierea celor trei
tipuri semiotice de greşeli nu este întotdeauna riguroasă.
Greşelile sintactice constau în încălcarea unor reguli, cele
semantice se referă la valori de adevăr, precum şi la
aspecte de adecvare şi de relevanţă, iar cele pragmatice se
referă la relaţia cu utilizatorii limbii. Greşelile de calcul şi
dezacordurile gramaticale sunt de natură sintactică.
Definiţia tangentei la o curbă ca dreaptă care o întâlneşte
într-un singur punct este o greşeală semantică (definiţia
este valabilă la cerc, dar nu mai rămâne valabilă în
general).
Chestiunile de adecvare şi de relevanţă ale unui
model matematic faţă de un anumit fenomen fizic sau
social sunt susceptibile de greşeli semantice. Costul
excesiv al unor operaţii, complexitatea prea ridicată a unor
sisteme conduc la inconveniente de natură pragmatică.
Folosirea seriei alternate a lui Leibniz în vederea
identificării zecimalelor succesive din reprezentarea
250

Rãni deschise

numărului π este o greşeală de natură pragmatică, din
cauza convergenţei foarte lente a seriei respective.
Dicţiunea defectuoasă a unui profesor este un inconvenient
de natură pragmatică.
O deficienţă gravă a sistemului educaţional este slaba
atenţie acordată greşelilor semantice şi celor pragmatice.
Concentrarea exclusivă pe greşelile de natură sintactică se
explică prin considerente de comoditate. Fiind asociate cu
încălcarea unor reguli, greşelile sintactice pot fi încadrate
mai uşor în bareme aşa-zis obiective. Dar neglijarea
celorlalte dimensiuni semiotice reduce învăţarea la reguli,
procedee şi algoritmi, golind-o de înţelesuri, de
semnificaţii, de idei. Nu numai învăţarea, ci şi predarea pe
care o realizează un profesor este uneori victimă a acestui
reducţionism. Mulţi studenţi îi califică pe profesori după
uşurinţa cu care iau notiţe la cursurile lor. Un curs constând
exclusiv din procedee alcătuite din reguli riguros ordonate
este optim din acest punct de vedere, elevul sau studentul
transcrie pur şi simplu în caiet cele scrise de profesor la
tablă. De aici până la schizofrenia sintactică a atâtor
manuale şi culegeri de probleme nu este decât un pas. De
la dictonul heideggerian „Wissenschaft denkt nicht” până
la „The Schizophrenia of Contemporary Mathematics” al
lui E. Bishop, itinerarul este foarte clar.

251

