Rãni deschise

Cuvânt rostit la Adunarea Anuală
a Societăţii de Ştiinţe Matematice
Colegiul Naţional „Gheorghe Lazăr”
Bucureşti, 23 ianuarie 2010

Sigur că noi putem să discutăm cât de formidabilă
este Matematica, ce trecut glorios are Societatea de Ştiinţe
Matematice, să-i evocăm pe Lalescu, pe Ţiţeica, e foarte
frumos!
Dar problema principală, pentru care cred că suntem
noi aici, este în primul rând să ne dăm seama unde ne
aflăm cu educaţia matematică în acest moment, aici, în
România.
A nu se confunda educaţia cu cercetarea matematică
de performanţă. Acolo stăm bine!
Iată ce citim în „Viaţa lui Eminescu”, scrisă de
Călinescu:
„Nu-i plăcea să-şi înveţe lecţiile şi de aceea lua note
rele. În primul rând, nu se împăca cu matematicile.
De aceea se învoise cu Constantin Ivanovici, care a
şi devenit profesor de matematică, urmând ca acesta să-i
facă temele, iar el să-i spună poveşti”.
429

Solomon Marcus

Apoi reproduce următoarea mărturisire a poetului:
„Eu ştiu chinul ce l-am avut cu matematica în
copilărie, din cauza modului rău în care mi se propunea.
Ajunsesem să cred că matematicile sunt cele mai grele
ştiinţe de pe faţa pământului”.
Aici se opreşte citatul reprodus de Călinescu, dar iată
ce spune Eminescu mai departe, într-unul din manuscrisele
recent publicate (nr. 2258):
„În urmă am văzut că matematicile sunt cele mai
uşoare, desigur de o mie de părţi mai uşoare decât limbile,
care cer memorie multă.”
Şi acum vine ceea ce recomand ca slogan al Anului
Matematicii în Şcoala Românească:
„Matematicile sunt un joc cu cele mai simple legi
ale judecăţii omeneşti, numai că acele legi nu trebuie
puse în joc goale şi fără un cuprins.”
Acum, la 150 de ani de când Eminescu le-a enunţat,
lucrurile sunt extrem de actuale.
Rapoartele lui Eminescu din perioada când era
revizor şcolar seamănă izbitor cu rapoartele pe care le
făcea Spiru Haret, ministru al învăţământului.
Nu am reuşit să aflu dacă au comunicat vreodată între
ei, fiind contemporani. N-am reuşit să obţin un răspuns
nici în aula Academiei, unde erau prezenţi mulţi dintre
eminescologii noştri. Nimeni nu a ştiut să răspundă ceva
în ceea ce priveşte relaţia Eminescu – Haret.
430

Rãni deschise

Vedeţi prin urmare unde este rana care ar trebui să fie
în centrul atenţiei noastre şi în particular a Societăţii de
Ştiinţe Matematice.
Repet: „Numai că acele legi nu ar trebui să fie puse
în joc goale şi fără un cuprins.” Aşa pune Eminescu
degetul pe rană şi această rană stă deschisă şi astăzi.
Aici trebuie să fie centrul preocupărilor noastre şi nu
am văzut nici măcar un rând în planul de activitate ce ni
s-a prezentat care să aibă în vedere acest lucru esenţial.
Asta este problema noastră.
Asistăm la zeci de dezbateri pe probleme de educaţie,
cu gazetari, cu oameni politici, cu sindicalişti, dar nu cu
cei care sunt în linia din faţă a frontului educaţional, adică
dumneavoastră şi elevii dumneavoastră, pentru că asta este
esenţial, faţă în faţă profesor şi elev, ce se întâmplă în
clasă, ce se întâmplă în familie, ce se întâmplă la televiziune, care de multe ori e mai degrabă anti-educaţională
decât educaţională.
Stăm pe o insulă care are plăcerile ei, desigur, dar pe
noi ne interesează cum facem faţă puternicelor tendinţe
anti-educaţionale care apar de multe ori în familie, la
televiziune, pe stradă, pe internet. Chiar nu avem
capacitatea de a reacţiona la ele.
Suntem capabili să-i punem în mână elevului un
manual care să-i facă plăcere, să-l incite să umple paginile
cu părerile lui personale, nu să fie terorizat să-l ţină curat,
431

Solomon Marcus

ca să-l predea altcuiva în anul viitor, măsură care ar trebui
imediat abandonată. Fără educarea spiritului critic, nu e
posibilă o adevărată învăţare.
Am făcut aici o listă de tipuri de activităţi care ar
putea să facă obiectul unor interacţii profesor-elev. Le
propun, pentru că planul pe care l-am auzit aici că vi-l
propuneţi reprezintă numai cadre, dar foarte puţine
puncte s-au referit la conţinutul cu care să se umple
cadrele respective.
Iată o primă chestiune:
1. Se foloseşte sintagma „cultură matematică”
Dar nu e clar ce înseamnă asta.
Dacă vă uitaţi la toate cărţile pe care le vedeţi pe
tejghelele de afară, matematica pe care o vedeţi acolo
înseamnă exerciţii, nimic altceva. Şi eventual unele
definiţii care fac obiectul exerciţiilor. La asta se reduce
cultura matematică? Răspunsul meu este categoric negativ.
Cultura înseamnă şi teoreme, idei, o anumită istorie,
motivaţii şi semnificaţii, relaţii cu alte discipline. Dacă
rămânem numai la exerciţii, matematica va fi redusă la o
prestare de servicii, cum ar fi, de pildă, să rezolvăm
exerciţii pentru bacalaureat.
Ar trebui să se organizeze discuţii în care în centrul
atenţiei să fie exact ceea ce de obicei este ignorat sau
432

Rãni deschise

marginalizat. De pildă, să se discute despre o anumită
noţiune matematică, o anumită teoremă, cum au apărut ele,
care este legătura cu alte domenii etc.
Cât de interesant ar fi de urmărit traiectoria lor şi să
se discute aspecte ale gândirii matematice care nu intervin
în actul de predare de către profesor!
2. Interdisciplinaritatea matematicii
Aceasta înseamnă ca elevul să fie stimulat să nu mai
gândească pe discipline separate, ca acum, ci să le pună în
legătură.
Să luăm un domeniu, de pildă biologia; le putem
cere elevilor dintr-o parte a clasei să dea exemple de
chestiuni care vin dinspre biologie spre matematică, iar
elevilor din cealaltă parte a clasei să dea exemple dinspre
matematică spre biologie. Ar trebui făcut efectiv acest
dialog şi în prezenţa unui profesor de biologie, iar o
discuţie similară se poate organiza şi pentru celelalte
discipline: matematică-gramatică şi gramatică-matematică, de pildă.
O discuţie despre logaritmi ar fi extraordinară când
discutăm despre interdisciplinaritatea matematicii.
De pildă, în clasa a X-a, să luăm la rând toate
materiile şi să vedem cum a intervenit logaritmul.
Creşterea exponenţială, despre care se vorbeşte tot timpul,
ar putea face şi ea obiectul unei dezbateri.
433

Solomon Marcus

3. Să se facă lectura critică a unei pagini din
manual prin exerciţii de tipul: Unde e greşeala?
Iată un lucru care cred că este şi foarte amuzant
pentru elevi.
Să luăm o pagină din manual, sau o pagină scrisă
de un elev, sau chiar o pagină de la o probă de examen
şi să căutăm greşelile. Mă refer la greşeli sub orice
aspect, greşeală de logică, greşeală de istorie sau de
ortografie, de gramatică, de matematică etc. etc. etc.
Veţi avea surpriza să constataţi că multe greşeli nu
sunt văzute, în schimb, sunt suspectate a fi greşeli
lucruri care nu sunt de fapt greşeli, sunt greşeli
imaginare.
La concursurile de matematică sunt bareme care se
referă exclusiv la matematică. Dar am văzut că elevul
poate să facă cele mai mari greşeli de ordin gramatical,
istoric sau cultural, greşeli de orice fel, iar profesorul
spune: „Nu e treaba mea”! E grav dacă nu putem să
depăşim această mentalitate.
Sau un alt exemplu, care nu este din interiorul
matematicii, ci se referă la greşeli de gândire logicomatematică, să spunem la televiziune.
Ar fi interesant să li se ceară elevilor să
urmărească într-o seară o emisiune anume, iar a
doua zi să discute despre ce greşeli au găsit sub toate
aspectele.
434

Rãni deschise

4. De ce nu putem face matematică folosind numai
cuvintele? De ce nu putem face matematică lipsindu-ne
de cuvinte?
Aici e o chestiune foarte importantă.
Un raţionament nu se poate exprima decât în cuvinte.
Matematica şcolară are tendinţa de a reduce prezenţa
cuvintelor, chiar de a le elimina, or, noi avem nevoie de
amândouă.
M-am uitat în maculatoarele şcolarilor. Nu e deloc
respectat echilibrul dintre cuvinte şi simboluri pe care îl
preconiza Paul Halmos.
Să discutăm cu elevii chestiunea asta: de ce nu putem
face matematică folosind numai cuvinte, dar şi de ce nu
putem face matematică lipsindu-ne de cuvinte? Să
discutăm ambele aspecte.
5. În ce măsură suntem în stare să aplicăm
rigoarea matematicii la un text din afara matematicii?
Iată exerciţiul următor: să luăm o pagină din
manualul de gramatică şi să fie citită cu rigorile
matematicii; la fel, şi o pagină din manualul de istorie sau
de biologie.
6. Avem în matematică fapte care confirmă
aşteptarea intuitivă, altele care contrazic flagrant
aşteptarea intuitivă. Să dăm exemple din fiecare.
435

Solomon Marcus

7. O discuţie asupra unei reflecţii memorabile a lui
Poincaré: „Geometria este arta de a raţiona corect pe
figuri greşite”.
Ce înseamnă asta?
Dacă luăm primele afirmaţii din Elementele lui
Euclid: „Numim punct ceea ce nu are părţi. Numim linie
ceea ce nu are grosime”, constatăm imediat că punctul şi
linia sunt invizibile, dar noi le reprezentăm făcându-le
vizibile, adică greşit.
8. Ideea de „infinit” în matematica şcolară.
Unde, când şi în ce forme apare ea, la clasele mici,
chiar dacă nu se pronunţă cuvântul infinit.
9. Simbolismul matematic în mituri, folclor,
religie etc.
De pildă, ternaritatea în creştinism, semnificaţiile
cercului în mituri, semnificaţiile pătratului, pentagonului etc.
10. Cum se naşte o problemă din răspunsul la o
altă problemă.
De pildă, acest rezultat este un răspuns la o întrebare,
dar în ce măsură acest răspuns sugerează o nouă întrebare?
Insistăm pe răspunsuri pe care elevii trebuie să le dea la
întrebările altora, dar nu educăm capacitatea elevului de a
inventa întrebări interesante. Acest dezechilibru este
436

Rãni deschise

inadmisibil. Elevii nu sunt antrenaţi să inventeze întrebări
deştepte!
11. Interacţie elev-elev. Lucrul ăsta mi-a venit în
minte când am văzut ce se întâmplă în timpul grevelor
profesorilor, când se spune, de la sindicat: „Profesorul va
veni la şcoală, dar nu va preda. Elevii vor veni şi ei şi vor
sta cuminţi în bănci”.
Cred că perioada de grevă a profesorilor este un prilej
extraordinar ca elevii să dezvolte interacţia elev-elev. Să
se întrebe cu privire la manual: tu cum înţelegi aici, dar tu
poţi explica chestia asta?
Acesta este un foarte bun prilej pentru ca verbul „a
preda” să fie cât mai mult marginalizat.
12. Matematica pe internet.
Niciodată, din păcate, nu se face educaţia frecventării
internetului, care ar trebui să fie o chestiune esenţială la
toate clasele. Să fim preocupaţi, de pildă, ca elevii să înveţe
a folosi inteligent internetul, pentru a le creşte interesul
pentru matematică!
13. Noi vorbim mereu de gândire matematică. Dar
acesta este un tip de gândire generic pentru foarte multe
tipuri de gândire, pentru că ai gândire deductivă, ai
gândire probabilistă, ai gândire inductivă, ai gândire
437

Solomon Marcus

analogică (după unii, asta îl defineşte pe matematician),
apoi gândire metaforică, gândire infinită, gândire
algoritmică, gândire recursivă. Dar concret, nu aşa în
general, ci concret, în ceea ce am făcut, de pildă, în acest
an. Să vedem unde era un tip de gândire, unde altul ş.a.m.d.
14. Putem să abordăm şi lucruri mai filosofice, o
întrebare de pildă la care răspunsurile sunt foarte
variate: matematica asta pe care noi o învăţăm, ce este
ea, invenţie sau descoperire?
15. Poate cel mai important aspect al matematicii
este că ne dă posibilitatea să transgresăm vizibilul şi să
facem inteligibile multe lucruri care nu sunt vizibile.
Răspunsurile sunt diferite, dar cred că toate lucrurile
astea se pot discuta, repet, concret, pe materia pe care
elevii o învaţă.
Respectând cadrele pe care dumneavoastră le-aţi
propus – unde, ce, când –, trebuie să dăm miez, să dăm
fond lucrurilor.
Evident că toate astea le propun nu doar pentru anul
matematic, toate astea ar trebui să umple educaţia
matematică, dacă vrem ca ea să fie într-adevăr cultură.
Iar dacă oamenii de vârsta a treia vor fi întrebaţi:
„Domnule, din matematica şcolară ce îţi aminteşti?” – uite,
de pildă, o discuţie despre logaritm, cum traversează
438

Rãni deschise

logaritmul toate disciplinele: fizică, chimie, biologie,
psihologie, sociologie –, nu-şi amintesc nimic cultural!
„Era ceva cu logaritmi, cu sinus, dar n-am înţeles niciodată
ce era cu ele, ştiu că se manipulau acolo!...”
O discuţie despre logaritmi ar fi extraordinară când
discutăm despre interdisciplinaritatea matematicii.
Astea sunt câteva exemple despre felul în care văd
educaţia matematică, iar anul educaţiei matematice este un
bun prilej pentru a experimenta aceste lucruri!
În ceea ce priveşte reflecţia lui Moisil, referitor la
legătura dintre gândirea corectă şi matematică, cred că în
acest caz el nu a fost foarte inspirat.
Matematica nu poate pretinde a avea un monopol
asupra gândirii corecte.

439

