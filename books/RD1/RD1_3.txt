Rãni deschise

Notă editorială la prima ediţie

Această carte este rodul strădaniei de peste un an a
unor tineri entuziaşti din Editura Spandugino şi al
contribuţiei unor colaboratori de la reviste, şcoli şi alte
instituţii de învăţământ şi cultură, din mass-media, pe
parcursul adunării, transcrierii şi prelucrării acestui bogat
material documentar.
Sunt multe persoane cărora ar trebui să le aducem
mulţumirile şi profunda noastră recunoştinţă. Daniel
Cîrneanu, Smaranda Dinescu, Dorina Ivan, Beatrice
Lăpădat, Sabin Nicoară, Adina Oprescu, Mihai
Pătraşcu, prof. Doina Tănăsescu, Georgiana Vătafu,
Costin Vătafu, prof. Sorina Woinaroski sunt numai o
parte a lor.
Întreaga noastră gratitudine se cuvine să o adresăm
profesorului Solomon Marcus, pentru că ne-a îngăduit
accesul la o sursă documentară nepreţuită. Este vorba
despre caietele domniei sale, în care a păstrat, într-o
ordine desăvârşită, articole şi interviuri din perioada
25

Solomon Marcus

anilor ’70, ’80, ’90, dar multe şi după anul 2000, pentru
care fie că nu există arhive electronice, fie că
identificarea şi cercetarea lor ar fi necesitat probabil
încă mulţi ani de aici înainte.
aprilie 2011

Lavinia Spandonide

26

