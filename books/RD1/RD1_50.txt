Solomon Marcus

De la derapajele televiziunii la carenţele
programelor de învăţământ
Dilema veche, nr. 346,
30 septembrie-6 octombrie 2010

Observaţi ce bazaconii ortografice, sintactice,
semantice, logice etc. se strecoară uneori pe benzile
inferioare, în mişcare, ale ecranelor de televiziune? Mă voi
ocupa de una dintre ele, foarte proaspătă. Am ales-o,
pentru că îmi permite să identific o lacună fundamentală a
şcolii româneşti: programe de învăţământ prăfuite, rămase
în urmă faţă de vremea în care trăim.
Vineri, 17 septembrie seara, şi sâmbătă, 18 septembrie dimineaţa, Realitatea TV a transmis în mod repetat
următoarea informaţie: „Casa matematicianului
Babeş Bolyai din Timişoara nu poate fi reabilitată
deoarece nu se află pe lista monumentelor istorice.
Matematicianul Babeş Bolyai a descoperit teoria
geometriei neeuclediene.”
Las deoparte ambiguitatea: casa sau matematicianul
este din Timişoara? Ea putea fi evitată printr-o topică
336

Rãni deschise

adecvată: „Casa din Timişoara a matematicianului…” Las
deoparte şi faptul că o teorie nu este descoperită, ci creată.
De altfel, cuvântul teoria este aici inutil, parazit; se putea
spune simplu: Bolyai a creat geometriile neeuclidiene.
Dacă cineva îşi propunea să inventeze un text cât mai
semnificativ pentru starea actuală de incultură în care se
află o mare parte a populaţiei, greu ar fi putut să
nimerească unul mai potrivit decât cel de mai sus.
Dar faptul cel mai grav, generator de comic
involuntar, este inventarea unui matematician, pe nume
„Babeş Bolyai”, obţinut prin combinarea a două nume de
savanţi reali: Victor Babeş şi János Bolyai. „Babeş” nu este
deci prenumele marelui matematician „Bolyai”, cum crede
redactorul respectivului post de televiziune şi cum cred
poate mulţi alţii.
Mă bazez pe faptul că elevii nu află în şcoală despre
existenţa geometriilor neeuclidiene şi deci nici despre
autorii acestor geometrii. Nu ştiu care este situaţia lui
Victor Babeş, un pionier al bacteriologiei, coautor al
primului tratat de bacteriologie din lume, publicat la Paris.
Bănuiesc că e mai bună decât a lui Bolyai, bacteriologia
şi microbiologia par a fi mai cunoscute decât geometria,
dar mă tem că nici despre el nu prea se află în şcoală.
Redactorul cu pricina, în mod evident, este la fel de
ignorant în materie de Babeş ca şi în materie de Bolyai; el
auzise de Universitatea al cărei nume alătură cele două
337

Solomon Marcus

cuvinte, de unde a dedus că primul este prenume, iar al
doilea nume; faptul că între ele era o cratimă a trecut
neobservat sau pur şi simplu nu i s-a sesizat semnificaţia
(alt semn de incultură). Dar, cine ştie? Nu cumva
redactorul televiziunii nu a făcut decât să preia prostia
altuia? Această eventualitate a unei ştafete de bazaconii
(preluarea necritică a unei bazaconii tot bazaconie este)
agravează situaţia. Amploarea ignoranţei nu poate fi
evaluată, dar o putem bănui, din moment ce timp de atâtea
ore bazaconia a fost prezentă pe ecran. Dacă acum îl avem
pe matematicianul Babeş, mâine l-am putea avea pe
bacteriologul Bolyai, totul e posibil.
Apariţia geometriilor neeuclidiene în prima jumătate a
secolului al XIX-lea nu priveşte numai geometria. Acum ştim
că aceste geometrii au marcat, în ordine cronologică, literatura
(Dostoievski, Fraţii Karamazov), artele vizuale (a se vedea,
în special, cubismul), fizica (teoria relativităţii), logica (prin
punerea sub semnul întrebării a unor principii ale logicii lui
Aristotel: necontradicţie, terţ exclus), muzica (muzica atonală
a lui Schönberg), informatica (posibila dependenţă
a calculatorului de geometria spaţiului-timp asociat),
biologia (implicarea geometriilor hiperbolice în domeniul
„bio-computing”) şi psihologia (ipoteza caracterului
non-euclidian al spaţiului vizual şi modul de reprezentare a
spaţiului la copii), pentru a nu mai vorbi de matematică
(geometriile hiperbolice au devenit protagonistul programului
338

Rãni deschise

propus de William Thurston, laureat al Medaliei Fields, pentru
a asocia o structură canonică fiecărei varietăţi 3-dimensionale)
şi de filosofie (incompatibilitatea geometriilor neeuclidiene
cu viziunea lui Kant asupra spaţiului şi timpului a constituit
un factor de respingere, multă vreme, a noilor geometrii).
Absenţa lor din programa de liceu nu este un fenomen izolat.
Numeroase, poate cele mai multe din ideile mari ale
ştiinţei, acelea care au o pondere culturală deosebită şi un
grad înalt de atractivitate, sunt absente din această
programă. Elevii nu află nici despre bazele pitagoreice ale
muzicii (care-şi aveau locul natural încă în manualele de
gimnaziu, la matematică), nici despre revoluţia relativistă,
nici despe cea cuantică, nu află nimic despre logicile
neclasice, care proliferează acum în întreaga cultură. Mă
întreb cât află despre marile rezultate din domeniul
eredităţii, cum ar fi dubla elice Watson-Crick. Dar despre
marile idei din disciplinele informaţiei? Noţiuni ca
automat, maşină Turing, gramatică generativă le rămân
străine. Dar ideea de joc de strategie, esenţială pentru
înţelegerea tipurilor de conflicte şi de negocieri din lumea
contemporană? Această carenţă are o contribuţie majoră
la insuficienta atractivitate a programei şcolare actuale, la
insuficienta ei relevanţă.
Formaţi la o şcoală de un anume tip, vom fi tentaţi să
credem că aspectele menţionate ar depăşi posibilităţile de
înţelegere ale vârstei şcolare. Desigur, propunerile noastre
339

Solomon Marcus

alternative comportă, la un anumit moment, unele dificultăţi
tehnice şi subtilităţi care pot depăşi capacităţile mentale ale
elevilor. Dar multe dintre ideile importante reclamă o
înţelegere în trepte succesive, care pot începe la clasele
primare, gimnaziale sau liceale şi eventual continua la
universitate; nici acolo procesul de înţelegere nu se va
încheia. Un exemplu tipic în acest sens este noţiunea de
număr. „A înţelege” nu este un predicat binar, el comportă
diferite etape. Există, în fiecare dintre ideile de mai sus,
anumite prime etape de înţelegere care sunt accesibile, unele
chiar la gimnaziu, altele la liceu; dar, de exemplu, în ceea ce
priveşte înţelegerea ideii de infinit, ea poate începe chiar de
la clasele primare, cu exerciţii de tipul: Am o pâine, mănânc
azi jumătate din ea, mâine, jumătate din ce a rămas azi,
poimâine, jumătate din ce îmi va rămâne mâine ş.a.m.d.
După câte zile voi termina de mâncat pâinea?
Din modul în care este scris cuvântul „neeuclediene”,
se poate bănui că nici numele lui Euclid nu-i era cunoscut
redactorului cu pricina. Nu mai ştiu dacă numele lui Euclid
apare în manualele şcolare. O altă obiecţie posibilă ar fi:
Nu sunt şi aşa supraîncărcate programele şi manualele?
Unde mai este loc pentru adăugarea unor lucruri noi?
Numai că multe din lucrurile existente pot fi prescurtate
sau eliminate. Problema este nu una de cantitate, ci una de
perspectivă; accent pe date, reguli, procedee sau pe idei,
cultură, istorie, conexiuni?
340

