Solomon Marcus

Eu, cetăţeanul
Participare la emisiunea Eu, cetăţeanul, moderată
de Cătălin Sava, TVR1, 11 ianuarie 2011

Cătălin Sava: Astăzi a venit timpul să vorbim despre
tineri şi despre viitor, despre proiecte şi despre
încremenirea în proiect [...] Despre logică şi matematică o
să vorbim astăzi cu academicianul Solomon Marcus.
Domnule academician, vă mulţumesc că aţi venit.
Solomon Marcus: Nu despre logică şi matematică
vom vorbi astăzi, ci despre rana deschisă, cu cel mai mare
număr de victime în materie de educaţie, şi anume despre
scenariul elev-educator, elev-părinte.
[...]
C.S.: Vom vorbi despre acele burse guvernamentale
care au pornit prin 2004-2005, s-au cheltuit 5 mil. de euro,
şi au fost şcoliţi 150 de tineri pentru a deveni specialişti.
[...] De ce nu ştim ce să facem cu specialiştii, Solomon
Marcus?
S.M.: Aţi început discuţia cu un proiect care priveşte
câteva sute de specialişti, dar mie mi se pare că are
906

Rãni deschise

prioritate un altul, care priveşte câteva sute de mii de copii,
şi anume ce se întâmplă cu copiii de vârstă preşcolară şi
şcolară. Am aici manualele de limba română şi matematică
pentru clasa a III-a primară şi por să spun un lucru
constatat şi direct, în şcoală. Întregul scenariu elevprofesor, elev-părinte este un scenariu bolnav, pentru că
este bazat în primul rând pe constrângere, descurajează
nevoia şi plăcerea de a înţelege lumea, descurajează
atitudinea interogativă în faţa lumii, înlocuind-o cu una
preponderent imperativă.
C.S.: E o şcoală de dresaj.
S.M.: Iată ce găsim într-o revistă şcolară recent
apărută: „Nu este frumos să spui NU” – e scris negru pe
alb. Ceea ce vă spun sunt lucruri de pe teren, văzute de
mine în şcoli. Am fost întâmpinat într-o şcoală de copii
care intonau în cor: „Noi suntem copii cuminţi”. Erau copii
de clasa a V-a, pe care i-am întrebat: „Eu nu-mi amintesc
definiţia copilului cuminte şi am să vă rog pe voi să mi-o
spuneţi.” Definiţia propusă a fost următoarea: „Un copil
cuminte nu vorbeşte neîntrebat.”Aici sunt rănile grave ale
educaţiei. Problemele de bază ale educaţiei au ca
protagonişti pe oamenii şcolii.
[...]
C.S.: Ce facem cu minţile astea luminate? Avem o
cercetătoare în chimie care a fost premiată de Obama,
907

Solomon Marcus

avem o grămadă de olimpici internaţionali. Ce facem cu
minţile astea luminate?
S.M.: Să ştiţi că noi beneficiem mult de pe urma lor,
chiar dacă rămân în Occident. Pot să vă spun că mulţi
dintre aceşti români, care lucrează de decenii în
universităţi americane sau vest-europene, au reuşit să
aducă nenumăraţi tineri bursieri români în Occident, să-i
ajute în primii paşi în cercetare şi în afirmare. Este vorba
de un metabolism foarte fin şi complex. Lucrurile nu sunt
în alb sau în negru.
C.S.: Sunt de acord că pentru ei e bine să plece şi să
studieze, dar pentru noi oare e bine?
S.M.: Toţi marii noştri oameni, care au deschis
drumuri noi în ştiinţă, au făcut o baie de Occident,
şi-au făcut doctoratele acolo unde era centrul ştiinţific al
lumii în acea vreme, la Göttingen sau la Sorbona.
C.S.: Cu diferenţa că atunci ei se întorceau, iar acum
nu se mai întorc.
S.M.: Nu se mai pune acum astfel problema pentru
că, repet, locul geografic în care te afli a devenit mai puţin
important. Totul s-a globalizat.
[...]
Florin Iaru: La facultăţile de vârf, la facultăţile de
specialitate, vin de pe băncile şcolii oameni tot mai prost
908

Rãni deschise

pregătiţi. Ei intră în facultate cu medii tot mai mici. Cei
care, acum 10 sau 20 de ani, nu ar fi trecut pragul facultăţii,
acum o absolvă. Ies nepregătiţi şi asta e o foarte mare
problemă.
S.M.: Aici se face în general o confuzie. Eu am făcut
liceul între cele două războaie mondiale, când nu numai
universitatea, ci chiar liceul avea un caracter elitar, nu
popular. Numărul de licee dintre cele două războaie era mult
mai mic decât cel al universităţilor de azi. Or, această trecere
de la elitar la popular trebuia plătită prin degradarea calităţii.
F.I.: Eu înţeleg că se plăteşte, dar nu mi se pare
normal s-o plătim astfel. Nu cumpăr orice, mi se pare
normal să pot alege.
[...]
Andreea Paul: Întrebarea fundamentală este cum să
procedăm pentru realizarea unor proiecte. Aici apar unele
probleme.
S.M.: Este iluzoriu să aruncăm un boom spectaculos
Părerea mea este că trebuie identificate probleme
punctuale, cum ar fi calitatea manualelor şi a programelor.
Trebuie făcute, de exemplu, concursuri pentru manualele
de la clasele primare. Şi aceasta pentru că sunt prăfuite, sunt
o nenorocire, sunt lipsite de imaginaţie. Aceste manuale
puteau fi publicate şi în urmă cu 50 de ani. Contextul în
care trăiesc copiii astăzi e complet ignorat. Se pune
909

Solomon Marcus

problema să intre în educaţia şcolară şi frecventarea, în
condiţii sănătoase, a internetului. Chiar existenţa orelor de
dirigenţie e un semn clar al unui sistem complet prăfuit,
pentru că diriginţi trebuie să fie toţi profesorii şi fiecare
dintre ei, indiferent de materia pe care o predă, trebuie să
fie în stare să conecteze manualul şi predarea lui în clasă
cu valorificarea a ceea ce e mai bun pe internet.
Un profesor de matematică de la Colegiul Naţional
Iaşi, fost olimpic, cu studii de specializare în Occident,
explică faptul că a optat să rămână în ţară. Crede că
opţiunea lui a fost bună. E profesor la şcoala în care a
învăţat el însuşi şi îşi face meseria cu plăcere. Profesorul
e filmat în timpul unei ore de curs.
S.M.: Din păcate, exact ceea ce profesorul lucra la
tablă, calculul primitivelor unei funcţii complicate, este un
exemplu clar de matematică prăfuită. Toată programa de
clasa a XII-a, unde se fac lucrurile astea, este o programă
complet învechită. Acesta este scenariul pe care trebuie
să-l schimbăm, al profesorului care scrie la tablă şi al
elevilor care iau notiţe în bănci.
[...]
F.I.: Copiii termină clasa a XII-a şi, dacă îi întrebi,
80-85% au citit maximum o carte. Ce fel de creativitate
este asta dacă nu ai din ce să creezi?
910

Rãni deschise

S.M.: Şcoala transmite în primul rând procedee de lucru,
reţete, date, fie calendaristice, fie nominale, ca la concursurile
acelea, „Vrei să fii miliardar?”. Şcoala nu formează
intelectuali. Atitudinea faţă de greşeală este esenţială. Asta
pentru că noi nu acordăm atenţie greşelii ca preţ inevitabil pe
care trebuie să-l plăteşti, dacă vrei să-ţi formezi o gândire
personală...
C.S.: Dar noi facem aceeaşi greşeală de vreo 20 de ani.
[...]
S.M.: Vă rezum, în patru cuvinte, starea actuală a
vieţii şcolare. Se cultivă foarte mult corectitudinea stearpă,
se ignoră fecunditatea unor greşeli. Aşadar, e un
învăţământ axat în primul rând pe aspecte sintactice,
aspecte care pot fi evaluate în termeni de abateri de la
reguli. Iar evaluările se fac după bareme. Ca să vă daţi
seama unde e rana, priviţi cum arată baremele după care
se evaluează lucrările. Sunt, pur şi simplu, caraghioase.
[...]
C.S.: Îi dăm cuvântul domnului profesor Solomon
Marcus.
S.M.: Mă tem că accentul excesiv pe care l-aţi pus pe
învăţământul universitar şi postuniversitar abate atenţia de
la faptul că frontul principal în educaţie nu-l constituie nici
parlamentul, nici guvernul, nici partidele politice, nici
presa, ci oamenii şcolii.
911

Solomon Marcus

C.S.: Cum îi facem să înţeleagă asta pe cei care
decid?
S.M.: Iniţiativele principale trebuie să vină de jos.
Sigur că, în vindecarea acestor răni pe care eu le-am
semnalat, au un rol stimulativ şi factorii de putere, dar rolul
principal îl au oamenii şcolii. Acolo e factorul principal.

912

