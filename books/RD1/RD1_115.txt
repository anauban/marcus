Rãni deschise

Întâlnire cu oamenii Şcolii „Al. I. Cuza”
cu ocazia celor 150 de ani de la înfiinţare
Bacău, 22 aprilie 2010

În sala de festivităţi a Colegiului Naţional „Ferdinand I”
din Bacău are loc o festivitate cu prilejul întâlnirii dintre foşti
şi actuali profesori ai şcolii, foşti şi actuali elevi. Invitat de
onoare este prof. acad. Solomon Marcus, fost elev al acestei
şcoli.
Deschide manifestarea doamna director Doiniţa Sîrbu,
urând bun venit celor de faţă. Urmează un program artistic
prezentat de elevi ai şcolii. Dintre recitările elevilor, spicuim:
– „În universul nesfârşit sunt o rază de lumină.”
– „După ploaie croiesc un curcubeu.”
– „Ca să se lege zare de zare în cântec şi iubire.”
– „Ghiozdanul plin, plin îmi aminteşte că şcoala ia
locul copilăriei.”
– „Şcoala este dor de copilărie.”
– „Acum, cu nostalgie privesc capitolele vieţii mele,
ce au fost învăluite în negura timpului, îmi privesc
gândurile şi speranţele cu patru trepte mai sus, dar
1181

Solomon Marcus

întotdeauna va exista o persoană peste care nu voi trece,
doamna învăţătoare.”
– „Am lăsat acasă toate jocurile (se aude un oftat).
Secunde, zile, ani în clepsidra toamnei.”
– „Vise, întrebări, care acum au un răspuns, aşa apar
anii ce au trecut, căci doamna învăţătoare ne-a condus spre
înţelepciune.”
Sunt lucruri recitate, citite de elevi din clasele a III-a
şi a V-a.
După programul artistic se oferă diplome foştilor
profesori ai şcolii şi dlui acad. Solomon Marcus.
Redăm cuvântul de mulţumire al acad. Solomon Marcus:
„Probabil că vă este cunoscută o emisiune de pe
canalul Cultural al televiziunii române, a lui Grigore Leşe,
«La Porţile Ceriului». E o emisiune splendidă, pe care el
o prezintă astfel: Vreţi să scăpaţi măcar Duminică de lumea
asta smintită? Veniţi la Porţile Ceriului!
Mi-am amintit de vorbele lui, urmărindu-i pe aceşti
copii, pentru că şi eu în faţa lor am simţit acelaşi lucru.
Când eram de vârsta acestor copii, prin anii ’30 ai secolului
trecut, şi am simţit că s-a încheiat copilăria mea, am avut
sentimentul unei pierderi ireparabile, dar, mai târziu,
mi-am dat seama că, în bună măsură, succesul meu în
viaţă, randamentul meu profesional va depinde în bună
măsură de modul în care voi reuşi să recuperez măcar o
parte din prospeţimea copilăriei. În asta constă o bună
1182

Rãni deschise

parte a succesului, a reuşitei în viaţă. Deşi organismul
nostru suferă o uzură inevitabilă, noi putem prin forţele
noastre intelectuale, sufleteşti, spirituale să recuperăm
această prospeţime pe care o vedem în ochii acestor copii
şi pe care, dacă o pierdem, ajungem în situaţia tristă în
care, din păcate, se găsesc mulţi tineri de azi.
Referindu-se la această generaţie, mulţi psihologi afirmă
că o parte a ei suferă de o îmbătrânire timpurie. Nu trebuie
să lăsăm ca prospeţimea din ochii acestor copii să devină
o simplă rutină. Spun mereu atunci când mă întâlnesc cu
oamenii şcolii că eu nu m-am plictisit niciodată şi că starea
de plictiseală e un simptom al eşecului.
Mulţi şcolari, întrebaţi ce simt ei în anii de şcoală, în
clasă cu profesorii lor, folosesc cuvântul „plictiseală”, şi
asta e grav. Aceşti copii, pe care i-am văzut aici, n-au
apucat să se plictisească. Se vede asta în ochii lor şi de noi
depinde ca această bucurie să se păstreze.
Învăţătorii, cei care lucrează cu aceşti copii, ar trebui să
fie consideraţi, social, mai importanţi decât profesorii de
liceu şi cei universitari, pentru că, dacă ei eşuează, elevii lor
nu vor mai recupera ulterior aceste pierderi. Există un binom
în care primul termen e plictiseala şi al doilea, care apare
inevitabil, pentru a scăpa de ea, e distracţia. Să fugim de
această plictiseală prin distracţie. Impresia mea este că elevii
dumneavoastră n-au ajuns în această situaţie, dar riscă să
ajungă. Aceasta depinde de profesorii lor, dar şi de părinţi,
1183

Solomon Marcus

de mediul din care ei provin. Acestea sunt de fapt adevăratele
probleme ale educaţiei, şi nu acelea la care se referă mereu
diverse persoane la emisiunile televizate, de pildă dacă liceul
trebuie să aibă trei sau patru clase. Dialogul dintre profesori
şi copii, cel dintre părinţi şi copii, acestea sunt scenariile în
care nu avem dreptul să eşuăm. Sunt scenarii esenţiale şi,
dacă eşuăm, va fi foarte greu să reuşim ulterior. De aceea vă
sunt foarte recunoscător că m-aţi invitat aici.
Aceste clipe petrecute între copii sunt un izvor de
energie şi noi trebuie să căutăm mereu să ne întâlnim cu
ei. O atenţie specială trebuie acordată televiziunii şi
internetului, dar deocamdată nu se face educaţia
frecventării lor. Ei trebuie să afle ce, unde şi când se găsesc
emisiuni interesante, ce să caute şi cum pe internet, pentru
că fără aceste unelte nu se mai poate trăi într-o Românie
modernă. Internetul e o mare metropolă, ca Parisul sau
New York-ul. Se găsesc de toate acolo, de la cele mai urâte
sau nocive lucruri, până la unele splendide. Dar, pentru a
putea profita de asta, trebuie să fii educat.
Am devenit elev în această şcoală la vârsta de 6 ani
şi jumătate, dar şcoala avea pe atunci doar patru clase.
Pentru următoarele patru clase am înaintat pe calea Oituz
şi am trecut la liceul „Ferdinand I”. Clasele V – VIII de
astăzi erau primele clase de liceu pe atunci şi-mi aduc
aminte cu emoţie că la acea vârstă, în primele clase de
liceu, am descoperit poezia.
1184

Rãni deschise

Această vârstă a claselor primare şi a gimnaziului, cu
care lucraţi dumneavoastră aici, este vârsta la care greşelile
făcute în educaţie au de multe ori efecte ireversibile.
Abandonul şcolar, de care se vorbeşte mereu, la această
vârstă a gimnaziului se produce.
Mă găsesc aici cu dumneavoastră din cauza unei
întâmplări triste. Trebuia să fiu în aceste zile la Paris, la o
masă rotundă, şi scrisesem doamnei directoare o scrisoare
în care îi comunicam că nu pot să vin. S-a întâmplat însă
ca un nor jucăuş să anuleze aproape toate zborurile cu
avionul, inclusiv cel cu care trebuia să plec eu, iar
preşedintele Academiei Române nu s-a putut întoarce la
timp din Suedia, motiv pentru care s-a anulat o adunare ce
trebuia să aibă loc azi-dimineaţă, şi astfel am putut ajunge
aici la timp. Cu această ocazie, mi-am amintit un poem în
proză al lui Charles Baudelaire, care, fiind întrebat ce
iubeşte el cel mai mult, a răspuns:
J’aime les nuages, les nuages qui passent, là bas, là
bas, les merveilleux nuages.
Şi iată că acum, în afară de norii de ploaie cu care
suntem atât de familiarizaţi, am descoperit că mai există
şi altfel de nori, la fel de imprevizibili şi de inefabili ca şi
norii de ploaie.
Venind aici, am constatat că pe vremea mea toţi
copiii ştiau să recite măcar câteva poezii şi că, astăzi,
acest lucru nu se mai întâmplă. Noua generaţie nu mai
1185

Solomon Marcus

are faţă de poezie emoţia pe care o aveau cei de vârsta
mea. Mi-aduc aminte că, la vârsta copiilor pe care i-am
văzut aici, noi învăţam poezii extraordinare, ce ne-au
fermecat copilăria: Şi-a venit un negustor, plin de bani,
cu vâlvă mare cumpără copii pe care nu-i iubeşte mama
lor.
Nu ştiu dacă am reprodus corect nişte versuri de
Coşbuc. Azi nu se mai ştiu asemenea lucruri. Înainte de a
intra în şcoală, am întâlnit un copil pe care l-am rugat
să-mi spună o poezie; nu s-a descurcat. Există un deficit
de emoţie, de lirism la noile generaţii şi cred că ar trebui
să salvăm acest fond emoţional, dacă nu cumva acest
fenomen are rădăcini mai profunde. Eu cred că, oricât de
multe lucruri ar învăţa un elev, fără poezie viaţa e searbădă.
Nu poţi să concepi să trăieşti fără ca, în unele momente,
să simţi nevoia să te refugiezi în marea poezie a lumii.
Chiar şi faţă de Eminescu, copiii manifestă o atitudine de
o anumită indiferenţă. N-am văzut o legătură afectivă
profundă.
Cred că ar trebui să reflectăm asupra acestor lucruri.
Am constatat, ca rezultat al întâlnirilor mele cu oamenii
şcolii, că există un deficit de afectivitate şi de interes în
a afla cum e lumea, de a înţelege ce se întâmplă în jurul
nostru, de a ne înţelege pe noi. Dragostea de lectură mi
se pare mult mai redusă decât aceea pe care o simţeam
eu. Sigur că e o diferenţă mare, de care trebuie să ţinem
1186

Rãni deschise

seama. Când eu eram elev, erau aşa de puţine licee, mai
puţine cred decât numărul universităţilor din România
de azi. Pe atunci şi liceele aveau un caracter elitar, nu
doar universităţile. Calitatea de profesor de liceu, cu
examen de capacitate, se obţinea foarte greu, iar lista
elevilor care absolveau gimnaziul şi reuşeau la examenul
de admitere în clasele superioare de liceu era trecută în
ziarul local din Bacău. Bacalaureatul era considerat o
performanţă. Astăzi trebuie să facem faţă unei situaţii
mult mai dificile, învăţământul a devenit de masă, iar
diferenţierea valorilor e mai complicată. Există, de
asemenea, o mentalitate păguboasă, prea mulţi părinţi
consideră drept ceva degradant pentru copiii lor faptul
de a nu obţine o diplomă universitară.
Această mentalitate nu există în alte părţi ale lumii.
Am avut colegi în Occident care îmi spuneau: „Vă
recomand pe fiul meu, un excelent bucătar” şi-l lăudau
pentru priceperea sa. Acum, cei mai mulţi părinţi consideră
că nu e un lucru demn ca odrasla lor să nu aibă o diplomă
universitară.
Acestea sunt problemele grave ale învăţământului şi
lor li se datorează principalele boli de care suferă tineretul,
faptul că ei nu mai descoperă minunile de care sunt
înconjuraţi, se plictisesc din incultură, lor nu li se
descoperă marile comori ale ştiinţei şi, trăind într-un
univers atât de sărac, ajung la tipul de distracţii atât de
1187

Solomon Marcus

jalnice, de felul celor transmise mereu pe diverse canale
de televiziune.”
După discuţiile şi amintirile profesorilor, întâlnirea se
încheie cu altă intervenţie a prof. Solomon Marcus:
„În încheiere, aş vrea să vă spun că persoana pe care
o invidiez cel mai mult din această sală este doamna
directoare Doiniţa Sîrbu. Când am văzut cu câtă dragoste
se poartă cu aceşti copii, abia treziţi la viaţă, mi-am spus
că e o fericire să trăieşti în acest mediu de prospeţime,
de care cei mai mulţi oameni duc lipsă în societatea
noastră de astăzi. Când am văzut gesturile de dragoste
faţă de aceşti copii, mi-am dat seama cât de puternic se
înşurubează acestea în afectivitatea lor. Această lipsă de
plictiseală în comportamentul dumneavoastră îi
modelează; să sperăm că şi acasă se bucură de o
protecţie asemănătoare. Din păcate, mulţi dintre colegii
noştri nu-şi pot păstra această prospeţime şi intră în
rutină, or, educatorul e ca un actor. Şi atunci când
interpretează a suta oară „O scrisoare pierdută” de
Caragiale, actorul trebuie s-o interpreteze ca şi cum ar
face-o pentru prima oară. La fel e şi cu educatorul, care
trebuie să-şi retrăiască aceleaşi sentimente cu fiecare
nouă generaţie de copii. În aceasta constă secretul,
în capacitatea de a trăi fiecare dimineaţă ca un nou
început, de a-şi scutura rutina şi de a porni din nou cu
prospeţime la drum.
1188

Rãni deschise

E lucrul care m-a marcat cel mai mult în întâlnirea de
astăzi.
Aceasta este clasa în care am fost elev între anii
1931 şi 1935. În clasa noastră exista o singură fată, fiica
învăţătorului nostru Davidescu, pentru că era şcoală de
băieţi: „Şcoala primară de băieţi nr. 2” Acum e o şcoală
cu clasele I – VIII, deci, în afară de cele patru clase, mai
are încă patru, care pe vremea mea erau primele clase de
liceu, pe care le-am urmat la Liceul „Ferdinand I” din
apropiere. Băncile erau, desigur, altele. În fiecare bancă
stăteau doi, uneori trei elevi şi aveam la fiecare colţ al
băncii câte o adâncitură, în care introduceam călimara,
(pe atunci se scria cu condei muiat în cerneală), ne
temeam să nu vărsăm cerneala pe caiet şi să fim
pedepsiţi.
Această spaimă nu mai există astăzi; nu mai există
călimară, nici condei, azi se scrie cu pixul. Noi dădeam
însă atenţie caligrafiei, scrisului frumos, atenţie care
astăzi s-a pierdut. Mai erau şi alte emoţii care astăzi au
dispărut.
Aceasta e şcoala mea dragă, mai puternică în
amintirea mea decât orice altă şcoală ulterioară. (şterge
tabla). Iată, acesta era gestul care pregătea ceea ce urma
să scriem pe tablă şi aşteptam verdictul învăţătorului,
care trebuia să aprobe sau să dezaprobe corectitudinea
celor scrise.”
1189

Solomon Marcus

***
Vineri, 23 aprilie 2010, a avut loc o întâlnire cu
profesorii şi elevii în sala de festivităţi a Colegiului Naţional
„Ferdinand I”.
Copiii, în cor:
– Bună ziua şi bine aţi venit la noi!
După un scurt program artistic prezentat de elevi
câştigători la concursuri artistice, dl dir. adj. se adresează
prof. Solomon Marcus:
– În această sală se găsesc elevi şi foşti elevi ai şcolii,
multipli olimpici, cu premii obţinute la diverse competiţii,
care vor şi ei să vă adreseze întrebări. Nu sunt întrebări
pregătite, o să fie naturale, în acelaşi spirit tineresc de care
ne vorbeaţi ieri. Îi invit pe copii la un asalt al ideilor. Începe
tu, Alex, că eşti cel mai mare.
– Sunt elev în clasa a VIII-a. Eu şi colegii mei avem
o curiozitate. Care este cheia succesului dumneavoastră?
– Nu-mi place foarte mult felul în care aţi formulat
întrebarea şi am să vă spun de ce. Mai întâi, păreţi siguri că
am avut succes, iar apoi nu-mi prea place cuvântul „succes”,
pentru că de multe ori succesul nu corespunde şi valorii. Se
întâmplă de multe ori ca mari valori să nu aibă succes, pentru
că noi înţelegem prin succes un anumit ecou în mass-media
– să se vorbească de tine la televizor, să scrie în presă. Eu
vin dintr-un domeniu, din una dintre cele mai sobre ştiinţe,
matematica, în care marile valori nu au succes în sensul
1190

Rãni deschise

social al cuvântului, nu apar la televizor, nu sunt băgate în
seamă de presă. De aceea, întrebarea dumneavoastră cu cheia
succesului m-a deranjat. S-a întâmplat ca eu să beneficiez de
unele invitaţii la televiziune, să dau multe interviuri, dar
situaţia nu e tipică. Am colegi, mari matematicieni, care sunt
apreciaţi de comunitatea profesională internaţională şi despre
care publicul nu ştie nimic.
Eu compar uneori cultura cu luna, care nu-şi arată
decât o singură faţă. Aşa e şi cultura, îşi arată mereu faţa
ei aşa-zis umanistă: muzica, artele vizuale, literatura. E rău
însă că nu-şi arată şi cealaltă parte, aceea corespunzătoare
matematicii, fizicii, chimiei, biologiei. Ele sunt partea
nevăzută a culturii, care apare în lume, în mass-media doar
întâmplător. Eu nu pun bază prea mare pe ceea ce spun
gazetarii despre mine.
Nu spun că nu contează deloc, pentru că trebuie să
avem o legătură cu oamenii de rând, cu publicul, dar
atenţia principală se îndreaptă spre reacţia celorlalţi
matematicieni faţă de mine. De aceea trebuie să faceţi
distincţie între valoare şi succes. Se întâmplă uneori să aibă
succes şi un om de o autentică valoare, dar adesea au
succes social mediocrităţi, în timp ce mari valori rămân
necunoscute publicului.
Am să vă povestesc o întâmplare comică, petrecută în
urmă cu vreo şase ani. Imitând un concurs organizat de un
post de televiziune, un ziar local a luat iniţiativa organizării
1191

Solomon Marcus

unui sondaj cu tema: „Care este cel mai mare băcăuan?”
S-a obţinut astfel o listă în care apăreau cântăreţi de muzică
uşoară, ziarişti, oameni de ştiinţă etc. şi e evident ce s-a
întâmplat. Îmi amintesc că un ziar local mă ironiza: „Iată
că Solomon Marcus mai are mult până să ajungă la scorul
Loredanei Groza”. E, desigur, o provocare căreia nu-i pot
face faţă, dar nici nu-mi propun, pentru că avem un public
diferit. Eu nu cred că trebuie să aveţi succesul ca scop în
viaţă, dar, dacă el apare, e foarte bine. Pentru că trebuie să
ştiţi că noi suntem ceea ce alţii spun despre noi că suntem.
Nu există valoare în sine şi fiecare poate crede despre sine
că este un geniu. Se întâmplă asta uneori, mai ales în
poezie, dar societatea are sistemele ei de evaluare. În fiecare
domeniu există criterii de evaluare stabilite la nivel
internaţional, pe care trebuie să le accepte.
Iată, voi trebuie să daţi teze, examene, ca să terminaţi
şcoala şi să intraţi la liceu, să susţineţi un examen de
bacalaureat etc. Iar apoi, toată viaţa, ca să puteţi promova
pe trepte superioare în domeniul profesional, trebuie să
treceţi prin diverse sisteme de evaluare, sisteme create de
societate. Trebuie să vă obişnuiţi cu asta, mai ales că uneori
evaluarea pe care ne-o fac alţii nu coincide cu aceea pe
care ne-o facem noi.
Referitor la întrebarea dumneavoastră, primul
moment în care am avut succes şi ecou în rândul
specialiştilor a fost la sfârşitul anilor ’50 din secolul trecut.
1192

Rãni deschise

Am început să mă ocup de un lucru pe care mulţi l-au
considerat un lucru trăznit, un fel de nebunie – lingvistica
matematică.
Am încercat să apropii, să creez o căsătorie între două
discipline pe care toată lumea le credea depărtate una de
alta, fără nicio legătură între ele. Una era lingvistica, adică
ceea ce se învaţă la limba română: limba, gramatica,
sintaxa, morfologia, şi alta era matematica. În şcoală nu se
vede nicio legătură între ele, aşa am fost cu toţii învăţaţi,
generaţie după generaţie.
Când am început să mă ocup de acest lucru, şi nu
era întâmplător, pentru că apăruseră în toată lumea
asemenea preocupări, reacţia multora dintre colegii mei
a fost de surpriză. Dar aveam în acel moment la activul
meu câteva zeci de articole de analiză matematică,
zestrea mea ca matematician era clară şi convingătoare,
astfel încât comunitatea matematicienilor nu m-a respins,
văzând noile mele preocupări. Am fost sprijinit şi de mari
profesori clarvăzători din matematică, Gr. C. Moisil, şi
din lingvistică, Al. Rosetti, şi în acest fel mi-am dat
seama cât de mult contează să beneficiezi de sprijinul
unor mari maeştri şi să ştii să lupţi contra curentului.
Orice idee nouă începe ca un paradox, devine o banalitate
şi sfârşeşte ca o prejudecată, observa Moisil. Cheia
principală a succesului în profesie (nu neapărat în massmedia) este să ai încredere în gândirea ta, în iniţiativele
1193

Solomon Marcus

tale, în măsura în care le poţi susţine într-un mod
convingător, chiar dacă lucrul ăsta este împotriva
mentalităţii dominante din acel moment.
– Ce părere aveţi despre tinerii care vor să studieze
în străinătate?
– Eu cred că trebuie să ne modificăm modul de
reprezentare a străinătăţii, iar întrebarea dumneavoastră
atinge aspecte foarte delicate, foarte controversate, dar
foarte importante.
Cine a realizat marea Revoluţie de la 1848 în Ţările
Române? Ea a fost realizată de intelectuali români care
au studiat în străinătate. La fel a fost şi cu începuturile
vieţii universitare în România. Mari profesori care au
studiat la universităţile din Europa s-au întors în ţară şi
au organizat învăţământul. Înainte de a veni să facă
reforma în ţară, Spiru Haret şi-a susţinut doctoratul la
Sorbona în anii ’70 ai secolului al XIX-lea. La fel a fost
şi cu Traian Lalescu.
România a avut o mare întârziere în dezvoltarea ei,
faţă de Occident, ca urmare a unor împrejurări istorice pe
care ar trebui să le aflaţi de la şcoală. A devenit astfel
natural ca noi să învăţăm, la început, de la cei mai avansaţi
ca noi. Situaţia s-a schimbat în bună măsură, dar s-a
schimbat şi ideea de străinătate, pentru că noi vorbim
astăzi de integrarea europeană. Eu cunosc mai bine situaţia
din matematică. Foarte mulţi matematicieni au plecat în
1194

Rãni deschise

Occident să studieze; şi asta s-a întâmplat şi în vremea
comunismului, şi după 1989, iar dacă facem un bilanţ,
rezultatul este foarte bun. Mulţi consideră că i-am pierdut
pe cei care au rămas şi şi-au făcut o carieră în alte părţi,
dar eu nu cred că se mai poate discuta în aceşti termeni.
Cunosc bine situaţia din matematică şi din informatică.
Mulţi matematicieni de valoare din România s-au
stabilit în mari centre din Europa Occidentală, din America
sau Canada. Mulţi dintre ei se află într-o interacţiune
permanentă cu cei din ţară, vin periodic în România, ţin
cursuri, au doctoranzi, participă la întâlniri internaţionale
care au loc în ţara noastră. Chiar aici, lângă Bacău, la
Slănic Moldova, au fost recent două întâlniri internaţionale
de semiotică, organizate de Universitatea din Bacău, la
care au participat cercetători veniţi din toate colţurile lumii,
de la Australia până în Canada. Vă daţi seama deci cât de
tare s-a descentralizat viaţa culturală. Aceşti cercetători
români, plecaţi în Occident, obţin nenumărate burse de
studii pentru românii din ţară, schimbul dintre cei din afară
şi cei din ţară e atât de puternic, încât e foarte greu să mai
distingi între aceşti cercetători.
Astăzi, cultura se elaborează şi se evaluează la nivel
internaţional în toate domeniile, de la muzică la
matematică sau filosofie. Globalizarea nu se referă doar la
economie, ea se referă şi la cultură, pentru că s-a globalizat
cunoaşterea, cad frontierele dintre discipline.
1195

Solomon Marcus

Marele proiect al genomului uman s-a realizat prin
colaborarea biologiei cu informatica, cu lingvistica, cu
matematica. De asemenea, s-a globalizat comunicarea. În
câteva secunde putem trimite un mesaj la celălalt capăt al
lumii. Iată, aveţi ca exemplu întâmplarea cu norul vulcanic
din Islanda, care a avut impact asupra întregii planete,
pentru că s-a văzut că ceea ce se întâmplă la un moment
dat într-un anumit loc poate influenţa lucrurile în cu totul
alt loc de pe pământ.
S-a spus că planeta a devenit un sat, adică suntem azi
atât de interconectaţi unii cu alţii, la fel ca oamenii din
acelaşi sat, care se cunosc şi comunică foarte uşor între ei.
E foarte bine că mulţi români studiază în străinătate şi
iată, s-a văzut că, de 200 de ani, marile înnoiri în România au
venit de la români care studiaseră în Occident. La fel a fost şi
pe vremea când Titu Maiorescu şi apoi E. Lovinescu cereau
tocmai acest lucru. Acum funcţionează în Bucureşti Şcoala
Normală de Studii Superioare în Matematică, de pe lângă
Institutul de Matematică al Academiei, creată după modelul
francez Ecole Normale Supérieure, şi a apărut şi procesul
invers, în care mulţi tineri de valoare din Europa Occidentală
şi SUA vin să studieze, să facă doctorate în România. E o
mişcare în ambele direcţii, dar e normal ca deocamdată ea să
fie mai puternică spre Occident decât invers.
Astăzi, ideea de identitate naţională trece prin mari
prefaceri, devine mult mai dinamică şi tendinţa de a o
1196

Rãni deschise

reduce doar la moştenirea trecutului este depăşită. Ea
trebuie să includă şi interacţiunea cu lumea din jurul
nostru. Identitatea unui obiect are o latură internă şi una
externă, iar integrarea europeană este numele pe care-l
capătă astăzi componenta interacţiilor noastre.
Reflectând asupra acestei chestiuni, mulţi dintre
intelectualii noştri au înţeles şi au temperat sau chiar
respins această teamă, şi-au dat seama că identitatea
europeană este un test al spiritului critic faţă de identitatea
naţională în concepţia ei tradiţională. De fiecare dată când
se apropie Crăciunul, se discută faptul că Europa ne cere
să renunţăm la modul nostru tradiţional de sacrificare a
porcilor şi apare o rezistenţă care ar trebui să ne facă să
reflectăm dacă trebuie păstrate chiar toate obiceiurile pe
care le-am apucat. Trebuie să vă dezvoltaţi spiritul critic
şi să judecaţi cu intelectul vostru tot ceea ce vi se spune.
Doar aşa puteţi să vă formaţi propria personalitate, să nu
deveniţi nişte fiinţe mimetice. Eu le spun întotdeauna
studenţilor mei:
„Dacă vreţi într-adevăr să-mi acordaţi respect, trebuie
să reacţionaţi critic la ceea ce vă spun, să-mi spuneţi când un
lucru nu vi se pare convingător sau când nu sunteţi de acord.”
În legătură cu studiul în străinătate, pot să vă fac o
listă de foşti studenţi de-ai mei care sunt printre vârfurile
matematicii mondiale şi care au la rândul lor discipoli de
toate naţionalităţile.
1197

Solomon Marcus

Un fost student, cu care mă laud foarte mult,
Gh. Păun, şi care a reuşit să realizeze o colaborare între
informatică şi biologie, recunoscut ca o somitate mondială,
a organizat anul trecut la Curtea de Argeş o întâlnire
internaţională în domeniul pe care l-a creat şi s-a văzut că
şcoala creată de el are discipoli în zeci de ţări ale lumii, pe
care i-a adus la Curtea de Argeş.
Astăzi, lucrurile se discută altfel şi în direcţia colaborării
dintre discipline. Am colaborat cu un profesor de geografie
de la Universitatea din Lausanne, Elveţia, care a prezentat
odată un istoric al specialităţii sale, geografia, din care a
rezultat că ea are un strămoş comun cu matematica, pe
Eratostene. Chiar şi împărţirea pe domenii, aşa cum o învaţă
elevii de vreo 200 de ani, nu mai corespunde situaţiei de azi
şi nici celei din vechime, pentru că lucrurile în realitate sunt
amestecate. La vechii greci, marele matematician Pitagora a
pus bazele muzicii, de la care a pornit toată cultura muzicală
occidentală, dar bazele pitagoreice ale muzicii nu se găsesc
nici în manualele de matematică, nici în cele de muzică. Noi
astăzi trebuie să înţelegem aceste lucruri şi să găsim
interconexiunile dintre ele.
Un alt elev întreabă:
– Aş dori să vă întreb ce sfaturi doriţi să ne daţi în
legătură cu ce trebuie să facem pentru a reuşi în viaţă.
– Mi se pare că lucrul cel mai important este să-ţi asculţi
vocea interioară şi să-ţi dai seama ce te pasionează, te
1198

Rãni deschise

interesează într-adevăr dincolo de împrejurările de moment şi
de loc şi să urmezi chemarea propriei tale vocaţii şi plăcerii de
a cunoaşte. La baza oricărei reuşite stă capacitatea de a vă
păstra curiozitatea, de a vă mira în faţa naturii, de a vă interesa
de tot ceea ce se întâmplă în jurul vostru. Marele poet american
Walt Whitman spunea că fiecare metru pătrat e acoperit de o
minune, dar trebuie să ştii să o vezi. Trebuie să vă păstraţi
curiozitatea şi prospeţimea, iar cuvântul „plictiseală” este
simptomul cel mai clar al unei vieţi nesănătoase. Trebuie să
vă menţineţi curiozitatea nesatisfăcută în ceea ce priveşte
secretele naturii, secretele fiinţei umane, ale psihicului uman.
În orice domeniu, pentru a obţine performanţă, trebuie să depui
efort, dar pentru asta trebuie să obţii o recompensă, să afli ceea
ce te interesează, să obţii plăcerea din tot ceea ce înveţi, iar la
copii această recompensă trebuie să vină imediat. Din păcate,
multe din manualele de azi nu sunt de foarte bună calitate, dar
dumneavoastră aveţi şi multe alte surse de informare. Trăiţi
într-o lume cu internet, televizor, telefon mobil şi trebuie să
învăţaţi să le folosiţi eficient pentru a vă fi folositoare.
O elevă întreabă:
– Credeţi că există deosebiri între elevii de azi şi cei
de ieri?
– Există multe deosebiri. Eu scriam în clasa întâi cu
creta pe tăbliţă şi cu creionul pe caietul liniat, iar în clasa
a II-a învăţam să scriem cu condeiul pe care-l muiam în
călimara cu cerneală. Călimara, care avea o îmbrăcăminte
1199

Solomon Marcus

de postav, se punea într-o scobitură specială a băncii, iar
noi ne temeam întruna să nu se verse pe bancă şi să ne
păteze caietele. Voi aţi scăpat de această grijă. În acel timp
se dădea importanţă scrisului frumos şi aveam o disciplină
care se numea caligrafie. George Bacovia a fost profesor
de caligrafie la liceul comercial, care se găsea peste drum
de şcoala noastră. Iată, el e un exemplu de o mare valoare,
un mare poet, care la început a fost cu totul lipsit de succes.
Succesul său a venit abia peste mai multe decenii.
Mai sunt şi alte deosebiri. Nouă ne plăcea mult să
citim poezii, dar am impresia că voi n-aţi descoperit încă
această minune şi cred că, dacă nu o veţi descoperi, viaţa
voastră va fi mai săracă. Eu pretind că şi matematica e o
minune la fel de mare ca poezia şi că e bine să le
descoperiţi la timp. Lipsa lor e un fel de infirmitate, o
infirmitate care nu e vizibilă, dar e la fel de reală.
O profesoară, fostă elevă a şcolii, aprobă cele spuse
de domnia sa:
– Aveţi dreptate referitor la dragostea faţă de lectură.
Elevii nu mai citesc din cauza internetului, pe care ei nu-l
văd în complementaritate cu cititul.
Ia cuvântul o fostă elevă a şcolii:
– Sunt cadru didactic universitar în domeniul
managementului şi apreciez prezenţa dvs. aici ca pe o
lecţie. Aş vrea să ştiu cum era percepută în vremea
dumneavoastră şcoala ca formator de destine.
1200

Rãni deschise

– Veniţi dinspre ştiinţele economice. Iată un alt
exemplu al pagubelor produse de izolarea disciplinelor.
Nici astăzi studenţii nu află că, încă de la începutul
secolului al XVII-lea, ştiinţele economice s-au dezvoltat
pe baza ideilor venite din fizică, din mecanică, apoi din
termodinamică în secolul al XVIII-lea, iar Mecanica
socială a lui Spiru Haret intră în această categorie. Aceste
deficienţe în predarea lucrurilor lasă pe dinafară lucruri
esenţiale şi nu dezvoltă colaborarea între discipline. Deşi
acest fenomen s-a manifestat cu putere abia în a doua
jumătate a secolului trecut, eu aveam în manualul de
matematică chestiuni de educaţie financiară, de matematici
actuale, elemente legate de dobânzi, de tabele de
mortalitate. Faptul că astăzi nu se mai învaţă asemenea
lucruri duce la o lipsă totală de conectare la universul de
azi al copilului.
Profesoara Cecilia Moldovan:
– Vreau să vă spun câteva cuvinte despre cartea pe care
am scris-o ca urmare a activităţii mele la catedră şi a unui
experiment făcut cu elevii, constituind un grup cu care am
elaborat un număr de mici piese de teatru în legătură cu
preocupările şi problemele lor. Am cerut părerea domnului
academician, iar domnia sa mi-a făcut onoarea de a-mi
postfaţa această carte. Iată ce scrie: „..nu mi-a trebuit prea
mult timp să descopăr în ea frământări care sunt şi ale mele
şi cărora chiar în urmă cu o lună le-am dedicat cartea Educaţia
1201

Solomon Marcus

în spectacol... Aceiaşi actori, la Moldovan şi la mine: elevi,
profesori, părinţi, rude, prieteni, strada, România şi lumea,
întregul univers tehnologic în care ne aflăm. Mă regăsesc în
neliniştile ei, în percepţia gravităţii marilor probleme
nerezolvate din educaţie.”
Solomon Marcus:
– Vreau să spun câteva cuvinte despre importanţa
teatrului în educaţie.
Educaţia este un spectacol, deoarece conflictele de
bază care sabotează procesul educaţional sunt de natură
teatrală, din acelea care menţin atenţia spectatorilor.
Educaţia e considerată în general ca o activitate care îi
priveşte doar pe elevi şi pe profesori, dar aici intervine o
serie întreagă de actori şi de roluri esenţiale: părinţii,
strada, internetul, televizorul, conflictele dintre elevi şi
părinţi, dintre elevi şi profesori. Părinţii nu ştiu să se
împrietenească cu copiii lor, la fel şi profesorii, iar copiii
nu ştiu să se împrietenească între ei şi intră într-o
concurenţă artificială, fără a se observa că e posibil a fi
performant fără ca asta să fie în dauna unui alt coleg. În
educaţie se potriveşte perfect întreaga terminologie
teatrală, cu regizori, roluri şi actori, iar ideea analogiei
dintre profesori şi actori nu este nouă, profesorul care
repetă actul educaţional cu fiecare generaţie în parte
trebuie să se comporte de fiecare dată ca şi cum ar face-o
pentru prima oară. Iniţiativa dumneavoastră cu grupele
1202

Rãni deschise

mixte de elevi şi profesori care scriu împreună o piesă de
teatru mi se pare minunată. De asemenea, faptul că aduceţi
în atenţie violenţa care asaltează multe canale de
televiziune. Urmăresc Tribuna Învăţământului şi în ea se
vede doar ce spun profesorii. Rareori apar acolo părerile
elevilor, părinţilor, ale celor care conduc diversele canale
de televiziune. Nu e sesizată întreaga complexitate a
procesului educaţional, sunt luate numai părţile ei
explicite, direct vizibile.
***
Spicuim din Cuvânt de mulţumire pentru invitaţia de
a participa la acest eveniment, de la spectacolul de
închidere a festivităţilor, organizat la Ateneul din Bacău.
„Există multe instituţii de cultură şi e greu de spus
care dintre ele este mai importantă, dar eu cred că nu e
nicio îndoială că, dintre toate instituţiile, cea mai
importantă în determinarea nivelului unei societăţi este
educaţia. Lucrul acesta e repetat mereu şi poate să pară o
banalitate, dar cele mai importante semnale sunt datorate
faptului că noi, cei din interiorul sistemului, suntem
marcaţi de o anume subiectivitate şi de multe ori ne
putem înşela sub impresia succeselor noastre locale şi
putem crede că lucrurile merg bine, dar observaţi cu
atenţie semnalele ce vin din exterior. Jonathan Sheele,
reprezentantul în ţara noastră al Comunităţii Europene,
atrăgea atenţia, în urmă cu câţiva ani, că principalul teren
1203

Solomon Marcus

pe care se joacă integrarea europeană este cel al educaţiei
şi că, din păcate, România se află în urmă în această
privinţă. Un alt semnal a venit de la Guvernatorul Băncii
Centrale a României, care a atras atenţia asupra faptului
că nu putem încă construi autostrăzi cum se cuvine, din
cauză că nu avem încă o clasă profesională competentă,
bine pregătită.
Fostul rector al Universităţii din Bucureşti şi preşedinte al Organizaţiei Rectorilor din România repeta mereu
că 80% din diplomele universitare din România sunt
diplome fără acoperire. Suntem într-un moment sărbătoresc
şi avem mulţi profesori şi educatori pasionaţi de munca lor,
dar să nu ne lăsăm păcăliţi, pentru că sunt multe lucruri încă
profund deficitare, abandonul şcolar este important, deci la
nivelul întregului sistem educaţional sunt multe semnale de
alarmă cum că lucrurile nu merg bine.
Bacăul are multe instituţii de cultură, are un teatru de
mare tradiţie, reviste de cultură foarte frumoase, o
universitate cu multe personalităţi şi are această şcoală,
care a constituit un izvor de energie pentru multe generaţii
de elevi. Să fim cu ochii pe şcoală şi să-i stimulăm mereu
calitatea.
Vă mulţumesc.”

1204

