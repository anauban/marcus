Rãni deschise

Întâlnire cu elevi de la Colegiul Naţional
„Spiru Haret”
Bucureşti, 17 februarie 2010

La întâlnire au participat elevii clasei a X-a I, prof.
Codruţa Missbach şi lector. univ. Sorina Woinaroski.
De ce nu se împrietenesc elevii cu părinţii lor?
Solomon Marcus: Am văzut filmul de la lansarea de
ieri a cărţii voastre (Codruţa S. Missbach & a X-a I:
„Într-o pauză – Universul”, Ed. Biblioteca Bucureştilor,
Bucureşti, 2010) şi am remarcat că sunteţi foarte frumoşi.
Sunt recunoscător pentru această carte şi doamnei
profesoare, şi vouă, pentru că am aflat şi am învăţat
multe lucruri de aici. Merg mereu în şcoli şi aveam o
idee vagă despre relaţia dintre părinţi şi copii, dar este
pentru prima dată când capăt despre ea o reprezentare
atât de detaliată şi de clară. În special pentru acest
lucru vă mulţumesc, dar şi pentru alte lucruri, pentru
că mi-aţi permis (nu numai mie, ci şi tuturor cititorilor)
1067

Solomon Marcus

să pătrund în lumea voastră, să înţeleg ce se întâmplă
cu voi.
Mă aflu într-o relaţie specială cu acest liceu prin
faptul că Spiru Haret, în primul rând, şi mulţi dintre cei
care au fost elevi sau profesori aici sunt printre spiritele
cele mai dragi mie, care au fost esenţiale în formaţia mea
ca intelectual şi ca educator.
O tradiţie glorioasă
Mă refer, la Spiru Haret, la Grigore Moisil, la unul
dintre membrii noi – Dan Barbilian care a fost elev şi apoi
profesor aici, la o generaţie celebră de foşti elevi ai acestui
liceu – Constantin Noica, Mircea Eliade, Barbu Brezianu.
Acesta din urmă povesteşte cu un talent extraordinar
şi cu multe detalii orele de matematică la Dan Barbilian,
care, decepţionat de prestaţia matematică a elevilor săi,
i-a trecut la poezie. Sunt oameni care fac parte din
construcţia mea spirituală. Vă daţi seama deci ce înseamnă
pentru mine Colegiul Naţional „Spiru Haret”.
Şi acum aş vrea să-mi permiteţi, ca să fim la egalitate,
să iau şi eu loc pe scaun.
Pentru că s-au format şi adjectivele haretişti şi
spirişti, mă interesează să văd în ce relaţie vă aflaţi cu
Spiru Haret, ca personalitate care a meritat să dea numele
atâtor instituţii importante.
1068

Rãni deschise

Cine a fost Spiru Haret?
(către o elevă): Ce îţi place în primul rând la Spiru
Haret?
Delia: Mă numesc Delia. Spiru Haret a fost, poate,
cel mai mare reformator al învăţământului românesc. Una
din statuile din Piaţa Universităţii îl reprezintă pe Spiru
Haret.
Solomon Marcus: Care ţi se pare că e cea mai mare
ispravă a lui, ca reformator al şcolii?
Delia: Sunt mai multe…
Solomon Marcus: În primul rând, hai să vedem, în ce
perioadă istorică? Aşa cu aproximaţie…
Delia: Secolul al XIX-lea…
Solomon Marcus (către clasă): Voi ce părere aveţi?
Din clasă se aud voci: Secolul XX… secolul XIX…
Solomon Marcus: El a încălecat cele două secole.
Sau, cum spune Sextil Puşcariu, „Călare pe două veacuri”.
Prestaţia sa s-a manifestat în ultimele decenii ale secolului
al XIX-lea şi primul deceniu din secolul al XX-lea.
Dacă ar fi să selecţionăm din reforma pe care el a
preconizat-o şi a şi înfăptuit-o în bună măsură, ce aţi alege?
O elevă: Spiru Haret a introdus gratuitatea
învăţământului primar.
Solomon Marcus: Cred că am putea să rezumăm aşa:
Spiru Haret e primul după Gheorghe Lazăr care dă o
1069

Solomon Marcus

lovitură puternică analfabetismului din acel moment. Dar
în afară de reforma şcolii, ce alte performanţe mai avea?
Haret ca savant în astronomie
O elevă: A fost un matematician renumit, astronom…
Şi-a luat doctoratul la Paris.
Solomon Marcus: Ai dreptate, dar ştie cineva ce
legătură există între Spiru Haret şi harta Lunii?
Voci din clasă: Da! Un crater de pe Lună a primit
numele său!
Solomon Marcus (zâmbind): Bine. Ce merit avea
Haret ca să merite ca un crater de pe Lună să poarte
numele său?
O elevă: Şi-a dat doctoratul în astronomie.
Solomon Marcus: Unde?
O elevă: La Paris.
Solomon Marcus: Este primul român care susţine o teză
de doctorat la celebra Universitate Sorbona, din Paris. Şi nu
numai că susţine o teză de doctorat, dar în acea teză vine cu
un rezultat surprinzător. Poate ştie cineva despre ce rezultat
era vorba, cu care a surprins întreaga comunitate ştiinţifică?
Voci din clasă: …despre instabilitatea axelor
planetelor.
Solomon Marcus: Da, aşa este. Timp de câteva secole
s-a crezut că sistemul solar este stabil. Mergând la o
1070

Rãni deschise

aproximaţie mai fină, cum se spune în matematică, de
ordinul al treilea dintr-o anumită dezvoltare în serie, el a
demonstrat că la aproximaţia de ordinul respectiv apare o
sursă de instabilitate. Asta este o problemă faimoasă, care
prezintă un interes cultural pentru toată lumea, indiferent
că faceţi secţia umană sau secţia ştiinţifică, problema celor
trei corpuri. O problemă aparent simplă. Există legea
atracţiei universale şi aceste trei corpuri: Soarele,
Pământul şi Luna. Dacă facem abstracţie de Lună şi
considerăm numai Soarele şi Pământul, nu e greu să
stabileşti mişcarea planetei Pământ ca urmare a legii
atracţiei universale a lui Newton. Când intervine şi un al
treilea corp, Luna, lucrurile se complică, în aşa fel încât
nici până azi această problemă nu este pe deplin rezolvată.
Este una din marile probleme ale ştiinţei, o provocare
extraordinară. Cei mai mari oameni de ştiinţă şi-au încercat
puterile în această privinţă, s-au adus mereu tot felul de
contribuţii, în special prin Henri Poincaré, unul dintre cei
care au remarcat teza lui Haret.
Mecanica socială
Dar vreau să vă întreb ce anume din opera lui Spiru
Haret stabileşte o punte între ştiinţele grele şi domeniul
socio-uman. Separarea care se menţine şi care mi se
pare complet anacronică, între o secţie ştiinţifică şi una
1071

Solomon Marcus

socio-umană, nu e deloc în spiritul lui Haret. Dacă ar
mai fi trăit azi, ar fi respins categoric această
mentalitate. La ce lucrare a lui Spiru Haret mă refer,
care a fost publicată tot la Paris şi a avut un ecou la fel
de extraordinar?
O elevă: „Mecanica socială”?
Solomon Marcus: Da, asta este. Aţi auzit de oximoron?
Ce este un oximoron? Asta trebuie să fi învăţat la literatură.
O elevă: Alăturarea paradoxală a două cuvinte total
opuse.
Solomon Marcus: Alăturarea a doi termeni incompatibili. Mecanica socială, chiar pentru mulţi sociologi, este
o sintagmă oximoronică. „Nu merge să studiezi societatea cu
ideile din domeniul ştiinţelor naturii. Societatea este cu totul
altceva.” – spun ei.
Haret a sfidat însă această idee.
Eşecul sistemului nostru educaţional
Diagnosticul care se pune în ansamblu învăţământului nostru este negativ.
Jonathan Scheele, care a fost reprezentantul Comisiei
Europene în România, până în urmă cu vreo trei ani, spunea
că principala problemă a integrării europene a României este
educaţia şi a adăugat: „Sistemul educaţional din România este
învechit” – a spus asta după ce a trăit câţiva ani în România.
1072

Rãni deschise

Acum să vă spun un avertisment venit ieri – pentru
că asemenea avertismente vin mereu –, să vedem ce părere
aveţi dvs. despre toate astea.
Economistul şef al Băncii Naţionale a României,
Valentin Lazea, a afirmat: „E un mit că avem o forţă de
muncă educată, bine calificată. Sărăcia nu poate fi
eradicată fără îmbunătăţirea sistemului educaţional”.
Clar, scurt şi simplu!
Dintr-o altă direcţie, o directoare a unui Teatru nou
înfiinţat, Mihaela Sârbu, în iunie 2008, întrebată fiind la
Jurnalul de la TVR Cultural de ce a înfiinţat acest nou
teatru, care se cheamă Teatrul fără frontiere, a răspuns:
„Copiii îşi pierd creativitatea odată cu intrarea la şcoală
şi noi am încercat s-o salvăm”.
Învăţare adevărată nu există fără bucurie
Cred că nu există decât o singură învăţare autentică,
aceea care se face în stare de bucurie. Atâta vreme cât
aceste două acte (actul imperativ al profesorului şi
momentul de bucurie al elevului) nu fac joncţiunea, totul
este sortit eşecului.
Boala începe chiar de la nivelul şcolii primare, unde
întrebarea este descurajată, manualele sunt complet lipsite
de imaginaţie şi totul duce la despărţirea, care se produce
la nivelul gimnaziului, dintre elevi şi şcoală.
1073

Solomon Marcus

Aş vrea să pun o întrebare punctuală. Puteţi să-mi daţi
exemple de manuale şcolare după care aţi învăţat, nu
neapărat în clasa a X-a, ci în anii anteriori, care v-au plăcut
într-atât, încât aţi simţit nevoia să vă întoarceţi la ele şi
după ce aţi trecut în clase superioare, care au fost cărţi de
plăcere, nu doar de datorie şcolară?
O elevă: Cărţile de limba engleză, în rest…
O altă elevă: Cărţile de geografie.
Solomon Marcus: La ce clasă?
O elevă: În clasa a opta, era foarte frumoasă şi foarte
bine schematizată.
Solomon Marcus: Dar din domeniul ştiinţelor grele,
matematică, fizică, chimie, biologie?
Voci din clasă: Nu… Poate Biologia de a şaptea…
Anatomia… Nu, mie mi se părea prea greoaie şi încărcată…
Solomon Marcus: În orice caz, trebuie să recunoaşteţi,
că e un fenomen foarte rar acesta. Părerea mea este că una
din sursele de boală aici se află.
Scenariul orelor de clasă trebuie schimbat
O altă sursă de boală, asupra căreia aş vrea să am
reacţia voastră, se referă la scenariul tradiţional al unei ore
de clasă. Chiar verbul acesta, profesorul predă, ce
înseamnă? E o relaţie dominată mai mult de modalitatea
imperativă, pe când singura metodă care poate să dea
rezultate este cea interogativă.
1074

Rãni deschise

Un elev: Da, singura metodă este cea interogativă,
dar problema fundamentală este că nu există o relaţie
profesor-elev deschisă. Pot să vă dau un exemplu, chiar
din această clasă: doamna Missbach mi se pare singurul
profesor care ne abordează de la acelaşi nivel, într-o
relaţie profesor-elev deschisă…
O elevă (îl întrerupe): O relaţie profesor-elev apare
atunci când ni se acordă încredere şi prietenie din partea unor
profesori, când putem să ne spunem punctul de vedere şi nu
suntem obligaţi să învăţăm de frică, pentru că ştim că asta
este datoria noastră; dar nu trebuie să ni se impună la modul
drastic. Astfel putem să reţinem şi informaţia mult mai uşor.
Solomon Marcus: Înţeleg, toate aceste boli, în mod
inevitabil, v-au marcat. Pe vremea comunismului, ne
dădeam seama de bolile publice şi căutam să ne distanţăm
prin ironie, dar până la urmă am constatat că tot am fost
marcaţi, ne-am îmbolnăvit toţi, unii mai mult, alţii mai puţin.
De unde atâtea certitudini la vârsta de 16 ani?
În cartea voastră, de pildă, foarte rar se întâmplă ca
cineva să manifeste o stare de îndoială. În general,
predomină certitudinea. Vă mărturisesc, când am fost
invitat să vin aici, imediat m-am gândit că voi întâlni tineri
de vreo 16 ani – clasa a X-a. M-am gândit imediat ce
făceam eu la 16 ani. Când se întâmpla lucrul acesta? Vă
1075

Solomon Marcus

las să calculaţi: dacă împlinesc zilele viitoare 85 de ani, în
ce an aveam eu 16 ani?
Voci din clasă (calculând): …1941.
Solomon Marcus: În ce situaţie se afla România în 1941?
Voci din clasă: Începuse al Doilea Război Mondial.
Solomon Marcus: Vă daţi seama ce moment de
tensiune, ce dificultăţi… Mi-am adus aminte că, atunci
când împlineam 16 ani, un poet, al cărui nume aş vrea să
mi-l spuneţi voi, a scris o poezie „Şaisprezece ani”:
Şaisprezece ani, şaisprezece stele, tot atâtea deziluzii/ De
mâine nu mai am voie să umblu hai-hui/ Voi plânge după
ani zece când mâncam cruzii/ Pepeni când umblam de
mână cu voinţa nimănui!
Cine a scris poezia asta, ştiţi? Geo…
Voci din clasă: …Dumitrescu.
Solomon Marcus: Da. Geo Dumitrescu. Într-adevăr,
iată un portret foarte frumos adus vârstei de 16 ani, la acel
moment. Acum e altul.
Prea multe cuvinte de argou
Dacă mă uit în cartea voastră, am unele dificultăţi cu
unele cuvinte. De pildă, cuvântul …napşa.
Clasa (corectându-l în cor): Naşpa!
Solomon Marcus: Aşa, naşpa! Dar şi alte cuvinte, pe
care m-am dus să le caut în Dicţionarul explicativ al limbii
1076

Rãni deschise

române. Nu le-am găsit, dar am aflat din altă parte că sunt
cuvinte de argou. Ştiţi ce scrie la cuvinte de argou, în
Dicţionarul explicativ al limbii române?
Spune aşa (citind un text de pe o foaie de hârtie):
„Limbaj convenţional, folosit mai ales de vagabonzi (se
aud hohote de râs ale clasei), răufăcători etc.” (ridicând
privirea spre clasă). La etcetera, intraţi voi! (clasa
izbucneşte în hohote de râs).
De ce oare? m-am întrebat. Răspunsul din dicţionar:
„pentru a nu fi înţeleşi de restul societăţii”.
Şi atunci, mi-am spus că ei au scris cartea pentru ca
noi să-i cunoaştem, dar în acelaşi timp şi ca un mod de a
se ascunde.
Am observat un lucru: când vorbiţi despre probleme
grave, despre iubire, despre societate, aveţi certitudini!
(Râzând): Cum spune tot un vers de Geo Dumitrescu:
„Certitudini, Doamne, ce bine e să ai certitudini!”
Şi cartea e plină de certitudini!
Valoarea reală a unei diplome
Dar din modul cum voi vă reprezentaţi societatea
de azi, de multe ori, greşiţi profund, din lipsă de
informaţie. De pildă, se insistă pe ideea că azi totul este
să ai diplome. Complet fals! Vă spun care e realitatea de
azi, e foarte important să înţelegeţi, pentru că asta
1077

Solomon Marcus

priveşte viitorul vostru. E adevărat că, pe baza unor
diplome, poţi să pătrunzi în anumite locuri, dar până la
urmă, contează ce ştii să faci. Aş vrea să vă intre bine în
cap lucrul acesta!
Vă rog să nu vă supăraţi că mă dau şi pe mine ca
exemplu, diplomele mele nu sunt decât în domeniul
matematicii. Dar multe din performanţele mele ştiinţifice
sunt din alte domenii, unde nu am nicio diplomă, de
exemplu în lingvistică.
Vă dau şi alte exemple. Cristian Tudor Popescu,
renumitul gazetar, este inginer la bază; o altă persoană, care
are o funcţie importantă la televiziunea română – Tudor
Călin Zarojanu, este unul dintre foştii mei studenţi de la
matematică. Cred că cel puţin jumătate dintre cei care au
obţinut diploma într-un anumit domeniu nu mai lucrează în
domeniul acela.
Cum ne alegem profesia
Am văzut la voi o anumită idee, aproape fixă, care e
de fapt falsă: vorbiţi de o carieră de succes într-o anumită
profesie, dar să ştiţi că profesiile sunt acum într-o
schimbare atât de rapidă, dinamica profesiilor este atât de
mare, încât o bună parte din profesiile care vor exista,
atunci când voi veţi termina o facultate, acum nici nu
există încă sau, dacă există, ele vor arăta cu totul altfel.
1078

Rãni deschise

De exemplu, arhitectura a existat şi acum 50 de ani,
şi astăzi. Dar, azi, profesia de arhitect este cu totul altceva
decât era atunci. Şi la fel este situaţia cu multe alte profesii.
Din experienţa mea personală vă spun că este cea mai
mare gafă să decizi ce profesie să alegi în funcţie de situaţia
la un anumit moment. Esenţialul este să te îndrepţi către
ceva despre care simţi că răspunde unei nevoi sufleteşti,
unei nevoi interioare. Asta este esenţial, chiar dacă, pentru
moment, acea îndeletnicire nu are un portret prea atrăgător.
Când venise vremea ca eu să merg la facultate, am
ales matematica, deşi nu îmi dădea altă perspectivă decât
aceea de a preda într-o şcoală. Simţeam însă că mă atrăgea
matematica. Profesia de cercetător nu exista încă în
România. Am observat că mulţi dintre voi au decis, în
clasa a X-a, ce vor să facă în viaţă. Mi se pare prematur,
pentru că ştiţi ce contează? Contează exact ce ai neglijat,
contează capacităţile tale de gândire şi de comportament.
Cum se citeşte?
Lucrul cel mai important, cred eu, pe care trebuie
să-l înveţe un tânăr, este cum să gândească, cum să citească
un text… Îmi amintesc de o carte în limba franceză, care
m-a impresionat mult în adolescenţă, a lui Emile Faguet:
„L’art de lire”. Primul său îndemn era: să citeşti încet, să
reiei lectura, să te convingi că înţelegi.
1079

Solomon Marcus

Spuneţi-mi, mai e valabilă măsura asta, că trebuie să
înapoiaţi manualele la sfârşit de an?
Clasa: Da!...Da!...
Solomon Marcus: Mie asta mi se pare o neînţelegere
totală a ceea ce înseamnă a învăţa. Pentru mine a învăţa
înseamnă ca faţă de un text, fie din manual, fie venit de la
catedră, să ai dreptul prezumţiei de suspiciune. Asta
înseamnă că până în momentul în care eu însumi, cu capacităţile mele, ader la textul respectiv, asimilez, înţeleg, am
dreptul să mă îndoiesc. Am observat că se vorbeşte mult în
cartea voastră de tocilari. Şi pe vremea mea li se spunea
unora tocilari. Dar tocilarii ştiţi cine erau? Erau cei capabili
să memoreze foarte multe lucruri, fără să le înţeleagă.
Acum vreo doi ani, au fost anunţate rezultate
statistice, care arătau că mai mult de jumătate din tinerii
de azi nu înţeleg ce citesc. De ce nu înţeleg ce citesc?
Pentru că şcoala i-a obişnuit ca textele care li se livrează
să fie memorate, pentru a putea fi restituite la examen sau
la alte competiţii. Preocuparea aceasta de a „învăţa” pentru
a trece un examen, şi nu pentru a avea nevoie de a înţelege
lumea, de a înţelege o anumită idee, este catastrofală.
Rostul manualului şi nevoia de umor
Aici este eşecul şcolii, aici este sursa sărăciei. Voi ce
părere aveţi?
1080

Rãni deschise

Dacă luăm problema manualelor care trebuie
returnate la sfârşit de an, asta înseamnă că eu nu mai am
dreptul să scriu nimic în el. Vă spun cum fac eu când am
de a face cu un text: fac însemnări pe margine, remarc
greşelile de orice fel, de la greşelile de ortografie, până la
cele de logică şi de informaţie istorică, pun semn de
întrebare acolo unde nu înţeleg, scriu „Nu” acolo unde
nu-s de acord; asta înseamnă pentru mine adevărata
lectură profitabilă. Să spunem lucrurilor pe nume:
educarea spiritului critic. Nu este aceasta o problemă
actuală a şcolii?
Vă propun să folosim o metaforă războinică.
Dacă asimilăm educaţia cu un război, un război cu
ignoranţa, cu necunoscutul, linia principală a frontului în
acest război este în ora de clasă, profesor şi elev faţă în
faţă, care trebuie să înlocuiască scenariul dominat de
modalitatea imperativă cu scenariul dominat de
modalitatea interogativă, cu îndoiala, cu actul critic, cu
ironia şi auto-ironia.
Cred că, aşa cum învăţarea adevărată nu există fără
bucurie, tot aşa nici învăţarea adevărată fără simţul
umorului aş îndrăzni să spun că nu există.
V-am văzut ieri la lansarea cărţii voastre. Am văzut
filmul şi am remarcat cu plăcere că râdeaţi tot timpul.
Asta este atitudinea omului inteligent, care are
întotdeauna surse de zâmbet şi de râs.
1081

Solomon Marcus

Ce urmăreşte televiziunea?
Noi trăim într-o societate în care ce vedem la
televizor? Ni se spune, chiar de către cel care conduce
televiziunea publică, că rolul televiziunii este triplu:
– să informeze
– să educe
– să distreze.
Am observat în cartea voastră acest binom, care
revine obsedant: plictiseală – distracţie. Cum să faci să nu
te plictiseşti şi cum să faci să te distrezi?
Vă fac o mărturisire, pe care o fac rar: aveţi în faţa
voastră un om care nu s-a plictisit niciodată.
Să nu confundăm succesul cu valoarea
Se face o deosebire între autorii de succes şi autorii
de valoare.
Succesul poate să depindă de nivelul scăzut al unui
anumit public. Pe când, dacă vorbim de valoare, ea este
stabilită de instanţele de calitate în profesia respectivă. Nu
vă fac niciun reproş, căci noi toţi am fost victime ale unui
sistem educaţional bolnav. În momentul de faţă, când
fenomenele de sărăcie explodează, învăţământul din toată
lumea are probleme, dar gravitatea şi urgenţa lor nu sunt
aceleaşi pentru Occident ca pentru noi, căci în România
1082

Rãni deschise

sărăcia strigă tot timpul şi toate aceste eşecuri educaţionale
au efecte catastrofale. Observaţi că vedem tot timpul
dezbateri în media pe teme de educaţie, dar aproape
niciodată nu sunt aduşi la aceste dezbateri cei din linia întâi:
adică voi, profesoara voastră... Şi cine vine la aceste
dezbateri? Vin oameni politici, vin gazetari, sindicalişti,
adică oameni care, deşi se laudă că au lucrat şi ei cândva în
învăţământ, totuşi legătura lor cu sistemul educaţional s-a
subţiat într-atât, încât practic nu mai există. Voi nu aveţi încă
o reprezentare corectă a lumii în care trăiţi, să nu vă amăgiţi,
să fiţi conştienţi. Preluaţi uneori fără să vă daţi seama ceea
ce au spus alţii.
Între rutină şi personalitate
În comportamentul unui om sunt două forme de acte.
Sunt unele aspecte preluate de la societate, părinţi, ele devin
acte de rutină; altele sunt cele ale intelectului nostru personal.
De aceea trebuie să fim atenţi când vorbim noi şi când vorbesc
alţii prin gura noastră. Cum spunea Eminescu: „Când mă
gândesc la viaţa-mi îmi pare că ea cură/ Încet repovestită de
o străină gură”. Deci să fim foarte atenţi că de multe ori
suntem vehemenţi în anumite discursuri, dar nu realizăm că
altcineva vorbeşte prin noi. Nu putem elimina complet
comportamentul acesta de rutină, de preluare rutinară a unor
obiceiuri, dar e bine ca locul lor să fie cât mai redus.
1083

Solomon Marcus

Menirea şcolii
Şcoala are menirea să apropie două elemente care par a
fi la distanţă astronomică: imperativul didactic de la catedră
şi bucuria şi plăcerea personală a elevului. Dacă aceste două
momente nu reuşesc să facă joncţiunea, totul e ratat. Să
micşorăm cât mai mult partea preluată mecanic din
comportamentul nostru, pentru a mări partea asumată, aceea
pe care o construim cu propria noastră personalitate, pentru a
ne influenţa propriul viitor. Televiziunea are un aspect pozitiv
şi unul negativ. Trebuie să învăţăm să le distingem. Locul
astronomiei, care în şcoală s-a subţiat, s-a diminuat, a fost luat
de astrologie. S-a ajuns să ni se prezinte vrăjitoare la
Parlament, care au venit să alunge spiritele rele.
Cuvântul revoluţie s-a demonetizat, pentru că este
folosit tot timpul, când trebuie şi când nu, dar apariţia
internetului este într-adevăr o revoluţie. O mare parte a
intelectualităţii noastre diabolizează această revoluţie
tehnologică şi nu vede minunea care este ascunsă în
internet. De aceea trebuie să cunoaşteţi istoria.
Noul provoacă o reacţie de respingere
Am să vă spun o istorioară: pe la 1830 urma să se
introducă trenurile în Statele Unite ale Americii şi primarul
New York-ului a adresat o scrisoare Preşedintelui Statelor
1084

Rãni deschise

Unite, atrăgându-i atenţia ca nu cumva să permită introducerea
acestui nou mijloc de transport, pentru că va fi o nenorocire,
se vor speria mamele, pruncii, vor intra în şomaj lucrătorii din
transportul naval, aceste trenuri vor circula cu o mare viteză,
de 34 de mile pe oră, extraordinară la acea vreme; dar, din
fericire, Preşedintele nu a urmat sfatul primarului. Fiecare
inovaţie a avut partea ei de respingere. Şi când s-a trecut de la
oralitate la scriere, la fel a fost. De la scriere la tipar, apoi când
au apărut avioanele, la fel. Deci vă daţi seama că toate aceste
acte de condamnare a internetului sunt acte de incultură.
Sunteţi victimele acestei separări artificiale: umanştiinţific. De pildă, am înţeles că vă place mult psihologia.
Aţi aflat de legea dată de Weber şi Fechner în secolul al
XIX-lea, care spune că senzaţia este logaritmul excitaţiei?
Deci, când un şir de excitaţii primite de organismul nostru
merge în progresie geometrică, senzaţiile respective merg
în progresie aritmetică.
O progresie aritmetică, ştiţi asta, oricât de umanişti
sunteţi, merge mult mai lent decât una geometrică.
Fiinţa umană domoleşte ritmurile naturii
după legea logaritmică
În cartea mea despre timp, am pus mare preţ pe relaţia
dintre timpul psihologic şi timpul cronologic. Şi aici s-a
demonstrat că timpul psihologic este logaritmul timpului
1085

Solomon Marcus

cronologic. Iată ce minunăţii avem dacă articulăm cele
două gândiri, cea umanistă cu cea ştiinţifică.
Paradigma dominantă în cultură acum este
informaţia, separaţia dintre ştiinţă şi umanistică a căzut
pentru că domeniul cognitiv, de exemplu, are nevoie de
toate disciplinele: logică, matematică, lingvistică, sociologie, psihologie, informatică.
Veţi avea de recuperat toate aceste pagube ale
sistemului educaţional, prin mijloace proprii. Trebuie să
fiţi conştienţi de aceste lucruri: că va trebui să compensaţi
tot ce a dus la falimentul educaţional. Vi se pare că
exagerez?
O elevă: Nu, nu mi se pare că exageraţi, este foarte
logic ceea ce spuneţi.
Altă elevă: Cum vă explicaţi faptul că elevii români
excelează totuşi în străinătate?
Esenţialul: să asimilezi moduri de gândire
cât mai variate
Solomon Marcus: Sigur că avem câteva sute de
români care excelează într-un domeniu, de exemplu în
matematică, dar asta e legea naturii, există 10%, 15%
alteori 8% de elemente dotate în fiecare generaţie, cei care
reuşesc prin capacităţile lor personale să compenseze
deficienţele educaţionale. Sigur că se întâmplă lucrul
1086

Rãni deschise

acesta. Mai există şi categoria celor care aici au avut o
prestaţie bunişoară, iar în mediul de afară prestaţia lor
s-a îmbunătăţit. Eu mai ţin legătura cu cei care acum sunt
afară; am vorbit recent cu o fostă studentă de-ale mele,
acum profesoară în Franţa, Anca Pascu, de la Universitatea
din Brest, care s-a realizat în domeniul lingvisticii
computaţionale. Observaţi denumirile domeniilor, chiar
acestea arată că nu se mai face separare între domeniile
ştiinţifice şi cele umaniste. Deci sunt şi cazuri în care
oameni cu o dotare normală ca inteligenţă şi prin muncă
foarte susţinută reuşesc, dar există şi oameni foarte talentaţi
care abandonează cercetarea şi intră în afaceri. De exemplu,
Cristian Sima, considerat expert în domeniul financiar şi
bancar, a făcut matematică, excela în matematică pură şi
acum este un expert în operaţii de bursă, fiind consultat de
Banca Naţională; şi el recunoaşte că a ajuns aici prin modul
de gândire matematică, pe care l-a dobândit în facultate.
Modul de gândire este esenţialul pe care trebuie să-l
asimilăm din şcoală. Dacă nu, o facem pe cont propriu.
Uite, să vă spun ceva. Pentru mine internetul este o chestie
extraordinară. Recent am lucrat asupra unei probleme
legate de iconicitatea în sintaxă. Pentru documentarea
respectivă pierdeai altădată un timp enorm, acum pe
Google poţi dobândi imediat o listă de sute de livrări. O
situaţie similară cu tema metafora matematică, aparent
oximoronică. Metafora cognitiv-creativă este un proces
1087

Solomon Marcus

esenţial în toate domeniile, mai ales în ştiinţă, unde nu
există concept fără de metaforă. Iată că metafora nu se
rezumă doar la literatură. Sintagma aceasta, oximoronul
acesta „metafora matematică” are o anumită maturitate.
Greşelile din Wikipedia
O elevă: Dar mulţi spun că în Wikipedia şi pe Google
sunt greşeli, dumneavoastră cum vedeţi această afirmaţie?
Solomon Marcus: Sigur că sunt greşeli, dar... astea
sunt întreprinderi care lucrează în foc continuu, care
înregistrează fiecare nouă apariţie.
Credeţi că marile enciclopedii tradiţionale ca
„Larousse”, „Brockhaus” şi altele nu sunt şi ele pline de
greşeli? Spiritul nostru critic trebuie să acţioneze
permanent. E nevoie de educaţia spiritului critic, să nu
credeţi în prestigiul cuvântului tipărit, doar pentru că este
tipărit, în prestigiul cuvântului de la catedră, doar pentru
că este de la catedră, ci să vă întrebaţi mereu dacă chiar
aşa este, să puneţi la îndoială ceea ce vi se spune.
Un elev: Dar internetul nu dă o oarecare dependenţă?
Cel puţin aşa se susţine acum.
Solomon Marcus: Să ştiţi că orice poate da
dependenţă. Termenul a fost împrumutat de la dependenţa
de droguri, dar putem vorbi de dependenţă de orice: de
lectură, de calculator, de teatru; orice depăşeşte limitele
1088

Rãni deschise

unei igiene mentale, psihosomatice este dăunător. Mereu
aud chestia asta. Dar orice mâncare consumată exagerat
are dezavantaje, orice pozitiv se poate transforma în
negativ şi de aceea nu văd de ce să considerăm numai
dependenţa de internet.
Un elev: Probabil la copii care se joacă doar.
Solomon Marcus: Acolo nu e vorba de dependenţa de
internet, ci de dependenţa de un anumit mod de a folosi
internetul.
Potenţialul internetului
Un elev: Sau poate că nu au descoperit decât un
anumit potenţial al internetului.
Solomon Marcus: Exact, iar conflictul dintre internet
şi carte este şi el o făcătură.
O elevă: Dar nu restricţionează vocabularul?
Solomon Marcus: Nu înţeleg.
C. Missbach: Ceea ce vrea să spună este că ei, la
vârsta lor, preferă, în loc să citească ceva pe internet, să
vadă un film sau să joace un joc şi vocabularul lor devine
astfel restrâns.
Solomon Marcus: Voi nu aţi înţeles ce e cu internetul
şi îl limitaţi la acest lucru, pentru că nu aţi aflat la ce e bun.
E ca şi când cineva ajunge la Paris şi se duce în locuri
derizorii, pentru că nu a aflat de marile muzee, de marile
1089

Solomon Marcus

teatre, biblioteci. De asta vă spun că e nevoie de o educaţie
a internetului, care a fost lăsat să fie folosit la întâmplare.
C. Missbach: Îmi permiteţi şi mie să spun ceva?
Solomon Marcus: Da, spuneţi!
C. Missbach: Este corect. Copilul are nevoie de
educare în privinţa internetului, dar mai important este să
capete mai întâi gustul lecturării, căci un copil fără gustul
lecturării, care deschide internetul şi vede o carte, o
respinge din start, închide pagina.
Solomon Marcus: Doamnă, să ne înţelegem. Luând
copilul de la naştere, primul lucru care intră în educare, în
China, India, SUA, este exerciţiul numerelor, încă înainte
de învăţarea alfabetului. Apoi vine învăţarea cititului şi
scrisului, etapă fundamentală, pe care fiecare elev urmează
s-o parcurgă cu grija de care vă vorbeam când îl evocam
pe Faguet. Spuneam că manualele sunt o catastrofă.
Universul tehnologic în care trăieşte azi copilul este
absent, manualele sunt plicticoase, lipsite de imaginaţie.
Când să înceapă împărţirea pe discipline
O elevă: Da, dar noi am fost învăţaţi să învăţăm
automat, ca nişte roboţi.
Solomon Marcus: Vă spun ce am văzut în străinătate.
Până la mijlocul gimnaziului nici nu trebuie menţinută
separaţia pe discipline. Elevii învaţă acolo dintr-un singur
1090

Rãni deschise

manual toate: cunoaşterea naturii, socotelile, limbajul şi
comportamentul social. Separaţia pe discipline când a
apărut? Abia la începutul secolului al XIX-lea. Deci timp de
mii de ani noi am avut o cultură dominant sincretică. Dacă
evoluţia socială a ajuns atât de târziu la separaţia pe domenii,
atunci şi individul uman, care repetă evoluţia socială, nu
trebuie să urmeze acelaşi curs? Cred că separaţia pe
discipline ar trebui să vină abia prin clasa a VI-a, a VII-a. Vă
întreb acum pe voi: v-aţi gândit că disciplinele astea pe care
le învăţaţi sunt foarte legate între ele? Ele sunt separate doar
artificial, pentru că prin natura lor ele sunt amestecate. Aşa e
în viaţă, în natură. Nu se acordă atenţie acestui metabolism.
În clasa a şasea, nu era normal oare să aveţi în manualul de
algebră „Bazele pitagoreice ale muzicii”? Pentru că Pitagora,
în Antichitate, punea bazele muzicii occidentale de azi. Aţi
aflat ceva despre astea?
C. Missbach: Acestea se studiază la filosofie în clasa
a XII-a, când se predă determinismul şi se face distincţia
dintre ordine – dezordine, aleatoriu.
Solomon Marcus: Bazele pitagoreice ale muzicii sunt
de natură matematică, nu are cum să dea seama de ele
filosofia. Iată ce frumos se leagă... muzica şi matematica
au legătură şi astfel lucrurile se înlănţuie. Voi trebuie să
revendicaţi dialogul dintre discipline. Astea trebuie să vină
de la voi. Voi sunteţi actorii sociali care trebuie să vă
spuneţi cuvântul.
1091

Solomon Marcus

O elevă: Asta doar dacă ne ascultă cineva.
Solomon Marcus: Nu, trebuie să vă formulaţi
revendicările. Nu aţi observat limbajul cazon, militar din
enunţul problemelor de examen: „Să se” afle, „să se”
demonstreze că...? Nu aţi observat că şcoala vă obligă să
răspundeţi mereu la întrebările altora, dar nu vă învaţă cum
să inventaţi voi întrebări? În orice răspuns la o problemă
se află ascunsă o altă întrebare. Aţi fost antrenaţi în acest
sens? Iniţiativele aici trebuie să vină de la voi şi de la
profesorii voştri. Trebuie să profitaţi de avantajele
democraţiei, căci nu trăiţi în societatea în care am trăit eu,
când nu aveam voie să călătorim. Ministrul Educaţiei
repetă mereu că aşteaptă de la şcoli, de la societăţile
profesionale de ramură să vină iniţiativele.
Solomon Marcus: Ora s-a terminat, dar eu sper că
scopul meu aici a fost să vă dau de gândit la o seamă
întreagă de probleme care au legătură cu viitorul personal
şi cu cel al ţării noastre.
Moisil, primul pe listă
Elevii se ridică din bănci, iar un grup îl înconjoară pe
prof. Solomon Marcus şi continuă discuţia la catedră.
Solomon Marcus: L-am invitat pe Constantin Noica,
să scrie o prefaţă la cartea „Ştiinţă şi umanism” cu articole
ale lui Moisil.
1092

Rãni deschise

Ştiţi cum începe prezentarea? „Pe placa de marmură
a liceului „Spiru Haret”, se află lista premianţilor de
onoare” (adică acei premianţi care au primit premiul I în
toate clasele). Lista începe cu Grigore Moisil, pe care îl
întreabă cineva: „Domnule profesor, chiar aţi fost
dumneavoastră un elev de nota 10, pe toată linia, că parcă
nu daţi impresia asta?” Ştiţi ce a răspuns Moisil? „Da’ de
unde, domnule, aveau nevoie de cineva care să deschidă
lista şi m-au găsit pe mine!” (hohote de râs).
Trebuie să recunosc că sunteţi un liceu cu o tradiţie
formidabilă.
O elevă: Vă mulţumim foarte mult pentru tot ce
ne-aţi învăţat azi, din păcate, niciun profesor nu ne-a spus
asta până să veniţi dumneavoastră.
Solomon Marcus: Dar eu trebuie să vă spun ceva.
Dacă deveneam şcolar la acest liceu, prima întrebare a mea
către primul profesor pe care îl întâlneam ar fi fost: „Am
devenit elev la acest liceu şi vreau să aflu cine a fost Spiru
Haret, ca să înţeleg şi eu de ce a meritat ca acest liceu
să-i poarte numele.”
O elevă: Am întrebat, dar ni s-au dat informaţii banale,
pe care le puteam afla şi de la o persoană de pe stradă.
Solomon Marcus: Vedeţi, aici vă ajută internetul,
puteţi afla acum totul de pe internet.
O elevă: Am avut în clasa a opta, la teza cu subiect
unic, un text în care un parvenit social s-a dus într-o
1093

Solomon Marcus

librărie şi a cumpărat cărţi cu cotoare verzi, ca să se
asorteze cu tapetul din cameră.
Solomon Marcus: Îmi amintesc că, înainte de
revoluţie, a apărut Getica lui Pârvan. S-a făcut coadă la
librăria Eminescu, era un număr imens de oameni care
cumpărau Getica lui Pârvan. Ştiţi de ce?
O elevă: Pentru că era gratis.
Solomon Marcus: Nuu! Costa foarte mult. Dar ştiţi de
ce o cumpărau? Ca să o ofere cadou unui medic, pentru că
obiceiul era să oferi un cartuş de Kent, dar unii nu erau
fumători, sau, mă rog, anumite persoane îşi spuneau că e
mai nobil să le oferi Getica lui Pârvan…
C. Missbach: Ca şi Istoria Literaturii a lui George
Călinescu…
Solomon Marcus: …pe care, evident, nu o deschideau
niciodată!
C. Missbach: Stimate domnule profesor, sper că v-au
plăcut copiii.
Solomon Marcus: Da, mi-au plăcut chiar foarte mult.
Cuvântul lectorului univ. Sorina Woinaroski către Solomon
Marcus (mesaj electronic din 18 februarie 2010)
Sorina Woinaroski: Am venit cu 10 minute mai
înainte, am cerut permisiunea doamnei profesoare să asist,
m-a primit, elevii au răspuns la salut, erau prezentabili şi
1094

Rãni deschise

în relaţii bune cu profesoara. Şi ţi-ai făcut intrarea în scenă.
Am constatat încă o dată stilul tău, la care nu rezistă nimeni.
Ştiu de mult cum este, şi totuşi mă uimeşti de fiecare dată.
Lângă mine, erau doi elevi, care la început se jucau
pe sub bancă cu telefonul mobil; n-am cronometrat cât
timp ţi-a trebuit ca să-i determini să-l închidă şi să te
asculte cu evident interes; n-a durat decât câteva minute.
Evident că ei nu mai auziseră asemenea lucruri. De unde
să aibă ei astfel de profesori? Chiar şi cei mai buni dintre ei
n-au cum face faţă. Şi nu-i vorba numai de pricepere şi de
bunăvoinţă, dar au o mulţime de restricţii impuse de
programă, de cerinţele şcolii, de părinţi şi încă altele.
Cred însă că le-ai pus în faţă probleme care nu sunt
încă de competenţa lor.
Ca să ai păreri, ca să ceri anumite lucruri, să faci
alegeri, să critici, e nevoie de oarecare instrucţie prealabilă,
de studiu, de un oarecare discernământ.
Cum să alegi între nişte lucruri de care nu ai habar?
Le spui că învăţarea presupune plăcere; te înţeleg şi
te aprob din inimă. Dar asta nu vine automat, ci cu oarecare
efort iniţial şi nu la toată lumea. Când ai spus asta, s-a
comentat lângă mine: „Poftim! Şi pe noi ne obligă să
învăţăm, nu ne lasă să o facem liberi şi ca o distracţie.”
De oarecare vreme mă feresc să folosesc cuvântul
„distracţie”, cuvânt inocent în alte vremuri. Astăzi e folosit,
mai ales de către cei tineri, într-un mod diferit. E impus şi
1095

Solomon Marcus

de televiziuni în acest mod. Distracţie înseamnă „chef”:
muzică urlată, răcnete, băutură ...and more! Tot distracţie
este să-l prinzi pe unul mai slab de înger şi să-l
batjocoreşti.
Desigur că se comenta pretenţia unor profesori de a
se susţine teze cu note la unele materii.
Ăsta e un lucru pe care eu nu-l pricep. „Pe vremea
mea” se dădeau teze la toate materiile, învăţam cu plăcere
(exact aşa cum spui) multe dintre materiile care-mi
plăceau, ele sau profesoara, şi mai pe apucate pe celelalte,
şi nimănui nu-i trecea prin cap că nu aşa ar trebui să stea
lucrurile. Eram cea mai bună din clasă la germană şi la
rusă, pentru că le iubeam pe profesoare. Aşa se face că nu
am avut nicio problemă să citesc cărţi ruseşti de
matematică.
Solomon Marcus: Cred că spiritul critic ne însoţeşte
de la naştere. Ne naştem plângând, manifestând în acest
fel incapacitatea de adaptare la noul mediu. Apoi spiritul
critic ia forma întrebărilor care, regretabil, îi enervează de
multe ori pe cei din jur. Apoi protestăm pentru că nu
înţelegem. Sigur că formularea opţiunilor vine mai târziu.
Aş mai adăuga ceva în legătură cu binomul plictisealădistracţie, care revine obsedant în cartea scrisă de elevi. Le-am
spus că mie starea de plictiseală mi-a rămas ceva străin.
E necesar să mai adaug că şi nevoia de distracţie
mi-a rămas străină. De ce? Pentru că mi-am satisfăcut nevoile
1096

Rãni deschise

ludice prin frecventarea ştiinţei, artei şi filosofiei. În copilărie
îmi plăcea mult circul, dar şi lectura. Lectura era una culturală
şi artistică. Tot ceea ce am frecventat mi-a angajat intelectul,
orizontul meu cultural, înţelegerea artistică.
În 1975 am vizitat Disney World, în statul Florida,
din Statele Unite. Să-i spun distracţie? Eu îi spun cultură,
a fost o experienţă extraordinară în materie de universuri
alternative.

1097

