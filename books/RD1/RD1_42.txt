Rãni deschise

Greşeli fecunde
Idei în dialog, nr. 12, decembrie 2009

Despre ce fel de greşeli este vorba?
Am observat că, de multe ori, mari idei, texte care
deschid drumuri noi conţin greşeli, dar nu atât de grave
încât să anuleze sau măcar să micşoreze caracterul de
pionierat al ideilor respective. Dimpotrivă, aceste greşeli
apar ca un ingredient aproape inevitabil al noutăţii
propuse. Explicaţia ar fi următoarea: o idee de o mare
noutate comportă, în prezentarea ei, o anumită stângăcie,
o dificultate de limbaj, o tatonare, o nesiguranţă şi deci o
ezitare, acestea confiscă atenţia autorului şi, ca urmare,
apar greşeli în partea care beneficiază mai puţin sau deloc
de atenţie.
Un alt fenomen, dar strâns legat de cel de mai sus, pe
care ne propunem să-l ilustrăm, este capacitatea unor
greşeli de a ascunde adevărate mine de aur, în sensul că
acolo unde ele se produc se află, potenţial, punctul de
plecare al unor noi drumuri interesante, al unor noi
289

Solomon Marcus

domenii sau noi importante rezultate. Acest lucru este şi
el de înţeles, este chiar de aşteptat, deoarece, atunci când
un mare om de ştiinţă (cum va fi cazul în exemplele pe
care le vom da) greşeşte, este foarte plauzibil că faptul a
avut loc acolo unde se află anumite capcane, anumite
situaţii mai dificile, mai delicate, de o semnificaţie mai
profundă, care, deci, pentru a nu ne scăpa, cer o atenţie
mărită. Greşeala are, în acest caz, o funcţie de semnalizare,
de avertisment. Aţi observat, în trafic, semnul care anunţă
conducătorilor de vehicule apropierea unei cotituri
periculoase? Acest semnal a fost preluat, într-un celebru
tratat de matematică al lui Nicolas Bourbaki, pentru a
semnala „cotiturile periculoase” ale subtilităţilor de
raţionament care urmează în text şi care cer o vigilenţă
deosebită. De acest fel sunt locurile în care, cu predilecţie,
matematicienii pot greşi.
Faptul că exemplele pe care le voi da sunt din
matematică, din logică, din lingvistică, din informatică se
explică în primul rând prin familiaritatea mea cu aceste
domenii. Dar mai este şi faptul, deloc neglijabil, că în
matematică şi în disciplinele conexe distincţia dintre
greşeală şi corectitudine este mai clară decât în orice alte
domenii. Aici nu se poate confunda greşeala cu diferenţa
de impresie, de opinie sau de punct de vedere. Se joacă
„cu cărţile pe masă”, cum observa matematicianul Iuri
Manin, într-o altă ordine de idei. Dar ar fi un exerciţiu
290

Rãni deschise

intelectual de mare interes testarea consideraţiilor de faţă
în alte domenii.
Greşeala lui Arhimede
Nu există un simbol mai clar al perfecţiunii, al
armoniei şi al simplităţii decât cercul şi sfera. Prezente în
mai toate tradiţiile culturale, ele au fascinat pe artişti şi pe
savanţi. Vechii greci (şi poate şi alte civilizaţii, înaintea
lor) au descoperit un fapt uimitor: toate cercurile, deci nu
numai cele observabile, ci şi cele numai imaginabile, dar
şi cele numai posibile, însă situate dincolo de imaginaţia
umană, au o proprietate comună: raportul dintre lungimea
şi diametrul lor este acelaşi pentru oricare dintre ele.
Această valoare care simbolizează universalitatea
circularităţii a fost notată, începând cu secolul al
XVIII-lea, cu litera grecească π, număr care intervine în
exprimarea tuturor parametrilor fundamentali ai cercului
şi sferei, ai cilindrului şi conului.
În aceste condiţii, era de aşteptat ca stabilirea valorii
lui π să devină o preocupare importantă. În tratatul său
Măsura cercului, Arhimede propune pe 3,1416 ca valoare
aproximativă a lui π. A trebuit să treacă multă vreme până
să se descopere că Arhimede se înşelase asupra cifrei de
pe locul 4, după virgulă: nu era 6. Greşeala lui Arhimede
a stimulat cercetarea zecimalelor succesive din
291

Solomon Marcus

aproximarea lui π. Importanţa acestei goane după
zecimalele lui π a crescut considerabil atunci când, în
secolul al XVIII-lea, s-a aflat că π este un număr iraţional,
deci că dezvoltarea sa zecimală este infinită şi neperiodică.
Nu demult, se ajunsese la cunoaşterea a sute de milioane
de zecimale ale lui π, dar din când în când se descoperă
greşeli în evaluările anterioare. De exemplu, progresul
metodelor de evaluare i-a permis lui D.F. Ferguson să
descopere în 1945 că, dintre cele 607 zecimale ale lui π
propuse de W. Shanke în 1853, cifrele începând cu rangul
527 erau toate greşite. Greşeala a fost mereu un stimulent
de a descoperi metode tot mai bune de aproximare a lui π.
Greşeala lui Cantor
Mii de ani considerat inaccesibil minţii umane,
infinitul actual a devenit obiectul sistematic de studiu al
lui Georg Cantor, în ultima parte a secolului al XIX-lea.
În lucrările sale de pionierat (care i-au împărţit pe
matematicieni în două tabere, unii, ca Dedekind şi Hilbert,
admirându-le, alţii, ca Kronecker şi Poincaré, respingându-le) privind teoria numerelor cardinale transfinite şi
a celor ordinale transfinite, Cantor a comis o infracţiune
chiar în modul de definire a numărului cardinal, mod care
implica un cerc vicios (Frege căzuse şi el în aceeaşi
capcană). Abia ulterior această situaţie a fost corectată.
292

Rãni deschise

Dialogul lui Cantor cu Vaticanul rămâne un exemplu de
interacţiune interesantă a ştiinţei cu teologia. Inexistenţa
unui cel mai mare număr cardinal transfinit a surprins prin
caracterul ei insolit.
Greşeala lui Hilbert
Putem transfera ideea de consecutivitate de la numere
naturale la numere cardinale transfinite? Admiţând că
numărabilul are, în lumea transfinită, rolul lui zero în
domeniul numerelor naturale, care este numărul cardinal
transfinit următor numărabilului? Aceasta este faimoasa
problemă a continuului, prima pe lista de 23 de probleme
formulate de David Hilbert în urmă cu peste o sută de ani.
Nu s-a găsit încă răspunsul la ea. Hilbert a crezut că l-a
găsit şi l-a publicat în 1926, dar se înşelase, după cum a
arătat Gustave Choquet în 1945. Dar, prin greşeala sa,
Hilbert l-a stimulat pe Paul Cohen, care, la începutul anilor
’60 ai secolului trecut, provocat şi de un rezultat anterior
al lui Kurt Gödel (din 1939), a stabilit o teoremă care,
împreună cu aceea a lui Gödel, implica independenţa
ipotezei conform căreia între numărabil şi continuu nu
există un număr cardinal intermediar. Prima problemă a
lui Hilbert rămâne în continuare o provocare: se va putea
reorganiza axiomatic teoria mulţimilor astfel încât la
întrebarea „există un număr cardinal transfinit cuprins
293

Solomon Marcus

strict între numărabil şi continuu?” să se poată răspunde
prin da sau nu?
Greşeli la cei care au marcat informatica şi
lingvistica: Turing şi Chomsky
Părintele calculatoarelor electronice, Alan M. Turing,
şi-a lansat proiectul printr-un articol de pionierat din 1936
– 1937, în Proceedings of the London Math. Society, dar a
revenit imediat cu o corectare esenţială, într-un număr
următor al aceleiaşi reviste. Părintele gramaticilor
generative, Noam Chomsky, şi-a lansat proiectul care avea
să marcheze lingvistica şi informatica printr-un articol din
1956, dar, în ciuda caracterului său de pionierat, acest
articol abundă în greşeli sintactice şi semantice, unele
benigne, altele maligne, dar care nu afectau provocarea
majoră pe care o lansa autorul: trecerea de la o lingvistică
descriptivă şi analitică, axată pe structură, la una
generativă, axată pe competenţă. Chomsky punea atunci
bazele teoriei limbajelor de programare la calculator.
A fost nevoie de 17 ani pentru corectarea greşelilor
sintactice ale lui Chomsky; acest efort pe durata a 17 ani a
culminat cu monografia Formal Languages a lui Arto
Salomaa (1973). Dar rămâneau greşelile semantice,
privind modul de interpretare, pentru limbaje naturale, a
noţiunilor din teoria generativă, elaborată pentru limbaje
294

Rãni deschise

formale. Acestea au fost detectate abia în anii ’80, iar
controversele în jurul lor sunt încă vii. Dar, independent
de evoluţia ulterioară a acestor controverse, efectul lor
benefic pentru cunoaşterea mai profundă a limbajului
natural este incontestabil.
Unde dai şi unde crapă: de la problema lui Fermat
la chimie şi filosofie
Ernst Kummer (secolul al XIX-lea) a eşuat în
încercarea sa de a rezolva problema lui Fermat. Dar cât
de bine inspirat a fost eşecul său! Pe parcursul demersului
său lipsit de succes, Kummer a introdus un concept care
s-a dovedit esenţial în teoria numerelor, cel de număr
ideal, şi în algebră: ideal într-un inel. Dar efectul
conceptului lui Kummer s-a manifestat şi dincolo de
matematică: la 5 aprilie 1928, Harris Hancock, de la
Universitatea din Cincinnati, prezintă la Secţiunea din
Ohio a Asociaţiei de Matematică a Americii o
comunicare cu un titlu de o semnificaţie surprinzătoare:
The Analogy of the Theory of Kummer’s Ideal Numbers
with Chemistry and its Prototype in Plato’s Concept of
Idea and Number. Pe de altă parte, chiar cel care a reuşit,
relativ recent, să demonstreze teorema lui Fermat,
Andrew Wiles, a fost, în prima sa tentativă, victima unei
greşeli, dar efortul de a o corecta a fost pe deplin
295

Solomon Marcus

răsplătit: varianta finală, deci cea corectă, a demonstraţiei
a fost mai simplă decât cea iniţială. Din nou, greşeala
s-a dovedit a fi fecundă.
Încercând să înlăture o greşeală,
Poincaré a creat ştiinţa haosului
La a 60-a aniversare a Regelui Oscar al Suediei şi
Norvegiei, s-a organizat o competiţie la care Henri
Poincaré a candidat cu un memoriu propus pentru
publicare în revista scandinavă Acta Mathematica, având
ca temă faimoasa problemă a celor trei corpuri (cum se
mişcă trei corpuri aflate în interacţie gravitaţională
mutuală, cum ar fi Soarele, Pământul şi Luna?), problemă
încă nerezolvată. Poincaré câştigă competiţia, lucrarea sa
intră la tipar, dar, chiar în timpul procesului de tipărire,
matematicianul suedez Phragmen identifică în manuscrisul
lui Poincaré o greşeală destul de gravă. I-o semnalează lui
Poincaré, care se dedică imediat încercării de înlăturare a
respectivei lacune. Între timp, se şi tipărise un prim tiraj al
acelui număr din Acta Mathematica, dar Poincaré a cerut
să nu fie difuzat. După multă trudă, el reuşeşte să elimine
greşeala, dar răsplata i-a fost pe măsură: noua versiune
constituia actul de naştere a ceea ce numim azi teoria
haosului. Poincaré a plătit din propriul buzunar retipărirea
cu versiunea corectă a numărului respectiv din Acta
296

Rãni deschise

Mathematica. A costat, dar merita! Toate acestea se
întâmplau spre sfârşitul secolului al XIX-lea.
Geometria fractală şi păcatele începutului
,
În anii 70, Benoît Mandelbrot publica lucrarea sa de
pionierat, The Fractal Geometry of Nature, unde se
propunea o alternativă la geometria tradiţională, bazată pe
idealul de frumuseţe al Greciei antice, asociat cu
simplitatea, simetria şi armonia. Renaşterea a preluat şi ea
acest ideal de frumuseţe. Figurile geometrice regulate au
fost astfel plasate în centrul atenţiei. Simetriile hexagonale
ale cristalelor au fost asociate cu lumea inertă, iar cele
pentagonale au devenit reprezentative pentru lumea vie.
Secolul al XIX-lea a marcat o modificare, de exemplu,
prin Florile răului de Charles Baudelaire, în poezie (a se
vedea, ulterior, în poezia românească, Flori de mucigai de
Tudor Arghezi), şi prin mulţimile şi funcţiile „urâte”, în
sensul că reprezentările lor vizuale se derogau de la
dezideratul tradiţional de simplitate şi de regularitate sau,
mai grav, se sustrăgeau, pur şi simplu, oricărei reprezentări
vizuale. Aici se plasează graficele funcţiilor continue, dar
fără derivată (Karl Weierstrass) sau curbele care umplu un
pătrat (Giuseppe Peano). Unii matematicieni ai secolului al
XIX-lea, precum Charles Hermite, respingeau aceste
obiecte, plasându-le în afara teritoriului matematicii, sub
297

Solomon Marcus

motivul că ele nu ar corespunde lumii reale. Geometria
fractală propusă de Mandelbrot argumentează în mod
convingător că numeroase obiecte, de la nori la fulgii de
zăpadă şi de la coastele oceanelor la mişcarea browniană,
tocmai de astfel de modele matematice au nevoie. Dar
marea surpriză o constituie faptul că definiţia propusă iniţial
pentru obiectele fractale, care lua drept criteriu de
fractalitate natura dimensiunii lor, căreia i se cerea să nu fie
exprimabilă printr-un număr întreg, era ulterior înlocuită
cu o alta, în care accentul cade pe proprietatea de autosimilaritate a obiectelor fractale. De această dată nu mai
este vorba de o greşeală de raţionament, ci de o ezitare de
natură conceptuală – fapt care se poate accepta atunci când,
ca aici, o teorie se află la primii săi paşi. Deocamdată deci
nu există un consens în această privinţă. Cuvântul fractal
este o invenţie a lui Mandelbrot, care l-a format după
latinescul fractus. Aplicaţiile geometriei fractale sunt dintre
cele mai neaşteptate, de exemplu în domeniul finanţelor.
Unii aşteaptă ca Mandelbrot să primească Premiul Nobel
pentru economie.
Cum ar putea fi stearpă corectitudinea?
La prima vedere, nu i se poate reproşa nimic
corectitudinii. În numeroase cazuri, poate că în cele mai
multe, corectitudinea, înţeleasă ca respectare a unor reguli
298

Rãni deschise

prestabilite (ele pot fi reguli explicite sau implicite de
comportament social, reguli gramaticale, reguli de
raţionament etc.), este necesară, iar educarea ei este unul
dintre principalele atribute ale şcolii. Dar corectitudinea
ţine numai de igiena vieţii sociale, numai de igiena
exprimării, numai de igiena gândirii noastre. Ea,
corectitudinea, nu este aceea care constituie carnea, fondul
vieţii noastre, acestea din urmă având nevoie de
semnificaţii, de idei, de spiritualitate, de afectivitate, de
valori morale şi estetice. Acestea sunt hrana, în absenţa
căreia putem muri de foame, în condiţiile celor mai
perfecte norme de igienă. Numai că semnificaţiile, ideile,
spiritualitatea, afectivitatea, valorile morale sau estetice nu
pot fi supuse unor reguli de tipul acelora care guvernează
corectitudinea de orice fel. Baremele folosite la evaluarea
celor care au de trecut diverse examene se prevalează de
ceea ce este reglementat, pentru a obţine obiectivitatea şi
comoditatea evaluărilor. Se ajunge astfel de multe ori, aşa
cum se poate vedea, de exemplu, în numeroase manuale
şcolare, la proliferarea corectitudinii sterpe, adică fără
acoperire semantică.

299

