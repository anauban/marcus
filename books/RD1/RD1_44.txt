Solomon Marcus

Sub egida

Astăzi e ziua ta: Solomon Marcus!
Jurnalul Naţional, 1 martie 2010

„Mi-e dor de anii fericiţi ai copilăriei, care mi-au
hrănit toate elanurile ulterioare”, mărturiseşte la ceas
aniversar Solomon Marcus. Renumitul academician român
împlineşte astăzi 85 de ani. „Jurnalul Naţional” îi urează
„La mulţi ani!”.
„Descoperirea poeziei...
un moment fericit al vieţii mele”
„La 1 martie 2010, lansez la Colegiul Naţional
„Sf. Sava” o carte de un fel deosebit. Ea are titlul Educaţia
în Spectacol şi este editată de Lavinia Spandonide, director
al Editurii Spandugino. Cartea include dezbaterile pe care
le-am avut cu oameni ai şcolii în anii 2009 şi 2010 şi
exprimă pariul meu educaţional, pentru care am primit în
octombrie 2009 Premiul LYKEION al Alianţei Colegiilor
Centenare.
306

Rãni deschise

Despre... România
Unul dintre lucrurile importante pe care le-am desprins
din întreaga mea activitate este faptul, confirmat acum pe
diverse căi, că educaţia este principala problemă a României.
În momentul de faţă, constatăm cu durere că prestaţia noastră
în această privinţă este în mare măsură un eşec. Mă bucură
că tot mai mulţi sunt cei care aderă la proiectul meu
educaţional. Atenţia îmi este concentrată pe fapte ca: prea
mulţi părinţi nu sunt capabili să fie prietenii copiilor lor; prea
mulţi profesori nu reuşesc să devină prietenii elevilor lor;
educaţia se bazează prea mult pe poruncă şi pe sancţiune şi
prea puţin pe rugăminte, întrebare, îndoială; educaţia nu
reuşeşte încă să valorifice dimensiunea ludică, să descopere
faţa pozitivă a greşelii, ca simptom al gândirii personale; nu
se face educaţia spiritului critic; atmosfera predominantă în
şcoală este mai degrabă una mohorâtă şi gravă decât una de
bună dispoziţie, de bucurie, de autoironie; programele şi
manualele sunt marcate de toate aceste păcate.
Dincolo de dificultăţile prin care am trecut, într-o viaţă
care a inclus cel de-al Doilea Război Mondial şi lunga noapte
comunistă, am beneficiat de cel puţin două alegeri fericite:
alegerea matematicii ca ocupaţie, în 1944, şi alegerea
partenerei mele de viaţă, Paula Diaconescu, în 1966. Un
moment fericit a fost descoperirea, încă din adolescenţă, a
poeziei, a literaturii în general, şi a filosofiei.”
307

