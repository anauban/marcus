Rãni deschise

Semiotica în spectacol
România literară, nr. 32,
17 august 2007

A comunica, a înţelege
Aproape 600 de comunicări au fost prezentate la cel
de-al nouălea Congres Mondial de Semiotică, desfăşurat
la Helsinki, în perioada 11-13 iunie şi, în continuare, la
Imatra, 14-17 iunie 2007.
Congresele internaţionale de semiotică au fost
inaugurate în anul 1974, când Umberto Eco a organizat, la
Milano, primul congres de acest fel. Alegerea Finlandei ca
loc al congresului din acest an a fost urmarea naturală a
alegerii, la cel anterior (de la Lyon), ca preşedinte al
Asociaţiei Internaţionale de Semiotică, a profesorului Eero
Tarasti de la Universitatea din Helsinki.
Fiecare congres de acest fel se desfăşoară sub semnul
unui anumit slogan. De această dată, sloganul a fost:
,,Communication, Understanding, Misunderstanding”.
Observaţi dificultatea găsirii unor termeni echivalenţi
155

Solomon Marcus

ultimilor doi în limba română. Vom recurge de aceea la
forma infinitivului: A comunica, a înţelege, a înţelege
greşit.
Un numitor comun
O simplă privire asupra programului şi o vizită la
biblioteca de semiotică a Centrului cultural de la Imatra, o
parcurgere a foilor publicitare şi a afişelor anunţând cele
mai recente apariţii au fost suficiente pentru a căpăta o
imagine a dezvoltării prodigioase a semioticii în ultimele
decenii. Periodice, monografii, manuale, enciclopedii şi
dicţionare pe teme semiotice sunt din ce în ce mai
frecvente. Semiotica a reuşit să aducă în raza ei de
cuprindere nu numai discipline umaniste clasice, precum
cele relative la limbaj, filosofie, antropologie, sociologie,
psihologie, drept, literatură şi arte, dar şi ştiinţele exacte şi
ale naturii, biologia, medicina, informatica, ştiinţele
cognitive, inteligenţa artificială. Domenii recente de mare
actualitate, cum ar fi calculabilitatea biologică şi biologia
computaţională, capătă şi ele o privire proaspătă din partea
semioticii. Propunându-şi să înţeleagă modul de
constituire, de funcţionare şi de evoluţie a sistemelor de
semne de orice fel, semiotica descoperă o albie comună
pentru toate disciplinele, deoarece nu se poate imagina un
domeniu din natură sau din societate care să se sustragă
156

Rãni deschise

unei reprezentări semiotice. Dar această generalitate şi
chiar universalitate a proceselor semiotice este în acelaşi
timp un potenţial avantaj şi o posibilă capcană. Câştigăm
o înţelegere unitară a unor procese aparent foarte
eterogene, dar riscăm ca unitatea pe care o obţinem să fie
superficială, să se apropie de banalitate. De aceea,
cercetările semiotice trebuie evaluate cu prudenţă, pentru
a nu fi victime ale unei false noutăţi. În oricare dintre
variantele ei, cea a lui Charles Sanders Peirce (pentru a ne
referi la cea mai importantă) sau cea a lui Algirdas
Greimas, semiotica şi-a creat un aparat conceptual şi o
terminologie care pretind o iniţiere prealabilă; dar, aşa cum
arborarea de semne şi termeni din matematică nu exprimă
întotdeauna o autentică gândire matematică, tot aşa nici
folosirea jargonului semiotic nu se asociază întotdeauna
cu o procedare semiotică interesantă. Congresul a fost
bogat în exemple de toate felurile.
O tematică variată
Dintre temele abordate, vom menţiona: semiotica
fino-ugrică (Vilmos Voigt), discursul istoric (Youzheng Li),
sinestezie şi multimedia (Yuri Tsivian), nostalgie, bucurie
şi melancolie în tango-ul finlandez (Pirjo Kukkonen), de la
semantică la semiotica politică – aplicaţie la Adam Schaff
(Susan Petrilli şi Augusto Ponzio), filosofie academică şi
157

Solomon Marcus

culturală în Finlanda (Ilkka Niiniluoto), semiotica
paradoxului (Antti Salminen), denotaţie şi referinţă în
sisteme nelingvistice (Alexandros Lagopoulos şi Karin
Boklund-Lagopoulos), diagramatologie (Paolo Fabbri), o
întâlnire ratată: semiotica şi psihanaliza (Cristina Alvares),
cum poate fi înţeleasă o metropolă (Roland Posner), urban
şi rural (Dong-yoon Kim), abducţie şi meta-abducţie în
opera romanescă a lui Umberto Eco (Ilias Yocaris); de la
tăcere contemplativă la interacţie cinetică: Jesus Soto
(Julian Cabeza şi Lourdes Molero), specificitatea semiotică
a textului fotografic (Garcia de Molero şi Farias de Estany).
Mese rotunde şi sesiuni speciale au fost dedicate unor teme
ca: semiotica socială, literatura coreeană, comunicare şi
spectacol, muzica contemporană, logica modală şi
teoremele lui Gödel, comunicarea pe internet, înţelegerea
celuilalt, teorii ale minţii, semiotica şi hermeneutica în
comunicarea interculturală, neurosemiotica, identitate,
semnele sănătăţii, concepte pragmatic-semiotice în artă,
biosemiotica, spaţiul urban, vorbire şi discurs, publicitate
şi marketing, animale şi peisaj în artă, mitologie şi folclor,
semne ale corpului şi ale incorporării, gestualitate şi tăcere,
cinema, narativitate muzicală, antroposemiotică, jurnalism
şi media, modă, design, falsificarea înţelegerii în literatură
şi înţelegerea falsă ca literatură, analiza comparativă a
semioticii chineze şi a celei occidentale, traducerea ca
înţelegere falsă, avangarda, comunicarea om-maşină,
158

Rãni deschise

memorie şi educaţie, arhitectură, înţelegerea oraşelor,
Claude Lévi-Strauss, religie, film, text-sunet-imagine,
alteritate şi înţelegere în literatură, la hotarele semioticii,
ritualuri, discursul politic. O sesiune de amploare, care a
durat trei zile, a fost dedicată operei lui Peirce, în cadrul
căreia o atenţie specială a fost acordată abducţiei, artei şi
literaturii, logicii, eticii, jocurilor şi virtualităţii,
epistemologiei şi cosmologiei, pentru a se încheia cu
discutarea editării actuale a operei lui Peirce, sub forma
Writings of Charles Sanders Peirce, care recuperează o
serie întreagă de scrieri absente din ediţia anterioară,
cunoscută sub titlul de Collected Works.
Dar cine putea să urmărească toate acestea? Lucrările
s-au desfăşurat în multe secţiuni paralele şi numai când
vor fi publicate Proceedings-urile Congresului vom putea
lua cunoştinţă de întreaga bogăţie acumulată în ele. Există
însă cartea de peste 500 de pagini cu rezumatele unora
dintre comunicări, rezumate care pot da o idee despre
noutăţile prezentate.
Participarea românească
Mulţi dintre românii care se ocupă de semiotică au
lipsit de la acest congres; totuşi, am avut satisfacţia de a
constata prezenţa mai multor universitari de la Iaşi,
Cluj-Napoca, Bacău, Galaţi şi Bucureşti, la care adăugăm
159

Solomon Marcus

şi un român profesor la o universitate pariziană; toţi au atras
atenţia prin comunicări interesante. Rodica Amel
(Bucureşti) a vorbit despre relaţia dintre folosirea semnelor
şi inventarea lor. Camelia M. Cmeciu şi Doina Cmeciu
(Bacău) au analizat manipularea comunicării prin imagini,
cu referire la campania electorală din 2004, pentru alegerea
primarului oraşului Bucureşti. Aurel Teodor Codoban
(Cluj-Napoca) a investigat ,,corpul limbajului” şi ,,corpul
ca limbaj”, discutând unele probleme de constituire în
semiotica gestuală. Solomon Marcus a analizat relaţia
dintre semiotica existenţială şi matematică. Solomon
Marcus (Bucureşti) şi Sanda Monica Tătărâm (Bucureşti)
au pus în evidenţă trei posibilităţi ale comunicării prin
calculator: succes, distorsiune şi eşec total. Costin
Miereanu (Paris) a condus secţia de ,,poly-art şi
tehnologie”. Diana-Elena Popa (Galaţi) a vorbit despre
comunicarea mediată de calculator şi relaţia ei cu sfera
publică. Pavel Puşcaş a prezentat o abordare semiotică în
hermeneutica tonalităţilor muzicale. Traian D. Stănciulescu
(Iaşi) a discutat despre metodologia semiotică, posibil, dar
încă neglijat instrument al puterii. Traian D. Stănciulescu
şi Aritia D. Poenaru (Iaşi) au analizat diferite aspecte ale
limbajului arhetipal. Gheorghe Vlasie (Iaşi) a analizat unele
dimensiuni ale semnului, în legătură cu fenomenul de
redundanţă semiotică. Până în urmă cu câţiva ani, nu a
existat o Asociaţie Română de Semiotică; a fiinţat numai
160

Rãni deschise

un Grup Român de Semiotică, în cadrul Societăţii Române
de Lingvistică. Recent, s-a constituit Asociaţia Română de
Semiotică (AROS), având ca preşedinte pe profesorul
Traian D. Stănciulescu de la Universitatea „Alexandru Ioan
Cuza”, Iaşi, şi ca secretar general pe profesoara Doina
Cmeciu, de la Universitatea din Bacău (aceştia fiind cei
care au avut iniţiativa întregii acţiuni), subsemnatului
revenindu-i preşedinţia de onoare a AROS. În octombrie
2006 a avut loc la Slănic-Moldova prima Conferinţă
Internaţională de Semiotică organizată de AROS, ale cărei
lucrări au fost publicate într-un volum de peste 600 de
pagini, prezentat la congresul din Finlanda. Să sperăm că
AROS va reuşi să recupereze cvasitotalitatea cercetătorilor
în semiotică din România (pe mulţi dintre ei i-a şi
recuperat) şi să procedeze la organizarea unor activităţi
specifice şi a unei reviste de profil. Dificultăţile în această
direcţie provin din diversitatea specialităţilor care sunt
implicate în demersul semiotic, o parcurgere transversală a
culturii, încă tributară prea mult segmentării pe discipline.
În faţa unui examen
În afara comunicărilor, Congresul din Finlanda s-a
remarcat printr-un mare număr de manifestări artistice, de
la teatru şi film la concerte şi expoziţii. Mai mult, destule
comunicări s-au transformat chiar ele în spectacole vizuale
161

Solomon Marcus

sau / şi sonore. Acest fapt este în tradiţia congreselor de
semiotică. Noile medii ale erei digitale, provocările lumii
cuantice şi ale celei moleculare, ale globalizării
comunicaţionale şi cognitive s-au manifestat cu putere în
multe dezbateri de la Congresul din Finlanda. Teoriile
semiotice existente, de la cele pe care ni le-a lăsat lumea
antică, trecând prin cele medievale şi clasice, până la cele
apropiate de timpurile noastre, se află acum în faţa unui
examen pe care nu-l vor putea trece fără o reconsiderare
critică a lor. În această direcţie a fost cu precădere orientat
congresul în discuţie.
În cadrul Adunării plenare de inaugurare a
Congresului, după Cuvântul Preşedintelui şi saluturile de
rigoare, au fost prevăzute şi două intervenţii de câte
15 minute, privind Comunicarea (Augusto Ponzio, Italia)
şi Înţelegerea (Solomon Marcus, România). Sper ca
într-un număr ulterior al României literare să am
posibilitatea de a prezenta textul intervenţiei mele pe
această temă.
(n.n.: A apărut în Idei în dialog, aprilie 2008,
reprodus în cartea de faţă.)

162

