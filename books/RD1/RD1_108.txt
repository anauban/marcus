Solomon Marcus

Întâlnire cu Mihaela Sîrbu
Bucureşti, Sediul Editurii Spandugino, februarie 2010

S.M.: Mă bucur foarte mult să vă cunosc, pentru că
v-am citat în cartea mea Educaţia în spectacol, cu un lucru
pe care v-am auzit spunându-l în cadrul unui interviu.
Nenumăraţi tineri care termină şcoala spun că ea este
inutilă, dar dvs. aţi acuzat-o chiar că e nocivă…
M.S.: Ei, nu, n-aş spune chiar aşa. Am spus că, în
şcoală, elevii îşi pierd creativitatea, pentru că şcoala e
orientată cu precădere pe disciplină şi …
S.M.: Păi, dacă îşi pierd creativitatea, ce înseamnă
asta? Atunci care mai e rostul şcolii? După părerea mea,
principalul ei pariu este să valorifice creativitatea, nu?
M.S.: Ar fi bine să fie aşa, dar nu poate, pentru că tot
sistemul este structurat pe disciplină şi pe imitaţie
S.M.: Elevii au nevoie de informaţii şi de deprinderi,
asta înseamnă creativitate! Înseamnă capacităţi care să-ţi
dea posibilitatea să te descurci în situaţii inedite.
1058

Rãni deschise

M.S.: De-asta facem noi, la „Teatrul fără frontiere”,
improvizaţie şi organizăm cursuri atât pentru copii, tineri,
cât şi pentru adulţi, prin care încercăm să dezvoltăm
tocmai ce se pierde prin sistemul şcolar.
S.M.: Noi le spunem mereu copiilor: Voi veţi trăi
într-o societate în care tabloul profesiilor va fi cu totul altul
şi va trebui să practicaţi profesii care azi nu există sau,
chiar dacă există, vor arăta cu totul altfel…
M.S.: Asta aşa este! La cursurile noastre vin foarte
mulţi adulţi, iar în privinţa profesiilor, tabloul lor se
schimbă de la an la an… şi vedeţi, de-asta propunem
improvizaţia…!
S.M.: Am făcut lista acuzaţiilor care i se aduc şcolii,
dar dintre toate, a dvs. e cea mai gravă! Vă dau câteva
exemple, pe care le iau în ordine cronologică.
– Valentin Lazea, economistul şef al Băncii Naţionale a
României, observă că avem o productivitate scăzută,
cauzată de deficienţele sistemului de educaţie.
– Ministrul Muncii spune, pe de altă parte, că şcoala
nu-i pregăteşte pe elevi pentru piaţa muncii.
– Cristian Ţopescu acuză filmele de desene animate,
vizibile la ore de maximă audienţă, pentru că în ele
predomină violenţa.
– Apoi veniţi dvs. şi spuneţi: ,,Copiii îşi pierd creativitatea
la şcoală, iar noi încercăm să o salvăm”.
1059

Solomon Marcus

– Şi, în sfârşit, fostul rector al Universităţii spune că
80% din diplomele universitare româneşti sunt fără
acoperire, deci diplomele nu dau competenţa
conferită în Occident.
Vedeţi, eu am adus şcolii exact acelaşi reproş pe
care-l aduceţi şi dvs., dar exprimat în alte cuvinte. Şi
anume că, până intră la şcoală, copilul manifestă o
curiozitate şi o inventivitate extraordinare, are o atitudine
predominant interogativă; în momentul în care intră la
şcoală, această atitudine interogativă îi este descurajată şi
înlocuită cu o atitudine imperativă: Trebuie să faci asta!,
Fă cutare lucru!, Fă aşa! …
M.S.: Nu mai este chemat să-şi aducă el însuşi
contribuţia, nu mai este încurajat să gândească singur, ci i
se dă cu linguriţa ce trebuie să facă şi cum să gândească…
S.M.: Da, am mai spus că, aşa cum în justiţie
funcţionează prezumţia de nevinovăţie, în educaţie ar
trebui să se adopte prezumţia rezervei critice, fără de care
formarea gândirii critice nu e posibilă.
Ce înseamnă asta? Înseamnă dreptul copilului de a
formula întrebări, de a spune: „Nu înţeleg” sau „Nu sunt
de acord”, or, în general, el este obligat, în primul rând, să
repete ce citeşte în manual, ce se scrie la tablă etc.
Prin urmare, aducând ca şi mine aceeaşi acuzaţie
şcolii, aţi devenit pentru mine un partener extrem de
1060

Rãni deschise

important. De aceea m-ar interesa o detaliere, dacă de pildă
cel care v-a luat interviul v-ar fi întrebat: „Cum puteţi
explica afirmaţia dvs.? Pe ce bază aduceţi această acuzaţie
şcolii”?
M.S.: V-am spus, prin faptul că şcoala e orientată pe
disciplină şi pe imitaţie, asta e tot! Nu pe creativitate!...
S.M.: Da, dar ei nu recunosc lucrul ăsta…
M.S. (zâmbind): Păi cum o să recunoască când totul
este imperativ, cum bine spuneţi şi dvs., adică disciplină.
Disciplină şi imitaţie: „Aşa sunt lucrurile şi aşa se fac!”
S.M.: Uneori, chiar dacă nu i se spune explicit,
copilul simte că întrebarea e un act de indisciplină. În
discuţiile mele cu elevi de la diverse şcoli, când am
întrebat „Ce este un copil cuminte?”, unul din ei mi-a
răspuns: „Un copil cuminte este un copil care nu vorbeşte
neîntrebat!”. Vă daţi seama ce înseamnă lucrul ăsta?
Copilul simte la şcoală că deranjează dacă întreabă.
M.S.: Tot învăţământul este construit în acest fel: „Eu
sunt la catedră, Eu sunt mare, Eu ştiu totul şi tu nu ştii
nimic şi trebuie să-ţi iei cu pipeta ce-ţi dau eu.”
S.M. (râzând): Vreau să vă spun că, atunci când am
afirmat acest lucru într-o şcoală, mi se pare că la Liceul
„Grigore Moisil”, directoarea mi-a răspuns: „Cum puteţi
1061

Solomon Marcus

să spuneţi aşa ceva, noi ne facem datoria!” Cei mai mulţi
oameni ai şcolii nici nu recunosc acest lucru.
M.S.: Eu vin şi din domeniul artei, unde, dacă îl oblig
pe student să mă imite, chiar nu ajungem nicăieri! De
aceea ne străduim să dezvoltăm partea creativă în studenţii
noştri.
S.M.: Formaţia mea de matematician mă face să
gândesc aşa: să nu cerem deodată prea mult. Hai să fac un
prim pas! Acest prim pas mi s-a părut că trebuie să fie legat
de forma cea mai simplă a creativităţii, care este întrebarea.
Prin urmare, provocarea pe care o lansez este : „Hai să ne
luptăm pentru dreptul copilului de a pune întrebări, şi nu
numai atât, ci să fie stimulat să pună întrebări”!
Deocamdată mă mulţumesc să reuşim acest lucru,
pentru că poate este prea mult să reuşească educaţia,
dintr-odată, să treacă de la o extremă la extrema opusă.
M-am luptat, peste tot pe unde am fost, pentru a
introduce această schimbare: ca atitudinea predominant
imperativă să fie înlocuită de atitudinea predominant
interogativă.
M.S.: Noi asta facem în teatru. Dacă studentul-actor
cu care lucrezi nu îşi găseşte singur răspunsurile, el nu va
putea să reprezinte personajele cu toată fiinţa lui. Rămâne
doar o chestiune de cap, or, actorul trebuie să-şi găsească
1062

Rãni deschise

răspunsurile nu numai cu mintea, ci şi prin intuiţie, prin
corporalizare ş.a.m.d., iar când nu îşi găseşte singur
răspunsurile, nu se poate vorbi despre un act artistic de
calitate.
S.M.: Şcoala se află într-o situaţie similară, adică
ceea ce nu descoperi cu propriile tale puteri nu se
asimilează în mod real.
M.S.: Da, în acelaşi fel, procesul creativ al actorului
trebuie stimulat altfel decât spunându-i ce să facă!Asta
înseamnă ca profesorul să fie deschis şi să-l întrebe mereu
pe copil: „Dar tu cum crezi că e asta?”
S.M. (râzând): Dar ştiţi ce mi s-a răspuns în legătură
cu această propunere? „Păi, dacă noi am face aşa ceva, ar
fi anarhie, domnule!” Când am discutat cu profesori din
diverse şcoli asupra acestei probleme, de a-i permite
copilului să pună întrebări sau să propună o altă cale de
rezolvare a unor probleme, motivaţia lor a fost
următoarea: „Păi, domnule, dacă-l las pe el cu părerile lui,
nu îmi mai duc planul la bun sfârşit, că am plan de
predare, de-aici-până-aici, până la o anumită dată
calendaristică.” Înţelegeţi deci că un argument birocratic
are importanţă mai mare pentru ei!...
M.S.: Cred că este nevoie şi de altceva: trebuie să ai
şi vocaţie pentru meseria asta!
1063

Solomon Marcus

S.M.: Da, de-asta eu spun aici în cartea Educaţia în
spectacol că mă înclin cu respect şi cu duioşie în faţa
acelor învăţători şi profesori care, în ciuda caracterului
repetitiv al exerciţiului didactic, nu se lasă copleşiţi de
rutină şi de blazare. Profesorul este un actor, iar învăţarea
este un spectacol. Totul se poate discuta în termeni de
spectacol. Nu întâmplător, argumentul meu principal că
totul e spectacol ştiţi care este? E unul de o evidenţă
extraordinară, şi anume că în toate domeniile acum
proliferează metafore din domeniul spectacolului. De
pildă, cuvântul „scenariu” e folosit peste tot.
Actor, personaje, punere în scenă, conflict, intrigă –
la fel, până şi în lingvistică, analiza gramaticală a unei
fraze e văzută de mari lingvişti în termeni de spectacol.
Scenariul de bază al educaţiei azi este următorul: „E
cineva la catedră care predă, scrie la tablă, vorbeşte, el este
cel activ, iar cei din bancă trebuie să fie cuminţi, adică să
scrie în caietele lor, să nu deranjeze!
M.S.: În cadrul cursurilor de improvizaţie, am avut
un invitat, profesor din străinătate, şi unul din primele
lucruri pe care le făcea, atunci când lucra cu copiii, era să
se aşeze pe jos, la acelaşi nivel cu ei. Asta îţi scade statutul
vizibil, fizic şi propune un alt fel de interacţiune.
S.M.: În şcoală, ca şi în teatru, este nevoie de un
director de scenă. Rolul lui este să realizeze dialogul, să-i
1064

Rãni deschise

stimuleze pe copii să-şi orienteze atenţia şi imaginaţia în
direcţia cea mai bună. Profesorul trebuie să fie director de
scenă şi, dacă ne întoarcem la ideea lui Socrate, trebuie
să-i conduci pe cei pe care vrei să-i înveţi, în aşa fel încât
să ajungă ei singuri să descopere fiecare lucru.
M.S.: Sunt acum şi forme de teatru care propun o
democratizare, există chiar o formă de teatru nouă în care
nu mai există un regizor, ci fiecare are dreptul să propună.
Şi în teatru avem de-a face cu un joc de forţe destul de
inegal…
S.M.: Tocmai asta este situaţia şi în educaţia şcolară.
De multe ori, trebuie să fii propriul tău regizor. De obicei
eşti actor, dar se întâmplă deseori că indicaţiile de regie tot
tu trebuie să ţi le descoperi. Este o suprapunere de roluri.
Vedeţi, peste tot se vorbeşte de roluri. Cum am spus,
proliferează peste tot metafora spectacolului. La începutul
lunii decembrie, am fost invitat la Bruxelles, la Catedra de
Teoria spectacolului de la Université Libre de Bruxelles şi
am ţinut o conferinţă pe tema asta.
Este un subiect care de altfel se discută de câteva
decenii. Îmi amintesc de popularitatea unui volum colectiv,
La sémiologie de la représentation (Semiologia reprezentării, n.r.), coordonat de André Helbo, preşedintele
Asociaţiei Internaţionale a Spectacolului. Contribuţia mea
la acest volum se intitula La théâtralité au-délà du théâtre
1065

Solomon Marcus

(Teatralitatea din afara teatrului, n.r.). Textul contribuţiei
mele publicate acolo a fost ca o prefaţă a conferinţei de la
Bruxelles din decembrie, al cărei titlu a fost: Le spectacle
comme paradigme universel (Spectacolul ca paradigmă
universală, n.r.).
În legătură cu acest lucru, aş aminti şi o carte
intitulată La mise en scène de la vie quotidienne a lui
Erving Goffman, care a fost tradusă şi în româneşte.

1066

