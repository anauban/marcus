Rãni deschise

Doctoratul, din nou sub lupă
Ziua, 8 august 2005

Există, în Hotărârea guvernamentală privind
organizarea doctoratului (Hotărâre la care ne vom referi în
continuare prin HGOD), unele lacune şi imprecizii care
privesc modul de înţelegere a componentei de cercetare.
Această componentă constituie esenţa doctoratului şi
culminează prin susţinerea tezei. Poate să pară curios, din
acest punct de vedere, faptul că HGOD prevede, pentru
programul de cercetare, o perioadă mai scurtă decât pentru
studiile universitare avansate. Realitatea de pe teren este
însă alta şi ea ar trebui să fie stipulată explicit. Frumos se
spune în HGOD că „doctoratul ştiinţific se bazează pe
creaţie şi cercetare avansată” (aici, cuvântul „avansată” ar
putea lipsi); dar, dacă este aşa, ar trebui specificat ca la
admiterea la doctorat să fie verificate nu numai
cunoştinţele candidaţilor, ci şi capacitatea lor de a se
angaja în cercetare. Orice profesor ştie că numai o parte
mică a celor ce încheie licenţa sau masteratul cu note mari
este capabilă de cercetare, după cum unii licenţiaţi sau cu
99

Solomon Marcus

master care nu au notele cele mai mari obţin, ulterior,
rezultate bune în cercetare.
Celui cu note mari, dar care nu a dat, până la
obţinerea masteratului, niciun semn că ar avea aptitudini
pentru cercetare, l-aş prefera pe cel cu note eventual mai
mici decât ale precedentului, la capitolul cunoştinţe, dar
care a demonstrat mai multă iniţiativă şi mai multă
imaginaţie. Practica doctoratului arată că bazele viitoarei
teze se cristalizează încă în perioada pe care HGOD o
numeşte „program de pregătire bazat pe studii universitare
avansate”; simptomele acestei situaţii se manifestă, de
regulă, prin publicarea, de către doctorand, a unor articole
în reviste de cercetare. În mod normal, teza nu vine pe un
teren gol, ci este pregătită de cercetare. Mi se pare firesc
ca, înainte ca doctorandul să elaboreze o lucrare de o sută
de pagini, să ne convingă de capacitatea sa de a elabora un
articol original de zece pagini. Nu aş numi prima
componentă ca fiind numai de studii, ci de studii şi
cercetare, iar a doua componentă ar deveni aceea a
elaborării tezei.
La originalitate nu se poate face rabat, aşa cum se
întâmplă la Art. 28, unde tezei de doctorat i se cere numai
să conţină „elemente de originalitate”; teza trebuie să fie
în întregime originală, nu numai în douăzeci de pagini
dintre cele o sută ale ei. Condiţia ca teza să includă şi
„modalităţi de validare ştiinţifică a rezultatelor” nu poate
100

Rãni deschise

fi îndeplinită fără publicarea prealabilă, în reviste
acreditate, a unora dintre rezultatele din teză. Nici nu ar
trebui admisă la doctorat o persoană care nu a publicat
nimic până atunci. Nu e obligatoriu ca doctoratul să
urmeze imediat masteratului. Prevederea care recomandă
stimularea publicării rapoartelor ştiinţifice ale
doctoranzilor (Art. 13, punctul 6) mi se pare insuficientă;
această publicare ar trebui să fie obligatorie.
Într-o altă ordine de idei, nu se ţine seama de faptul
că, din diverse motive, au existat şi există persoane care
nu se angajează într-un program de doctorat imediat sau
la scurt timp după obţinerea licenţei sau a masteratului, dar
care sunt foarte active în cercetare şi acumulează, pe
parcursul mai multor ani, o zestre ştiinţifică semnificativă,
constând în rezultate originale recunoscute de comunitatea
ştiinţifică internaţională. Potenţial, aceste rezultate
alcătuiesc cu prisosinţă o teză de doctorat şi este firesc să
nu le mai pretindem, celor din această categorie, să
parcurgă itinerarul prevăzut de HGOD.
Iată însă că nu există, în textul HGOD, niciun articol
referitor la situaţia semnalată. Incomplete mi se par şi
referirile la „comisii de experţi”, la faptul că „fiecare
instituţie de organizare a doctoratului este autorizată să
stabilească propriile reguli de evaluare a doctoranzilor”,
atâta vreme cât nu se specifică faptul că în toate aceste
cazuri trebuie să se respecte criteriile de evaluare acreditate
101

Solomon Marcus

în cadrul comunităţii ştiinţifice internaţionale. Ignorarea
acestei condiţii a produs multe pagube culturii româneşti,
dar nu putem dezvolta aici acest punct. Specificarea din
Art. 2, privind promovarea interdisciplinarităţii, rămâne
vagă atâta vreme cât singurul lucru pe care îl aflăm în
această privinţă este faptul că îndrumarea poate fi realizată
de doi conducători de doctorat, provenind din discipline
diferite. Nu aflăm ce fel de diplomă de doctorat va primi
cineva care, de exemplu, face o teză de biologie
computaţională sau de lingvistică matematică;
nomenclatorul domeniilor de doctorat (Art. 2) nu lasă loc
domeniilor interdisciplinare. Ţinând seama de dinamica
ştiinţei actuale, lista specialităţilor de doctorat ar trebui să
rămână deschisă.
Alte observaţii: Punctul (2) de la Art. 11 („Cercetarea
ştiinţifică se bazează pe metode cantitative şi calitative
agreate de comunitatea ştiinţifică dintr-un domeniu”)
trebuie eliminat, fiind implicit inclus în alte prevederi mai
cuprinzătoare. Specificarea din Art. 10, privind includerea
în planul de învăţământ al doctoratului a unor „module de
pregătire complementară, necesare pentru o inserţie rapidă
a absolventului de studii superioare de doctorat pe piaţa
muncii” trebuie relativizată prin „acolo unde este cazul”.
Valoarea cercetării ştiinţifice constă, în bună măsură, în
libertatea ei de a urma căile naturale ale cunoaşterii; a i se
cere să aibă în faţă o scadenţă apropiată, printr-o legătură
102

Rãni deschise

directă cu „piaţa muncii”, ar putea fi, în multe cazuri, o
greşeală cu consecinţe grave, care s-a dovedit extrem de
nocivă în anii comunismului şi nu trebuie s-o repetăm.
Cerinţa ca teza de doctorat să fie pe o temă centrală din
activitatea conducătorului de doctorat trebuie şi ea
atenuată, deoarece tinerii aleg de multe ori subiecte foarte
proaspete, în care nimeni din ţară nu e specialist. Ar trebui
să recurgem prea des la conducători de doctorat din afara
ţării, posibilitate la care HGOD se referă numai sub formă
de co-tutelă. Rămâne deci să mai reflectăm în vederea
cristalizării unui statut corespunzător al doctoratului.

103

