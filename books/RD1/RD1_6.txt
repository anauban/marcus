Solomon Marcus

Aspecte ale învăţământului matematic
în România
Şcoala românească, încotro?
Maria Ciobanu-Băcanu, Petruş Alexandrescu (coord.),
Ed. Paralela 45, 2004, pp. 297-309

Multe dintre problemele şi faptele pe care le vom
prezenta şi dintre semnificaţiile pe care le vom desprinde
pot prezenta interes nu numai pentru matematicieni, ci şi
pentru ceilalţi oameni de cultură; nu numai pentru cei din
România, ci şi pentru comunitatea matematică şi
educaţională internaţională. Textul de faţă poate fi
interpretat şi ca propunere a unui mod mai cuprinzător
decât acela în care se înţelege de obicei starea unei ştiinţe
într-o anumită ţară, deoarece avem în vedere întregul
context ştiinţific, educaţional, social, istoric, cultural şi
politic în care s-a dezvoltat matematica românească în
secolul al XX-lea. Pornind de la ideea universalităţii
matematicii, ca disciplină a cărei rază de acţiune cuprinde
cvasitotalitatea cunoaşterii umane, prin educaţie
matematică nu înţelegem numai predarea şi învăţarea ei în
38

Rãni deschise

şcoli şi în universităţi, ci toate formele prin care cultura
matematică şi gândirea pe care ea o promovează pot fi
asimilate la diferite vârste, de diferite straturi sociale, de
diferite profesii; înţelegem, de asemenea, educarea modului
în care matematica poate deveni un instrument util în viaţa
de fiecare zi, dar şi în discipline, altele decât matematica;
în sfârşit, sau poate în primul rând, avem în vedere educarea
capacităţii matematicii de a declanşa mari bucurii spirituale
şi emoţii estetice. În raport cu aceste premise, repunem în
discuţie toate canoanele care limitează, deocamdată,
prezenţa socială a matematicii şi care au dus la criza actuală
a educaţiei matematice în România.
Într-o carte cu titlu înşelător, Linii drepte, drumuri
strâmbe, Nicolae Ciorănescu observa: ,,Dacă e scuzabil să
crezi că ai dreptul la succes, e condamnabil să faci din el
scopul vieţii”. Continuând această idee, vom observa că
ceea ce îi uneşte pe matematicieni este bucuria de a face
matematică. Succesul în rezolvarea unor probleme
importante e rezervat numai câtorva, dar toţi avem datoria
de a educa la tinerele generaţii capacitatea de a se bucura
de frumuseţile matematicii, această inefabilă disciplină,
culme a spiritualităţii. Dar cât de departe suntem încă de
atingerea acestui deziderat!
Miron Nicolescu a exprimat într-un mod fericit
solidaritatea dintre diferite generaţii de matematicieni: ,,De
la toţi am învăţat. Mă surprind uneori vorbind olimpian ca
39

Solomon Marcus

Pompeiu, apăsat ca Ţiţeica, senin şi simplu ca David
Emmanuel. Căci noi nu suntem numai fiii părinţilor noştri,
ci şi fiii profesorilor noştri”. Când apreciem performanţele
lor, trebuie să avem în vedere că fiecare stă, metaforic
vorbind, pe umerii tuturor generaţiilor anterioare de
matematicieni.
Matematica şcolară nu se justifică încă
prin calităţile ei intrinseci
Situaţia este gravă. Pe de o parte, diferite organisme,
cum ar fi UNESCO, diferite institute de cercetări în
domeniul educaţiei, departamente universitare, profesori
cu o îndelungată experienţă, filosofi şi sociologi
organizează sesiuni de comunicări şi dezbateri privind
necesitatea unor reforme care să ţină seama de noile
tendinţe ale societăţii informaţionale şi ale globalizării pe
toate planurile şi de nevoia trecerii la o organizare
modulară a predării; pe de altă parte, practica şcolară şi
universitară rămâne în mare măsură tributară vechii
mentalităţi, formată în perioada de naştere şi proliferare a
unor discipline şcolare cât mai multe şi mai înguste.
Unii vorbesc despre inter- şi transdisciplinaritate,
despre nevoia unei priviri holistice, integratoare în
educaţie şi învăţământ; alţii continuă să privilegieze un
mod pur operaţional în educaţie, cu accent pe procedee şi
40

Rãni deschise

cu neglijarea ideilor şi sensului. Educaţia matematică
suportă urmările acestei situaţii. Se manifestă un
conservatorism păgubos în alcătuirea programelor de
învăţământ şi a manualelor şcolare.
Matematica şcolară se justifică, în ochii majorităţii
elevilor, nu prin calităţile ei intrinseci, ci prin factori
exteriori: este nevoie de ea pentru a putea promova clasa,
pentru a te pregăti de capacitate, pentru bacalaureat sau
pentru admiterea la facultate. Scoateţi proba de matematică
de la toate aceste examene şi în mod automat sutele de
culegeri de probleme care inundă librăriile nu-şi vor mai
găsi niciun cumpărător. Matematica şcolară nu este încă în
stare să se justifice în ochii elevilor prin propriile ei calităţi,
aşa cum se justifică informatica, prin etalarea componentei
ludice, care-i fascinează pe copii şi pe adolescenţi.
Ceea ce este şi mai grav ţine de pericolul de a
exacerba latura formală a matematicii, sintaxa ei, igiena ei
de rigoare logică, în dauna semanticii, înţelesului, sensului.
Nu este suficient ca o teoremă sau o formulă să fie formal
coerentă, mai este nevoie şi s-o înţelegem în ceea ce ea
semnifică. Asistăm, în o bună parte a matematicii şcolare,
la o proliferare a unor dezvoltări formal corecte, dar care
apar lipsite de semnificaţie. Limbajul natural cedează în
favoarea simbolurilor şi formulelor. Asupra acestui decalaj
a atras atenţia E. Bishop, care a numit fenomenul respectiv
,,schizofrenia matematicii contemporane”. René Thom
41

Solomon Marcus

s-a referit şi el la acest fenomen, plasându-l într-o alegere
dramatică: rigoarea şi sensul se sabotează reciproc. Dar
cine mai are timp să caute sensul în situaţia în care trebuie
parcurse sute de definiţii, de notaţii, de formule, de
proprietăţi şi de teoreme întinse pe sute de pagini, în
condiţiile în care alte zece manuale, la fel de obeze, la alte
discipline, aşteaptă şi ele să fie învăţate? Numai atât şi este
suficient să realizăm patologia actualului sistem
educaţional.
Programele şi manualele şcolare
Să examinăm acum unele mostre de programe şi
manuale de matematică.
Am în faţă volumul 4 din seria de programe şcolare,
care se referă la aria curriculară ,,Matematica şi ştiinţele
naturii”, publicată de Consiliul naţional pentru curriculum
în 2001, partea de matematică referindu-se aici la clasele
X-XII. Chiar şi în ,,Cuvânt înainte” se recunoaşte că
,,respectarea particularităţilor de vârstă ale elevilor nu este
o problemă învechită, despre ea se vorbeşte de mulţi ani,
dar s-a realizat puţin” şi că ,,în ultimii ani, s-a realizat o
legătură mai bună între clase, pe verticală, iar pentru
cealaltă legătură, pe orizontală, între disciplinele de
învăţământ, mai este de lucru”. Se mai recunoaşte că ,,în
ansamblu, elevii au de asimilat o cantitate mare de
42

Rãni deschise

cunoştinţe, unele fără să fie necesare, iar altele cu mult
peste posibilitatea de cuprindere a elevilor”. Numai că
aceste racile există de decenii, lupta împotriva lor a fost
proclamată şi în documentele de partid de pe vremuri,
pentru ca acum să fie exprimate din nou, în jargonul
adecvat noului context. Cum stăm, de exemplu, cu
,,respectarea particularităţilor de vârstă”? Iată ce citim în
primele pagini ale unui manual de matematică de clasa a
VII-a din acest început de mileniu trei: ,,Dacă doi termeni
reprezintă acelaşi obiect, se spune că ei sunt egali”. Dar ce
sunt termenii? Nici această întrebare nu este lăsată fără
răspuns: ,,Obiectele matematice sunt reprezentate prin
cuvinte, semne, grupări de semne, pe care le numim
termeni”. În acest fel, autorii se angajează într-o aventură
semiotică foarte delicată, care în niciun caz nu-şi avea
locul într-un manual de clasa a VII-a, iar în forma propusă
nu duce la nimic, nici mai târziu. ,,Definiţii” ca cele de mai
sus sunt ,,răspunsuri” la întrebări pe care un elev obişnuit
de clasa a VII-a nici măcar nu şi le pune. Elanul
epistemologic îi conduce pe autorii manualului respectiv
să ,,definească” şi statistica matematică, dar într-un mod
care se potriveşte tot atât de bine (sau de rău) şi biologiei,
economiei, sociologiei sau chimiei. Aflăm că ,,statistica
matematică se ocupă de gruparea, analiza şi interpretarea
datelor referitoare la un anumit fenomen, precum şi la
unele previziuni privind producerea lui viitoare, pornind
43

Solomon Marcus

de la cauzele care stau la baza acestuia”. Toate acestea se
întâmplă, repet, într-un manual de clasa a VII-a, deci după
care trebuie să înveţe un adolescent de 12-13 ani.
Exprimarea însăşi este de multe ori chinuită; într-un
manual de matematică de clasa a VIII-a citim: ,,Orice
fracţie zecimală infinită, neperiodică, nu este un număr
raţional”. De ce simplu, când se poate şi complicat, intrând
în conflict şi cu limba română? De fapt, intenţia era să se
spună că, dacă reprezentarea zecimală infinită a unui
număr este neperiodică, atunci el este iraţional.
Îi asigurăm pe cititori că situaţii de acest fel nu sunt
izolate; problema accesibilităţii manualelor rămâne
deschisă.
Interdisciplinaritatea, între slogan şi realitate
În ceea ce priveşte afirmaţia că ,,pentru cealaltă
legătură, pe orizontală, între disciplinele de învăţământ mai
este de lucru”, nu putem spune decât că exprimă într-un
mod foarte eufemistic o realitate crudă: sloganul
interdisciplinarităţii este arborat de vreo cincizeci de ani;
el face, încă de atunci, obiectul a tot felul de dezbateri, i
s-a consacrat o întreagă literatură, dar programele şi
manualele şcolare, ca şi realitatea de fiecare zi a şcolii, pur
şi simplu, îl ignoră. Predarea matematicii e cu deosebire
afectată de această situaţie, deoarece matematica are, prin
44

Rãni deschise

natura ei, o anumită universalitate, raza ei de acţiune
include practic toată cunoaşterea umană. Gândirea
matematică poate fi valorificată în aproape orice disciplină
predată în şcoală, iar iniţiativa, în această privinţă, ar trebui
să pornească de la matematică. Şcoala oferă această
extraordinară ocazie de a-l pune pe elev concomitent în
contact cu o sumedenie de discipline ştiinţifice şi artistice.
Este o şansă deosebită pentru educaţia matematică de a
valorifica potenţialul ei interdisciplinar. În ,,Nota de
prezentare” a programelor găsim afirmaţii care corespund
la ceea ce cerem şi noi aici: ,,Studiul matematicii în liceu
urmăreşte dezvoltarea capacităţilor elevilor de a reflecta
asupra lumii, de a formula şi rezolva probleme pe baza
relaţionării cunoştinţelor din diferite domenii [...] faţă de
un demers strict disciplinar şi teoretic, noul curriculum
propune organizarea activităţii didactice pe baza
relaţionării şi corelării domeniilor de studiu [...]” .
Sunt, în continuare, vehiculaţi termeni din arsenalul
pedagogic: competenţe, conţinuturi, valori, atitudini,
curiozitate, imaginaţie, tenacitate, perseverenţă,
obiectivitate, imparţialitate, simţ estetic şi critic, se
pretinde chiar că se urmăreşte ,,formarea obişnuinţei de a
recurge la concepte şi metode matematice în abordarea
unor situaţii cotidiene sau pentru rezolvarea unor probleme
practice”. Când le căutăm însă pe toate acestea, nu prea le
găsim nici în programele preconizate, nici în manuale. În
45

Solomon Marcus

primul rând, selecţia materiei de predare este de multe ori
nefericită. Geometria elementară este o bună gimnastică a
minţii. Câteva teoreme fundamentale şi transformările
geometrice nu pot lipsi din programă, dar o bună parte a
ei trebuie să fie sacrificată, pentru a face loc atâtor altor
fapte la fel de frumoase, dar cu un orizont cognitiv şi
practic mai larg, cum ar fi numerele prime (cu multe
aspecte elementare), codurile, combinatorica, probabilităţile discrete, la care s-ar putea adăuga elemente de
teoria jocurilor, de grafuri şi de teoria informaţiei. Nu ar
trebui să lipsească din liceu noţiuni ca algoritm, automat,
complexitate, gramatică, maşina Turing, calculabilitate,
fractal, decidabilitate, sistem dinamic discret, atractori,
haos determinist. Multe dintre aceste noţiuni cuprind, în
raza lor de acţiune, fenomene şi procese studiate la alte
discipline şcolare; caracterul lor transdisciplinar le conferă
un potenţial cultural deosebit, pentru a nu mai vorbi despre
calitatea lor estetică.
Cum poate deveni matematica şcolară un act de cultură?
Toate aceste propuneri se subsumează unui principiu
general: şcoala trebuie să se orienteze spre ceea ce are o
scadenţă apropiată, deci spre lucrurile care îşi dezvăluie
semnificaţia fără amânare; în particular, şcoala nu trebuie
privită ca o anticameră a universităţii, aşa cum sugerează,
46

Rãni deschise

din păcate, denumirea de ,,învăţământ preuniversitar”,
folosită azi în România. Cu alte cuvinte, ca să poată fi
percepute ca acte de cultură, faptele de cunoaştere pe care
le pune în evidenţă şcoala trebuie să fie inteligibile şi
atrăgătoare chiar în absenţa posibilelor lor continuări în
învăţământul superior. Acesta este criteriul care ne-a ghidat
în propunerile de mai sus. Prin ,,scadenţă” avem în vedere
nu neapărat o aplicaţie practică; scadenţa poate fi de natură
cognitivă sau estetică. Chiar numai câteva elemente de
trigonometrie şi de numere complexe permit să se pună în
evidenţă relaţia lui Euler, care leagă patru entităţi
fundamentale ale universului: unitatea, numărul pi (simbol
al cercului), numărul e, simbol al logaritmilor naturali, şi
numărul i, simbol al imaginarului.
Această relaţie, considerată una dintre cele mai
frumoase ale matematicii, merită să fie contemplată şi
pentru performanţa ei estetică: maximum de gând în
minimum de cuprindere. În ceea ce priveşte progresiile şi
logaritmii, raza lor de acţiune este atât de mare, încât ar fi
păcat să o ratăm. Ce ne spune în această privinţă programa?
,,Identificarea unor situaţii care pot fi exprimate matematic
utilizând progresii” şi ,,exprimarea în limbaj matematic a
unor situaţii concrete ce se pot descrie printr-o funcţie de o
variabilă”; acesta este stilul tipic în care lichidează
programa chestiunile de natură interdisciplinară. Era
inevitabil ca un mesaj atât de vag să nu aibă consecinţe în
47

Solomon Marcus

manuale, unde semnificaţiile majore ale progresiilor şi ale
funcţiilor exponenţiale şi logaritmice în fizică, chimie,
biologie, geologie, lingvistică, psihologie, teoria
informaţiei, informatică etc. sunt eludate. La obiecţia că
aceste semnificaţii s-ar referi la o materie neparcursă încă
la disciplinele respective, se poate răspunde că educaţia
matematică poate lua pe cont propriu sau anticipa anumite
fapte, după cum alte discipline pot anticipa unele idei sau
relaţii matematice. De ce n-ar putea manualul de clasa
a X-a să prezinte legea Weber-Fechner, după care senzaţia
este logaritmul excitaţiei (sau acelaşi lucru, în limbaj de
progresii aritmetice şi geometrice), sau expresia lui
Shannon a cantităţii de informaţie, care include un logaritm
în baza 2 şi este rezultatul unui splendid exerciţiu de
combinatorică? Sau de ce n-ar putea discuta unele decalaje
ale proceselor actuale de globalizare în termeni de creştere
logaritmică, polinomială sau exponenţială?
Trecerea la manuale alternative
Nu mai puţin interesante sunt problemele apărute în
legătură cu trecerea, în ultimii ani, de la manualul unic la
pluralitatea manualelor şi deci la posibilitatea unei opţiuni.
Pe de o parte, unii factori de putere şi-au exprimat
nedumerirea faţă de posibilitatea unor manuale alternative
la matematică; ,,teorema lui Pitagora e teorema lui Pitagora,
48

Rãni deschise

unde e loc pentru alternativă?” se întrebau ei, la ore de
audienţă maximă a unor emisiuni de televiziune. Pe de altă
parte, desfidând această apreciere, la unele clase s-au
înregistrat peste 20 de variante de manuale de autori diferiţi,
fapt aspru criticat, care a condus, la un moment dat, la
limitarea la 3 a numărului admis de variante ale aceluiaşi
manual. Prea multe sau prea puţine? Care este adevărul? O
examinare mai atentă ne arată că diversitatea care ni s-a
propus, cel puţin în cazul matematicii, este numai aparentă.
Manualele de matematică pentru aceeaşi clasă seamănă
prea mult între ele; de exemplu, critica pe care am
adresat-o mai sus, în legătură cu sărăcia şi cu
inaccesibilitatea dialogului cu alte discipline, este în esenţă
valabilă pentru toate variantele de manuale de clasa a X-a.
La fel, pentru celelalte clase. Există, desigur, diferenţe,
dar ele nu privesc concepţia de ansamblu. O altă deficienţă
comună a manualelor, care provine şi ea dintr-o deficienţă
corespunzătoare a programelor, este absenţa dimensiunii
istorice. Unii autori de manuale cred că înlătură această
lacună prin introducerea unor note privind biografia unor
autori de teoreme sau de teorii sau a unor legende antice
privind numerele. Însă adevărata dimensiune istorică se
referă la explicarea evoluţiei ideilor matematice şi a
relaţiilor matematicii cu celelalte ştiinţe şi cu artele.
În această ordine de idei, cum am putea oare eluda,
încă de la gimnaziu, bazele pitagoreice ale muzicii, pe baza
49

Solomon Marcus

proprietăţilor de divizibilitate ale numerelor? Rămâi
stupefiat să constaţi absenţa acestor lucruri din manualele
de matematică; nici măcar elevii liceelor de muzică nu află
despre ele, aşa cum am putut constata într-o discuţie
recentă.
Profesorul şi elevii, faţă în faţă
O analiză a matematicii şcolare ar rămâne incompletă
fără considerarea modului în care ea se prezintă în realitatea
orelor de clasă, acolo unde nu mai apar direct decât
profesorul şi elevii săi, faţă în faţă. Inspecţiile pe care
le-am făcut în vederea acordării gradului I unor profesori
cu vechime şi experienţă în învăţământ mi-au dat
posibilitatea, timp de vreo 15 ani, să cunosc realitatea ,,pe
teren” a matematicii şcolare. Am asistat la ore de clasă în
şcoli de toate felurile, la oraşe şi la sate, la clase mici şi la
clase mari, teoretice şi industriale. Mi-am rezervat, de
fiecare dată, câteva minute de dialog direct cu elevii. Am
umplut două caiete cu însemnări pe marginea acestor vizite.
Am asistat de obicei la o punere în scenă a cărei
finalitate principală era o cât mai convingătoare prestaţie a
celui care urma să obţină calificarea superioară în
învăţământ. Ca în mecanica cuantică, unde obiectul observat
este deformat chiar prin actul de observare, spectacolul la
care asistam era deformat chiar de prezenţa acolo a comisiei
50

Rãni deschise

care efectua inspecţia. Nu fac din această constatare un act
de acuzare, nici măcar nu critic această situaţie, absolut
inevitabilă în orice inspecţie. Din fericire, o serie de aspecte
importante nu pot fi trucate, ele îşi păstrează autenticitatea.
Într-o emisiune de televiziune din toamna anului
2003, s-a putut vedea o lecţie de matematică în care în faţa
unei table pline de formule se aflau un profesor nervos,
care bătea cu creta în tablă, şi un elev complet năucit, care
privea cu disperare la cele scrise pe tablă. În bănci, câteva
zeci de elevi, unii plictisiţi, alţii amuzaţi, sporovăind şi
chicotind tot timpul. Pe acest fond, ni se spunea că elevilor
nu le place matematica. Discuţiile cu elevi foarte buni şi
cu profesori, dar mai ales cu părinţi ai elevilor, mi-au
dezvăluit spaima de matematică a multor elevi şi mai ales
speranţa pe care şi-o pun într-un eventual meditator
priceput, care să-i scoată din încurcătură. Impresia directă,
la clasă, este înşelătoare, deoarece atmosfera generală este
aceea a disciplinei, la care elevii au datoria de a se supune.
În timpul inspecţiei, ei au o atitudine gravă şi destul de
ţeapănă, îşi dau seama că trebuie să vină în ajutorul
profesorului lor. O atmosferă cazonă, dominată de modalitatea
imperativă, este atotstăpânitoare. ,,Se consideră...”, ,,Fie...”,
,,Să se arate că...”, ,,Se dau...” sunt expresiile tipice folosite.
Limbajul simbolurilor şi al formulelor nu prea mai lasă loc
limbajului natural, de multe ori elevul lucrează în tăcere la
tablă, cei din bancă scriu în tăcere în caietele lor, iar profesorul
51

Solomon Marcus

se uită în tăcere la ceea ce scrie cel de la tablă, alteori
plimbându-se printre bănci, pentru a privi ceea ce scriu elevii
în caiete. Din când în când, tăcerea e ruptă de o intervenţie a
profesorului, pentru a da o indicaţie sau a corecta o greşeală.
Impresia generală este aceea a respectului pe care elevii trebuie
să-l arate faţă de profesor, nepunând întrebări care ar putea
abate lecţia de la planul fixat iniţial.
Cuvântul ,,plan” este aici cheia situaţiei. Profesorul e
preocupat, în primul rând, să respecte planul prestabilit,
acela de a duce la bun sfârşit ceea ce şi-a propus pentru
ora respectivă. Orice eveniment neprevăzut, deci care ar
putea să-l abată de la plan, îl supără şi chiar îl enervează.
Întrebarea devine de multe ori o formă de indisciplină şi
este descurajată prin răspunsuri ca ,,stai jos” sau ,,nu e
bine”. La fel sunt întâmpinate acele răspunsuri la
întrebările profesorului care diferă de cele avute în vedere
de profesor, chiar dacă sunt interesante.
O lectură atentă a caietelor elevilor şi mai ales a
maculatoarelor în care se oglindeşte predarea lecţiei la
clasă dă posibilitatea să se vadă cum se lucrează în general,
deci în orele ,,normale”, fără inspecţii. Multe figuri şi
calcule, foarte puţine cuvinte; motivaţii şi comentarii nu
apar aproape deloc; aceste aspecte par să caracterizeze
stilul de predare şi de învăţare. Greşelile nu sunt tăiate în
aşa fel încât să poată fi vizualizate ulterior, pentru a se
învăţa din ele, ci şterse cu radiera.
52

Rãni deschise

Discuţiile directe cu elevii nu fac decât să confirme
cele de mai sus. Întrebaţi ce semnifică un anumit cuvânt
folosit de profesor, ridică din umeri. ,,Dacă nu cunoşteaţi
acest cuvânt, de ce nu aţi întrebat ce înseamnă el?” Tăcere.
Calculele cu numărul pi merg strună, dar, întrebaţi care
este definiţia numărului π, răspunsul este: 3,14; nu s-a
înţeles că acest număr este rezultatul unei teoreme:
Raportul dintre lungimea unui cerc şi diametrul său este
acelaşi la orice cerc şi valoarea sa este notată cu pi.
În general, teorema, cărămida matematicii, nu
beneficiază de atenţia cuvenită, fiind preferate reţetele (sub
formă de algoritmi) şi formulele.
Atunci când sunt întrebaţi pe cine reprezintă
portretele pe care le au pe pereţi, în laboratorul de
matematică, se arată surprinşi şi încearcă abia atunci să se
lămurească. ,,Ce nume de matematicieni români
importanţi cunoaşteţi?” Răspund cu numele autorilor unor
derizorii culegeri de probleme. În ce secol se află anul
1453? Când a trăit Euclid? Întrebări riscante, la care poţi
primi răspunsuri halucinante.
Lucrările de grad ale profesorilor
Cum arată lucrările de grad I ale profesorilor? Scopul
lor era să prezinte experienţa lor la catedră, soldată cu
măcar câteva idei şi propuneri de ameliorare a predării
53

Solomon Marcus

matematicii. Unde se află punctele nevralgice ale materiei
predate? Unde se împotmolesc de obicei elevii? Care sunt
greşelile tipice pe care ei le comit? Cum ar putea deveni
mai atractivă predarea cutărei noţiuni sau cutărui fapt? Ce
legături s-ar putea face cu predarea altor discipline? Ce
deficienţe manifestă în această privinţă programele şi
manualele? ş.a.m.d. De la o lucrare de grad a unui profesor
care are în spate o experienţă de mulţi ani este normal să
te aştepţi la câteva răspunsuri la întrebări de felul de mai
sus. În loc să se întâmple aşa, numărăm pe degete lucrările
de grad I din care se poate extrage un articol publicabil
într-o revistă de didactică matematică. Este clar că sistemul
actual de acordare a gradelor în învăţământul de cultură
generală este ineficient şi trebuie reconsiderat. Nu sunt mai
puţin descurajante sesiunile de comunicări ale profesorilor,
din care rareori se poate alege un spor efectiv în procesul
educaţional. Este tolerată, uneori stimulată, compilaţia
după surse nespecificate şi de aici până la plagiat nu-i decât
un pas. Lucrările de grad I sunt şi ele puternic afectate de
acest păcat. Răul acesta vine din primii ani de şcoală, când
elevilor li se dă impresia că a face o ,,comunicare
ştiinţifică” este ceva la îndemâna oricui. Sunt sechele ale
deprinderilor cultivate în perioada comunistă, când
cercetarea era considerată o activitate de masă, care face
obiectul întrecerilor dintre şcoli şi judeţe. Rezolvarea unor
probleme, alcătuirea unor referate şi elaborarea unei
54

Rãni deschise

comunicări sunt, uneori, şi acum puse laolaltă şi chiar
confundate.
Omagiindu-i pe profesorii care se abat, în sensul bun
al cuvântului, de la acest scenariu, trebuie totuşi să spunem
că aceasta a fost realitatea tipică a orelor de matematică în
şcolile din România anilor ’70, ’80 şi ’90 ai secolului al
XX-lea. Este ea mai bună acum? Unii mă avertizează că,
între timp, situaţia s-a deteriorat şi mai mult.
Olimpiadele de matematică
Olimpiadele au reprezentat şi reprezintă singura
componentă mediatizată a matematicii româneşti, poate prin
analogie cu olimpiadele sportive, care consacră pe
câştigători, neobservându-se că premiaţii olimpiadelor de
matematică (sau ai altor discipline ştiinţifice) nu sunt încă
vedete, ci numai promisiuni. Dacă ar fi să indicăm un singur
nume românesc al unuia care s-a dedicat în cea mai mare
măsură îmbunătăţirii olimpiadelor de matematică, nu am
ezita să-l propunem pe Ion Cuculescu, creator al unui stil
propriu de alegere şi abordare a problemelor de olimpiadă,
pe care l-a prezentat în cartea sa Olimpiadele internaţionale
de matematică ale elevilor (Editura Tehnică, 1984), elaborată
pe baza bogatei sale experienţe ca membru al mai multor
jurii la astfel de întreceri. Succesul olimpiadelor nu ne
împiedică să vedem carenţele lor şi să sugerăm nevoia
55

Solomon Marcus

îmbunătăţirii lor. Latura sportivă, provocatoare şi atractivă a
olimpiadelor plăteşte un tribut deloc neglijabil: mintea
omenească este pusă să lucreze contra cronometru, deci
împotriva unei vechi experienţe, care arată că ideile nu vin
la oră fixă. ,,Matematica nu se face în stare de urgenţă”, ne
previne şi Cuculescu. Mai este şi faptul că multe dintre
problemele de olimpiadă au un orizont ştiinţific şi cultural
modest, dacă nu derizoriu. Educaţia matematică a tineretului
are azi o miză mult mai importantă, pariul ei social are în
vedere o nevoie de matematică resimţită de mai toate
domeniile. Ne întrebăm, de aceea, dacă nu cumva mai
profitabile au fost cercurile ştiinţifice şcolare şi, mai ales,
cele studenţeşti, în rarele, dar nu puţinele cazuri în care, în
cadrul lor, unii tineri de valoare şi-au făcut o autentică
ucenicie în cercetare. Mai păstrăm şi azi Buletine ale acestor
cercuri, adevărate pepiniere de cercetători; se pot da exemple
de matematicieni ajunşi la mari performanţe, care au trecut
mai întâi prin aceste cercuri. Lipsite însă de latura sportivă a
olimpiadelor şi, implicit, de mediatizare, ele s-au stins şi
nimeni nu-şi mai aminteşte de ele.
Este nevoie de un aer proaspăt!
Criza generală a învăţământului matematic, tocmai
într-o perioadă în care, în condiţiile globalizării economice
şi culturale şi ale integrării europene, răspunderile pe care
56

Rãni deschise

le are educaţia matematică sunt din ce în ce mai mari, ar
trebui să constituie o provocare importantă a comunităţii
noastre matematice şi, în mod special, a SSMR. Măcar dacă
revistele SSMR ar reproduce fragmente din marile dezbateri
pe aceste teme care au loc în lume. Menţinem, în virtutea
inerţiei, programe prăfuite, ne este frică să deschidem larg
ferestrele, pentru a lăsa să pătrundă aerul proaspăt al atâtor
idei noi, bogate în semnificaţii şi care nu implică dificultăţi
tehnice atât de mari ca cele privind diferenţierea şi integrarea
funcţiilor, dar care ar putea produce mari satisfacţii
spirituale.
Profesia de matematician
Profesia de matematician este destul de tânără, mai
cu seamă în ţara noastră, iar înţelegerea naturii ei, ca ceva
distinct de profesia didactică (chiar dacă strâns legată de
aceasta), lasă de dorit. Matematicianul a fost mereu
confundat cu altcineva: cu contabilul, cu inginerul, cu
logicianul, cu statisticianul, cu informaticianul. O corectă
înţelegere a profesiei de matematician este aceea care-l
apropie de artist. Matematicianul nu este un prestator de
servicii, cum şi-l imaginează unii profani. Matematicieni
români, ca Gh. Ţiţeica sau P. Sergescu, au explicat acest
lucru, dar cine se mai preocupă azi de reeditarea scrierilor
acestor învăţaţi? Am în vedere aici scrierile lor pe teme
57

Solomon Marcus

generale, ca educaţia matematică şi de istorie a ştiinţei; cât
de utilă ar fi publicarea unei ediţii critice a cărţii lui Petre
Sergescu Gândirea Matematică! Cine să explice
publicului, oamenilor de cultură natura profesiei de
matematician, miza ei ştiinţifică, socială şi culturală, dacă
nu matematicienii, prin asociaţiile lor profesionale?
Ne plângem periodic de neînţelegerea, din partea
societăţii, a factorilor de putere, în ceea ce priveşte natura
profesiei noastre. Intervine problema accesibilităţii: muzica
şi pictura plac unui număr mare de oameni, care cred că le
înţeleg, pe când matematica rămâne accesibilă numai unui
număr restrâns de specialişti. Cu un anumit efort,
matematicienii ar putea micşora, dar nu şi elimina această
discrepanţă. Mai este şi faptul că matematica a demonstrat,
încă din Antichitate, şi virtuţi aplicative, ea nu este numai
ceva de contemplat, ci şi un instrument esenţial în inginerie,
tehnologie şi economie; de îndată ce societatea a sesizat
acest fapt, pretinde mereu matematicii să schimbe în bine
viaţa oamenilor, fără a observa că aplicaţiile profunde nu
au fost cele planificate, ci acelea care au căzut ca un fruct
copt din pomul cunoaşterii, la momentul oportun, care
poate fi după un an, după zece, după o sută sau după o mie
de ani. Ar fi fost de dorit ca anii de şcoală să inculce în
mintea elevilor aceste idei şi ca ele să marcheze evoluţia
lor ulterioară. N-a fost să fie. Suntem de aceea încă departe
de recunoaşterea acestei realităţi.
58

Rãni deschise

O părere a lui Mircea Eliade
Nu de puţine ori, matematica este combătută indirect,
prin referire la primejdia pe care o prezintă ştiinţa. Iată ce
scria în jurnalul său, în octombrie 1944, Mircea Eliade
(Vatra, nr. 9, septembrie 1984, p. 13): ,,De două zile privesc
cu mare atenţie savanţii. Gândul că aş putea ajunge şi eu ca
oricare dintre ei îmi amorţeşte sarcasmul, întristându-mă.
Oamenii aceştia, probabil, ştiu foarte mult despre foarte
puţin, căci aşa definea cineva savantul, un om care ştie din
ce în ce mai mult despre din ce în ce mai puţin. Unul
cunoaşte tot despre pietre, iar altul aproape tot despre
anumite hârtii. Sunt nişte posedaţi şi, dacă îşi cheltuiesc
zilele şi nopţile în laborioase cercetări, nu o fac pentru
aflarea adevărului şi înlăturarea erorii, ci, în primul rând,
pentru că nu pot face altceva, şi nu pot face altfel pentru că
sunt pasionaţi de munca lor, dominaţi de o patimă egală
aceleia a unui colecţionar de timbre sau de melci.
Majoritatea savanţilor, din întreaga lume, sunt simpli
meseriaşi, neodihniţi salahori. Pe figurile lor nu citeşti nobila
oboseală a gândului, obsesia delirantă a adevărurilor
fundamentale, pasiunea pentru înţelegerea globală a lumii,
ci numai surmenajul fizic, modestia sau trufia, timiditatea
sau absenţa. Oare pentru fixarea şi prosperarea acestui tip
uman au venit pe lume, au gândit, s-au zbătut şi au suferit
un Aristotel, un Galileu, un Leonardo da Vinci, un
59

Solomon Marcus

Leibniz?!” Încheierea creează o ambiguitate, deoarece face
referinţă la matematicieni care nu au fost numai
matematicieni, deci nu e clar la ce ipostază a lor se referă.
Dar gândul ne poartă inevitabil la reflecţia lui Heidegger:
,,Wissenschaft denkt nicht”, iar mai departe, în trecut, am
putea merge la Goethe, căruia i se atribuie adversitatea sa
faţă de matematică. Dar, în acelaşi timp, nu a lipsit nici
tradiţia cealaltă, de cultivare a ideii de incompatibilitate a
celor două culturi. Cel mai bun exemplu ne este oferit de
atitudinea faţă de Eminescu, a cărui operă şi ale cărui Caiete
ilustrează o personalitate călăuzită de convingerea totalităţii
drept condiţie a unei autentice culturi.
Cazul Eminescu este emblematic şi pentru situaţia
matematicii în şcoală. Aşa cum aflăm din Viaţa lui Mihai
Eminescu de G. Călinescu (Editura pentru literatură,
Bucureşti, 1964, p. 60), Eminescu a mărturisit el însuşi că
a fost o victimă a unui învăţământ defectuos al matematicii:
,,Eu ştiu chinul ce l-am avut însumi cu matematica în
copilărie, din cauza modului rău în care mi se propunea,
deşi de fel eram unul din capetele cele mai deştepte.
N-ajunsesem nici la vârsta de 20 de ani să ştiu tabla
pitagoreică, tocmai pentru că nu se pusese în joc judecata,
ci memoria. Şi, deşi aveam şi memorie fenomenală, numere
nu puteam învăţa deloc pe de rost, încât îmi intrase în cap
ideea că matematicile sunt ştiinţele cele mai grele de pe faţa
pământului.” Aici se opreşte citatul reprodus de Călinescu,
60

Rãni deschise

dar din ms. 2258, filele 251 şi 251 verso, aflăm şi
următoarea continuare: ,,În urmă am văzut că-s cele mai
uşoare, desigur, de-o mie de părţi mai uşoare decât limbile,
care cer memorie multă. Ele sunt un joc cu cele mai simple
legi ale judecăţii omeneşti, numai acele legi nu trebuie puse
în joc goale şi fără un cuprins [...]”. Tocmai din aceste
ultime rânduri aflăm că Eminescu nu a rămas certat cu
matematica; însă, şcolar fiind, a fost o victimă a unei
pedagogii inadecvate. Ulterior, ca revizor şcolar, avea să
cunoască în mod profund racilele învăţământului
matematic. Problemele discutate de Eminescu în scrierile
sale pedagogice – rapoartele de revizor şcolar etc. îşi
păstrează şi azi actualitatea. Pe măsură ce s-a maturizat,
matematica i-a devenit foarte apropiată. Din păcate, azi, la
peste 140 de ani de la anii de şcoală ai lui Eminescu, încă
nu putem spune că am rezolvat problema accesibilităţii şi
atractivităţii matematicii şcolare. Generaţii după generaţii
continuă să cadă victime ale unei pedagogii defectuoase, la
noi şi aiurea.
Încercări de excludere a matematicii din cultură
Cercetătorii creierului afirmă că acesta se constituie
într-o reţea de module interactive, fiecare modul având o
anumită specializare, dar cu tendinţa de a inhiba activitatea
modulelor vecine. Aşa se face că o slăbire a centrilor
61

Solomon Marcus

sonori poate avea drept rezultat o amplificare a celor
vizuali. Probabil că, în mod instinctiv, cei îndrăgostiţi de
literatură, de exemplu, cred că îşi accentuează această
pasiune dacă îşi subliniază antitalentul lor matematic; dar
prin aceasta ei îşi trădează prejudecata asupra matematicii,
văzută ca ceva opus umanului şi artisticului. Paradoxal,
această judecată funcţionează concomitent cu credinţa
celor mai mulţi matematicieni în rolul fundamental pe care
îl are factorul estetic în creaţia matematică. Jacques
Hadamard sublinia unitatea de structură a proceselor de
creaţie ştiinţifică şi artistică. De unde ar putea proveni
această neînţelegere? Răspunsul nu comportă nicio
ambiguitate: din modul greşit în care se face educaţia
matematică.
Există însă şi o altă faţă a medaliei. Ştiinţa în general,
dar matematica cu precădere este, în mentalitatea multor
intelectuali, în afara culturii. ,,Argumentele” sunt
numeroase. Se pretinde că în cultură îşi au loc numai
operele perene, în timp ce în ştiinţă totul e perisabil, fiecare
perioadă istorică aduce în atenţie noi teorii, noi paradigme,
care le înlocuiesc pe cele anterioare. Se mai pretinde că, în
cultura unui popor, îşi au loc numai acele opere care îi
conferă identitate, în timp ce ,,ştiinţa nu are patrie”, ea este
universală, aceeaşi în toată lumea, deci nu este identitară.
Cultura este o sumă de valori umane şi sociale, în timp ce
ştiinţa este în căutarea obiectivităţii, subiectul (adică fiinţa
62

Rãni deschise

umană) fiind pus între paranteze. Alţi autori, cum ar fi
filosoful român Constantin Noica, opun ştiinţa filosofiei:
,,În ştiinţă este vorba despre lucruri chiar atunci când se
vorbeşte despre om; în filosofie este vorba despre om chiar
atunci când se vorbeşte despre lucruri.” Dacă omul este
gonit din ştiinţă, ştiinţa este scoasă din cultură, deoarece
cultura este axată pe om. Ne oprim aici cu argumentele
aduse în sprijinul caracterului anticultural al ştiinţei şi îl
lăsăm pe cititor să le combată. Fapt este că în practica
politică şi culturală ştiinţa este separată de cultură, ele au
ministere diferite, instituţii diferite, reviste diferite şi edituri
diferite. Puţinele publicaţii care încearcă să le cuprindă pe
amândouă nu pot evita dezechilibrul dintre ele.
Prezenţa matematicii în mass-media
Prezenţa matematicii în mass-media este foarte
scăzută, iar rarele cazuri în care ea se produce conţin de
obicei distorsiuni care o compromit; faptul că aceste
distorsiuni nu sunt deliberate nu schimbă prea mult situaţia.
Nu a fost aproape deloc mediatizat Congresul al Cincilea
al Matematicienilor Români. Nici măcar decorarea, cu acel
prilej, a unor matematicieni nu s-a bucurat de atenţia
televiziunii şi a presei centrale. Vinovaţi de această situaţie
sunt atât matematicienii (stângaci în contactele cu presa),
cât şi gazetarii (care îi înţeleg prea puţin pe matematicieni
63

Solomon Marcus

şi nici nu îi consideră o categorie profesională de interes
mediatic). Excepţie fac olimpicii care sunt asimilaţi cu
sportivii şi beneficiază de toate favorurile acordate acestora.
Publicului i se sugerează, astfel, că medaliaţii de la
olimpiadele de matematică reprezintă expresia supremă a
valorii matematicii româneşti. Pe de altă parte, zeci de
matematicieni care, datorită harului şi muncii lor, au reuşit
să se facă apreciaţi de comunitatea matematică
internaţională şi ridică prestigiul ţării noastre în lume rămân
cu totul ignoraţi de presa scrisă şi de televiziune.
Relaţiile cu puterea politică
Chestiunea relaţiilor cu puterea politică rămâne,
pentru comunitatea matematică românească, o problemă
deschisă. S-au menţinut întotdeauna compromisurile în
limitele strict necesare ori s-a manifestat şi un exces de
zel? Numai o examinare atentă a documentelor, coroborată
cu amintirile celor care au trăit în epoca respectivă, va
putea da răspunsul. Dar vom avea curajul şi vom manifesta
perseverenţa în a dezvălui şi analiza toate acestea? A fost
necesară prezenţa unor matematicieni în comitetul central
al partidului comunist? A fost necesară publicarea unor
articole de plecăciune în faţa dictatorilor? Cât a fost impus
de împrejurări şi cât a fost imoral? Lăsăm aceste întrebări
fără răspuns, dar ele ne vor chinui în continuare.
64

Rãni deschise

Textul de faţă alătură unele fragmente din Raportul
pe care l-am prezentat la al Cincilea Congres al
Matematicienilor Români, Piteşti, 2003. Varianta
prezentată trebuie considerată provizorie. Varianta
ameliorată a fost prezentată la Congresul Internaţional de
Educaţie Matematică (Copenhaga, 2004) sub formă de
carte (Mathematics in Romania, CUB Press 22, Baia
Mare). Acţiunea a fost posibilă cu sprijinul Agenţiei pentru
Strategii Guvernamentale (grant no. 406/28.03.2004)
şi datorită suportului financiar acordat de un contract
al Institutului de Matematică al Academiei Române.

65

