Solomon Marcus

Olimpiade şi concursuri,
între competiţie şi educaţie
Tribuna învăţământului, nr. 953,
9-15 iunie 2008, p. 1, 7

Am participat la multe întreceri ale elevilor la diferite
discipline, cele mai recente fiind Olimpiada Naţională de
Matematică, pentru clasele VII-XII, de la Timişoara, la
sfârşitul lunii aprilie 2008, şi Concursul „Plus sau minus
poezie“ pentru clasele V-VI, faza naţională, Galaţi, 16-18
mai, 2008. Acest din urmă concurs, iniţiat în anul 2007, se
adresează elevilor care sunt foarte buni atât la limba
română cât şi la matematică. Atât la Timişoara, cât şi la
Galaţi au ajuns la faza naţională câteva sute de elevi.
Dintre cei premiaţi la Timişoara, s-au ales cei care ne
reprezintă la olimpiade internaţionale. Modul de
desfăşurare a acestor întreceri, cu accent pe solemnitatea
finală, de premiere a celor mai buni, dă impresia că latura
lor competitivă este aspectul cel mai important. Atunci
când, în urmă cu vreo 60 de ani, a apărut ideea de a se
opera un transfer metaforic al ideii de olimpiadă, din
220

Rãni deschise

domeniul sportului în cel al învăţării diferitelor discipline
şcolare, s-a avut în vedere atractivitatea pe care o prezintă
sportul pentru copii şi adolescenţi; s-a sperat că transferul
spiritului olimpic, de întrecere, în viaţa şcolară, va aduce
o creştere a interesului elevilor pentru învăţătură. S-a
sperat că, învelită în această foiţă de ciocolată, problema
de matematică, fizică, chimie sau gramatică să pară mai
puţin amară decât este ea simţită de obicei. Parţial, scopul
a fost atins. Dar numai parţial. Cei mai buni elevi, cei
talentaţi la o disciplină sau alta au avut posibilitatea de a
se evidenţia şi de a primi recunoaşterea pe care o merită;
unii dintre ei beneficiază de burse pentru continuarea
studiilor în străinătate. Să mai observăm faptul că
premierea olimpicilor este partea cea mai mediatizată a
unor discipline ca matematica, fizica şi chimia, care altfel
nu se bucură de aproape nicio atenţie publică.
Spiritul competitiv al olimpiadelor şi concursurilor
şcolare se află în centrul atenţiei şi din alte puncte de
vedere. Cu prilejul întrecerilor de la Timişoara şi de la
Galaţi, câteva mari tarabe au fost acoperite cu cărţi şi
reviste, dintre care cele mai multe îşi afirmă drept principal
obiectiv pregătirea elevilor pentru olimpiade şi concursuri.
Sute şi sute de probleme îi asaltează pe potenţialii
concurenţi, revendicându-le atenţia, pentru ca şansa lor de
a da rezultate optime la olimpiade şi concursuri să fie cât
mai mare. Este clar că spiritul olimpic, înţeles în acest fel,
221

Solomon Marcus

tinde să acapareze viaţa şcolară, nemairămânând loc şi
pentru altceva. Pentru altceva, adică pentru ce? Răspunsul
este simplu: pentru ceea ce constituie scopul şcolii, adică
educaţia. Să vedem despre ce anume este vorba.
Concurentul la întrecerile în discuţie este obligat să
răspundă la întrebări fixate de alţii, de cele mai multe ori i
se furnizează chiar răspunsul, lui rămânându-i numai
verificarea. Multe dintre problemele propuse nu au nicio
noimă, sunt bune pentru olimpiade, dar atâta tot. În
realitate, educaţia constă în a-l conduce pe elev, pas cu pas,
la descoperirea întrebărilor cu motivaţii profunde şi, apoi,
a răspunsurilor la ele, dacă este cazul; ceea ce nu este
obţinut prin propriul tău efort nu este cu adevărat asimilat.
Obligaţia de a rămâne într-un loc fix este şi ea
împotriva naturii; stai de multe ori la masa de lucru ore în
şir şi nu-ţi vine nicio idee, pentru ca ulterior, când ai trecut
la altă activitate, să te trezeşti că te vizitează ideea de care
aveai nevoie. Ce să mai spunem despre constrângerea de
a rezolva o anumită problemă într-un timp determinat, fie
el şi generos, de trei sau patru ore? Şi această condiţie este
împotriva naturii, deoarece îi interzice candidatului să se
rătăcească, încercând o cale diferită de aceea învăţată la
clasă. Explicit nu-i interzice, dar durata fixă are drept
consecinţă aproape inevitabilă faptul că un candidat care
se rătăceşte la unul dintre puncte va intra în criză de timp
la alte puncte. Să mai spunem cât este de important să
222

Rãni deschise

educăm la elevi dreptul de a încerca să gândească cu
propriul lor cap, de a se rătăci eventual, de a greşi, de a
eşua, de a încerca din nou? Nici modul de evaluare, pe
baza unui punctaj, a unui barem, nu este în conformitate
cu felul în care viaţa îl va evalua pe elev mai târziu.
Performanţa ştiinţifică nu este evaluată de o comisie, ci de
întreaga comunitate profesională internaţională, prin
mecanismele bine stabilite ale monitorizării internaţionale
a culturii. Facem toate aceste observaţii nu pentru a cere
desfiinţarea olimpiadelor şi concursurilor, ci pentru a
înţelege cadrul lor de relevanţă. Organizatorii lor trebuie
să observe cu atenţie păstrarea unui echilibru între
caracterul lor de competiţie sportivă şi funcţia lor
educaţională. Există între ele o tensiune, care trebuie
atenuată. Nu premiile sunt cel mai important lucru, ci
participarea, curajul de a te lua la întrecere şi starea de
bucurie care ar trebui să-i anime pe toţi participanţii.
Pentru ca întrecerile acestea să-şi atingă scopul lor
educaţional, este important ca fiecare participant să afle
cum a fost evaluată lucrarea sa, nu doar ce notă a primit.
Lucrarea sa trebuie să se întoarcă tot la el, însoţită de
indicarea tuturor locurilor în care s-au comis infracţiuni.
Prin acest ultim termen, avem în vedere greşeli de tot
felul (logice, gramaticale, de ortografie, de fizică, de istorie
etc.), nonsensuri, vorbe de umplutură, complicări inutile
etc. Infracţiunile pot fi de cu totul altă natură decât aceea
223

Solomon Marcus

avută în vedere în baremuri. Voi da un singur exemplu. La
Galaţi, mulţi concurenţi, în comentarea versului „Dar un
nor fără o corabie nu ştiu cât face” din „Altă matematică“
de Nichita Stănescu, au interpretat pe „nu ştiu” ca „nu se
poate”. Este desigur imposibil de a pretinde unui barem
stabilit în prealabil să fi prevăzut infracţiuni de acest fel.
Dar cele mai multe infracţiuni sunt de un tip pe care
baremurile prealabil alcătuite nu-l prevăd. Cel care
evaluează o lucrare trebuie s-o facă din toate punctele de
vedere. Nu se poate accepta, de exemplu, ca greşelile de
ortografie să fie penalizate numai de profesorul de română.
Toate acestea presupun că locurile în care s-au comis
infracţiuni sunt indicate clar, cel mai bine cu o anumită
culoare. S-ar putea eventual folosi un cod, fiecărui tip de
greşeală rezervându-i-se o altă culoare. Am auzit tot felul
de opinii: că aceste întreceri sunt făcute pentru competiţie,
nu pentru educaţie; că elevii se descurajează când văd
lucrarea lor cu multe sublinieri în roşu; că evaluarea
trebuie făcută rapid şi nu este timp să se facă sublinierile,
mai ales că sunt doi care le evaluează; că sublinierile pot
influenţa judecata celui de-al doilea evaluator etc. Am aflat
chiar că au fost perioade în care Ministerul interzicea
evaluatorului să scrie ceva pe lucrarea elevului. Obsesia
încălcării obiectivităţii (care oricum nu poate fi realizată
pe deplin), obsesia ierarhizării cu orice preţ au eclipsat
exact scopul principal: formarea personalităţii elevului,
224

Rãni deschise

capacităţii sale de a gândi cu propriul său cap. Găsim
întotdeauna pretexte pentru a nu face exact ceea ce este
mai important. De atitudinea faţă de greşeală depinde totul.
Să-i educăm pe elevi în ideea că greşeala nu este un păcat
nu este un lucru ruşinos, ci ţine de normalitate. Să-i
educăm în ideea că au dreptul de a fi oameni liberi: liberi
de a încerca diferite căi de rezolvare a unei probleme, fără
teama că vor fi pedepsiţi în cazul unui eşec. Greşeala şi
eşecul sunt preţul inevitabil pe care trebuie să-l plătim
pentru a ne forma ca oameni cu personalitate. Mai sunt şi
alte probleme care merită a fi discutate. Ce cărţi li se oferă
premianţilor? Am fost plăcut impresionat de faptul că
Fundaţia Traian Lalescu a oferit, la Timişoara şi la Galaţi,
drept premii, exemplare din cartea despre viaţa lui Traian
Lalescu şi cartea de poezii a fiului marelui matematician.
Dar întreaga problemă a premiilor trebuie reexaminată.

225

