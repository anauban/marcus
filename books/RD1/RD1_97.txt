Rãni deschise

Capcane ale comunicării şi ale limbii române
Interviu acordat lui Ioan Dănilă, conf. univ.
la Universitatea din Bacău – Bacău, aprilie 2010

Partea întâi
I. Dănilă: Avem bucuria să ne fie din nou oaspete
domnul academician Solomon Marcus. Domnule Profesor,
bine aţi venit la emisiunea noastră şi vă mulţumim pentru
bunăvoinţa de a trata un subiect aproape fără de sfârşit,
starea limbii române. Vă propun să plecăm de la conceptul
de normă, pe care să-l detaliem. Norma lingvistică, norma
literară în accepţia lui Eugen Coşeriu sau în accepţia
generală, pentru că, orice am face, trebuie să ne raportăm
la o normă, fie ea socială, ştiinţifică, şi în cazul de faţă
vorbim despre norma ştiinţifică în limba română.
S. Marcus: Am publicat un articol cu tema
„Gramatica, între normativ şi descriptiv”. Oamenii de
ştiinţă sunt interesaţi în primul rând de aspectul descriptiv
analitic, iar cei care conduc instituţii au răspunderi
instituţionale în ministere, în academii, trebuie să aibă în
853

Solomon Marcus

vedere şi aspectul normativ. Asistăm la o realitate evidentă
în ultimii 20 de ani, şi anume faptul că nu mai există o
autoritate suficientă a normei, autoritate care să se impună
în mod general. Acest lucru se datorează şi faptului că o
normă stabilită trebuie explicată utilizatorilor, pentru ca
ei să înţeleagă cărui fapt se datorează aceasta. E normal
ca o regulă impusă, dar neînţeleasă de majoritatea
vorbitorilor, să nu aibă prestigiul dorit. Asta s-a întâmplat
în ultimii 20 de ani şi, începând cu „â” şi „î” sau cu „sunt”
şi „sînt”, divergenţele au apărut chiar în rândul
specialiştilor. Diferenţele nu au apărut între oamenii de
ştiinţă şi profani, între profesionişti şi amatori, ci deruta a
existat chiar în interiorul breslei lingviştilor. Nu mai e
nevoie să amintesc nume de specialişti care s-au plasat de
o parte sau de alta, nume la fel de prestigioase. Limba e
un fenomen atât de complex, încât se poate întâmpla ca
anumite aspecte istorice să pledeze pentru o variantă, iar
altele pentru o altă variantă. Depinde de punctul de vedere
în care ne situăm.
Nu trebuie tratate lucrurile cu un radicalism
intransigent. Chiar în probleme recente de ortografie, este
clar că norma de scriere pentru „niciun” şi „nicio”, scrise
într-un singur cuvânt, n-a întâmpinat o acceptare
majoritară. Aceste reguli trebuie totuşi stabilite; nu putem
lăsa să domine arbitrarul. Există o serie de situaţii în care
trebuie acceptată coexistenţa mai multor variante. Ele
854

Rãni deschise

trebuie să rămână la alegerea vorbitorului. E bine
cunoscută situaţia cuvântului „filozofie” versus
„filosofie”, faţă de care, chiar în interiorul Academiei
Române, lingviştii şi filosofii se situează pe poziţii
diferite. Există încă multe alte asemenea exemple. Sunt
de asemenea exemple în care trebuie făcute sondaje
pentru a constata care este situaţia. A existat la
un moment dat o normă ortografică permiţând o
diversificare semantică a pluralului „nivele” de pluralul
„niveluri”.
I.D.: În prima ediţie a Dicţionarului ortografic,
ortoepic şi morfologic, din 1982, diferenţierea era clară.
„Nivele” era pluralul pentru „nivelă” – un instrument ce
măsoară orizontalitatea unui plan, iar „niveluri” era
pluralul cuvântului „nivel” – stadiu sau etaj. Noua ediţie
din 2005 a dicţionarului nu mai face distincţia între aceste
variante şi dă libertatea folosirii la alegere a celor două
forme de plural.
S.M.: Da, aşa este.
I.D.: Este o criză de autoritate ştiinţifică.
S.M.: Din moment ce nu există unanimitate în
interiorul breslei profesioniştilor, n-ai cum să pretinzi
această unanimitate în afara breslei. Acest lucru nu este
posibil.
855

Solomon Marcus

I.D.: Discuţia aceasta mă duce cu gândul la clasica,
de acum, discuţie despre „succese” şi „succesuri”.
S.M.: Aici mi se pare că „succesuri” sună urât.
I.D.: Categoric, dar la nivelul limbii române există o
concurenţă permanentă în cazul pluralului substantivelor
neutre între terminaţiile „e” şi „uri”. Avem libertatea de
a le folosi la alegere.
S.M.: Cred că insistenţa în a discuta aceste chestiuni
este nemotivată, în timp ce atâtea alte probleme grave
există în folosirea limbii române.
I.D.: Absolut aşa este. A folosi „fortuit” în loc de
„forţat” mi se pare o greşeală, în timp ce a folosi pluralul
„e” în loc de „uri” este adesea o chestiune de convenţie.
S.M.: Desigur că şi în vorbire există greşeli benigne
şi greşeli maligne, trebuie să le diferenţiem, nu toate au
aceeaşi importanţă.
I.D.: Categoric. Dar, domnule profesor, există o
atitudine a colectivului de redactare a DOOM 2 din 2005,
relativ la faptul că modelul este reprezentat de vorbirea
bucureşteană. Poate da capitala un model şi în privinţă
lingvistică?
S.M.: Aici m-aţi prins pe un teritoriu pe care nu sunt
pregătit să răspund. Contactele mele cu vorbitorii de limbă
856

Rãni deschise

română nu sunt atât de variate, încât să pot să mă pronunţ.
Eu pot să vă spun că în Bucureşti, când mergi în şcoli,
descoperi argoul elevilor, argou pe care-l găseşti şi pe
internet. Comentariile elevilor, referitoare la examene şi la
profesorii lor, sunt într-un cu totul alt limbaj.
I.D.: Aceasta este o realitate generală, dar eu mă
refeream la faptul că modul de a vorbi al bucureşteanului
ar trebui să devină o normă, aşa cum, cândva, a devenit o
normă supradialectală a subdialectului muntean.
S.M.: Nu capitala ar trebui să dea tonul vorbirii
curente, dar acesta este prototipul.
I.D.: Sunt variaţii ca pluralul „cireşi – cireşe” sau
chiar lipsa determinării negative ca în cazul „Am decât o
carte” în loc de „Nu am decât o carte” Aici e un fenomen
de redundanţă. Practic, nu am nevoie decât de o negaţie,
dar există această obişnuinţă, această tradiţie în utilizarea
limbii, pe care nu o putem contesta.
S.M.: Dar trebuie să ne înţelegem. Alegerea unei
norme este de multe ori o chestiune de convenţie. Noi
trebuie să facem o alegere şi ni se pare mai comodă una
dintre variante. Dar nu chestiunile de această natură mi se
par cele mai importante din punctul de vedere al
pericolelor ce pândesc evoluţia limbii române, a posibilei
ei degradări.
857

Solomon Marcus

I.D.: E vorba de proprietatea termenilor.
S.M.: E vorba în primul rând de sărăcia vocabularului
multor vorbitori, de folosirea inadecvată a multor cuvinte,
folosire improprie, ca şi de conflictul care apare între
aspectul lingvistic şi cel logic. Aici e vorba de o rană
deschisă, pentru că nu mă refer doar la vorbitorii de pe
stradă, ci şi la cei pe care îi vedem în talk-show-uri la
televiziune. Se vede clar că uneori ei spun una, dar se
gândesc la cu totul altceva.
I.D.: Manipularea prin limbaj.
S.M.: Această neconcordanţă între ce vrei să spui şi
ce spui dă naştere la tot felul de pseudocontroverse.
Vorbeam de sărăcia vocabularului şi am să vă dau un
exemplu. Discuţia noastră are loc la Şcoala „Al. I. Cuza”
din Bacău.
I.D.: Suntem chiar în cabinetul de Limba şi
Literatura română, Sergiu Şerban.
S.M.: Am participat la festivităţile aniversare ale
şcolii, cu ocazia împlinirii a 150 de ani de activitate. Or,
iată, în contrast cu inscripţiile pe care le vedem pe pereţi:
„Limba este tezaurul cel mai preţios pe care-l
moştenim....”, ce am putut vedea zilele trecute la serbarea
şcolii. Am văzut un spectacol în care fiecare copil venea
pe scenă şi citea, de pe o foaie foarte mare, un text de
858

Rãni deschise

vreo patru rânduri. Nu i se vedea privirea, pentru că era
cu ochii în foaie. În primul rând, e de neînţeles de ce,
când ai de spus doar patru rânduri, nu eşti capabil să le
spui liber, fără să trebuiască să le citeşti. În al doilea rând,
ce reprezentau textele respective? Erau nişte improvizaţii
de multe ori dulcege, în timp ce noi avem în literatura
română atâtea texte frumoase. Era exact o bună ocazie de
a ne prevala de texte din marii scriitori români, din marii
intelectuali români. Şi, în loc să vedem lucrul acesta, am
asistat la rostirea unor texte fără substanţă, nu le am sub
ochi ca să le putem comenta, dar e clar că ele nu puteau
rivaliza cu ceea ce ne rezervă mai bun cultura
românească.
I.D.: Le-am urmărit şi eu. Aveau o notă comună,
elogiul adus dascălului. De fapt, aceste texte, alcătuite de
elevi şi redactate de ei, totuşi corect, nu erau impuse, ci
proveneau din simţămintele lor.
S.M.: Da, dar toate erau alcătuite linear, la fel, într-un
mod superlativ naiv.
I.D.: Măcar la o zi aniversară merită.
S.M.: Dar şi ieri s-a repetat fenomenul acesta, ca şi
textele cântate de diverşi elevi.

859

Solomon Marcus

I.D.: E conform modelului mass-mediei şi al culturii
de internet.
S.M.: Sunt lucruri cu totul superficiale, cu un
vocabular foarte sărac. N-am văzut nicio referire, nicio
utilizare a marilor texte din cultura română, din istoria
literaturii. La fel stau lucrurile şi cu istoria. Revenea mereu
sintagma „permanent învingători, permanent triumfători”,
această versiune conform căreia de-a lungul a 2.000 de ani
am dus-o din victorie în victorie, într-o exprimare dulceagă
şi săracă în vocabular.
I.D.: Domnule Profesor, aş vrea să discutăm despre
volumul Gramatica limbii române. E un subiect fierbinte,
deoarece el nu a intrat încă în şcoli. Am aici primul volum
rezervat substantivului, în care sunteţi citat.
S.M.: Nu putem discuta despre chestiunea citărilor în
modul în care este alcătuită această lucrare. Nimeni nu este
citat, pentru că, deşi există la sfârşit o bibliografie, nu
există în text locul şi modul în care este folosită sursa
respectivă.
I.D.: Este foarte adevărat. Dumneavoastră sunteţi
citat la capitolul rezervat substantivului, la bibliografia de
la sfârşit, cu un articol apărut în 1962 în „Studii şi
Cercetări lingvistice”, despre genul substantivelor. Or, în
interior, lucrurile arată puţin altfel.
860

Rãni deschise

S.M.: Trebuie să vă spun că acel model al genului
gramatical s-a bucurat de multă atenţie în lume. Îndoiala
lui Rosetti, care altfel mă aprecia foarte mult, atunci când
am prezentat genul gramatical, provenea din faptul că, din
principiu, el nu credea că ar putea să existe un model logic
al acestei categorii. El considera că genul gramatical nu e
o chestiune de logică. Pe mine însă mă interesa cu totul alt
lucru, pentru că matematicienii nu caută să lămurească
semnificaţii în sine, ce e genul natural, genul gramatical, ci
cum se trece de la unul la altul, ceea ce e cu totul altceva;
am identificat anumite operaţii logice prin care se trece de
la genul natural, adică sex, la genul gramatical, le-am testat
pe mai multe limbi şi a ieşit ceva foarte coerent. A fost un
model care a avut un ecou puternic în literatura lingvistică,
mai întâi la ruşi, care au dezvoltat mult aceste lucruri, şi pe
urmă în Occident. Asta este o distincţie pe care o faci cu
gândirea matematicianului şi la care nematematicienii se
gândesc mai puţin. Pe matematician nu îl interesează lucrul
în sine, natura genului, ci relaţia, modul în care se trece.
Astfel se ajunge la introducerea unei noţiuni de distanţă
între masculin şi feminin, între masculin şi neutru sau între
feminin şi neutru, distanţă între genul natural şi cel
gramatical. Aceste lucruri le-am prezentat mai întâi la
Congresul Internaţional de Lingvistică de la Bucureşti în
1967 şi am revenit la Congresul Internaţional de Lingvistică
Romanică, Bucureşti, 1968, cu o comunicare despre o
861

Solomon Marcus

tipologie a genului gramatical în limbile romanice, faţă de
limbile slave. Am avut controverse puternice, în special cu
un lingvist din Timişoara, dar era vorba de diferenţa dintre
un mod tipic de gândire al unui matematician, faţă de cel
al unui lingvist. Dacă o să vă uitaţi în orice tratat de
gramatică, nu o să găsiţi nicăieri o definiţie generală a
categoriei gramaticale. Se găsesc doar definiţii pentru
categorii punctuale, ca de pildă a cazului gramatical, dar şi
acelea sunt cu totul superficiale. Or, iată cum, cu ajutorul
logicii şi al matematicii, s-a putut identifica un model al
categoriei gramaticale, în care intră şi cazul, şi genul, şi
numărul, la fel ca şi o serie de categorii sintactice.
I.D.: Un fel de gen proxim, urmat de o diferenţă
specifică. Domnule Profesor, vă mulţumesc foarte mult. E
o invitaţie la lectură, nu numai din Gramatica Limbii
Române, dar în primul rând din cărţile dumneavoastră.
Vă mulţumesc pentru participarea la emisiunea noastră.
S.M.: Vă mulţumesc.
Partea a doua
I.D.: Invitatul nostru de astăzi este, din nou,
academicianul Solomon Marcus. Domnule Profesor, vă
mulţumesc pentru bunăvoinţa de a participa la o nouă
discuţie despre limba română, despre rolul şcolii în
862

Rãni deschise

cultivarea limbii române şi despre starea ei în învăţământul
românesc. V-aş propune să plecăm de la discrepanţa dintre
ceea ce enunţăm şi ceea ce înfăptuim. Avem aici un citat al
lui Vasile Alecsandri despre transmiterea limbii române ca
o moştenire şi care este totodată un reflex al relaţiilor dintre
generaţii. Mai comunică astăzi generaţiile între ele?
S.M.: Între timp, chiar a apărut pe coperta manualului
de limba română termenul de comunicare.
I.D.: Da. După 1990.
S.M.: De ce a fost nevoie să se introducă termenul de
comunicare? Pentru că, pe vremea lui Alecsandri, nu exista
alt mijloc de comunicare decât cea directă, prin cuvinte.
Nu exista nici telefon, nici radio, nimic. Singura
comunicare se făcea prin vorbire şi scriere în mod direct.
Acum există atâtea moduri de comunicare, diversitatea lor
e atât de mare, iar cantitatea actelor de comunicare a
devenit uriaşă. Astfel au apărut probleme cu totul noi.
Progresul a fost enorm atât în privinţa distanţei la care se
poate comunica, cât şi a vitezei de comunicare. Dar această
facilitate implică şi riscul superficialităţii. Faptul că e atât
de uşor să scrii un mesaj electronic creează tendinţa de a
bagateliza prin frecvenţă acest act, de a-l transforma
într-un joc sintactic. Acum poţi transmite pe cale
electronică, într-un timp foarte scurt, 100 de mesaje de
felicitare şi din cauza asta cuvintele nu mai au greutatea
863

Solomon Marcus

afectivă, psihologică pe care o aveau altădată, când ele
erau rezultatul unui efort.
I.D.: E şi problema revenirii asupra cuvântului.
S.M.: Problema este următoarea: Alecsandri se referă
la comunicarea dintre generaţii, dintre bunici, părinţi,
copii. Or, aici s-a creat o defecţiune majoră, un lucru care
scapă, în general, celor din domeniul educaţiei. Este
recunoscut în principiu, dar în fapt nu i se acordă atenţie,
nu se ţine seama de el. Copilul nu petrece în şcoală mai
multe ore decât în familie şi eu cred că principalii actori ai
educaţiei sunt mai întâi membrii familiei în care trăieşte şi
abia după asta intervin dascălii.
I.D.: E un fapt cunoscut.
S.M.: Dar nu se ţine seama de asta. Ştiţi ce a rezultat
din discuţiile cu copiii?
I.D.: Dialogul orizontal.
S.M.: A rezultat următorul lucru: Radiografiind modul
în care discută ei între ei, se vede insistenţa lor în a accentua
faptul că se simt neînţeleşi, că nu sunt înţeleşi de părinţii lor,
pretind ei, şi nici de o bună parte din profesori. „Nu ne
înţeleg”, zic ei, şi de aici apar un sentiment de frustrare, nevoia
de izolare şi de a-şi crea o lume a lor, ceea ce duce la apariţia
argoului. Am urmărit anul trecut pe internet, în perioada
864

Rãni deschise

bacalaureatului, discuţiile dintre candidaţii care ieşeau şi cei
care urmau să intre la examen. Cei care ieşiseră dădeau sfaturi
celorlalţi şi discutau toţi într-un limbaj, într-un argou, care era
evident altul decât cel în care comunicau cu părinţii şi
profesorii lor. Într-un fel, li se face portretul acestora şi se
poate afla cum sunt ei văzuţi de către copii.
Am înţeles clar un lucru, ca să mă întorc iar la ce
spunea Vasile Alecsandri. Când i-am întrebat pe copii ce
ştiu ei despre părinţii lor, în mare măsură ştiau unde s-au
născut, când s-au născut, dar despre bunici ştiau mult mai
puţin. Întrebaţi cu ce se ocupă părinţii şi bunicii lor, ei ştiau
mult mai puţine lucruri. Unul dintre copii mi-a răspuns:
„Ştiu că bunicul de abia aştepta să se pensioneze.” Se vede
clar faptul că nu există nicio comunicare mai bogată cu
părinţii şi bunicii lor. Totul se reduce la necesităţile
imediate de tipul: „Ai mâncat?”, „Pune-ţi fularul, să nu
răceşti.” E o comunicare redusă la imperative scurte,
cuvinte, sintagme şi îndemnuri. Nu există nicio
comunicare de natură mai amplă, o comunicare în care să
se vadă o poveste. Iar lipsa de interes e de ambele părţi.
Copilul porneşte cu prezumţia că el trăieşte o altă realitate
decât părinţii săi şi că nu are ce să afle de la ei, iar părinţii
şi bunicii fie n-au timp prea mult, fie nu sunt în stare de
mai mult, intelectual vorbind. Chiar dacă au biblioteci în
casă, nu se vede că ar fi marcaţi puternic de acest fapt.
Desigur că există şi excepţii.
865

Solomon Marcus

Am întâlnit într-o clasă pe nepotul unui bine cunoscut
matematician şi s-a văzut imediat. Se vede uşor diferenţa
dintre un copil căruia părinţii şi bunicii îi acordă atenţie şi
cel care nu beneficiază de această atenţie. Copilul care nu
beneficiază de atenţie nu are exerciţiul unui răspuns
constând din fraze construite complet şi care să decurgă
logic una din alta. Ei nu pot răspunde decât cu frânturi de
fraze. Acest lucru se întâmplă şi la emisiunile de la
televizor, când foarte puţine persoane reuşesc să ducă o
frază până la capăt. Vorbitorul fie e încălecat în discurs de
altcineva, fie e victima unor anacoluturi. Începe o frază
într-un fel şi deodată o trece în cu totul alt context. Exact
ceea ce cerea Alecsandri nu se mai întâmplă.
I.D.: E grea meseria de părinte, dar eu aş vrea să ştiu
ce ar trebui să facă şcoala în această situaţie.
S.M.: Problema nu e simplă. Trebuie să admitem că,
mai cu seamă la vârste mici, o clasă nu trebuie să conţină
mai mult de 15 copii. Este esenţial ca elevii să vadă că se
bucură de o atenţie diferenţiată din partea dascălului,
niciunul dintre ei să nu se creadă un simplu element
dintr-o turmă. El trebuie să simtă că are o relaţie personală
cu profesorul. Asta însă presupune o clasă nu prea mare,
iar pe de altă parte, e nevoie şi de o relaţie personală între
dascăl şi părinţii fiecărui elev, astfel ca profesorul să ştie,
pentru fiecare elev în parte, pe ce se poate baza, cu ce bagaj
866

Rãni deschise

afectiv, intelectual, moral vine el de acasă. Numai aşa se
poate conjuga cum trebuie acţiunea acestor actori
importanţi, profesori şi părinţi, implicaţi în procesul de
educaţie.
I.D.: Altfel nu se poate închide triunghiul.
S.M.: La clasele mari e poate mai puţin important,
dar la şcoala primară este absolut esenţial; eu zic că şi la
gimnaziu.
I.D.: Categoric.
S.M.: Pe măsură ce înaintează în vârstă, problema nu
se mai pune atât de acut şi doar aşa învaţă un elev să
comunice. De asemenea, nu se prea mai citeşte; chiar
elevii cu rezultate bune nu citesc mai nimic. Vă spun din
nou că am urmărit spectacolul de ieri al şcolii: insistenţa
cu care se succedau cântece cu texte ieftine, cu dragoste,
soare, iubire etc.; texte de o superficialitate absolut
penibilă, care nu mi s-au părut potrivite cu vârsta lor;
potrivit nu era nici dialogul între doi fraţi gemeni cu
întrebări de genul: „Mai fumezi?” Ce rost are această
discuţie despre nişte vicii care nu corespund vârstei lor?
I.D.: Domnule Profesor, teoria aceasta a discursului,
aplicată în şcoală, ar trebui să privească pe toţi profesorii,
indiferent de specialitate.
867

Solomon Marcus

S.M.: Aici e iar un lucru important şi vă mulţumesc
că l-aţi adus în discuţie. Metoda asta a baremelor a dus la
situaţii îngrozitoare, în care un test la matematică poate
conţine grave greşeli de ortografie şi gramatică, care rămân
nepenalizate pentru că nu sunt în barem, aşa cum la
gramatică poţi comite greşeli de istorie sau geografie pe
care baremul nu le are în vedere. Acest sistem de verificare
cu barem ar trebui relativizat, deoarece în realitate, într-o
lucrare scrisă, se poate întâlni o serie întreagă de tipuri de
greşeli. Poţi să nu fii relevant, să nu fii în subiect, există o
infinitate de moduri în care poţi rata un subiect şi
niciun barem nu le poate cuprinde pe toate.
Eu cred că acest sistem ar trebui desfiinţat. Particip
anual, în luna mai, la concursul „Plus sau minus poezie”,
unde am reluat şi eu lucrări citite şi notate de membrii
comisiei şi m-am îngrozit văzând ce-au făcut corectorii.
Am găsit tot felul de infracţiuni nepenalizate, deoarece ele
nu intră în barem. Cele mai reprezentative dintre ele sunt
nonsensurile. În cartea mea Şocul matematicii, am un
capitol întreg dedicat acestora, Urmuz la examenul de
admitere.
I.D.: Sunt trecute cu vederea. Se merge doar pe
punctarea răspunsurilor valabile, şi nu pe depunctare.
S.M.: Avem obsesia obiectivităţii notării. Profesorul
are ca argument faptul de a se pune la adăpost de obiecţiile
868

Rãni deschise

părinţilor care vor justificări pentru notele primite de copiii
lor. Acest lucru trebuie descurajat. Atunci când citesc o
lucrare, indiferent de disciplina la care este încadrată, ea
trebuie evaluată şi notată ţinând cont de toate
perspectivele.
I.D.: Corectitudinea lingvistică şi corectitudinea
ştiinţifică.
S.M.: Corectitudinea informaţiei de orice natură,
relevanţa în raport cu subiectul, aspectele logice, ca şi limba
română folosită, toate sunt importante. Greşelile în folosirea
limbii române ascund adeseori greşeli de logică, gândire
incorectă sau confuzii esenţiale în folosirea unor noţiuni.
Diferenţa dintre o condiţie necesară şi una suficientă, modul
în care negăm o propoziţie, toate sunt importante. Am
observat, de pildă, la talk show-urile de la diverse posturi de
televiziune, făcute cu diverşi politicieni, că există tendinţa
de a se confunda planurile de discuţie. Ca să-şi compromită
partenerul de discuţie, care este membru al altui partid, caută
să fie încadrat în dialog la categoria mincinoşi, în situaţii în
care termenul adecvat ar fi cel de „greşeală”. Se spune că
minte, pentru a ridica gravitatea faptului.
I.D.: Pentru că vrea să-l compromită.
S.M.: Vă dau un exemplu. Pentru o promisiune
nerealizată, cuvântul „minciună” nu este adecvat. Era
869

Solomon Marcus

exemplul lui Aristotel. Dacă afirmi: „Mâine va avea loc o
bătălie navală”, iar ea nu are loc, aceasta nu este o
minciună. Se confundă minciuna cu greşeala, cu puncte
diferite de vedere. E vorba de o serie întreagă de infracţiuni
care includ şi aspecte lingvistice, logice, aspecte
intelectuale sau morale. Şi atunci, cel ce urmăreşte toate
aceste discuţii ce concluzie poate trage? El aude despre
fiecare personaj, spus de către cineva, că este un mincinos.
Concluzia care se impune este că facem parte dintr-un
popor de infractori şi acest lucru ne împinge spre disperare.
E evident însă că aceste lucruri au două cauze principale.
Pe de o parte, există tendinţa celor care dirijează activităţile
sociale şi deţin mijloacele de comunicare în masă şi care,
în scopul de a-şi mări audienţa, nu promovează decât
scandaluri şi ştiu că o crimă sau o infracţiune are o
audienţă mai mare decât o faptă bună, iar pe de altă parte,
există tendinţa oamenilor politici de a-şi stigmatiza
adversarii în moduri absolut reprobabile şi în toate astea
se amestecă infracţiuni de toate felurile.
I.D.: Ce-i de făcut atunci, Domnule Profesor?
S.M.: Rădăcinile acestei situaţii sunt în primele clase
de şcoală. Trebuie modificat de la rădăcină modul de a
vedea lumea, modul de comportament şi lucrul acesta
trebuie făcut în primul rând cu copiii de la grădiniţă, şcoala
primară şi de la gimnaziu. Altfel, din elevi superficiali şi
870

Rãni deschise

mediocri, se vor recruta politicieni superficiali şi mediocri,
iar cei care vor conduce canalele de televiziune se vor ghida
tot după considerente de acest fel. Totul vine din faptul că
aceşti oameni nu au descoperit la timp, după cum spunea
Al. Paleologu, lucrurile cu adevărat importante în viaţă. Din
păcate, ei nu le descoperă pentru că sunt ghidaţi de anumiţi
părinţi cu mentalităţi cu totul greşite. Şi ei vin şi te întreabă
care e reţeta de succes în viaţă.
I.D.: Asta se întâmplă pentru că modelele sunt
derizorii.
S.M.: Ar fi normal să fie preocupaţi de modul în
care pot descoperi ce e mai interesant, mai frumos în
viaţă, ceva care să-i pasioneze. Ei se interesează de reţeta
reuşitei, a succesului, de partea socială, şi nu de cea
sufletească.
I.D.: În încheiere, mi-aţi promis că vom discuta puţin
despre Vasile Alecsandri, despre necesitatea unei case
dedicate scriitorului în oraşul său natal. Vrem să realizăm
un muzeu Alecsandri în Bacău.
S.M.: Nu există?
I.D.: Singurul muzeu existent este la Mirceşti.
S.M.: Am fost şi eu victima acestei confuzii şi-l
legam pe Alecsandri de Mirceşti. El a fost poetul naţional
871

Solomon Marcus

al românilor şi a beneficiat peste măsură de toate onorurile,
dar la un moment dat am realizat valoarea poeziei lui
Bacovia şi faptul că el n-a avut parte nici pe departe de
atenţia şi de recunoaşterea de care a avut parte Alecsandri.
Probabil că la fel au reacţionat şi băcăuanii, care au avut
tentaţia ca mai întâi să-şi recupereze poetul emblematic al
oraşului, pe Bacovia. Desigur, faptul că Bacăul este locul
de naştere al poetului Vasile Alecsandri este un lucru de
mândrie pentru oraş şi acest fapt trebuie onorat aşa cum se
cuvine.
I.D.: Mulţumim, Domnule Profesor, şi sperăm ca
semnele de întrebare pe care le-aţi semnalat să fie pentru
noi tot atâtea nelinişti.

872

