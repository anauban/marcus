Prefaţă la
Grigore C. Moisil: un profesor
nu ca oricare altul.
Articole, interviuri şi cugetări selectate
de Viorica Moisil,
Editura Tehnică, Bucureşti, 1998.
Dezvoltarea matematicii româneşti a cunoscut mai
multe etape.
După generaţia eroică, a deschizătorilor de drumuri
(Spiru Haret, David Emmanuel), a urmat generaţia
iniţiatorilor cercetării matematice de cursă lungă
(Gh. Ţiţeica, Dimitrie Pompeiu), apoi cea de a treia
generaţie, a primilor mari şefi de şcoală în matematica
românească (T. Lalescu, S. Stoilow, Al. Myller etc.).
Cea de a patra generaţie, a profesorilor şi maeştrilor
acelora care azi au devenit, la rândul lor, maeştrii în
viaţă ai matematicii româneşti, include la loc de frunte
pe Gr. C. Moisil.
Personalitatea ştiinţifică şi umană a lui Gr.C. Moisil
a marcat profund evoluţia nu numai a matematicii, ci a
întregii culturi româneşti, iar actualitatea operei sale este
mereu subliniată. S‑au scris, pe această temă, numeroase
274

Dezmeticindu-ne

articole; dintre cele mai recente, recomandăm în mod
special pe cele inserate în numărul pe martie al revistei
„Academica”.
Opera ştiinţifică a lui Gr.C.Moisil a fost publicată
în trei volume la Editura Academiei Române şi în alte
câteva volume anterioare acestor trei, incluzând în
special unele studii de logică matematică.
În afară de opera matematică propriu‑zisă, Gr.C.
Moisil s‑a manifestat cu putere în domeniul filosofiei
ştiinţei şi în publicistică pe teme de educaţie ştiinţifică
şi cetăţenească, de politică editorială şi culturală, de
organizare a învăţământului şi cercetării etc. Unele dintre
cărţile sale, ca Îndoieli şi certitudini şi Ştiinţă şi umanism
includ exclusiv eseuri pe aceste teme. Aceste cărţi, ca şi
toate celelalte relative la Moisil, sunt de mult epuizate şi,
în ciuda actualităţii lor, nu au fost reeditate.
Este o împrejurare deosebit de fericită faptul că
doamna Viorica Moisil, văduva distinsului savant, s‑a
dăruit cu pasiune şi cu talent perpetuării amintirii lui
Gr.C. Moisil. Este exemplar modul în care domnia sa a
valorificat arhiva Moisil, prin publicarea unor cărţi care
ne aduc în atenţie viaţa personală şi de familie a lui Gr.C.
Moisil.
Cartea de faţă întregeşte acest tablou, aducând în
prim plan ipostaza de profesor şi educator a lui Moisil,
gândirea sa profundă şi subtilă.
275

Solomon Marcus

De la Moisil am învăţat că ştiinţa poate fi o mare
bucurie a spiritului şi o imensă sursă de emoţie, de
afectivitate şi de umor. Noile generaţii, uneori atât de
debusolate, trebuie să cunoască înţelepciunea acestui
mare spirit al veacului care se încheie.

276

