Cuvânt înainte la
Răzvan Satnoianu, Dumitru Săvulescu,
Mihaela Capră: Matematica pentru admiterea
în învățământul superior economic,
Editura Briell, Brașov, 1993, 200 p.
Pe unul dintre autorii lucrării de faţă, Răzvan
Satnoianu, l‑am cunoscut încă din vremea când era
student în anul I, în ciuda faptului că nu aparţinea
seriei de studenţi cu care lucram. Explicaţia se află în
faptul că, spre deosebire de cei mai mulţi colegi ai săi,
care se mulţumesc să înregistreze ceea ce se predă la
cursuri, Răzvan Satnoianu dorea să treacă dincolo de
programă, pentru a se apropia de frontiera care desparte
ceea ce se cunoaşte de ceea ce nu se ştie încă. Această
atitudine creatoare faţă de ştiinţă l‑a determinat să se
angajeze în dialog nu numai cu profesorii săi direcţi, ci
şi cu ceilalţi matematicieni, care l‑ar fi putut orienta în
hăţişul curiozităţii sale nepotolite. A fost pentru mine o
mare satisfacţie de a‑i asculta întrebările şi de a‑l orienta
în ştiinţa vie, din revistele de cercetare recente, într‑un
moment în care cei mai mulţi colegi ai săi abia dacă
parcurgeau notiţele de curs şi seminar.
231

Solomon Marcus

Rezultatul acestei hărnicii nu a întârziat să apară.
Răzvan Satnoianu are la activul său câteva articole
de cercetare, unul dintre ele publicat într‑o revistă
internaţională foarte exigentă. Lucrarea de faţă este şi
ea o expresie a acestei atitudini creatoare manifestate
de astă dată în direcţia didactică şi pedagogică. Răzvan
Satnoianu, asistent universitar la A.S.E., în calitate de
coautor şi coordonator şi colegii săi, experimentatul
profesor Dumitru Săvulescu, autor cunoscut al mai
multor lucrări apreciate de didactică şi pedagogie a
matematicii şi Mihaela Capră, asistentă universitară la
A.S.E. (până în 1990 o excelentă studentă la Facultatea
de Matematică din Bucureşti) au elaborat această lucrare
având ca principală preocupare apropierea de interesele
potenţialului candidat la admitere, de cunoştinţele şi de
viziunea acestuia. L‑au plasat pe potenţialul cititor exact
în condiţiile de concurs, fără însă a face compromisuri în
ceea ce priveşte rigoarea ştiinţifică.
Iată suficiente motive pentru a recomanda lucrarea
de faţă celor care se pregătesc pentru admiterea în
învăţământul superior, cu precădere în cel de profil
economic.

232

