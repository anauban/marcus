By three paths:
Professor Marcus in Interview
Questions by Laura Shintany
Meta: A Semiotic Websource
Solomon Marcus: University of Bucharest, Romania
Context: June 15th, 1999

Midafternoon, very warm day. In a sky lit alcove
along the underground passageway between the new and
old parts of the Valtionhotelli, Imatra, Finland. It is cool,
quiet and echoey there. Two chairs and a coffee table.
Content:
How did I arrive to semiotics?:
By three paths: The first path is mathematics,
which is in some sense a chapter of semiotics.
Peirce was basically a mathematician; then after
pure mathematics, I became interested and became
enough known in Mathematical and Computational
Linguistics. I took a slogan from Claude Lévi‑Strauss:
that linguistics is a pilot science and Jakobson said a
similar thing: Linguistics is the mathematics of social
303

Solomon Marcus

sciences. Linguistics has the power of expansion into
all other fields. By means of mathematical models, I
realized that this function of language is a universal
phenomenon, in science, in culture, in everyday life.
Then I observed that the pattern called language has
some restrictions, because it is always sequential.
But what evolved so many phenomena that are not
sequential but polydimensional? I needed something
more general than linguistics and that was semiotics.
This happened in the late sixties when the International
Association of Semiotics and the Journal Semiotica
emerged. Then the third thing, which was important in
the sixties: Thomas Sebeok came in Romania for the
first time. He was invited to an international meeting,
but at that time he was considered mainly as a linguist.
He was then the editor of a huge encyclopaedia, Current
Trends in Linguistics. He came to the city of Sinaia,
in the mountains. I was not invited there, but by my
own initiative I went there to meet Sebeok. It was a
difficult period, when Romanian scholars were not
allowed to travel in non‑communist countries; the only
possibility to contact Western scholars was to have
them in Romania. Professor Sebeok has understood
this situation and was ready to help us. He was then
known for linguistics and folkloristics. He invited me,
because I had published in international journals before,
304

Dezmeticindu-ne

to write an account about mathematical linguistics in
Europe. Then I said that there was something missing
in his encyclopaedia: linguistics as a pilot science;
he immediately invited me to write this. Then I gave
him both and he published there in Current Trends in
Linguistics. He invited me to collaborate to Semiotica;
later he organized the Linguistic Institute of America
in Tampa, Florida and invited me there. So, Professor
Sebeok created for me a practical context, to be involved
in problems that I was always interested in. I became
interested in semiotics, because it was for me the most
advantageous and natural way to bridge mathematics and
the other fields of research, be they scientific or artistic;
semiotics became for me a framework of unification of
human knowledge and human behaviour.
Your studies in Romania and Your Students:
I taught in the Department of Mathematics and
the Department of Languages of the University of
Bucharest; then I became very interested in poetics and
I published a book called Mathematical Poetics and I
gave classes on this. I attracted to my classes students
of different backgrounds: mathematics, linguistics and
humanities in general; the next step was semiotics. In the
Proceedings of the first International Semiotic Congress
in Milano, there are many Romanian contributions just
305

Solomon Marcus

on these topics, but no Romanian attended this congress.
Umberto Eco was the organizer and I met him for the
first time in Urbino in 1973, and he invited me to give
in Milano a plenary lecture on the semiotics of scientific
languages. The communist power did not like the idea of
us travelling abroad, because they wanted to “protect” us
of contamination with capitalist behaviour. I asked for
a passport, but it did not come. So the congress arrived
and I did not have a passport. Eco called me and asked
if I could not come, but we could not say that we could
not get a passport, we were obliged to say that we are
too ill or we are too busy. So I said that I am ill because
all these telephone calls were checked, by the political
power. Eco was very clever and he told the congress:
look, Solomon Marcus cannot come because he is
sick: his sickness is called passport. This was told to
me by Professor Sebeok. Then at all these congresses
of semiotics you can see in the Proceedings some
Romanian contributions. One of our strongest schools
was on the strategy of theatrical characters, semiotics of
theatre. There is an account of Romanian semiotics by a
Romanian semiotician, now in the United States, Sanda
Golopentia, in the volume The Semiotic Sphere, edited
by T. A. Sebeok.

306

Dezmeticindu-ne

Figures of Influence:
Some years ago, Professor Sebeok asked me about
my mentors, this was never published in Semiotica,
because some scholars felt uncomfortable listing this
kind of hierarchy. For me however, it is mathematics
which fascinated me and mediated my way to semiotics.
Mathematics is mediation, because it can describe both
finite things and infinite things, that is why mathematics
is my first teacher of semiotics.
On Theatre:
It is the model of human conflict, understood
via the different configurations of characters in the
successive scenes of a play. To each theatrical play we
associate a Boolean matrix where each line corresponds
to a character and each column is associated to a scene.
At the intersection of line i with column j we insert
the digit 1 if the character i appears in the scene j; we
insert the digit 0 in the contrary case. The process of
this binary information, by adequate parameters and by
computational means, leads to surprising conclusions;
for instance, a great part of the information we
apparently get from means of the theatrical dialogue,
can be obtained from the evolution of configurations of
characters along the successive scenes.

307

Solomon Marcus

On Poetry:
I was attracted by the topological nature of the
poetic semantics, as opposed to the discrete structure of
the scientific language; there is a semiotic crisis in the
former; due to the conflict between the continuous nature
of its semantics and the discrete nature of its syntax no
such conflict exists in scientific language. I tried to show
that a deep understanding of the differences between the
poetic and the scientific communication can be obtained
only within the framework of similarities between the
respective types of communication.
On Semiotics:
Semiotics is a common denominator of all
creative fields. It is, to some extent, unavoidable, so
most scientists and artists are doing semiotics in a very
implicit way, i.e., without being aware of it. Doing work
in semiotics but not using the title semiotics, this is sad
and it is damaging to our field. Many discover semiotics
very late when they are forty or fifty, for example the
Nobel laureate in chemistry Roald Hoffmann published
the book The Same and Not the Same at Columbia Univ.
Press, 1995, practically on the semiotics of chemistry.
He discovered this very late and this can be seen in
other fields such as in medicine too. Maybe this will
be the situation of semiotics for many years; it will
308

Dezmeticindu-ne

not change very quickly. Semiotics is for most people
a second step activity, in respect to hermeneutics,
for example. Semiotics still fails in its attempts to be
recognized as having an important impact on fields such
as cognitive science, artificial intelligence, cryptography,
computer science; but at the same time semiotics is
slow in its capacity to acquire and process the great
novelty of quantum physics, of computational biology,
of biological computation etcetera. Its metabolism
with other universal approaches such as system theory
should be improved. Partly, the weak social impact of
semiotics is due to its failure to become a recognized
academic discipline; you can count on your fingers the
exceptions. It is here to some extent, a vicious circle,
because we may ask whether the failure in becoming
an academic discipline is not just due to its weak social
impact. What brings mathematics near to semiotics? Like
semiotics, mathematics is mediation, for instance between
thermodynamics, physics and information science, what
made possible the transfer of entropy from physics to
information sciences? This transfer became possible
because the thermodynamic entropy has a mathematical
expression, a logarithmic one, which permits to separate
the pattern of a phenomenon from its substantial,
particular part. This pattern acquires, due to mathematics,
a general form, that can be immediately adapted by the
309

Solomon Marcus

fields of completely different nature. To give another
example, the formal modelling of textual structures made
possible the transfer of a text structure in fields different
from languages; the world itself may become a text.
Pedagogical Concerns?:
Teaching is a prisoner of the late nineteenth century
and early twentieth century, where all disciplines were
separated. Generations after generations were educated
in the spirit of an opposition between humanities and the
sciences, for example. Semiotics invites on the contrary,
to transgress such false borders, to go across disciplines
and to reach segmentations of knowledge other than
the disciplinary ones. This aim of semiotics comes in
conflict with the existing system of education and with
the existing bureaucracy of culture. Semiotics means
transdisciplinarity, while the predominant mentality is
still disciplinary. This is the reason why, when asked
by the Journal Degrées to give them an article about
semiotic education, I chose the title Too Early for
Teaching Semiotics, but obviously, we should take it cum
grano salis.
Implicit Versus Explicit Semiotics:
Many people that we now consider brilliant
semioticians came to practice semiotic thinking on their
310

Dezmeticindu-ne

own. Often, they invent their own jargon such as von
Uexküll. In this way there are many semioticians that are
what could be considered implicit semioticians and their
work is of great worth, while we are already familiar
with the great figures of explicit semiotics. Actually most
of semiotics is of this implicit type. Professor Sebeok has
referred to this as the Monsieur Jourdain factor; his first
example in this respect was Harley C. Shands, who was
stimulated by a lecture of Professor Sebeok, to realize
that he was doing semiotics for a long time, without
being aware of this. Harley Shands became one of the
main figures in medical semiotics.
What to Do With Semiotics?:
In the nineteen fifties and sixties, semiotics had
a delicate position in Eastern Europe, due to its strong
connections with some fields of humanities and social
sciences, suspected to be ideologically in opposition with
the communist ideology. A shift in view was occurring
towards information sciences and computer technology;
with these new connections the communist power felt
that it would be good to stand behind this idea that there
is a kind of technological revolution. Semiotics coupled
itself with this information sciences and it continues to
flourish. In terms of employment, in any profession
you need to have a capacity to convince your audience
311

Solomon Marcus

that what you are doing is valuable and you need to
be appreciated and this might he accomplished by
insisting on creating a sense of fascination. I think that
employment problems for students is about the same, for
example, architecture students not all were getting jobs,
some became successful and others not. You must make
it your own, you must show that it is your own life.
At the University of Toronto:
I was invited there, for the field of mathematical and
computational linguistics by Professor Barron Brainerd,
but I was also to teach in the field of mathematical
analysis. Before, Brainerd came to Romania spending
a long time with me in relation with our common
interest in mathematical linguistics and mathematical
theatrology. I can say that I was a pioneer in mathematical
computational linguistics. In Toronto, I had an opportunity
to interact with the great geometer Coxeter, with Anatol
Rapoport, a famous specialist who applied game theory
to social sciences, with Lubomir Doležel, an excellent
specialist in the study of narrative structures, with the
eminent psychologist Berlyne, with the specialist in circus
semiotics Paul Bouissac. But I got invitations to many other
Canadian universities, from Montréal to Vancouver and from
London‑Ontario to Edmonton. I travelled extensively to
interact with people and ideas, but for me this came very late.
312

Dezmeticindu-ne

What is Semiotics?:
There are things difficult to be defined; among
them: poetry, philosophy, mathematics and semiotics. It
may be interesting to observe that each of these fields
is of an involutive nature: it makes sense to speak of
the poetry of poetry, the philosophy of philosophy,
the mathematics of metamathematics, the semiotics of
semiotics. By contrast, it makes no sense, excluding a
possible metaphoric use, to speak of physics of physics,
chemistry of chemistry or biology of biology.
In February of 1994, I was visiting a hospital in
Dresden, and one of the medical doctors asked me about
the reason of my presence in Dresden. When I said that
I came to an International Conference in Semiotics, the
medical doctor asked me, “what is semiotics?” I tried
unsuccessfully to explain it to him, but then I realized
that I cannot do it in a short statement. Then another
medical doctor told his colleague: semiotics is what
Umberto Eco is doing. Things stopped here. Umberto
Eco in his turn found the way to avoid the trap and, in
one of his writings defined semiotics by a joke: semiotics
is a way to explain why a lie is possible, in conformity
with his way to look at sign as a presence accounting
for an absence. For me, semiotics, having as its object
the sign processes of any type, is genuinely related
to mediation and we can convince our partners of the
313

Solomon Marcus

interest of semiotics by explaining that any human
access to cognition is a mediated one.
What Are Your Current Interests in Semiotics?:
For several years I have been involved in the study
of sign processes that take place in the new fields born
in the twentieth century: quantum physics, relativity,
computational biology, biological computation, DNA
computing, quantum computation, etcetera. There is a
semiotic crisis pointed out by Niels Bohr in respect to
language; he observed that human language, born and
developed in the context of the macroscopic universe,
is not adequate to cope with the phenomena beyond
the macroscopic universe. What Niels Bohr said about
human language can be extended to human semiosis in
general. There is an increasing crisis of representation,
visible not only in relation to the crisis of the subject/
object distinction, but also with the iteration of the
representation operator; this iteration marginalizes the
initial object, placing it more and more in a shadow. The
representation of the object becomes more and more a
representation of another representation. This is my main
semiotic concern today.

314

