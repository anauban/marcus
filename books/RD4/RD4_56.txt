„SOL”
București, 14 decembrie 2009,
Banca Națională a României,
la lansarea cărții SOL
de George Virgil Stoenescu
George Virgil Stoenescu s‑a dedicat rondelului.
Rondelul s‑a născut din muzică și are un caracter
pronunțat ludic. Este un joc care ni se propune și puteți
vedea că în deschiderea recentului său volum SOL avem
un citat ludic din Iisus Hristos. Dar, propunându‑ne un
joc, ne provoacă să preluăm jocul și să‑l ducem mai
departe. Este ceea ce am încercat să fac.
Niciodată titlul unei cărți a lui George Virgil
Stoenescu nu a fost atât de provocator ca acum. De
obicei, titlurile cărților sale erau sintagme. Acum ni se
propune o silabă, dacă ne referim la ceea ce reprezintă
titlul în limba română, sol. O silabă care de la început
m‑a emoționat, pentru că, întâmplător, este și începutul
prenumelui meu. Evident că nu acest lucru este
semnificativ. A mizat pe o altă ipostază a acestei silabe,
faptul că această silabă în limba latină înseamnă soare.
Pe acest pariu a mers în volumul de față.
540

Dezmeticindu-ne

Mă întorc la rondel. Rondelul a mizat de‑a
lungul istoriei sale pe capacitatea pe care o au unele
cuvinte de a fi folosite în mod repetat, de fiecare dată
cu altă semnificație. Am să încerc să fac acest lucru
cu titlul acestei cărți. Să vedem dacă nu cumva, în
afară de semnificația soare, nu descifrăm în sol și alte
semnificații.
Am să încep cu următoarea: sol nu este numai o
silabă, este și un cuvânt în limba română, atunci când,
de pildă, ne propunem să ne ridicăm de la sol și să
ajungem în văzduh. În general, pariul oricărui scriitor
este tocmai acesta. Să se ridice de la sol, adică de la
universul contingent, cotidian, în văzduh. Adică în
ficțiune, imaginație, libertate. Este pariul pe care l‑a
propus și Charles Baudelaire în celebrul său Albatrosul,
în care ideea de bază era că un creator se simte bine
în văzduhul creației sale și, cum este readus la sol, în
universul contingent, devine stângaci și foarte redus în
posibilitățile sale.
Autorul acestei cărți are două ipostaze. Una de
economist, finanțist, profesor la Academia de Științe
Economice, importantă personalitate la Banca Națională
a României. Această personalitate ne grăbim să o situăm
la sol, cel puțin în mentalitatea curentă. E situată în
universul strict material. Cealaltă ipostază, pe care
mizează în acest volum, este ipostaza îndrăgostitului
541

Solomon Marcus

de rondeluri. Volumul este dedicat unei personalități
remarcabile, aici prezente, Guvernatorul Băncii
Naționale. Se folosește acolo o metaforă splendidă,
vorbindu‑se despre soarele din inima sa de leu. Deci
mereu ipostaza de soare. Imediat ne putem întreba
dacă metafora aceasta nu are și o semnificație mai
profundă, dacă finanțele, pe care probabil o mare parte
din cei aflați în această sală le practică, se reduc în
universul contingent. Grăbiți, putem să dăm un răspuns
afirmativ, dar nu este așa. Fără să fiu finanțist, am tatonat
acest domeniu ca matematician, așa cum, în virtutea
universalității gândirii matematice, am tatonat multe
alte lucruri. Pot să vă spun că mi‑am dat bine seama că
finanțele au și ele văzduhul lor. S‑a întâmplat să recenzez
o carte privind istoria gândirii economice în secolul
al XX‑lea și mi‑am dat seama că marile performanțe
în acest domeniu s‑au putut obține numai în măsura în
care s‑a produs și o ieșire în ficțiune. Nu știu dacă Banca
Națională a continuat editarea discursurilor laureaților
Nobel în economie. Nu știu dacă s‑a ajuns la discursul
lui Gérard Debreu, premiul Nobel în științe economice
în 1983. El arată că pentru a înțelege bine mecanismele
unei piețe cu n participanți este nevoie în anumite cazuri
să vedem ce se întâmplă atunci când n crește spre infinit.
Cu alte cuvinte, să studiem comportamentul asimptotic
al acestei piețe când numărul participanților crește
542

Dezmeticindu-ne

indefinit. Evident, aceasta este o excursie în ficțiunea de
care avem nevoie în creația științifică și în cea artistică,
pentru că numai prin ea putem aprofunda lucrurile,
revenind apoi în contingent cu folos. Gérard Debreu a
primit premiul Nobel tocmai pentru astfel de rezultate.
El este celebru pentru că a folosit ceea ce numim în
matematică analiza matematică non‑standard. Alt
laureat al premiului Nobel, un matematician pur, care a
produs un rezultat din propria sa curiozitate matematică,
privind echilibrul Nash, este John Nash. Tot o evadare
în ficțiune, nu este un rezultat strict terestru. Cred că și
această ipostază a lui sol din titlul cărții lui George Virgil
Stoenescu este foarte interesantă, o provocare.
Când aflăm că e vorba de o personalitate de la
BNR care predă la ASE, suntem tentați să o plasăm
în domeniul strict al utilitarului. Iată că această
personalitate a simțit nevoia să se desprindă de sol. Am
încercat să vă sugerez prin aceasta că finanțele au acces
la poezia lor, nu în sensul că au acces la poezia la care
ne referim când vorbim de literatură. Au poezia lor
interioară. Dacă autorul ne‑a propus un joc, să‑l ducem
mai departe. Sol indică imediat cheia sol din muzică.
Cum să nu ne gândim la ea, când rondelul tocmai din
muzică s‑a desprins. Este o notă muzicală și cheia sol
este cea mai cunoscută, am învățat‑o la școală. Este,
în esență, un fel de indicație de regie. Cum să începi
543

Solomon Marcus

și cum să continui. Am mers mai departe pe acest
aspect ludic al titlului cărții dvs. (către George Virgil
Stoenescu) și am să vă împărtășesc, sper, o noutate.
Am încercat să compar schema prozodică pe care merg
rondelurile dvs. cu schema prozodică pe care au mers
rondelurile în alte perioade istorice. La dvs. schema
rondelului este pe 13 versuri și este, ca tip de rimă: a
b a b prima strofă, a doua la fel, iar a treia strofă a b a
b a. Observați următorul lucru. Dacă ai un șir doar de
două elemente, a b în cazul de față, prima întrebare pe
care ți‑o pui este dacă el e periodic sau nu. Care e gradul
lui de regularitate? Regularitatea în această schemă a
dvs. o strică ultimul vers. Dacă nu era ultimul vers, al
13‑lea, aveam o periodicitate clară în care perioada era
a b. Periodicitatea este la îndemâna oricui, dar ați intrat
într‑un fenomen care extinde periodicitatea și care
se numește cvasi‑periodicitate. Această idee a apărut
concomitent din două surse complet diferite. Una dintre
ele este muzica, iar cealaltă genetica moleculară. Acolo
nu e vorba doar de două elemente, e vorba de cele 4
tipuri de baze nucleotide. Și acolo, modul în care se
structurează ADN‑ul nostru conduce la noțiunea de
cvasi‑periodicitate. Am mers înapoi să văd cum se
prezenta schema prozodică la rondelul acela care avea 15
versuri. Acela nu mai e nici periodic, nici cvasi‑periodic.
Deci, prin modificarea de care v‑ați prevalat ați introdus
544

Dezmeticindu-ne

o regularitate mai fină decât periodicitatea, mai greu
vizibilă cu ochiul liber. Nu se încheie aici ambiguitatea
titlului acestei cărți.
Sol trimite și la o mare figură din Biblie, regele
Solomon, fiul regelui David. Toată lumea știe că regele
Solomon este un simbol al înțelepciunii. Înțelepciunea
din acest volum este prezentă de la prima până la
ultima pagină. Am să aleg, pentru fiecare literă, un
cuvânt dintr‑un rondel în care în primul vers apare
cuvântul respectiv. Adevărul, bucuria, când (evocă
un timp de altă dată), dincolo, Dumnezeu (nevoia de a
trece dincolo de nivelul terestru), era, fără (interdicția),
iubeam, încercam (libertatea de a încerca chiar dacă nu
vei reuși), jocul, lumea, mă (reflexivul care revine cu o
insistență extraordinară), moartea, nimeni, nimic, neantul
(nevoia de negare), ochiul (cel mai frecvent substantiv
în poezia lui Eminescu), poate (îndoiala), resemnarea,
singurătatea, trebuia, țipătul, unde, visul, zborul. După
impresia mea, substantivul cel mai frecvent, cel puțin
în acest volum este, cum era de așteptat, visul, nevoia
de visare. Continuați acest joc, pentru că continuându‑l,
răspundeți la provocarea lui George Virgil Stoenescu!

545

