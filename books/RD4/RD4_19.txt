De la informatică și teoria informației la
teoria algoritmică a informației
Academica, anul IV, nr. 1(37) noiembrie 1993, p. 20
Secolul nostru a adus în prim plan paradigma
informaţiei; aceasta a schimbat nu numai configuraţia
disciplinelor de cercetare, ci şi structura învăţământului,
a profesiilor, viaţa socială în ansamblul ei.
Ştiinţele şi ingineriile informaţiei s‑au constituit
cu precădere spre mijlocul secolului nostru, iar dintre
ele menționăm informatica (în terminologia engleză,
Computer Science) şi teoria informaţiei (iniţiată de
Claude Shannon). În ciuda asemănării terminologice,
aceste două discipline se deosebesc atât prin obiectul,
cât şi prin istoria lor. Informatica se bazează pe logica
matematică modernă (noţiunile de funcţie recursivă,
de algoritm, de maşină Turing, de calculabilitate,
decidabilitate, de complexitate algoritmică etc.), în
timp ce teoria informaţiei se sprijină pe conceptul
de probabilitate şi pe un transfer foarte îndrăzneţ al
conceptului de entropie din domeniul energiei în cel
al informaţiei. În aproape 50 de ani de dezvoltare,
informatica şi teoria informaţiei au urmat căi diferite
233

Solomon Marcus

şi, până nu demult, au interferat relativ puţin. Această
situaţie este reflectată şi de istoria culturii româneşti.
Dacă în domeniul preliminariilor informaticii se disting,
în prima jumătate a secolului nostru, puţine nume de
rezonanţă (printre acestea situându‑se Gabriel Sudan
şi Grigore C. Moisil), conceptele de energie şi de
informaţie, cu tot ceea ce implică pe una dintre ele ca
metaforă a celeilalte, ocupă aproape un secol de istorie
a culturii româneşti, istorie pe care o vom schiţa mai jos.
Constantin Rădulescu Motru a elaborat doctrina
personalismului energetic, în cadrul căreia a abordat
problematica personalităţii umane, prevalându‑se de legea
conservării şi transformării energiei. Faţă de criticile
care s‑au adus acestei doctrine, trebuie să observăm
că transferurile din domeniul fizic în cel uman pot fi
considerate abuzive în raport cu metoda tradiţională a
observaţiei, experimentului şi descrierii, dar ele capătă
o anumită justificare dacă sunt plasate în cadrul unui
proces de modelare şi de metaforizare, unde diferenţa
de natură între obiect şi modelul său este nu numai
inevitabilă, necesară, ci şi foarte semnificativă. În această
ordine de idei, este deosebit de relevantă opera lui N.
Georgescu‑Roegen, care articulează legea entropiei cu
procesul economic (o expresie a impactului puternic al
acestei opere este numărul recent pe care revista „Libertos
Matematica” l‑a dedicat lui Georgescu‑Roegen).
234

Dezmeticindu-ne

În 1966, Octav Onicescu introduce noţiunea
de energie informaţională (ca un caz particular al
celei de corelaţie informaţională, şi obţine un analog
informaţional al principiului al doilea al termodinamicii.
Gh. Zapan propune un instrument de evaluare a gradului
de organizare a unor sisteme evolutive calitative, reuşind
astfel să identifice aspecte care scapă entropiei lui
Shannon. Aceasta din urmă evaluează numai gradul de
nedeterminare, nu şi cantitatea reală de informaţie.
Mişcarea de idei dinspre fizică spre economie este
vizibilă încă din secolul trecut, iar metafora entropică
a căpătat o utilizare tot mai mare. În această ordine de
idei trebuie plasată şi triada energie‑entropie‑informaţie.
Filosofia ştiinţei nu putea rămâne indiferentă la acest
proces, mai cu seamă că dezvoltarea mecanicii cuantice
şi a biologiei a produs mutaţii surprinzătoare. O analiză
pătrunzătoare a acestor metamorfoze este întreprinsă
în opera lui Stephane Lupasco, de explicare a logicii
cuantice şi sistemice. Conceptul de energie ocupă aici un
loc central şi are o semnificaţie foarte generală.
Față de disparitatea tradiţională manifestată de
informatică şi teoria informaţiei, prima bazată pe ideea
de algoritm, a doua sprijinindu‑se pe conceptul de
probabilitate, primele semne ale unei noi orientări,
de convergenţă şi colaborare a algoritmicului şi
aleatorului, apar încă în urmă cu câteva decenii; ele
235

Solomon Marcus

capătă o formă mai sistematică în deceniul al şaptelea al
secolului nostru, în primul rând prin cercetările lui A.N.
Kolmogorov şi P. Martin Löf. Problema care prilejuieşte
această colaborare este aceea a modelării matematice
a ideii de şir aleator; strategia adoptată aici distinge
net cazul şirurilor finite de cel al şirurilor infinite.
Această problematică este preluată şi de noua generaţie
de matematicieni şi informaticieni români. După ce
rezultatul clasic al lui Gabriel Sudan (rezultat datând
din deceniul al treilea, dar neindentificat şi nereceptat la
timp), privind primul exemplu de funcţie recursivă care
nu este primitiv recursivă, este continuat şi dezvoltat
într‑un şir de articole, declanşat de o observaţie a lui Gr.
C. Moisil (a se vedea istoria acestor cercetări în articolul
de sinteză C. Calude, S. Marcus, „Sudan’s recursive but
not primitive recursive function: a retrospective look”,
Analele Univ. Bucureşti, seria Matematică‑Informatică
2, 1989, p. 25‑30), C. Calude şi I. Chiţescu publică,
începând cu anul 1981, un ciclu de articole relative la
şirurile aleatoare în sensul lui Kolmogorov şi/sau în cel
al lui Martin‑Löf. C. Calude dezvoltă aspectul topologic
al problemei, antrenând în această acţiune şi câţiva
matematicieni mai tineri, ca M. Zimand, C. Câmpeanu şi
alţii.
Începând cu anul 1987, devine clară conturarea
unui nou domeniu de cercetare: teoria algoritmică a
236

Dezmeticindu-ne

informaţiei. În acest an apare cartea lui G. I. Chaitin
Algorithmic Information Theory (Cambridge University
Press). Desigur, cartea era pregătită de o serie de articole,
extinsă pe o perioadă de aproape 25 de ani (primul
articol al lui Chaitin datează din 1965). Cartea din 1987
cunoaşte trei ediţii, cea mai recentă, revăzută, datând din
1990. Acelaşi autor publică ulterior cartea Information,
Randomness and Incompleteness (ediţia a doua în 1990
la World Scientific), iar în 1992 apare (tot la World
Scientific) cartea lui Chaitin Information‑Theoretic
Incompleteness. Chaitin aduce în atenţie aspecte noi
şi surprinzătoare ale rezultatelor lui Turing şi Gödel;
prin aceasta, ca şi prin multe altele, teoria algoritmică a
informaţiei, articulând ideea de informaţie cu aceea de
construcţie algoritmică, dezvoltă semnificaţii filosofice
şi culturale de o deosebită însemnătate. Se produce
o accentuare a calitativului în dauna cantitativului, a
globalului în dauna localului. În cele ce urmează şi în
câteva numere următoare din Academica prezentăm
câteva articole pe această temă.

237

