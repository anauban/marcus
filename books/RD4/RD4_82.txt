Simpozionul național de estetică,
Centrul de cultură „George Apostu”
Bacău, noiembrie, 2011
În a doua parte a anilor ’40 ai secolului trecut
se produce marea revoluție în cunoaștere, marcată
de trecerea de la predominanța paradigmei energiei
la predominanța paradigmei informației. Puțin mai
devreme apare teoria jocurilor de strategie, a lui Von
Neumann și Morgenstern. Nu mai vorbesc de faptul că
pe vremea aceea nu erau încă în prim‑plan ceea ce s‑au
numit mai târziu marile probleme globale ale omenirii:
problema energiei, problema hranei, cea ecologică. Toate
aceste lucruri și altele au schimbat complet modul de a
înțelege jocul. În afară de jocul care se bazează pe reguli,
cum sunt cele mai multe jocuri care au apărut în istorie și
au creat o întreagă tradiție, există jocuri în care nu intrăm
de bună voie și ale căror reguli nu ne sunt cunoscute. Noi
decidem în ce moment vrem să jucăm o partidă de șah
sau de tenis. Dar există multe jocuri în care ne trezim
implicați, regulile sunt impuse de alții, noi nici nu știm
de multe ori care sunt acestea. Începutul îl reprezintă
în primul rând jocul biologic, pentru că în ființa umană
857

Solomon Marcus

există multă natură. Pe urmă se adaugă, treptat, și
cultură.
Natura are și ea jocurile ei și descifrarea acestora,
ale naturii, biologice sau de alt fel, este o activitate foarte
complicată. Sunt apoi jocurile creației, jocul artei, al
științei. Acestea nu se desfășoară după reguli prestabilite,
ca tenisul sau ca fotbalul. Noi, cei care încercăm să
le practicăm, o facem în numele libertății de care
beneficiază ființa umană. Când această libertate nu există
sau este diminuată, jocul creației nu se mai desfășoară
cum trebuie. Nici nu știm exact dacă, de pildă, în jocul
creației artistice sau științifice există reguli și, dacă
există, care anume sunt ele. Dacă sunt reguli care se
constituie în procesul de creație și nu reguli prestabilite.
Aceasta e una dintre marile probleme ale educației
în lume. Faptul că modul în care se desfășoară educația
nu mizează pe joc ca pe o paradigmă fundamentală.
Că nu se înțelege faptul că învățarea adevărată, cultura
adevărată nu există fără joc. Ele implică o componentă
ludică obligatorie, care este de fapt motorul și răsplata
celui care învață și a celui care caută să facă ceva nou.
Vă amintesc aici de Steve Jobs, unul din co‑fondatorii lui
Apple, care a spus câteva lucruri extraordinare, pe care,
de când le‑am auzit, în urmă cu vreo trei săptămâni, le
repet mereu, oriunde mă aflu. Le spunea tinerilor: Fiți
flămânzi! (de cultură), fiți nebunatici! (în imaginație, și
858

Dezmeticindu-ne

aici intră jocul), Nu lăsați pe alții să vă trăiască viața!
(nu te lăsa manipulat de jocurile impuse de alții sau
de natură) Trăiți‑vă propria voastră viață! Eu n‑am
mai găsit, spuse în doar câteva cuvinte, atâtea lucruri
esențiale.
Esențial în educație este să educăm foamea. Trebuie
să educi, să formezi la oameni foamea de cultură. Restul
vine de la sine. Dacă noi impunem elevilor lecturi care
nu sunt urmare a foamei de acele lecturi, eșuăm. Așa
cum cineva care nu mănâncă în măsura în care i se face
foame, ci mănâncă dezordonat, se îmbolnăvește.
Discurs la acordarea diplomei de excelență și a
premiului Centrului de cultură „George Apostu”
Acordarea diplomei de excelență și premiul
Centrului de cultură „George Apostu” domnului
Solomon Marcus pentru originalitatea științifică, rigoarea
metodologică și erudiția generoasă întru revelarea
unității structurale și de sens a cunoașterii umane, pentru
umanismul demersului cultural de sorginte renascentistă,
pentru situarea legitimă în elita făuritorilor de paradigme
universale.
S.M.: Cred că cel mai potrivit ar fi să vă povestesc
pe scurt drumul meu spre paradigmele universale.
Acest drum a început aici la Bacău, în anii ’20‑’30.
Despre partea din anii ’20 am însă slabe amintiri. Eram
859

Solomon Marcus

pasionat de joc și, dacă încerc să găsesc în copilăria
mea rădăcinile pasiunilor mele de mai târziu, cred
că principala atitudine care m‑a stăpânit și care m‑a
pregătit pentru evoluția mea ulterioară se exprimă prin
cuvântul „dincolo”. Această curiozitate imensă pentru a
descoperi ce se află dincolo de spațiul în care mă aflam.
Spațiul dominant al copilăriei mele în anii ’30 era strada
Iernii, derizorie ca lungime, ca mărime, din spatele
străzii Centrale pe care se află acum Teatrul Bacovia. Pe
vremea aceea, era cinematograful Mărăști, principalul
cinematograf al orașului Bacău. M‑a stăpânit curiozitatea
de a descoperi ce se află dincolo de strada Iernii. Când
am ieșit pentru prima oară în strada Centrală, paralelă
cu strada Iernii, a fost o revelație. Marea revelație a
fost însă în ziua în care m‑am aventurat pe strada care
duce spre Fabrica de hârtie Letea. Ajunsesem într‑o zi
cam până prin zona unde e acum Universitatea „Vasile
Alecsandri”. Părinții mei, considerându‑mă rătăcit, au
trimis mai multe persoane să mă caute.
Mereu am fost stăpânit de acest dincolo, care
lingvistic se exprimă prin obiceiul nostru devenit rutină,
de a pune la un moment dat virgulă și trei puncte de
suspensie. Aceste trei puncte de suspensie pe care le‑am
găsit inevitabile atunci când trebuia să scriu numerele.
În mod inevitabil, la un moment dat oboseam, dar îmi
dădeam seama că șirul nu s‑a terminat. Puneam virgulă,
860

Dezmeticindu-ne

trei puncte de suspensie și treceam la o altă activitate.
Curiozitatea imensă ascunsă după acest paravan al lui
dincolo, al celor trei puncte de suspensie, m‑a urmărit
toată viața.
În a doua jumătate a anilor ’30, am descoperit
poezia și teatrul. Această fascinație a posibilității unei
persoane de a trăi într‑o altă persoană mi s‑a părut
o revelație, o libertate care ne deschide nenumărate
orizonturi. Nu mai sunt obligat să mă trăiesc numai
pe mine. Pot să intru în pielea altor personaje și să am
libertatea de a alege aceste personaje în care să mă
instalez. În zonă era Ateneul Cultural, cam peste drum
de ceea ce era atunci Tribunalul, care acum cred că
este Consiliul Local. Prima piesă de teatru pe care o
văzusem în acea perioadă era un spectacol dat de o
trupă de amatori: Omul care a văzut moartea de Victor
Eftimiu. Am fost pur și simplu vrăjit. Nu conta talentul
actorilor și importanța piesei respective, despre care
astăzi nu prea se mai vorbește. Simplul fapt că vedeam
că pe o scenă se instalează un fel de viață artificială mi
se părea ceva extraordinar. La scurt timp, am descoperit
O scrisoare pierdută a lui Caragiale, din care nu
înțelegeam mare lucru la vârsta de 12 ani. Reținusem
pe dinafară întregi șiruri de fraze ale lui Trahanache,
ale lui Tipătescu. Simțeam în acele replici ceva foarte
atrăgător și mă urcam pe un scaun să le declam, chiar
861

Solomon Marcus

dacă nu le înțelegeam foarte bine. Am descoperit la
13‑14 ani marea poezie. Trebuie să vă spun, nu știu dacă
cu mâhnire sau cu bucurie, toate aceste mari descoperiri
s‑au produs în afara perimetrului școlii. Modul în care
găseam matematica și literatura în manualele școlare
nu mi‑a aprins imaginația. Am descoperit lucrurile
acestea alternativ și m‑au marcat foarte mult. Faptul
că descopeream că dincolo de viața pe care o trăiam
cotidian, într‑un univers contingent, există această
capacitate a noastră de a descoperi lumi de ficțiune, ni
se oferă un spectacol pentru care merită să trăiești. Acest
sentiment a rămas nealterat, departe de toate dificultățile
prin care am trecut. Capacitatea de a ne construi
universuri de ficțiune a hrănit întreaga mea activitate
de‑a lungul vieții și mă menține și acum cu o curiozitate
de a afla mereu lucruri noi.
Terminând școala, intenția mea era fie să merg
la Filozofie, fie la Facultatea de Litere. Fratele meu
mă sfătuia să merg la Arhitectură, pentru că acolo se
îmbină arta cu știința. În vara fierbinte a anului 1944,
în care războiul se apropia de orașul meu natal, am
descoperit o carte despre lucruri de geometrie care nu se
învățau nici atunci și nu se învață nici acum la școală,
geometriile neeuclidiene. Dintr‑o dată am realizat că
matematica îmi poate da satisfacții similare cu cele pe
care mi le dă poezia. Sentimentul infinitului, al lumilor
862

Dezmeticindu-ne

de ficțiune și al unei alte logici decât aceea a vieții
curente. Atunci, datorită faptului că în toamna anului
1944 am fost clasificat pe primul loc la un Bacalaureat
la care se prezentaseră 156 de candidați, aveam dreptul
și libertatea de a mă duce la orice facultate fără niciun
examen de admitere. După o vizită prin diverse holuri de
facultăți, fascinat de afișele pe care le găsisem la Secția
de Matematică a Facultății de Științe a Universității din
București, m‑am dus acolo, nefiind sigur. Foarte mulți
studenți în acea perioadă mergeau la o facultate, iar în
anul următor o abandonau și se duceau la alta. Atunci
s‑a produs una din marile șanse ale vieții mele. Să am
contact cu personalități care parcă erau anume pregătite
să‑mi meargă la suflet. Nu pot să uit în această privință
efectul de vrajă pe care l‑a avut asupra mea prof. Miron
Nicolescu, un adevărat părinte spiritual, de care nu
m‑am despărțit niciodată. Am descoperit că într‑adevăr
există o știință, o disciplină care studiază infinitul,
procesele cu o infinitate de etape, care construiește lumi
de ficțiune extraordinare. Această știință se numește
matematica. Am devenit un pasionat cercetător în
domeniul Analizei matematice, disciplina care studiază
procesele cu o infinitate de etape, mi‑am construit în
decurs de 10 ani un palmares pe care de cele mai multe
ori oamenii îl realizează într‑o perioadă mai lungă.
Nimeni nu mă mai putea contesta ca matematician
863

Solomon Marcus

profesionist în matematica pură. Pe la mijlocul anilor
’50, îmi parvin din diverse direcții semne ale apariției
unei preocupări noi, care construiește o punte între
matematică și lingvistică. Din America, prin gramaticile
generative ale lui Chomsky, de la Moscova, unde câțiva
importanți matematicieni, printre care Kolmogorov,
se aventurează în abordarea matematică a limbajului,
din Europa Occidentală, etc. Simt dintr‑o dată că
această veste n‑am voie să n‑o introduc imediat în
preocupările mele și încep cu un entuziasm extraordinar
să îmi fac întâi o cultură lingvistică. Iau legătura cu
cei mai importanți lingviști ai momentului: cu cel
mai recent discipol al lui Rosetti, Emanuel Vasiliu,
și cu alți lingviști, printre care cea care avea să devină
soția mea, Paula Diaconescu, de la catedra condusă de
Alexandru Rosetti. Încep să studiez lingvistica după cele
mai moderne cărți ale momentului. În câțiva ani, îmi
construiesc o cultură lingvistică, studiez cu atenție toate
lucrările care veneau din est și din vest, intru în ele cu
pasiune și încă o dată simt cât de mult mă ajută modul
matematic de gândire. Nu cunoștințele de matematică,
nu anumite teoreme sau tehnici, ci modul ei de gândire.
Reușesc să elaborez cartea care avea să devină prima
monografie de lingvistică matematică din lume, cerută
imediat la traducere la New York, la Paris, la Moscova,
la Praga și începe aventura mea în domeniul lingvisticii
864

Dezmeticindu-ne

matematice. Atunci am simțit un lucru pe care în școală
nu‑l realizam. Că, de fapt, unul din motivele centrale
care m‑au atras spre poezie a fost interesul meu pentru
problemele de limbaj. Mi‑am explicat de ce eu am
fost atras în primul rând de acei poeți care produceau
modificări esențiale în capacitățile limbajului: Eminescu
a schimbat limba română, la fel și Arghezi. Mi‑am
dat seama de ce simțeam o atracție mult mai puternică
pentru Arghezi decât pentru Blaga. Pentru că Arghezi
era un poet care miza mai mult pe limbaj decât Blaga.
Mi‑am explicat de ce mai târziu am fost atât de pasionat
de Nichita Stănescu, un alt poet care a schimbat limba
română.
Simțeam o nevoie extraordinară să construiesc
o punte între pasiunea mea pentru poezie și cea pentru
matematică. Unul dintre cei care m‑au ajutat foarte mult
a fost astronomul Constantin Drâmbă de la Observatorul
Astronomic al Universității din București. La începutul
anilor ’60, m‑a chemat la el și mi‑a pus pe masă o serie
întreagă de documente, lucrări ale unui român cu nume
francez, Pius Servien, care nu era altcineva decât fiul
marelui astronom român Șerban Coculescu. Drâmbă
era prieten cu Pius Servien, pentru că era și discipol
în astronomie al lui Șerban Coculescu. Mi‑a dat cu
împrumut acele lucrări și am trăit o altă fascinație. Am
simțit că ceea ce era acolo era exact ceea ce căutam eu să
865

Solomon Marcus

fac și iată că cineva făcuse.
La scurt timp după aceasta, se întâmplă un alt
fenomen fericit. Era perioada când începuseră să
vină în România ziarele franceze. Le găseai în unele
locuri speciale. Îmi cade sub ochi un număr din ziarul
Le Monde de pe la sfârșitul anilor ’50, care anunță
moartea Matilei Costiescu Ghyka, nepot al ultimului
domnitor al Moldovei. Citesc acel articol și rămân
înmărmurit. O altă descoperire extraordinară, caut
imediat să găsesc lucrările sale. Matila Ghyka se ocupase
exact de construirea unei punți între matematică și lumea
artei. A publicat un ciclu întreg de cărți. Apoi mi‑am
dat seama că Matila Ghyka devenise un clasic în acest
domeniu și a căpătat o faimă mondială înainte să fi
căpătat o faimă mondială Eliade sau Cioran, despre care
se vorbește foarte mult. Într‑o etapă anterioară acestora,
Matila Ghyka a fost, cred, primul român care a reușit
să‑și construiască o faimă mondială. Rusia comunistă i‑a
tradus o carte în anii ’30 ai secolului trecut, dar noi nu
știam de lucrurile acestea. Am constatat cu consternare
că am fost primul după Al Doilea Război Mondial
care a scris în presa culturală de la noi despre Matila
Costiescu Ghyka. În felul acesta, am descoperit ceea ce
am numit poetica matematică. Am beneficiat de sprijinul
unor mari oameni de cultură, sprijin fără de care nu aș
fi putut rezista, pentru că era o îndoială generală față de
866

Dezmeticindu-ne

aceste preocupări ale mele de atunci: Miron Nicolescu,
despre care am vorbit, Grigore Moisil, matematicianul,
Alexandru Rosetti și nu mai târziu, nu cu mult timp
înainte de a muri, Tudor Vianu. În felul acesta, îmi
extinsesem perimetrul: matematică, lingvistică, poetică.
Dar, mergând pe drumurile lingvisticii matematice,
am descoperit imediat că toate rădăcinile ei sunt strâns
legate de biologie. Studiul sistemului nervos era esențial
la mijlocul secolului al XX‑lea. Lingvistica matematică
a început în mare măsură ca o preocupare pentru studiul
sistemului nervos. Activitatea nervoasă superioară era
văzută ca rezultat al unei mașini de tipul unei gramatici
generative. Am început astfel să studiez sistemul nervos.
Imediat s‑a întâmplat că au apărut marile descoperiri în
domeniul biologiei moleculare al eredității, care intră
imediat și ele sub raza de acțiune a lingvisticii.
Am înțeles că toate lucrurile se leagă strâns de
domeniul emergent în acea perioadă al informaticii și
calculatoarelor, pentru că prima preocupare practică în
studiul lingvisticii matematice a fost problema traducerii
automate. Dintr‑o dată am înțeles că trebuie să mă
inițiez serios în domeniul informaticii și calculatoarelor.
Toate mergeau mână în mână, absolut într‑un mod
natural, dintr‑una în alta. Au venit anii ’70 și o nouă
experiență de viață mi‑a extins enorm raza de acțiune.
Printr‑un concurs de împrejurări, ajung șeful echipei
867

Solomon Marcus

Universității din București pentru Proiectul Internațional
al Universității Națiunilor Unite cu sediul la Tokio,
Obiective, procese și indicatori de dezvoltare. Și pentru
că era un proiect care aducea zeci de mii de dolari anual
Universității din București, mi s‑a acceptat participarea
cu o echipă de matematicieni la acest proiect timp de
mai mulți ani. Participând la întâlniri cu personalități
venite din aprox. 30 de țări ale lumii, din cele mai
variate domenii posibile: economie, inginerie, medicină,
filosofie, calculatoare, etc., am intrat într‑un dialog
interdisciplinar extraordinar și mi‑am dat seama că
multe din lucrurile pe care le asimilasem din lingvistică,
matematică, biologie, își au un analog potențial sau deja
existent în discipline complet diferite.
După toate acestea, a venit descoperirea semioticii.
Studiul proceselor de semne. Am intrat și în această
aventură cu entuziasm și în acest moment mă găseam
în fața unui spectacol extraordinar. Simțeam că, de fapt,
între toate aceste domenii care în birocrația culturală
apar izolat, fiecare pe cont propriu, există posibilitatea
de a construi punți și că acestei construcții trebuie să mă
dedic. Acestea sunt paradigmele universale.
Vă mulțumesc pentru atenție. Mă bucur că această
recunoaștere vine din orașul meu natal. După ce am
parcurs și geografic și cultural întreaga lume, să mă
întorc și să primesc aici o recunoaștere îmi oferă o mare
868

Dezmeticindu-ne

satisfacție. După ce mi‑am potolit succesiv curiozitatea
de a descoperi ca băcăuan mai întâi Bucureștiul, apoi
Parisul, apoi America și Extremul Orient, ce mare
plăcere este să revin aici!

869

