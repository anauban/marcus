Există un secret al învățării matematicii?
Preuniversitaria – Ghidul candidatului la admiterea în
învățământul superior și de orientare profesională –
ediție specială – nr. 6(48), decembrie 1990, pp. 30‑31
Există două surse ale inaccesibilității matematicii:
una internă, deci care ține de natura matematicii, alta
externă, care se referă la relația matematicii cu lumea.
Sursa internă: gândirea matematică procedează în etape
riguros ordonate, fiecare etapă sprijinindu‑se esențial pe
etapele anterioare. Este deci necesară obișnuința cu o
activitate intelectuală care se prelungește mult în timp
și care reclamă o disciplină severă a atenției; în contrast
cu activitățile intelectuale de fiecare zi, care îngăduie
uitarea multor etape anterioare. Pentru a face față acestor
exigențe, matematica își creează un limbaj propriu al
simbolurilor și formulelor; prin acest limbaj, nu lipsit de
dificultăți, gânduri și judecăți care exprimate în cuvinte
ar conduce la fraze nepermis de lungi se reduc la expresii
lapidare și foarte sugestive. Să mai observăm faptul că
însuși obiectul matematicii este destul de inaccesibil.
Dacă este ușor să explicăm unui profan cu ce se ocupă
fizica, chimia sau biologia, este incomparabil mai greu
82

Dezmeticindu-ne

să‑i explici cu ce se ocupă matematica. În aceste condiții,
cum să ne împrietenim cu limbajul matematic, aparent
rece și indiferent?
Poate fi atenuată dificultatea care provine din
situația pe care tocmai am evocat‑o? Da, desigur;
dar pentru aceasta este nevoie de o anumită artă
pedagogică și în felul acesta ajungem la sursa externă
a inaccesibilității matematicii. Generații după generații
au absolvit școala de cultură generală fără a afla în ce
constă obiectul matematicii. Matematicienii au fost
confundați mereu când cu contabilii, când cu inginerii
sau, mai recent, cu informaticienii. Imaginea comună
a matematicianului este aceea a unui individ capabil
să efectueze rapid calcule dintre cele mai complicate.
Matematica este de obicei asimilată cu “știința cifrelor”
și puțini bănuiesc posibilitatea unei matematici
calitative. O confuzie ceva mai subtilă este aceea dintre
matematică și logică. Într‑adevăr, logica este haina în
care matematica iese în lume, modul prin care ea își
elaborează ideile este de o mare varietate, incluzând
bănuieli, tatonări, emoții, intuiții și afectivitate,
imaginație și invenție.
Educația matematică ar trebui să se miște în această
direcție, să‑i inițieze pe tineri nu numai în calcule și
deducții, ci și în metamorfoza prin care o problemă
relativă la natura înconjurătoare devine o problemă de
83

Solomon Marcus

matematică; apoi, modul de abordare a acestei probleme
presupune elaborarea unui model matematic pe care
trebuie să‑l inventăm printr‑un efort de analogie care nu
suportă rețete. Este ușor să rezolvi anumite ecuații pe
baza unei formule învățate pe dinafară; mai dificil, dar și
mai dătător de satisfacții, este să descoperi singur tipul
de ecuație care descrie un anumit fenomen care se cere
studiat; apoi să rezolvi ecuația și să interpretezi soluția în
raport cu problema de la care ai plecat.
Putem înțelege de ce atât de mulți tineri nu iubesc
matematica; deoarece ea comportă un efort deosebit,
care de cele mai multe ori nu este însoțit de o satisfacție
intelectuală pe măsură. Pedagogia matematică ar trebui
să reducă și chiar să elimine discrepanța dintre efort și
recompensă, în învățarea matematicii, prin eliminarea
elementelor artificiale, de natură să intimideze pe
începători. Dacă ești obligat să înveți o teoremă fără
a înțelege semnificația și utilitatea ei, dacă ți se cere
să efectuezi calcule laborioase cărora nu li se vede
finalitatea, dacă nu ți se dă posibilitatea de a înțelege
legătura dintre matematică și celelalte domenii ale
științei, ale culturii în genere, atunci cum poți iubi
matematica?
Este deci nevoie de o schimbare fundamentală
în sistemul de educație matematică, în așa fel încât
să fie reliefate funcția de cunoaștere a matematicii,
84

Dezmeticindu-ne

solidaritatea ei cu celelalte teritorii ale culturii. În această
nouă perspectivă, matematica nu se mai poate reduce la
latura ei operatorie, ci reclamă un echilibru între idei și
tehnică de lucru, între inducție și deducție, între tatonare
și verificare, între observație, experiment și generalizare,
între invenție și descoperire. În același timp, se impune
o repunere în drepturi a limbajului natural, care a fost
marginalizat în educația matematică în așa măsură
încât aproape nu mai sunt folosite cuvinte. Simbolurile
și formulele trebuie utilizate numai atunci când sunt
inevitabile; altfel, mărim în mod artificial și dăunător
inaccesibilitatea limbajului matematic. Dacă educația
științifică se rupe de limba naturală, ea se autosabotează,
deoarece gândim prin intermediul limbii naturale.
Galilei atrăgea atenția, în urmă cu câteva secole,
că marea carte a naturii este scrisă în limba matematicii.
Acum putem să aprofundăm această idee și s‑o
modificăm în sensul că această carte a naturii chiar
noi o scriem; ea nu este gata și complet scrisă de către
alții, dinaintea noastră, și nici măcar de Dumnezeu.
Limbajul eredității și‑a găsit o expresie matematică
abia în secolul nostru; cibernetica este o știință a
analogiilor, iar numitorul comun al acestor analogii este
de natură matematică. Calculatoarele nu‑s decât fațada
unei strategii intelectuale preconizate încă de Pascal
și Leibniz, în urmă cu câteva secole, dar care și‑a găsit
85

Solomon Marcus

explicitarea în ultimii 60 de ani, prin precizarea statutului
matematic al gândirii algoritmice și prin elaborarea
limbajelor de comunicare om‑mașină. Cosmonautica nu
ar fi posibilă fără calculatoare, cu întreaga matematică
de finețe care le pune în mișcare; aceeași matematică de
finețe se află la baza construirii navelor cosmice.
Este foarte răspândită ideea după care toată educația
matematică ar fi favorizată considerabil de frecventarea
jocurilor matematice, de tipul acelora pe care le găsim
la rubricile de divertisment din reviste de tot felul. Fără
a nega un anumit rol pozitiv al acestor jocuri, credem
totuși că mult mai eficientă este folosirea în educație
a aspectelor ludice imanente științei, matematicii în
particular. Matematica are, în chiar țesătura ei, un aspect
de joc, de la tatonarea unor încercări alternative până
la manipularea simbolurilor puse în mișcare printr‑un
anumit sistem de reguli. Jocul este un corolar inevitabil
al faptului că numărul de încercări este, în general, mai
mare decât numărul de reușite. Din nefericire, mulți
profesori ratează această șansă ludică, dătătoare de mari
satisfacții, mai ales la vârsta adolescenței și a tinereții;
acești profesori, știind dinainte calea optimă, renunță
să‑i mai pună pe elevii lor în fața alegerii unei variante
din mai multe posibile, sub pretextul că „nu este timp”.
Dar dacă nu avem timp pentru lucruri atât de importante,
atunci să nu ne mai mire eșecul.
86

Dezmeticindu-ne

Datorită numeroaselor greșeli comise în educația
matematică, greșeli concretizate uneori în reforme
care, chiar dacă purtau girul unor mari matematicieni
(Andre Lichnerowicz și Jean Dieudonné în Franța, A.
N. Kolmogorov în Uniunea Sovietică), nu erau lipsite de
vicii esențiale, care le‑au condus la eșec, astăzi ne aflăm,
în cele mai multe țări, în situația unei mari majorități a
elevilor care nu prea înțeleg matematica predată la clasă
decât în partea ei de rețetă și, cu atât mai mult, nu o
iubesc. Procentul de 80% este probabil o bună aproximare
a proporției de elevi care se află în această situație.
Desigur, toate aprecierile pe care le‑am făcut se
referă la situația generală. Vârfurile nu au lipsit în nicio
generație. Fiecare promoție de elevi a furnizat un număr
de talente matematice remarcabile, care au făcut posibile
succesele românești la olimpiadele internaționale de
matematică și care au condus treptat la formarea școlii
românești de matematică, despre care nu putem avea o
imagine adecvată decât dacă includem și pe numeroșii
matematicieni români care, de‑a lungul anilor, au
emigrat – din motive bine cunoscute – devenind, mulți
dintre ei, profesori de prestigiu ai unor mari universități
europene sau americane; istoria acestei școli va trebui să
fie scrisă.
Învățarea matematicii capătă o cu totul altă
coloratură atunci când este vorba de cei dotați pentru
87

Solomon Marcus

matematică. Dar și în cazul lor, pedagogia poate
accelera sau întârzia procesul. În mod special astăzi,
talentul singur nu poate face un cercetător, deoarece
fără o cultură suficient de vastă talentul se risipește cu
preponderență în regăsirea unor rezultate cunoscute.
La începutul secolului nostru mai era posibil ca un
matematician autodidact de geniu ca indianul Ramanujan
să descopere pe cont propriu lucruri uimitoare, utile și
azi; acum însă, un nou fenomen de tip Ramanujan s‑ar
părea că nu mai este posibil. De altfel, chiar Ramanujan,
considerat la fel de genial ca și Gauss, în absența unei
educații matematice adecvate a dat un randament mult
inferior lui Gauss.
Care este deci „secretul învățării matematicii”?
Este ambiția de a înțelege cu ce se ocupă ea, de ce
este importantă și interesantă, și de a gândi apoi totul
cu capul tău, neacceptând nicio autoritate alta decât
explicația naturală și argumentul bine întemeiat. În
acest fel, învățarea matematicii se transformă și într‑o
experiență estetică remarcabilă.
Dificultatea de a prinde într‑o definiție obiectul
matematicii se explică tocmai prin împrejurările
descrise mai sus. Matematica este, în același timp, un
studiu al structurilor și al evoluției formelor; o ordonare
riguroasă a faptelor, după gradul lor de complexitate; un
univers de ficțiune și de experiențe îndrăznețe; un șir de
88

Dezmeticindu-ne

ipoteze, de la cele mai cuminți la cele mai nebunești;
o știință a infinitului; o artă a tatonărilor intuitive și a
raționamentelor ingenioase; o mediere între cunoștințe
dintre cele mai variate; o sursă de modele cognitive; un
punct de vedere potențial verificabil în orice împrejurare
de viață. Scadența ei este îndepărtată, dar eficacitatea ei
este, pe cât de indirectă, pe atât de profundă.

89

