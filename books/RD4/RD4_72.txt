„Am trăit în două universuri:
al poeziei şi al războiului“
Interviu de Roxana Lupu,
Adevărul, 2011
Profesorul Solomon Marcus (86 de ani) vorbeşte
despre matematică, poezie, revelaţie, visuri, alcătuiri
ale unei vieţi închinate descoperirii: „Dar oare ce‑o fi,
dacă priveşti mai departe şi mai departe?“, se întreba
matematicianul încă din copilărie.
R.L.: Sunteţi foarte energic: dezbateri, prezentări,
congrese ştiinţifice... 
S.M.: ...Am considerat întotdeauna că una din
marile şanse ale vieţii mele a fost aceea de a fi ales
această profesie: profesor cercetător în matematică şi
toate prelungirile ei, care, pentru mine, merg peste tot. 
R.L.: Aţi mizat pe interdisciplinaritatea aceasta
dintre matematică şi tot ceea ce ne înconjoară. 
S.M.: Dar asta a venit în mod natural. Din etapă în
etapă m‑am trezit că am nevoie de alte şi alte orizonturi.
Nu a fost un program, a fost cursul natural al vieţii mele.
Când eram elev, pasiunile mele erau literatura şi teatrul.
707

Solomon Marcus

Filosofia iarăşi mă interesa. Matematica am descoperit‑o
abia la vârsta Bacalaureatului. 
R.L.: Şi a fost un risc asumat: aţi dat la Facultatea
de Matematică pentru că vă fascina denumirea
disciplinelor, afişate la avizier. 
S.M.: Am făcut acest lucru perfect conştient de
riscul pe care mi‑l asum. A fost un mare noroc că nu a
trebuit să schimb ulterior specializarea.
R.L.: Dar cum aţi numi această cotitură bruscă şi
radicală spre matematică? Fler?
S.M.: A fost o bănuială. Că s‑ar putea ca acolo să
se ascundă ceva extraordinar. Doar o bănuială, care s‑a
confirmat foarte repede. De la primele cursuri de Analiză
matematică am înţeles că am nimerit într‑un loc care mă
aştepta. Am simţit că matematica este studiul proceselor
cu o infinitate de etape şi lucrul acesta mă pasiona încă
din copilărie.
R.L.: Cum aşa?
S.M.: Eram copil de şcoală primară şi la sfârşitul
caietelor mele mă apucam să scriu, în mod instinctiv,
numerele: 1, 2, 3... Şi eram fascinat de faptul că nu te
poţi opri. Că nu se termină şirul ăsta. Şi lucrul acesta m‑a
impresionat puternic: dar oare ce‑o fi, dacă priveşti mai
departe şi mai departe? mă întrebam.
R.L.: Vă gândeaţi la asta în copilărie?

708

Dezmeticindu-ne

S.M.: Da. Era o curiozitate, mă uitam la viaţa
mea. În fiecare zi vorbim de mâine, apoi de poimâine,
ca şi numerele se tot înşiră, ca mărgelele pe aţă, şi nu
se vede un sfârşit; şi, de fapt, niciun început. Pentru
că noi, în mod artificial, începem de la un anumit
moment. Găsisem undeva întrebarea aceasta, pe care
am reluat‑o apoi mereu la întâlnirile mele cu elevii: „Ai
un măr. Mănânci azi jumătate din el, mâine jumătate
din ce‑a rămas, poimâine jumătate din ce ţi‑a rămas cu
o zi în urmă. Şi atunci, mărul acesta când se termină de
mâncat?” Noi ştim din practică: un măr se termină de
mâncat destul de repede. Dar după această socoteală, el
nu se termină niciodată. Toate aceste întrebări veneau
peste mine, dar nu‑mi dădeam seama unde bat, pentru că
matematica şcolară nu m‑a atras.
R.L.: Ce aţi citit pe avizierul acela din Facultate, ce
era scris acolo?
S.M.: „Aritmetică transfinită”, „Calcul infinitezimal”.
Erau cuvinte care aveau pentru mine un efect de vrajă,
deşi nu le înţelegeam conceptual.
R.L.: Folosiţi un cuvânt surprinzător ca să descrieţi
starea indusă atunci de aceste expresii: „vertij”.
S.M.: Cuvântul acesta nu l‑am pronunţat eu prima
dată în legătură cu matematica. În cartea mea, „Poetica
matematică”, am citat un mare matematician american, 
Marston Morse, care spune că disciplina aceasta te duce
709

Solomon Marcus

în momentele ei supreme la o stare de vertij, de beţie, de
desprindere din contingent. 
R.L.: Dar avuseseţi acelaşi sentiment cu poezia,
înainte de această întâlnire.
S.M.: Cronologic da; întâlnirea cu poezia s‑a
petrecut la 15 ani. Unul din primele momente de acest
gen s‑a petrecut când am citit „Sara pe deal”. Ei, acolo,
la capătul lecturii, am simţit că m‑am umplut de o
stare atât de puternică, încât nu am mai putut continua.
Ştiam alte poezii de Eminescu, de pildă „Ce te legeni,
codrule?” sau „Somnoroase păsărele”, pe care le învăţam
pe dinafară, dar nu înţelegeam mare lucru din ele.  Dar
la „Sara pe deal” este o incandescenţă extraordinară. Am
simţit această stare de vrajă. Am regăsit‑o apoi la Rilke,
Edgar Allan Poe, Baudelaire. În matematică a venit
mult mai târziu, din cauză că matematica şcolară nu
era îndreptată spre idee, spre viziune, era confiscată de
procedee mecanice. Îţi educa simţul ordinii, dar nu era
ceva de profunzime.
R.L.: Şi profunzimea ei când aţi descoperit‑o cu
adevărat?
S.M.: Profunzimea ei o descopăr mereu, în noi
şi noi înfăţişări. Nu se termină această descoperire.
Descopăr noi aspecte şi mă minunez în faţa lor cu
aceeaşi prospeţime cu care un copil vede prima oară
soarele. Târziu, la maturitate, am început să citesc mai
710

Dezmeticindu-ne

mult despre geometriile neeuclidiene şi sunt unele
lucruri pe care le‑am aflat abia în ultimii ani şi cu care,
de multe ori, adorm în minte. Dar astea le‑am descoperit
în ultimii ani, pentru că matematica trăieşte un decalaj
extraordinar între ceea ce este ea în profunzime –
privilegiul unui număr redus de oameni, care i se dedică
– şi cum apare ea în sistemul educaţional.
R.L.: Dumneavoastră aţi făcut‑o atrăgătoare,
aţi făcut legătura dintre matematică şi poezie, dintre
matematică şi teatru, dintre matematică şi artele vizuale.
S.M.: Atunci când am început  „Poetica matematică”,
bănuiam că numeri pe degete pe cei cărora le‑a dat prin
cap să asocieze poezia cu matematica. Am trăit multă
vreme cu ideea că asocierea acestor două domenii e
o chestie neobişnuită, singulară şi greu asimilată de
oameni. Am descoperit treptat că toţi marii poeţi ai
lumii au scris măcar o dată despre această asociere. De
pildă, Dante are în „Divina Comedie” referinţe bogate
la matematică;Shakespeare, în sonetele lui, Goethe,
în „Faust”, în convorbirile cu Eckermann (n.r. ‑ poet şi
memorialist german, secretarul particular al lui Goethe);
marele poet italian Leopardi, Edgar Allan Poe, Novalis,
Paul Valéry... Lista aceasta este extraordinar de lungă.
Toţi găsesc că un poet, pe deplin conştient de misiunea
lui, nu poate să rateze, din câmpul lucrurilor în faţa
cărora se minunează, acele expresii ale ordinii, regăsite
711

Solomon Marcus

în ştiinţele exacte. Numai lipsa de cultură şi informaţie
face ca o serie de umanişti să continue să vorbească
despre caracterul ciudat al asocierii matematicii cu
poezia sau al ştiinţei cu literatura. Nu, această asociere
este deja un fenomen frecvent, curent, sistematic.
R.L.: Matematica şi poezia ne apropie amândouă de
paradoxul existenţei, aţi spus.
S.M.: Asta ţine de modul în care eu m‑am apropiat
iniţial şi de matematică, şi de poezie. Şi la una, şi la
cealaltă, infinitul are un rol esenţial, în ambele ieşi
din logica vieţii cotidiene şi intri într‑o altă lume, a
paradoxurilor de tot felul, într‑un univers de ficţiune. Ele
răspund pentru mine unei nevoi vitale: mi‑am dat seama
că m‑am născut cu această dorinţă de a înţelege lumea şi
de a mă înţelege pe mine. 
R.L.: Aţi primit răspunsuri?
S.M.: Am primit o serie de răspunsuri, dar mereu
rămâne loc pentru noi răspunsuri pentru că misterul
lumii este inepuizabil. 
Poezia şi matematica răspund pentru mine unei
nevoi vitale: mi‑am dat seama că m‑am născut cu
această dorinţă de a înţelege lumea şi de a mă înţelege
pe mine.

712

Dezmeticindu-ne

Descopăr noi aspecte şi mă minunez în faţa lor cu
aceeaşi prospeţime cu care un copil vede prima oară
soarele.
„Trăiam într‑o Românie care mă respingea”
R.L.: Spuneţi‑mi o revelaţie pe care v‑a dat‑o
matematica în viaţa particulară.
S.M.: La 15 ani trăiam într‑o realitate exterioară
dramatică. Era al Doilea Război Mondial, iar eu eram
supus unei discriminări care mă arunca la marginea
societăţii. Trăiam într‑o Românie care mă respingea.
Şi în aceste condiţii citeam „Sara pe deal” şi mă
îndrăgosteam de adevărata Românie: Eminescu, dar şi
Arghezi şi Ion Barbu. M‑am regăsit atât de organic în
poezia lor, am simţit că ei răspund atât de bine nevoilor
mele sufleteşti, încât nu mă mai puteam dezlipi de ei. Şi
totul era legat, în mod evident, de limba română. Trăiam
în două universuri paralele: al poeziei şi al războiului,
pentru că România a avut o legislaţie rasială, între 1940
şi 1944. Nu aveam voie să frecventez nicio şcoală. Iar
în viaţa cotidiană, dacă voiam să cumpăr o pâine trebuia
să stau la o linie de aşteptare, dar oricine mă putea lua
şi duce la coada cozii, pentru că el avea prioritate. Erau
interdicţii care te puneau în situaţii foarte umilitoare, te
simţeai ca un non‑cetăţean, simţeai că nu eşti o fiinţă
umană autentică. Vă daţi seama cum e ca la vârsta de
7 ani să te vezi fugărit de un alt copil, care‑ţi strigă din
713

Solomon Marcus

urmă: „Mă, jidane, de ce l‑ai omorât pe Iisus Hristos?”.
Era o situaţie curentă. Nu mai vorbesc că exista pericolul
ca de la un moment la altul să fii deportat. Am avut
norocul şi am scăpat, pentru că nu eram în imediata
apropiere a frontului. Şi în condiţiile acestea am
descoperit poezia.
R.L.: V‑aţi refugiat în ea. 
S.M.: Da, a fost o adevărată binecuvântare şi, prin
aceşti poeţi, nu m‑am putut despărţi de limba română
de‑a lungul întregii mele vieţi. Mă întrebau unii:
„Domnule, ai primit invitaţii din toată lumea, de ce
n‑ai rămas acolo?”. Iar eu le‑am spus: „Nu mi‑am pus
niciodată această întrebare. Dumneata mi‑o adresezi
acum”. 
R.L.: Aţi avut parte de mentori celebri, personalităţi
marcante ale matematicii.
S.M.: Din păcate, nu pot include printre mentorii
mei pe niciunul dintre profesorii de şcoală, deşi mulţi
îmi erau simpatici, dar n‑au lăsat urme în mine. Primul
care m‑a marcat a fost Miron Nicolescu, apoi Grigore
Moisil, Simion Stoilow, Dan Barbilian (n.r. ‑ numele
adevărat al poetului şi matematicianului Ion Barbu),
Gheorghe Vrănceanu. Apoi, abia în urmă cu câţiva ani,
am descoperit mentori în matematicieni pe care nu i‑am
avut ca profesori, Traian Lalescu, Dimitrie Pompeiu.
Sau de pildă Gheorghe Lazăr, pe care îl descopăr ca pe
714

Dezmeticindu-ne

un personaj fascinant. Pentru mine ideea de mentor nu
trebuie confundată cu ideea care circulă acum, ideea de
modele. La fiecare dintre aceştia pe care i‑am enumerat
pot să vorbesc şi despre lucruri care nu mi‑au plăcut. Dar
mi‑au fost mentori pentru că, într‑o anumită perioadă
a vieţii, m‑au marcat foarte puternic. Mi‑am dat seama
că oamenii nu sunt niciodată perfecţi. Fiecare om
are anumite direcţii în care excelează şi altele în care
eşuează.
R.L.: Dumneavoastră în ce domeniu eşuaţi?
S.M.: Eu? În cele mai multe. (râde) De pildă
azi dimineaţă, când am încercat să închid lumina, la
comutator, mi‑am dat seama că s‑a defectat tabloul
electric. La lucruri de genul acesta eşuez, în general. 
R.L.: Aţi simţit că aplecarea aceasta profundă către
studiu v‑a afectat mecanismele de relaţionare socială?
S.M.: În general, câştigul obţinut din opţiunea
făcută era suficient pentru a mă împăca în ceea ce
priveşte lucrul la care am renunţat. Foarte rar au fost
situaţii în care să mă găsesc într‑o dilemă. Dar îmi
amintesc una care ar fi putut fi, însă nu a fost. Era în
1972 şi mă aflam la o universitate din Olanda, unde
primisem oferta unui contract pe durata a doi ani, ca
profesor. Am răspuns în mod spontan: „Nu pot. Sunt
aşteptat la Bucureşti”. 
R.L.: Cine vă aştepta? 
715

Solomon Marcus

S.M.: Mă aştepta soţia mea, Paula Diaconescu,
profesoară la Facultatea de Litere, mă aştepta Miron
Nicolescu, mă aştepta o lume întreagă de studenţi.
Mi‑a scris zilele trecute un matematician român care
acum, la aproape 50 de ani, este profesor la Facultatea
din Marsilia. Spunea că şi‑a amintit perioada când era
student în anul al doilea şi eu îi eram profesor la cursul
de Analiză matematică şi că i‑a plăcut mult acest curs. A
dat o serie întreagă de detalii. Şi mi‑am spus: „Iată ce am
însemnat eu pentru el în acel moment”.
R.L.: Aţi devenit, la rândul dumneavoastră, mentor.
S.M.: Am lucrat întotdeauna personal cu studenţii
foarte buni, mergeam cu ei la bibliotecă. Dar să ştiţi că
şi unii dintre ei au devenit, de‑a lungul timpului, mentori
pentru mine: matematicianul Vasile Ene, prin modul
său de a se dedica şi de a trăi problemele matematice.
Mai târziu, mi‑a spus: „Când mi‑aţi dat revista aceea de
matematică, m‑am dus cu ea acasă şi mi‑am dat seama
că asta era ce aveam eu nevoie şi nimeni nu‑mi arătase
până atunci. Această revistă mi‑a schimbat viaţa”. 
Prin aceşti poeţi, Eminescu, Arghezi şi Ion Barbu,
eu nu m‑am putut despărţi de limba română de‑a lungul
întregii mele vieţi.

716

Dezmeticindu-ne

„Am un vis recurent: acela că mă rătăcesc”
R.L.: Cum erau părinţii dumneavoastră? 
S.M.: Tatăl meu era croitor, dar în anii ̓30, când
mi‑am trăit copilăria, decăzuse foarte mult, ajunsese
într‑o stare de sărăcie. Mama era croitoreasă şi ea,
dar casnică, pentru că avea de crescut opt copii. Doi
dintre fraţii mei au murit în Primul Război Mondial de
tifos exantematic. Mama mea a avut această vocaţie a
sacrificiului, muncea toată ziua. Aveam o relaţie afectivă
foarte intensă cu ea. Tatăl meu suferea că nu reuşise să
atragă pe niciunul dintre băieţii lui la croitorie şi şi‑a
zis că poate îi reuşeşte cu mine, mezinul familiei. După
ce mi‑a explicat vreo oră întreagă cum se mânuieşte
foarfeca, a pus diagnosticul clar: „Nu eşti bun de
nimic!”. El voia să nu mă mai dea la şcoală ‑ tocmai
rămăsesem repetent în clasa a doua, dar unul din fraţii
mei s‑a dus şi m‑a înscris.
R.L.: Spuneaţi undeva că momentul acela, când un
profesor v‑a lăsat repetent, e o rană ce nu s‑a închis nici
astăzi. 
S.M.: M‑a lăsat repetent la limba română, care mie
îmi plăcea atât de mult. M‑am încurcat la o poezie foarte
lungă. Când am văzut afişat cuvântul „repetent”, timp de
o lună n‑am ieşit din casă de ruşine. Copiii se strângeau
în jur, mă arătau cu degetul şi strigau: „Repetentul!
Repetentul!”. Şi asta m‑a apropiat de poezie şi de teatru.
717

Solomon Marcus

Ţin minte prima piesă pe care am văzut‑o: o trupă de
amatori a interpretat „Omul care a văzut moartea”, de
Victor Eftimiu. M‑am furişat în teatru şi chestia asta,
că poţi să recompui viaţa pe‑o scenă, m‑a impresionat
foarte mult. Şi de atunci m‑am îndrăgostit de teatru
şi chiar voiam să mă fac actor. Mi se părea că este
extraordinar să trăieşti într‑un altul.
R.L.: Filosofia compromisului spune că în timp ce
obţii un avantaj pe de o parte, plăteşti cu un dezavantaj.
Ce v‑a luat matematica? 
S.M.: Unul din compromisuri a fost acesta, că în
loc să fac ce fac cei mai mulţi ‑ se duc să se distreze,
cum se spune, eu rămâneam să citesc şi să scriu. Se
vorbeşte mereu că trebuie să optimizăm cutare activitate,
dar cele mai multe împrejurări de viaţă nu se rezolvă
în termeni de optimizare, ci în termeni de compromis.
Adică, ce pierzi când câştigi cutare lucru sau ce câştigi
când pierzi cutare lucru. Trebuie mereu să cântăreşti. Şi
există o întreagă matematică a compromisului.
R.L.: Puteţi afirma despre dumneavoastră că sunteţi
un optimist?
S.M.: Niciodată nu discut în termeni de optimism
şi pesimism. Nu folosesc termeni de referinţă care ţin de
întâmplare. Folosesc acei factori care depind de mine. 
R.L.: Şi totuşi, se poate pune semnul egal între ce
depinde de noi şi ce nu depinde de noi?
718

Dezmeticindu-ne

S.M.: Toată problema asta este: să ştii, să înţelegi că
multe lucruri depind de tine şi să le foloseşti. Ca atunci
când discuţi despre viitor să primeze nu imaginea a ceva
care vine peste tine, ci imaginea a ceva proiectat de tine.
Ca un lucru pe care, în mare măsură, tu îl poţi proiecta.
Altfel, cred că optimistul e cel care crede că întâmplarea
îl va favoriza, iar pesimistul ‑ cel care crede invers. 
R.L.: Atunci cum v‑aţi numi această disponibilitate
a dumneavoastră de a vă bucura de viaţă în simplitatea
ei?
S.M.: În asta constă o adevărată personalitate, cred:
în faptul că nu se lasă făcut de altul. Să nu te comporţi ca
şi cum eşti manipulat: de lecturi, persoane, împrejurări.
Să te comporţi prin modul tău personal de gândire. Dar
şcoala nu‑l învaţă pe elev să gândească cu capul lui. El
mereu asimilează şi repetă lucruri care vin din altă parte,
nu din capul lui. 
R.L.: Spuneaţi undeva că în perioadele de
solitudine vă analizaţi visurile.
S.M.: Dacă nu eşti atent la visurile tale, ratezi o
posibilitate foarte importantă de a te înţelege. Pentru
că, vedeţi, noi în viaţa conştientă avem un autocontrol.
În vise, nu‑l mai avem. Şi decalajul acesta te face să
înţelegi în ce măsură comportamentul tău vine dintr‑o
reprimare a unui alt eu al tău. Toată psihologia fiinţei
umane e axată pe diverse tipuri de eu‑ri. 
719

Solomon Marcus

R.L.: Aveţi un vis recurent?
S.M.: Da, sunt visurile în care mă rătăcesc. Ajung
într‑un loc, nu ştiu ce e cu locul acela, unde sunt.
Pierdere. Un alt vis recurent este că‑mi dispar cheile sau
actele. Asta este şi o obsesie în viaţa reală. Ultima mea
groază în această privinţă este că trebuie să‑mi schimb
actele de la gaz de pe numele soţiei mele, care a decedat
acum şase ani, pe numele meu. Se pare că e o chestie
foarte complicată, iar persoana care se ocupă de asta a
pronunţat un cuvânt care îmi stârneşte groaza: „N‑aveţi
cadastru!” Aţi auzit de cuvântul ăsta? Este înfiorător. 
Dacă nu eşti atent la visurile tale, ratezi o
posibilitate foarte importantă de a te înţelege.
Tatăl meu suferea că nu reuşise să atragă pe
niciunul dintre băieţii lui la croitorie şi şi‑a zis că poate
îi reuşeşte cu mine, mezinul familiei.

Una din două
•	 Cézanne sau Picasso? Picasso.
•	 Johannes Brahms sau Richard Wagner? Brahms. 
•	 Stephane Mallarmé sau Paul Valéry? Valéry. 
•	 Poezii eminesciene: „Mortua est” sau „Epigonii”?
„Epigonii”.
•	 Reiner Maria Rilke sau Edgar Allan Poe? Rilke. 
720

Dezmeticindu-ne

•	 „Faust” (Goethe) sau „Divina Comedie” (Dante
Alighieri)? „Faust”.
•	 Teatru: Samuel Beckett sau William Shakespeare?
Shakespeare.
•	 Arhimede sau Euclid? Arhimede.
•	 Tango sau balet? Tango.
•	 Vioară sau pian? Vioară.

721

