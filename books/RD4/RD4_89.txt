Întâlnire cu elevii la
Colegiul Național “Spiru Haret”
București, 12 decembrie 2011
Mă aflu pentru a doua oară aici și emoția mea are
rațiuni multiple, în primul rând pentru numele pe care‑l
poartă acest colegiu și apoi pentru întreaga sa istorie.
Știind că azi voi veni aici, ieri am încercat să vă vizitez pe
site. Din păcate, site‑ul arată destul de sărac. Unul dintre
primele lucruri pe care am vrut să le văd a fost revista
Vlăstarul. Este menționată, dar nu am putut afla mai multe
lucruri despre ea. Mă bucur că am primit‑o acum, eu aș
fi vrut ca în acest moment să o fi citit deja și să vă spun
impresiile. Vreau doar să vă spun că mă impresionează
condiția ireproșabilă în care apare și să vă atrag atenția
asupra unei probleme care afectează multe reviste școlare
la noi. Atunci când se preia un text, trebuie neapărat
precizată sursa de unde a fost preluat. Am văzut în revistă
un text cu un titlu foarte frumos: “Nu‑ți droga viața,
droghează‑te cu viață!” care înțeleg că este un text preluat,
nu se indică niciun autor. Dar nu se indică de unde este
preluat. Acest lucru nu e permis, așa cum nu e permis nici
ca într‑un articol personal, dacă vreau să folosesc chiar
901

Solomon Marcus

numai o frază dintr‑o anumită sursă, să nu indic sursa.
Încep cu această observație critică pentru că ea se
referă la un fapt foarte important. Copiii trebuie să învețe
de la cea mai fragedă vârstă nu numai ce înseamnă
proprietate asupra unui lucru, dar să învețe să distingă
și proprietatea intelectuală. Un text nu devine al meu
doar pentru că eu l‑am transcris cu mâna mea. Astfel
de confuzii au putut fi detectate în ultimii ani nu doar
la nivelul elevilor, au ajuns până la nivel de miniștri.
Sunt oameni care ajung la vârsta adultă și care nu au o
reprezentare corectă asupra proprietății intelectuale.
Să ne întoarcem la preocuparea noastră principală.
Vreau să vă vorbesc despre Spiru Haret. Motivul principal
nu e doar faptul că peste scurt timp, în primul trimestru al
anului viitor, se vor împlini 100 de ani de la moartea lui
Spiru Haret. Ci pentru că veți vedea că, vorbind la modul
general și nu aprofundat despre Haret, putem să vorbim
foarte bine despre întreaga istorie a secolului al XIX‑lea
și început de secol al XX‑lea și putem vorbi despre
problemele actuale, foarte actuale ale școlii românești.
Volumul al IV‑lea din Operele lui Spiru Haret se referă
la sfârșitul secolului al XIX‑lea. Ca să înțelegeți acțiunea
lui Spiru Haret din acel moment, va fi necesar să vă rezum
întreaga desfășurare culturală a secolului al XIX‑lea.
Avem în primele decenii acțiunea lui Gheorghe
Lazăr la București, care înființează acea școală de la
902

Dezmeticindu-ne

Sfântul Sava, o instituție formidabilă pe care numai o
minte sclipitoare o putea concepe. Chiar dacă a trăit
foarte puțin și evenimentele istorice nu i‑au permis
să facă mai mult, el a marcat istoria. La Iași a marcat
istoria Gheorghe Asachi cu școala de inginerie. Ei sunt
cei care au demarat în Țara Românească și în Moldova
învățământul matematic în limba română. Ei sunt primii
autori de manuale. Gheorghe Lazăr a făcut o întreagă
școală cu discipolii săi: Ion Heliade Rădulescu, Ion
Ghica, Petrache Poenaru etc.
Are loc apoi Revoluția de la 1848, spre mijlocul
secolului avem Academia Mihăileană la Iași. Toate
aceste instituții, în virtutea unei tradiții care vine din
secolul al XVI‑lea, am în vedere școala latină de la
Cotnari, apoi vin Academiile Domnești, au un scenariu
care este totdeauna același. El se repetă și în secolul al
XIX‑lea, când se înființează noi instituții de cultură. Mai
întâi se dă atenție limbii, literaturii și istoriei, în a doua
etapă se dă atenție celorlalte discipline socio‑umane și
abia în a treia etapă se acordă atenție științelor naturii,
științelor exacte (matematică, chimie, biologie etc.). Este
un scenariu din care cultura românească n‑a putut ieși
până spre sfârșitul secolului al XIX‑lea.
Vine Unirea Principatelor, când se precipită o serie
de evenimente extraordinare. Trecerea la alfabetul latin,
înființarea societății Junimea de către Titu Maiorescu în
903

Solomon Marcus

1863, înființarea Universității din Iași în 1860, înființarea
Universității din București câțiva ani mai târziu,
înființarea Academiei Române la 1866. Exact ca în
întreaga tradiție, sunt primiți la Academie în primul rând
istorici, filologi, scriitori, iar rândul oamenilor de știință
vine câțiva ani mai târziu. În volumul 4 din Operele lui
Spiru Haret, el se află deci la sfârșit de secol al XIX‑lea.
Exista o lege a învățământului de la 1864 pe care Haret o
analizează și o dezvăluie ca fiind cu totul inacceptabilă.
De altfel, și V. A. Urechia recunoaște că acea lege fusese
făcută într‑un mod foarte superficial, grăbit, și că dădea
atenție exclusiv învățământului umanist. Tradiția era
atât de puternică încât era foarte greu să o disloci. Veți
observa că toate numele pe care vi le voi evoca, și
care se referă la acțiunea lui Haret, au intrat în istorie.
Aici aveți ocazia să îi vedeți pe toți aceștia ca oameni
vii, care își ciocneau punctele de vedere. Este o lecție
extraordinară de a vedea comportamentul unor oameni
care, atunci când sunt în acțiune umană, apar cu toate
virtuțile, dar și cu toate slăbiciunile lor. Noi trebuie să‑i
știm așa și nu sub forma congelată de mai târziu. Unul
dintre cei care‑i opuneau rezistență lui Spiru Haret era
Barbu Ștefănescu Delavrancea. El glorifica pe drept
cuvânt tradiția greco‑latină și avantajele culturii limbilor
latină și greacă, dar atât de mult încât nu mai vedea
necesitatea și a unei instrucțiuni în domeniul științelor
904

Dezmeticindu-ne

naturii și a științelor exacte. A te ciocni cu un orator
de talia lui Barbu Ștefănescu Delavrancea nu era ușor
și lucrul acesta îl face Spiru Haret. Mai erau și Tache
Ionescu, Gh. Dem Theodorescu.
Legea din 1864 nu ținea seamă de realitate. Chiar
în perioada de valabilitate a acestei legi, practica vieții
cotidiene a încălcat‑o. Haret spunea că s‑a născut de
la început ca o lege moartă, care nu se putea aplica.
România era atunci în plin proces de naștere a unei
clase burgheze. Primii pași spre o industrie națională,
spre comerț. Toate aceste activități impuneau învățarea
științelor și ingineriilor, așa cum le preconizau inginerul
Gh. Lazăr la București și inginerul Gh. Asachi la Iași.
E foarte interesant să observați ce însemna
matematica în acea vreme, chiar pentru Spiru Haret. O
să vă dați seama cât de mult s‑a schimbat reprezentarea
noastră despre matematică, despre știință în general,
de atunci și până azi. Matematica atunci era exclusiv
una practică. Aș spune că ea corespundea istoric la
ceea ce a reprezentat, în evoluția matematicii, perioada
babiloniană, o matematică înainte de apariția teoremei,
care a venit abia cu vechii greci. Era o matematică mai
degrabă empirică. Și eu am folosit acele manuale în care
se acorda un loc foarte mare calculului dobânzilor, tabele
de asigurări de mortalitate etc. Manualul de matematică
era dominat de lucruri practice și nu prea apărea cuvântul
905

Solomon Marcus

teoremă. Doar în câteva cazuri celebre, teorema lui
Thales, a lui Pitagora. Scenariul teoremă‑demonstrație
nu apăruse. Deci matematica școlară era în etapa ei
babiloniană. Raza de acțiune a matematicii nu era cea de
azi, cu o rază universală de acțiune. Nu există domeniu
în care matematica să nu‑și exercite impactul într‑un fel
sau altul. Raza ei de acțiune, așa cum o vedea societatea
de atunci, era limitată la mecanică, astronomie, fizică și
economie. Se făcea mare caz de folosirea instrumentului
matematic în domeniul economic‑financiar. Spiru Haret
căuta să convingă că e nevoie să avem o bifurcare și
chiar o trifurcare, care în mod normal ar trebui să vină
după gimnaziu. Bifurcarea era secția clasică cu accent
pe limbile latină și greacă, și secția reală cu accent pe
matematică și științele naturii. S‑a gândit chiar la o
trifurcare, o secție clasică, una de științele naturii și alta
de matematică. Atunci totul se afla la început, aveam
foarte puține școli. În 1864, când apărea prima lege,
numărai pe degete toate liceele din țară. Nicăieri nu era
în același loc un liceu clasic și unul real, elevii nu aveau
posibilitatea să aleagă.
Azi noi avem incomparabil mai multe universități
decât aveam licee în anii ’30 ai secolului trecut. Fiind
atât de puține licee, era nevoie de foarte puțini profesori
și a ocupa o catedră la un liceu în acea vreme era o mare
performanță.
906

Dezmeticindu-ne

Mentalitatea dominantă acum în rândul părinților
este că orice părinte care se respectă trebuie să‑și trimită
odraslele la universitate. Nimeni nu concepe ca odraslele
sale să se orienteze spre o ocupație pentru care este
suficient liceul. Avem această tendință generală de a
obține cât mai multe certificate, diplome. Este o boală
socială. Am ajuns în situația de a livra noilor generații
o mare cantitate de diplome fără acoperire. Încercăm
să interpretăm în contextul de acum ceea ce făcea
Spiru Haret în contextul de atunci. Faptul că avem azi
un foarte mare număr de elevi care n‑au o motivație
profundă pentru învățătură, dar pot obține diplome cât
mai înalte, este foarte grav.
Steve Jobs, unul din cofondatorii Apple, făcea printre
altele observațiile următoare, adresându‑se tinerilor:
„Căutați să vă îndreptați interesul și ocupațiile spre zone
pe care le îndrăgiți, de care vă simțiți îndrăgostiți. Dacă nu
le‑ați găsit, continuați să căutați, pentru că atâta vreme cât
ajungi să te ocupi de un lucru de care nu ți‑e drag, eșecul
te așteaptă.ˮ Și eșecul se vede în faptul că atâția oameni
au îndeletniciri de care abia așteaptă să scape. Evitați
situația celor care continuă să urmeze diverse instituții de
învățământ numai pentru a obține o diplomă.
Observați valorile pe care le promovează mass‑media,
una din principalele forme ale vieții sociale. Promovează în
mare măsură non‑valori. Buletinele de știri sunt în primul
907

Solomon Marcus

rând informații privind condamnări și dări în judecată.
Chiar azi a murit un academician și a murit și o
cântăreață de muzică ușoară. În contrast cu cea de‑a
doua, despre academician doar pe un singur canal a fost
o informație telegrafică, în rest nimic. Despre oamenii
noștri de știință și performanțele lor aflăm foarte puțin.
Valorile sociale sunt cele pe care le creează școala. Dacă
școala nu e în stare să transforme procesul educațional
într‑un proces cultural și intelectual, nu avem la ce să
ne așteptăm. Așa cum sunt făcute azi cele mai multe
manuale școlare și programe, dezvoltă prea puțin
nevoia de adevăr, de a înțelege lumea, nu doar de a
reține cunoștințe pe care de fapt în mare măsură nu
le înțelegem și nu le putem explica. La televizor se dă
din când în când un citat din Antoine de Saint Éxupery
care spune că a oferi cultură înseamnă a oferi setea,
restul vine ca o consecință. Noi livrăm în mare măsură
tinerilor cunoștințe pentru care nu reușim să creăm în
prealabil setea de ele. Ceea ce spun nu vine în conflict
cu faptul că avem mulți profesori foarte merituoși. Dar ei
folosesc programe învechite, știința pe care o învățați azi
în școală e știința din secolul al XIX‑lea, în cel mai bun
caz. Exact despre cele mai spectaculoase lucruri nu auziți
nimic, e o știință care nu vă pune în ritm cu vremea
noastră. Nu vă trezește dorința de a înțelege lumea, de
a descoperi adevărul, ci doar de a reține, înmagazina
908

Dezmeticindu-ne

cunoștințe. Unde mai e dragostea de frumos în modul în
care se predă matematica azi cu manuale super‑obeze?
Unde mai e loc să stai să contempli o teoremă pentru
frumusețea ei? Lucruri care au făcut istorie, cum ar fi
iraționalitatea diagonalei unui pătrat, sunt expediate de
multe ori la subsolul paginii. Nu e loc de contemplare,
de îndoială, de întrebare. Exact valorile sociale care ar
trebui să existe sunt înnăbușite.
Profesor: Lucrurile acestea trebuie spuse nu
elevilor, ci forumurilor de la care vine programa.
S.M.: Dacă așteptați ca aceste schimbări să vină de
sus, veți aștepta o veșnicie. Schimbările esențiale trebuie
să vină din lumea școlii. Inițiativele unor alte programe și
manuale trebuie să vină de la cei mai buni elevi, profesori,
părinți. Să luăm exemplul SUA, unde reglementările
venite de la guvern nu contribuie cu nimic la progres.
Profesor: Este un exemplu prost. În America se
face o pledoarie pentru dezvoltarea tuturor științelor
și crearea unor mari cohorte de mari științifici care știu
totul, dar nu pricep nimic.
S.M.: Este țara care dă cele mai multe premii
Nobel, țara către care se îndreaptă cei mai buni tineri ai
noștri și se grăbesc să ajungă la universitățile de acolo.
Dacă o batjocoriți în acest fel, nu cunoașteți realitatea.
Elev: Tinerii nu se grăbesc să ajungă în universitățile
americane, se grăbesc să ajungă în societatea americană,
909

Solomon Marcus

care promovează valorile adevărate, oferindu‑le bani.
Profesor: Totul se face pentru bani. Se duc acolo ca
să câștige mai mulți bani.
S.M.: Vă înșelați. Eu am contact cu sute de tineri.
Nu neg că există și unii din această categorie, dar
motivațiile celor câteva sute de matematicieni români
care lucrează în universități americane și canadiene
sunt de ordin spiritual, intelectual. Unii s‑au mutat în
universitățile din Europa Occidentală, dar sunt oameni
pentru care nu banul contează în primul rând. Dacă eu
am această mentalitate la vârsta de aproape 90 de ani, o
am tocmai pentru că trăiesc în acest mediu de sute și mii
de tineri care știu să dea vieții o rațiune nobilă.
Elev: Raportat la timpul prezent, nu putem spune că
toți se duc din motive spirituale, ci din motive financiare.
S.M.: Există o stratificare, evident. Școala, așa cum
este organizată, înnăbușă dorința firească a copilului de a
pune întrebări.
Profesor: Citeam o carte despre un studiu asupra
învățământului din vreo șase state ale lumii, care spunea
foarte clar că școala, în forma actuală, în scurt timp va
dispărea. Toți copiii vor fi educați de corporații, de
structuri financiar‑bancare puternice, care vor dicta
ce vor de la acești copilași. Școala cu săli de clasă și
profesori nu va mai fi de actualitate.

910

Dezmeticindu-ne

S.M.: Acesta este un posibil scenariu al viitorului,
dar noi discutăm acum de situația actuală.
Profesoară: Școala trebuie să promoveze cu
precădere aceste două valori ale societății contemporane,
utilitatea și eficacitatea, sau trebuie să educe și alte
valori?
Profesor: Școala ar trebui să dea un concept despre
lume și viață a oamenilor cu care ei să se poată descurca.
Liceul clasic avea toate valorile. Relativ la ce a spus
elevul nostru și a spus foarte bine, la noi, și tineretul, și
cei mai în vârstă caută calea de mijloc. Oricum am lua‑o,
societatea americană îți dă alt context de viață și alt
context de afirmare. Sigur că dacă societatea îi dă voie
să trăiască cum trebuie, el poate să și dezvolte valorile.
El este și practician al vieții, până la valori trebuie să
trăiască.
S.M.: Noi escamotăm aici esențialul. Problema
este ce motivație are copilul să învețe când vine la
școală. Singura învățare de succes este învățarea făcută
cu plăcere. Învățarea cere un efort și ca să accepți acest
efort trebuie să primești o recompensă sufletească, să‑ți
placă ceea ce ți se propune să înveți.
Elev: Dacă mie îmi place ceea ce fac și societatea
nu mă promovează și nu mă ajută, nu‑mi oferă un
statut, un loc de muncă, nu voi putea niciodată să fac
performanță. Nu există performanță fără susținere.
911

Solomon Marcus

S.M.: Vorbiți din perspectiva unei persoane care
n‑a descoperit niciodată plăcerea unei anumite ocupații.
Într‑o educație ideală, ar trebui ca un tânăr să descopere,
să identifice până la vârsta de 14 ani ce îi face plăcere să
facă.
Elev: Eu vreau să studiez economia. Dacă nu mi
se oferă școli, profesori, cum pot eu să mă afirm în acest
domeniu?
S.M.: Sunt o serie de fundații prin care toți tinerii
noștri care au iubit să facă cercetare au reușit să ajungă
în universități de prestigiu.
Elev: Spuneți că avem nevoie de un sistem care să
facă performanță, dar și că ar trebui să plecăm afară.
S.M.: Această poveste cu cel care rămâne în
țară și cel care pleacă nu mai funcționează. Am avut
recent al șaptelea Congres al matematicienilor români
de pretutindeni, unde au venit, pe lângă cei din țară, și
vreo două sute de matematicieni de la mari universităși
străine. În momentul de față contează mult mai puțin
decât altă dată locul geografic în care te afli. Avem un
matematician care lucrează în Noua Zeelandă și care
conduce doctoranzi la noi, vine periodic în România. E
o colaborare strânsă. Astăzi poți fi mai legat de România
dacă trăiești geografic în altă parte, decât unii care trăiesc
geografic în România și de fapt trăiesc mult mai puțin
realitățile românești.
912

Dezmeticindu-ne

Profesoară: Școala ar trebui să‑i facă pe elevi să‑și
dea seama ce le place și să‑i îndemne să facă bine ce
le place. Cei mai nefericiți oameni de pe lume sunt cei
care toată viața fac ceva ce nu le place. Faptul că trebuie
neapărat să pleci ca să te realizezi în ceea ce‑ți place, eu
zic că este total fals chiar în România zilelor noastre. Eu
am văzut o promoție de elevi care au terminat în anul
2003, care sunt toți în România, toți au niște locuri de
muncă foarte bine plătite. Dar, atenție, din clasa aceea
n‑a fost niciun absolvent al clasei a XII‑a care să fi avut
media generală sub nouă. Toți au făcut facultăți de top
aici la noi. Dacă ești foarte bun și dacă îți place ce faci,
poți și în România să te realizezi foarte bine.
S.M.: Plăcerea nu e decât uneori ceva cu care te
naști. Ea se educă, și tocmai acest lucru noi nu îl facem.
Ascultați acest citat din Spiru Haret, care se referă la un
lucru pe care nici azi nu l‑am realizat. Pentru că se afla
copleșit de câteva personalități care nu acordau științelor
naturii aceeași importanță ca aceea pe care o acordau
științelor umaniste, spune următorul lucru: “Vorbesc
despre aceste două învățăminte ca instrumente de
cugetare, ca gimnastică intelectuală (valoarea formativă
a gândirii matematice). Și în această privință încă o dată
raționamentul științific nu numai că nu cedează oricărui
alt raționament, dar desigur că este mult superior.”

913

