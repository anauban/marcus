Normă, normalitate, azi
Anchetă Viața Românească, nr. 9‑10, 2011
Solomon Marcus:
Rădăcina principală a răului se află în sistemul
educațional
1. Este lumea aceasta în care trăim o lume normală
(la cap)?
Întrebarea nu este clară. Normalitatea e ceva foarte
vag, prea impregnat de elemente subiective, termenul e
foarte ambiguu sub toate aspectele: social, psihologic,
medical. Pentru unii, e normal ceea ce exprimă starea
majorității populației. Pentru alții, e normal ceea ce
era între cele două războaie mondiale etc. Se consideră
de multe ori normal ceea ce exprimă starea de mijloc:
nici prea prost, nici prea deștept; nici prea sărac,
nici prea bogat; nici prea ignorant, nici prea erudit,
etc. Putem continua la infinit. Tot aşa se vehiculează
uneori expresii la fel de vagi, ca: “omul de pe stradă”,
“cetăţeanul obişnuit”. Observaţi că în medicină se evită
definirea directă a stării de sănătate; se indică intervalul
de normalitate al unor parametri, cum ar fi pulsul,
677

Solomon Marcus

temperatura, glicemia, tensiunea, dar nu toate aspectele
psiho‑somatice pot fi cuantificate şi de aceea definirea
stării generale de sănătate este dificilă. Se poate muri cu
valori bune ale multor parametri ai stării de sănătate.
Care este atunci strategia corectă? În medicină,
se studiază bolile, nu starea de sănătate. O educaţie
eficientă a gândirii logice se face prin explicarea
greşelilor de logică cele mai frecvente; la fel, pentru
greşelile de ortografie şi de gramatică. Se stabilesc
sancţiuni, care merg de la contravenţii până la pedepse
cu închisoarea, în cazul infracţiunilor comise în
trafic, de şoferi sau de pietoni. Toate domeniile care
sunt guvernate de reguli ajung astfel la o idee de
corectitudine, de normalitate, dar ajung la ele pe cale
indirectă, prin intermediul observării abaterilor de la
reguli. Greşeala este mai clară, mai uşor observată şi
evaluată decât corectitudinea.
Apar situaţii care scapă regulilor. Îmi aduc aminte
că, atunci când am scris prima mea carte pentru o editură
de la Paris, primeam frecvent observaţii de genul, „On
ne dit pas ça en français”, fără să fi încălcat o regulă
precisă, explicit formulată în cărţile de specialitate.
Dincolo de regulile scrise, explicite, sunt regulile
practicate din instinct, de către vorbitorii nativi ai limbii
franceze. Dar şi cu limba română e la fel. Câţi îşi dau
seama că, în numele unei reguli nescrise, spunem “19
678

Dezmeticindu-ne

mere”, dar “20 de mere”? De ce acest de lipseşte în
primul caz, nu şi în al doilea?
În domeniile în care greşeala poate avea consecinţe
grave, de vătămare corporală, se elaborează regulamente
şi legi care prevăd sancţiuni penale. Dar şi aici sunt
multe situaţii în care luarea unei decizii poate fi
tergiversată multă vreme.
Încercarea de a se instaura o normă în domeniul
folosirii limbii române nu a reuşit decât parţial, după
cum bine se ştie. La fel, încercarea de a se institui
o normă de comportament social prin faimoasa
corectitudine politică a dus uneori la situaţii atât de
inacceptabile, încât ideea respectivă a fost compromisă,
în ciuda bunelor intenţii cu care fusese introdusă.
O situaţie deosebit de gravă are loc în domeniul
educaţiei. Încercarea de a se introduce norme de evaluare
a prestaţiei elevilor, prin alcătuirea unor baremuri, mi
se pare un eşec de proporţii. Am scris de mai multe
ori despre această problemă, a se vedea, de exemplu,
cartea mea Răni deschise (Ed. Spandugino, 2011).
Încercarea de a se introduce norme pentru evaluarea
rezultatelor obţinute în cercetare a dus la crearea unui
nou domeniu în ştiinţă, scientometria. Comunităţile
profesionale ale diferitelor domenii de cercetare discută
relevanța normelor respective, într‑un spirit critic foarte
pronunţat. Se propun mereu ameliorări, modificări de
679

Solomon Marcus

tot felul. Minţile cele mai lucide au ajuns la concluzia că
normele, oricât ar fi ele ameliorate, nu vor putea singure
să rezolve problema evaluării cercetării; suplimentarea
lor cu intervenţia directă a factorului uman este
inevitabilă. Aceste lucruri au consecinţe asupra criteriilor
de promovare pe diverse posturi, în învăţământul
universitar.
S‑a discutat mult despre norma diferitelor perioade
de dezvoltare a literaturii, dar şi aici au apărut serioase
divergenţe.
În consecinţă: normele singure sunt de cele mai
multe ori fie neclare, fie neputincioase, fie cu efecte
negative. De la caz la caz, trebuie să decidem cu ce le
suplimentăm.
2. Suntem noi (sunt eu, care întreb) normal(i) şi
lumea a‑normală, sau invers?
Citind despre modul în care oamenii din diferite
perioade ale istoriei au perceput vremea în care au trăit,
ai impresia că ei au avut, mai toţi, impresia că trăiesc
vremuri excepţionale. Luaţi ziarele romaneşti de la
1900 (moment pe care acum îl percepem ca pe unul
relativ liniştit) şi vă veţi convinge. Asta nu înseamnă că
nu veţi găsi în ziarele respective şi cronica mondenă,
tot felul de întâmplări anodine. Dar acum nu e la fel?
Mai toate televiziunile şi ziarele prezintă atât crime
680

Dezmeticindu-ne

dintre cele mai oribile, cât şi parada modei şi ultimele
concursuri de gastronomie, atât proporţiile sărăciei, cât şi
aventurile excentrice ale celor mai bogaţi. Sunt, desigur,
deosebiri mari între mentalitatea de ieri şi cea de azi,
în primul rând la nivelul paradigmelor dominante: am
trecut de la energie la informaţie, de la maşina cu aburi
la calculatorul electronic, altele sunt azi metaforele
dominante, de la comunicarea cu vecinii am ajuns la
comunicarea cu lumea; la acest nivel global normele
culturale s‑au schimbat. Oamenii gravitează între doi
poli: urgenţa unor probleme grave şi cufundarea în
inerţie şi lene.
3. Cât din normalitatea noastră originală se
datorează experimentului comunist, cât postcomunismului bezmetic în tranzitivitatea lui nenorocită?
În ciuda răului imens pe care l‑a produs societătii
românesti, ar fi ultrasimplificator să vedem în comunism
unica sursă a răului. În multe privinţe, comunismul nu
a făcut decât să potenţeze neajunsuri care vin dintr‑o
istorie mai îndepărtată. De exemplu, observaţiile critice
ale lui Dimitrie Cantemir, motivaţiile acţiunii critice a
lui Titu Maiorescu şi cele ale acţiunii lui Spiru Haret nu
şi‑au pierdut nici azi actualitatea. Ei au pus degetul pe
răni care nu s‑au vindecat nici până azi, şi‑au schimbat
numai înfăţişarea. Noi trăim şi acum având în faţă
681

Solomon Marcus

dezideratele formulate în proiectul Junimii maioresciene.
4. Este „normalitatea” socială doar un concept
irealizabil, idealizant, o iluzie naivă, sau e o stare de
fapt, reală, conform(ă) unor norme/principii de viaţă
acumulate, verificate, omologate de‑a lungul timpului?
Şi încă anormalitatea ar fi doar un derapaj temporar
de la normalitate – cronicizat, permanentizat din lipsa
normei, a principiului?
Dacă vrem să identificăm sursa fenomenelor
patologice din societatea românească, atunci situaţia
este foarte clară, pentru cei care au voinţa s‑o
urmărească: rădăcina principală a răului se află în
sistemul educaţional, în programele şi manualele
şcolare, dar mai ales în relaţia de cele mai multe ori
greşită dintre profesori şi elevi, dintre părinţi şi copii şi
în incapacitatea sistemului educaţional de a transforma
mass media şi internetul în aliaţii săi. Am scris foarte
mult pe această temă, cartea pe care am evocat‑o mai sus
îi este dedicată. Trăim într‑o societate bolnavă în multe
privinţe, chiar dacă în cadrul ei există şi zone de sănătate
şi plenitudine.
5. Într‑un dialog cu Dorin Tudoran pe scena
Ateneului Român, Nicolae Manolescu definea
normalitatea prin norma însăşi: omul potrivit la locul
682

Dezmeticindu-ne

potrivit. Dumneavoastră aveţi o definiţie/viziune
personală a normalităţii?
Omul potrivit la locul potrivit sună frumos, am
folosit şi eu acest slogan care, însă, azi, ascunde multe
capcane. De exemplu, ieri putea să însemne să nu pui
pe cineva să lucreze într‑un domeniu diferit de cel
pentru care s‑a pregătit şi e atestat de o diplomă. Numai
că mutaţiile care au avut loc în domeniul profesiilor,
ca urmare a metabolismului diferitelor discipline, au
deplasat accentul de la cunoştinţe la capacităţi de gândire
şi comportament. Cine ar putea să conteste creaţia în
domeniul gândirii muzicale a lui Dan Tudor Vuza, sau
creaţia literară a lui Bogdan Suceavă, sau prestaţia în
domeniul finanţelor a lui Cristian Sima, sau prestaţia ca
jurnalist a lui Tudor Călin Zarojanu, numai pentru faptul
că diploma lor universitară este de matematician?
Azi, este frecvent şi devine aproape un fenomen
sistematic faptul că oamenii ajung să dea un randament
foarte bun în alte domenii decât acelea pentru care au
ei diploma. Trebuie, deci, să fim atenţi la felul în care îl
folosim pe potrivit.
6. Prezentarea/relevarea/descrierea a‑normalităţii
corpului social, în mass‑media, în literatură şi
artă, conduc la abolirea acesteia şi la instaurarea
normalităţii, sau dimpotrivă, la perpetuarea şi
683

Solomon Marcus

potenţarea „relelor moravuri”? Sau, nici‑nici?
Atingeţi aici o problemă foarte gravă. Un singur
exemplu: multe filme, inclusiv cele pentru copiii până la
12 ani, sunt mostre de anti‑educaţie, ele seamănă foarte
mult cu ceea ce aflăm din jurnalele de ştiri. Îmi aduc
aminte de un film, Tess, în care o femeie îşi omoară
soţul; filmul fusese dat chiar în ziua în care la ştiri aflam
despre un caz similar, în România zilelor noastre.
7. Mai e de sperat în norma/normalitatea zilei de
mâine? Dacă da, în ce rezidă, după dumneavoastră,
această speranţă?
Speranţa este în schimbarea din rădăcini a
sistemului educaţional.

684

