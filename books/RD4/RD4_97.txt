N‑am avut dreptul să‑mi bat joc de viaţă!
Interviu realizat de Oana Olariu,
Opinia Veche, 2011, Iași
„Nu am niciodată un program fix. Uneori lucrez
în miezul nopţii, alteori dimineaţa. Niciodată nu ştiu
exact ce şi când voi face”, îmi spune. Fiindcă nu vrea
să‑şi chircească viaţa în tipare. „Acum am stare şi timp
să stăm de vorbă”, îl aud. E trecut de miezul nopţii.
Matematicianul Solomon Marcus jubilează. Degeaba îl
întreb de Diogene. N‑a stat niciodată în butoiul lui. „Pe
mine nu m‑a vizitat niciodată această stare! Cinismul
este situaţia în care răutatea umană se aliază cu ironia.
Eu mereu am iubit lumea!” Are 87 de ani. Şi în război,
şi în comunism a iubit această lume, în care a putut să
afle minuni precum poezia sau matematica. Îmi vorbeşte
despre cărţile lui, publicate în Franţa, SUA, Rusia,
Cehia, Italia sau Spania încă de dinainte de ’89. Nu
academicianul Solomon Marcus îmi vorbeşte, ci omul
fascinat de universul în care trăieşte.
S.M.: Vrei să‑ţi spun care sunt tendinţele
anti‑junimiste pe care le‑am observat în societatea de azi?
1004

Dezmeticindu-ne

O.O.: Da.
S.M.: Ar trebui poate să încep cu modul în care
educația încalcă principiul junimist al unei relații
echilibrate între tradiție și înnoire. Programele de
învățământ sunt încremenite într‑un proiect acoperit
de rugină, deocamdată se arată incapabile de înnoire.
O altă încălcare gravă a programului junimist este
tendinţa de a diaboliza Internetul. Fiindcă foarte mulţi
intelectuali umanişti consideră că Internetul este un
pericol pentru cultură şi chiar s‑a lansat această expresie:
„dependenţa de Internet”, astfel încât să se asocieze
Internetul cu drogurile. Dar mie, personal, Internetul
mi‑a schimbat viaţa! În bine! Înainte, când mă apucam
de‑o problemă, totul îmi lua enorm de mult timp. În
momentul de faţă, introduci cuvintele cheie într‑un
motor de căutare şi‑ţi apar, dintr‑odată, sute de articole.
Trebuie să ştii doar să te orientezi, să decelezi. De
exemplu, îţi spun!, Wikipedia este la momentul de faţă
net superioară tuturor enciclopediilor tradiţionale, de la
marea Enciclopedie Britanică, la Larousse‑ul francez. De
departe e cea mai bună! Şi se evaluează că în decurs de
cinci, zece ani, toate bibliotecile lumii vor fi transferate
pe Internet. Aceasta e lumea în care veţi trăi. Iar în
condiţiile acestea, îţi dai seama ce crimă este să nu faci
educaţie tinerilor, în tainele Internetului?! Se navighează
dintr‑un site în altul! Păi, îţi dai seama ce aventură a
1005

Solomon Marcus

cunoaşterii înseamnă asta?!
Internetul este azi un mijloc esențial de conectare la
universalitate, de conectare cu lumea, deci de realizare
a comandamentului junimist al unei relații corecte,
echilibrate între local și universal.
O.O.: Poţi vedea orice cu ochii minţii. V‑ar fi plăcut
vreodată să nu aveţi corp?
S.M.: O, nu! Nu numai că nu m‑a vizitat această
idee, dar impresia mea puternică este că tot ceea ce fac
pe plan intelectual, o fac cu tot corpul! Nici măcar ideea
asta, că „simt cu inima, gândesc cu capul” eu, personal,
n‑am trăit‑o! Impresia mea e că eu trăiesc intelectual
cu tot corpul! Cred că secretul longevităţii mele este
faptul c‑am avut şansa ca obligaţiile mele profesionale
să fie ceea ce am iubit. Totul am făcut cu o plăcere
extraordinară! De aceea, mie mi se pare că una dintre
marile probleme ale omenirii şi simptomul eşecului
sistemului educaţional global constau în faptul că cei
mai mulţi oameni obosesc până la 60 de ani, când simt
nevoia să se pensioneze. Lucrul ăsta mă îngrozeşte. Stau
şi mă gândesc: dacă eu m‑aş fi oprit la 60 de ani? Păi,
exact după vârsta asta m‑au vizitat cele mai interesante
idei! Cred că ceea ce am publicat în ultimii 20 de ani este
pentru mine mult mai semnificativ decât ceeea ce‑am
publicat în tinereţe. Într‑un articol, Noica înlocuieşte
apoftegma „viaţa este o pregătire pentru moarte” cu
1006

Dezmeticindu-ne

„viaţa este o pregătire pentru bătrâneţe.” La fel cred şi
eu! Că la bătrâneţe dai ce ai mai bun!
Toată viaţa am trăit o succesiune de miraje
O.O.: V‑aţi simţit vreodată disociat?
S.M.: Foarte tare. Am trăit experienţa aceasta foarte
mult timp. Pentru că am trecut şi prin război, şi prin
comunism. Prima dictatură din România s‑a instaurat în
1938, când aveam 13 ani. Iar de‑atunci, tot în dictatură
am trăit, până în anii ’90, când aveam 65 de ani. De
asta a trebuit să învăţ mereu cum să trăiesc pe două
planuri. Lucrul ăsta te falsifică, orice s‑ar spune! Adică
noi credeam că dacă îl ironizăm pe Ceauşescu în viaţa
noastră personală, prin asta ne păstrăm autenticitatea.
Ei bine, ne‑am înşelat! Faptul că a trebuit atât de mult
să ne prefacem ne‑a marcat negativ! După înlăturarea
lui Ceauşescu eram pătrunşi de această obsesie – că
nimeni nu spune ceea ce crede cu adevărat. Eram prins
de o suspiciune atât de profundă, încât vedeam de la
fereastra casei mele anumite demonstraţii, dar ca să mă
lămuresc în legătura cu semnificaţia a ceea ce vedeam
cu ochii mei, ascultam Europa Liberă. Nu mai vorbesc
despre suspiciuni la nivel personal. Fiindcă mereu exista
un gând de felul: omul ăsta nu cumva e un agent al
Securităţii? Iar această suspiciune intimă ne‑a îmbolnăvit.
1007

Solomon Marcus

O.O.: Aţi putut călători totuşi în timpul
comunismului.
S.M.: Să spun cum a fost: în 1967 mi s‑a publicat
în Franţa o carte. Iar ei m‑au invitat pentru lansare. Ca
să pot pleca, a trebuit să pun la bătaie artileria grea.
Câţiva dintre profesorii mei, precum Moisil, care
erau interlocutori valabili pentru puterea politică, au
intervenit la Comitetul Central. Asta e cel mai important,
c‑am avut această interfaţă! Dacă n‑aş fi avut şansa
ca profesorii mei să aibă această credibilitate şi ei
să‑şi dorească să facă ceva pentru mine, eram pierdut.
Datorită lor, la 42 de ani, am primit dreptul de călătorie
la Paris pentru două săptămâni. 
O.O.: Vă mai amintiţi primul Paris?
S.M.: Prima dată când am sosit, pur şi simplu
umblam pe stradă de parcă eram beat. Mergeam şi
vedeam imaginile din manualul meu de franceză. Mă
simţeam ca‑n vis. Această fascinaţie a Parisului s‑a
destrămat şi‑a fost înlocuită de un alt miraj: al Statelor
Unite. Toată viaţa am trăit o succesiune de miraje. De
exemplu, în copilărie, în urbea mea băcăuană, visul
meu era să văd Bucureştiul. Stăteam acasă şi citeam, în
dreptul cinematografelor, numele filmelor care rulează
şi observam că revenea mereu un cuvânt. Mi‑am spus,
„măi, ăsta trebuie să fie un film extraordinar, că rulează
la şapte cinematografe”. Filmul se chema „Relache”. Era
1008

Dezmeticindu-ne

cuvântul franţuzesc care înseamnă că‑n ziua respectivă
e pauză. Când am învăţat franceză am aflat că ceea ce
eu credeam că este extraordinar, de fapt, nu era nimic.
Am avut mereu câte o fascinaţie. Prima dată a fost a
Bucureştiului. Apoi a Parisului. Apoi a Americii. Apoi
a Orientului Îndepărtat. Am ajuns şi în Alaska, în Noua
Zeelandă, în Japonia. Până la urmă, fascinaţia mea
geografică s‑a consumat. Mi‑am dat seama că e la fel de
interesant să mă întorc în Bacău. La mine a fost mereu
foamea de a cunoaşte lumea. Dar mi‑am dat seama
că rezerva cea mai mare de diversitate e în peisajul
intelectual. Şi‑am descoperit adevărate minuni revelate
de ştiinţă! Iar revelaţiile acestea continuă mereu! Chiar
anul acesta am descoperit noi aspecte ale matematicilor
ne‑euclidiene, noi legături între artă şi ştiinţă! Îmi dau
seama că ăsta e adevăratul infinit! 
„Marcus s‑a lăsat de analiză matematică, un lucru
serios şi s‑a apucat de trăsnaia asta!”
O.O.: A început să vă curgă timpul mai tihnit?
S.M.: Nu! Am foarte multe de făcut! Mereu mă
pregătesc să intru în rolul pe care îl voi interpreta
în etapa următoare. De pildă, acum, pentru mine,
problema este să intru în atmosfera lui Ion Barbu,
care e matematicianul Dan Barbilian, fostul meu
1009

Solomon Marcus

profesor. Trebuie să ţin o expunere despre el. Dar
cele mai importante roluri sunt cele din activitatea
mea de cercetare. Trebuie să public articole – şi mai
ales în limba engleză. Pentru că ceea ce este expresia
naţionalului se validează acum la scară planetară. Chiar
cercetarea însăşi se face prin interacţiune cu întreaga
planetă. Unii îmi spun: „Ce vrei, domnule, să scriu în
engleză despre Costache Conachi?” Păi, tocmai ăsta este
pariul: să aduci anumite aspecte locale într‑o perspectivă
care să le facă interesante la nivel internaţional.
Personalităţile dintr‑un anumit domeniu de cunoaştere
nu sunt niciodată strict locale. Poţi să fii sigur că toate
culturile au câte un Ivireanu. Fiindcă există o structură
istorică analogă, comună. 
O.O.: Când aţi ajuns prima dată în Franţa, aţi simţit
acest „comun” istoric?
S.M.: Am ajuns acolo fiindcă ei mi‑au publicat în
limba franceză o carte despre lingvistica matematică, pe
care eu am publicat‑o în română în ’63, prima ediţie, în
’66 a doua ediţie. Era perioada de pionierat a lingvisticii
matematice. Atunci am simţit că e una dintre primele
mele victorii intelectuale. Fiindcă am câştigat pariul de
care colegii mei români râdeau. Impresia generală a fost:
„Uite, măi, Marcus s‑a lăsat de analiză matematică, un
lucru serios şi s‑a apucat de trăsnaia asta!”. 

1010

Dezmeticindu-ne

O.O.: V‑aţi simţit exclus din propria istorie
cotidiană?
S.M.: Am avut atunci un mare noroc. Singurul
motiv pentru care nu m‑au exclus din comunitatea
lor a fost faptul că îmi adusesem deja contribuţiile
mele în matematică pură, aşa că nu mă mai puteau
contesta ca matematician. Dacă nu aveam această
zestre achiziţionată anterior, eu riscam să fiu dat afară
şi din facultate. M‑au susţinut nişte mari vizionari:
Grigore Moisil, matematicianul şi Alexandru Rosetti,
lingvistul. Asta a fost şansa mea! Că am avut sprijinul
acestor oameni! La un anumit nivel, eram respins de
toţi. Matematicienii nu înţelegeau nimic, iar lingviştii
spuneau că fac matematică în haină lingvistică. Dar
Moisil spunea aşa: lucrurile noi încep ca un paradox,
continuă ca o rutină şi se termină ca o prejudecată.
O.O.: Ştiaţi ce faceţi când aţi ales să daţi examenul
la Facultatea de Matematică?
S.M.: Nu. Faptul că am trăit adolescenţa sub
această teamă, că nu ştiam dacă voi supravieţui sau
nu, m‑a făcut să nu fiu sigur dacă voi ajunge ziua de
mâine. N‑a rămas loc pentru preocupări de viitor.
Obsesia supravieţuirii m‑a confiscat cu totul. Tot ce
am putut să fac a fost să‑mi dezvolt un fel de carpe
diem comprehensiv, să mă bucur de lumea deschisă
prin cărţi. Dar lumea din jur mă obliga să ţin totuşi
1011

Solomon Marcus

seama de ea. M‑am trezit transportat la Detaşamentul
doi Grinzi Beton Doaga‑Putna. Acolo căram grinzi de
beton pe umăr. Nu puteam să mă refugiez cu totul în
cărţi. Amândouă lumile erau la fel de pregnante. Atunci,
în adolescenţă am deprins sensul gravităţii vieţii. În
momentul în care regele a citit mesajul său la 23 august,
mi‑am dat seama deodată că ies din această obsesie a
supravieţuirii. Eu n‑am mai avut dreptul să‑mi bat joc de
viaţă! Am ştiut atunci că, dacă mi s‑a dat dreptul la viaţă,
sunt responsabil pentru ceea ce fac cu ea! Totul în mine
e marcat de acest sentiment al gravităţii vieţii umane.
Trebuie să valorifici fiecare secundă! Când văd o coadă,
o linie de aşteptare, cum era pe vremea lui Ceauşescu,
când se stătea şi ore ca să cumperi un pachet de unt,
dar nu văd pe nimeni cu o carte în mână, citind, pur şi
simplu sunt dezolat. Mă întreb, cum, domnule, pentru
oamenii aceştia, timpul nu are nici o valoare?! În loc
s‑aştepte, ar putea să afle ceva!
Am trăit în două lumi diferite, concomitent
O.O.: Vi se spunea des în copilărie să staţi locului?
S.M.: Eu m‑am aflat într‑o situaţie specială, fiindcă
am fost ultimul, al optulea copil la părinţi. Doi dintre
fraţi muriseră în timpul Primului Război Mondial şi nu
îmi mai dădeau părinţii mare atenţie. Că eram prea mulţi.
1012

Dezmeticindu-ne

Mai degrabă, eram în grija unui frate mai mare decât
mine cu 10 ani, care îmi aducea cărţi.
O.O.: Reuşea fratele dumneavoastră să vă explice
de ce vă strigau alţi copii: „Mă,  jidane, de ce l‑ai omorât
pe Hristos”?
S.M.: Într‑adevăr, timp de patru ani a fost o
legislaţie rasistă. Trebuia să port steaua lui David
pe mânecă sau bandelora galbenă şi eram considerat
cetăţean de categoria a doua. Nazismul şi discriminarea
rasială au fost active în perioada adolescenţei mele, a
perioadei de maximă sensibilitate şi maximă rezonanţă
cu lumea. Am studiat mult despre timp. Am scris şi o
carte, aşa că pot spune, de pildă, că pentru o persoană
care trăieşte 75 de ani, trăirile lui psihologice de până
la vârsta de 10 ani reprezintă mai mult de jumătate din
întreaga lui viaţă. Ceea ce se întâmplă în copilărie şi
adolescenţă te marchează pentru tot restul vieţii.
O.O.: Cum vă apăraţi de această lume în care
oamenii se purtau urât?
S.M.: Nu prea puteai să te aperi. N‑aveai cum.
Fiindcă erai, hai să spunem eufemistic, un cetăţean
de categoria a doua. Dar de multe ori erai chiar un
non‑cetăţean. Aşa că mă refugiam în cărţi şi‑n cultură.
De pildă, în momentul de maximă duritate, în 1940,
când au venit legionarii la putere, aveam 15 ani. Nu
mai aveam dreptul să frecventez nici o şcoală. Purtam o
1013

Solomon Marcus

banderolă galbenă, ca lumea să ştie că eram de a doua
categorie. Dar acelaşi moment, 1940, a fost pentru mine
momentul în care, prin lecturi, am descoperit marile
minuni ale poeziei. Simţeam nevoia să găsesc şi pe alţii
care să simtă lucrurile astea! Dar mi‑am dat seama că
oamenii nu prea rezonau cu ele. Nu prea aveam cu cine
să împărtăşesc aceste descoperiri... De aceea vă spun, am
trăit atunci în două lumi diferite, concomitent! 
Infinitul este obiectul de studiu al matematicii și
atributul lui Dumnezeu
O.O.: În copilărie scriaţi, la sfârşitul caietelor, şiruri
interminabile de cifre. Aţi simţit vreodată sacralitatea
numărului?
S.M.: Ceea ce mă fascina, de mic copil, era
infinitul: faptul că oricând puteai găsi un număr mai
mare, iar numerele nu se termină niciodată. Eram foarte
impresionat că anumite lucruri nu au început şi sfârşit.
Iar în poezie, foarte puternic m‑au impresionat două
aspecte: plasarea într‑un univers de ficţiune şi faptul că
în poezie nu mai funcţionează logica vieţii cotidiene, că
pot să apară paradoxuri. Abia mai târziu am înţeles că
în poezie apar diferite teme, motive şi proceduri care
se regăsesc şi în matematică. Dar nu în matematica
pe care o învăţam la şcoală şi care se predă şi acum!
1014

Dezmeticindu-ne

Aceea pur şi simplu îţi ascunde exact lucrurile cele
mai interesante! De asta am căpătat pentru toată viaţa
obsesia de a schimba educaţia. Mi‑am dat seama că
am fost o victimă a unei proaste educaţii şi că e foarte
important să contribui la schimbarea ei. Vorbele astea ale
lui Steve Jobs mi se par extraordinare: „Fiţi înfometaţi!
De cunoaştere.” Asta e esenţialul în educaţie: să creezi
foamea! Adică oamenii să simtă nevoia organică,
nesăţioasă de‑a primi cultură, nu să li se vâre pe gât
cultura. Şi în continuare: „Fiţi nebunatici! În imaginaţie.
Nu lăsaţi pe alţii să vă trăiască viaţa!” Fiindcă cei mai
mulţi oameni asta fac: trăiesc într‑o rutină aplicând
reguli pe care alţii le‑au găsit şi le aplică, uneori fără
măcar a le înţelege.
O.O.: Cum înţelegeţi divinitatea?
S.M.: Sigur, există cărţi ale unor mari oameni de
ştiinţă, de pildă este cea a unui savant american, despre
cosmologie cuantică, care pretinde că prin metodele
calculului cuantic demonstrează existenţa lui Dumnezeu.
Dar şi în Evul Mediu, cei mai mari teologi catolici
îşi propuneau să demonstreze existenţa, unicitatea şi
atotputerea lui Dumnezeu prin mijloacele matematice
ale acelei perioade. Ceea ce Biserica actuală nici nu
concepe. Spune că Dumnezeu e rezultatul unei revelaţii
şi nu poţi demonstra matematic ceea ce se revelează. 
O.O.: Dumneavoastră ce credeţi?
1015

Solomon Marcus

S.M.: Am un deosebit respect pentru tot ceea ce
a marcat istoria omenirii. Am scris lucrări şi am primit
chiar şi un premiu din partea Fundaţiei Templeton
din Statele Unite (n.r.: fundaţie filantropică destinată
susţinerii cercetării ştiinţifice pe subiecte referitoare la
„Marile întrebări ale Omenirii” în legătură cu scopul
vieţii şi realitatea ultimă) pentru abordarea relaţiei
dintre religie şi ştiinţe. M‑am preocupat mereu de aceste
lucruri. Dar numai din perspectivele care mi s‑au părut
mie interesante. De exemplu, m‑a interesat să văd
aspectul metaforic al abordării divinităţii în diferite
religii. Fiindcă fiecare tradiţie recurge la alte metafore,
dar în absolut toate religiile, unul dintre atributele
divinităţii este tocmai infinitatea. Numai că infinitatea
este exact obiectul de studiu al matematicii. Uneori chiar
e definită matematica drept studiul infinitului. Mi‑am
dat seama că apare o problemă foarte interesantă: cum
matematica se ocupă cu clasificarea infinitului, existând
zeci de feluri de infinităţi, pentru mine a fost important
să văd la ce tip de infinitate matematică recurge fiecare
tradiţie. Deocamdată am făcut doar primii paşi, dar mă
fascinează această problemă.

1016

Dezmeticindu-ne

Voiam să înregistrez spectacolul lumii
O.O.: Ce‑aţi învăţat din copilărie?
S.M.: Copilăria m‑a antrenat într‑un exerciţiu care
mi‑a folosit toată viaţa – cum să alternez perioadele
de singurătate cu cele de ieşire în public. Adică am o
nevoie organică de singurătate. Pentru mine, „public”
înseamnă orice manifestare care mă pune în relaţie. Iar
ieşirea în public trebuie să fie precedată de o perioadă
de însingurare. Măcar o zi, dacă nu chiar două, trei, o
săptămână simt nevoia să rămân numai cu gândurile
mele. Dar tot organic simt şi nevoia de a spune şi altora,
de‑a împărtăşi altora o bucurie. Adică pentru că sunt plin
de bucurie că am înţeles nişte lucruri, simt nevoia să‑i
contaminez şi pe alţii de această fericire.
O.O.: N‑aţi fost judecat pentru nevoia de retragere?
S.M.: Evident că unii au observat că eu nu prea
veneam la ce se numeşte acum „mersul la club”. Astfel
de întâlniri nu le‑am frecventat deloc. Uneori îmi
reproşau că „bă, da’ tu nu mergi niciodată nicăieri!”. Dar
eu n‑am simţit niciodată această nevoie! După cum în
adolescenţă, la 15 ani, când alţi copii încep să citească
literatură poliţistă sau de aventuri, eu n‑am simţit deloc
nici această nevoie. Iar literatura aşa‑zisă pentru copii, de
exemplu, poveştile lui Ion Creangă, am sorbit‑o cu mare
plăcere, dar nu în copilărie, ci la maturitate! Când am
1017

Solomon Marcus

putut s‑o înţeleg altfel.
O.O.: Credeţi că oamenii înţeleg singurătatea
altora?
S.M.: Nu înţeleg! De exemplu, mama l‑a cunoscut
pe Bacovia. Locuia gard în gard cu el şi îmi povestea
cum îşi bateau joc copiii de el. Aici, eu am fost mai
norocos. Am participat la viaţa de pe uliţă. Ţineam chiar
un ziar al străzii, unde notam fiecare eveniment: cine
a câştigat la jocul de castane, la fugă, la întrecere. Iar
ziarul ăsta îl scriam cu creionul pe hârtie. Îl prindeam pe
peretele unei magazii ca să citească şi alţii. Iar la şcoală,
aveam obsesia catalogului. Visam în mod periodic că am
în mână catalogul clasei şi mă uitam în el, să văd ce note
are fiecare. Fiindcă îmi rămânea inaccesibil, ştii ce‑am
făcut? Un catalog propriu! Notam într‑un caiet toate
absenţele elevilor şi notele pe care le aflam! Uneori,
fiindcă profesorii mai uitau, veneau să mă întrebe pe
mine: „Ia spune, ce‑ai trecut acolo, la cutare?”. Şi
acum, când văd cataloagele în şcoli, am o tresărire.
Numai că vezi ce se întâmplă! Acum aflăm că elevii
fură cataloagele, pun note, şterg absenţe... Eu aveam o
obsesie a înregistrării evenimentelor despre care aflam,
voiam să înregistrez ceea ce văd în jur, spectacolul lumii.

1018

Dezmeticindu-ne

Publicul începe când te priveşti în oglindă
O.O.: V‑a ajutat această idee, a lumii ca spectacol,
să vă stabilizaţi în ciuda disocierii în care trăiaţi?
S.M.: Asta este ceea ce pur şi simplu m‑a confiscat.
A început prin a‑mi plăcea teatrul. Primul spectacol
l‑am văzut când aveam vreo 10 ani şi eram la un Cămin
Cultural. De fapt era o echipă de amatori care interpreta
„Omul care a văzut moartea” de Victor Eftimiu.
Posibilitatea aceasta de a trăi în altul m‑a confiscat.
Multă vreme căutam să nu‑mi scape nici un spectacol.
Îmi ziceam, „domnule, ce poate fi mai formidabil decât
să trăieşti într‑un altul?!”
O.O.: Aţi reuşit să trăiţi în alţii?
S.M.: Am descoperit că ceea ce face actorul se
regăseşte perfect şi în profesia de profesor. Profesor
fiind, pot să recuperez toate satisfacţiile pe care le
poate simţi un actor, fiindcă în esenţă, e acelaşi lucru.
Paralelismul între actul educaţional şi spectacol merge
până la cele mai mici detalii. Mai mult, am descoperit
că orice ieşire în public înseamnă exact asta: să trăieşti
într‑un rol, în altul, să creezi un spectacol. Iar public,
ştii ce‑nseamnă? Să mai existe lângă tine altcineva, fie
şi‑o singură persoană. Şi în familie poţi să fii în public,
fiindcă eşti văzut. Mă rog, soţia mea a murit de şase ani.
Dar cât am stat cu ea în casă, nu am trăit o viaţă privată.
1019

Solomon Marcus

Mă simţeam în public. Reflexul publicului începe când
simţi nevoia de a te privi în oglindă. Adică te interesează
cum te vor vedea alţii. 
O.O.: Nu vă obosesc rolurile?
S.M.: Nu, pentru că am momentele de retragere
în mine însumi. E esenţial să te retragi ca să poţi
intra înapoi în lume. E ca şi cum ai nevoie de un nou
combustibil, ca să poţi să arzi. Să simţi nevoia de a trăi
iarăşi ceva foarte intens, ca apoi să simţi că plesneşti de
bucurie şi să apară nevoia de‑a împărtăşi cu alţii ceea
ce‑ai acumulat şi‑ai trăit în singurătate. Este în asta o
putere extraordinară de‑a te împrospăta, de‑a nu te uza.
Faptul de a mă regăsi în singurătatea altcuiva mă
vindecă pe mine însumi de singurătate
O.O.: Mai aveţi puterea de‑a învăţa?
S.M.: Mai ales de la cei mai tineri. Anumiţi
discipoli mi‑au devenit la o anumită etapă de viaţă,
maeştri. Ştii ce se întâmplă? Nu mai există delimitări ale
timpului: trecut, prezent, viitor, tânăr, bătrân. Faptul că te
vezi pe aceeaşi lungime de undă cu cineva este mult mai
important decît diferenţa de vârstă.
O.O.: Aţi simţit că nu contează că sunteţi cu 200 de
ani mai tânăr decât autorii care vă plac?

1020

Dezmeticindu-ne

S.M.: Am avut uneori senzaţia, citind pe cei care
m‑au captivat, că eu trăiesc în ei. De exemplu, m‑am
regăsit foarte puternic în poeziile lui Baudelaire despre
pisică. Prima mea amintire, de la trei ani, de altfel, este
cu o fereastră unde am văzut o pisică. Sau în Rilke. Am
simţit mereu fascinaţia aceasta a eliberării prin alteritate.
Fiindcă autorii care m‑au captivat au exprimat elemente
foarte puternice din subconştientul meu, dar pe care
le cenzuram şi nu le permiteam să pătrundă în viaţa
publică.
O.O.: V‑au dat vreodată curaj să vă eliberaţi de
auto‑cenzură?
S.M.: Mi‑au dat senzaţia că uneori simţămintele
mele sunt convergente cu omenescul cel mai profund.
M‑am simţit validat. Când cineva exprimă cu atâta
putere o singurătate esenţială în care te regăseşti, apare
această minune: că singurătatea se transformă în opusul
ei. Faptul de a mă regăsi în singurătatea altcuiva mă
vindecă pe mine însumi de singurătate. E o mare bucurie
când apare această împărtăşire. Când am văzut că un
om poate trăi într‑un alt om, am spus: „Ah!, câtă nevoie
am şi eu de aşa ceva!” Am înţeles de ce sunt eu atât
de fascinat de metaforă. Fiindcă metafora înseamnă a
înţelege un lucru printr‑un alt lucru! 

1021

Solomon Marcus

Greşeala şi eşecul sunt inerente libertăţii
O.V.: Când nu vă simţiţi bine în propria piele,
aveţi nevoie de‑o listă albă, a celor care cred în
dumneavoastră?
S.M.: Se întâmplă de multe ori să fiu într‑o stare
negativă. În astfel de situaţii, terapia pe care mi‑o
administrez este cea de a mă gândi la oamenii care mi‑au
apreciat opera. De pildă, mă preocupă extraordinar să
nu înşel aşteptările oamenilor care cred în mine. Dacă
nu trăieşti cu această nevoie de a face mereu lucrurile
mai bine decât înainte, părerea mea e că eşti un om
terminat. Pentru mine, asta înseamnă să‑ţi păstrezi de
fiecare dată o capacitate emoţională ca şi cum ai fi mereu
la prima expunere, să ai mereu emoţii. În momentul în
care eşti sigur de tine şi nu mai ai emoţii, te afli într‑un
pericol mortal! Eu nu trăiesc din certitudini! Trăiesc
într‑o permanentă stare de îndoială şi curiozitate,
iar curiozităţile mele sunt mult mai multe decât
răspunsurile pe care sunt în stare să le dau. Nici nu am
nevoie de certitudini! Faptul că am această bucurie a
contemplării, că beneficiez de spectacole extraordinare
prin poezie, matematică, limbaj şi artă, spectacole pe
care le contemplu cu o bucurie extraordinară, asta îmi
dă un sens vieţii! Eu nu sunt capabil de certitudini! Eu
exist sub principiul lui Descartes. Mă îndoiesc, deci
1022

Dezmeticindu-ne

gândesc. Gândesc, deci exist. Dacă n‑aş simţi bucuria
contemplării spectacolului lumii, mi‑aş pierde sensul.
O.V.: Vă e frică să greşiţi?
S.M.: Nu! Eu cred că greşeala este o sursă de
creativitate! Să ne înţelegem: greşeala face parte
din natura omului! E preţul pe care‑l plătim pentru
posibilitatea creativităţii. Greşeala şi eşecul sunt inerente
libertăţii de care ai nevoie pentru a descoperi ceva,
pentru a nu încremeni.
O.V.: Dumneavoastră aţi creat prin eşec?
S.M.: O, sigur! Am avut nenumărate eşecuri. Ele
sunt mereu mai numeroase decât succesele. Atâta doar
că ele rămân de obicei private, personale, fiindcă ieşim
în public cu succesele. Eşecurile rămân în sertare. Nu
le arătăm. Eu, dacă ţi‑aş arăta laboratorul meu de lucru,
ai vedea zeci de caiete cu mâzgâleli. Am încercat fel
de fel de metode din care, de cele mai multe ori, nu s‑a
ales nimic. Pe unele caiete le‑am aruncat, dar pe cele
mai multe le păstrez în casă. Le răsfoiesc uneori ca să
mă regăsesc. Şi îmi dau seama că faţă de cât am publicat,
mâzgâlelile sunt de zece ori mai numeroase. Am totuşi
bănuiala că ele n‑au fost pierdere de vreme, că prin
exerciţiile acestea m‑am antrenat, m‑am şlefuit că să
ajung la altceva. Mi‑am ţinut mintea activă. Într‑adevăr,
de cele mai multe lucruri care ne trec prin gând, nu se
alege nimic palpabil. Dar ele ne formează.
1023

Solomon Marcus

O.V.: Ce proiecte imposibile aţi avut?
S.M.: O, a fost tot când eram copil şi ţineam ziarul
străzii. Mi‑a trecut atunci prin gând să merg mai departe,
să ţin un jurnal nu doar al străzii, ci al tuturor copiilor cu
care mă jucam. Adică îmi propusesem să‑i urmăresc pe
aceşti copii pe tot parcursul vieţii şi să ţin un jurnal al
evoluţiei lor. Dar fireşte c‑a fost imposibil, fiindcă s‑au
împrăştiat în toate colţurile lumii. 
Ne împrospătăm în măsura în care reuşim să ne copilărim
O.V.: V‑aţi gândit că trăiţi abstract?
S.M.: Pentru mine, exerciţiul întâlnirii cu oamenii,
pe care‑i privesc în ochi şi încerc să‑i înţeleg, mă apără
de‑a cădea în abstractizare. Îi urmăresc cu atenţie, în
special pe cei tineri. Am impresia că cei mai mulţi
tineri se pierd în lucruri anodine şi derizorii. Dar mă
suspectez că greşesc eu în judecata mea. De exemplu,
că nu au o foame de cultură, îmi este clar. Dar ce au ei
în loc de asta, nu‑mi este clar. Când eram eu adolescent,
dorinţa mea cea mai aprigă era să citesc poezie, teatru şi
filosofie. Mă tem că fiindcă ei nu au conştiinţa dramelor
majore care îi înconjoară, nu au nici năzuinţe majore.
O.V.: De fapt, vă impresionează oamenii sau ideile?
S.M.: Mă impresionează cele mai felurite lucruri.
Uite, de exemplu, am fost odată în vizită la Liceul
1024

Dezmeticindu-ne

German. Şi într‑o primă etapă m‑am întâlnit cu mulţi
copii foarte mici – clasa a IV‑a sau a V‑a. Când am
intrat în clasă, copiii erau aliniaţi într‑un front şi
m‑au întâmpinat cu următoarele cuvinte rostite în
cor: „Noi suntem copii cuminţi!” Am avut atunci o
emoţie extraordinară. M‑am gândit, „domnule, ce să
le zic eu lor?” Mi‑am dat seama atunci că mă aflu în
faţa unei situaţii umane fără precedent pentru mine.
E foarte important lucrul acesta: să nu mergi la sigur
niciodată, să nu crezi că, la experienţa ta, nimic nu te
mai surprinde, ci să ai o oarecare teamă. Trebuie să fie
totuşi un echilibru: să‑ţi fie teamă, dar să ai încredere în
capacitatea ta de a seduce. Fiindcă toată viaţa e o artă a
seducerii celuilalt, pe care niciodată nu‑l cunoşti întru
totul. Un actor, de pildă, ştie foarte bine când are sala
în mână şi când a căscat cineva. Atunci e cuprins de o
dezolare extraordinară. Pentru mine, când văd în sală pe
cineva căscând, este un moment pe care mi‑l depăşesc
foarte greu. Încerc să‑l înving, dar niciodată nu pot, cu
toate că ştiu că a căsca poate fi semnul nevoii de aer, de
oxigen, iar nu al plictiselii. Dar numai bănuiala că cineva
cască fiindcă tu nu l‑ai interesat te aruncă într‑o stare
negativă, pe care trebuie să o depăşeşti prin adaptare la
celălalt, prin cunoaştere spontană, neprevăzută în planul
tău de‑acasă.

1025

Solomon Marcus

O.V.: Istoria în care aţi trăit nu v‑a făcut să vă
naşteţi bătrân?
S.M.: Eu am impresia aceasta, că pe măsură
ce‑am înaintat în vârstă, am câştigat în tinereţe. Dar, ca
să spun drept, a fost un moment, pe la 14 ani, când am
avut deodată senzaţia că am ieşit din copilărie. Mi‑aduc
aminte că era într‑o toamnă şi a fost prima dată când
am purtat pantaloni lungi într‑un sezon în care încă era
cald. Acest detaliu, pe care l‑am memorat şi l‑am resimţit
atunci, a marcat trecerea. Nu ştiu cum, după aceea
s‑au schimbat şi anumite obligaţii sociale ale mele, ca
elev mai mare. Treptat, mi‑am dat seama că toată viaţa
m‑am alimentat cu ce e copil în mine. Asta am făcut tot
timpul, aşa că atunci când te trezeşti şi ţi se destramă
visele, încerci să recuperezi frânturi. Am încercat să‑mi
recuperez copilăria, starea de joc, de imaginaţie şi de
libertate. Cred că ne împrospătăm în măsura în care
reuşim să ne copilărim. 
O.V.: V‑a fost vreodată frică de întuneric?
S.M.: Dar şi‑acum mi‑e frică de întuneric! Lumina
este pentru mine cel mai important semn al vieţii şi‑al
lucidităţii. Există un banc, dar care e foarte profund: un
om şi‑a pierdut ceasul, noaptea şi‑l caută sub un felinar.
Vine un trecător şi‑l întreabă unde a pierdut ceasul. Omul
îi spune: „L‑am pierdut pe drum, până s‑ajung aici, la
felinar”. „Atunci de ce‑l cauţi aici, dacă l‑ai pierdut
1026

Dezmeticindu-ne

înainte s‑ajungi?”, îl întreabă trecătorul. „Fiindcă aici am
lumină”. Răspunsurile noastre fac lumină numai ca să
mai putem căuta în alte zece locuri, nu pentru a găsi ceva
şi să ne oprim. Conştiinţa ignoranţei noastre ne măreşte
capacitatea de curiozitate. Credinţa mea e că rămânem
vii numai în măsura în care ne putem pune întrebări.
Când viaţa devine prizoniera rutinei, eşti un om terminat!
Nu mă pot închipui fără nevoia de mâine
O.V.: De moarte vă e teamă?
S.M.: Da. Trebuie să recunosc că mi‑e tare frică de
moarte. Ştii cum funcţionează frica asta? Simt mereu că
am încă multe proiecte. Şi gândul că nu le‑aş putea duce
până la capăt mă înfricoşează. În felul acesta mă tem de
moarte.
O.V.: Imaginaţi‑vă c‑aţi trăit mult de tot, până când
v‑aţi împlinit toate proiectele. În momentul acela, aţi
muri liniştit?
S.M.: Dar nu pot să concep cum arată un om
viu care nu mai are nimic de realizat! Eu nu mă pot
reprezenta pe mine fără nevoia de proiecte, deci de
mâine. Nu, nu pot să‑mi imaginez lucrul ăsta. Pentru
mine, nevoia de mâine face parte din constituţia vieţii.
E parte organică a vieţii, să ai nevoie de mâine. Ştii
c‑aici funcţionează, la mine, o anumită psihologie. Deşi
1027

Solomon Marcus

se repetă mereu: „Omul este o fiinţă muritoare, Socrate
este om, deci Socrate este muritor” şi deşi văd în jurul
meu atâţia oameni care au murit, eu trăiesc cu iluzia că
o să fiu un contra‑exemplu. Ştiu, toată lumea o să spună:
ce‑i cu iluzia asta? E o prostie! Ei bine, mă hrănesc cu
această iluzie, care mă ajută să‑mi păstrez vitalitatea.
Îmi dă un echilibru existenţial, aşa cum îmi dă infinitul.
Să cred că după azi vine un mâine, pentru mine e aşa
cum ştiu că după 14 vine 15 şi tot aşa, chiar dacă nu pot
cuprinde tot şirul. În fiecare zi mă alimentez din gândul
acesta: ce voi face eu mâine, ce oameni mă aşteaptă
mâine... Pur şi simplu nu m‑am preocupat niciodată de
altceva.
O.V.: Nici în adolescenţă?
S.M.: Nu. Dintotdeauna, când un gând ca acesta
s‑a apropiat de mine, am încercat, aşa ca atunci când dai
cu mâna, că se‑apropie de tine o muscă, să‑l alung.  Iar
când eram mic, spectacolul înmormântării mă amuza. Să
ştii, copiii nu înţeleg moartea. În copilărie găseam ceva
amuzant în ritualuri. Dintotdeauna am avut acest defect:
să mă pufnească râsul în împrejurări sobre. Evit să merg
la înmormântări, fiindcă nu ştiu ce spiriduş se află în
mine, că‑n situaţii ca acestea îmi vin în minte tot felul de
lucruri nostime. De pildă, că bocitoarele au făcut grevă
fiindcă au fost scoase din nomenclatorul de profesii.

1028

EDITURA SPANDUGINO, 2014

