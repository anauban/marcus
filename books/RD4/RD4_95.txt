Solomon Marcus
despre secretele matematicii
Revista VIP, decembrie 2011
(un dialog devenit monolog)
Introducere din partea revistei VIP:
Matematica este o adevărată cale a aleșilor. Ne
începem, astfel, multe dintre gândurile noastre despre
această știință, pentru că matematica reprezintă o
perspectivă cu adevărat diferită asupra lumii. Militant
pentru înțelegerea și receptarea fără prejudecăți a
matematicii, Solomon Marcus nu contrazice această
mentalitate, însă mai spune că această știință poate fi
predată astfel încât să fie mai aproape de logică, dar și de
sufletul elevilor.
  Discursul cunoscutului matematician ascunde însă
o tristețe: ceea ce spune este căutat și împărtășit mai
degrabă de către publicațiile de specialitate și aproape
deloc de instituțiile cu putere de decizie. Dorința lui de
a comunica, de a transmite ceea ce a descoperit este însă
mai puternică decât opacitatea societății, fiind o dovadă a
vocației sale de profesor.

977

Solomon Marcus

„Așa cum dvs. ați făcut matematica la școală, ați
bănuit vreodată că aceasta este strâns legată de metaforă?
Aici se vede deosebirea dintre matematica școlară și cea
adevărată. Din păcate, lucrurile astea nu le aflăm la școală,
ci mai târziu sau niciodată“, ne‑a spus matematicianul,
lăsându‑ne, mai întâi, să ne gândim care este poziția
noastră față de această știință. De fapt, deși poate oferi
informații exacte, ce învățăm la școală împiedică o
cunoaștere profundă a acestui domeniu. Baza cunoașterii
noastre se formează într‑un mod artificial, pentru că, încă
din primii ani de școală, sunt învățate anumite procedee,
adeseori doar pe bază de memorare, fără să existe o
adevărată înțelegere a domeniului propus spre a fi studiat. 
„Satisfacția vine din acest itinerar: curiozitate, efort,
înțelegere. Din păcate, de foarte multe ori nu se întâmplă
așa, pentru că elevii se supun cerințelor școlii, de teama
că vor fi sancționați, într‑un fel sau altul. Problema este
de a‑i face să dorească să înțeleagă“, ne‑a spus Solomon
Marcus. L‑a citat apoi pe Steve Jobs, fondatorul Apple,
arătându‑și dorința ca discursul marelui mentor al lumii
computerelor să fie cât mai popularizat. „A spus ceva
extraordinar: fiți înfometați, fiți nebunatici. Nu lăsați
ca alții să vă trăiască viața. Mie mi se pare că aceste
lucruri ar trebui să se regăsească pe pereții tuturor
școlilor. Înfometarea înseamnă foame de cultură. Fiți
nebunatici: dați frâu liber imaginației voastre. Așa a
978

Dezmeticindu-ne

inventat el iPad‑ul și iPhone‑ul. Munca doar pe bază de
procedee de rutină înseamnă că alții îmi trăiesc viața; de
aceea, este necesară încrederea în puterea imaginației
proprii. Aveți aici esențialul reușitei personale în viață
și al reușitei sociale. Din păcate, cei mai mulți oameni
nu se înscriu în aceste tipare. Preferă, din comoditate,
să repete ce au știut dinainte decât să învețe ceva nou.
Trăiesc pe baza unor acte de repetiție, ceea ce înseamnă
o viață searbădă“, a argumentat matematicianul,
adaugând că nivelul de educație este strâns legat de cel al
preocupărilor noastre.
Redescoperirea bucuriei de a învăța
În cartea sa de 1.300 de pagini, Răni deschise,
lansată anul acesta, academicianul a adunat multe dintre
articolele pe care le‑a scris sau la care a colaborat, de‑a
lungul anilor. „În Răni deschise sunt peste 100 de texte,
în care, pornind de la contactul meu personal cu lumea
școlii, evidențiez anumite idei pe care le‑am constatat
la tineri, observând cum s‑au manifestat și ce sugerează
acestea. Poate, de multe ori, nu am reușit să spun cum
trebuie făcut, dar ceea ce cred că am reușit este să pun
degetul pe cât mai multe răni deschise.
Acolo discut despre problemele nerezolvate, dar,
pe de altă parte, dau și multe exemple de persoane
care au îndrăznit și au dorința de a fi ele însele“, a
979

Solomon Marcus

argumentat Solomon Marcus. Provocarea pe care
academicianul o lansează tuturor celor care își pun
întrebări despre capacitatea lor de a învăța sau de a
ieși din rutină este să încerce să înțeleagă chestiuni cât
mai complexe. „Pentru mine este o mare satisfacție să
aflu despre anumite teoreme matematice sau să citesc
o poezie extraordinară, ceva ce nu se înțelege ușor
de la prima lectură. Curiozitatea mă împinge să fac un
efort, care primește imediat o recompensă: plăcerea“, a
continuat matematicianul. Urmează explicația firească a
necesității lui de a transmite din ceea ce a învățat de‑a
lungul anilor. „Unul dintre marile privilegii ale ființei
umane este de a se bucura de libertate. Am fost mereu
invitat în școli, universități sau alte instituții culturale să
prezint conferințe, să dau interviuri, să public articole.
Tribuna învățământului îmi cere mereu articole. Dar
n‑am observat ca această prestație a mea să aibă vreo
influență asupra actelor decizionale. Le accept pentru că
am nevoie organică de un public. Am nevoie de oameni
pe care să îi contaminez“, a argumentat Marcus. Potrivit
acestuia, a fi profesor se identifică aproape complet
cu meseria de actor. De fapt, între a fi cărturar și a ieși
în scenă este o continuă alternanță. „Studiul, lectura,
reflecția, cercetarea te împing în singurătate, dar, după
ce s‑a făcut plinul, ai nevoie să ieși în lume și să spui și
altora“, a conchis academicianul.
980

Dezmeticindu-ne

 Globalizarea comunicării, un vis împlinit
Discutând cu Solomon Marcus, am avut revelația
dorinței permanente de a cunoaște. Mărturisirea sa
uimește prin simplitate. „În fiecare etapă a vieții, am avut
o preocupare. La un moment dat, m‑a preocupat infinitul
matematic“, ne‑a spus academicianul. 
Am încercat să aflam ce i‑a stârnit curiozitatea
față de domeniul vast al matematicii și cum a început
formarea sa ca intelectual. Părinții fiind deja la o vârstă
înaintată, nevoia de modele, atât de specifică vârstei,
a fost îndreptată către frații mai mari. „În perioada
adolescenței, cred că școala nu mi‑a furnizat oameni care
să fi avut asupra mea o influență hotărâtoare, dar am avut
un frate care aducea în casă cărți filozofice. Am avut și
vreo doi prieteni alături de care discutam probleme de
cultură, dar marii mei maeștri au venit abia după război.
Am avut autori care m‑au impresionat, nu oameni cu
care să fi avut contact“, a explicat matematicianul.
  Era dornic să comunice cu persoane din diverse
colțuri ale lumii, ceea ce nu a ajuns să se concretizeze
foarte ușor decât în epoca internetului. „M‑am refugiat
foarte mult în lectură. Eram nemulțumit că, prin
capacitatea mea de a comunica, nu puteam aspira să
ajung decât la oameni care îmi erau foarte apropiați în
timp și spațiu. Globalizarea comunicării a făcut ca,
prin internet, să pot să am contacte de la polul opus
981

Solomon Marcus

al planetei. Chiar am, zilnic. Scriu cuiva din Noua
Zeelandă și, uneori, mi se răspunde după câteva minute“,
ne‑a mai spus Marcus. Este preocupat de problemele
limbajului. Deși învățase engleza și putea să citească în
orice limbă romanică și în rusă, singura limbă în care
se putea exprima cu toate nuanțele necesare era limba
română.
Regalitatea – un reper
Nu a putut onora invitația de a asista la discursul
regelui în parlament, dar ne‑a mărturisit atașamentul față
de instituția regalității. „Mi‑a venit chiar aici o invitație
personală de a asista la prezența regelui în parlament,
dar nu eram în București. În aceeași zi eram invitatul
Universității „Alexandru Ioan Cuza”, din Iași, de a ține
o conferință, chiar la ora 10, când vorbea regele Mihai.
Vreau să spun că legătura mea de suflet cu regele a
început mai de mult, la 23 august 1944, când l‑am auzit,
la radio, citind proclamația către țară, că România rupe
alianța cu nazismul și oprește războiul din Est. După un
acord cvasigeneral, el a scurtat războiul cu câteva luni.
A evitat nu știu câte alte noi victime în armata română
și, lucru esențial, a creat circumstanțele favorabile pentru
recuperarea Ardealului de Nord. Pe urmă, l‑am urmărit
la deschiderea universității, după 23 august 1944“, a
susținut cunoscutul matematician.
982

Dezmeticindu-ne

În 10 mai 2011, a fost invitat la Palatul Elisabeta
și regele i‑a înmânat personal decorația Nihil Sine
Deo. Tot Casa Regală l‑a invitat să colaboreze la
volumul Lumea regelui, dedicat celor 90 de ani ai
regelui Mihai. „Am trăit o mare satisfacție prin aceste
momente, pentru că, efectiv, am fost foarte impresionat
de personalitatea morală și intelectuală a regelui. Cine
nu înțelege lucrul acesta să citească cele două volume
cuprinzând dialogul regelui Mihai cu scriitorul Mircea
Ciobanu. Argumentarea mea nu se referă deloc la
compararea monarhiei cu republica. Nu discut asta. Este
o chestie mult mai complicată. Probabil că republica
este superioară. Dar, fapt este că am avut președinți aleși
democratic, care au devenit cei mai odioși dictatori,
cum a fost Hitler, și am avut regi cum ar fi Ferdinand,
Carol I, Mihai, care, deși au venit prin succesiune, au
devenit oameni de mare înțelepciune…“, ne‑a mărturisit
Solomon Marcus.

983

