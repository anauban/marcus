Copii în avion
Dilema veche, 14 septembrie 2011
Ce era mai potrivit să fac în timpul unui zbor
transoceanic de șapte ore, apoi într‑un alt zbor, de trei
ore, care urma să mă aducă acasă? Parcurgerea revistelor
oferite de compania aeriană cu care călătoream nu mi‑a
luat mai mult de zece minute. Greu dai în ele de ceva
care să‑ți oprească atenția. Există, la unele companii,
bunul obicei de a ți se oferi ziare proaspete. N‑am avut
această șansă și, oricum, nici aceste ziare nu‑ți pot opri
atenția mai mult de vreo 20 de minute. Laptopul îl
lăsasem acasă, i‑pad‑ul încă nu‑l asimilasem. Știam însă,
din situații similare anterioare, că este bine să am cu
mine ceva pe gustul meu, de citit și de reflectat. Numai
că, de această dată, lucrurile au luat o altă turnură.
În zborul de șapte ore, locul meu era lângă o mamă
cu un copil de vreo doi‑trei ani, în brațe. Imediat mi‑am
adus aminte de frecventele situații în care un copil de
această vârstă reușește să deranjeze, prin țipete și plâns.
Speranța mea de a citi în liniște ceea ce aveam cu mine
sau, pur și simplu, de a dormi, se destrămă. Mă bătea
gândul să‑mi găsesc un alt loc. Numai că alt loc nu
685

Solomon Marcus

exista, avionul era plin.
După vreo jumătate de oră de la decolare,
mi‑am dat însă seama că scenariul pesimist pe care‑l
construisem nu se confirma. Devenisem curios să înțeleg
cum anume reușește tânăra mamă să‑și țină pruncul
într‑o liniște aproape ireală. M‑am lămurit. Fiecare
călător avea în fața sa un ecran pe care, cu o telecomandă
care‑ți oferea o alegere, puteai obține, eventual, ceva
care să te intereseze. Nu intenționam să folosesc această
posibilitate, mă pregăteam pentru lecturile pe care mi
le planificasem. Privirea mi‑a alunecat însă spre copilul
de lângă mine. Mama sa reușise să găsească ceva care
să‑l captiveze. Mă uitam în ochii acestuia, parcă lipiți
de ecran. O stare de încântare îi cuprinsese întreaga
ființă, mirarea sa era atât de mare, încât vraja sub care se
afla părea să‑l fi făcut să uite de tot ceea ce era în jurul
sau. Numai din când în când, de oboseală, pleoapele
i se închideau și adormea. Ce se întâmplase? Copilul
descoperea lumea animalelor de tot felul, de la cele
gigantice, preistorice, la cele care populează azi jungla
africană, braziliană sau australiană. Era clar că devenise
prizonier al acestei lumi, în care imaginația sa hoinărea
acum.
La zborul următor, pe rândul din fața mea se afla o
fetiță de vreo patru ani, având la stânga pe mama ei, iar
la dreapta pe bunica. Eu eram în spatele bunicii. La un
686

Dezmeticindu-ne

moment dat, fetița mă privește, râde, eu îi răspund cu un
semn de acceptare și cu o privire la fel de prietenoasă,
dar atunci ea se retrage brusc, ascunzându‑se în scaunul
ei, pentru ca, după un scurt timp, să reapară, dar numai
cu ochiul stâng. Iar o salut, îi zâmbesc, dar atunci din
nou se retrage și tot așa, în mod repetat. După un timp,
reia același joc de apariție și de ascundere, cu persoana
aflată în spatele mamei sale. Și această persoană îi
acceptă invitația, se prinde în joc, îi răspunde cu aceeași
prietenie. Mama și bunica o observă, nu înțeleg nevoia
ei de joc și o ceartă pentru o imaginară culpă; ele nu văd
în comportamentul fetiței decât o lipsă de bună cuviință,
care deranjează persoanele din jur. Mi‑am dat seama
ca încercase să‑și găsească parteneri și în rândul din
fața ei. Aflată acum sub interdicție, caută să descopere
posibilitățile ludice ale spațiului foarte restrâns în care
se află și din care orice încercare de evadare era imediat
descurajată. Ce‑i mai rămânea? Mai întâi a explorat
locurile situate sub scaunele pasagerilor, dar nu a găsit
nimic interesant. Mama i‑a pus apoi în mână revistele
din buzunarul din față, pline de poze de bijuterii și
cosmetice, care nu i‑au oprit atenția decât pentru o
foarte scurtă durată. Le‑a închis plictisită și a început să
pocnească cu ele spatele scaunului din fața ei. Bunica i‑a
oferit atunci o linguriță de plastic, rămasă de la gustarea
oferită în avion, urmând ca fetița să descopere modul în
687

Solomon Marcus

care acest obiect poate deveni o jucărie. Numai că, tot
scrijelind cu lingurița ceea ce se putea, a rupt‑o și nu
a mai fost bună de nimic. A trecut atunci la explorarea
capacităților ludice ale spătarului, care, aflat în poziție
oblică, a fost transformat cu succes în tobogan. Bucuria
i‑a fost însă de scurtă durată. De la stânga și de la
dreapta ei, a venit imediat imperativul bine cunoscut:
„Fii cuminte, stai la locul tău!” Norocul ei a fost că, la
scurt timp, avionul a aterizat la Otopeni.

688

