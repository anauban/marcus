Matematica românească într‑o nouă etapă
Academia Română, 29 septembrie 2011,
Sesiunea despre viitor
Comisia de Studii Prospective a Academiei
Române ne propune următoarea sintagmă: „Viitorul
științei românești”. Mă voi ocupa de matematică, dar
indiferent că spunem viitorul științei românești sau
viitorul matematicii românești, fiecare termen ridică mari
probleme.
Viitorul. Cred că se are în vedere nu viitorul
imediat, de mâine, ci cel de poimâine. De cel imediat se
ocupă alte instituții. A avut loc, în acest an, la Brașov,
Al VII‑lea Congres al Matematicienilor Români de
Pretutindeni. Am fost invitat să prezint, în deschiderea
congresului, un raport despre starea actuală a
matematicii românești. Încercarea de a obține informația
necesară pentru a prezenta un astfel de raport în mare
măsură a eșuat, dat fiind că prezentarea performanțelor,
a marilor succese, am tot făcut‑o și mi‑am dat seama
că trebuie să prezentăm și partea care se ascunde de
obicei. Știm noi cam câte persoane în România obțin
un venit din profesia de cercetător în matematică? Fie
689

Solomon Marcus

într‑o universitate, fie într‑un institut de cercetări? A
fost imposibil să aflăm. Toate universitățile particulare
invitate să ne furnizeze informația necesară, și multe din
ele găzduiesc mulți matematicieni, oameni care primesc
un salariu dintr‑o activitate care include și componenta
de cercetare, au boicotat acest apel, n‑au răspuns.
Dl. președinte v‑a prezentat o mulțime de tabele.
Din niciunul dintre aceste tabele nu veți afla cam câți
sunt cei care, în România, ocupă locuri în universități și
în institute de cercetare și nu se încadrează nici măcar în
minimul de exigență în materie de cercetare științifică. E
o zonă în care suntem pur și simplu ignoranți. Niciunul
dintre rapoartele institutelor Academiei, care apar în
revista Academica, nu furnizează informații despre acest
aspect. Nu suntem în stare să monitorizăm prezentul, vă
dați seama cât de greu este să bănuim cam ce viitor ne
așteaptă.
Cercetarea nu e totdeauna organizată pe discipline.
De pildă, o problemă în care sunt personal
implicat, celula biologică. Toate posibilitățile actuale
de monitorizare a cercetării eșuează în nevoia de a
determina unde ne aflăm în acest domeniu. Aici lucrează
cercetători veniți din atât de multe orizonturi, încât
o organizare ca cea actuală, axată pe segmentarea în
discipline, nu poate să‑i facă față. Pe această problemă
am prezentat o comunicare internațională la care am pus
690

Dezmeticindu-ne

un titlu provocator: Celula biologică în spectacol. Am
prezentat 12 direcții de cercetare în domeniul celulei
biologice în care sunt implicate informatica, fizica,
lingvistica, matematica, etc. Fiecare domeniu își are
premisele lui, jargonul lui, specialiștii lui, matematica
fiind implicată cam în toate. Aceste direcții nu comunică
între ele aproape deloc. Deci, suntem într‑o mare criză în
ceea ce privește monitorizarea stării actuale a cercetării,
din cauză că monitorizarea este încă dominată de
prejudecata segmentării în discipline, iar cercetarea din
ce în ce mai mult se face altfel.
Al doilea termen, matematica. E o problemă să
spunem ce este azi matematica. Vă dau un exemplu
șocant și recent. Un mare matematician american
a demonstrat o teoremă care în enunț nu are nimic
matematic, e un enunț înțeles de toată lumea. Dacă
liberul arbitru funcționează la nivelul individului uman,
atunci el obligatoriu funcționează la nivel atomic de
asemenea. Dar demonstrația e foarte complicată.
Toată matematica injectată în economie,
în inginerie, în informatică, nu mai vorbesc de
domeniile clasice: fizica, chimia, numai în parte este
monitorizată de instanțe matematice. Cea mai mare
parte a acestor aspecte nu este recenzată în revista
Mathematical Reviews, pentru că nu e cunoscută. Ea
este pulverizată în reviste dintre cele mai diverse și
691

Solomon Marcus

scapă oricărei monitorizări. Fizicienii s‑au grăbit să
spună că matematica e un limbaj, inginerii să spună că
matematica e un instrument. Până la urmă se pare că
această universalitate a ei ține mult de modul de gândire
pe care îl promovează și care nu e neapărat întotdeauna
manifestat în formule.
Termenul românească. Matematica românească.
Colegul meu, academicianul Viorel Barbu, are
următoarea părere despre acest epitet: „Sintagmele
știință românească și matematică românească sunt
depășite, deoarece o cercetare competitivă nu se poate
face decât prin colaborarea și în cadrul echipelor
performante din institutele europene, americane și
eventual din Asia. Numai așa matematicienii români
vor putea obține rezultate importante și, cine știe, se vor
putea califica și pentru o medalie Fields.” Vă mărturisesc
că trăiesc personal această realitate. Am acum două
articole în curs de publicare, unul într‑o revistă din
Coreea de Sud și celălalt într‑o revistă chinezească.
Matematica românească a acumulat până acum vreo
10 generații. Începând cu generația întemeietorilor. Ce
e nou la ultimele generații? La primele 3 congrese ale
matematicienilor români nu exista o diasporă matematică
românească. Treptat, componenta diaspora a crescut
atât de mult, încât astăzi, dacă vă uitați în programul
Congresului Internațional de la Brașov, ea a reprezentat
692

Dezmeticindu-ne

cam o treime din totalul de participanți. S‑a putut vedea
calitatea ei extraordinară și extraordinarul metabolism
care există între diaspora matematică românească și
matematica din România, un lucru care nu figurează
în niciun fel de tabele. Este cu totul ultrasimplificator
să se spună că ei au migrat în America și i‑am pierdut.
Tot acest limbaj mi se pare complet desuet în momentul
de față, cel puțin în domeniul matematicii și al
informaticii, pentru că aici pot da zeci de exemple în care
interacțiunea este atât de puternică încât îți dai seama că
din ce în ce mai puțin contează locul geografic în care te
afli. Revoluția care s‑a petrecut în domeniul comunicării
și al informației a marginalizat locul geografic în care te
afli și a adus în prim plan capacitatea de a interacționa
cu cei care au preocupări similare. De exemplu, un
matematician român, Alexandru Zaharescu, în SUA,
e un lider în domeniul teoriei analitice a numerelor, un
domeniu care nu prea se cultiva la noi înainte. A reușit
să facă o școală în care împreună cu componenta din
diaspora există și o componentă din România. Mulți
dintre elevi sunt chiar din promoția lui universitară. E
o școală în care colaborarea merge perfect, unii sunt la
București, alții în SUA, alții în Franța. Nu contează că se
află în afara României.
Un alt exemplu, tânărul Andrei Teleman,
reprezentant al celebrei familii de matematicieni,
693

Solomon Marcus

Teleman, care este la noi similară cu dinastia Bernoulli
în Occident. Andrei Teleman a prezentat o lucrare de 50
de pagini la Congresul de la Brașov, care s‑a publicat în
ultimul număr pe 2010 din Annals of Mathematics. La
ultimul Congres Internațional de Matematică, (a avut
loc în India anul trecut) s‑a discutat o problemă specială,
Metrics in Mathematics. Cum stăm cu măsurarea
performanțelor matematice. Toate discuțiile au mers
spre concluzia că aplicarea diverșilor parametri trebuie
menținută, cu condiția ameliorării lor continue, așa cum
rezultă din studiile revistei internaționale Scientometrics,
dar niciodată să nu ne rezumăm la ele. Ele trebuie
însoțite de o privire umană. Să vedem calitatea citărilor,
revistelor. Să nu rămânem la numere și să fim atenți ce
anume numărăm, pentru ca lucrurile să fie semnificative.
Ceea ce deosebește matematica actuală de cele mai
multe alte domenii e faptul că a ajuns la o unificare
a problematicii ei, care este aceeași în toate părțile
globului. S‑a ajuns foarte greu și foarte târziu la această
situație, abia în a doua jumătate a secolului trecut. Din
cauza aceasta se țin periodic congrese internaționale
ale matematicienilor, lucru care încă nu există nici
în domeniul fizicii, nici în domeniul informaticii.
Problematica informaticii americane e destul de diferită
deocamdată de problematica informaticii europene și,
după informațiile pe care le am, nici în fizică nu s‑a ajuns
694

Dezmeticindu-ne

la o unificare care să atenueze conflictul dintre fizicienii
practicieni, aplicativi și cei teoreticieni. Ceea ce e nou în
matematică acum este faptul că unificarea ei nu e doar
geografică, e o unificare organică între diversele ramuri
ale matematicii. Foarte rar mai găsești acum cercetări
de nivel înalt care să fie cantonate doar în analiză, doar
în algebră sau doar în probabilități. Tendința este de
cooperare organică a tuturor ramurilor matematicii.
Acest lucru este un fenomen care își are originile la
începutul secolului trecut. Începutul secolului al XX‑lea
a fost marcat de lista problemelor propuse de Hilbert
și la începutul secolului actual matematicienii s‑au
gândit să facă o nouă listă. A apărut chiar o carte, The
Millennium Problems. Din lista de probleme propusă de
Hilbert, doar unele au fost complet rezolvate și multe din
cele nerezolvate încă privesc tocmai raportul matematicii
cu restul lumii. Una dintre probleme este axiomatizarea
fizicii. Chiar fundamentele matematicii sunt încă în
chestiune. Nu știm exact care este situația ipotezei
continuului, de exemplu.
În
domeniul
metabolismului
matematicii
cu celelalte discipline, vreau să aduc în atenția
dumneavoastră, alături de aspectele binecunoscute,
puternicul impact al matematicii în mecanică, unde
avem școli puternice. În biologie impactul este
atât de puternic, încât congresele internaționale de
695

Solomon Marcus

matematică includ acum un raport privind matematica
în biologie. Este extraordinar ceea ce se întâmplă în
domeniul socio‑uman, unde este o pătrundere atât de
masivă a matematicii, încât nu pot să nu menționez
un succes extraordinar al cercetării românești. Un
matematician român care a reușit să producă ceea ce
specialiștii în domeniul muzicii numesc o revoluție în
gândirea muzicală. Numele acestui matematician este
Dan Tudor Vuza, cercetător principal la Institutul de
Matematică al Academiei Române. Ceea ce vă spun
nu este aprecierea mea, este aprecierea celor mai mari
reviste de specialitate din domeniul teoriei muzicale
din America și din Europa Occidentală. Vuza figurează
acum în bibliografia de doctorat în muzicologie, la mari
universități americane și europene. Limbajul matematicii
se află acum în atenția lingviștilor, în atenția filozofilor,
se țin mereu conferințe internaționale privind natura
limbajului matematic. Este un metabolism în toate
direcțiile și toate aceste tendințe scapă monitorizării pe
segmentare disciplinară, pentru că ele parcurg mai multe
discipline și e ca și cum nu ar intra în niciuna.

696

