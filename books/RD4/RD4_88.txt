Ascultându‑l pe Steve Jobs
Tribuna învățământului, 12 decembrie 2011
Pionier al microinformaticii, al calculatoarelor
personale, cofondator al Societății Apple, un adevărat
triumf al tehnologiei secolului trecut, se îmbolnăvește
de cancer la pancreas în 2003, când nu avea decât 48 de
ani, și moare în octombrie 2011. Boala nu‑i stăvilește
elanul creator, numele său este organic asociat cu apariția
iPhone‑ului și a iPad‑ului, în zorii celui de‑al treilea
mileniu. Milioanele de utilizatori ai Macintosh‑ului, cei
care beneficiază și se bucură de cele mai recente cuceriri
ale erei digitale, la început de secol 21, ar trebui să afle
cum a arătat viața unuia dintre cei care au făcut posibile
aceste inovații.
Iată un om al cărui parcurs merită să fie cunoscut.
Să‑l urmărim, începând cu copilăria sa furtunoasă, până
la atingerea celebrității, în manifestările sale publice,
deoarece avea vocația întâlnirilor cu oameni tineri. Vom
descoperi un om care, din experiența propriei sale vieți,
a reușit să extragă atât de multă înțelepciune, încât nicio
dezbatere asupra problemelor educației nu‑l mai poate
ignora.
896

Dezmeticindu-ne

Părinții săi naturali, o occidentală și un sirian,
dispar repede din orizontul său și ulterior nu‑și mai
amintește și nu mai recunoaște decât pe cei care‑l adoptă
la scurt timp după naștere. Acești părinți adoptivi, al
căror nume de familie (Jobs) îl preia, acordă o atenție
specială educării lui Steve. Tatăl, mașinist la o companie
de laser, îl învață electronica, dar, mai mult decât atât,
face cu el adevărate exerciții de euristică: “Iată, îi spune
lui Steve, am acest obiect mai complicat, îl descompun
în diverse componente, încearcă să vezi în ce fel le
poți recompune altfel”. Mama, contabilă, îl învață să
citească încă înainte de a merge la școală. «Am vrut să
fiu pentru Steve un tată cel puțin la fel de bun cum a fost
tatăl meu pentru mine», a mărturisit tatăl adoptiv unui
ziarist. Ca școlar, Steve frecventează, după‑amiaza, niște
cursuri speciale ale companiei Hewlett Packard. Acolo,
începe să colaboreze cu Steve Wozniak, cu cinci ani mai
în vârstă, viitorul său partener la Apple. Se întâmpla
aceasta în 1971, când Steve avea 16 ani. De aici începe
o ascensiune tot mai vertiginoasă, dar care, pe segmentul
ei inițial, nu l‑a privat de tot felul de dificultăți, care
probabil au contribuit la îmbolnăvirea sa. Dar lupta pe
care a dus‑o, timp de opt ani, cu cancerul, modul în care,
depășindu‑și suferința fizică, a demonstrat că voința de a
dărui oamenilor optimul capacității sale creatoare poate
fi mai puternică decât boala cea mai cruntă, se constituie
897

Solomon Marcus

într‑un exemplu pe care noile generații trebuie să‑l aibă
în față.
“Singurul mod de a face un lucru bun este de a
iubi ceea ce faci. Atâta vreme cât n‑ai găsit ceva de
care să te îndrăgostești, continuă să cauți”. Dar ce fel
de comportament favorizează descoperirea unor lucruri
pe care să le iubim? Ne răspunde tot Steve Jobs: “Să
fii mereu flămând, mereu nesătul, să fii nebunatic,
neastâmpărat; să nu te mulțumești niciodată cu starea
existentă, să vrei s‑o depășești!”
Cum adică să fii nesătul? Cunoașterea, cultura
seamănă cu mâncarea. O condiție a unei alimentații
sănătoase este să mănânci în măsura în care ți se face
foame. De această foame are grijă în primul rând
natura din noi. Dar de foamea de a înțelege lumea, de
a ne înțelege pe noi, are grijă tot natura? Pentru copiii
preșcolari, răspunsul este afirmativ, dar, pe măsură ce
înaintează în vârstă, această foame a copilului nu prea
este stimulată, mai degrabă este anihilată, cel puțin în
sistemul nostru educațional. Foamea de a înțelege lumea,
foamea de lectură, foamea de cultură, pretind o educație
corespunzătoare și tocmai această educație lipsește.
Livrăm copiilor, adolescenților, tinerilor o sumedenie de
cunoștințe pentru care nu am creat în prealabil foamea
de ele. În aceste condiții, nu se produce asimilarea lor
reală. Aici apare și o deosebire între foamea de mâncare
898

Dezmeticindu-ne

și foamea de cultură. Prima este potolită periodic, a doua
n‑ar trebui să se potolească niciodată. De aici, îndemnul
lui Steve Jobs de a nu ne simți niciodată sătui, de a nu
ne epuiza niciodată curiozitatea de a înțelege lucrurile
cât mai profund, de a fi capabili să descoperim în orice
răspuns o sursă de noi întrebări.
Mai spune Steve: „Să ai totdeauna comportamentul
unui debutant, îmi dau seama că lucrurile care‑mi ies cel
mai bine sunt acelea pe care reușesc să le privesc cu un
ochi proaspăt, nu să aplic certitudinile și experiențele
mele anterioare”. Descifrăm aici oroarea de rutină, de
comportament mecanic, în care acționăm nu gândind cu
capul nostru, ci preluând din comoditate procedee care
ne vin de la alții. Steve ne cere să privim mereu lucrurile
ca și cum o facem pentru prima oara, să nu lăsăm niciun
moment să se aștearnă pe ele colbul, rugina, plictiseala.
De fapt, el ne cere să aplicăm spiritului nostru același
principiu pe care trebuie să‑l aplicăm trupului nostru.
Fără mișcare suficientă, trupul lâncezește, iar hrana pe
care o primește trebuie să corespundă caloriilor pe care
le arde. Fără menținerea sa în acțiune, spiritul nostru
se anchilozează. Tocmai asta vrea să spună îndemnul
de a fi într‑un neastâmpăr continuu, de a da frâu liber
imaginației.
A ajuns posesorul unei averi respectabile, dar niciun
moment nu a făcut din acest fapt un scop. Motorul vieții
899

Solomon Marcus

sale a fost și a rămas tot timpul dorința de a se manifesta
ca o ființă creatoare. Restul, a venit ca un corolar.
Să învățăm de la Steve Jobs!

900

