Viziunea sistemică
Jurnalul Literar, nr. 15‑18, aug‑sept. 2000, pp. 1‑7
Unul dintre aspectele cele mai caracteristice și
mai generatoare de controverse ale filosofiei lui Ștefan
Lupașcu se referă la modul în care acesta concepe
organizarea sistemică. Unele dintre cărțile sale apar
exact în perioada de emergență a teoriei sistemelor, în
biologie, în matematică, în științele social‑umane și în
alte teritorii ale cunoașterii, începând cu anii ̓40 și până
în anii ̓80 ai secolului al XX‑lea.
Paradigma sistemică a intrat în atenția cercetătorilor
începând cu anii ̓50, ca o completare la ideea de
structură, aflată în centrul atenției încă de la sfârșitul
secolului al XIX‑lea, în chimie (noțiunea de izomerism),
în lingvistică, în psihologie și în alte discipline.
De fapt, noțiunea de sistem apare încă în interiorul
structuralismului lingvistic, de exemplu în cadrul
Cercului din Praga (anii ̓20), cu referire la solidaritatea
dintre local și global; este caracteristic în această privință
sloganul lui Ferdinand de Saussure, după care limbajul
este o totalitate de elemente în care „tout se tient”, cu alte
cuvinte există o legătură între părți și între întreg și parte.
317

Solomon Marcus

În anii ̓50 se cristalizează ideea de sistem ca entitate
dinamică, în opoziție cu structura, predominant statică.
Clasificării sistemelor după natura componentelor
lor (sistem fizic, biologic, psihic, lingvistic, social etc.),
Lupașcu îi opune o viziune unitară, în cadrul căreia
numitorul comun al sistemelor de cele mai variate tipuri
este dat de caracterul lor unitar, constituit de conflictul
dintre energii antagoniste. În orice sistem, coexistă
anumite forțe de atracție între componente și anumite
forțe de repulsie între ele. Caracterul sistemic este dat,
în viziunea lui Lupașcu, de faptul că între cele două
tipuri de forțe apare o tensiune, cele de atracție (repulsie)
opunându‑se celor de repulsie (atracție). Acest fapt este
valabil pentru orice tip de sistem, fie el atomic, biologic,
psihic sau social.
Există ceva comun între ideea de mai sus, a
lui Lupașcu, și ideea de structură disipativă a lui
Prigogine. În viziunea lui Prigogine, un sistem trece
la un stadiu superior atunci când trebuie să facă față
unor dificultăți, întocmai ca în înțelepciunea populară,
conform căreia omul se călește în luptă cu greul. În
viziunea lui Lupașcu, o forță devine cu atât mai activă
cu cât are de luptat împotriva unei alte forțe, care i se
opune. Asemănarea este evidentă; numai că la Lupașcu
este vorba de un proces simetric, bilateral, în timp ce la
Prigogine această trăsătură nu este obligatorie.
318

Dezmeticindu-ne

Dincolo de acest numitor comun al sistemelor de
orice fel, încep să apară deosebirile. Pentru a le înțelege,
Lupașcu introduce distincția actualizare‑potențializare.
O forță se actualizează numai în măsura în care forța
care i se opune se potențializează, una câștigând ceea
ce cealaltă pierde. Situația amintește de jocurile de
sumă nulă din teoria matematică a jocurilor de strategie
a lui John von Neumann. Numai că în situațiile care
apar în realitatea naturală sau socială nu totdeauna se
întâmplă așa; în unele situații conflictuale suma dintre
ceea ce câștigă o parte și ceea ce pierde cealaltă parte
nu este egală cu zero, situația nu este simetrică. Un
echivalent al acestei posibilități îl putem identifica și
în abordarea lui Lupașcu; acesta introduce o a doua
distincție fundamentală, aceea dintre forțe omogenizante
și forțe eterogenizante. Distincția aceasta nu este
străină de conceptul de entropie, așa cum apare el în
termodinamică, dar și în economie (a se vedea cercetările
întreprinse de Nicolas Georgescu‑Roegen) și în teoria
informației (Shannon). Tipologia triadică pe care o
propune Lupașcu se referă la trei tipuri de sisteme: 1)
sisteme la care forțele omogenizante sunt mai puternice
decât forțele eterogenizante, care li se opun; este ceea ce
se întâmplă în sistemele macrofizice, aflate sub acțiunea
principiului al doilea al termodinamicii; 2) sisteme în
care forțele eterogenizante sunt mai puternice decât
319

Solomon Marcus

cele opuse, de omogenizare; aici intră sistemele din
lumea vie, care creează, cum spune Prigogine, insule
de entropie descrescândă într‑un ocean de entropie
crescândă; 3) sisteme în care forțele de omogenizare se
află în echilibru cu cele de eterogenizare, conducând la
așa numită stare T, pe care o interpretăm ca terțul inclus,
în contrast cu terțul exclus din cel de al treilea principiu
al logicii clasice a lui Aristotel; aici intră sistemul
cuantic, sistemul neuro‑psihic și sistemul creativității
artistice.
O ilustrare semnificativă a aceste tipologii este dată
de clasificarea propusă de J.G.Miller în monumentala
sa lucrare „Living systems” (McGraw‑Hill, New York,
1978), unde se disting șapte niveluri de organizare a
viului: celula (apărută ca o insulă de eterogenizare și
de relativă stabilitate într‑o mare de omogenizare și de
instabilitate), organul (la nivelul căruia apare capacitatea
unei mase organizate de celule de a înlocui celulele
care mor), organismul (care face posibilă învățarea
asociativă, intercondiționarea organelor și limbajul
simbolic), grupul (care permite activitatea cooperativă
și dezvoltarea strategiilor de obținere a hranei și
informației), organizația (cu un nou statut al persoanei
și cu dezvoltarea relațiilor interpersonale), societatea
(care permite sisteme elaborate de înrudire, noi tipuri
de guvernare și dezvoltare a științei, artei și tehnologiei)
320

Dezmeticindu-ne

și sistemul supranațional (în cadrul căruia se dezvoltă
organismele internaționale de rezolvare a conflictelor
dintre națiuni).
Față de simetria inițială dintre forțele aflate în
conflict, apare acum o asimetrie. După cum observa
J.Costagliola, este mai ușor să cobori panta omogenizării
decât să urci aceea a eterogenizării. Viul este depășit de
mineral. Opusul viului nu este, pentru Lupașcu, moartea,
ci fizicul, mineralul, omogenul.
Viziunea lui Lupașcu trebuie pusă în legătură cu
teoria holonomiei. Cuvântul „holon” a fost adoptat din
biologie de Arthur Koestler, pentru a exprima entitățile
contradictorii de tipul lui Janus cel cu două fețe. Un
holon este un sistem care se comportă simultan ca un
subsistem și ca un suprasistem. Este exact ceea ce se
întâmplă cu cele șapte niveluri din tipologia lui Miller
prezentată mai sus, în care fiecare nivel intermediar
constituie un holon. Holonomia integrează punctul de
vedere al teoriei sistemelor cu cel al psihologiei umane.
Fiecare sistem viu este, pentru Lupașcu, o totalitate
pentru sistemele inferioare și o parte pentru cele
superioare. Totul amintește de situația păpușilor rusești;
Costagliola, unul dintre cei mai importanți comentatori
ai lui Lupașcu, modifică puțin acest limbaj: un holon
nu conține holonii inferiori, ci este pur și simplu
ansamblul acestora. O moleculă nu conține atomi, ci
321

Solomon Marcus

este acești atomi, într‑o anumită organizare. Corpul
nostru nu conține celule, ci este aceste celule, într‑o
anumită organizare. Din nou întâlnim antagonismul
omogenizare‑eterogenizare; metaforic, un holon este
un tot egoist alcătuit din părți altruiste, dar și o parte
altruistă a unui întreg mai vast. Pentru Lupașcu, cei
mai importanți holoni sunt celula, organismul și, pentru
unele specii (de exemplu, oamenii, albinele și furnicile),
societatea. Orice alt tip de holon nu se poate menține
decât în interiorul unuia dintre aceștia trei, dar societatea
este înțeleasă de Lupașcu într‑o accepțiune mai restrânsă
decât la Miller.
Ar fi interesant de urmărit legătura cu holomerii lui
Noica din a sa logică a lui Hermes.
Ar mai fi de discutat locul relativ redus acordat
paradigmei informației în filosofia lui Lupașcu, în
contrast cu rolul primordial atribuit energiei, în ciuda
faptului că a doua jumătate a secolului al XX‑lea a stat
mai degrabă sub semnul paradigmei informaționale
decât al celei energetice, caracteristice pentru perioada
1850‑1950.
Filosofia lui Lupașcu rămâne în continuare o
provocare interesantă și stimulatoare.

322

