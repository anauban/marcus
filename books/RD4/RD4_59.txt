Interviu realizat de Radu Moraru
Emisiunea Nașul, 10TV, 14 decembrie 2010
R.M.: Bună seara! Avem oaspeți distinși, vrem
cu adevărat să promovăm valori, ca tinerii noștri să
privească la ceva de calitate pe micile ecrane.
Avem onoarea să îl avem alături de noi pe acad.
Solomon Marcus. Citind foarte multe lucruri despre
domnia sa, am putut să descopăr cât de frumoasă, cât de
romantică, cât de poetică este matematica! Mulțumesc,
domnule profesor!
Domnul Nicolae Zamfir, director al Institutului
de Fizică Nucleară. România lucrează la un program
extraordinar, un construct pe care îl vom developa în
această seară. România face parte, încă, și sper mult timp
de acum încolo, din elita cercetării mondiale.
Avem și un om care s‑a confruntat cu cele mai mari
greutăți. Multiplu campion mondial, olimpic, Nicu Vlad!
La 20 de ani în glorie, de atunci tot așa o ține și sper să le
aducă glorie și tinerilor pe care îi antrenează.
R.M.: Domnule Solomon Marcus, de ce oare, chiar
dacă românii nu sunt cunoscători în haltere, atunci când
Nicu Vlad era pe podium și lupta pentru aur, o națiune
559

Solomon Marcus

întreagă era cu sufletul la gură, indiferent de ocupație și
de gradul de cultură?
S.M.: Ridicarea greutății este o metaforă a oricărui
efort uman. Există o înțelepciune și la noi, și în Extremul
Orient, că un sistem dobândește o calitate superioară
atunci când este stimulat să facă față unor forțe adverse.
Omul se călește în lupta cu greutățile. Ceea ce pentru
noi sunt greutăți în sensul abstract, pentru colegul
nostru ele sunt chiar în sensul material, și evident că
orice performanță e un stimulent pentru o performanță
superioară.
R.M.: Să spunem că orice victorie a lui Nicu Vlad
ar putea fi ulterior transformată pentru fiecare dintre noi,
într‑o victorie a noastră, personală.
S.M.: Da. Lucrul acesta este pasionant. În orice
sport și în orice activitate umană, ai impresia că primul
adversar este celălalt, dar în ultimă instanță îți dai seama
că noi ne luptăm cu propriile noastre resurse. Vrem
să ne autodepășim, deci lupta e cu noi! Mereu vrem să
scoatem la suprafață rezerve de energie și de inteligență
pe care nu le‑am activat anterior. Asta se vede și într‑un
joc de tenis, și în performanțele științifice.
Este evident că, și în acest sport, noi ne angajăm cu
întregul nostru corp. Și creierul își are rolul său, la fel
ca și brațele, picioarele. Este necesară multă inteligență,
mult control psihologic pentru a‑ți mobiliza și armoniza
560

Dezmeticindu-ne

toate resursele cerebrale, afective, energetice. Este foarte
înșelătoare ideea simplistă că gimnastica, sportul se fac
doar cu corpul, iar matematica doar cu creierul. În toate
e vorba de o mobilizare a tuturor resurselor noastre. Este
în ființa umană o dorință aprigă de autodepășire, ceea ce
numim sisteme cu autoorganizare.
R.M.: Cu oameni ca Nicu Vlad, putem să ne
repatriem tezaurul din alte părți, nu din Rusia.
S.M.: Avem de‑a face cu un domeniu în care
performanța este riguros măsurabilă. Există însă multe
domenii în care această măsurabilitate nu este posibilă.
De pildă, în activitatea de cercetare științifică, sau în
activitatea educațională, trebuie să fim mai prudenți
în folosirea cuvintelor „cel mai, cea mai”. Există din
păcate o tendință exagerată de a vedea toate lucrurile în
termeni de superlativ‑relativ. Trebuie descurajată această
tendință, oricât ar fi ea de spectaculoasă. În cele mai
multe domenii de activitate, evaluările nu se pot face
în termen de „cel, cea maiˮ. Trebuie să ținem seama că
valorile umane au o mare diversitate.
R.M.: Sunt prea frumoși oamenii pentru a fi
încadrați în aceste clasamente sau topuri de genul acesta?
S.M.: Nu se pot face clasamente metrice, bazate
pe măsurători. Ele nu se pot face în niciun domeniu, cu
excepția unor probe de sport, gimnastică etc.

561

Solomon Marcus

R.M.: Acum o sută de ani, se gândea cineva la
aplicațiile matematicii în medicină?
S.M.: La mijlocul secolului trecut, erau mari
matematicieni care spuneau că teoria numerelor, care e
considerată regina matematicii, nu va avea niciodată
aplicații practice, că rămâne doar ca o bijuterie estetică.
La treizeci de ani după ce s‑a făcut această afirmație,
de marele matematician englez Hardy, s‑au descoperit
aplicații extraordinare ale teoriei numerelor în domeniul
criptografiei, descifrării scrierilor secrete. O activitate de
mare valoare practică.
Logica matematică era de asemenea considerată
o disciplină gratuită, fără nicio posibilă aplicație. Însă
spre mijlocul secolului trecut s‑a descoperit că logica
matematică are aplicații extraordinare în ingineria
circuitelor electrice. Trebuie să fim foarte precauți cu
folosirea cuvântului „niciodată”. Trebuie să îl evităm pe
cât posibil, deoarece imaginația pe care o dezvoltă știința
întrece de departe imaginația fiecărui individ în parte.
În anii ’60 ai secolului trecut, unul din cei mai mari
dramaturgi ai lumii, Eugen Ionescu, cerea scriitorilor
și oamenilor de teatru să asimileze imaginarul științei.
Aceasta a reușit să întreacă în imaginație mult din ceea
ce a gândit mintea omenească anterior.
R.M.: Milioane de oameni s‑au întrebat ce o fi în
surâsul Giocondei. Mulți au spus că surâde ca o iubită,
562

Dezmeticindu-ne

ca o femeie însărcinată, ca o amantă. Alții au susținut
cum că ar fi chiar un autoportret al lui Da Vinci. Ați
văzut probabil în presă cum că în ochii Giocondei se
ascund mistere! Și ochii fac parte din surâsul Giocondei,
dar nu știam că acolo sunt litere și cifre.
S.M.: Volumul al II‑lea din ciclul meu de patru
volume de Paradigme universale, are chiar acest titlu:
Pornind de la un zâmbet; volumul începe cu o discuție
despre ambiguitatea surâsului, a zâmbetului. Râsul
se descifrează mai ușor. Arăt acolo că surâsul are o
ambiguitate atât de mare, încât numărul de moduri
posibile de interpretare, de decodificare a unui zâmbet,
este de ordinul zecilor de mii.
Mi s‑a întâmplat ca un zâmbet al meu de simpatie
să fi fost interpretat ca unul cu semnificație de ironie.
Și am plătit pentru această neînțelegere cu stricarea
unei relații umane. Să distingi un surâs ironic, de
unul amar, de unul de simpatie și de alte interpretări, e
foarte complicat. De multe ori, comunicarea umană în
general dă naștere mai degrabă la neînțelegere decât la
înțelegere și am să vă dau un exemplu care a devenit
celebru în teoria comunicării. Un profesor american,
pentru a testa valoarea zâmbetului, a făcut următorul
experiment. Într‑o dimineață, venit la departament, în
loc de How are you s‑a hotărât să spună fiecăruia You are
stupid, dar având grijă să însoțească aceste vorbe cu un
563

Solomon Marcus

zâmbet foarte pronunțat. Nimeni nu a înregistrat vorba
insultătoare.
R.M.: Oare câte războaie or fi ieșit pe lumea
aceasta dintr‑un surâs neînțeles? Mă întreb, cum i‑o fi
surâs Hillary Clinton lui Bill Clinton, de a ieșit, la un
moment dat, război în Iugoslavia?
S.M.: De aceea exista telefonul roșu între Kremlin
și Casa Albă, pentru asigurarea comunicării dintre cele
două mari puteri. Și nu se întrerupe niciun moment.
Este prima funcție a comunicării internaționale și anume
funcția fatică, ea asigură funcționarea canalului de
comunicare, pentru a se preveni astfel de accidente.
R.M.: Cred că România ar trebui să aibă sute de
mii de telefoane, de fire roșii între toți oamenii politici,
pentru că ei se pare că nu mai comunică. Vă puteți
explica de ce duce azi România o asemenea lipsă de
comunicare?
S.M.: Din fericire, există în societatea noastră
o componentă puternică de oameni minunați. La
această vârstă, nu numai că mă mențin, dar sunt și
activ, deoarece am știut să‑mi aleg partenerii și anume
studenții, elevii, colegii profesori, cercetători și nu doar
din România, ci din întreaga lume. Aceștia au fost mereu
selecționați, în măsura în care i‑am considerat ca fiind
pe aceeași lungime de undă cu mine. În mare măsură
depinde de noi să întreținem o anumită calitate umană,
564

Dezmeticindu-ne

care să ne asigure un anumit confort, fără de care nu e
posibilă o activitate de succes. Dacă noi am trăi personal
toate dramele cu care încep jurnalele de știri, cu crime și
diverse lucruri rele și dacă am sta să le punem la inimă
pe toate, viața ar fi imposibilă. Din păcate, mass‑media
nu înțeleg că altceva ar trebui ele să livreze populației.
R.M.: Dacă ați fi manager de televiziune și,
ascultându‑vă mi‑am dat seama că ați putea fi... cum
ați construi o televiziune care să asigure această
comunicare?
S.M.: Aș construi‑o, în primul rând, cu gândul
la funcția educațională pe care ar trebui să o aibă
televiziunea.
R.M.: Dacă nu funcționează, nu încasează bani și
vine patronul și spune să băgați niște manele, niște fetițe
ușor dezbrăcate... Care este drama aici?
S.M.: Vrând, nevrând, ajungem la problema
educației, televiziunea se face vinovată și ea de
degradarea educației. Televiziunea ar fi putut să fie un
factor esențial într‑o educație sănătoasă. Până și filmele,
emisiunile pentru copii, desenele animate, în mare
măsură se bazează pe violență. Este inacceptabil acest
lucru. Stimulăm în om cele mai primare porniri, în loc
să le descurajăm. Fără o mutație în politica televiziunilor,
educația nu‑și va găsi drumul cel bun.

565

Solomon Marcus

R.M.: Dar cum le vedeți? Veniți dintr‑o perioadă
în care televiziunea nu exista. Aveți 85 de ani. Mulți
înainte! Vă mențineți într‑o formă incredibilă! Acum
știm secretul, v‑ați înconjurat de oameni buni, tineri,
frumoși.
S.M.: Am parcurs o epocă tragică a istoriei. Mi‑am
trăit adolescența și prima tinerețe în timpul celui de Al
Doilea Război Mondial. Au urmat după aceea toate
nenorocirile aduse de comunism. Nu doar m‑am salvat
și am supraviețuit, ci m‑am salvat și din punct de vedere
intelectual și moral, pentru că am căutat acele insule de
curățenie și de omenesc. Întotdeauna, din fericire, găsim
în jurul nostru oameni exemplari.
R.M.: Până să existe această carte, Întâlniri cu
Solomon Marcus, academicianul avea propria imagine
despre sine. Aici există imaginea celorlalți despre
Solomon Marcus. Mi‑ați spus că este o diferență foarte
mare.
S.M.: Nu ne dăm seama cât de mult diferă imaginea
noastră pentru noi, față de imaginea noastră pentru alții.
Am fost profesor de la mijlocul secolului trecut, în mod
continuu.
R.M.: Deci din anii ̓50.
S.M.: Chiar în 1950 mi‑am făcut debutul didactic.
M‑am adresat întotdeauna unui amfiteatru, care la
început îmi era în mare măsură necunoscut.
566

Dezmeticindu-ne

R.M.: Cam cum sunt acești oameni?
S.M.: Acestor oameni încerc să le ghicesc interesele
și dorințele. Mi‑am dat seama, din mărturiile culese în
această carte, de la foști studenți ai mei, cei mai vechi
fiind și ei octogenari astăzi, că trebuie să ai curajul să dai
tot ce este mai bun în tine, chiar dacă nu știi exact dacă
e cineva care să sesizeze, să primească, să prelucreze,
ceea ce tu dai. Din această carte am aflat că, într‑adevăr,
au fost, în toate promoțiile de studenți cu care am lucrat,
studenți care m‑au înțeles poate chiar mai mult decât
m‑am înțeles eu. Trebuie să avem încrederea că există
o valoare umană, mult mai bogată decât ne‑o putem
imagina la prima vedere. Însă nenorocirea este că exact
valoarea umană rămâne mereu periferică, marginală. Ea
se ascunde, ea nu caută să intre în scena care se vede.
Mereu scena este ocupată mai mult cu false valori.
Mass‑media, din păcate, aduc în față astfel de situații,
pentru motivul că sunt mai spectaculoase. Mizează pe
această pornire primară, care există evident în oameni.
Pornire de a obține lucruri surprinzătoare fără niciun
efort intelectual sau afectiv sau de energie. Din această
cauză, noi nu realizăm cât de bogat este omenescul care
ne înconjoară. Chiar dacă nu îl vedem, el există. Această
carte a cules exact acest omenesc.
R.M.: Ați avut contact permanent cu românii ca
profesor, ca savant, ca cetățean. S‑a schimbat ceva în
567

Solomon Marcus

filonul acesta național de 70 de ani?
S.M.: S‑a schimbat contextul. Unii spun că „Ne
pleacă toate valorile”. Lucrurile însă nu sunt chiar atât
de simple. În această carte există mesaje de la peste o
sută de foști studenți ai mei, care s‑au răspândit în toate
colțurile planetei. Cei mai mulți dintre ei nu au părăsit
realitatea românească. Acest lucru se manifestă în foarte
multe feluri. De pildă, un fost student, acum în Noua
Zeelandă, mare profesor și foarte important în domeniul
său, în informatică, conduce doctoranzi în România.
Vine periodic în România și ține conferințe. Este într‑un
continuu metabolism cu cei din țară. Sunt nenumărate
exemple de acest fel.
R.M.: Cum a ajuns România să aibă succes în
matematică în anii ’80? Practic eram atunci o mare forță
prin matematicienii noștri, ulterior prin informaticienii
noștri.
S.M.: Acum suntem mai puternici decât atunci, dar
lucrul acesta devine acum mai greu vizibil, pentru că în
momentul de față nu mai contează atât de mult ca altă
dată locul geografic în care te afli. Există mulți români
care lucrează de zeci de ani departe de țară și care trăiesc
mult mai puternic realitățile românești decât mulți colegi
ai mei care sunt în România și care aproape că habar nu
au care sunt durerile acestui loc.

568

Dezmeticindu-ne

R.M.: Dar de ce noi? Am auzit de noi și de indieni.
De ce nu bulgarii? Aici a fost o școală specială, o
orientare specială pentru a scoate aceste talente?
S.M.: Știința este prin natura ei planetară. Omul
de știință are o nevoie organică de a comunica cu toată
lumea și de a circula mereu în nenumărate universități.
Ați discutat și cu colegul meu, profesorul Nicolae
Zamfir, și el membru al Academiei Române. Vă spun un
lucru, pe care el din modestie nu vi l‑a spus. S‑a întors în
România, după ce s‑a încununat de succes în S.U.A. și a
devenit foarte cunoscut în domeniul său. Deci cultura se
face la nivel mondial și se validează la nivel planetar.
R.M.: Pe finalul discuției noastre, avem alături
de noi atât părinți, cât și tineri. Le spuneți ceva despre
matematică? Vorbiți atât de frumos despre o știință
pe care mulți dintre noi am văzut‑o într‑o formă foarte
abstractă. Dvs. ați vorbit în termeni de filosofie, de
poezie, despre matematică. Ce le‑ați spune acum?
Fiindcă sunt convis că unii dintre ei vor fi deturnați.
S.M.: Merg mereu în școli și sunt antrenat să discut
cu elevi de toate vârstele. Mi‑am dat seama imediat
unde este rădăcina răului. Rădăcina răului se vede chiar
la copii de grădiniță. De cele mai multe ori, ei primesc
o educație în care predomină imperativul. Modalitatea
imperativă, de comandă. Elevul este antrenat în ideea
că trebuie să asculte ceea ce îi spun părinții acasă și
569

Solomon Marcus

educatorii la școală. Nu vreau să spun că nu trebuie să
facă lucrul acesta, dar dacă acesta este tonul principal
în educație, partida e pierdută. În educație contează
enorm modalitatea interogativă, dreptul copilului de a
pune întrebări, pregătirea copilului de a avea dreptul
să înțeleagă ceea ce i se spune, și apoi cea imperativă.
Cei mai mulți copii în România, atunci când nu înțeleg
un lucru, nu sar precum un resort să întrebe, să ceară
explicații. Ei nu au antrenamentul de a se îndoi, de a
depăși starea de da sau nu. Nu este educat să se mire
în fața unui lucru nou, să fie stimulat să‑și întrețină
această facultate de mirare. Nu e educat că are dreptul
de contemplare a unui lucru frumos, de manifestare a
nedumeririi.
R.M.: La statura dumneavoastră de academician, de
dascăl, ați spus la un moment dat, „Aberez, deci exist”.
S.M.: Nu am spus eu asta. Asta a fost o declarație a
unei eleve, după ce a ieșit de la examenul de bacalaureat.
Ea a spus unei colege: „Fii atentă, să nu te oprești niciun
moment. Să vorbești în mod continuu, să aberezi, dar să
nu taci. Pentru că profesoara, în momentul în care taci,
îți pune întrebări și nu poți să știi ce te întreabă”. Este
o mentalitate foarte interesantă, care vizează și pe unii
profesori. Dascălii au și ei de învățat din aceste reacții
ale elevilor și studenților.

570

Dezmeticindu-ne

R.M.: Încurajați această atitudine „Să aberăm și să
existăm? ˮ
S.M.: Evident că este o prostie imensă.
R.M.: Ați sugerat, la un moment dat, că elevii pot
să nu meargă zilnic la școală și educația s‑ar putea face,
poate pe internet.
S.M.: Ați pronunțat un cuvânt magic, „internetul”.
Marea noastră problemă este că internetul și
televiziunea, care ar trebui să fie doi aliați de mare
valoare în educație, au devenit adversari. De ce?
Pentru că nu se face educația frecventării sănătoase a
internetului. Internetul este o revoluție și copiii noștri
vor trăi mult mai puternic în această revoluție decât noi.
Copiii sunt lăsați să umble pe internet la întâmplare și
ajungem în situația aceasta de a blestema internetul, ca
și cum ar fi un drog. Televiziunea, la rândul ei, ar fi putut
să fie un aliat puternic al educației. Parcurgeți cele mai
multe canale de televiziune și nu găsești decât trivialul,
triumful trivialității. Este foarte trist.
R.M.: Dar, încă nu este târziu.
S.M.: Să sperăm!

571

