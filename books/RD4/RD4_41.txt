Prefață la
Gina Sebastian Alcalay: Poate mâine…,
Editura Universal Dalsi, București, 2004
Anii din preajma și din timpul celui de‑al doilea
război mondial, într‑un oraș din Moldova. A supraviețuit
sau nu. Tensiunea este atât de mare, clipa atât de dilatată,
încât, ca în situația celebrului personaj al lui Laurence
Sterne, ar fi nevoie de un an pentru radiografierea
mișcărilor sufletești ale unei singure zile.
Atât autoarea cărții de față, cât și autorul acestor
rânduri s‑au aflat, ca adolescenți, prizonieri ai
universului terifiant din acel oraș moldovean al anilor
̓40. După război, ea a ieșit treptat din raza observației
mele, dar unele semne îmi parveneau indirect. Acum, la
o distanță de o jumătate de veac, o regăsesc în această
carte, în care evenimentele datate încep cu anul 1948, dar
amintirile din perioada războiului revin obsesiv.
Unii spun: nazismul a durat numai 12 ani, pe
când comunismul s‑a extins pe cea mai mare parte a
secolului XX. Însă septuagenarilor de azi nazismul
le‑a marcat copilăria și adolescența, perioadă în care
timpul psihologic este mult mai bogat decât vârsta
429

Solomon Marcus

adultă, astfel încât, pentru o persoană care trăiește 75
de ani, primii 20 de ani reprezintă, sub aspectul trăirii
subiective, mai mult decât jumătate din viață. Putem
astfel înțelege de ce multe dintre personajele acestei
cărți sunt obsedate de amintirea războiului. Aceste
personaje sunt atinse de o dublă traumă; abia ieșite din
război, când tocmai începeau să simtă mirosul libertății,
se descoperă ostateci în întunecatul tunel, lung de câteva
decenii, al totalitarismului comunist. Rezultatul: o lume
de bolnavi care încearcă să‑și înfrângă înstrăinarea
(uneori chiar de prieteni, de familie și mai ales de ei
înșiși) și să‑și construiască o identitate. Autoarea nu
insistă în direcția tragică a acestor destine. Percutanța
observației psihologice este dublată de luciditate, fiecare
atitudine este cenzurată înainte de a deveni patetică și
acaparatoare.
Nu‑mi dau seama cât de mult intră, în interesul meu
pentru această carte, faptul că în ea se fac multe referiri
la oameni, evenimente, locuri, idei, emoții, obsesii care
au făcut parte și din existența mea; este pentru mine o
carte de stări, de spaime, de tresăriri și de așteptări, de
atmosferă, de entuziasme și dezamăgiri, de himere și de
halucinații care, strecurate în ființa mea în anii mei tineri,
nu m‑au mai părăsit niciodată.
Cât despre măsura în care aceste texte au un
caracter autobiografic, ea mi se pare nerelevantă, în
430

Dezmeticindu-ne

primul rând pentru că nu este clară. Paul Ricoeur,
în Temps et récit, argumentează că timpul istoriei se
distinge tot mai greu de timpul ficțiunii, tinzând să
se confunde cu acesta din urmă. Jean Baudrillard, în
La transparence du mal, se sperie de faptul că noile
generații nu mai pot înțelege nici cel de‑al doilea
război mondial, nici holocaustul, iar ceea ce vedem
întâmplându‑se azi confirmă această teamă.
Avem nevoie de această carte, atât de frumos
intitulată Poate mâine… Undeva autoarea mărturisește
că, încă foarte mic, băiețelul ei obișnuia să‑i spună: „Tu
te naști mâine!” Acest fapt este în concordanță cu datele
psihologiei, conform căreia abia la vârsta de doi ani și
jumătate începe un copil să înțeleagă cuvântul mâine (iar
pe ieri îl asimilează și mai târziu, spre trei ani). Ca adulți,
intrăm într‑o rutină a unui ieri‑azi‑mâine derizoriu, al
ceasurilor de diferite feluri. Dar universurile de ficțiune
pe care ni le construim ne ajută să transgresăm această
rutină și să regăsim, de astă dată prin efort intelectual,
deruta temporală a copilului și frumoasa lui ezitare între
ieri și mâine. Ceea ce copilului i‑a fost dat de natură și
este considerat ca o privațiune adultului îi este dat prin
cultură și se constituie într‑o adevărată performanță.

431

