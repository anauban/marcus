Ipostaze ale Europei
Ateneu, anul 28, nr.1 (254), ianuarie 1991, p. 2
Viaţa m‑a adus mereu în faţa unei alte Europe.
Adolescent fiind, am fost fascinat de valorile culturii
occidentale, în special cele literare şi filosofice. La vârsta
de 16‑17 ani eram hipnotizat de Critica Raţiunii Pure a
lui Kant şi trăiam sub vraja poeziei lui Baudelaire. Dar
anii imediat următori m‑au adus în faţa unei alte Europe,
ruşine a omenirii şi istoriei : oroarea hitleristo‑fascistă.
În momentul în care m‑am eliberat de coşmarul
războiului, sub a cărui ameninţare am trăit câţiva ani,
cu teama că nu‑i voi supravieţui, m‑am îndreptat cu
fermitate spre matematică. Am descoperit în această
ştiinţă o sursă supremă de satisfacţii intelectuale, aptă
să motiveze o viaţă. Mi‑am dat repede seama că aproape
întreaga istorie a acestei ştiinţe este o parte a culturii
europene. Preluând tradiţiile cele mai valoroase ale
matematicii greceşti, matematica modernă are o istorie
de vreo 400 de ani, desfăşurată cu precădere în patru ţări
ale Europei Occidentale: Anglia, Franţa, Germania şi
Italia. Întregul limbaj al ştiinţei moderne este de sorginte
europeană, istoria sa fiind legată de nume ca Galilei,
200

Dezmeticindu-ne

Descartes, Newton şi Leibniz, pentru a nu‑i menţiona
decât pe întemeietori. Să observăm însă şi cealaltă faţă
a medaliei: marxismul s‑a născut în centrul Europei,
dar ştafeta acestei doctrine, nu lipsite de interes, a fost
preluată de tirani care au înfăptuit cel mai mare genocid
cunoscut în istorie, o bună parte a sa producându‑se
în Europa. Generaţia mea a fost marcată de acest
eveniment.
Acest aliaj de grandoare şi decadenţă, de mândrie şi
ruşine, care străbate istoria Europei, a iradiat pe întreaga
planetă, transformând Europa într‑o sursă de bine şi
de rău care nu a cruţat aproape nicio ţară din lume.
Europa este prima care a lichidat sclavagismul şi tot ea
a trecut prima la lichidarea feudalismului. Capitalismul
s‑a născut în Europa şi a dat naştere colonialismului
practicat de ţări europene dintre cele mai avansate, în
Africa, Asia, America şi Australia. Acestui colonialism
i s‑a pus capăt abia în secolul nostru. Însă în prima sa
fază, colonialismul a fost o acţiune normală, rezultând
din aspiraţia civilizaţiei europene de a‑şi extinde
cunoaşterea şi de a întreprinde noi descoperiri. Abia
la începutul secolului nostru, reacţia anticolonialistă a
devenit de o legitimitate clară.
Tot în Europa s‑a născut şi socialismul, sub al
cărui steag s‑au desfăşurat mişcări de idei şi partide
politice dintre cele mai eterogene, unele dintre ele
201

Solomon Marcus

conducând la ideologiile totalitare bine cunoscute,
altele constituindu‑se în modalităţi care întrunesc multe
adeziuni ale lumii civilizate.
Dubla faţă a Europei ne urmăreşte şi ne obsedează.
În Europa s‑au creat epopeile homerice şi Elementele
lui Euclid, teatrul lui Shakespeare şi teoria gravitaţiei
universale, teoria electromagnetismului şi darwinismul,
genetica şi psihanaliza, teoria relativităţii şi mecanica
cuantică, după cum tot în Europa, atât în Est cât şi în
Vest, s‑au produs valorile filosofice, literare şi artistice
atât de bine cunoscute. Dar ne pot consola toate acestea
pentru faptul că tot Europa a fost locul de declanşare a
două războaie mondiale şi tot ea poartă răspunderea
apariţiei celor mai barbare ideologii cunoscute în istorie?
Europa a dictat lumii şi binele şi răul, ea s‑a
constituit într‑un centru de putere şi de inteligenţă. A
înţelege Europa înseamnă nu numai a urmări acţiunile
ei binefăcătoare, ci şi sistemul de opoziţii în care ea s‑a
înserat. Începutul secolului nostru marchează începutul
unei tensiuni care, de atunci încoace, avea să se amplifice
mereu; este vorba de relaţia Europei cu două puteri ale
economiei mondiale: S.U.A. şi Japonia. Tot atunci, aşa
cum am observat, a început şi tensiunea dintre Europa
şi coloniile ei, dar sistemul colonial al ţărilor din Vestul
Europei a fost treptat lichidat abia după al doilea război
mondial.
202

Dezmeticindu-ne

Concomitent cu opoziţiile dintre Europa şi
restul lumii, s‑au dezvoltat opoziţiile din interiorul
Europei. Ciocnirea dintre capitalism şi comunism avea
să evolueze până la cele două blocuri militare care
încă scindează Europa. Este uimitor însă cât de rapid
evoluează lucrurile. Numai în urmă cu vreo doi ani nu
i se întrevedea încă acestei scindări un sfârşit apropiat;
iar astăzi, perspectiva lichidării ei se conturează cu
deosebită claritate. Dacă până mai ieri se vorbea cu
insistenţă despre două Europe, mulţi identificând Europa
cu Europa Occidentală, astăzi metafora Casei comune
europene este acceptată aproape de toţi. Dar, desigur,
rămân şi alte opoziţii în interiorul Europei, despre care
însă ne propunem să discutăm altă dată.
În ultimii 13 ani, mi s‑a deschis o nouă înţelegere
a poziţiei Europei în lume, datorită colaborării mele
la unele proiecte ale Universităţii Naţiunilor Unite, cu
sediul la Tokio. Am înţeles astfel că europocentrismul
a intrat în opoziţie puternică faţă de lumea a treia, care,
în faza ei postcolonialistă, de independenţă cel puţin
aparentă, se afla (se mai află oare şi astăzi ?) în căutarea
unor căi proprii de valorificare a resurselor ei interne
şi de dezvoltare a tradiţiilor ei specifice. S‑au elaborat,
în acest sens, proiecte pentru patru regiuni distincte ale
planetei: Africa, lumea arabă, Asia şi America Latină.

203

Solomon Marcus

Însă concomitent cu opoziţia dintre Europa
şi o lume a treia decolonizată, în luptă cu propriul
ei complex de inferioritate, se dezvoltă şi o altă
opoziţie, mai profundă, care se accentuează mereu.
De la o Europă înţeleasă în primul rând ca civilizaţie
occidentală, s‑a ajuns treptat la înţelegerea unei entităţi
mai cuprinzătoare, în care Europa s‑a inserat, aceea
a Nordului bogat, opus unui Sud în mizerie. Dincolo
de tensiunea cu S.U.A. şi Japonia, Europa avea să‑şi
sublinieze numitorul ei comun cu acestea două: o
considerabilă dezvoltare a civilizaţiei industriale şi a
serviciilor, pe fondul unui uimitor progres tehnic.
Care este situaţia în momentul de faţă? Sub toate
aspectele, asistăm la o globalizare a vieţii sociale, prin
interdependenţe economico‑sociale şi informaţionale tot
mai accentuate. În perspectivă, este pusă în chestiune
nu numai identitatea Europei, ci şi a oricărei alte părţi
a planetei. Această planetă a devenit, din multe puncte
de vedere, un sat. Tot ceea ce se întâmplă într‑un punct
al ei, are repercusiuni relativ rapide asupra restului şi
se află imediat, în orice loc. Europa devine o uliţă unde
fiecare ştie totul despre fiecare. Această nouă etapă
este o consecinţă a trecerii societăţii umane de la etapa
substanţialist‑energetică la cea informaţională.
Cel puţin aparent, Europa nu s‑a mai aflat, de
astă dată, în frunte, nu a mai dat ea tonul, cedând locul
204

Dezmeticindu-ne

Statelor Unite ale Americii şi Japoniei. La o observare
mai atentă a fenomenelor, constatăm însă că cele mai
multe forţe ştiinţifice şi tehnice care au realizat, în
S.U.A., exploatarea energiei atomice şi care au construit
primele calculatoare electronice, sunt de origine
europeană! America ştiinţifico‑tehnică este, în bună
măsură, un produs european.
Un aspect global dintre cele mai evidente este acum
cel ecologic. Prin mecanismul „efectului de fluture”,
distrugerea unor păduri braziliene poate deveni o
catastrofă ecologică pentru Europa. Structura holografică
a universului în care trăim nu a fost niciodată mai clară
ca acum. Fiecare parte răspunde pentru tot întregul.

205

