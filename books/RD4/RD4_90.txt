Vorba de cultură
Invitat Solomon Marcus,
Radio România Cultural, 19 decembrie 2011
Ema Stere: Bună ziua, stimați ascultători! În
această săptămână emisiunea Vorba de cultură are un
invitat cu totul special, pe dl. acad. Solomon Marcus. Îmi
este greu să încep conversația cu domnia sa. Îl cunoașteți
de mult, din publicații, din activitatea transdisciplinară,
termen care mi se pare chiar sărac în context.
Sunteți un bun comunicator, pe lângă un bun cercetător.
S.M.: Vă revine dvs. să apreciați. Mulțumesc!
E.S.: Vi se pare important ca un cercetător să
țină legătura cu publicul, sau, până la urmă, e treaba
publicului să se intereseze de ce se mai întâmplă în
științe, în arte?
S.M.: Până în secolul al XIX‑lea, limbajul științific
nu era foarte diferit de limbajul cotidian. Oamenii de
știință comunicau mult cu publicul. Pot să dau exemple
chiar din istoria românească: Țițeica, Lalescu, cu 100
de ani în urmă, scriau mereu articole științifice pentru
marele public. Între timp, decalajul dintre limbajul
științelor și cel cotidian a crescut atât de mult, încât acest
914

Dezmeticindu-ne

lucru a devenit mai dificil. Oamenii de știință își creează
tot mai greu drumul spre acest tip de comunicare și chiar
există un blocaj. În domeniul matematicii, comunicarea
este foarte slabă. Cei mai mulți matematicieni au o
mare dificultate în a explica publicului cu ce se ocupă.
Și nu numai matematicienii, ci și fizicienii, într‑o
măsură destul de mare. Chiar și în biologie este mai
greu să explici în ce constă, de pildă, marele proiect al
genomului uman. Toate presupun o anumită pregătire, pe
care din păcate școala nu o furnizează decât într‑o prea
mică măsură.
E.S.: Este adevărat că în fiecare an când se anunță
câștigătorii Premiului Nobel, nu mai înțelegem pentru ce
au câștigat acest premiu. Ne‑am aștepta la medicină, de
pildă, să se câștige Nobelul pentru vindecarea cancerului.
De fapt, se câștigă pentru o mică descoperire.
S.M.: Se întâmplă uneori și lucrul acesta, dar de
cele mai multe ori isprăvile lor greu pot fi explicate
marelui public. Știința pe care o învață elevii la școală,
pe care o știe cel care are la activul său bacalaureatul,
este de acum 100–200 de ani. Nu e știința de azi.
E.S.: Există o anumită rezistență la schimbarea
sistemului de învățământ.
S.M.: Știința de azi a pătruns doar într‑o
mică măsură în programele școlare și chiar în cele
universitare. Există un decalaj mare între nivelul
915

Solomon Marcus

cercetării în știință și nivelul didactic, cel la care se
desfășoară educația școlară sau universitară.
E.S.: Dar vi s‑ar putea răspunde că este destul de
grea școala, ca dovadă rezultatele tot mai slabe.
S.M.: Nu e grea din cauza naturii științei, ci
pentru că pornește de la început cu un scenariu greșit.
Scenariul de bază al școlii și al educației în familie este
unul care pune în prim‑plan datoria. Tu, copil, ești dator
să înveți, să te pregătești pentru mai târziu. Accentul
cade pe datorie în educația în familie și în cea școlară.
Or, învățarea cere un efort. Acest efort, mai cu seamă
la copil, cere o recompensă imediată. Dacă unui adult
îi poți spune că ceea ce învață îi va folosi mai târziu și
poate înțelege, la un copil așa ceva nu funcționează.
Nu poate face un efort cu rezultate bune decât dacă
recompensa vine imediat sau concomitent cu procesul
de învățare, sub forma plăcerii. Copilul ar trebui să simtă
că ceea ce i se cere corespunde unei așteptări naturale pe
care o are, o curiozitate naturală, o bucurie. Trebuie să
descopere acolo un element ludic. Dacă învățarea nu este
însoțită de plăcere, de curiozitate naturală, nu își atinge
scopul. Chiar dacă elevul reține niște lucruri, copiii
pot memora și lucruri pe care nu le înțeleg, nu le reține
pentru mult timp.
Uneori apare pe Canalul Cultural al TVR un
slogan al lui Antoine de Saint‑Exupéry, „A face cultură
916

Dezmeticindu-ne

înseamnă a produce setea, restul vine de la sine”. În
sistemul actual educațional, noi nu producem la copil
setea care să facă naturală asimilarea cunoștințelor.
Îi furnizăm cunoștințe, îi pretindem să le rețină, să
le restituie la examen. Nu ne preocupăm să creăm în
prealabil setea de aceste cunoștințe. E ca și cum în loc
să mâncați în măsura în care vi se face foame, ați mânca
la întâmplare și ați fi obligată să mâncați când nu vă este
deloc foame. Aceasta este una din bolile de bază ale
educației.
E.S.: Nu este aceasta o etapă obligatorie? Cineva
care se apucă să învețe o limbă străină trebuie să treacă
de o anumită perioadă în care învață mecanic, în care
„tocește”. Cineva care învață să cânte la un instrument
trebuie să învețe mai întâi gamele, să învețe cum se
manevrează instrumentul respectiv. Abia după aceea să
poată ajunge la bucurii mai mici sau plăceri mai mari.
S.M.: A învăța o limbă străină, în aceste condiții,
este o premisă a eșecului. Stau și mă gândesc cum am
învățat eu franceză și engleză. În primul caz, nu a contat
franceza pe care am învățat‑o la școală. A contat cea pe
care am învățat‑o având la bază o motivație profundă:
atracția pentru literatura franceză. Eram adolescent.
Copiii care sunt puși să învețe limbi străine sau să cânte
la pian la o vârstă fragedă le învață aparent. La facultate,
dacă li se dă să citească, pentru lucrarea de diplomă, un
917

Solomon Marcus

articol în limba respectivă, nu o pot face, pentru că n‑au
avut ocazia să o practice într‑un mod natural, cerut de
viață. Învățarea unei limbi străine fără să ai o motivație
nu dă mari roade.
E.S.: Vă referiți aici la o modificare de manuale, de
programă școlară, sau la un efort pe care trebuie să‑l facă
fiecare profesor?
S.M.: Programele și manualele trebuie modificate
exact în funcție de ce am discutat până aici. Ele trebuie
să țină seamă de ceea ce copilul poate să înțeleagă și, pe
baza faptului că poate să înțeleagă, e posibil să apară la el
și plăcerea de a le înțelege. Dacă nu creăm aceste premise,
eșuăm. Programele sunt prăfuite, trec de la o generație la
alta, din comoditate, din rutină și acum au ajuns la decalaj
enorm față de ceea ce înseamnă lumea, viața de azi.
E.S.: Ele urmează un algoritm. Spuneați și dvs. în
Singurătatea matematicianului că matematica are un
algoritm inevitabil. Nu poți lua o carte de matematică, să‑i
rupi primele pagini și să înțelegi din acel punct mai departe.
S.M.: Nu e vorba de algoritm, ci de faptul că în
matematică există o legătură organică între diferitele
părți. E o dependență a fiecărei părți de cele anterioare,
mult mai puternică decât în alte domenii.
E.S.: Poate că asta a încercat programa școlară. Să
stabilească aceste părți într‑o succesiune care a părut
logică la momentul respectiv.
918

Dezmeticindu-ne

S.M.: Faptul că aceasta este una din caracteristicile
matematicii e una și modul în care facem educația
matematică este cu totul altceva. În primul rând, nu e
natural ca la copiii mici matematica să vină singură.
Faptul de a avea discipline separate la copii de școală
primară este o greșeală. Ei ar trebui să aibă o singură
educație, în care să se amestece toate. Despărțirea pe
discipline trebuie să vină mult mai târziu.
Gândiți‑vă că modul în care se formează copilul
repetă în bună măsură istoria. Dacă privim istoria,
constatăm că separarea culturii pe discipline a venit
foarte târziu. A venit în mod explicit abia în urmă cu 200
de ani. Atunci când a venit și toată această proliferare a
unor specialități din ce în ce mai înguste. Până atunci,
cultura a avut un caracter predominant sincretic. Diferite
preocupări se dezvoltau laolaltă. Pentru Pitagora,
educația matematică mergea mână în mână cu educația
muzicală, tocmai din această cauză. Cultura avea o
unitate organică. Mai târziu, a continuat cam în același
fel. Metoda axiomatic‑deductivă practicată de Euclid
în geometria sa a devenit o metodă de bază în toate
domeniile: în domeniul moralei cu Spinoza, în domeniul
teologiei. Toți teologii catolici din Evul Mediu foloseau
metoda axiomatică a lui Euclid. Deci, din moment ce
împărțirea pe discipline a venit istoric vorbind atât
de târziu, nu e clar că și în educația unui copil trebuie
919

Solomon Marcus

să vină la momentul potrivit, care, zic eu, ar fi pe la
mijlocul gimnaziului? Cam ceea ce reprezintă acum
clasele a VI‑a și a VII‑a. Am făcut experimente de acest
fel la Liceul „Gheorghe Lazăr”.
Am văzut cum se face educația în Finlanda. Copiii
nu învață numerele și operațiile elementare la modul
abstract, ca la noi. Ei nu învață nimic despre numărul
5. Asociază numărului 5, cuvântul „Euro” și spun „să
ne întrebăm ce se poate cumpăra cu 5 Euro”. Și arată
că toate veșmintele pe care ei le poartă sau alimentele
pe care le consumă se cumpără cu un anumit număr de
Euro. Deci învață numerele ca prețuri ale obiectelor
cu care se întâlnesc în viața cotidiană. Operațiile de
adunare, de scădere sunt făcute în raport cu aceste
operații pe care le fac părinții lor ca să le procure cele
necesare. Iată că primele noțiuni de aritmetică sunt
predate în contact direct cu viața din mediul lor natural.
E.S.: Aceasta înseamnă că nu se deosebește operația
de adunare, spre exemplu, de înmulțire. În momentul
în care se prezintă un număr sub forma sa utilitară,
sub forma sa de preț, înseamnă că se predau operațiile
matematice în context?
S.M.: Da. Evident că la copiii foarte mici se predă
doar numărătoarea, apoi adunarea, și tot așa într‑o
anumită ordine de dificultate.

920

Dezmeticindu-ne

S.M.: Ideea este foarte simplă: să pleci de la
universul său de viață. El va înțelege cel mai bine
mediul în care el se mișcă în viața cotidiană, lucrurile
cu care el se întâlnește în mod curent. Acesta este
modul cel mai natural de a te asigura că te înțelege și
că vii în întâmpinarea unor așteptări naturale. Imediat
vine întrebarea: de unde au părinții bani, ca să cumpere
toate acestea? Și dintr‑o dată se deschide o poartă
extraordinară de a se descrie diverse ocupații. Ocupații
pe care le au părinții lor, care sunt folositoare oamenilor
și pentru care ei primesc în schimb acești bani cu care
le cumpără lor ce au nevoie. Imediat se face legătura cu
mediul social. Tocmai faptul că nu mai respect rigorile
unei singure discipline îmi dă posibilitatea să rămân
mereu în contact cu viața, așa cum se prezintă ea în mod
natural. Dacă le iei pe rând, se poate spune că ocupațiile
te duc la chestiuni de economie ș.a. Tocmai asta e
interesant, că toate se amestecă.
Relația lor cu părinții ori cu bunicii lor imediat îi
conduce la curiozitatea de a afla ceva din istorie și din
geografie. Unde s‑a născut tatăl tău? Unde e locul acela?
Imediat începe să învețe geografie. Când s‑a născut? În
deceniul al doilea al secolului trecut. Să vedem, ce se
întâmpla atunci? Învață istorie.
Viața nu e împărțită pe discipline. Împărțirea pe
discipline este o operație foarte artificială, pe care o
921

Solomon Marcus

înțelegi abia târziu, după ce lucrurile capătă o anumită
complexitate.
E.S.: Cu ani în urmă, un profesor mi‑a spus: atât
timp cât încerc să explic orice printr‑un joc, anecdotă,
poveste, copiii sunt atenți. Dar am constatat că dacă nu
sunt totuși foarte sever cu evaluarea lor, dacă nu‑i pun
să învețe acasă, ei rămân cu anecdota și nu rețin ceea ce
trebuie.
S.M.: Metoda aceasta cu anecdota nu este corectă.
Noi trebuie să descoperim jocul din interiorul învățării.
Învățarea ca atare are o componentă ludică. Ea este
rezultatul natural al unor facultăți cu care copilul se
naște. Copilul abia născut începe să tatoneze tot ce
este în jurul lui, are o curiozitate extraordinară, pe care
încearcă să și‑o satisfacă cu posibilitățile pe care le are.
Urmăriți cu atenție comportamentul copilului foarte mic
și veți vedea că are o curiozitate permanentă. Trăiește
mereu sub semnul unor întrebări, pe care nu e capabil
să le formuleze. Pipăie tot ce vede în jur, se uită la ele,
încearcă să desfacă jucăriile. La bază este dorința de
a înțelege. Tot secretul educației este ca noi să știm
să educăm această curiozitate, pe de o parte de a nu o
descuraja și apoi de a o orienta. Dacă ea nu e asociată
cu o anumită educație, poate să opereze în mod anarhic,
cum se întâmplă cu copiii care umblă pe internet la
întâmplare pentru că nu li s‑a făcut educația frecventării
922

Dezmeticindu-ne

internetului. Și aceasta este una dintre marile probleme
ale educației, astăzi.
Trebuie să educăm această capacitate a copilului de
a fi curios, de a pune întrebări. Învățarea este rezultatul
acestor stări. Cea de curiozitate, de interes, de întrebare,
pe care școala de azi, de la noi, o descurajază. Chiar de
la educația în familie. Cum se comportă un părinte când
copilul său pune întrebări? La prima întrebare încearcă
să‑i răspundă. La a doua mai are încă răbdare. La a
treia sau a patra întrebare se enervează. Cam acesta e
scenariul obișnuit.
I‑am întrebat pe elevi cam ce înțeleg ei prin copil
cuminte, iar unul dintre ei mi‑a raspuns că un copil
cuminte nu vorbește neîntrebat. Deci noi descurajăm
întrebarea. În general, mergând pe acest scenariu, al
profesorului care trebuie să‑și îndeplinească un anumit
plan cu orice preț, decidem că elevii care pun întrebări
sau formulează obiecții îl abat de la acest plan, deci
trebuie descurajați în acțiunea lor.
E.S.: Acest lucru este adevărat și în multe părți în
mediul academic. În universități, profesorul vine cu un
curs de acasă, iar dacă este întrerupt la fiecare 5 minute
de întrebări nu mai reușește să și‑l țină.
S.M.: Da. Noi suntem în situația inversă. Aici
este una dintre rănile de bază. Copilul, elevul nu sunt
antrenați ca, atunci când nu înțeleg ceva, să întrebe.
923

Solomon Marcus

Nu sunt educați în ideea că nu trebuie doar să rețină
lucrurile, ci să le și înțeleagă.
E.S.: În plus, a pune o întrebare de multe ori
presupune să cunoști cât de cât domeniul cu pricina.
S.M.: Nu. Nu se pun întrebări la lucrurile cele mai
elementare. De pildă, să se ceară explicația unui cuvânt
necunoscut pe care profesorul l‑a folosit.
Am propus unor elevi să discute o poezie, în cadrul
concursului „Plus sau minus poezie”. Analizând lucrările
lor, mi‑am dat seama că ei, de fapt, n‑au înțeles anumite
cuvinte din poezia respectivă. I‑am întrebat de ce nu au
scris că nu le înțeleg. Răspunsul a fost: de frică să nu
li se scadă nota. Sunt lucruri aparent minore, dar aici e
rana. Și la bacalaureat vin candidați care au foarte multe
cunoștințe, dar numai o mică parte din aceste cunoștințe
sunt lucruri pe care le‑au și înțeles. Mai târziu, când stați
de vorbă cu oameni de 60‑70 de ani și îi invitați să‑și
amintească de școală, vă răspund că din matematica
școlară nu‑și amintesc decât câțiva termeni ca logaritm,
sinus, fără să fi înțeles ce‑i cu ei.
E.S.: Dar să știți că oamenii care au acum 60‑70 de
ani au făcut școala și au învățat despre sinus și logaritm
în perioada educației comuniste, care e considerată de
mulți un moment mult mai bun decât cel actual.
S.M.: Aceasta este altă poveste falsă.

924

Dezmeticindu-ne

E.S.: Și eu cred că e o poveste falsă, dar recunoașteți
că în ultimii ani s‑a dezvoltat un mit al învățământului
comunist, care ar fi fost mult mai eficient.
S.M.: Eu am făcut școala înainte de război și
păcatele despre care vă vorbesc existau încă de atunci.
Educația matematică s‑a orientat spre sloganul american
know‑how, să știi cum să faci. De pildă, procedeul pentru
rezolvarea ecuației de gradul al II‑lea, aplici formula
și gata. Școala ne‑a furnizat o serie de procedee, dar
nu a dat atenție înțelegerii conceptelor, ideilor care se
află într‑o anumită materie. Nu a dat atenție istoriei și
întregului context cultural. Fiecare manual este 90%
o întreprindere pe cont propriu. Legăturile cu alte
discipline, cu viața, sunt aproape inexistente. Școala
creează un scenariu complet fals, față de viața autentică.
Deodată, intri în viață și constați că aproape nicio
problemă nu se poate aborda cu ajutorul unei singure
discipline.
E.S.: Permiteți‑mi să exagerez puțin, domnule
Solomon Marcus. Propuneți așadar, pentru un școlar mic,
o învățătoare care să vorbească despre orice, fără a avea
în orar materii precum matematica, româna, geografia.
S.M.: Vă înșelați dacă credeți că finlandezii n‑au un
itinerar clar. Au unul, dar de altă natură. Credeți că, dacă
nu împart cunoașterea pe discipline, introduc anarhia?

925

Solomon Marcus

E.S.: Nu mă refeream la anarhie, poate e un
sistem bun. Eu mă întrebam dacă propuneți așa ceva.
O abordare complexă în care să dispară granițele dintre
discipline.
S.M.: Până la un anumit moment, da. Cred că
până prin clasa a VI‑a de gimnaziu nu trebuie împărțită
materia pe discipline. E necesar să vă gândiți bine care
sunt posibilitățile copilului. Cunoașterea conceptuală,
rațională începe abia la vârsta gimnaziului. Până atunci,
predomină la copil cunoașterea empirică, senzorială,
intuitivă. Istoria ne vine iar în ajutor. Matematica
babiloniană era bazată în primul rând pe percepție
empirică și intuiție. Modul rațional‑demonstrativ, pe
bază de teoreme, începe abia la vârsta gimnaziului. Până
atunci, educația trebuie să se bazeze predominant pe
aspectele empirice, experimentale, senzorial intuitive.
Noi nu respectăm aceste lucruri.
E.S.: Vă propusesem să vorbim astăzi despre viața
dvs. Iată că am vorbit numai despre educație. Aș mai
avea să vă întreb ceva de final. Vorbiți din perspectiva
unui om de foarte mare succes. În copilărie, ați fost un
școlar cuminte, în sensul în care v‑a spus copilul acela?
S.M.: N‑am fost. Din această cauză, am avut parte
și de eliminări. Primul meu act de necumințenie a fost
în clasa a II‑a de gimnaziu, când ni s‑a dat să scriem ce
credem despre profesorii noștri. Ni s‑a spus că putem
926

Dezmeticindu-ne

scrie orice, fără să ne temem, puteam să nici nu ne
semnăm. Eu am luat în serios această invitație. Am scris
că am aflat că un profesor de română din școală a luat
nota 2 la examenul de capacitate pentru profesori. Am
fost eliminat pentru 10 zile și obligat să vin cu tatăl meu
la el acasă, să mă dojenească. Deci am avut probleme.
Nu am fost un școlar de nota 10, în general.
E.S.: Știți că asta devine un mit paralel, cel al
geniilor.
S.M.: Exagerați când spuneți că am un foarte
mare succes. Am trecut și prin succese și prin eșecuri.
S‑au împletit și nu cred că e posibil altfel. Rezistența la
greșeală și la eșec este esențială. Aici este iar una dintre
marile scăderi ale educației. Greșeala este considerată în
toate cazurile un lucru rău.
E.S.: Greșeala se pedepsește în școală.
S.M.: Dar are și o față pozitivă. Există o întreagă
tipologie a greșelilor. Există unele care pot fi asimilate
cu infracțiuni, dar există și greșeli care reprezintă prețul
inevitabil pe care trebuie să‑l plătim pentru ca gândirea
personală să fie posibilă.
E.S.: Vă mulțumesc, dle. academician! Vă
mulțumesc și dumneavoastră, stimați ascultători!

927

