Are matematica o valoare culturală?
România literară, 15‑21 septembrie 2004, anul XXXVII
Fragment din studiul „Matematica în România”
Matematica, între păcat și coșmar
Percepţia publică a matematicii este încă dominată
de confuzie. Nici măcar oamenii de cultură, cea mai
mare parte a intelectualilor, nu înţeleg cu ce se ocupă
matematicienii. Reprezentarea obişnuită este aceea care
vede în matematician un om al cifrelor, iar în matematică
o ştiinţă a numerelor şi cantităţii; „matematic” este
echivalat cu „precis”, cu „exact”. Dar de aici nu‑i decât
un pas până la reducerea matematicianului la cel care este
incapabil să distingă nuanţele, la cel blocat de rigiditate,
de fixitate, de simplism, de logica lui „da” sau „nu”,
şi astfel ne găsim confruntaţi cu tradiţionala opoziţie
pascaliană dintre spiritul geometric şi cel de fineţe
(„fotbalul e o chestie complicată, nu‑i ca matematica, doi
şi cu doi fac patru”, observa un celebru jucător român de
fotbal). Un actor îşi amintea, în momentul în care fusese
premiat, de recunoştinţa pe care i‑o poartă fostului său
profesor de matematică, nu pentru ce l‑a învăţat, ci pentru
faptul de a‑l fi înţeles că „n‑are el treabă cu matematica”.
458

Dezmeticindu-ne

Un venerabil nonagenar, exeget al artei brâncuşiene,
îl evoca pe fostul său profesor de matematică, nu altul
decât Dan Barbilian, accentuând retoric şi fluturând ca
un stindard adresarea acestuia către el şi colegii săi:
„Sunteţi nişte troglodiţi!”, după ce constatase ignoranţa
lor matematică. Un istoric octogenar, întrebat despre
perioada şcolarităţii sale, ţine şi el să pună în contrast
pasiunea sa pentru istorie cu nulitatea sa la matematică.
O atitudine similară se regăseşte la mulţi alţi intelectuali
umanişti. Ignoranţa în materie de matematică, lipsa de
interes pentru ea nu sunt ascunse, ci strigate cu plăcere,
arborate ca un stindard, probabil pentru a mări, prin
contrast, imaginea pasiunii lor pentru „umanioare”.
Un pictor și scriitor din Iași, invitat să‑și evoce anii de
școală, ne spune, în România Literară, că trăiește și
acum coșmarul examenelor de matematică. Un cântăreț
își amintește, în Jurnalul Național, că a fost de două ori
corigent la matematică şi că nu se împăca cu geometria.
Ancheta ziarului constată că viitoarelor vedete ale
muzicii uşoare nu le‑au plăcut ştiinţele exacte. La aceste
mărturii recente putem adăuga şi altele, mai vechi.
Criticul literar Şerban Cioculescu scria, în 1978: „Cât
despre metodele matematice, ele mă sperie şi, barbar,
rămân la ele rebarbativ”. În 1954, psihologul Mihai Ralea
vedea în psihologia matematică un refugiu al concepţiilor
idealiste în psihologie. Logicianul Petre Botezatu
459

Solomon Marcus

considera că „matematica trebuie tradusă în limbajul
uzual”, pentru a o face accesibilă (lucru tot atât de
imposibil ca şi traducerea poeziei în limbaj uzual). Ştefan
Odobleja, într‑o carte postumă din 1979, considera că „a
matematiza este a traduce, iar traducerea nu‑l transformă
pe traducător în autor. Dacă se traduce Regele Lear în
româneşte (continuarea poate fi uşor ghicită).”
Atitudinea inginerilor ‑ între rezervă și atracție
Există însă şi alte puncte de vedere. Scriitorul
Mircea Horia Simionescu se arată, într‑un interviu din
România literară, foarte interesat de teoria fractalilor, în
care întrevede pattern‑uri de semnificaţie literar‑artistică.
Poeta Ioana leronim, într‑un interviu din Revista 22, se
declară ataşată de metaforele matematice ale poeziei. Un
inginer mecanician ca Radu Voinea şi un inginer chimist
ca Alexandru Balaban considerau, în 1982, că prezenţa
matematicii în pregătirea inginerilor este esenţială, iar
cel din urmă cerea să se mărească locul matematicii
la examenul de admitere în politehnică. În schimb,
alţi reprezentanţi ai ingineriilor de diverse specialităţi
nu au manifestat entuziasm față de matematică. Un
hidroenergetician ca Dorin Pavel consideră că în
pregătirea inginerilor se acordă prea mare atenție
matematicii, de la admitere până la absolvire, în dauna
formării gândirii inginerești.
460

Dezmeticindu-ne

Pentru un inginer metalurgist ca D. Drimer
matematica este nu atât un mod de gândire, cât o simplă
unealtă. (A se vedea, pentru toate acestea, Lucrările
Simpozionului privind eficienţa predării matematicii în
învăţământul tehnic superior, Editura Tehnică, Bucureşti,
1982).
Când nu este diabolică, matematica poate fi
îngerească. Actriţa Emilia Popescu, într‑o emisiune la
Profesioniştii cu Eugenia Vodă, se mândrea cu faptul că
fiica ei este foarte bună la matematică, iar o altă actriţă,
Oana Pellea, îşi aminteşte şi ea că a fost foarte bună la
matematică. Alte ori, sub forma unei aparente laude, se
ascunde ideea incompatibilităţii dintre matematică şi
cultura umanistă. Iată, de exemplu, ce declara lingvistul
Iorgu Iordan (în De vorbă cu Iorgu Iordan, de Valeriu
Mangu, Ed. Minerva, 1982): „Un limbaj universal este
însă cel ştiinţific. Poate, în momentul când toţi oamenii
de pe pământ vor şti matematică într‑un grad superior,
acest deziderat se va realiza. Ar însemna atunci că
va dispărea cultura umanistă, fiind înlocuită cu una
ştiinţifică. Cine ştie?” Ar fi o eroare să se creadă că
aceasta este o opinie izolată; o specialistă în literatură
de la Universitatea din Bucureşti, Monica Spiridon,
considera, în urmă cu câţiva ani, că umanistica trebuie
să facă faţă imperialismului matematic. În discuţiile
privind numărul de ore acordate disciplinelor umaniste,
461

Solomon Marcus

s‑a argumentat frecvent prin referire la primejdia
dezumanizării pe care o prezintă extinderea orelor de
matematică şi informatică, în dauna orelor de română şi
de istorie.
O părere a lui Mircea Eliade; de la Cantemir la
Barbilian și Blaga
Nu de puţine ori, matematica este combătută
indirect, prin referire la primejdia pe care o prezintă
ştiinţa. Iată ce scria în jurnalul său, în octombrie 1944,
Mircea Eliade („Vatra”, nr. 9, septembrie 1984, p. 13):
„De două zile privesc cu mare atenţie savanţii. Gândul că
odată aş putea ajunge şi eu ca oricare din ei îmi amorţeşte
sarcasmul, întristându‑mă. Oamenii aceştia, probabil, ştiu
foarte mult despre foarte puţin, căci aşa definea cineva
savantul, un om care ştie din ce în ce mai mult despre din
ce în ce mai puţin. Unul cunoaşte tot despre pietre, iar
altul aproape tot despre anumite hârtii. Sunt nişte posedaţi
şi, dacă îşi cheltuiesc zilele şi nopţile în laborioase
cercetări, nu o fac pentru aflarea adevărului şi înlăturarea
erorii, ci, în primul rând pentru că nu pot face altceva şi
nu pot face astfel pentru că sunt pasionaţi de munca lor,
dominaţi de o patimă egală aceleia a unui colecţionar
de timbre sau de melci. Majoritatea savanţilor din
întreaga lume sunt simpli meseriaşi, neodihniţi salahori.
Pe figurile lor nu citeşti nobila oboseală a gândului,
462

Dezmeticindu-ne

obsesia delirantă a adevărurilor fundamentale, pasiunea
pentru înţelegerea globală a lumii, ci numai surmenajul
fizic, modestia sau trufia, timiditatea sau absenţa. Oare
pentru fixarea şi prosperarea acestui tip uman au venit
pe lume, au gândit, s‑au zbătut şi au suferit un Aristotel,
un Galileu, un Leonardo da Vinci, un Leibniz?!”.
Încheierea creează o ambiguitate, deoarece face referinţă
la matematicieni care nu au fost numai matematicieni,
deci nu e clar la ce ipostază a lor se referă. Dar gândul ne
poartă inevitabil la reflecţia lui Heidegger: „Wissenschaft
denkt nicht”, iar mai departe în trecut am putea merge
la Goethe, cunoscut pentru adversitatea sa faţă de
matematică (dar la mijloc este o confuzie). În cultura
românească, a existat o tradiţie de natură sincretică, în
care‑i putem include pe Dimitrie Cantemir, Costache
Conachi, Gh. Asachi, Gh. Lazăr, Ion Heliade Rădulescu,
Petrache Poenaru, Ion Ghica, Spiru Haret, M. Eminescu,
Matila C. Ghyka, Pius Servien, Dan Barbilian, Lucian
Blaga, toţi spirite cu vocaţia totalităţii, manifestând deci
interes atât pentru artă şi literatură, cât şi pentru ştiinţă
şi pentru filosofie. Dar, în acelaşi timp, nu a lipsit nici
tradiţia cealaltă, de cultivare a ideii de incompatibilitate
a celor două culturi. Cel mai bun exemplu ne este oferit
de atitudinea faţă de Eminescu, a cărui operă şi ale cărui
Caiete ilustrează o personalitate călăuzită de convingerea
totalităţii drept condiţie a unei autentice culturi.
463

Solomon Marcus

Un caz emblematic: Mihai Eminescu
Cazul Eminescu este emblematic şi pentru situaţia
matematicii în şcoală. Aşa cum aflăm din Viaţa lui Mihai
Eminescu de G. Călinescu (Editura pentru literatură,
Bucureşti, 1964, p. 60), Eminescu a mărturisit el însuşi
că a fost o victimă a unui învăţământ defectuos al
matematicii: „Eu ştiu chinul ce l‑am avut însumi cu
matematica în copilărie, din cauza modului rău în care
mi se propunea, deşi în dealtfel eram unul din capetele
cele mai deştepte. N‑ajunsesem nici la vârsta de douăzeci
de ani să ştiu tabla pitagoreică, tocmai pentru că nu se
pusese în joc judecata, ci memoria. Şi deşi aveam şi
memorie fenomenală, numere nu puteam învăţa deloc
pe de rost, încât îmi intrase‑n cap ideea că matematicile
sunt ştiinţele cele mai grele de pe faţa pământului”.
Aici se opreşte citatul reprodus de Călinescu, dar din
ms. 2258, filele 251 şi 251 verso aflăm şi următoarea
continuare: „În urmă am văzut că‑s cele mai uşoare,
desigur de‑o mie de părţi mai uşoare decât limbile, care
cer memorie multă. Ele sunt un joc cu cele mai simple
legi ale judecăţii omeneşti, numai acele legi nu trebuie
puse în joc goale şi fără un cuprins [...]”. Tocmai din
aceste ultime rânduri aflăm că Eminescu nu a rămas
certat cu matematica; însă şcolar fiind, a fost o victimă
a unei pedagogii inadecvate. Ulterior, ca revizor şcolar,
avea să cunoască în mod profund racilele învăţământului
464

Dezmeticindu-ne

matematic. Problemele discutate de Eminescu în
scrierile sale pedagogice ‑ rapoartele de revizor şcolar
etc. ‑ îşi păstrează şi azi actualitatea. Pe măsură ce s‑a
maturizat, matematica i‑a devenit foarte apropiată. Din
păcate, azi, la peste 140 de ani de la anii de şcoală ai lui
Eminescu, încă nu putem spune că am rezolvat problema
accesibilităţii şi atractivităţii matematicii şcolare.
Generaţii după generaţii continuă să cadă victime ale
unei pedagogii defectuoase, la noi şi aiurea.
Un alt aspect semnificativ merită de asemenea să
fie pus în evidenţă: care a fost atitudinea intelectualităţii
româneşti faţă de acele însemnări din Caietele lui
Eminescu în care se făceau referiri la matematică,
fizică, biologie şi alte ştiinţe? Am discutat pe larg
această chestiune în Invenţie şi descoperire (Ed. Cartea
Românească, Bucureşti, 1989). Voi reţine aici faptul
că oameni de seamă ca T. Maiorescu, G. Ibrăileanu,
E. Lovinescu, G. Călinescu şi, pe urmele lor, criticii
literari din a doua jumătate a secolului al XX‑lea, nu
au sesizat semnificaţia preocupărilor ştiinţifice, în
particular, matematice din caietele eminesciene; ele
erau ale unui poet convins că fără un orizont cultural
total, deci care include şi ştiinţa, creaţia sa poetică ar
fi diminuată. Cei care‑i cunosc opera îşi dau seama cât
de mult a fost ea marcată de acest fapt. Dar prejudecata
incompatibilităţii celor două culturi, cea ştiinţifică şi cea
465

Solomon Marcus

umanistă, a funcţionat perfect la toţi cei specificaţi. Nu
se cunoştea oare exemplul unui Novalis la germani, al
lui Valéry la francezi, al unui Leopardi la italieni, poeţi
cu interes deosebit pentru ştiinţă şi, în particular, pentru
matematică?
Încercări de excludere a matematicii din cultură
Cercetătorii creierului afirmă că acesta se constituie
într‑o reţea de module interactive, fiecare modul având
o anumită specializare, dar cu tendinţa de a inhiba
activitatea modulelor vecine. Aşa se face că o slăbire a
centrilor sonori poate avea drept rezultat o amplificare
a celor vizuali. Probabil că, în mod instinctiv, cei
îndrăgostiţi de literatură, de exemplu, cred că‑şi
accentuează această pasiune dacă subliniază antitalentul
lor matematic; dar chiar prin aceasta, ei îşi trădează
prejudecata lor asupra matematicii, văzută ca ceva opus
umanului şi artistului. Paradoxal, această prejudecată
funcţionează concomitent cu credinţa celor mai mulţi
matematicieni în rolul fundamental pe care‑l are factorul
estetic în creaţia matematică. Jacques Hadamard sublinia
unitatea de structură a proceselor de creaţie ştiinţifică şi
artistică. De unde ar putea proveni această neînţelegere?
Răspunsul nu comportă nici o ambiguitate: din modul
greşit în care se face educaţia matematică.

466

Dezmeticindu-ne

Există însă şi o altă faţă a medaliei. Ştiinţa
în general, dar matematica cu precădere, sunt, în
mentalitatea multor intelectuali, în afara culturii.
„Argumentele” sunt numeroase. Se pretinde că în
cultură îşi au loc numai operele perene, în timp ce în
ştiinţă totul e perisabil, fiecare perioadă istorică aduce
în atenţie noi teorii, noi paradigme, care le înlocuiesc pe
cele anterioare. Se mai pretinde că în cultura unui popor
îşi au loc numai acele opere care‑i conferă identitate, în
timp ce „ştiinţa nu are patrie”, ea este universală, aceeaşi
în toată lumea, deci nu este identitară. Cultura este o
sumă de valori umane şi sociale, în timp ce ştiinţa este
în căutarea obiectivităţii, subiectul (adică fiinţa umană)
fiind pus între paranteze. Alţi autori, cum ar fi filosoful
român Constantin Noica, opun fiinţa filosofiei: „În ştiinţă
este vorba despre lucruri chiar atunci când se vorbeşte
despre om; în filosofie este vorba despre om chiar atunci
când se vorbeşte despre lucruri”. Dar dacă omul este
gonit din ştiinţă, ştiinţa este scoasă din cultură, deoarece
cultura este axată pe om. Ne oprim aici cu argumentele
aduse în sprijinul caracterului anticultural al ştiinţei şi
îl lăsăm pe cititor să le combată. Fapt este că, în practica
politică şi culturală, ştiinţa este separată de cultură, ele
au ministere diferite, instituţii diferite, reviste diferite
şi edituri diferite. Puţinele publicaţii care încearcă să le
includă pe amândouă nu pot evita dezechilibrul dintre ele.
467

Solomon Marcus

Prezența matematicii în mass‑media
Prezența matematicii în mass‑media este foarte
scăzută, iar rarele cazuri în care ea se produce conțin
de obicei distorsiuni care o compromit; faptul că aceste
distorsiuni nu sunt deliberate nu schimbă prea mult
situația. Nu a fost aproape deloc mediatizat „Congresul
al Cincilea al Matematicienilor Români” (spunem
„aproape”, deoarece a fost mediatizat în presa locală
din Pitești, unde s‑a ținut congresul; dar numeroase
inadvertențe au umbrit această mediatizare). Nici măcar
decorarea, cu acel prilej, a unor matematicieni de
către Președintele României nu s‑a bucurat de atenția
televiziunii și a presei centrale. Vinovați de această
situație sunt atât matematicienii (stângaci în contactele
cu presa), cât și gazetarii (care‑i înțeleg prea puțin
pe matematicieni și nici nu‑i consideră o categorie
profesională de interes mediatic). Excepție fac olimpicii,
care sunt asimilați cu sportivii și beneficiază de toate
favorurile acordate acestora. Publicului i se sugerează
astfel că medaliaţii de la olimpiadele de matematică
reprezintă expresia supremă a valorii matematicii
româneşti. Din când în când ne trezim cu câte o
bizarerie, cum ar fi semnalarea de către ziarul Adevărul
(2002) a faptului că „pionierului informaticii româneşti”
i se refuză titlul de profesor universitar sau prezentarea,
pe canalul România Internaţional, a unui amplu dialog cu
468

Dezmeticindu-ne

un octogenar care ar fi dobândit mari merite în educaţia
matematică şi merită recunoştinţa patriei (2003). În
ambele cazuri ideea (enunţată explicit, în primul caz;
numai sugerată, în al doilea) era că ştiinţa „oficială”
îi persecută pe cei în cauză, iar presa şi televiziunea îşi
fac datoria de a le face dreptate. De fapt, nu era vorba
de nici un pionier, iar octogenarul în cauză se ilustrase
cu nişte cărţi de duzină, care au fost de mult abandonate
de practica învăţământului matematic. Pe de altă parte,
zeci de matematicieni care, datorită harului şi muncii lor,
au reuşit să se facă apreciaţi de comunitatea matematică
internaţională şi ridică prestigiul ţării noastre în lume,
rămân cu totul ignoraţi de presă şi de televiziune.
Nu este vorba aici de simpla vanitate de a intra în
notorietate, ci de nevoia de a discuta cu oameni de acest
fel problemele reale ale ştiinţei româneşti, de felul celor
care au fost semnalate în textul de faţă sau de orice alt
fel.
Au fost mediatizate unele proteste ale elevilor
contra cantităţii prea mari de matematică cerută la
anumite profile de învăţământ. A devenit un eveniment
demn de a fi mediatizat faptul că un profesor de
matematică venea beat la ore. Pe un canal de televiziune
s‑a putut vedea, pe la ora 3 noaptea (desigur, în reluare)
o lecţie despre spaţii vectoriale. Axiomele acestor spaţii
erau proiectate şi arătate cu o vergea de către prezentator,
469

Solomon Marcus

care nu făcea altceva decât să citească cu voce tare ceea
ce oricum se vedea scris foarte ordonat. Oare în această
direcţie are nevoie predarea matematicii de televiziune?
Ce‑și amintesc adulții din matematica învățată la
școală?
Este cazul să ne întrebăm: Ce şi cât din matematica
şcolară se incorporează în cultura generală a unui
absolvent de liceu? Se pare că, în afara celor patru
operaţii cu numere întregi (dacă nu comportă şi
manipulări de zecimale), mai nimic. Simpla referire
la o ecuaţie de gradul întâi sau al doilea, la o bine
cunoscută teoremă de geometrie, la funcţia logaritmică,
polinomială sau exponenţială este considerată de obicei
inoportună într‑un text adresat nematematicienilor (se
mai întâmplă să fie salutată din pur snobism). De ce oare
iese matematica atât de repede din mintea absolvenţilor
de liceu, lăsând doar o amintire vagă a unui lucru
dezagreabil? Iese repede deoarece nu a pătruns niciodată
în mod temeinic, ci numai în asociere cu o procedură
mecanică de rezolvare a unor exerciţii; nu s‑a asociat
cu o anumită reprezentare intuitivă, cu o anumită idee
mai generală, care, la rândul ei, este pusă în legătură cu
o altă idee, eventual din altă disciplină şcolară. Dacă
logaritmul era asociat cu reprezentarea geometrică a unei
creşteri foarte lente, contrastând cu creşterea polinomială
470

Dezmeticindu-ne

echilibrată şi cu cea exponenţială, rapidă; dacă se făcea
legătura cu relaţia dintre progresia geometrică şi cea
aritmetică, văzută ca model matematic al relaţiei dintre
senzaţie şi excitaţie; dacă se mai făcea legătura cu
rolul logaritmului în fizică (entropia termodinamică),
în studiul informaţiei (entropia informaţională) şi în
psihologie (timpul subiectiv, ca logaritm al timpului
cronologic), nu devenea logaritmul o valoare culturală
care nu putea fi uitată? Dar cum să fie posibil acest lucru
cu actualele programe şi manuale, orientate spre ceea
ce francezii numeau „matematica–reţete de bucătărie”?
Este deci clar că, cel puţin la nivelul învăţământului de
cultură generală, şansa matematicii de a deveni un factor
de cultură se află, în bună măsură, în legăturile ei cu
celelalte domenii.
Dar învăţământul matematic a ales o altă cale,
aceea a unei matematici care‑şi este suficientă sieşi, aşa
cum este jocul de şah o lume în sine. Numai că regulile
şahului se învaţă repede şi răsplata ludică vine imediat,
în timp ce regulile matematicii sunt infinite, le învăţăm
toată viaţa şi tot nu le terminăm. De aceea, elevul
cedează psihic mult înainte de a apuca să savureze jocul
matematicii. Avem în vedere, desigur, elevul statistic, nu
pe medaliatul olimpic.

471

