Academicianul Solomon Marcus
în dialog cu Tudorel Urian
Curierul românesc, editor Institutul Cultural Român,
anul XVI, nr. 8, august 2004, pp. 16‑18
T.U.: Domnule profesor Marcus, cui au dat dreptate
evenimentele de la 11 septembrie 2001 și tot ceea ce
a urmat (Afganistan, Irak)? Lui Francis Fukuyama
care prevedea „sfârșitul istoriei” (în sens hegelian),
odată cu căderea sistemului comunist și impunerea
la scară planetară a democrației de tip liberal, sau lui
Samuel Huntington, cel care a lansat ideea „șocului
civilizațiilor”?
S.M.: Nu știu să răspund la această întrebare, dar
am de făcut două observații: (a) Evenimentele de la 11
septembrie 2001 sunt expresia unei frustrări profunde,
convertite într‑o ură diabolică împotriva oamenilor și
mai ales împotriva a ceea ce inteligența acestora poate
produce. Înlăturarea cauzelor acestor frustrări este la fel
de importantă ca și lupta împotriva autorilor crimelor
respective. Dar această luptă trebuie dusă de toate
popoarele; dacă unele guverne consideră că problema
nu le privește, atunci șansele de succes în lupta contra
448

Dezmeticindu-ne

terorismului internațional sunt diminuate.
În ceea ce‑i privește pe Fukuyama și Huntington,
la fiecare dintre ei există o doză de adevăr, dar și una
de retorică publicitară. De aceea, nu cred că se pune
problema unei alegeri ferme între cele două variante.
Sintagme ca „sfârșitul istoriei”, „moartea artei”,
„sfârșitul științei” au fost puse în circulație în ultimele
decenii, fie pentru a forța atenția unui număr cât
mai mare de cititori (iar de aici până la interesul pur
comercial nu‑i decât un pas), fie pentru că „sfârșiturile”
și „disparițiile” sunt, de multe ori, maniera în care
percepem perioadele de criză. Atunci când o entitate se
schimbă, ea dă impresia că dispare. „Criza identității”
este în mod frecvent schimbarea modului de a înțelege
identitatea. La fel „criza culturii”, „criza literaturii”,
„criza limbajului” etc. „Sfârșitul istoriei” nu este decât
un alt mod de a înțelege istoria, iar frustrarea la care
m‑am referit ar putea avea rădăcini în fenomene de tipul
celor analizate de Huntington.
T.U.: Care sunt principalele probleme cu care
se confruntă lumea contemporană și cum pot fi ele
rezolvate?
S.M.: Principalele probleme ale omenirii sunt
sărăcia și insuficienta educație. Ele se află într‑o legătură
strânsă, deoarece starea de sărăcie este, în bună măsură,
rezultatul unei educații precare iar, pe de altă parte,
449

Solomon Marcus

aceasta din urmă este favorizată de cea dintâi. De
exemplu, România este confruntată acum cu problema
abandonului școlar, care apare în special la copiii
provenind din familiile foarte sărace. Dar, chiar dacă
vine la școală, un copil subnutrit și ai cărui părinți nu‑s
în stare să se ocupe de educația lui este defavorizat față
de un copil aflat în atenta supraveghere a familiei sale.
Deci împotriva sărăciei și insuficientei educații trebuie
lucrat concomitent, nu consecutiv.
La nivelul popoarelor, problema este mai gravă.
Există încă țări, de exemplu în Africa, în care oamenii
abia acum învață să facă agricultură; lipsa de educație se
răzbună crunt, producând sărăcie în proporții de masă.
T.U.: Într‑o carte care a stârnit multe valuri în presa
românească, „Omul recent”, H.‑R. Patapievici făcea o
pledoarie pentru spiritul conservator, pornind de la ideea
că întotdeauna atunci când se câștigă ceva într‑un plan se
pierde altceva în alt plan. România se află în plin proces
de aderare la Uniunea Europeană. Ce se va pierde și ce
se va câștiga atunci când acest proces va fi unul încheiat?
S.M.: Să nu confundăm încheierea oficială cu cea
reală. Unificarea Germaniei (RDG+RFG) s‑a încheiat
oficial de mult, dar și acum persistă deosebiri.
„Ce se pierde când ceva se câștigă?” este o
întrebare care face parte dintr‑un fenomen mai general,
care apare în știință, în artă, în societate; este vorba de
450

Dezmeticindu-ne

mulțimile conjugate, având drept cazuri particulare
„perechile conjugate”. Avem în vedere mulțimi de cereri
(condiții, deziderate) aflate în situația în care unele
dintre cereri nu pot fi satisfăcute decât pe seama altor
cereri. De exemplu, în comunicarea umană, condiția
unei cât mai bune codificări a mesajului obligă la
reducerea expresivității sale. Creșterea preciziei impune
uneori diminuarea corectitudinii (dacă spun că am
sosit la ora 5, 1 minut și 16 secunde, șansa de a greși
este mai mare decât dacă aș spune că am sosit în jurul
orei 5). Principiul de incertitudine al lui Heisenberg,
din mecanica cuantică, afirmă existența unei perechi
conjugate de forma „poziție, viteză” pentru orice obiect
cuantic. Timpul și spațiul, la calculator, formează și ele
o pereche conjugată. Intrarea în Uniunea Europeană
ne pune în fața unui număr mare de perechi (mulțimi)
conjugate. Ce se câștigă într‑un metabolism în creștere
cu celelalte țări europene va avea drept consecință
renunțarea la unele practici tradiționale, la nivel local.
O interacțiune mai puternică în domeniul culturii, cu
celelalte țări, va fi plătită cu un efort în creștere în
învățarea și folosirea unor limbi internaționale și cu
abandonarea unor publicații incapabile să intre în atenția
unui public internațional.
Patapievici sugerează că, în cazul pe care‑l
analizează, ceea ce se câștigă e mai puțin decât ceea
451

Solomon Marcus

ce se pierde. În ceea ce privește intrarea în UE, trebuie
să distingem între efectele pe termen lung și cele
imediate. Acestea din urmă vor fi șocante, deci ar putea
fi resimțite ca o pierdere. Pe termen lung, avantajele
vor precumpăni. Ne aflăm, în mai toate privințele, sub
acțiunea unor mulțimi conjugate, aproape totul este o
negociere, compromis, strategie.
T.U.: Se aud voci (Cristian Preda) care reproșează
actualei conduceri a României faptul că acordă o
prioritate nejustificată relațiilor cu Statele Unite în
detrimentul celor cu Uniunea Europeană (că de dragul
integrării în NATO a neglijat integrarea în UE). În ce
direcție credeți că ar trebui să se îndrepte prioritățile
politicii externe românești în această perioadă?
S.M.: Ar fi o greșeală gravă orice încercare de a
opune Statele Unite Uniunii Europene, de a baza politica
externă pe ideea unei alegeri între ele. În domeniul
politicii externe, România a ales calea optimă; să sperăm
că ea va fi păstrată, independent de rezultatul alegerilor
de la sfârșitul anului 2004. Eventuale tensiuni între
SUA și Europa Occidentală sunt problema lor; noi ne
îndreptăm atenția spre ceea ce le unește, incomparabil
mai puternic decât unele neînțelegeri punctuale. Pe
termen scurt, NATO și UE pot să pară o formă de
pereche conjugată; pe termen lung, nu! În orice caz,
cultura are alte repere decât politica. Opoziției european
452

Dezmeticindu-ne

vs. american îi prefer atributul euro‑american. Până în
urmă cu vreo 50 de ani exista o matematică europeană
și una americană, azi distincția nu mai există. Probabil
situația e similară în fizică, chimie, biologie etc. Alta e
situația cu științele mai tinere, cum este informatica,
unde persistă deosebiri frapante între cele două
continente, dar care se vor netezi treptat.
T.U.: Mai este naționalismul o problemă în
România anului 2004? Se pare că și controversatul C.V.
Tudor a suferit o spectaculoasă schimbare de față.
S.M.: Problema înțelegerii și acceptării alterității
(din care face parte problema naționalismului), problema
înțelegerii faptului că identitatea unui popor, a unei
persoane, se construiește cu succes numai în strânsă
legătură cu o atenție egală acordată alterității, colaborării
cu ea, va cere un timp îndelungat de rezolvare, deoarece
prea multe prejudecăți s‑au adunat de‑a lungul unei lungi
perioade istorice. Măsuri la nivelul deciziei factorului
executiv pot fi luate peste noapte, dar schimbări ale unei
mentalități cultivate îndelungat nu se pot produce decât
după un lung efort.
T.U.: În ultima vreme se trag foarte multe semnale
de alarmă privind starea culturii. Pe de o parte, asaltul
de vulgaritate venit dinspre mass‑media și tentația mare
pe care o exercită produsele multimedia, pe de altă parte
interesul tot mai redus al publicului pentru produsele
453

Solomon Marcus

de înaltă valoare estetică (explicațiile sunt multe, de la
oboseala cotidiană care‑i face pe tot mai mulți să prefere
moțăitul în fața televizorului unei ieșiri la un spectacol
sau citirii unei cărți valoroase, până la prețul pentru mulți
prohibitiv al produselor culturale de elită). Credeți în
viitorul culturii? De ce?
S.M.: Nu cred că în trecut a fost altfel, dar fiecare
perioadă istorică este percepută de cei care o trăiesc
drept una excepțională, de criză. Viitorul culturii este în
mare măsură în mâinile celor care fac educația noilor
generații. Dacă școala va continua să pună accentul pe
procedee, neglijând ideile, dacă va prefera în continuare
modalitatea imperativă celeia interogative, prezentarea
descriptivă celeia problematice, dacă va continua să
acorde insuficientă atenție conexiunilor dintre discipline,
aspectelor ludice și estetice (inerente oricărei discipline),
dacă va rata în continuare șansa de a oferi copiilor și
adolescenților un spectacol pentru a cărui vizionare
spectatorii să decidă că merită investit un efort, atunci
cultura va rămâne în continuare apanajul unei minorități.
T.U.: Înseamnă introducerea calculatorului și a
Internetului în fiecare casă moartea cărții? Se poate spera
la o coabitare?
S.M.: Viciul se află chiar în modul în care ați
formulat întrebarea. Calculatorul și Internetul, nu numai
că nu amenință cultura, dar o stimulează considerabil,
454

Dezmeticindu-ne

îi dau aripi. Cartea, în forma ei tradițională, nu este
amenințată de calculator și de Internet, ci de posibila
criză a hârtiei, pe care ar putea‑o cunoaște un viitor nu
foarte depărtat. Rezervele forestiere ale planetei par
limitate.
T.U.: Care credeți că trebuie să fie rolul școlii
în societatea contemporană? Se vorbește foarte mult,
de exemplu, de lipsa de audiență a poeziei în lumea
contemporană (nu doar la noi). Cum poate contribui
școala la producerea unor viitoare generații de cititori de
poezie? Dumneavoastră mai sunteți preocupat de poezie
(poetica matematică)?
S.M.: Un răspuns parțial l‑am dat în legătură cu
o întrebare anterioară. Poezia nu a fost nici în trecut
în sfera de interes a unei părți mari a populației. Bănui
că vizați și aici amenințarea care ar veni din direcția
tehnologiei, aflate în plină ascensiune. Dacă nu reușim
să transgresăm această eroare, atunci literatura va fi
mereu în conflict cu societatea, iar șansele ei s‑ar putea
reduce simțitor, cum bine a observat încă din anii ̓60 ai
secolului trecut Eugen Ionescu. Știința și tehnologia
au o dimensiune artistică deloc neglijabilă, o doză
considerabilă de omenesc, pe care literatura ar trebui
să le valorifice. Din fericire, acest lucru a început să se
producă, dar el rămâne încă o floare rară.

455

Solomon Marcus

Fără poezie, omul nu e om. Dar când spun aceasta,
am în vedere nu numai poezia calificată ca atare, ci
și pe aceea ascunsă în orice îndeletnicire omenească.
Pentru a mă referi la profesia mea de bază, aceea de
matematician, voi atrage atenția asupra faptului că mai
toți marii matematicieni afirmă prioritatea factorului
artistic în cercetarea matematică. Mărturii similare
se regăsesc la fizicieni și la chimiști, la biologi și
informaticieni. Poezia este o paradigmă universală,
un numitor comun al tuturor eforturilor creatoare ale
omului; numai o mică parte a ei prinde (deocamdată?)
formă de specie literară repertoriată de teoreticienii
literaturii.
În lumina acestor considerații, veți înțelege că
întrebarea privind „preocuparea mea de poezie” are un
răspuns inevitabil afirmativ, iar legătura cu matematica
rămâne permanentă, chiar dacă nu se mai prevalează
de eticheta „poetică matematică”, etichetă care a fost
necesară un timp, din rațiuni retorice, dar care devine
inutilă acum.
T.U.: Ce se va întâmpla cu produsele culturale
românești și cu scriitorii români în contextul integrării
europene?
S.M.: În contextul integrării europene, produsele
culturale românești și scriitorii de limba română vor fi
provocați să nu se mai sustragă (așa cum, din păcate,
456

Dezmeticindu-ne

s‑a întâmplat deseori) competiției internaționale a
valorilor, fapt care inițial ar putea fi resimțit într‑un
mod incomod, dar ulterior se va dovedi a avea efecte
benefice. Amintesc un lucru pe care l‑am afirmat de
multe ori: cultura românească a cunoscut, de‑a lungul
întregii ei istorii, un decalaj între capacitatea de creație
și capacitatea de comunicare cu lumea, în defavoarea
acesteia din urmă; integrarea europeană ne va impulsiona
să reducem și apoi să anulăm acest decalaj.
T.U.: Ce ați mai făcut în ultimii ani în plan științific
și ce proiecte aveți pentru viitorul apropiat?
S.M.: Ca răspuns la această întrebare, nu‑mi
rămâne decât să vă invit să mă vizitați la adresa: http://
funinf.cs.unibuc.ro/~marcus.
Veți vedea că am fost și sunt foarte activ în
cercetare, în informatica matematică, în combinatorică,
în teoria limbajelor formale, în domeniul lingvisticii
computaționale, în istoria și filozofia științei, în
semiotică, lingvistică și educație. Ar urma să
sistematizez această producție în câteva monografii de
sinteză.

457

