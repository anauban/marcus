Cine este pe bancnota de 10 lei?
Dilema veche, 11 august 2011
Cumpărasem ceva și am întins vânzătoarei o
bancnotă de 10 lei, dar chiar în acel moment mi‑a venit
ideea unui experiment. Simulând ignoranța, cu aerul
că vreau să aflu de la ea, o întreb: Știți cumva cine
este bărbatul de pe această bancnotă? Răspunsul a fost
spontan și fără curiozitatea de a privi bancnota: „Nu
știu, domnule, o fi vreun procopsit de scriitor. Dar nu are
nicio importanță; totul este valoarea și ea este scrisă clar:
10 lei“.
Nu era scriitor, era pictor, dar cred că pentru
persoana respectivă o atare diferență nu prezintă nici un
interes. Scriitor sau pictor, tot un „procopsit“ era.
Selecția personalităților care apar pe cele șapte
tipuri de bancnote a fost bine chibzuită. În ordinea
crescătoare a valorii bancnotelor, avem un istoric:
Iorga; un compozitor: Enescu; un pictor: Grigorescu; un
pionier al aviației: Vlaicu; trei scriitori: Caragiale, Blaga
și Eminescu. Primii doi și ultimul provin din Nordul
Moldovei, zonă recunoscută a fi excelat prin creativitate
în domeniile umaniste; al treilea și al cincilea sunt din
641

Solomon Marcus

Muntenia, iar ceilalți doi sunt transilvăneni. O relativă
omogenitate de generație poate fi observată: toți s‑au
născut în secolul al XIX‑lea și, cu o singură excepție, au
murit în secolul trecut.
Să introduci un element de cultură pe un obiect
strict utilitar – banul – iată o idee care nu poate fi decât
salutată, în ipoteza că privirea cetățeanului se va opri
măcar în treacăt pe chipul de pe bancnotă, va fi curios
să afle ceva despre el, dacă nu cumva știa dinainte
despre cine este vorba; și în acest caz, își va întipări mai
bine în minte însemnătatea sa. Dar, cu regret trebuie să
observăm, acest ipotetic scenariu, de foarte multe ori, nu
funcționează.
Eșecul se observă nu numai în cazul bancnotelor, ci
și în multe alte cazuri similare. Foarte multe persoane nu
au curiozitatea de a ști cine este cel care dă numele străzii
pe care locuiesc. Am identificat numeroși elevi care ignoră
– și nici nu au avut curiozitatea de a întreba – cine este cel
care dă numele școlii la care ei învață. Mi s‑a întâmplat ca
la festivitatea de premiere a câștigătorilor unor concursuri
școlare care purtau nume celebre (Arhimede, Lalescu)
să constat că unii premiați ignorau identitatea numelor
respective („desigur, o fi un matematician, altfel nu era
ales ca nume al unui concurs de matematică“). Dacă
doreai să afli mai mult – de exemplu, cam când a trăit –,
atunci riscai răspunsuri dintre cele mai extravagante.
642

Dezmeticindu-ne

Câți dau atenție chipurilor de pe timbrele poștale?
Câți sunt curioși să știe proveniența din nume proprii
a unor cuvinte comune („simptomul de celebritate
este faptul că ți se scrie numele cu literă mică“)? O
situație penibilă o prezintă de multe ori portretele de
pe pereții școlilor. În Cabinetul de matematică al unei
școli în care elevii aveau zilnic ore de clasă se aflau pe
pereți portretele lui Pitagora, Euclid, Newton, Țițeica,
Pompeiu, Lalescu, Vrănceanu și Moisil. Am constatat că
cei mai mulți elevi nici măcar nu avuseseră curiozitatea
de a privi acele chipuri, de a dori să afle ce ispravă au la
activul lor, de ce merită ei să fie aduși în atenția noilor
generații. Nici unul nu‑i adresase profesorului o întrebare
în acest sens. Dar, parcă într‑o deliberată complicitate,
nici profesorii nu avuseseră inițiativa de a‑i informa pe
elevi în acest sens. I‑am întrebat pe elevi: De ce credeți
voi că se pun aceste tablouri pe pereți? Răspunsul a fost
dezarmant: „Ca să nu rămână pereții goi“.
Dacă m‑aș opri aici, aș putea fi bănuit că preconizez
reluarea concursurilor de pe vremuri, de tipul Cine știe
câștigă sau că doresc să se introducă și la noi concursuri
de tipul acelora de la TV5 Monde, Questions pour un
champion. Or fi având şi ele un rost, dar altceva mă
interesează aici. Fenomenele negative pe care le‑am
semnalat sunt efectul unor deficienţe grave ale sistemului
educaţional şi al mentalităţii dominante în multe sectoare
643

Solomon Marcus

ale vieţii sociale, în viaţa multor familii, dar în primul
rând în mass‑media. Tinerii primesc în școală anumite
cunoştinţe, reţin anumite date, exersează folosirea
unor unelte, a unor procedee, dar nu capătă esenţialul:
nevoia de a înţelege, curiozitatea de a afla, plăcerea de a
asimila idei, bucuria de a face ceva cu propriile lor puteri
intelectuale şi ambiţia de a se realiza ca personalităţi.
Evident, este aici o apreciere generică, pe care trebuie
s‑o înţelegem cu excepţiile de rigoare. Şcoala actuală
nu reuşeşte să convertească activitatea sa într‑un act de
cultură, în ciuda faptului că avem mulţi profesori foarte
buni; dar ei lucrează pe un scenariu învechit, prăfuit,
cu programe având grave lacune, cu multe lucruri care
trebuie eliminate, cu manuale la fel de deficitare ca şi
programele pe care se bazează, cu un scenariu depăşit al
relaţiei educator‑elev, în care accentul cade pe predare
şi comandă, nu pe interacţiune, întrebare şi discuţie
critică. Drept urmare, foarte mulţi tineri intră în viaţă şi
în profesie fără nevoia de cultură; dacă nu au avut şansa
de a se naşte cu ea sau/şi de a beneficia de o educaţie
specială acasă sau/şi de condiţii speciale în şcoală,
indiferenţa lor faţă de marile personalităţi ale istoriei este
de aşteptat. Dar de aici ni se trag multe nenorociri.

644

