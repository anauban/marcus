Unsprezece
Dilema veche, noiembrie 2011
Nu, nu e vorba despre fotbal, e vorba de ora 11 din
ziua de 11 a celei de a 11‑a luni a anului 2011.
Scriu aceste rânduri la ora 11 noaptea din această
zi fatidică. La un jurnal de televiziune de la ora 20:00,
s‑a anunţat că la ora 11 dimineaţa străzile erau pustii,
oamenii aşteptând sfârşitul lumii. Aşa să fi fost oare? Am
ratat şi de această dată un eveniment atât de interesant.
Am fi tentaţi să credem că numărul 11 e rău pe toată
linia (ne amintim şi de blestematul 11 septembrie
2001). Aflăm însă, dintr‑un alt jurnal de televiziune al
respectivei zile, că la ora 11 din ziua a 11‑a a lunii a 11‑a
a anului 1918 s‑a încheiat Armistiţiul care a pus capăt
ostilităţilor în Primul Război Mondial. 
N‑am urmărit prestaţiile diverşilor numerologi
şi astrologi care formează clientela multor posturi de
televiziune publice sau particulare şi care aveau acum un
nou prilej de a contribui la zăpăcirea unei populaţii şi aşa
destul de dezorientate. Dar va fi şi la anul ocazia, la ora
12 din ziua de 12 a lunii a 12‑a a anului 2012. Voi spune,
în treacăt, că am urmărit uneori matinalul de la BBC, de
892

Dezmeticindu-ne

la TV5 Monde, de la televiziuni americane sau canadiene
şi n‑am văzut niciun astrolog şi niciun numerolog, cum
se întâmpla, mai mult sau mai puţin mascat, la TVR 1
(România zi de zi).
Aflu că circulă pe Internet o nostimă problemă
de aritmetică: Oricine ai fi, dacă acum, în 2011, aduni
vârsta pe care o ai cu numărul format din ultimele două
cifre ale anului naşterii tale, obţii numărul 111. Verific,
aşa este: m‑am născut în 1925, am deci acum 86 de ani,
suma dintre 25 şi 86 este 111. Basarab confirmă şi el:
este născut în 1942, are acum 69 de ani, suma lui 42 cu
69 este 111. Dar imediat găsesc contraexemple, lucrurile
nu se verifică la persoane născute în secolul al XIX‑lea,
nici la copiii care în 2011 au sub 11 ani. 
Este deci, clar, un fenomen asociat cu secolul trecut.
Vă invit să suportaţi patru rânduri de aritmetică. Orice an
din secolul al XX‑lea se scrie sub forma 19x, unde x este
un număr cuprins între 0 şi 99.
  Avem 19x = 1900 + x. Vârsta v în 2011 este
diferenţa dintre 2011 şi 1900 + x. Dar diferenţa dintre
2011 şi 1900 este tocmai numărul 111, deci v+x =
111. Iată deci că nu e nimic misterios în prezenţa lui
111 ca un fel de constantă universală a celor născuţi în
secolul trecut. Pentru a nu mai vorbi de inconsistenţa
speculaţiilor care asociază acest fapt aritmetic cu
întâmplările trecute sau viitoare ale vieţii noastre.
893

Solomon Marcus

Îi las pe cititori să găsească cine ia rolul lui 111
când trecem la alte secole. Dar deocamdată să stăruim
asupra lui 111. Simbol al trinităţii în simbologia
creştină, valoare numerică a lui Aleph, în Kabala
(după cum aflu de la Basarab Nicolescu), numărul 111
trimite, metonimic, şi la proporţia de aur, căreia Matila
C. Ghyka i‑a consacrat, în anii ’30 ai secolului trecut, 
două volume devenite celebre. Vechii greci se referă
la problema împărţirii unui segment de dreaptă în două
subsegmente inegale, în aşa fel încât raportul dintre
subsegmentul mai mare şi cel mai mic să fie egal cu
raportul dintre segmentul total şi subsegmentul mai
mare.
Cred că problema se află şi în manualele de
gimnaziu. Proporţia de aur este aceea care dă răspunsul.
Dar reprezentarea în fracţie continuă infinită a acestei
proporţii (egale cu jumătatea sumei dintre 1 şi rădăcina
pătrată a lui 5) se scrie numai cu cifra 1; deci 1 repetat
nu doar de trei ori, ci de o infinitate de ori.
Niciodată nu vom şti câtă trudă a fost necesară
pentru ca civilizaţia umană să depăşească pe unu şi să
ajungă la doi şi câtă altă trudă a fost necesară pentru a
se depăşi şi bariera binarităţii. Un rafinat poet englez,
Philip Larkin (1922‑1985), răsplătit, pentru poeziile
sale (strînse în volumul  The Whitsun Weddings), cu
„Medalia de Aur a Reginei“, are o poezie intitulată
894

Dezmeticindu-ne

„Counting“, în care tocmai despre această dificultate
este vorba:  „Thinking in terms of one / Is easily done
– / One room, one bed, one chair, / One person there, /
Makes perfect sense; one set / Of wishes can be met, /
One coffin filled. // But counting up to two / Is harder to
do; / For one must be denied / Before it’s tried.“

895

