„Matematica înseamnă mai mult
decât un șir de cifre”
Interviu realizat de Dan Stanca
Academician Solomon Marcus la 85 de ani
Timpul, 2010,
Redacția: Această pagină va fi dedicată
personalităților românești care au îndrăznit, care și‑au
asumat riscuri, au ajuns în vârf, au lăsat ceva în urma lor,
au contribuit la schimbarea lumii și sunt modele pozitive
demne de venerație. Să nu lăsăm să zboare faima. Să
dovedim că nu suntem lipsiți de valori. Nu putem vorbi
despre România numai în termeni negativi. O pagină
utilă pentru semințele viitoarelor reușite, pentru cei
care vor să‑și hrănească sufletul cu inspirație ori sunt
lipsiți de ambiție. Va fi dedicată nivelului de vârf, rețetei
succesului clădit pe acțiune, termenului de referință,
întru bucuria lucrurilor adevărate. Prilejul – împlinirea
unei vârste rotunde, din cinci în cinci ani.
D.S.: Am în minte și acum, după mulți ani de la
lectură, cartea dvs., Lingvistica matematică. Considerați
că la ora actuală, având în vedere progresul din ultima
546

Dezmeticindu-ne

vreme, matematica poate oferi un model universal
cognitiv care poate fi aplicat oricărei discipline umane?
S.M.: Timp de două mii de ani, „Elementele” lui
Euclid au constituit un model de prezentare a cunoașterii
umane. Modul axiomatic‑deductiv introdus de vechii
greci a fost adoptat nu numai de matematicieni ca
Arhimede și Newton, dar și de teologii din Evul Mediu,
de filosofi ca Spinoza, în prezentarea Eticii sale, iar în
secolul al XX‑lea mari lingviști, ca L. Bloomfield, tot
de acest mod de prezentare s‑au prevalat. Între timp
însă, gândirea axiomatic‑deductivă a trecut și ea prin
mari prefaceri, deoarece a trebuit să facă față unor
situații conflictuale. Logica simțului comun, bazată
pe principiile lui Aristotel, identitate, necontradicție
și terț exclus, a intrat în criză și ideea de raționament
deductiv a trebuit să fie supusă unui examen critic
foarte sever. S‑a înțeles astfel că există situații în care
anumite enunțuri pot avea un statut mai complicat
decât cel de adevărat sau fals. S‑au dezvoltat logici
alternative față de aceea a vieții cotidiene. Concomitent
și ulterior, semnale ale nevoii de a se depăși, în anumite
situații, logica aristotelică au venit și din alte direcții,
din fizică și biologie, din literatură, artă și informatică,
din lingvistică, din mitologie și din filosofie. Dar, în
înțelegerea statutului noilor logici, matematica și‑a
păstrat rolul de protagonist.
547

Solomon Marcus

Modul matematic de gândire este exemplar pentru
orice altă disciplină și din alte puncte de vedere: a
proceda în etape riguros ordonate, în care fiecare etapă
se prevalează de achizițiile din etapele anterioare,
este desigur un deziderat natural în orice activitate
intelectuală. Dar matematica este unică prin modul
în care această procedare are un caracter explicit, „cu
cărțile pe masa”, cum s‑ar spune. Pentru aceste motive
și pentru altele, matematica are o anumită universalitate.
A ști să vezi în matematică această universalitate, a nu
reduce matematica la un simplu prestator de servicii,
la o simplă unealtă, iată ce ar trebui să aibă în vedere
educația matematică, ceea ce ar trebui să înțeleagă orice
intelectual, orice om de cultură.
Dar, în același timp, această universalitate nu are
un caracter exclusivist, ea este concomitentă cu acțiunea
altor discipline, care pot avea, la rândul lor, o rază
universală de acțiune. Matematica nu‑și extinde raza de
acțiune în dauna altor modalități, ci în cooperare cu ele.
În cunoaștere, este loc pentru orice modalitate, dar este
nevoie de un antrenament în direcția cooperării dintre
discipline, antrenament pe care școala nu prea îl are în
atenție.
D.S.: Care sunt totuși limitele matematicii?
S.M.: Încercările anterioare de a se fixa limitele
matematicii au eșuat, au fost contrazise de evoluția
548

Dezmeticindu-ne

matematicii. S‑a crezut, în secolul al XIX‑lea, că
matematica se reduce la studiul aspectelor cantitative și
al formelor spațiale, dar în secolul al XX‑lea matematica
și‑a extins obiectul mult dincolo de cantitate și de forma
spațială. S‑a crezut că matematica este incompatibilă
cu finețea, nuanțele și complexitatea fenomenelor
socio‑umane, dar acum metabolismul matematicii cu
domeniile socio‑umane este foarte puternic, fie că e
vorba de științele socio‑umane, fie că ne referim la
arte și literatură. Fără a pretinde că matematica poate
orice, pretenție absurdă și trivială (un gazetar se mira
că matematica nu poate prevedea câștigurile la loterie),
prudența ne recomandă să ne abținem de a‑i fixa anumite
limite. Orice predicție în această privință este riscantă.
D.S.: Vă gândiți că viitorul ar putea fi dominat de
un homo mathematicus, formulă ad‑hoc, așa cum în
Evul Mediu și în societățile tradiționale a existat homo
religiosus?
S.M.: Aceasta întrebare se situează în directa
continuare a întrebării 1, deci și răspunsul îl va continua
pe cel dat acelei întrebări.
Complexitatea vieții și a societății actuale nu mai
permite unui singur mod de a vedea lumea să‑și impună
acțiunea în dauna oricărui alt mod. Cu atât mai mult nu
va fi, cred, posibil acest lucru în viitor. De fapt, nici în
Evul Mediu lucrurile nu au fost atât de simple cum au
549

Solomon Marcus

fost ele descrise în mod insistent. Unii autori prevăd o
societate dominată de analfabeți, extrapolând probabil
anumite situații îngrijorătoare din domeniul educațional
și cultural. Dar dacă vedem viitorul ca ceva care vine
peste noi în loc de ceva construit de noi, atunci nu ne
putem aștepta la nimic bun.
D.S.: Sunteți membru plin al Academiei. Ce mai
înseamnă această distincție în ziua de astăzi? Ce pondere
mai are Academia în societatea contemporană?
S.M.: Academia Română își propune să fie forul
suprem de știință și cultură al României. În atingerea
acestui scop, ea are de învins unele neajunsuri, iar
dacă veți răsfoi revista ACADEMICA veți afla multe
lucruri interesante în această privință. Chiar cu cele
mai bune intenții, atunci când este vorba de o selecție
a celor mai buni din diferite domenii de cercetare și de
creație, rezultatele nu pot beneficia totdeauna de acordul
general. Academia Română primește propuneri din
partea unor instituții de cercetare și universitare, dar și
din partea unor uniuni de creație; uneori acestea nu și‑au
elaborat criterii de selecționare a celor mai valoroși ai
specialității respective, iar propunerile sunt marcate de
superficialitate.
În orice caz, au fost destule situații în care
Academia Română a avut o prezență socială corespunzătoare menirii ei, în știință, în tehnologie, în umanistică,
550

Dezmeticindu-ne

în educație și învățământ. A făcut recomandări
corespunzătoare factorilor de decizie. Un factor negativ
îl constituie existența unor Academii paralele, unele
dintre ele fără un suport științific satisfăcător. Anumite
instituții de învățământ poartă și ele denumirea de
„Academie”. Există apoi Academii sportive, ca acelea
de fotbal, de tenis, de biliard etc., toate contribuind la
crearea unei confuzii privind natura statutului academic.
O clarificare este de dorit.
D.S.: În comunism matematica a avut de suferit,
ca orice disciplină a cunoașterii pure. Dacă nu era Zoe
Ceaușescu, poate ar fi suferit și mai mult. Comunismul
voia producție, nu cercetare. Acum fondurile pentru
cercetare sunt tot mai reduse. Ce poate face un tânăr
matematician de talent ca să nu‑și părăsească țara?
S.M.: Situația matematicii și a matematicienilor,
în perioada comunistă, nu poate fi expediată prin
aprecierea pe care dvs. o faceți, dar nu voi discuta aici
această chestiune. Mă voi referi la situația actuală. În
condițiile actuale, locul geografic în care te afli nu mai
este atât de important ca în trecut. Nevoia de a călători,
pentru a se întâlni cu alții din domeniul său, este vitală
pentru un om de știință. Tot mai mulți dintre cei care au
plecat din România și sunt afiliați la univesități străine
sunt în legătură permanentă cu cei din țară, se fac multe
lucrări în colaborare, ei vin la noi să țină conferințe, să
551

Solomon Marcus

organizeze seminarii de cercetare, să conducă doctorate,
iar cei din țară merg la ei. Desigur, este de dorit să avem
instituții de cercetare și învățământ cât mai puternice,
dar faptul acesta nu va anula nevoia de călătorie,
de participare la întâlniri internaționale. Oricât de
importantă ar fi comunicarea electronică, internetul în
general, nimic nu poate înlocui contactul personal între
cercetători. În cele din urmă, nivelul cercetării nu poate
fi radical diferit de cel al economiei noastre, al societății
noastre, în general.

552

