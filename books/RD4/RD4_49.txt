Alte documente privind acțiunea
universitarilor din 1944
România literară, anul XXXVIII, nr. 1,
12‑18 ianuarie 2005, p. 20
În continuarea relatării unor evenimente din viața
universitară în frământatul an 1944 (a se vedea „R.l.”
450), semnalăm existența în dosarul Neculcea a altor
documente, după cum urmează. Este vorba, firește, de
punctul de vedere al partidului comunist, exprimat în
primul rând de prof. Mihail Neculcea, de la Facultatea
de Matematică a Univ. București. Documentele sunt
următoarele:
1.	 Lista celor 66 de cadre didactice universitare
care au semnat Memoriul adresat Mareșalului
Antonescu în aprilie 1944 (listă alcătuită în
1964);
2.	 Textul adresat prorectorului Universității din
București, Alexandru Balaci, „Pentru istoricul
Universității din București”, redactat pentru a
fi inclus în volumul dedicat centenarului acestei
universități, din 1964;
3.	 Scrisoarea adresată în februarie 1964 de M.
484

Dezmeticindu-ne

Neculcea lui Leonte Răutu, împreună cu un
„Scurt istoric al Memoriului universitarilor din
aprilie 1944”;
4.	 Lucrarea elaborată de M. Neculcea pentru
Institutul de Istorie al PMR;
5.	 Scrisoarea adresată de un grup de universitari
Mareșalului Antonescu, în legătură cu arestarea
delegatului cu strângerea semnăturilor și
confiscarea Memoriului.
Documentul meționat la punctul 2 se încheie cu
fraza: „Reproducem mai jos textul Memoriului […]”, dar
acest text lipsește din dosar.
Dintre acestea, prezentăm în cele ce urmează
documentul 1, apoi o scrisoare a lui Stoilow către Dej,
după care vom face comentariile de rigoare la acest text
semnificativ pentru relațiile intelectualilor cu puterea
comunistă.

Din intervenția interesantă a d‑lui Puiu Stoiculescu
(„R.l.” 49, p. 31), ca reacție la articolul nostru „R.l.”
45 și ținând seamă de cele arătate în articolul nostru
din „R.l.” 50, rezultă că au existat trei trimiteri din
partea universitarilor către Mareșalul Antonescu.
Prima în aprilie 1944, semnată de 66 de universitari
(după dl. Stoiculescu, numai 65), a doua, în iunie 1944,
485

Solomon Marcus

constând în retrimiterea memoriului (în copie), însoțită
de scrisoarea de protest față de arestarea celui care
strângea semnăturile pe memoriu, și a treia, mult mai
scurtă, datată 4 august 1944 și reprodusă în facsimil în
„R.l.” 49; aceasta nu a mai fost semnată decât de 10
universitari. Examinând cu atenție această reproducere
în facsimil, am avut surpriza să recunoaștem printre
semnături pe aceea a matematicianului Miron Nicolescu,
profesor la Universitatea din București; semnătura sa
nu figura printre cele 66 din primul memoriu. Ar fi
interesant de clarificat cui aparțin celelalte semnături, în
afară de cele clare ale lui Danielopolu și Gr. T. Popa.
Neconcordanțele de detaliu privind, de exemplu,
ordinea semnăturilor, ar putea fi datorate unor acțiuni
paralele ale celor care se ocupau de memoriu. Rămân
însă neconcordanțele majore, la care ne‑am referit în
primul articol. (S.M.)

Semnatarii:
Prof. Dr. C. Angelescu, fost ministru liberal,
decedat; Prof. C. Bordeianu, Facultatea de Farmacie;
Prof. Banu Gheorghe, Facultatea de Medicină, decedat;
Prof. Dr. Danielopolu Daniel, Facultatea de Medicină,
decedat; Prof. Demetrescu Gheorghe, Facultatea de
Științe, Institutul Astronomic; Prof. E. Herovanu,
486

Dezmeticindu-ne

Facultatea de Drept, decedat; Prof. Dr. Amza Jianu,
Facultatea de Medicină, decedat; Prof. Dr. N. Lupu,
Facultatea de Medicină, decedat; Prof. Micescu Istrate,
Facultatea de Drept, decedat; Prof. Munteanu Bazil,
Facultatea de Litere și Filozofie (Paris); C. Motaș,
Facultatea de Științe, Institutul Speologic; Prof. C.I.
Parhon, Facultatea de Medicină, decedat; Prof. Popa
Gr. T. Facultatea de Medicină, decedat; Prof. Popovici
Const., Facultatea de Științe, decedat; Prof. Francisc
Reiner, Facultatea de Medicină, decedat; Prof. Ralea
M., Facultatea de Litere și Filozofie, decedat; Prof.
Rosetti Alex, Facultatea de Litere şi Filozofie; Prof.
Stănescu P.P., Facultatea de Farmacie, decedat; Prof.
Theodorescu Dan, Facultatea de Medicină, decedat;
Prof. Vrănceanu Gheorghe, Facultatea de Ştiinţe;
Conferenţiar Dr. D. Bagdasar, Facultatea de Medicină,
decedat; Prof. Dr. Enăchescu Marin, Facultatea de
Medicină; Prof. Gerota D. D., Facultatea de Drept,
decedat; Prof. Ghika Alexandru, Facultatea de Ştiinţe,
decedat; Prof. Dr. Jovin, Facultatea de Medicină;
Prof. Ștefan Vencov, Facultatea de Ştiinţe, decedat;
Prof. Zamfirescu Ion, Facultatea de Litere şi Filozofie;
Asistent Anton Dumitriu, Facultatea de Litere şi
Filozofie; Prof. Mezincescu Eduard, Facultatea de
Medicină; Prof. Mezincescu Florica, Facultatea de
Ştiinţe; Prof. Neculcea Mihail, Facultatea de Ştiinţe;
487

Solomon Marcus

Prof. Parhon, Facultatea de Medicină; Prof. Popa N.,
Facultatea de Medicină, Universitatea din Iaşi: Prof. Dan
Bădărău, Facultatea de Litere şi Filozofie, decedat; Prof.
Botez I., Facultatea de Ştiinţe, decedat; Prof. Balmuş
Constantin, Facultatea de Litere şi Filozofie, decedat;
Prof. Cernătescu Radu, Facultatea de Ştiinţe, decedat;
Prof. Claudian Alexandru, Facultatea de Litere şi
Filozofie, decedat; Prof. C. Mihu, Facultatea de Ştiinţe;
Prof. Myller Alexandru, Facultatea de Ştiinţe, decedat;
Prof. Oţetea Andrei, Facultatea de Litere şi Filozofie;
Prof. Răşcanu V., Facultatea de Medicină; Prof.
Ţiţeica Şerban, Facultatea de Ştiinţe; Prof. Zane Gh. I.,
Facultatea de Drept; Prof. Pavelcu Vasile, Facultatea
de Litere şi Filozofie; Conferenţiar Capri, Facultatea
de Medicină; Prof. Caraman Petru, Facultatea de Litere
şi Filozofie; Prof. Popa Ilie, Facultatea de Ştiinţe; Prof.
Stihi Emil, Facultatea de Ştiinţe; Asistent Agavriloaiei,
Facultatea de Litere şi Filozofie; Universitatea din Cluj:
Prof. Daicoviciu Constantin, Facultatea de Litere şi
Filozofie; Prof. Moşoiu Tiberiu, Facultatea de Drept,
decedat; Prof. Petrovici Emil, Facultatea de Litere şi
Filozofie, decedat; Prof. Popovici D.; Prof. Popovici
G.; Prof. Pîrvulescu C., Facultatea de Ştiinţe, decedat;
Politehnica Bucureşti: Prof. Ciorănescu N., decedat;
Prof. Pantazi Alex, decedat; Prof. N. Profiri, decedat;
Conferenţiar Roşca Radu; Asistent Gorciu Vasile; Prof.
488

Dezmeticindu-ne

G. G. Constantinescu; Politehnica Iaşi: Conferenţiar
Potop Aurel, decedat; Asistent Davidescu Eugen;
Conservatorul din Iaşi: şrpf. Bîrsan Mircea.
Observaţie: Ordinea dată în prezentul tabel nu
coincide nici cu ordinea reală din memoriul original,
nici cu ordinea apărută în ziarul „România liberă” din 6
septembrie 1944.
Numele persoanelor din prezentul tabel sunt acelea
ale universitarilor care au semnat Memoriul original,
confiscat de Siguranţă la Alba Iulia, şi care au fost
reproduse în copia înmânată lui I. Antonescu o dată cu
protestul privind arestarea subsemnatului la Alba Iulia.
În lista semnatarilor publicată de „România liberă”
la 6 septembrie 1944 s‑au strecurat următoarele greșeli
de tipar, la numele citate mai jos:
În loc de Radu Cernătescu s‑a tipărit N. Cernătescu;
Anton Dumitriu s‑a tipărit Anton D‑tru; G.G.
Constantinescu s‑a tipărit G.G. C‑tinescu.

489

