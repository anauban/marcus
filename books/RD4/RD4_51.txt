„Aș avea nevoie de încă 80 de ani”
Jurnalul Național, anul XIII, nr. 3589,
1 martie 2005, p. 22
„La tinereţe eşti cam ignorant şi nu ştii să preţuieşti
timpul atât cât merită, la bătrâneţe înveţi să‑l preţuieşti,
dar nu mai ai forţa, energia necesară pentru a face faţă
cum se cuvine la ceea ce‑ţi dai seama că ai de făcut.
La 80 de ani mă simt oarecum jenat de faptul că am
ajuns la o vârstă care e mai mult decât dublul vieţii lui
Eminescu, al cărui vers spunea: «Optzeci de ani îmi pare
în lume c‑am trăit, / Că sunt bătrân ca iarna, că tu vei
fi murit». Am senzaţia că mai am încă foarte multe de
făcut, mai am foarte mult de lucru, şi din acest punct
de vedere nu sunt deloc mulţumit cu anii pe care i‑am
trăit până acum. Orice an în plus înseamnă foarte mult,
înseamnă că încă am timp să îmi pun ordine în hârtii şi
în idei. N‑am obiceiul să marchez prin festivităţi, nici
măcar cu cei apropiaţi, ziua de naştere, dar acum aşa se
întâmplă, că pe măsură ce înaintezi în vârstă şi mai ai şi
manifestări publice, intri în atenţia altora care îţi aduc
aminte că o anumită zi are o anumită semnificaţie. Abia
de la 50 de ani am început să‑mi marchez cifrele rotunde,
506

Dezmeticindu-ne

pentru că ai mei colegi au organizat câte ceva, mi s‑au
dedicat volume. Sunt autorul unei cărţi care se cheamă
«Timpul», apărută în urmă cu 20 de ani. Trecerea
timpului este o problemă care m‑a preocupat foarte mult.
Pe măsură ce înaintăm în vârstă, timpul se scurge mai
repede. Deci la vârsta de 20 de ani timpul meu, subiectiv,
psihologic, se scurgea mult mai încet decât acum, dar
ăsta e un lucru adevărat pentru toată lumea. Caut să
încetinesc scurgerea timpului, umplându‑l cu folos. Am
planuri atât de multe, că aş avea nevoie de încă 80 de
ani ca sa le duc la împlinire. Dintre toate lucrurile care
îmi dau satisfacţie, cea mai mare e faptul că am reuşit
cât de cât să ajut un număr de tineri să intre cu succes în
cariera de cercetare ştiinţifică; am impresia că acest lucru
este mai important decât tot ceea ce am publicat. Pe de o
parte, cu înaintarea în vârstă puterea de muncă scade şi
trebuie să te agăţi aproape cu disperare de cele câteva ore
din zi în care ai forţa necesară să reflectezi, să scrii, să
citeşti. Sunt un om pentru care cea mai mare plăcere este
să citească şi să savureze tot ceea ce reprezintă o culme
fie în ştiinţă, fie în artă, şi mărturisesc că mai multe
satisfacţii trag din lectură, din contemplare a ceea ce au
realizat cei mari, decât din ceea ce eu personal am făcut.“

507

Solomon Marcus

O precizare
În nr. 6/2005 al României literare se publică o
replică la scrisoarea lui Stoilow către Gheorghiu‑Dej
şi la comentariul nostru pe marginea ei (R.l. 2/2005).
Autorul replicii, dl. prof. univ. dr. Alexandru A.
Popovici, prezintă personalitatea ştiinţifică şi umană
a lui Andrei Popovici, o personalitate care merită
într‑adevăr să fie cunoscută şi pe care nici Stoilow nu o
contestă. În scrisoarea către Gheorghiu‑Dej este vorba
de o situaţie punctuală de la Institutul de Fizică, unde
Andrei Popovici era director adjunct ştiinţific. Prof.
Popovici afirmă că deţine detalii privind acel conflict de
la Institutul de Fizică, dar nu le dezvăluie, ratând astfel
posibilitatea de a aduce lumină în problema discutată
de Stoilow. Greşit mi se atribuie rolul de judecător şi
procuror; dacă mi l‑aş fi asumat, ar fi trebuit nu doar să
discut cu semnatarul replicii, ci să deschid o anchetă
printre supravieţuitorii vieţii academice din anii ̓50 şi
printre urmaşii şi cunoştinţele tuturor celor nominalizaţi
de Stoilow; ar mai fi trebuit să consult arhiva Academiei
Române şi să caut la bibliotecă diferite scrieri din epocă.
Evident că nu putea fi vorba de aşa ceva. Nu am făcut
decât să dau câteva informaţii privind situaţia generală
din lumea academică şi universitară a anilor ̓50 şi
identitatea celor implicaţi în scrisoarea lui Stoilow, în
care, după cum chiar acesta o spune, referirea la Andrei
508

Dezmeticindu-ne

Popovici se face numai „în treacăt”. Este evident pentru
cititor că acesta nu este pus pe acelaşi plan cu Roller şi
cu Aurel Pârvu. Dar rostul publicării unor documente
de acest fel este, între altele, tocmai acela de a da unora
dintre cititori posibilitatea completării dosarului.

509

