Informatica românească la răspântie
Ateneu, nr. 2(255), februarie 1991, p. 2
Stimulenţii care asigură dezvoltarea unei ştiinţe
sunt dintre cei mai variaţi; ei merg de la curiozitatea şi
setea de cunoaştere a cercetătorului individual, trec prin
nevoile manifestate de alte domenii de cercetare şi ajung
până la imperativul unui cadru instituţional adecvat. De
obicei, între motivaţiile personale şi cele interdisciplinare,
ca şi între acestea din urmă şi cele sociale, se interpun
intervale temporale destul de mari, care uneori merg până
la o mie de ani şi mai mult; să ne gândim la distanţa în
timp între cercetările întreprinse de Apollonius din Perga
asupra secţiunilor conice şi descoperirea traiectoriilor
eliptice ale Pământului în jurul Soarelui; sau, un exemplu
poate şi mai impresionant, cei aproximativ două mii
de ani care separă cercetările vechilor greci asupra
numerelor prime şi valorificarea contemporană a acestor
cercetări şi a celor ulterioare, de tipul micii teoreme a lui
Fermat, în domeniul criptografiei.
Dezvoltarea informaticii oferă un exemplu de
cu totul altă natură, în care stimulenţii externi şi cei
interni au fost de la început şi au rămas în continuare
206

Dezmeticindu-ne

concomitenţi. Se poate chiar spune că presiunea socială
a precumpănit, nedând încă informaticii un prea mare
răgaz de a se dezvolta într‑un mod mai relaxant, nesupus
convulsiunilor de tot felul venite din afară. Acesta este
şi motivul pentru care nu numai profanii şi factorii de
decizie socială, dar chiar mulţi oameni de ştiinţă, mulţi
intelectuali văd în informatică o preocupare exclusiv
aplicativă, nesesizând faptul că şi informatica tânjeşte
după un echilibru între latura ei teoretică şi latura ei
aplicativă; sau, atunci când se recunoaşte nevoia acestui
echilibru, se consideră totuşi că în timp ce aplicaţiile au
un caracter de urgenţă, teoria poate să mai aştepte. O
atare atitudine simplificatoare reduce informatica la o
strategie de azi pe mâine, problemele ei de perspectivă
fiind escamotate.
Adevărul este că între latura speculativă a
informaticii, aflată la contactul direct cu logica
matematică, filozofia, combinatorica, algebra şi alte
discipline, şi latura ei direct aplicativă, se află o serie
de aspecte intermediare, toate în interacţiune. Mai mult,
prin conexiunile ei cu Inteligenţa Artificială, Informatica
şi‑a consolidat relaţiile cu ştiinţele neuronale şi cu
psihologia proceselor de învăţare. Informatica generează
astfel un nou tip de interdisciplinaritate.
Dar nu este numai atât, informatica suplimentează
inteligenţa umană ca o proteză deosebit de puternică
207

Solomon Marcus

şi de eficientă, cerută de insuficienţa celei dintâi în
manipularea simbolurilor. Prin aceasta, informatica
produce o mutaţie în domeniul limbajelor umane, fapt
care are implicaţii considerabile în procesul educaţional
al noilor generaţii. Încă în secolul al XVII‑lea, limbajul
ştiinţific a început a se emancipa de tutela limbajului
natural, prin adăogarea, la componenta sa naturală, a
unei componente artificiale din ce în ce mai complexe.
Această evoluţie nu a fost rezultatul unei opţiuni, ci
rezultatul inevitabil al nevoii de satisfacere a exigenţelor
de precizie şi rigoare crescândă din ştiinţă. Prin
dezvoltarea informaticii, se produce o nouă cotitură
în domeniul limbajului uman; la nevoia mai veche de
cuantificare, axiomatizare şi formalizare se adaugă
acum nevoia de modelare algoritmică şi de reducere
a complexităţii diferitelor operaţii, nevoia unui dialog
om‑maşină tot mai perfecţionat. De la calculul numeric
de ieri s‑a ajuns la calculul simbolic de azi, cu o arie
de cuprindere incomparabil mai mare. Dar această
proliferare sintactică a simbolurilor, stimulată şi
potenţată de calculatoare din ce în ce mai puternice şi
de programe din ce în ce mai inteligente, trebuie să‑şi
găsească un sens uman şi social. Facem progrese în
înzestrarea cu calculatoare — chiar în aceste zile s‑au
instalat o serie de calculatoare personale în facultatea
la care lucrez — dar trebuie să învăţăm să le dăm şi o
208

Dezmeticindu-ne

utilizare pe măsură, să nu ne aflăm în situaţia ridicolă
a celor care aduc un tun pentru a omorî... o muscă. Din
diferite direcţii ne parvin veşti conform cărora potenţialul
informatic existent nu este nici pe departe folosit la
capacitatea sa, iar specialiştii în informatică nu sunt nici
ei folosiţi la nivelul lor superior de competenţă. Desigur,
de această situaţie sunt într‑o anumită măsură vinovaţi şi
unii informaticieni, care nu au ştiut să atragă atenţia unor
factori de conducere şi de decizie asupra posibilităţilor
informaticii. Ne aflăm într‑o etapă în care mai mult decât
parametrii fizici ai calculatorului prezintă importanţă
inteligenţa programelor pe care le realizăm deci, implicit,
a modelelor algoritmice care se află la baza lor.
Miza socială a informaticii este imensă. Probabil
că niciodată nu s‑a mai întâmplat ca apariţia unei noi
ştiinţe să producă mutaţii de o asemenea anvergură,
într‑un timp atât de scurt. Nu ne aflăm printre ţările
cele mai avansate în acest domeniu, deşi am fost printre
primele ţări din lume în care s‑au construit calculatoare.
Declinul general al României sub totalitarism s‑a resimţit
şi în informatică. Dar, cum se spune, uneori tot răul e
spre bine; putem acum învăţa din experienţa ştiinţifică,
tehnologică, socială, educaţională a informaticii din
ţările cele mai avansate, putem evita greşelile altora,
putem alege între variante deja experimentate de alţii (un
singur exemplu: alegerea limbajului BASIC în vederea
209

Solomon Marcus

iniţierii şcolare în informatică). Dar aceste variante nu
pot fi preluate tale quale; ele trebuie adaptate unei ţări
care părăseşte o organizare socială falimentară, pentru
a trece într‑un timp cât mai scurt la o altă organizare,
bazată pe libera iniţiativă şi pe descătuşarea forţelor
creatoare.
Rolul informaticii în transformările pe care trebuie
să le parcurgem este esenţial, dar această esenţialitate
este deocamdată potenţială, ea nu se actualizează de la
sine. Programul pe care informatica românească trebuie
să‑l urmeze nu‑l poate nimeni trasa în detalii, acest
program trebuie descoperit şi construit pe parcurs. Este
necesară o educaţie informatică a marelui public. Chiar
dacă gradul de informatizare a societăţii româneşti nu
este încă atât de ridicat încât această informatizare să fie
resimţită în viaţa de fiecare zi a cetăţeanului de rând, o
educaţie informatică generală se impune, deoarece ne
pregătim pentru un ritm rapid de informatizare chiar
în anii imediat următori. În particular, o deosebită
importanţă o prezintă educaţia informatică a copiilor şi
tineretului, educaţie deocamdată superficială, nerealizând
reale deprinderi de gândire, nedispunând nici de o bază
materială corespunzătoare, nici de un corp profesional
adecvat. Spre remedierea acestei situaţii se îndreaptă
efortul actual.

210

