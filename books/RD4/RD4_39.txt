Un punct de vedere privind predarea
analizei matematice la liceu
Arhimede, nr. 5‑6, mai‑iunie 2002
Mai întâi trebuie respectate câteva principii
generale, valabile pentru orice disciplină, la orice clasă:
a.	 manualul trebuie să se constituie într‑o
valoare culturală în sine și nu în funcție de eventuale
continuări la universitate: scadența fiecărui fapt, sensul,
semnificația și motivația sa trebuie să apară imediat
(acest deziderat vine în conflict cu sintagma „învățământ
preuniversitar”, care sugerează conceperea școlii de
cultură generală ca o anticameră a universității);
b.	 realizarea unei interacțiuni(conexiuni) maxime
cu celelalte materii predate în școală;
c.	 evitarea fenomenelor de obezitate: un manual
nu trebuie să depășească 150‑200 de pagini normale ca
mărime și corp de literă;
d.	 eventuale dezvoltări pentru elevii care vor să afle
mai mult trebuie clar separate de manualul propriu‑zis.
Ideea care ar trebui să se afle la baza predării
analizei matematice este aceea a existenței a două tipuri
412

Dezmeticindu-ne

de aspecte ale fenomenelor (proceselor) de orice fel:
aspecte locale și aspecte globale.
Ar fi deci de început cu exemple cât mai variate de
astfel de aspecte din geometrie (panta tangentei, arie),
mecanică (viteză, spațiu parcurs; accelerație, viteză;
densitate materială, masă etc.), electricitate (densitate de
repartiție a unei sarcini electrice, sarcină electrică totală),
științe economice, biologie (temporalitate organică față
de cea cronologică), psihologie (temporalitate subiectivă
față de cea cronologică; legea Weber‑Fechner), lingvistică
(densitate de repartiție a vocabularului, vocabular total),
geologie (metoda carbonului radioactiv) etc.
Cele două mari probleme care apar sunt:
Cunoscându‑se descrierea globală a unui fenomen,
cum obținem descrierea sa locală? Aceasta este problema
derivării, care a apărut relativ târziu, abia prin secolul al
XVI‑lea.
Cunoscându‑se descrierea locală a unui fenomen,
cum obținem descrierea sa globală? Aceasta este
problema integrării, care a apărut încă din antichitate
(Eudox, Arhimede) și a continuat în Evul Mediu și în
perioada Renașterii. Unificarea celor două probleme și
nașterea propriu‑zisă a calculului diferențial și integral
s‑au produs cu Leibniz și Newton (secolul al XVII‑lea),
însă la acel moment nu exista încă o reprezentare clară
a naturii funcțiilor supuse operațiilor de integrare sau
413

Solomon Marcus

derivare. Așa‑numita formulă a lui Leibniz și Newton,
de integrare a unei funcții cu ajutorul primitivei
(antiderivatei) funcției respective își găsește, prin
Cauchy (secolul al XIX‑lea), o legitimare în cazul
funcțiilor continue. Ceea ce la Cauchy devine o teoremă
era la Leibniz și Newton un fel de pariu, o întrebare, un
deziderat, un program pe care cercetarea urma să‑l ducă
la îndeplinire.
Problema integrabilității apare pentru prima oară
odată cu Riemann; la scurt timp după acesta, apare și
divorțul dintre primitivă și integrală: există funcții care
au primitivă, sunt mărginite, dar nu sunt integrabile
Riemann (Volterra, Pompeiu), după cum există funcții
care sunt integrabile Riemann, dar nu au primitivă.
Formula Leibniz‑Newton rămâne în vigoare pentru
funcțiile integrabile Riemann care au primitivă.
Cum depășim impasul la care am ajuns? Prin
definirea unei integrale Riemann generalizate, capabile
să integreze orice derivată finită și să restabilească
validitatea formulei Leibniz‑Newton în cazul general,
preconizat de inițiatorii calculului diferențial și integral.
Dar acest deziderat se realizează abia în prima parte
a secolului al XX‑lea (Denjoy, Perron), iar o versiune
elementară a soluției, în spiritul sumelor riemanniene și
al gimnasticii epsilon‑delta, apare abia în a doua parte a
secolului al XX‑lea, prin J. Kurzweil și R. Henstock (a
414

Dezmeticindu-ne

se vedea detaliile în cărțile noastre: Noțiuni de analiză
matematică (1967, p. 182‑187) pentru varianta Perron
și Șocul matematicii (1987, p. 279‑282) pentru varianta
Henstock). Dezvoltările respective nu sunt cu nimic
mai complicate tehnic decât cele relative la integrala
Riemann clasică. Integrabilitatea Lebesgue se definește,
în acest cadru, drept acel caz particular de funcție care
este integrabila Henstock împreună cu modulul ei.
Dacă, pentru funcții mărginite, integrabilitatea
Riemann pe un interval compact implică integrabilitatea
Lebesgue, integrabilitatea Riemann improprie nu implică
integrabilitatea Lebesgue, dar aceasta din urmă o implică
pe aceea Henstock. Nu mai este nevoie să definim aria
în mod separat, înainte de definirea integralei; aria apare
ca un caz particular de integrală. Prin integrare, aria este
în același timp definită și evaluată. Nu mai este cazul să
dezvoltăm separat formule de integrare prin părți și de
schimbare de variabilă pentru primitive, pe de o parte,
pentru integrale, pe de altă parte. Evaluarea primitivelor,
văzute ca un sport în sine, practicat pe multe pagini ale
manualelor din secolul al XX‑lea, nu‑și mai are rostul
acum și poate fi redusă considerabil; accentul ar trebui
pus pe calculul aproximativ, efectuat în combinație cu
programe adecvate de calculator. Merită atenție noțiunile
de diferențiabilitate și diferențială, în perspectiva
aproximării liniare a variației unei funcții.
415

Solomon Marcus

Câteva ecuații diferențiale simple, ca modele
matematice ale unor fenomene fizice sau de altă natură,
pot fi discutate în aceeași perspectivă.
Pentru partea istorică, se poate folosi cartea
noastră, Noțiuni de analiză matematică (1966). Pentru
alte aspecte, ar fi utilă și folosirea vechii cărți a lui A.I.
Hincin, Opt lecții de analiză matematică; ea există și în
versiune românească. Exercițiile la toate cele discutate
mai sus ar trebui să vizeze nu atât proprietățile formale,
cât cele de modelare a unor fenomene naturale sau
sociale; de construit funcția care descrie cutare fenomen
și de studiat derivabilitatea sau integrabilitatea ei, de
construit ecuația diferențială care modelează cutare
fenomen.
Itinerarul schițat mai sus se subsumează unei idei
unitare, aceea a legăturii dintre aspectul local și cel
global al unui proces; matematic vorbind, ea revine
la parcurgerea etapelor prin care derivata și integrala
reușesc să devină operații inverse una celeilalte, întocmai
ca adunarea și scăderea.
În ceea ce privește noțiunile de șir și serie, cu
proprietățile asociate de limită și convergență, ele trebuie
pregătite încă din clasele anterioare, la lecții privind
lungimea unui cerc, progresiile aritmetice și geometrice
infinite și la alte procese care implică șiruri infinite. O
problemă ca: „După câte zile termin de mâncat o pâine
416

Dezmeticindu-ne

dacă mănânc azi o jumătate din ea, mâine o jumătate din
ce a rămas, poimâine o jumătate din ce îmi va rămâne
mâine ș.a.m.d.?”, îl pregătește pe elev încă din clasa
a patra primară pentru ideea de „sumă infinită” de mai
târziu.

417

