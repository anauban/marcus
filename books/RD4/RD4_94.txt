Vorba de cultură
Invitat Solomon Marcus,
Radio România Cultural, 23 decembrie 2011
Ema Stere: Bună ziua, stimați ascultători și bun
venit la Vorba de cultură! În această săptămână, alături
de dl. acad. Solomon Marcus. Am ascultat în deschidere
o piesă care se numește Timp de copii și care vine cumva
în replică la discuția pe care ați inițiat‑o dvs., acum
câteva zile, privind copilăria și importanța ei pentru
tot restul vieții. Aceasta se leagă de cartea Timpul. O
discuție care pe mine m‑a șocat, pentru că nu o știam,
care vine de la Schopenhauer și se referă la timpul
subiectiv și timpul cronologic, este prezentă și în această
carte. “Există patru perioade de viață subiectiv egale,
dar care corespund cronologic următoarelor intervale
temporale foarte inegale. Prima, de la jumătate de an
până la un an și nouă luni, a doua, de la un an și nouă
luni până la șase ani și o lună, a treia, de la șase ani și o
lună până la 21 de ani și a patra, de la 21 de ani până la
75 de ani sau mai mult.” Așadar ceea ce trăim noi până
la un an și nouă luni este subiectiv egal cu tot ceea ce
trăim după vârsta de după 21 de ani.
963

Solomon Marcus

S.M.: Ca bogăție de impresii, de senzații, de emoții
este la fel de bogată ca oricare din perioadele următoare.
Durata cronologică e cu totul alta decât durata
subiectivă, psihologică.
E.S.: Este adevărat că în copilărie zilele parcă
păreau mai lungi și mai bogate în evenimente, dar nici
nu m‑aș fi gândit că perioada de la 21 la 75 de ani are
aceeași bogăție pentru om ca prima perioadă, de un an și
ceva.
S.M.: Aici au intervenit intuiții venite încă din
secolul al XIX‑lea, apoi au fost și din partea unor
romancieri. În Muntele vrăjit de Thomas Mann, Castorp,
unul din personajele principale, exprimă o intuiție de
acest fel. În a doua jumătate a secolului trecut, cercetarea
științifică s‑a referit la aceste date și le‑a confirmat în
bună măsură, în școala lui Jean Piaget, în cercetările din
cele două Americi. Acestea din urmă au adâncit acest
decalaj între durata psihologică și durata cronologică.
E.S.: Ca matematcian, cum demonstrați așa ceva?
S.M.: Aici se întâlnesc două tipuri de abordare.
Abordarea intuitivă‑senzorială, pe baza căreia s‑au
exprimat filosofi și alți gânditori sau romancieri ca
Thomas Mann. Abordarea experimentală propune
experimente cu persoane de diferite vârste și se
înregistrează datele. Abordarea teoretico‑matematică,
de pildă faptul de a spune că pe măsură ce înaintăm în
964

Dezmeticindu-ne

vârstă timpul trece mai repede, conduce la o anumită
ecuație diferențială. Soluția ecuației poate să fie o funcție
logaritmică. Se știe că logaritmul de x crește mai încet
decât x. Când x tinde la infinit și logaritmul de x tinde
la infinit, dar mult mai încet. Dar se poate întâmpla ca
timpul subiectiv să fie o funcție polinomială de timpul
cronologic.
E.S.: Eminescu spunea “Clipe dulci  ce par ca
veacuri”. Timpul subiectiv are și el salturi personale de‑a
lungul vieții.
S.M.: Evident, da. Și în La steaua și în unele
Scrisori și în Luceafărul: “Și căi de mii de ani treceau/
În tot atâtea clipe”.
E.S.: Ar fi o suprapunere de timpi subiectivi. Pe de
o parte, această scindare a cronologiei în etape diferite,
corespunzătoare unor etape psihologice diferite, pe de
altă parte evenimentele din viața unui om.
S.M.: Sigur, aici intervin mai mulți parametri.
Pentru un copil, nu toate perioadele temporale sunt
percepute la fel sub aspect subiectiv. Evenimentele care
îl surprind pe copil și care se transformă în spectacole
determină un anumit tip de percepție, în timp ce
evenimentele de care copilul se plictisește generează
un alt tip de percepție. Timpul subiectiv diferă de la un
eveniement la altul, în funcție de natura evenimentului.
Un copil care stă pironit pe ecranul televizorului, unde
965

Solomon Marcus

evoluează tot felul de animale sălbatice îi paralizează
atenția atât de puternic, încât nici nu știe când a trecut
timpul. În schimb, dacă îl pui să se uite la lucruri pe care
nu le înțelege, timpul trece mai greu. Apreciarea se face
global, se consideră percepția pe durate mari.
E.S.: Am în față cartea Întâlniri cu Solomon
Marcus, în care aveți un text autobiografic, De la
provocări, spaime și traume la mulțumire și bucurie.
Prima amintire pe care o notați aici este pisica de la
fereastră.
S.M.: Și ieri și azi, înainte de a veni aici, la radio,
mi‑a tăiat calea o pisică neagră.
E.S.: Sunteți superstițios?
S.M.: Absolut deloc, dar interesul meu pentru acest
animal este foarte mare. Puteți vedea ce loc important
ocupă pisica în poezia lumii. Mari poeți au dedicat
poeme pisicilor într‑o măsură mult mai mare decât
câinelui. Poate în scrierile în proză câinele e mai prezent,
dar în poezie pisica se detașează.
E.S.: În ce sens v‑a marcat?
S.M.: În sensul misterului și al ambiguității.
Mi se pare un animal care tot timpul e curios. Are o
privire scrutătoare, care te sfredelește pur și simplu. Îmi
amintesc acum un vers: “Pe laiță sub scoarțele moi/ Cu
murmure line torcea un pisoi/ Uitându‑se țintă la mine”.
Este un animal care te impresionează în primul rând
966

Dezmeticindu-ne

prin câte de frumos se joacă. Are o adevărată vocație a
jocului și a misterului. Mai are și vocația atașamentului.
Îți dă impresia că are nevoie să vină lângă tine, exprimă
puternic această dorință de companie. Baudelaire s‑a
referit la asemănarea dintre femeie și pisică. Esenin
s‑a referit în special la vocația ludică a pisicii. Este un
animal care exprimă atmosfera de cămin pe care o
întâlnim, spre exemplu, la Bacovia, deși la el nu apare în
poezie pisica.
E.S.: Biologii spun că pisica, câinele și toate
animalele pe care le ținem ca animale de companie își
prelungesc de fapt comportamentul de pui prin faptul că
nu trebuie să‑și procure singure hrana. Noi le prelungim
copilăria și, de aici, și joaca la vârsta adultă.
S.M.: Nu vânează șobolani? Goana pisicii după
șobolani e tot din dorința de a se juca. După ce îl prinde,
ai impresia că predomină dorința de joc mai mult decât
cea de a se hrăni.
E.S.: Este remarcabil că prima dvs. amintire
privește o pisică și nu altceva.
S.M.: Da. Pisica mai are și vocația contemplativă.
Găsește totdeauna locul care‑i oferă poziția cea mai
comodă și stă într‑o atitudine de contemplare. La alte
animale, nu am mai întâlnit o astfel de atitudine.
Povestea recentă, cu tigrul care a fost scăpat din
cușcă la Sibiu, a fost un spectacol grandios. Înainte de a‑l
967

Solomon Marcus

prinde, au arătat modul în care tigrul alerga în pădurea
vecină. Se vedea cu câtă voluptate savura libertatea. Era
de o frumusețe grandioasă. Tigrul era în această ipostază
de animal care își trăiește fericirea de a fi scăpat din
cușcă și aleargă cu mișcări de o frumusețe excepțională.
Era în același timp atât de speriat, pentru că simțea că în
această libertate e ceva nenatural. De aceea a încercat să
atace un medic veterinar, iar oamenii, ca să se apere, l‑au
împușcat. Compar cum arăta el ca un animal viu care
alerga liber în pădure și apoi trântit ca o masă de carne
pentru care se făceau eforturi la transport. A fost ceva
foarte interesant, dar și de o mare tristețe. Îți dai seama
că natura e făcută în așa fel încât bucuria unei specii se
produce pe seama nenorocirii altei specii. Modul în care
natura vie supraviețuiește este pe baza acestei situații
foarte des întâlnite și în societatea umană. Te întrebi dacă
lumea animală și vegetală e construită pe acest principiu
al unui echilibru între fericire și nenorocire, nu cumva și
societățile umane, în mod inevitabil, trebuie să se supună
unei situații similare. Omul se hrănește, supraviețuiește
pe seama regnului animal și a celui vegetal.
E.S.: Am promis ieri că vom discuta despre viitor.
Dacă matematicienii intervin în această discuție privind
echilibrul ecologic, de exemplu, spuneați de balansul
dintre specii. Evident, sunt multe specii pe cale de
dispariție. Lucrurile acestea sunt statistic controlabile?
968

Dezmeticindu-ne

Din punctul de vedere al unui matematician, statistician,
există o privire sumbră sau o vedere luminoasă asupra
viitorului?
S.M.: Aici sunt probleme foarte delicate și părerile
sunt împărțite. Planeta are anumite resurse, care nu sunt
nelimitate. Spre exemplu, hârtia înseamnă exploatarea
pădurilor. Rezervele forestiere ale planetei se știe bine
acum că nu pot asigura necesarul de celuloză pentru
nevoile de hârtie în plină ascensiune. Viitorul cărții
depinde foarte mult de această situație.
Probabil că se va pune într‑un viitor mai îndepărtat
problema mutării pe alte corpuri cerești a unei părți din
omenire.
Primul viitor este cel denumit de cuvântul mâine.
Am văzut că, în unele discuții culturale, apare întrebarea
“Cum ți‑ai trăi ziua de mâine, dacă ai crede că e ultima
ta zi de viață?” Într‑o astfel de zi aș fi deja pe jumătate
mort, pentru că nu pot concepe să trăiesc fără un mâine
în fața mea. Există un mâine pe care mi‑l proiectează
relațiile cu ceilalți. Trebuie să am în vedere că la început
de martie voi participa la o întâlnire tradițională care are
loc la Istanbul și trebuie să trimit acolo o lucrare până
la o anumită dată, să mă organizez din timp. Trebuie
să mă organizez de pe acum, dat fiind faptul că în luna
iunie voi avea datorii științifice în Finlanda și apoi, în
octombrie, voi dori să particip la Congresul Internațional
969

Solomon Marcus

de Semiotică ce va avea loc în China. Acum, toată lumea
științifică e organizată pe câțiva ani înainte. Trebuie să
știu că m‑am angajat să predau la o editură un manuscris
ca să apară peste 2 ani și să organizez de pe acum în
etape bine eșalonate redactarea manuscrisului. Deci
modul în care noi interacționăm cu semenii noștri și cu
felul în care vrem să participăm în marile competiții
culturale ne obligă la realizarea unui calendar pe 2‑3
ani de acum înainte. Nu întâmplător aproape toate
calendarele de tip carte care se editează cuprind, pe
lângă anul în curs, și vreo doi ani după el și anul care
tocmai s‑a încheiat. Până la urmă, timpul e definit de
evenimente și nu e un cadru vid.
În momentul în care un om nu mai este implicat
în niciun fel de eveniment, pentru el se poate spune că
timpul nu prea mai există.
E.S.: Este ceea ce prevedea Alvin Toffler în Șocul
viitorului, încă din anii ’70?
S.M.: Știința, începând de la Leibniz și Newton
și culminând cu secolul al XIX‑lea, era definită în
primul rând prin capacitatea ei de a prevedea. Era chiar
sloganul francez „Savoir c’est prévoir”, „A ști înseamnă
a prevedea”. Acest lucru a fost valabil până în secolul
al XIX‑lea, cu marile succese ale predicțiilor eclipselor
de lună și de soare, care au dat oamenilor încredere
în capacitatea predictivă a științei. Credeam că dacă
970

Dezmeticindu-ne

scriem ecuația diferențială a unei mișcări, ecuația ne va
arăta și cum s‑a mișcat acel corp în trecut și cum se va
mișca în viitor. Situația s‑a schimbat radical în secolul al
XX‑lea, când funcția principală a științei nu mai este cea
predictivă, pentru că știința a eșuat în această privință
în tot mai multe situații. Pe primul loc a trecut funcția
ei explicativă, a înțelege cum se desfășoară lucrurile.
Aceasta a schimbat complet întregul scenariu.
Capacitatea predictivă a lingvisticii e foarte redusă.
Nu e posibil să știm cum vor arăta limbile peste 300
de ani. Se pot emite anumite păreri, dar nu au valoare
riguroasă.
Predicția a avut succes atâta vreme cât se referea
la fenomene care presupuneau o distincție netă între
subiect și obiect. Puteam spune că subiectul, cel care
observă fenomenele, este separat net de fenomenele și
procesele care sunt observate și studiate. Pentru mișcarea
corpurilor cerești, condiția aceasta era îndeplinită, ființa
umană nu are cum să influențeze mișcarea corpurilor
cerești. Dar în măsura în care noi am luat în atenție
fenomene de altă natură, această separație netă între
subiect și obiect n‑a mai funcționat. Așa se întâmplă în
mecanica cuantică, unde observatorul își pune amprenta,
modifică pur și simplu comportamentul obiectului
observat. Aceasta se întâmplă în toate fenomenele
social‑economice. Un economist care încearcă să
971

Solomon Marcus

facă o predicție despre fenomenele economice în
desfășurare, riscă, după ce a făcut predicția, să constate
că agenții implicați în procesele economice își schimbă
comportamentul, în așa fel încât să invalideze predicția
făcută.
Deci nu se mai vorbește despre predicție decât
într‑o anumită măsură. N‑a dispărut complet, dar rolul
ei este incomparabil mai mic decât cel pe care‑l avea în
secolul al XIX‑lea.
Vă dați seama cum decurg lucrurile dacă vă uitați cu
atenție la corpul uman. El are o capacitate reactivă, adică
de a reacționa la procesele care se desfășoară, dar și una
anticipativă, de a face față unor posibile evenimente
ulterioare. Un biolog german a propus, în prima jumătate
a secolului trecut, noțiunea de Umwelt, mediu ambiant,
dar nu cel în care nou născutul se trezește când își începe
viața, ci acea componentă a mediului care face parte din
ereditatea noastră. Existența unui mediu ambiant care e
proiectat de propria noastră ereditate. Când vorbim, deci,
de mediul ambiant, acolo e o componentă care preexistă
ființei care tocmai s‑a născut și o componentă adusă
de ereditatea celui care se naște. Cel care se naște vine
pregătit cu un anumit mediu ambiant, care să‑i asigure
într‑o anumită măsură supraviețuirea.
E.S.: Dacă se va naște un copil de eschimos va fi
mai rezistent la frig decât un copil de african.
972

Dezmeticindu-ne

S.M.: Exact, pentru că în ereditatea lui există acest
Umwelt. Viața își ia aceste precauții pentru supraviețuire.
Vin și cu propria mea umbrelă, nu mă bazez doar pe
adaptarea la mediu.
Lucrurile acestea sunt valabile și pentru sistemele
sociale, economice, etc. Aici e problema cu viitorul. Sunt
două moduri de a înțelege viitorul. E o linie, care vine de
la Parmenide și Arhimede, trece prin Newton și ajunge
la Einstein, conform căreia timpul de fapt aproape că nu
există, timpul este spațiu. Un parametru t pe care‑l pot
reprezenta pe o axă. Și în muzică reprezentăm pe abscisă
timpul și pe ordonată frecvența.
Există însă o altă tradiție, care vine de la Aristotel,
de la Heraclit și mai aproape de zilele noastre Bergson,
dar și de la civilizațiile extrem orientale, conform
căreia timpul e mult mai mult decât durată. Timpul nu
e reductibil la spațiu, numai că deocamdată în știință a
câștigat prima variantă. Întreaga știință a redus timpul la
spațiu. Observați că mai toate metaforele temporale sunt
de natură spațială. Problema este că pot să mai disting și
între două modalități de a privi viitorul.
E foarte interesant să vedeți cum priveau viitorul
cei din trecut. O să observați că în general oamenii
s‑au înșelat în materie de predicție. Marele nostru
matematician Traian Lalescu la 1914 ținea un discurs
pentru studenți și le spunea: “Am avut o perioadă
973

Solomon Marcus

tulbure, de conflicte sociale, în sfârșit acum sperăm
să avem o perioadă de liniște.” La scurt timp după
aceasta avea să izbucnească Primul Război Mondial.
Incapacitatea întregii comunități de oameni care se ocupă
de științele politice de a prevedea eșecul peste noapte al
Uniunii Sovietice este și ea semnificativă.
E.S.: Ce părere aveți despre viitor? Știu că
discuțiile despre viitor vă plac, sunt dintre acelea în care
disciplinele se amestecă, în care glasul matematicianului
este obligatoriu, dar incert. Mă gândesc aici la faptul că
nu s‑a tranșat încă problema încălzirii globale.
S.M.: De ce a devenit tot mai dificil să ne
reprezentăm viitorul? Din cauză că a crescut peste orice
limite complexitatea fenomenelor și proceselor în care
trăim. Este atât de înaltă, încât nu o mai putem stăpâni.
Atunci, copleșiți de ignoranță, neantrenați pentru efort
personal, cădem prizonieri ai unui viitor care vine peste
noi ca un puhoi, ca un tsunami.
Vezi reviste pline cu pagini întregi de horoscop,
adică nu rămâne decât să ne supunem viitorului și să
aflăm de la vrăjitoare, numerologi și astrologi cam ce ne
așteaptă.
Rostul educației este de a respinge total această
mentalitate și de a accentua măsura în care oamenii pot
ei să determine viitorul lor. În măsura în care noi suntem
ființe educate și de un nivel cultural ridicat viitorul este
974

Dezmeticindu-ne

propria noastră construcție. Există chiar o carte, Omul
multi‑dimensional, pe care o descriu în Timpul, în care se
arată acest lucru. Un om se naște cu acces la un anumit
număr de dimensiuni, dar el poate prin cultură, prin
cercetare, să‑și mărească numărul de dimensiuni la care
are acces, adică să mărească accesul lui la complexitate
înaltă. Putem ca noi să construim, să determinăm cum
vor arăta anii care vin.
Vă dau exemplul extraordinar al lui Steve Jobs,
co‑fondator Apple, care a reușit, prin capacitatea lui
creatoare, să determine printr‑un bun număr de ani de
aici înainte modul în care arată și va arăta preocuparea
de bază a copiilor, modul în care copilul se joacă și
folosește internetul. Prin rolul său în crearea iPad‑ului,
iPhone‑ului care acum devin tot mai populare, iată un
om care schimbă viața. Care determină cu ce ne ocupăm
noi, de exemplu.
E.S.: Câștigăm ceva, pierdem ceva? Bunul sălbatic
al lui Rousseau mai are vreun loc în ecuația asta?
S.M.: Nu spun că putem să determinăm viitorul în
întregime. Lumea financiară, de pildă, este foarte greu
să o înțelegi. Dar noi trebuie să învățăm să lucrăm nu
cu un singur scenariu de viitor, ci cu mai multe scenarii.
Să procedăm nu cu o temporalitate în linie dreaptă,
ci cu una arborescentă, în care, la diverse momente,
există diverse ramuri posibile, iar noi să fim pregătiți
975

Solomon Marcus

pentru oricare din ele. E singura strategie rezonabilă,
inteligentă. Dar pentru asta este nevoie de multă
învățare, știință, multă capacitate de visare și imaginație.
E.S.: Vă mulțumesc mult, dle. academician!

976

