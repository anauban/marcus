Loading paths...
Loading text...
Statistical significance for readability: t-statistic, pvalue =  Ttest_indResult(statistic=4.0699529620748551, pvalue=5.7897474976546367e-05)
Statistical significance for avg_senlen: t-statistic, pvalue =  Ttest_indResult(statistic=0.79907741060838877, pvalue=0.42477534873816236)
Statistical significance for lexical_density: t-statistic, pvalue =  Ttest_indResult(statistic=5.0095910538555843, pvalue=8.5855841283881868e-07)
Statistical significance for lexical_richness: t-statistic, pvalue =  Ttest_indResult(statistic=4.1910510413675217, pvalue=3.5021337085274579e-05)
Statistical significance for avg_wlen: t-statistic, pvalue =  Ttest_indResult(statistic=10.601829058401753, pvalue=5.074585865790562e-23)
