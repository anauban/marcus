This project tries to comparatively analyze some works of Solomon Marcus: the articles published in the 4 volumes of "Răni deschise", 2 of them (2+3) written during the communist regime, and the other 2 (1+4) post-communist.

In the end we hope to reach a conclusion about how the text evolves, and if any significant differences can be observed between the texts in the two periods (communist and post-communist), and which.

The texts should be analyzed from both a stylome and a topic perspective (and others?).

##Contents of repo

- Data is all in the `books` directory
- Main code file is `marcus_stylome.py`

##Preprocessing

###Splitting the text into articles

Starting from 4 pdfs each containing 1 volume of "Răni deschise", the texts were split into individual articles.

All texts can be found in the `books` directory. They are organized in 4 directories, each corresponding to 1 volume (RD1 = Răni deschise 1 ...etc). 

Each directory contains all articles in the corresponding volume, along with the table of contents for each volume ("cuprins").

###Labelling texts with year of publication and publication name

On top of the high-level communist and post-communist categories, each article was also labelled with its exact year of publication.

A json with the year for each text can be found in `books/books_years.json`.
The code which does the labelling is `label_years()`, in `marcus_stylome.py`.

I also tried to label each text with the name of the publication where it was originally published. This labelling has some flaws, but the results so far can be found in `books/books_publications.json`. The code is in `label_publications()`, in `marcus_stylome.py`

###Other 

Other lists were made with some special types of texts which we may want to treat differently. These were manually labelled, and stored as lists of text names in .txt files:

- `nonro_texts_names.txt`: texts which are not in Romanian (a minority)
- `interviews_texts_names.txt`: texts which are interviews. We may want to treat these differently because they include lines of the interviewer.
- `otherexcept_texts_names.txt`: other texts which are not in the above categories (probably) but I decided they should be excluded from the analysis for some reason, can't currently remember why.

##Analysis

The 2 major types of analysis proposed are: stylome and topic modelling.
So far stylome was more thoroughly done.

Implemented analyses:

- represent text as vector of frequencies of stopwords (or, optionally, most frequent words including content words), truncated at a certain length, reduce to 2 dimensions using PCA (`text_2Drepresentation()`)
- create scatterplot of these representations in 2D (`plot_2D_representations()`). Label the points by coloring them according to volume (RD1/RD2/RD3/RD4), or optionally by setting their size proportional with publication year etc
- draw dendrogram (`draw_dendrogram()`)
- optionally group texts into larger groups of texts, each belonging to the same volume, did this because individual articles seemed to be too small to be relevant + the dendrogram turned out to be too big. `load_texts_grouped()`. Caveat: currently the code just groups them arbitrarily without any criteria, could be done better (by year etc)

##Results

- when using individual articles (not grouped) dendrogram is too large and results do not look significant
- when grouping texts, both dendrogram and scatterplot look better, the texts seem to cluster according to the 4 volumes
- when doing dendrogram of whole books they also get grouped as we'd expect: 2+3 and 1+4

##TODO

- better grouping of texts
- topic modelling
- find most discriminating features between the 2 groups
- conclude on what are the main differences between the 2 groups if any

## Some plots

- stylome PCA analysis on the grouped texts (as vectors of stopwords frequencies):

![stylome_grouped](plots/marcus_stylome_PCA_grouped.png)

- stylome dendrogram on grouped texts:

![stylome_grouped2](plots/marcus_stylome_PCA_grouped.png)

- stylome PCA on whole books

![stylome_books](plots/marcus_stylome_PCA_books.png)

- stylome dendrogram on whole books

![stylome_books](plots/marcus_stylome_books.png)

- stylome PCA analysis on individual articles

![stylome_all](plots/marcus_stylome_PCA.png)

- stylome dendrogram on individual articles

![stylome_all2](plots/marcus_stylome_all.png)

- topic analysis (using all words as features, otherwise same as above), PCA, on grouped articles

![topic_grouped](plots/marcus_topic_PCA_grouped.png)

## Evolution of some text properties over time

We also looked at some text properties indicative of style, and how these change in time, for each of Marcus' essays. We plot the year the essay was written and for each essay, the value of the metric.

There is a slight evolution noticeable for some of the metrics, but overall they don't seem very conclusive.

### Word length

![word_length_variation](marcus_word_length_variation.png)

### Sentence length

### Readability

### Lexical richness

### Lexical density